---
layout: post
title: "First of all, Thanks Ray for getting the new board out to me"
date: November 07, 2018 21:11
category: "FirmWare and Config."
author: "Kestrels Fury"
---
First of all, Thanks Ray for getting the new board out to me.  It is working like a dream.  

A question for all:  



I have one of the digital power displays.  I have seen where some of the analog settings should be about 10 mA.  Where should be the setting for the digital controllers?  When I am running the test g code, all the lines look like the same power setting.



Thanks to you all.





**"Kestrels Fury"**

---
---
**Joe Alexander** *November 07, 2018 22:03*

try running a grayscale test so you can see the differences between the intensities. google has plenty of images that are easy to grab, then tweak the power until your satisfied with the results.


---
**Ray Kholodovsky (Cohesion3D)** *November 07, 2018 23:09*

Docs —> faq —> pwm tuning. 


---
**John Harris** *November 12, 2018 04:38*

What wattage is your laser tube? I have a K40 with a 40 Watt tube with a digital percentage display and no Amp meter. I looked up my tube and the max that it can handle is 18mA so I installed an Amp meter in my K40 and tested what settings equaled what amperage. This is my settings. Yours might be different:

![images/dc65ca9f782b57d537a4fcc1bea0d392.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dc65ca9f782b57d537a4fcc1bea0d392.jpeg)


---
**samsunet** *January 18, 2019 14:04*

is this faq only for smoothi or is it working with grbl-lpc?


---
*Imported from [Google+](https://plus.google.com/104761945487452832708/posts/8JbQJr4eCpt) &mdash; content and formatting may not be reliable*
