---
layout: post
title: "First off, thanks for the great products and website"
date: April 03, 2018 00:21
category: "General Discussion"
author: "Frank W"
---
First off, thanks for the great products and website. I just received my Cohesion 3D mini and got it installed and everything appears to be working good. My question is about the GLCD. I have it installed and it too is working. I can figure out most of the menu options but there are some that I am not sure what the do? I thought I'd ask if there is any instruction about the menu option before I just jump right in and start experimenting and maybe frying something. Any help would be greatly appreciated.





**"Frank W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2018 02:05*

Not really.  WCS is Work Coordinates and MCS is Machine Coordinates - you can look those up if you want to know what the columns on the home screen actually mean :)  I just ignore the right column. 



Jog will move the head. 



You won't use probe but probe --> status is a useful way to see the endstop states if you need to check them. 



Play is how you would select a gcode job file on the MicroSD Card in the Mini board to run. 


---
*Imported from [Google+](https://plus.google.com/103281174995497406502/posts/Uh4N1c9yg1x) &mdash; content and formatting may not be reliable*
