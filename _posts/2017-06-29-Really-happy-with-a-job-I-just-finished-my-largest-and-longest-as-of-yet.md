---
layout: post
title: "Really happy with a job I just finished, my largest and longest as of yet"
date: June 29, 2017 03:27
category: "Show and Tell"
author: "John Milleker Jr."
---
Really happy with a job I just finished, my largest and longest as of yet. Thought I'd add it to the Show and Tell of what my machine and the C3D board can accomplish. Don't mind the bad lighting, I haven't hooked my LED's back up. The detail on this is impressive.



"The Battle of Gettysburg" laser engraved on natural slate, original artists Kurz and Allison, 1886. Picture before treatment with a walnut oil/beeswax mixture.



Details: Source file was 150mb TIF file, resized to 8x12" 300dpi photo that was sharpened, adjusted and inverted in Photoshop. Converted into a 65mb gcode file in LaserWeb4, 2+ hour engrave using the awesome Cohesion3D Mini on a generic K40 Chinese Laser at 10% power (100% set at 15ma) and a speed of 125mm/s w/Air Assist. Cohesion3D Mini and steppers being driven by 24v 6a external power supply. PWM set to 400 in config. Exported from LaserWeb4 to a gcode file, copied in Windows to the SD Card and then unmounted and physically disconnected from the computer. Run completely from the SD Card/LCD.

![images/e224731d55abdee8e3cf569219835d32.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e224731d55abdee8e3cf569219835d32.jpeg)



**"John Milleker Jr."**

---
---
**Ashley M. Kirchner [Norym]** *June 29, 2017 03:41*

That looks amazing, great job!


---
**Ned Hill** *June 29, 2017 04:01*

Wow, that looks awesome!  Are you running slow at low power because it's slate?


---
**Joe Alexander** *June 29, 2017 05:34*

Awesome engraving job and great post. I cannot thank you enough for thinking about your post ahead of time and including all the pertinent information. I tip my hat to you good sir :)


---
**John Milleker Jr.** *June 29, 2017 05:54*

Thanks, the machine did all the work. I just gave it the file.. :)



Ned, I'm running at that speed because I really like the quality of 10% at 125mm/s. I even like 5% power too. If I were to test 300, 350 and 400mm/s (my test file goes to 250) I might like something there too, but I'm still a little gun shy over having lockup and shifting issues with my previous C3D board.



My test file (crummy cell phone shot) is cut using Laser Fill in Laserweb and the power starts at 5, and then jumps by ten at 10-100. Speed is 5, 10, 25, 50, 75, 100, 150, 200 and 250. The letters are difficult to see.



![images/a98246b19852dc1b5bb1d357edbf08d2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a98246b19852dc1b5bb1d357edbf08d2.jpeg)


---
**Tako Schotanus** *March 02, 2018 19:03*

Hi **+John Milleker Jr.**, I only just saw your post and I'm intrigued by your test file. Is that a single file that tests all the different speeds and power settings? How did you make that? Is it easy to do? Is it perhaps something that you could share with us? Because it seems like the perfect test file for testing new materials.


---
**John Milleker Jr.** *March 03, 2018 05:37*

**+Tako Schotanus** - Just a simple grid of squares. Make one for scanning and one for cutting. Then run one for each material you use. Takes the guesswork out of what to set for a certain material and if you suspect you have a hardware issue, you can always refer to the test cuts to compare.



I made this file back when I used LaserWeb. The file is available as an SVG or a LaserWeb file with the cut and speed settings already there. LaserWeb is free, if you're using a much better program (like LightBurn) with your C3D you can still get LaserWeb to run the file.



[thingiverse.com - Laser Cutter Material Test Chart by SDMFabShop](https://www.thingiverse.com/thing:2530600)


---
**Tako Schotanus** *March 03, 2018 16:48*

Very useful, thanks **+John Milleker Jr.**!


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/9jmvR7NzCWB) &mdash; content and formatting may not be reliable*
