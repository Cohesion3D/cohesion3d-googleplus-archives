---
layout: post
title: "So here goes, I am attempting to double my y axis nema 23 stepper motors"
date: August 04, 2017 18:22
category: "C3D Remix Support"
author: "Joshua Jones"
---
So here goes, I am attempting to double my y axis nema 23 stepper motors. I am using the Cohesion 3d remix for a cnc machine utilizing 4 external TB6600 Stepper Motor Drivers.  My question is how do you clone an axis when going to an external driver?



1. Can I clone (A) to match (Y)using the External Stepper Driver Adapters within the software?



2. Or should I Run both sets of signal wires from (A) External Stepper Driver Adapter to each of the external TB6600 Stepper Motor Drivers?





**"Joshua Jones"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 18:58*

Cannot clone an axis in config (this would result in only being able to send step pulses half as fast, which the devs didn't want to compromise on).  So option 2 is the way to go. 



If you're building an OX where the Y motors need to spin in opposite directions then flip the wire order for the 2nd one on the tb6600 outputs where you connect the motor.


---
**Joshua Jones** *August 04, 2017 19:49*

would this be correct then? Thanks for the help. 

![images/0356ed78b1106c7982dfd609f8ad3c8f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0356ed78b1106c7982dfd609f8ad3c8f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 19:55*

I see other issues.  Please follow this guide: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



The 5v on the ReMix is in top right, please consult pinout diagram. 


---
*Imported from [Google+](https://plus.google.com/105400335561752829962/posts/EMXvn9qWQi4) &mdash; content and formatting may not be reliable*
