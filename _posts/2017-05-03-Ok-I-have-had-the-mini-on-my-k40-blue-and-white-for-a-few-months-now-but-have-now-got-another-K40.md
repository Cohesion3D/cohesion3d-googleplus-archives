---
layout: post
title: "Ok, I have had the mini on my k40 blue and white for a few months now, but have now got another K40"
date: May 03, 2017 09:54
category: "C3D Mini Support"
author: "Colin Rowe"
---
Ok, I have had the mini on my k40 blue and white for a few months now, but have now got another K40.

Its running the M2 nano board and does not have the ribbon cable, looks like the end stops are on one of the cables.

It has the digital readout and buttons, unlike the old one i had (pot and Analogue readout)

I need to check all the wires out but want to swap out the M2 for the mini.

The psu was blown on this one so will be putting a new one in (not same as came with it)

The new psu is this one

[http://www.ebay.co.uk/itm/322470377835?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/322470377835?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) 



see pic for the board, any advice before i wire it up and where di all the wires go.

The psu outputs are in with the pictures. Thanks

Thanks



![images/3adec11f858603f8186a2553c1761487.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3adec11f858603f8186a2553c1761487.jpeg)
![images/6c6509dedd130d5bc47e9523efd6262e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c6509dedd130d5bc47e9523efd6262e.jpeg)
![images/07048e26c3c2041a42617286d9e20371.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07048e26c3c2041a42617286d9e20371.jpeg)
![images/bc2ea67f63b64ec9eae3f5cc91d0991f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc2ea67f63b64ec9eae3f5cc91d0991f.jpeg)
![images/a071714a68ec8625851094290b57e16c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a071714a68ec8625851094290b57e16c.jpeg)
![images/4f7b7ece8b603f6b426ac40573383f1e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f7b7ece8b603f6b426ac40573383f1e.jpeg)
![images/77f051128bb0ddfa8bdab48083823d4c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/77f051128bb0ddfa8bdab48083823d4c.jpeg)

**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2017 13:08*

See the instructions, they cover this board type. 


---
**Colin Rowe** *May 03, 2017 19:50*

Ok rewired and all ok no shorts now. New psu in along with the new board. All going ok but I cannot get the y stepper motor to work correctly.  Swapped wires over so it moves in correct direction and it ref to corner ok but jog it around it jumps around.



Will post video when done.

Photos of wires attached.

![images/094c8cf26aafd0cfc9dfe62c412ace45.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/094c8cf26aafd0cfc9dfe62c412ace45.jpeg)


---
**Colin Rowe** *May 03, 2017 19:50*

![images/34147c40bbc3cdc6308e5c12e65e4390.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/34147c40bbc3cdc6308e5c12e65e4390.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2017 20:55*

What specifically do you mean by work correctly? Also I don't see a video here. 


---
**Colin Rowe** *May 03, 2017 21:03*

[facebook.com - Log In or Sign Up to View](https://www.facebook.com/colin.rowe.5895/videos/10154776070742858/)


---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2017 21:06*

Gotcha.  You can try increasing power to the drivers (power off and turn the pot clockwise 45 - 90 degrees).  But I think it could be a mechanical interference in the machine itself.  Have you run this particular machine before? 


---
**Colin Rowe** *May 03, 2017 21:12*

no, bought it as new with blown psu. I changed psu and re wired it all as dont trust China electrics.

I will strip out bed and check for binding but moves ok with motors off, I did a light oil on side runner and moves even better.

Will check wire and power feed but all looks ok so far.

where is the pot?




---
**Ray Kholodovsky (Cohesion3D)** *May 03, 2017 21:15*

[lh4.googleusercontent.com](https://lh4.googleusercontent.com/-8DvpkJyHmy0/Tsd5FdyLquI/AAAAAAAAF2U/Qm9xijmsHEI/s576/pololu.jpg)


---
**Colin Rowe** *May 03, 2017 21:22*

thanks, just seen it. will try tomorrow




---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/GJbDd8YsjVm) &mdash; content and formatting may not be reliable*
