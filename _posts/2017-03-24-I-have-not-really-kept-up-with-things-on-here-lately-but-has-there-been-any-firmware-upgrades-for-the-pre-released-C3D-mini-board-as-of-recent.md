---
layout: post
title: "I have not really kept up with things on here lately but has there been any firmware upgrades for the pre-released C3D mini board as of recent?"
date: March 24, 2017 22:22
category: "General Discussion"
author: "Jonathan Davis (Leo Lion)"
---
I have not really kept up with things on here lately but has there been any firmware upgrades for the pre-released C3D mini board as of recent? though still  on my Chinese benbox laser engraving machine I am still trying to figure out how to get the endstops installed properly without having to buy a 3D printer to make the parts. 





**"Jonathan Davis (Leo Lion)"**

---
---
**Griffin Paquette** *March 25, 2017 00:26*

Cohesion3D doesn't have their own specific firmware updates. Check out the Smoothieware site to see if your firmware version is the newest stable version. 


---
**Jonathan Davis (Leo Lion)** *March 25, 2017 03:06*

**+Griffin Paquette** okay and good to know though my board was a pre release of C3D mini board that Ray built himself. 


---
**Alex Krause** *March 25, 2017 04:43*

If you need parts 3d printed to assist in endstop installation there are plenty of people in the community that could help you out...


---
**Alex Krause** *March 25, 2017 04:44*

**+Jonathan Davis**​ it still runs the same firm ware as all the other boards 


---
**Griffin Paquette** *March 25, 2017 11:25*

**+Jonathan Davis** his prerelease has only hardware changes from the official (endstop connectors and mosfet connectors in particular) you should be completely fine with the stock smoothie firmware. As long as your config file is set to your machine then you should be good. Hope this helps!


---
**Jonathan Davis (Leo Lion)** *March 26, 2017 23:03*

**+Griffin Paquette** alrighty and my machine was originally Arduino based before it was converted 


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/JDMzG2Dponu) &mdash; content and formatting may not be reliable*
