---
layout: post
title: "Hi all I have a K40 laser that currently has a Arduino UNO as the controller"
date: April 08, 2018 02:01
category: "C3D Mini Support"
author: "tony turner"
---
Hi all I have a K40 laser that currently has a Arduino UNO as the controller. So all the old controller and control panel have been discarded, but I see that the cohesion board connects to the ribbon cable from the old control panel.

Does it need this to operate or is there a way to wire up the missing controls that were on the panel. I wish to find this out before I make my purchase of the cohesion controller





**"tony turner"**

---
---
**Anthony Bolgar** *April 08, 2018 02:13*

The only front panel item that connects to the mini is nothing at all . The current regulation pot, mA meter, test fire button, and laser on/off and main power on off switches all connect to the PSU. If you add a GLCD it connects to the mini if you bought the glcd adapter with it, but this is not necessary, especially if you flash it with GRBL-LPC which does not use the GLCD, only use it if you are flashing smoothieware to the mini.. Hope this helps


---
**tony turner** *April 08, 2018 03:03*

Still a bit confused as looking at the instructions and the photo's it has the ribbon cable from the original front panel plugged into the cohesion mini board. My laser did have the digital readout panel. I have found the old controller board but not the front panel board or connectors. I will buy the Cohesion3D Mini Laser Upgrade Bundle which has the firmware with it


---
**Joe Alexander** *April 08, 2018 03:15*

the ribbon cable you see is the one that comes from the small board mounted on the side of the x-axis motor which also has the circuitry for the optical endstop. Even then I'm pretty sure all those pins are accessable from other points.


---
**tony turner** *April 08, 2018 03:46*

this is the cable I am referring to the flat one with the writing on it. Sorry should have been much clearer to start of with.



[lh3.googleusercontent.com](https://lh3.googleusercontent.com/-nmYqvPeqU4Q/WgsrgjxH3kI/AAAAAAAAFcA/qUgbANKUY_U32jZiqmQL_K7fnbvirO0ggCJoC/w530-h942-n/20171109_105027.jpg)


---
**Anthony Bolgar** *April 08, 2018 09:55*

That cable goes to the stepper motor and limit switch, not the front panel.


---
**tony turner** *April 08, 2018 10:56*

That's a worry as mine had the 4 pin plugs on the stepper motors that went into the  2 headers on the bottom and the limit switches went into a 5 pin header on the left. I'll have to find the control panel, maybe it did not have a ribbon cable, I just can't remember.

Thank you to those that have answered my question.

Maybe mine was wired up differently for some unknown reason.

![images/a4324f7ceb0e67360f53b8b9f1eeeb83.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a4324f7ceb0e67360f53b8b9f1eeeb83.jpeg)


---
**Anthony Bolgar** *April 08, 2018 11:05*

The mini can also be used with your configuration, the ribbon cable is one variation, the setup you have is also supported by the mini board (It has connectors for both variations.)




---
**tony turner** *April 08, 2018 11:26*

**+Anthony Bolgar** 

Thank you for that information, I was very confused by it all, I have placed an order for the mini to replace the current set-up. I have already purchased Lightburn and want to get the full use out of it as the UNO is a slug.


---
*Imported from [Google+](https://plus.google.com/110291018143272470366/posts/7XNyNKFTCpt) &mdash; content and formatting may not be reliable*
