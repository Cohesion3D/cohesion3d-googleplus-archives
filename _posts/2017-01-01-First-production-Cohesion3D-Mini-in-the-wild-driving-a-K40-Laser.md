---
layout: post
title: "First production Cohesion3D Mini in the wild driving a K40 Laser"
date: January 01, 2017 02:57
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
First production Cohesion3D Mini in the wild driving a K40 Laser.   **+Joe Spanier** did a multi-hour stream last night of aligning his K40, installing the new Mini, and doing a few test cuts.  Full video is here: [https://www.facebook.com/rclpeoria/videos/1822193224725745/](https://www.facebook.com/rclpeoria/videos/1822193224725745/)



Thanks Joe and Happy New Year Everyone!





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *January 01, 2017 06:10*

Happy New Years everyone!


---
**Ryan Branch** *January 02, 2017 18:49*

**+Ray Kholodovsky** Hey, here are the files for the Control Panel and air assist used on our K40 at River City Labs. [https://github.com/haqnmaq/K40-Laser-Cutter](https://github.com/haqnmaq/K40-Laser-Cutter)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/DfydJfLA7Vk) &mdash; content and formatting may not be reliable*
