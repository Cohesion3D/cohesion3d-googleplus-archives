---
layout: post
title: "Hello everyone, I received my Cohesion3d Mini board today and promptly installed it in my K40"
date: September 10, 2018 06:05
category: "C3D Mini Support"
author: "Matthew Kelley"
---
Hello everyone, I received my Cohesion3d Mini board today and promptly installed it in my K40. I have an extensive electronics background and there was nothing difficult here. I did read the installation notes before installing and they mentioned the PCB should be mounted in a way that isolates it from the chassis ground. I 3d printed a bracket and installed it. I was able to use it all day today while I was getting a feel for the Lightburn software. I wasn't having any trouble.  Then towards the end of the day, just before I went to start a job smoke spewed out of the board and I immediately cut power.



After the smoke cleared I unhooked the Cohesion3d board and checked the K40 power supply. Power supply is fine. I removed the C3D board and looked at every component and found no visible damage, no smoke residue, nothing. It smelled burnt near the AMS1117 (3.3v voltage regulator). I used a current limiting bench power supply and slowly increased the current while watching the board with a FLIR thermal camera and the regulator reached 130F within 10 seconds. I am assuming that is the problem. 



I have another AMS1117 chip, and I could probably replace it, but I wanted to see what all of you thought about this problem first. The Cohesion3D site claims that all sales are final, so I am assuming there is no warranty to speak of.



![images/31b6688d4a9000fefe8a051cef527997.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/31b6688d4a9000fefe8a051cef527997.jpeg)
![images/cdaf4b424a2b27da7c5a2aa769d198f7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cdaf4b424a2b27da7c5a2aa769d198f7.jpeg)

**"Matthew Kelley"**

---
---
**Ciaran Whelan** *September 10, 2018 06:37*

Hmm.. this is a worry. I now have doubts to purchase one seeing that they don't support warranty if there is an issue with a faulty board. 


---
**Matthew Kelley** *September 10, 2018 06:54*

**+Ciaran Whelan**, I haven't contacted them directly yet. I figured I would see what was recommended by the community first.


---
**Ray Kholodovsky (Cohesion3D)** *September 10, 2018 12:27*

Mind doing a continuity check for me, between 3.3v rail (on the serial header) and Gnd? 


---
**Matthew Kelley** *September 10, 2018 15:53*

**+Ray Kholodovsky**, Sure. I'm getting 1.2 Ohms between 3.3v and Gnd which is pretty much a short. I would've expected around 1.5K here. I've also put the original board back into the laser and verified that everything else still works.


---
**Ray Kholodovsky (Cohesion3D)** *September 10, 2018 15:57*

Happen to have any pics of how the board was installed/ wired?



At the least, can you tell me if you were using a separate PSU to power the board?


---
**Matthew Kelley** *September 10, 2018 16:34*

![images/7f6c1432849c2eb3364d30274ba49146.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7f6c1432849c2eb3364d30274ba49146.jpeg)


---
**Matthew Kelley** *September 10, 2018 16:34*

![images/ab81f28a86ba669edeeb999cff5c72bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ab81f28a86ba669edeeb999cff5c72bd.jpeg)


---
**Matthew Kelley** *September 10, 2018 16:39*

**+Ray Kholodovsky** I had tested the power supply for stability and it was stable even under load, but I was planning to do the upgrade to the seperate power supply today anyways.


---
**Ray Kholodovsky (Cohesion3D)** *September 12, 2018 01:51*

Thanks for the info, I’ll email you back shortly. 


---
**Matthew Kelley** *September 12, 2018 03:46*

Okay, thanks **+Ray Kholodovsky**!


---
**Matthew Kelley** *September 12, 2018 09:17*

I've been thinking that it may have been a capacitor that vented and failed in a  shorted state. Perhaps the 16v cap above the 3.3v regulator? If the capacitor failed, it could cause the regulator to heat up and finally burn itself out. When I first tested the regulator at the time I took the thermal images, it was still putting out 3.3v for a short time and the red LED still lights.   I'm just thinking aloud, I don't know if it helps any. I'm quite curious as to what happened myself. 


---
*Imported from [Google+](https://plus.google.com/+MatthewKelley/posts/799CfxxWcti) &mdash; content and formatting may not be reliable*
