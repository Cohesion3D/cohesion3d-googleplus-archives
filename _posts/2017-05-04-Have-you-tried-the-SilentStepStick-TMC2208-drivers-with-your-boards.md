---
layout: post
title: "Have you tried the SilentStepStick TMC2208 drivers with your boards?"
date: May 04, 2017 13:57
category: "3D Printers"
author: "Sebastian Wicksell"
---
Have you tried the SilentStepStick TMC2208 drivers with your boards? I'm building a super quiet corexy printer, single extruder, would the mini work or do you recommend the ReMix?





**"Sebastian Wicksell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 14:22*

Yes, my main machine runs these and Remix and I am very happy. 



It depends on what features you need like bed heating capabilities (Remix has higher current mosfet), additional io (Remix supports more peripherals), whether you want glcd direct support (Mini requires an adapter)... list goes on. In short prices add up with the Mini while ReMix has everything on it and leaves room to expand. Maybe you'll want multiple extruders in the future too. With these new Y splitters it's never been easier to go dual - and you only need the one hotend still. 


---
**Sebastian Wicksell** *May 04, 2017 14:37*

Thanks for the quick answer! Yeah I will probably go with the remix then :) Are you useing the 2208 drivers in 1/256 native or 1/16 stealthChop?


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 14:39*

What I like about the 2208's is that the default stealthchop2 actually works with no torque loss. This means they plug right in using default settings. This means that the Remix is sending steps at 1/16 MS and the driver is interpolating to 1/256 internally. That's how this usually works. But you don't have to reprogram the driver. 


---
**Sebastian Wicksell** *May 04, 2017 14:49*

Oh that is sweet :) Thanks for the info!


---
**Sebastian Wicksell** *May 04, 2017 15:57*

Thanks for your comment **+Alex Skoruppa**! 


---
*Imported from [Google+](https://plus.google.com/101362808799497364823/posts/j7WLUakqyzF) &mdash; content and formatting may not be reliable*
