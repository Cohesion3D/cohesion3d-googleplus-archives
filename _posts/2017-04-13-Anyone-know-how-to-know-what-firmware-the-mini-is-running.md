---
layout: post
title: "Anyone know how to know what firmware the mini is running?"
date: April 13, 2017 23:28
category: "FirmWare and Config."
author: "ALFAHOBBIES"
---
Anyone know how to know what firmware the mini is running? Here is a picture of my LCD on startup.



![images/82b700be4b33ac223b3e44e58876d03c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82b700be4b33ac223b3e44e58876d03c.jpeg)



**"ALFAHOBBIES"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 23:31*

It's Smoothie.

The next screen, once the startup clears, can tell us whether it's Regular or CNC version.  

If you bought the Mini from me as part of the Laser Bundle you've got Smoothie CNC firmware. 

The edge- numbers is the build number and below it would be the compile date. 


---
**ALFAHOBBIES** *April 13, 2017 23:35*

This is my next screen. Would there be new firmware for mine?

![images/fcea918c31d43396665c9cd7838c1509.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fcea918c31d43396665c9cd7838c1509.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 23:41*

Yes, that is the CNC firmware.  



Yes, there tends to be new firmware from Smoothie quite often.  



It is here: [https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin)



Do #1 and 2 here for the CNC Firmware) and reset the board. 

[lh3.googleusercontent.com](https://lh3.googleusercontent.com/-v3WdHYJz9Ao/WPAAArRugeI/AAAAAAAAejw/pp7oOiboaPsk0eZ8LZTzRcOjkUV46b9ggCL0B/h1334/2017-04-13.jpg)


---
**ALFAHOBBIES** *April 13, 2017 23:45*

Excellent! Thanks for the help. I'll read up and see if I should update the firmware.


---
*Imported from [Google+](https://plus.google.com/118265639968468013312/posts/YJPA6w3FDHb) &mdash; content and formatting may not be reliable*
