---
layout: post
title: "Hello, I have installed the c3d into a k40 that had a moshiboard"
date: September 26, 2018 08:21
category: "C3D Mini Support"
author: "Mark W"
---
Hello, I have installed the c3d into a k40 that had a moshiboard. On the 6 pin power connector i have connected the 24v and ground, and the laser wire to the p2.5 bed. Do I need to connect the laser ground wire to the c3d mini?. In the pics on the installation instructions, it doesnt show it connected, but it seems weird not to connect it.



Thanks in advance.





**"Mark W"**

---
---
**Mark W** *September 26, 2018 22:07*

I ask this because at the moment i have an issue with the lightburn software not firing the laser when running any job and thought the ground wire not connected might be the problem. 


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2018 22:21*

Yeah, I'd start there.  The ground is a reference for L to fire the laser, it should also be connected to C3D ground, the same place as the power gnd.


---
**Mark W** *September 26, 2018 22:38*

Ok thank you i will try that when i get home and post back.


---
**Mark W** *September 27, 2018 10:01*

Hi, connecting the laser ground to the ground on the c3d ground makes the laser fire. Thanks for that. This might be worth adding into the instructions for installing the c3d mini - it might be an obvious connection for people in the know, but for people that aren't, this might cause confusion, problems installing and at worst, damage to their machine.


---
**Rich** *October 06, 2018 17:12*

Mark, I just read this as I'm having the same issue. Thanks for sharing your results; the C3D documentation creator really needs to get this listed.  


---
**Mark W** *October 06, 2018 21:45*

Glad to see this post has helped :)


---
**adstaggs** *October 13, 2018 13:56*

I am having the same issue but after burning up my last C3D board I am reluctant to connect any wires so if you could show me how to correctly connect the ground wire from k40 to C3D board I would really appreciate it 


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 14:26*

I would simply recommend following the guides at Cohesion3D.com/start - they should show you exactly what to do. [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**adstaggs** *October 13, 2018 14:51*

I am just not seeing how to do this?

Yeah, I'd start there.  The ground is a reference for L to fire the laser, it should also be connected to C3D ground, the same place as the power gnd.


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 15:29*

Keep the k40 power plug. Cut the 24v wire. Follow the guide.

Which step is different for you than what the psu guide shows? 


---
**adstaggs** *October 13, 2018 16:12*

As far as I can see everything connected but still can not fire laser either from Lightburn or Laser Test button on top panel but it fire with the test button on the k40 power supply, I cut the 24v line and put in an external 24v ps from Cohesion3d 

![images/8b011b6d2fd203756ee755ed7ed9f6e8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8b011b6d2fd203756ee755ed7ed9f6e8.jpeg)


---
**adstaggs** *October 13, 2018 16:12*

![images/4539904fb1b2b33d24467dcb36cc5f30.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4539904fb1b2b33d24467dcb36cc5f30.jpeg)


---
**adstaggs** *October 13, 2018 16:12*

![images/ea44c87af0719b9cdadf6c7978d6e554.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ea44c87af0719b9cdadf6c7978d6e554.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 17:11*

Unrelated question: did you isolate the board from the frame of the laser? I see metal screws. 




---
**Mark W** *October 13, 2018 21:15*

Hi adstaggs, which stock k40 board did you have previously? I had a moshiboard with a 6 pin connector and had to cut all power connections. 

In the start guide where it says about 6 pin connector, it shows a diagram for it. I followed that and had the one wire left which was the laser fire ground wire. That wire goes into the 24v ground. The same 24v ground where your external psu goes. This fixed my problem. I will post a picture later for you.


---
**Mark W** *October 13, 2018 22:34*

![images/f9657125046ffc3c3837e60701f6936c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f9657125046ffc3c3837e60701f6936c.png)


---
**adstaggs** *October 14, 2018 01:46*

I have a m2NANO what is the wiring to cohesion3d board.

![images/59576dc4a5a9f48c5b91c24aa9c6d51f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/59576dc4a5a9f48c5b91c24aa9c6d51f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 14, 2018 01:49*

It is, once again, in the instructions. Tell me exactly which part differs from your setup. 


---
**adstaggs** *October 14, 2018 02:06*

Mark W suggested in another post how to wire you C3D to fire the laser (see photo) but he has a mosiboard and I have a m2nano, so I was wondering how the m2nano wiring would look like.

![images/85d89bcdc1dea467eb501e3dcf9963a1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/85d89bcdc1dea467eb501e3dcf9963a1.jpeg)


---
**Mark W** *October 14, 2018 02:15*

The moshi and m2nano are quite different. With the m2nano, you shouldn't have to cut any wire. Should all be plug and play. 


---
**adstaggs** *October 14, 2018 02:40*

so i guess I am confused, so if i connect a 24v external ps to the C3D board I can still connect all the connectors from the nano to C3D without cutting any 24v from the k40 power supply?


---
**Ray Kholodovsky (Cohesion3D)** *October 14, 2018 04:27*

You cut the one 24v wire in the k40 power plug, as shown in the last step/ pic of the psu guide. The remaining wires stay in the plug. The laser should fire as one of those is the ground going from the C3D to the laser, and another one is the L to fire the laser.  You just cut the 24v wire. 


---
**adstaggs** *October 14, 2018 13:01*

this is the wire I cut , the laser will fire now when I hit the Laser Test button but will not fire when running Lightburn, the machine going through the motion fine but the laser will not fire.

![images/e2208a1b5eddf191dd45e953af24d71d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e2208a1b5eddf191dd45e953af24d71d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 14, 2018 15:01*

Make sure that the L and Gnd wires have not gotten loose in that plug. Sometimes they do. 


---
*Imported from [Google+](https://plus.google.com/109729420081686432611/posts/5hLbMmA4dBj) &mdash; content and formatting may not be reliable*
