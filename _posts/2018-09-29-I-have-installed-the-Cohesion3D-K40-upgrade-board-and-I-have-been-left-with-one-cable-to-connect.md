---
layout: post
title: "I have installed the Cohesion3D K40 upgrade board and I have been left with one cable to connect!"
date: September 29, 2018 15:56
category: "K40 and other Lasers"
author: "Tobe K\u00e4llner"
---
I have installed the Cohesion3D K40 upgrade board and I have been left with one cable to connect! My K40 was originally converted with Ramp 1.4 board which had me to cut and replace the main power connector, 24 v power is now connected to the proper power plint as the Laser Fire (L) wire but where should the Laser PWM cable be connected? The Cohesion3D modified runs as intended but I can't regulate/control the laser power....

Would appreciate some help here.



I have tried to download/open the schematics for the board but it turns out on black background making it impossible to read the text.





**"Tobe K\u00e4llner"**

---
---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 17:59*

[lasergods.com - Cohesion3D Mini Pinout Diagram](https://www.lasergods.com/cohesion3d-mini-pinout-diagram/)


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 18:00*

can you post detailed pictures of your board and its connections as well as the power supply connections and the control panel wiring please


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 18:03*

the pinout diagram shows black in chrome and some other browsers because those browsers display the alpha channel in transparent png files. save the image and open in another viewer or convert to jpg. the link above has been converted already and there is a download link for it at the bottom of the image




---
**Tobe Källner** *September 29, 2018 18:13*

![images/83f3e3f3821350a48f1180d1aa0a10c7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/83f3e3f3821350a48f1180d1aa0a10c7.jpeg)


---
**Tobe Källner** *September 29, 2018 18:14*

![images/14907c9a1c1e24395741437aebf36c22.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/14907c9a1c1e24395741437aebf36c22.jpeg)


---
**Tobe Källner** *September 29, 2018 18:15*

The black wire is Laser Fire and the red is Laser PWM


---
**Tobe Källner** *September 29, 2018 18:34*

Black and blue wire to the plint are 24 volt in from power supply 


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 19:06*

do you have the analog or digital stock control panel?


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 19:22*

![images/c303ece995cc69022043ae98f7448533.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c303ece995cc69022043ae98f7448533.jpeg)


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 19:25*

![images/8f9d91be3e4e6c633f0a33e8aead4cf8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8f9d91be3e4e6c633f0a33e8aead4cf8.jpeg)


---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 19:27*

if you dont have the stock control panel wired to your power supply then you won't be able to set max output reliably. for instance, i have an analog control panel so i test fire the laser, set it to 15mA, and forget it. then the c3D controls it from there.


---
**Tobe Källner** *September 30, 2018 09:48*

First of all my appreciation for you concerne and help.

I have an old rewired  analog panel and I'm able to set it to 15 mA via the pot and test fire both thru the the panel and via the lcd panel/c3D but I'm not able to change the power of the laser via c3D.


---
**Tobe Källner** *September 30, 2018 10:39*

Would I I need to make a connection to the "K40 PWM Output" center pin P2.4 also?


---
*Imported from [Google+](https://plus.google.com/103553230105350011133/posts/MEQnv88vtnA) &mdash; content and formatting may not be reliable*
