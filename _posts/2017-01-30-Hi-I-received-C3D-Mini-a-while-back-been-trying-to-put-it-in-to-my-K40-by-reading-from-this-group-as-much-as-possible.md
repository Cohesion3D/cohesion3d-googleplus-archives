---
layout: post
title: "Hi, I received C3D Mini a while back, been trying to put it in to my K40 by reading from this group as much as possible"
date: January 30, 2017 16:19
category: "General Discussion"
author: "Pongsuta Vachiramon"
---
Hi, I received C3D Mini a while back, been trying to put it in to my K40 by reading from this group as much as possible. 

Right now Im at my dead end can someone help me check my setup? After I turn the machine on the buzzer ring non-stop what should I do? 



![images/07dc8de5f5977d8bf31dce4edc1f9dac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07dc8de5f5977d8bf31dce4edc1f9dac.jpeg)
![images/36de508b9e8051afc1b9a76f6a4788bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/36de508b9e8051afc1b9a76f6a4788bd.jpeg)
![images/8e41075a82f8094e776cd9112cf5a441.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8e41075a82f8094e776cd9112cf5a441.jpeg)
![images/1ca7570633344218148897d89e2cdfab.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1ca7570633344218148897d89e2cdfab.jpeg)
![images/86ce6c92ca49c23acb814c0e672ef8f9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/86ce6c92ca49c23acb814c0e672ef8f9.jpeg)

**"Pongsuta Vachiramon"**

---
---
**Anthony Bolgar** *January 30, 2017 16:25*

Please post pictures of your wiring, and as many details as possible, also please post your config file. It will make it easier for us to help you.




---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 16:29*

It is normal for the buzzer to ring for several seconds while the board boots. However it should then be off. As Anthony said, we will need more information. What are the LEDs on the board doing? Can you plug into USB on your computer? Is the board recognized? (It sounds like you are just turning on the laser for now, for computer you need to install smoothie drivers if you are running Win 7 or 8, but not for Win 10). 


---
**Pongsuta Vachiramon** *January 30, 2017 16:45*

**+Ray Kholodovsky** **+Anthony Bolgar** sorry for the trouble I buzzer freaked me off, after I let it buzz for awhile everything seem to be working now, I haven't try firing the laser yet. I have another question, if I'm not going to use the power control(The blue panel in my picture) I can leave the k+ k- on the PSU unplug right?


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 16:47*

So with the L wire being pulsed, you need to keep the power control hooked up. It will set the maximum power and the power level in LaserWeb will be a proportion of that. So remove the "PWM wire" going to the mini and hook the original wiring back up there. 


---
**Pongsuta Vachiramon** *January 30, 2017 17:01*

This is my current wiring, the power control panel is actually broken is there a way to make it funtional without the power control panel?

![images/2d0c6a18c7c376891e3a4a0b7a4d0206.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d0c6a18c7c376891e3a4a0b7a4d0206.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 17:04*

Ok, in that case we will keep everything as is.  Just change the switch.laserfire from 2.6 to 2.5 in config. But leave the laser pin as 2.4!


---
**tyler hallberg** *January 31, 2017 03:27*

I have the same board as you. The only thing I got to work to make it 100% functional without the stock power managment was keeping the 4 pin power wire where its at, the 3 wire white plug going from the board, remove the center wire from the PSU end and put it on the right side pin of the 2 pin socket directly to the left of the 3 pin socket. I will post pictures shortly.


---
**tyler hallberg** *January 31, 2017 03:37*

![images/6cb63b53b6c359fac06eaaca32270ece.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6cb63b53b6c359fac06eaaca32270ece.jpeg)


---
**tyler hallberg** *January 31, 2017 03:37*

![images/59638d3eb70647c4bfb2551973647a4e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/59638d3eb70647c4bfb2551973647a4e.jpeg)


---
**tyler hallberg** *January 31, 2017 03:37*

![images/e183867aab57969a70cae9c6bd911169.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e183867aab57969a70cae9c6bd911169.jpeg)


---
**tyler hallberg** *January 31, 2017 03:38*

My new control panel that will be getting re done here shortly now that I have laserweb all figured out

![images/1f34a33bd65bec1c347b98ff384ce4da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f34a33bd65bec1c347b98ff384ce4da.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 03:39*

Me likey!


---
**tyler hallberg** *January 31, 2017 03:39*

I have 100% control of laserpower via laserweb as well as via the screen in laser setting s


---
**tyler hallberg** *January 31, 2017 03:41*

Ray let me know if you need me to pull out my camera stuff and get some hi res photos of everything since it seems I may have been one of the first to get the system fully working with this specific machine. Would make life easier if they just had one setup instead of the what seems to be 10+ setups from the manufacturer 


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 03:42*

Much appreciated.  How's that X stagger issue doing?


---
**tyler hallberg** *January 31, 2017 03:45*

Seems to be hit and miss still. I think it's to do with laserweb, seems when I upload a grayscale hi res photo it does it, when I upload a bitmap photo I drop the res on its a lot less. Possibly to much information whether it be in laserweb computing the movements or the board getting to much info in one g code file is where I'm at. 


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 03:46*

In the interest of fully ruling out any possibilities - get a set of A4988's of Amazon and swap them out... Just to be sure...


---
**tyler hallberg** *January 31, 2017 03:48*

Just ordered a pack of 5, will be here Thursday, will report back


---
**Pongsuta Vachiramon** *January 31, 2017 11:40*

**+tyler hallberg** do I need any changes to the config file?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 16:17*

**+Pongsuta Vachiramon** that depends - are you keeping the PWM wire in?  If so, you need to change the switch.laserfire pin to 2.5 (from 2.6)



If you are going to put the panel back into play then you need to change the laser pin from 2.4! to 2.5

And please make sure the same pin is not defined in both places. 


---
**tyler hallberg** *February 01, 2017 06:48*

[https://drive.google.com/open?id=0B1OukruAjLauY0tRd1JUMGVJcnc](https://drive.google.com/open?id=0B1OukruAjLauY0tRd1JUMGVJcnc)


---
**tyler hallberg** *February 01, 2017 06:49*

Link to my config file, this has everything working on my machine with the way I have pictured it hooked up


---
**tyler hallberg** *February 08, 2017 02:22*

**+Ray Kholodovsky** Got the A4988s today, just swapped them, same problem with it shifting at random spots during pictures


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 02:24*

Ok, that rules out faulty electronics. Back to the drawing board...


---
*Imported from [Google+](https://plus.google.com/106925256323364269644/posts/Q84csjSEbDC) &mdash; content and formatting may not be reliable*
