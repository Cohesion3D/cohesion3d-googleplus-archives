---
layout: post
title: "Hey All! Just joined the group. Thinking of going with the Cohesion3D Remix board to control my new CoreXY project!"
date: January 03, 2017 21:40
category: "General Discussion"
author: "Casey Griffin"
---
Hey All! Just joined the group. Thinking of going with the Cohesion3D Remix board to control my new CoreXY project! (pics attached, children for scale)



400mm^3 build volume, dual e3d Titan Extruders into e3dv6 hotends.



Still designing / redesigning the hotend carriage, but hopefully have it moving, by the end of this week.. maybe even printing poorly! :D



![images/9ba8083332d22e634daaa4e30a9adf8e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9ba8083332d22e634daaa4e30a9adf8e.jpeg)
![images/80d01e058ffbcf8b812d58d51fee1cb0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/80d01e058ffbcf8b812d58d51fee1cb0.jpeg)

**"Casey Griffin"**

---
---
**Casey Griffin** *January 03, 2017 23:18*

Had a couple of questions for you guys...



Can I use the C outputs to control a laser cutter/engraver ?



Also, smoothieware provides the ability to create and control switch modules. Does the Remix have the hardware capability to use that? I noticed it's got two aux outputs (and an input), would that be done via those pins? If so, does anyone know what pin numbers they are?



Thanks!


---
**Samer Najia** *January 04, 2017 03:54*

**+Ray Kholodovsky**


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 04:02*

The C sockets are shared with the status LEDs so you could use that instead. 



You've pretty much nailed that the available pins are the C socket once the LEDs are disabled, the EXP1 and EXP2 has some pins, and Servo 2.4 in the lower right, (as well as any other unused stepper sockets). 

There's also the FET6, this also requires the status led's disabled, and that pin is level shifted to 5v. That's the way we run the laser pwm on the mini. Other ways to do this as well... You'll need to elaborate on what you want to do there though...

Go to the site, I added a documentation link and you can find the pinout diagram there. 


---
**Casey Griffin** *January 04, 2017 04:17*

I don't have any immediate plans on the laser engraver yet, but would like to be able to swap one on later down the road for messing around with. 



**+Ray Kholodovsky**​ do you, off the top of your head know the pull down resistor value on the z endstop ? Planning on using an ir sensor for bed leveling, and the 1k resistor on the smoothie board made it not quite plug and play.. (work arounds seemed easy enough though)


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 04:27*

Samer has done/ is doing a diode laser in a printer, so he's the guy to talk to.  And boy does he have ReMix boards...



As I mentioned to you earlier today, the Cohesion3D Boards use a diode buffer on the endstop circuit.  

The diode is the first link in the chain, it inverts the endstop input, and we have a 10k pull up and a 100nF "pull down" capacitor on that signal. 

So there is not pull up or pull down on the line that you hook your endstop into.  You are welcome to add your own externally, and wire that to any of our ground connections or a 3.3v or 5v one depending on what voltage the sensor wants and whether it wants a pull up or pull down. 



TLDR: diode circuit = no pull up or pull down resistor connected to the sensor/ endstop line, roll your own external resistor as needed.


---
**Casey Griffin** *January 04, 2017 04:33*

<3 okay, it helps to have that bit written down haha



I've got a spare induction sensor around here somewhere if the ir one doesn't work right away...


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 04:37*

Agreed, I acknowledge that I have the ability to regurgitate a large amount of highly technical information very rapidly. But then I TLDR it for you :)


---
**Casey Griffin** *January 04, 2017 04:46*

:D My brain was all over the place too (like usual...)



Gotta pitch it to the wife, and should be pulling the trigger here in a bit haha


---
**Casey Griffin** *January 04, 2017 20:00*

**+Ray Kholodovsky** What was the name of that other Stepper Driver you recommended for potentially running dual Z motors?


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 20:03*

You may be talking about the Trinamic SSS2100


---
**Casey Griffin** *January 04, 2017 20:08*

yassir! Thanks~!


---
**Chris Sader** *January 10, 2017 19:55*

**+Casey Griffin** are you planning to do auto bed leveling on that? If so, proximity sensor or otherwise? Trying to figure out the best route for the D-Bot I'm building at the moment.


---
**Casey Griffin** *January 10, 2017 19:58*

+Chris SSader I'll be using the mini ir height sensor !


---
*Imported from [Google+](https://plus.google.com/115338173424593280709/posts/4vhgHqyANG5) &mdash; content and formatting may not be reliable*
