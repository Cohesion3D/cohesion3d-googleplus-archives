---
layout: post
title: "Hey guys, I have read through all the documentation and am running into some issues getting the C3D installed"
date: October 21, 2017 00:31
category: "K40 and other Lasers"
author: "Abe Fouhy"
---
Hey guys, 



I have read through all the documentation and am running into some issues getting the C3D installed. I guess my questions are:



1. There are a lot of connections on the C3d that are duplicates, I have a power below the aux power header that is a +24VDC/GND connector by terminals and then on listed for the K40 that has the laser fire, gnd +24VDC. I am assuming I use the k40 connections.

2. What are the terminal screw mosfet connections for?

3. I have a set of pins for end stops and another series of end stops broken down by the mosfet terminals. Can I just use the one by the k40 power plug? My endstops use 3 wires, 1 brown, 1 blue, 1 black for each endstop. So i have two blue, two black going into my old controller connector and two brown going into the 24vdc side on the plug. What is the pinout on the C3D connector, which pin is 24vdc, gnd, and signal for the endstop connector?

4. How can I expand it to use the ethernet ports?

5. There is a marking for the k40 PWM outuput, how/why should I hook this up?



Thanks in advance all!





**"Abe Fouhy"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 21, 2017 00:34*

For the k40 you only need to install exactly what is shown in the instructions at Cohesion3D.com/start 



As you said, many connections are duplicated for "general use", printers, other machines, etc. 



If your wiring is different than something in the instructions please post pictures of that and I will advise. 



Ethernet header is compatible with a Lan8720 module. I find the Smoothie web interface to be on the slow side and advise sticking with USB, the MicroSD card + GLCD, or putting the LaserWeb server on a raspberry pi. Topics for later... [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Abe Fouhy** *October 21, 2017 00:44*

OK. Cool. That helps a lot **+Ray Kholodovsky** I really appreciate the help. My issue left than is #3, I have 3 pairs of wires per end stop and 5 pins in the k40 end stop pins. The previous owner of the unit put the brown wires to the 24vdc of the power connection and had two pairs of wires (black and blue) going to the end stops. On your unit, which ones go to which pin? I'll need to create a custom plug for these I think.


---
**Ray Kholodovsky (Cohesion3D)** *October 21, 2017 00:45*

I will need to see pictures. 


---
**Abe Fouhy** *October 21, 2017 01:06*

OK. :) Thanks bud!


---
**Abe Fouhy** *October 21, 2017 01:11*

OK. The problem was I am upgrading the entire unit, new tube, new PS, and think I over confused myself. This is what I have. I think I can just take the plug out of the original, plug it in as you have it in the docs, and then wire the two brown wires to the plug behind it, I was just looking for a cleaner solution. :)

![images/9c0e9fdcc06c5bd7fa8c2775a17f1063.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9c0e9fdcc06c5bd7fa8c2775a17f1063.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 21, 2017 01:41*

Another pic of the endstop connector please (black and blue wires by your fingers there). Need a cleaner view of that. 


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/SAh7VJ1ssTP) &mdash; content and formatting may not be reliable*
