---
layout: post
title: "I just installed the new Cohesion3D board in my K40 and it will not move the laser"
date: October 03, 2018 08:26
category: "C3D Mini Support"
author: "John Harris"
---
I just installed the new Cohesion3D board in my K40 and it will not move the laser. Here are pictures of the original controller before the installation and the wiring of the Cohesion3D afterwards. I get some green lights on power up; L1-L4, 3V3 and Play LED. I get one red light; VMOT.





![images/9c7d6f533e4293f33faf5c2a2ea33776.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9c7d6f533e4293f33faf5c2a2ea33776.jpeg)
![images/0546a585a419c2dd034a7b6f22b3ed04.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0546a585a419c2dd034a7b6f22b3ed04.jpeg)
![images/5747b2bcf24b823e423cc8c2382d0662.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5747b2bcf24b823e423cc8c2382d0662.jpeg)

**"John Harris"**

---
---
**Dushyant Ahuja** *October 03, 2018 12:30*

I think your SD card needs to be pushed in fully. 


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 13:26*

Yeah the sd card is inserted incorrectly 


---
**John Harris** *October 03, 2018 15:56*

Are you sure? The third pic down on the following page looks the same as I have:

[cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions. : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 16:08*

i was looking on my phone while driving so... no, im not positive now that i am looking at them full screen. what are the leds doing? this may help: [lasergods.com - Cohesion3D LED Indicator Status](https://www.lasergods.com/cohesion3d-led-indicator-status/)


---
**John Harris** *October 03, 2018 16:56*

I removed and reinserted but with the same results. Could the card be damaged or corrupted?

![images/486abf7b9ed63fd61e66941d9bc5aeed.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/486abf7b9ed63fd61e66941d9bc5aeed.jpeg)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 17:00*

Check the status lights and compare to the linked doc above and let me know the result please 


---
**John Harris** *October 03, 2018 17:10*

It appears to be NORMAL OPERATION status with all the LEDs lit and the L2, L3 flashing.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 17:16*

does the lightburn console show your laser as connected? can you post a screenshot of the lightburn panel?




---
**John Harris** *October 03, 2018 20:14*

Ok I resolved the issues and am now up and running.

Issues that I found and fixed:

  1.Incorrect assumption: I assumed the Cohesion board would auto home the laser on power up - False

  2.Loaded GRBL driver into LighBurn and it failed to move the laser. Then I checked Windows Device Manager to see a Smoothie USB driver loaded --> Loaded Smoothie driver into Lightburn and laser homed in wrong direction - grinding gears...

  3.Reversed the wire orientation on both the X and Y connectors

Successful Test burn completed



Thanks for all of your time and help


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 20:20*

Awesome!


---
**Joe Alexander** *October 05, 2018 06:45*

I also believe that with smoothie you can make a .txt file to put on the sd card that will run the g-code within on power-up. I think that is how you can get your auto-home on startup back if you so choose. I believe the info is on [smoothieware.org - start [Smoothieware]](http://smoothieware.org) 


---
*Imported from [Google+](https://plus.google.com/117587099300180272958/posts/WrvB8d7inA5) &mdash; content and formatting may not be reliable*
