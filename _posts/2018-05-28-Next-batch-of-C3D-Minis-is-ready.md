---
layout: post
title: "Next batch of C3D Minis is ready"
date: May 28, 2018 02:10
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Next batch of C3D Mini’s is ready.  We’re expecting shipment shortly, and to be back in stock and sending out orders next week. 



Thanks for everyone’s patience and ongoing support!





**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/U5HLsfYJeD5) &mdash; content and formatting may not be reliable*
