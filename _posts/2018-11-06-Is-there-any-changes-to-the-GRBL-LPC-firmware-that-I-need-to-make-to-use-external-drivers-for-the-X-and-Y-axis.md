---
layout: post
title: "Is there any changes to the GRBL-LPC firmware that I need to make to use external drivers for the X and Y axis?"
date: November 06, 2018 19:53
category: "C3D Mini Support"
author: "Chad Traywick"
---
Is there any changes to the GRBL-LPC firmware that I need to make to use external drivers for the X and Y axis?



Im not running a table or rotary 





**"Chad Traywick"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 06, 2018 20:17*

Here are our compiles:





[https://www.dropbox.com/s/4zgce8yuekduzpl/firmware_grbl-lpc_c3dmini_4axis_odZ_odA.bin?dl=0](https://www.dropbox.com/s/4zgce8yuekduzpl/firmware_grbl-lpc_c3dmini_4axis_odZ_odA.bin?dl=0)







[https://www.dropbox.com/s/vlxct86ldz6qi40/firmware_grbl-lpc_c3dmini_4axis_odA.bin?dl=0](https://www.dropbox.com/s/vlxct86ldz6qi40/firmware_grbl-lpc_c3dmini_4axis_odA.bin?dl=0)



[https://www.dropbox.com/s/eoagcfrgm03w83s/firmware_grbl-lpc_c3dmini_4axis_odX.bin?dl=0](https://www.dropbox.com/s/eoagcfrgm03w83s/firmware_grbl-lpc_c3dmini_4axis_odX.bin?dl=0)



[https://www.dropbox.com/s/qg6ewcdvclhf7he/firmware_grbl-lpc_c3dmini_4axis_odXYZA.bin?dl=0](https://www.dropbox.com/s/qg6ewcdvclhf7he/firmware_grbl-lpc_c3dmini_4axis_odXYZA.bin?dl=0)



[https://www.dropbox.com/s/88d5xn681m3sijj/firmware_grbl-lpc_c3dmini_4axis_odY.bin?dl=0](https://www.dropbox.com/s/88d5xn681m3sijj/firmware_grbl-lpc_c3dmini_4axis_odY.bin?dl=0)



[https://www.dropbox.com/s/zryx0lc74yjktpi/firmware_grbl-lpc_c3dmini_4axis_odZ.bin?dl=0](https://www.dropbox.com/s/zryx0lc74yjktpi/firmware_grbl-lpc_c3dmini_4axis_odZ.bin?dl=0)







odXYZA will do what you want.  


---
**Chad Traywick** *November 06, 2018 20:48*

Actual total forgot, is there an easy way to change bed size? My bed on my custom is 940mm x 610mm


---
**Ray Kholodovsky (Cohesion3D)** *November 06, 2018 21:22*

[github.com - grbl](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration) 


---
*Imported from [Google+](https://plus.google.com/101895536937391772080/posts/46r6JWFaHxK) &mdash; content and formatting may not be reliable*
