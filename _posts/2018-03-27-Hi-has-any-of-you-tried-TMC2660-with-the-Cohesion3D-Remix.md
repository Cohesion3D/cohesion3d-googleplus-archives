---
layout: post
title: "Hi , has any of you tried TMC2660 with the Cohesion3D Remix ?"
date: March 27, 2018 02:19
category: "General Discussion"
author: "richieharcourt"
---
Hi , has any of you tried TMC2660 with the Cohesion3D Remix ? I am struggling to find a Trinamic 2660 based external driver online. I can probably use much less powerful driver but I like the look of the 2660 for my larger printer and I want the power so I can test very high accelerations. Thanks, Richard 





**"richieharcourt"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 30, 2018 19:53*

Yeah, aside from your CNC shop you told me about with their own designed hardware, this really doesn't exist.  Just get a nice external driver and follow our wiring guide at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) (you'll find the idea in the Z table/ rotary article.).


---
*Imported from [Google+](https://plus.google.com/109222201938249559140/posts/LdAHj9EpoAa) &mdash; content and formatting may not be reliable*
