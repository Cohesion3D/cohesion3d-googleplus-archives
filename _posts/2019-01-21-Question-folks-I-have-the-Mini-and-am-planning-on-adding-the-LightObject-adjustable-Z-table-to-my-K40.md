---
layout: post
title: "Question, folks: I have the Mini and am planning on adding the LightObject adjustable Z-table to my K40"
date: January 21, 2019 05:32
category: "C3D Mini Support"
author: "Richard Perry"
---
Question, folks:



I have the Mini and am planning on adding the LightObject adjustable Z-table to my K40. The stepper in that kit is spec'ed at 2A and I'm going to run it off an external stepper-driver, not via a Pololu on the Mini. With the stock Cohesion/additional PSU (the 4A one), I should be ok to power the additional stepper-driver off that as well as the Cohesion board, right? Or should I be looking at getting a better 24VDC PSU at the same time? 



My thought is that I don't need a bigger PSU mainly because I'm not ever going to move the Z axis at the same time as the X/Y, so all it needs is holding torque/amperage. 



Right? Or am I once again proving why I'm a machinist, not an electrician? :)



Thanks -



Richard Perry





**"Richard Perry"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:03*

If you follow our Z table instructions exactly such that you set the external driver DIP switches to 1 amp, then yes you can split off from the 4a C3D power brick to power both the board and the external driver.  1 amp should be enough for the LO table. 


---
*Imported from [Google+](https://plus.google.com/115442261504955032163/posts/F4dsxkwVe2o) &mdash; content and formatting may not be reliable*
