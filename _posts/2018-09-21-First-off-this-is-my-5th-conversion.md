---
layout: post
title: "First off this is my 5th conversion"
date: September 21, 2018 03:43
category: "K40 and other Lasers"
author: "Tim Francis"
---
First off this is my 5th conversion. I was having a hella time earlier with a c3d upgrade. i connected the board worked fine. Then I  installed on the the old control bracket board. Switched over to lap top and  firmware had problems finally fiqured out. One of many problems with these upgraded can have, ends up milling sucks on case and was making sd card not sit right. shimmed the bracket board all good. I bought this machine 2nd hand and fella said he bought in last few months on ebay. machine had no water safety.  Keep and eye out new k40ers

![images/eb3b8e13d48001d1cfd4d5b794c3c6c9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eb3b8e13d48001d1cfd4d5b794c3c6c9.jpeg)



**"Tim Francis"**

---
---
**Kelly Burns** *September 21, 2018 15:48*

What do you mean it had no water safety?   


---
**Tony Sobczak** *September 21, 2018 16:46*

No water flow monitor. Shuts laser off.


---
**Kelly Burns** *September 21, 2018 16:49*

**+Tony Sobczak** I have never seen a K40 with a stock water flow sensor and interlock.  I have seen temperature gauges on a few, but that doesn’t seem to be common either.  






---
**Kelly Burns** *September 21, 2018 16:50*

Also, if it came with one, it’s bound to be cheap crap and I wouldn’t rely on it.  


---
**Tony Sobczak** *September 22, 2018 18:58*

Mine came with one.  Judging from other posts, so did many others.




---
**Kelly Burns** *September 23, 2018 19:28*

**+Tony Sobczak** I certainly didn't think you were lying ;) I just know that its not a rare thing to NOT have that.  Maybe some of the suppliers started including to make sure customers were hooking up coolant.  Not a bad idea, but I wouldn't rely on them long-term. 






---
*Imported from [Google+](https://plus.google.com/108380611421143689222/posts/atNdeQ2ydJZ) &mdash; content and formatting may not be reliable*
