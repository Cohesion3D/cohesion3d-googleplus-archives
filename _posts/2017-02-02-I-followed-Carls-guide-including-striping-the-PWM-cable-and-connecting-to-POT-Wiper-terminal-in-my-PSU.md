---
layout: post
title: "I followed Carl's guide including striping the PWM cable and connecting to POT Wiper terminal in my PSU"
date: February 02, 2017 06:32
category: "General Discussion"
author: "Alvin Manalastas"
---
I followed Carl's guide including striping the PWM cable and connecting to POT Wiper terminal in my PSU. So should I be able to use the Test laser option in my LCD or would I need to do it thru LaserWeb? I tried doing just that and the laser is not firing.



What are there pros and cons between using Carl's way and the one connecting to P2.5 bed?



Thanks!





**"Alvin Manalastas"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 12:27*

If PWM cable - pot approach - test fire from the GLCD then hit the test fire button on the machine. In this case the GLCD is used to set the power level, as if it was the pot. 


---
*Imported from [Google+](https://plus.google.com/112841546145461029963/posts/dQkXphdpGEf) &mdash; content and formatting may not be reliable*
