---
layout: post
title: "+Ray Kholodovsky Hi Ray I've just fitted the mechanical endstops to my K40 C3D mini but they were not working and I could not send the head home"
date: February 04, 2017 14:22
category: "General Discussion"
author: "Andy Shilling"
---
+Ray Kholodovsky  Hi Ray I've just fitted the mechanical endstops to my K40 C3D mini but they were not working and I could not send the head home. I have updated the LW3 config to G28.2  settings you posted in another answer and now run the M119 code.



 The board returned all 4 endstops reading 1 ( only two actually installed) when not activated and 0 when activated, I know this is the wrong way round but could you tell me how I resolve this please. I am hoping it will just be a case of swapping the wires over.





**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 14:55*

It's not a matter of swapping wires, it's a difference between normally open and normal closed switches. You need to invert them in config by adding a ! to the end of the pin # for endstops. Also if you have 4 endstops then we may need to fiddle with config to make them all work, but later. 


---
**Andy Shilling** *February 04, 2017 15:14*

**+Ray Kholodovsky** I don't have 4 endstops, the board was just responding to my M119 request; I wasn't sure if it should respond to 4 if only 2 are fitted?

I take it you mean like this and because my y  endstop is at the top I should set it as max and not min.?



Alpha min = x min and

 Beta Max = y max



               

![images/50e835a88722eb8d8eb86bfa3dedd72a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/50e835a88722eb8d8eb86bfa3dedd72a.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 15:40*

Alpha min and beta max. You got it. And the config is already set to home to max for y (beta). So the ! is all you need then. Give it a shot with M119 once you save and reset. 


---
**Andy Shilling** *February 04, 2017 15:53*

**+Ray Kholodovsky** Thank you buddy, I'll re-test in a bit, on another note can I access the sd card when connected to LW3 on my Macbook or am i better shutting down the Laser and removing it?


---
**Andy Shilling** *February 04, 2017 16:45*

**+Ray Kholodovsky** 

Sorry Ray X axis is homing fine but Y is still not responding as it should. I might have it connected wrong, do I use Y Min off the board or the Y max taking the +5v from the Y Min pin?

![images/be0b8fb3e5c58a55a4e5f6458328a88a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/be0b8fb3e5c58a55a4e5f6458328a88a.png)


---
**Andy Shilling** *February 04, 2017 17:17*

Dont worry  I figured it out I put the ! in the wrong place.

Thanks




---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/Bcb7VG74WBA) &mdash; content and formatting may not be reliable*
