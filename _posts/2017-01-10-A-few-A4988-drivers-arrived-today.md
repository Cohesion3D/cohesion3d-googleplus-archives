---
layout: post
title: "A few A4988 drivers arrived today"
date: January 10, 2017 05:04
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
A few A4988 drivers arrived today. 

![images/405ece71f20b96981150514a779875ec.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/405ece71f20b96981150514a779875ec.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Alex Krause** *January 10, 2017 05:26*

Will you have any extras?


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 05:38*

Not from what you see there - those should just cover the Mini batch. 

I have more though. And more on the way. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/5mMF1AnUN1N) &mdash; content and formatting may not be reliable*
