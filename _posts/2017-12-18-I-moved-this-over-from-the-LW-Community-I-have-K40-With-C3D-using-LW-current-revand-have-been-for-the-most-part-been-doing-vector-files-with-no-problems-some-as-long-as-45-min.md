---
layout: post
title: "(I moved this over from the LW Community) I have K40 With C3D using LW (current rev)and have been for the most part been doing vector files with no problems some as long as 45 min"
date: December 18, 2017 17:07
category: "FirmWare and Config."
author: "Steve Clark"
---
(I moved this over from the LW Community)

 

I have K40 With C3D using LW (current rev)and have been for the most part been doing vector files with no problems… some as long as 45 min. Now I’m trying to learn raster engraving but keep having comm. or some other issues with the program stopping.



This usually happens after 3 to 7 min of the program running and I’m having to go back and completely restarting after noting the y axis position (Program is stating at the bottom going to the top). I turn the laser back on at the dropped off point. This happens about 70% of the time. I’m running at 4000 mm/min but have tried it also at 3000mm/min and the results are the same.



I’m using a Ferrite shielded USB cable is 2 ft long (I have four cables now) These pictures below show the screen at starting, when it just decides to miscommunicate and also the raster image which took two runs to complete because of stoppage. 



On the procnc site I noted the statement about “Consider installing grbl-LPC on your smoothie compatible board for real fast raster engraving.”



Is this the logical route to go? If so, I have some questions about the instructions and terms used.





![images/be5bd157e15b53841894ed9c9d523bb3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/be5bd157e15b53841894ed9c9d523bb3.jpeg)
![images/53782e7357175b9ec6294dedec22efdd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/53782e7357175b9ec6294dedec22efdd.jpeg)

**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2017 21:11*

There's a bunch of things that could be causing this including:



Brownout of K40 Psu --> Do you have a separate 24v PSU installed to power the board?



Problem with LaserWeb communications --> Smoothie.  

Do you have a GLCD Screen? If yes, save the gcode file, put it on the MicroSD Card in the Mini, and run it via the GLCD Play Menu.

If no, you can try grbl-lpc.



In either case, Lightburn software should be out in 2 weeks and you should give that a shot, it's working very well in testing, with both firmwares Smoothie and grbl-lpc.



There is an article here about it with a firmware build: [cohesion3d.freshdesk.com - K40 Laser Documentation : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/folders/5000283974)




---
**Steve Clark** *December 18, 2017 23:19*

Yes I got the GLCD screen. I'll see it If I can save the gcode to it and run it that way.



BTW - Lightburn looks very interesting. I look forward to it. 



Thanks Ray.






---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/VhoD5t57SAj) &mdash; content and formatting may not be reliable*
