---
layout: post
title: "For those who installed a C3D board in a K40, what VRef values did you use for the X/Y axis settings on the A4988 drivers?"
date: October 30, 2017 21:41
category: "C3D Mini Support"
author: "David Fruehwald"
---
For those who installed a C3D board in a K40, what VRef values did you use for the X/Y axis settings on the A4988 drivers?  





**"David Fruehwald"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2017 21:47*

I don't really go by a voltage, I just go by the position of the "flat" of the potentiometer. By default it should be on the top edge, sticking out. The K40 motors can handle it being increased 45 to 90 degrees. 

The problem comes with the fact that the stock K40 Power Supply is woefully underpowered.  If you increase current, it may not supply enough and start browning out. 



If/ when you upgrade to (add) an external 24v power supply for the board, this is no longer a concern. 



Regarding the vibrations you mentioned earlier, some of that probably has to do with drivers and stepper noise.  If you're looking to remove that, Trinamics are extremely quiet: 

[digikey.com - TMC2208 SILENTSTEPSTICK Trinamic Motion Control GmbH &#x7c; Development Boards, Kits, Programmers &#x7c; DigiKey](https://www.digikey.com/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626)


---
**David Fruehwald** *October 30, 2017 22:09*

I don't think its stepping noise it sounds more like stalling when trying to do small slow moves like approaching the endstops with G28.2 .  On longer 100mm moves it sounds fine.




---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2017 22:17*

Allrighty. 



As a fix, you could, in config.txt, take 	alpha_slow_homing_rate_mm_s and change it to the same [larger] value as it is for the fast homing rate.

Then do the same for beta.

Then save, safely eject your card/ drive in Computer, and reset the board. 




---
**David Fruehwald** *October 31, 2017 01:21*

I found it.  When I had to reverse the direction on the stepper cable I didn't get them in quite right.  The racket was caused by improper stepping.  When I took the whole assembly back out and rechecked everything its working as expected now.




---
**David Fruehwald** *October 31, 2017 01:21*

Thanks for the help.


---
*Imported from [Google+](https://plus.google.com/110578764378886132074/posts/LuCdtQBT3AW) &mdash; content and formatting may not be reliable*
