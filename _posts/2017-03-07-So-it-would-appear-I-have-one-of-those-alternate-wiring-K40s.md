---
layout: post
title: "So it would appear I have one of those alternate wiring K40s"
date: March 07, 2017 15:13
category: "C3D Mini Support"
author: "floyd923"
---
So it would appear I have one of those alternate wiring K40s.  I make this assumption as I only have one set of wires heading to the drivers.  Any recommendations before I attempt to wire up my new C3D Mini?



Also some clarification on what the L wire to 2.5 bed means.  I know the L wire is coming out of the PSU, and it seems to currently be going to the main power plug on the chinese board.  Do I leave that in the plug, or am I supposed to re-route it somehow?  I've seen Ray mention to not use the white cable that came with the C3D board, but when I look at his pictures, it looks like he is using this white cable.  So I am a little confused.



I've have laserweb3 installed on my Mac OS, but I really have no idea what I am doing with it.  Is there a good tutorial on how to use laserweb3 that is noob friendly?



Take a look at my pictures, and thanks for helping out this noob.







![images/1536cba5a3f01b4a4fd87973ab67be0e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1536cba5a3f01b4a4fd87973ab67be0e.jpeg)



**"floyd923"**

---
---
**floyd923** *March 07, 2017 15:13*

Some more pictures just in case.



![images/3090c99896114093a8f5688cc53c435f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3090c99896114093a8f5688cc53c435f.jpeg)


---
**floyd923** *March 07, 2017 15:13*

![images/40199c157c124f7323b029f4d3b04cf1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/40199c157c124f7323b029f4d3b04cf1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 15:22*

This is the ribbon cable variant, quite common. 

Just move all the cables that are plugged into the green board over to the C3D. Don't cut any wires and don't use the included pwm wire. 

Keep simple, and check Steven's thread, he just did the same wiring. 


---
*Imported from [Google+](https://plus.google.com/107810051940552772349/posts/T7YLdUDmJdY) &mdash; content and formatting may not be reliable*
