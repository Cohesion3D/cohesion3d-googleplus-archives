---
layout: post
title: "Hi all, I just installed my Cohesion mini into my k40 yesterday and all went well"
date: June 07, 2017 18:04
category: "Laser Web"
author: "Walter Manning"
---
Hi all, I just installed my Cohesion mini into my k40 yesterday and all went well. 

 I also installed LaserWeb4 on the computer I use to control the laser however when I click on settings the screen stays at whatever screen I was on prior to clicking on settings.  The settings button is highlighted on the left hand side but the screen does not change to settings.  I was able to get the program to work on my laptop but not on the computer I use for my laser.  I'm running Windows 7 Pro 32 bit on my laser computer.  I looked around but couldn't find anyone with the same issue.  I would really appreciate it if anyone had any ideas.  Thanks





**"Walter Manning"**

---
---
**Walter Manning** *June 07, 2017 18:23*

Oh and I uninstalled and reinstalled Laserweb4 several times but still no luck.  Thanks again.


---
**Walter Manning** *June 07, 2017 21:45*

I forgot to mention I have tried 4.0.733-79,  4.0.732-78, and 4.0.731-68 and all have the same result. 


---
**Walter Manning** *June 07, 2017 23:44*

I moved this post over to the LaserWeb group as it is probably a more appropriate question for that group.  Thanks again 


---
*Imported from [Google+](https://plus.google.com/102562602465646433618/posts/9XLPcxe9yBC) &mdash; content and formatting may not be reliable*
