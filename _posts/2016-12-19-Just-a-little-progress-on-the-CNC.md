---
layout: post
title: "Just a little progress on the CNC"
date: December 19, 2016 16:34
category: "General Discussion"
author: "Carl Fisher"
---
Just a little progress on the CNC. I'm working on adapting one of the older style C3D mini boards to replace the TinyG that I'm currently using on my CNC. However since my Shapeoko uses dual Y motors and I'm planning to run a rotary, I had to adapt the Y-axis socket to run two drivers since there weren't enough driver sockets on the board. Normally without the rotary you could just take over the A output and configure it accordingly but I can't let things be that simple can I :)



Here is where I'm at so far. This will allow me to jumper directly from the socket on the mini and I have everything but the stepper outputs slaved between the two. I added a second full set of headers in case I ever wanted to clip the jumpers and run them independently but otherwise I only need one set of inputs to drive both A4988 steppers. 



This also allows me to flip one motor easily since one runs backwards.

![images/6ef0e5e53464a12ab7c216232d059219.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6ef0e5e53464a12ab7c216232d059219.png)



**"Carl Fisher"**

---
---
**Jonathan Davis (Leo Lion)** *December 19, 2016 17:51*

intresting 


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/39Zfkqye9xX) &mdash; content and formatting may not be reliable*
