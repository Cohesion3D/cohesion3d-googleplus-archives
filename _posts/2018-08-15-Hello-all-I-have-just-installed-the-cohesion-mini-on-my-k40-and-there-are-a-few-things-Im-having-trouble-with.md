---
layout: post
title: "Hello all, I have just installed the cohesion mini on my k40 and there are a few things I'm having trouble with"
date: August 15, 2018 13:02
category: "C3D Mini Support"
author: "Andrew Hayden"
---
Hello all,



I have just installed the cohesion mini on my k40 and there are a few things I'm having trouble with.

1) The L1 LED which is supposed to be on is not on at all, although all the other LEDs are on in the way described on the getting started section of Cohesion3d web page

2)After installing Lighburn, and connectingthe laptop to the cohesion mini board with the usb cable, in the Laser window it says that the laser is ready... however, I cannot get the laser to respond when I press the home button or anything else.

3)When first setting up Lightburn I mistakenly chose back left as the origin which I want to change to front left but can't find where to do this



Thank you





**"Andrew Hayden"**

---
---
**Andrew Hayden** *August 15, 2018 13:05*

PS I just figured out how to change the origin to front left


---
**Andrew Hayden** *August 15, 2018 13:06*

I'm running windows 10 by the way, which means I don't have any drivers I need to download right?


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2018 14:17*

Check that the memory card is in the board all the way. Also check that you can put the card into a reader and your computer will see the config and firmware file on it. 

Always have the board off when plugging the card. 


---
**Andrew Hayden** *August 15, 2018 22:11*

Thanks. I didn't put the card in at all. Will try that now


---
**Andrew Hayden** *August 15, 2018 22:14*

All good now. Thanks Ray.


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2018 22:19*

Yes, if you don't follow put the card in the board like the instructions tell you to, then nothing should work. 


---
*Imported from [Google+](https://plus.google.com/116886129894437647553/posts/H1baWnhn3Ew) &mdash; content and formatting may not be reliable*
