---
layout: post
title: "Originally shared by Cohesion3D Check out these keychains Ashley Kirchner made for the Ingress Mission Day event in Nebraska last weekend using his K40 Laser Cutter"
date: March 02, 2018 23:52
category: "Show and Tell"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Cohesion3D</b>



Check out these keychains Ashley Kirchner made for the Ingress Mission Day event in Nebraska last weekend using his K40 Laser Cutter.  Between the variable power control of the Cohesion3D board and powerful features of Lightburn like "Make Grid Array" he was able to raster engrave and cut 35 of these in one job, making over 120 total keychains in just 2 days. 



![images/f93d0cb7274c864ab8f4ac1d9640453c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f93d0cb7274c864ab8f4ac1d9640453c.jpeg)
![images/439b0a8463be4c6709b48df3c8a89c41.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/439b0a8463be4c6709b48df3c8a89c41.jpeg)
![images/cef1bbcc23c99142df8cbc82873aba33.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cef1bbcc23c99142df8cbc82873aba33.jpeg)
![images/0d6ca99dea9f6cf33a12a3461c22f585.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d6ca99dea9f6cf33a12a3461c22f585.png)

**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/bXpUzwoSgEP) &mdash; content and formatting may not be reliable*
