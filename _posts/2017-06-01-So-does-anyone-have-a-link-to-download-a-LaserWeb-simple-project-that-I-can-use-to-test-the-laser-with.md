---
layout: post
title: "So does anyone have a link to download a LaserWeb simple project that I can use to test the laser with?"
date: June 01, 2017 03:44
category: "K40 and other Lasers"
author: "Keith Burns"
---
So does anyone have a link to download a LaserWeb simple project that I can use to test the laser with? Just cutting a circle is fine. The program is very different from what I have become used to using for my CNC. Every SVG file I have causes an error in LW4, but works just fine in Inkscape and in Vectric Aspire.





**"Keith Burns"**

---
---
**Ashley M. Kirchner [Norym]** *June 01, 2017 04:47*

For others reading this, the issue already "resolved" in a different thread.


---
**Joe Alexander** *June 01, 2017 05:00*

pro tip: ignore the log in the bottom right regarding errors. When I find a file that doesn't claim error I'll let you know :P


---
*Imported from [Google+](https://plus.google.com/105762576990882303746/posts/hommLqx5b1N) &mdash; content and formatting may not be reliable*
