---
layout: post
title: "A big thanks and either a six pak of virtual beer or if you live on the proper coast the real deal Ray Kholodovsky You definitely deserve it for putting up with us knuckle heads, I envy your ability to take the high road"
date: March 09, 2017 20:41
category: "Thank you and other tips"
author: "Steven Whitecoff"
---
A big thanks and either a six pak of virtual beer or if you live on the proper coast the real deal **+Ray Kholodovsky**

You definitely deserve it for putting up with us knuckle heads, I envy your ability to take the high road every time.





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 20:51*

You got it! Just help spread the word please. 



And if you're up for it I think Claudio (LW dev team) still needs funds toward his k40, I will cue **+Peter van der Walt** with the donations hat :)


---
**Steven Whitecoff** *March 09, 2017 23:11*

Sebastian now has $60, I've got to feed a 48"x24"  workspace hole so I can make this stuff pay for itself in savings on model kits or I would contribute more. If the Chinese play nice with me I will be in a better position to show appropriate thanks, right now its tough to stay in budget, but I'm making steady progress in separating the wheat from the chaff as it were.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/2Yb9e1NV4Et) &mdash; content and formatting may not be reliable*
