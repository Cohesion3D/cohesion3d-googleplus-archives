---
layout: post
title: "Hello, Are there any write ups out there for the wiring of my k40 laser controller to the cohesion mini board without using the standard plugs?"
date: March 19, 2018 03:58
category: "K40 and other Lasers"
author: "Brian Albers"
---
Hello,



Are there any write ups out there for the wiring of my k40 laser controller to the cohesion mini board without using the standard plugs?  I ripped out my factory wiring for a Mach3 based controller many years ago and don't have any of the old parts.  Trying to look around and find diagrams to wire up all the features but thus far still have some gaps.  24v Power, motor and limit switches are good to go. Just need laser power and trigger control.





**"Brian Albers"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2018 04:00*

The last picture in the install guide shows you where L gets plugged in. 


---
**Brian Albers** *March 19, 2018 04:21*

Can the board control laser power as well?


---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2018 04:23*

Yes. Just plug it in and use it. Don't overthink it. 


---
*Imported from [Google+](https://plus.google.com/109986797182285961346/posts/2gbJeTC2uAP) &mdash; content and formatting may not be reliable*
