---
layout: post
title: "On my K40 I have a very noisy air assist and its a real pain to have on when not needed"
date: January 23, 2018 06:29
category: "C3D Mini Support"
author: "Wild Bill"
---
On my K40 I have a very noisy air assist and its a real pain to have on when not needed. I have been going through all the details of how LightBurn works and noticed that on the cut/scan/image page there is an option to enable air assist. Looking at the generated G-Code they are using M106 and M107 (fan on off). I also noticed that they are not using the S option to set the pwm value. So my question is can I go into the config.txt file and change switch.fan.output_type from PWM to digital and not mess anything up? I have an SSR for switching 120V that I could use to turn the air pump on and off. I am already using one on my 3D printer with a 120V bed heater which draws way more power the the air pump.



Also looking at the board I need to hook up one of the aux power header pins.  The SSR will take an input voltage for 3 - 32 volts so where would be the best place to pick up power to hook up to the Aux Header?   





**"Wild Bill"**

---
---
**Wild Bill** *January 23, 2018 06:43*

Looking at it some more does the aux header for +5 already have an internal connection? Then I could just use the +5 pin on the mosfet connector as is.


---
**Wild Bill** *January 23, 2018 18:11*

Not one to sit idle - I went into config and enabled the switch and made it digital. Wired up the SSR and have it working - so now my air assist pump only comes on when cutting.

So how I did this was take a small extension cord and slice the middle open, cut the white wire and put lugs on it. Then I mounted the SSR on the back of the K40 just above where I removed one of the plug so I can run the control wires out through the hole. In the end it was quite simple and I can work in quiet except for when the laser is actually working.

![images/8fc6794459823a8190edcf75a70f8e65.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8fc6794459823a8190edcf75a70f8e65.jpeg)


---
**Wild Bill** *January 23, 2018 18:23*

Picture of the connection to the mini

![images/b396b0f22320108783bab50077de930b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b396b0f22320108783bab50077de930b.jpeg)


---
**Wild Bill** *January 23, 2018 18:24*

Picture of SSR mounted on K40 and working.

![images/6bcd63091ca5d2a77a73ddaccd0b09a2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6bcd63091ca5d2a77a73ddaccd0b09a2.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 19:57*

Cool.  

Yes, those things sound good and the +5v in both fet 3 and fet4 and the aux headers are all from the onboard 5v regulator.


---
*Imported from [Google+](https://plus.google.com/110558812712628719446/posts/L9ThTZaeV7q) &mdash; content and formatting may not be reliable*
