---
layout: post
title: "Hi, has somebody here an idea, how to activate bed leveling?"
date: March 19, 2018 21:36
category: "FirmWare and Config."
author: "Marc Pentenrieder"
---
Hi, has somebody here an idea, how to activate bed leveling?



<b>Originally shared by Marc Pentenrieder</b>



Hi, I have a question on auto bed leveling in smoothie.

I configured auto bed leveling (cartesian grid leveling) and the leveling works as expected, but the printer doesn't respect the leveling results (I think).

When the nozzle goes to Z0 at x0,y0 everything is fine, but when the nozzle goes to Z0 at x500,y500 (mid bed) it is way to high, so I can move a sheet of paper easily under the nozzle)

Do I have to activate the leveling mesh somehow ?



Here is my start gcode I use:



G28 ; home all axes (Z will be homed to max)

M280 S3.0 ;push probe down

G32 ; Bed auto level (Cartesian Grid Level)

G30 Z12.2 ; Find Z-Min

G1 Z20 ; move z up

M280 S7.0 ; pull probe up

G0 X5 Y5 Z5 ; prepare for print



Do I have to use the following M-Codes ?

M373 Z grid strategy: completes calibration and enables the Z compensation grid 



Do I have to use the following before each G32 if I want to level before each print?

M561 clears the plane and the bed leveling is disabled until G32 is run again 



Thanks for any suggestion







**"Marc Pentenrieder"**

---


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/Wd71eDsQyES) &mdash; content and formatting may not be reliable*
