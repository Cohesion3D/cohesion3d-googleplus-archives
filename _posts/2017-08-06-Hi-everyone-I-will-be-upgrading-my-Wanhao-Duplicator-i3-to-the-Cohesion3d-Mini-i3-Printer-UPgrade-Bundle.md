---
layout: post
title: "Hi everyone, I will be upgrading my Wanhao Duplicator i3 to the Cohesion3d Mini i3 Printer UPgrade Bundle"
date: August 06, 2017 17:32
category: "3D Printers"
author: "David Scull"
---
Hi everyone, I will be upgrading my Wanhao Duplicator i3 to the Cohesion3d Mini i3 Printer UPgrade Bundle.  



I have two Wanhao i3's.  One I purchased off ebay for around $100.  I made a number of upgrades on these machines and I have over 4800 hours on both of them with outstanding results as far as quality prints.  Recently the older model (from ebay) failed due to a melted wire connector on the Melzi board.  It was an issue I was not aware of but is now well documented on the internet. (I work long hours and don't have enough time to stay up with all that's happening in the community.)  Anyway, the Melzi board is fried.



I did a little research and I've decided to upgrade to the C3D mini.  I received my package today and I have a few questions:



1. Where can I find the STL file for the bracket to hold it inside the Wanhao controller box?  I could model one in Fusion360, but if its available, that will save me time.



2. Is anyone else here doing this upgrade.  I see that Ray was working on one in December of 2016.  I'd love to take advantage of everyone's experiences if I can.



Thanks,

Dave





**"David Scull"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 17:41*

Welcome David. 



I don't believe the other gentlemen who did the C3D + Di3 upgrade hang out in this group, but we've got a bunch of other 3D Printing people and laser folk that might weigh in.



I'll be walking you through whatever concerns you may run into, I ask you take as many pictures as possible so that I can later ask for them and compile a guide. 



Here's the adapter plate/ bracket I designed up, I've been advised that I need to pull the board from the edge back more, will wait for your input and then adjust the design accordingly.  Also something about the Melzi screws not being long enough (the printed plate being 1mm thicker or some such, and needing to design in a pocket there).  Start here: [dropbox.com - C3D Mini Di3 Upgrade Plate v1](https://www.dropbox.com/sh/cdnxcu4w8zrjwd1/AAB2jt5fVzCrNfBUuqdS6D8Ga?dl=0)



Cheers!


---
**David Scull** *August 06, 2017 20:55*

Thanks, I'll take pictures and keep notes. Hopefully we can end up with a guide to conversion.  I'll post my questions here.  It might be a while before I get to this - August is going to be very heavy at work. :(


---
**David Scull** *August 12, 2017 02:42*

Ray, if I connect the power supply to the Mosfet Power in, plug in the sd card and connect the graphic smart controller, should the board boot up?  Or do I have to wire everything up to test the board.  I'm not getting anything and my multimeter is seeing 12.3 volts from the power supply. Thanks.




---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 02:47*

You need to hook up to main power in at the top left, then it will do as you said. 


---
**David Scull** *August 12, 2017 13:58*

Figured that out, misread the diagram as the Aux Power.  It's up and running, thanks.  Taking pictures as I go, I've watched the video a few times, it's just hard to see what he's doing.  But I think I'll be fine.  Designed a new bracket with support for the stepper splitter.  Printing that and testing it today. Thanks.




---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 14:05*

Yeah, I have my webcam mounted same as Chris and it's nearly impossible to show the small details like that.  Mostly just shots of the top of our heads :)  Maybe a swing arm is a better choice to get the camera right into position. 



Did I provide the Di3 config file already? 


---
**David Scull** *August 12, 2017 14:18*

I don't know.  Is it on the sd card provided?  Also, what type of wire terminals are you using and will this crimping tool work?



[amazon.com - IWISS SN-2549 Crimping Tools for AWG28-18 (0.08-1.0 mm2) PH2.0/XH2.54/Dupont 2.54/2.8/3.0/3.96/4.8/KF2510/JST Terminal Crimper Plier Ratcheting Wire Connector Crimping Tool - - Amazon.com](https://www.amazon.com/Crimping-AWG28-18-0-08-1-0-Ratcheting-Connector/dp/B01N4L8QMW/ref=sr_1_6?ie=UTF8&qid=1502545243&sr=8-6&keywords=wire+connector+crimping+tool)



Thanks


---
**David Scull** *August 12, 2017 14:21*

There is a ascii config file and a CUR file named FIRMWARE on the sd card.


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 14:24*

Honestly I just use some really fine pliers.  Your tool link has "KF2510" in the description so good on that perspective but I don't know about the quality of it. 



So yes, a card is provided in the bundle, I honestly don't remember if I gave you the default config or the Di3 specific one on there. 



I will follow up with a Dropbox link for Di3 config for you a bit later. 


---
**David Scull** *August 12, 2017 21:36*

Here's the new bracket with the support for the stepper splitter

![images/26b42a13047bf155e3dabd5fa3bedf57.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26b42a13047bf155e3dabd5fa3bedf57.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 21:38*

Great idea!  May I suggest that you model fingers to hold the splitter board from the left and right? I would be more concerned with it slipping off the board than it bending downward, although both can be prevented. 


---
**David Scull** *August 12, 2017 21:39*

Power connected and board in place.

![images/6d5d18d18773c6f69fb80afe1ff18aa7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d5d18d18773c6f69fb80afe1ff18aa7.jpeg)


---
**David Scull** *August 12, 2017 21:42*

Sure... let me see what I can do.




---
**David Scull** *August 12, 2017 21:42*

It makes a big difference having the support underneath the splitter.  Of course the whole thing will hang upside down. :)


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 21:43*

Oh, the story gets better.  That LCD layout suggests I just sent you our laser config (that's how I get the cards preloaded).  I'll be getting you the proper firmware and config file for your setup.   :)


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 21:44*

Don't expect to see a temp reading, heaters working, or the extruder to spin with the current cnc firmware on there.  You should be able to jog XYZ though. 


---
**David Scull** *August 12, 2017 21:45*

I just am short on time right now.  I feel a lot better upgrading than going back to the Melzi - lots of problems showing up now that folks have had some time to run these printers.  I'm confident this is going to work very well.  Thanks for your time and attention.


---
**David Scull** *August 12, 2017 21:48*

Chris mentioned something about providing support to the splitter during the video - I didn't know what he was talking about then, but it does make things more stable - I'll look at adding side to side support once I see how the cables sit.  I need to leave some tolerance too.


---
**David Scull** *August 12, 2017 22:04*

Understood. Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2017 13:23*

New SD Card files as promised:



[dropbox.com - C3D Mini Di3 SD Config 1](https://www.dropbox.com/sh/ie0rmw4vpoeq88u/AADPb5U_6SrAMQ0TdEPAy2-Na?dl=0)



remove the existing files on the card, copy these on, safely eject/ dismount, and reset the board.


---
**David Scull** *August 13, 2017 13:42*

Thanks


---
**David Scull** *August 14, 2017 19:26*

Ray, the Melzi board end stops only have two wires.  The C3D has three connections, +, ground, and signal.  Is the signal on the melzi a ground as well? 




---
**David Scull** *August 14, 2017 19:32*

Nevermind - just read that I don't use the +5 volt pins




---
**Ray Kholodovsky (Cohesion3D)** *August 14, 2017 21:41*

Endstops to Sig and Gnd, correct. 


---
**David Scull** *August 15, 2017 16:09*

Hi Ray, so I'm going to have to put new terminations on the endstops. Does it matter which wire goes to what pin?  I want to put the female connectors on correctly.




---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 20:24*

Simple switch. Sig and Gnd. Order doesn't matter. 



We've found that you can bend the wall of the Mini connectors back and put the di3's jst on. Maybe a bit of hot glue at the end to keep them from slipping off. 



Please take some pics so I can understand the conflict your are getting and adjust the crimps provided in the bundle accordingly. 


---
**David Scull** *August 15, 2017 23:05*

Thanks. I was uncomfortable about bending them. I have plenty of connectors and I like how they snap in place. Kind of OCD. :) I was going to ask you about hot glue. 


---
**David Scull** *August 15, 2017 23:06*

I want this to not vibrate apart over time.


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2017 00:00*

Yeah, the upside down aspect (if you're putting the electronics box back together when done) certainly helps connectors fall out as well. 



Look, I love the locking terminals, but I don't expect everyone to be up for re-crimping them. So I accept hot glue as the cost of doing business :) 


---
**David Scull** *August 16, 2017 23:27*

So I added a hole through the splitter support and I'm using a wire tie to hold things steady.  This gives us the horizontal and vertical stability.  

![images/284ebe7778bd34e4fd5e1cfb8c40f7de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/284ebe7778bd34e4fd5e1cfb8c40f7de.jpeg)


---
**David Scull** *August 16, 2017 23:28*

As far as extra crimping vs. hot glue we can provide the options in the final guide. :)




---
**David Scull** *August 16, 2017 23:28*

I'm hoping to get some time this weekend.


---
**David Scull** *August 18, 2017 21:47*

Hi Ray, sorry to bother - I can't figure out where the hotend fan goes. Must of missed it in the video.  Thanks.




---
**David Scull** *August 18, 2017 21:48*

The fan you can adjust during the print.  Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 18, 2017 23:14*

Contradicting phrasing.



The 2 FETs in the top right of the board are for fans.  One is 2.6 and the other is 2.4.  



Heatsink fan is the one that turns on above nozzle 50C to cool the heatsink of the hotend.

Part cooling fan is the one that blows on the printing part.  This is the adjustable one. 



You'll need to reference the config file to see which is which. Let me know if you need me to dive in to advise. 



And you'll be hooking the fans up to HV/ +12v (left pin) (red wire) and Controlled FET pin - (center pin) (black wire)


---
**David Scull** *August 18, 2017 23:15*

Thanks


---
**David Scull** *August 18, 2017 23:17*

I wanted to get it wired up before bothering you.


---
**Ray Kholodovsky (Cohesion3D)** *August 18, 2017 23:18*

We're good.  I'm back to having constant internet access now, so at least that's a plus :)


---
**David Scull** *August 18, 2017 23:55*

Is there a place in the software to control the fan speed?  Trying to test.  Is there some document I should read before I start asking you a million questions? :)


---
**Ray Kholodovsky (Cohesion3D)** *August 19, 2017 00:07*

It's all here really: [smoothieware.org - start [Smoothieware]](http://smoothieware.org/)



Fan control is under switch: 

[http://smoothieware.org/switch](http://smoothieware.org/switch)



Ok, so fan is 2.6.  That's your part cooling fan. Send M106 S255 to turn it on (S values can differ between 0 - 255 for power control) and M107 to turn it off. 



Heatsink fan, which by default is tapped to the power supply for always on, can be left plugged in there, or if you want to control it and have silent standby, that'll go to 2.4. M42 turns it on and M43 turns it off. 



All I had to do is read the config.txt file, it's almost all spelled out there :)


---
**David Scull** *August 19, 2017 02:54*

Thanks! All wired up.  Motors all working.  I have the same Z vibration that Chris had in the video - so I'll have to adjust the pot.  One problem - I started the preheat sequence - software indicates it's started but no power is coming from the mosfets to either bed or hotend.  Thoughts?


---
**David Scull** *August 19, 2017 02:55*

Oh... endstops are working and the thermisters are reading temperature.




---
**David Scull** *August 19, 2017 02:57*

Nevermind - I have to power them.  Sorry.  That's what the extra power wires are for... Sorry.


---
**David Scull** *August 19, 2017 03:15*

Everything's working.  I just need to make adjustments now.  Thanks.  


---
**David Scull** *August 19, 2017 14:59*

Ray do you know if the procedure for tuning the stepper motors will be the same?  Is the sense resistor value the same on the Smoothie as it is on the Melzi?  Thanks.




---
**Ray Kholodovsky (Cohesion3D)** *August 19, 2017 15:24*

I just turn the pots 90 degrees clockwise so that the flat faces right. 


---
**David Scull** *August 19, 2017 18:05*

Okay turning the pots 90 degrees puts the voltage at about 680 which is good for X and Y - the steppor motors on the Z are not the same.  I've tried every voltage from minimal to max and can't get the vibration out of the Z.  X and Y are .95 amps and Z is 1.2 amps.  Thoughts?



Here's a video on tuning the steppers in the Wanhao:




{% include youtubePlayer.html id="9moNexk8Kg4" %}
[youtube.com - Wanhao Duplicator i3 V2: Stepper Tuning](https://www.youtube.com/watch?v=9moNexk8Kg4)



Thanks


---
**David Scull** *August 19, 2017 22:17*

I think we need more current to drive the z steppers.


---
**David Scull** *August 19, 2017 22:25*

First print showed layer shifting due to Z not keeping up.


---
**Ray Kholodovsky (Cohesion3D)** *August 19, 2017 22:28*

You can go another 45 or so on the pots for the flat to be facing south east. 


---
**David Scull** *August 19, 2017 22:30*

Although looking online it seems the VREF is .864 for Z on the Melzi. That's where I have it.  Is it something in the configuration of Z?




---
**Ray Kholodovsky (Cohesion3D)** *August 19, 2017 22:31*

Failing that, can turn down the z / gamma max speed in config. 


---
**David Scull** *August 19, 2017 22:31*

If you go too far on the pot it goes back to zero - .86 or .87 is the best I can get.  


---
**David Scull** *August 19, 2017 22:37*

Got it thanks


---
**David Scull** *August 20, 2017 11:49*

So I've tried a bunch of config changes and it isn't working.  I sent the following Gpaz (a successful customer) and I'm going to check his config file.



Hi Gpaz,



Thank you for taking the time to respond to me.



I believe I am using the same smoothie hardware configuration that you are using - I obtained it from Ray.  It's the same configuration that Chris used in his Practical Printing video of conversion.  (Chris also noticed the heavy vibration in the Z axis after he switched out the boards.)  My Melzi was fried due to a melted wire terminal (bed).



Practical Printing's live stream: 




{% include youtubePlayer.html id="QDNocu2MLpc" %}
[https://www.youtube.com/watch?v=QDNocu2MLpc&list=PLKlntVS2tLE7O1fpIlRV0pkaI1J87W52M](https://www.youtube.com/watch?v=QDNocu2MLpc&list=PLKlntVS2tLE7O1fpIlRV0pkaI1J87W52M)



I am upgrading a V2 Wanhao i3 duplicator.  As you say, the current through the A4988 needs to be higher for the Z steppers.  I've adjusted it as high as it will go and I'm still not getting a smooth movement.  First prints are showing layer shifts because Z can't keep up with X and Y.



My machine is a V2 and ships with:



X, Y, E Motors – 42HS34(L)-0954-JA05 (0.95 amps)

Z Motors – 42HS34(L)-1204-JA05 (1.2 amps)



I'm guessing Chris' machine is the same.



Here is the guidance for setting the VREF (I think that stands for voltage reference) is here:



[https://3dprinterwiki.info/setting-the-stepper-current-on-the-melzi-board/](https://3dprinterwiki.info/setting-the-stepper-current-on-the-melzi-board/)



I have my pots tuned to 0.684 for X,Y & E and I have the Z at 0.864.  The Z is still not moving smoothly.  If you turn the pot higher it goes back to 0 VREF.  By the way, thanks for the warning about the ceramic screwdriver.  I've made all my changes with no power to the board because I don't have one yet.  I don't think I've damaged anything.



I think it's possible that Chris and I have Z motors that need more current than yours do.  If not - it's a  could be another configuration issue.  I just want to verify that your Z motors require more than .95 amps.  Some V1 machines shipped with all motors requiring 1.3 amps. If that is the case this hardware should work and I have a software configuration issue.



It could be that my A4988 is defective, but I doubt that because Chris had the same problem in the video and I don't think he solved it by tuning the pot.  (I've sent him a question but haven't heard back yet.)



I'm working with Ray, but he's busy and I thought checking with you would be worth the time because of your success.  



Here's my exchange with Ray at Cohesion3d for your reference as well:



[plus.google.com - Hi everyone, I will be upgrading my Wanhao Duplicator i3 to the Cohesion3d M...](https://plus.google.com/u/0/107221361722011316733/posts/X3aUczhv9zG)



I am working long hours currently, so I really haven't had the time to focus on this much until yesterday.  



Thanks again for taking time,



Dave



Thanks...


---
**David Scull** *August 21, 2017 00:48*

Okay it looks like I will need a stepper driver that can provide enough current to two 1.2 Amp stepper motors.  GPaz Monoprice Z motors are low amperage.  Any suggestions for replacements and will this require a firmware change?  Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 21, 2017 00:50*

I know the guys have played around with drv8825's as well, i personally don't like them.



This is my favorite driver, and I can't make them this cheap, so have at it.  Silent motors.  

[digikey.com - TMC2208 SILENTSTEPSTICK Trinamic Motion Control GmbH &#x7c; Development Boards, Kits, Programmers &#x7c; DigiKey](https://www.digikey.com/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626)



You will need to solder header pins onto it. 



Should only need to flip dir in config for the TMC with a ! or flip the polarity of each motor header. 


---
**David Scull** *August 21, 2017 01:17*

None available that don't require soldering.  I havent soldered in 20 years.  Before I commit to that step are you confident it will solve the current gap?  Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 21, 2017 01:26*

DRV8825, should come with headers soldered, and pretty cheap with amazon prime or ebay with US shipping.  They do work...



For that, will need to double the steps per mm value in config.txt.



Personally, I would first review with you what you have tried so far, A4988 with the pot at top current, and a lower speed should work.  When you have time, I would like to get some more details of where you are at with this. 



It's also possible that the Z axis is out of sync (left to right) and this is causing binding, which would then indeed cause the stepper to skip steps.  You can check if you can rotate the leadscrews by hand without much resistance, and do "blueprinting' which would involve moving the Z all the way to the top so that it levels itself out against the frame, then jogging down from there. 


---
**David Scull** *August 21, 2017 01:36*

Okay I will double check the z axis. But the printer worked flawlessly until the melzi fried. I hardly have to level the bed after I replaced the y carriage. I have the pot at top current and based on everything I read today it should work. So let's check it out. I'd love to know what practical printing is doing. I think he has the same situation.


---
**David Scull** *August 21, 2017 21:37*

Okay, here's an update.  The silver benchy came off the Wanhao this morning.  The Einstein was the last thing printed on the Wanhao we are upgrading before the Melzi board fried.  The two white benchies are the first print with standard config.txt file (middle) the second white benchy was printed with acceleration dropped down 20% in the config.txt file and printing very slowly (20 mm/sec)It prints very smoothly for a while but the taller the object goes the more layer shifting occurs.

![images/e71d2db59c42368ca2ddb6d860e49adb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e71d2db59c42368ca2ddb6d860e49adb.jpeg)


---
**David Scull** *August 21, 2017 21:49*

The VREF voltages are almost exactly the recommended voltages for the Melzi - very close to three digits accuracy.  I don't know if the VREF should be the same for our upgraded system as the Melzi.  The Z axis still vibrates - it vibrates more the lower the voltage and less the higher the voltage. Layer shifting is usually from acceleration (too fast), mechanical issue (belt tension, etc.) or a current situation (too high or too low) with the stepper motor.  Since I haven't moved the printer and there were no mechanical issues before the upgrade, the most likely issue is with the current to the z motors.  Unless there is a way to reconfigure config.txt to address the z vibration I think we are stuck.  I've tried a few config.txt changes with acceleration and max speed.  It doesn't seem to make a difference.


---
**David Scull** *August 22, 2017 14:09*

It looks like some A4988 compatible stepper drivers can deliver 2 amps and some cannot.  Amazon customers indicate you can't believe what the seller claims.  I think that is what we are dealing with here.  Pololu claims to be able to drive current up to 2 amps.  I'm going to try to find a verified A4988 that can provide the 2 amps.


---
**David Scull** *August 22, 2017 14:11*

 Pololu claims a max of 2 amps:



[pololu.com - Pololu - A4988 Stepper Motor Driver Carrier](https://www.pololu.com/product/1182/specs)






---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2017 14:16*

I'm providing fairly cheap A4988's from China.  They do work, but I don't expect more than an amp out of them.  I have had to go to DRV8825 for the double Z axis on some larger machines I have when the A4988 wouldn't pull it. 



Genuine A4988 are the ones from pololu's own site, and at 6 bucks per, I'd be much more inclined to get the TMC2208's for just a bit more.  If you need me to solder the header pins onto those for you I can do that.  



If you could post your latest config for me please (dropbox links are great) I would like to check the max speed and accell values in there.  



Your last benchy seems nice, I do think there is some squashing happening on a few layers - it might be a z not moving up thing, or it might be over extrusion/ other 3d printer dynamics. 


---
**David Scull** *August 22, 2017 15:48*

Sorry the last benchy is from the other Wanhao. I ordered the Pololus already soldered. I think it will solve the problem. I love your board and I like the idea that I can make the machine do what I want and change menu items etc. I want to get this right because it probably won't be long till the other Wanhao fails.


---
**David Scull** *August 22, 2017 16:25*

I was trying to give you a comparison of the two machines.  Before the board failure I had them performing equally well.  I don't think there are any structural issues with the machine.  They are very stable now that I've replaced the Y carriages - the ones that come with the machines warp and bend and make bed leveling a nightmare.  Anyway, I found some other folks online who had a similar problem with the stepper motors I'm dealing with - If I get a stepper driver to deliver 2 amps I believe there will be no problem.  Gpaz is using stepper motors in his z motors that draw .3 amps or something like that so he's not going to have the problem.  I am confident that the printer will be better with your board.


---
**David Scull** *August 22, 2017 17:52*

I want you to know that you've been great and I should have done my homework ahead of time.  But I think we can figure this out and still provide a way for Wanhao / Monoprice / Cocoon / etc. users to have a path to fix their machines.  And I believe many more of these machines based on Melzi will fail.  The boards are not well engineered.  Thanks.  I'll post my config file when I get the chance.  I lowered acceleration and the max speed settings - several times - in each case the machine slowed down, but the layer shifting continued.


---
**Ray Kholodovsky (Cohesion3D)** *August 24, 2017 03:05*

Thanks for sticking with it.  Sorry for the silence, I've been doing the Di3 conversion myself (again) so that I can try to reproduce what you've got going on.  I have some ideas. 


---
**David Scull** *August 24, 2017 13:13*

You've been great, Ray.  No problem at all.  On paper, what you provided should work.  The stepper drivers should be able to deliver 90% of the 1.2 amps we need with a VREF of 0.864 which should deliver 1.08 amps to the Z motors.  This wiki is where I started to think it might be an amperage issue:



[3dprinterwiki.info - Setting the Stepper current on the Melzi board](https://3dprinterwiki.info/setting-the-stepper-current-on-the-melzi-board/)



The 1 ohm resister it mentions is on the Melzi board and I wonder if that is where there is a difference on the stepper drivers we're using?  I found several places on the Internet where people said the cloned A4988s could not deliver more than an amp and the Pololu steppers could.  I'll have them on Friday or Saturday and we'll see.



It could be something completely different, Ray.  I've only had an hour here and there to work on this.  What I do know is that when I adjust the VREF voltage, the lower the voltage the more intense the vibrations in the Z axis, the higher the voltage, the less the vibration.  The Wanhao that I have up and running right next to the machine we are upgrading has no vibration and the Z axis is quiet.  Also, the X and Y are actually smoother than the Wanhao's movement. That's why I'm thinking the amperage issue is what we're dealing with.



Again, I love a good puzzle and I'm learning and resurrecting brain cells as we do this.  Once we get the hardware right, I want to go through the config.txt line for line and understand all the ways I can modify the machine's menus and functions.  


---
**David Scull** *August 24, 2017 13:16*

For clarity: the X and Y axis on the upgraded machine with the smoothieboard are more smooth in movement than the orignal Wanhao's X and Y movement.


---
**David Scull** *August 25, 2017 00:19*

Drum roll!!! So I got the new stepper drivers today.  Popped them on and tuned the VREF.  All the vibration went away.  The stepper motors were completely quiet.  I replaced the config.txt file with your original and printed this Benchy.  There is a little ringing.  But this is awesome.  I haven't even set up the slicer for the new board.  

![images/b874525172ebac55b24d2936422c1981.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b874525172ebac55b24d2936422c1981.jpeg)


---
**David Scull** *August 26, 2017 13:10*

Hi Ray, I'm trying to create a custom menu choice for turning the hotend fan on at 10%, 25%, 50%, 75% and 100% and also turning it off.    For some reason it's not taking the pwn S command and it just turns the fan on 100% no matter what I send it:



custom_menu.Fan_10.enable                  true              #

[custom_menu.Fan_10.name](http://custom_menu.Fan_10.name)                    Fan_10_Percent    #

custom_menu.Fan_10.output_pin           2.6               #

custom_menu.Fan_10.output_type         pwm               # 

custom_menu.Fan_10.command            M106 S26          #



Thoughts?



I've got things pretty locked in now. Prints are almost back to the quality I had before I changed out the board.


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2017 13:12*

Can you link me your entire config.txt please? 



The hotend fan is just on off, there is no intermediate speed application intended for it. 



The part can is variable. 



This can be reconfigured, possibly, but why? 


---
**David Scull** *August 26, 2017 14:10*

How do I send you the config.txt?  I have dropbox, but I've never exchanged files through it.



The fan I'm talking about is the variable fan used to cool the extruded plastic.  I'm just trying to mirror the functionality of the original Wanhao duplicator i3 menu.  The fan can be manipulated manually during a print.  Sometimes I'll experiment with fan speeds with different materials, distance from the bed, etc.  Chris asked for a fan control on the menu during his video (practical printing - think his name is Chris).



It's on pin 2.6.  I can control fan speeds with Simplify3d through the USB and through the G code created by Simplify3d.


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2017 14:13*

Yeah just make a folder in Dropbox, put it in the folder, and right click on the folder to get a share link, paste it here. 



Ah, the term for that one is "part cooling fan". I will investigate once I see the config file. 



Yepp, that's Chris.  



Can you confirm if you made any changes to your mount plate, and link me the latest version? I would like to print that. Thank you! 


---
**David Scull** *August 26, 2017 14:19*

Yes - I have the mount plate sized perfectly now - I use a small wire tie to hold the splitter.  



When it asks for who do you want to share with? Do I just enter your name?


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2017 14:21*

Perfect, please send that.



[https://www.dropbox.com/help/files-folders/view-only-access](https://www.dropbox.com/help/files-folders/view-only-access)


---
**David Scull** *August 26, 2017 14:28*

[dropbox.com - Cohesion3d](https://www.dropbox.com/sh/9ehrzklxy01n1qv/AAALhW9c3oxRP7P-bbA5DTnla?dl=0)






---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2017 14:33*

Case sensitivity.  Needs to be fan lowercase. 






---
**David Scull** *August 27, 2017 04:17*

Ray, I broke the sd card slot, it came right off the board.  What is the configuration of the board that comes with the upgrade kit.  I'll need to order a new board.  Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2017 11:00*

Email info at cohesion3d dot com

Include some pictures of it please

Well get you taken care of. 


---
**David Scull** *November 11, 2017 14:31*

Ray, I still can't get a command to work from a custom menu to allow the part fan to have variable output.  I've changed cases on the code and no matter what I've done the command only turns the fan on to full power regardless of the S??? value - S255 produces the same result as S127 (1/2 speed).  



Heres my current configuration:



custom_menu.fan_on.enable                  true              #

[custom_menu.fan_on.name](http://custom_menu.fan_on.name)                    Fan_On            #

custom_menu.fan_on.output_pin              2.6               #

custom_menu.fan_on.output_type             PWM               # PWM output settable with S parameter 

custom_menu.fan_on.command                 M106 S255         #



custom_menu.fan_on_50.enable                  true              #

[custom_menu.fan_on_50.name](http://custom_menu.fan_on_50.name)                    Fan_On_50_Per     #

custom_menu.fan_on_50.output_pin              2.6               #

custom_menu.fan_on_50.output_type             PWM               # PWM output settable with S parameter 

custom_menu.fan_on_50.command                 M106 S127         #



Do I need to use the switch command to get the S??? parameter working?



Thanks

[smoothieware.org - pwm-capable [Smoothieware]](http://smoothieware.org/pwm-capable)


---
**David Scull** *November 11, 2017 14:48*

To be clear no matter what S value parameter I use I get a full power fan




---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 21:57*

I think you need to use an underscore instead of a space in your command lines in config there. 



Otherwise... can you send the respective commands via a terminal and verify they work properly wrt fan speed? 


---
*Imported from [Google+](https://plus.google.com/107221361722011316733/posts/X3aUczhv9zG) &mdash; content and formatting may not be reliable*
