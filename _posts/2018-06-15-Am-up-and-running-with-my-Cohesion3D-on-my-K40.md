---
layout: post
title: "Am up and running with my Cohesion3D on my K40"
date: June 15, 2018 13:55
category: "K40 and other Lasers"
author: "Scruff Meister"
---
Am up and running with my Cohesion3D on my K40. This is the first time I have given the laser a serious test. I am finding when I cut 1/8" balsa that my cut is not vertical as shown in the example below.



I don't think this is a result of the Cohesion system, but is this a result of (poor) mirror alignment or something else? I have checked my bed and it is level. What I can't figure out is how both surfaces angle outward. Any advice gratefully received!

![images/aa8bf30a93518d3a5e1aa4b2512a25ca.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aa8bf30a93518d3a5e1aa4b2512a25ca.png)



**"Scruff Meister"**

---
---
**Anthony Bolgar** *June 15, 2018 14:05*

The laser beam is a cone, you will never have a perfectly vertical cut. You can minimize it by using a longer focal length lens like a 4" instead of the common 2" (Would be half as much of an angle)


---
**Anthony Bolgar** *June 15, 2018 14:06*

If the bevels were in the same direction, then it would be a squareness issue. But what you are seeing is completely normal.




---
**Jim Fong** *June 15, 2018 15:21*

1/8” balsa should get near vertical sidewalls, especially such a easy to cut material.   



Check laser/material focus height. Check the laser head is vertical.  They are so cheaply put together that mine was installed crooked. 



Also make sure the bed is flat and parallel to the linear rails.   I weigh down the edges of thin material so it doesn’t lift up.  



Picture of actual cut would help to see how bad it is. 



Also add that going to slow will burn the top more than bottom, leading to the angled cuts.   Properly cut 1/8” balsa should hardly see any kind of sidewall burning.  Play around with speed and power values. 


---
**Scruff Meister** *June 15, 2018 16:25*

Thanks, it sounds like I need to do more experiments. The problem is not as extreme as my picture which was just for graphic purposes.



For 1/8" balsa (fairly hard grade), my cut settings were 100mm/sec at 70% of 20mA power. Took four passes to cut. I've tried a number of other settings but this seems best so far.



Is there a directory of "known good" cut settings or at least starting points for given materials/thicknesses?


---
**Jim Fong** *June 15, 2018 16:43*

**+Scruff Meister** I would try to find a feed/power setting that will cut it in one pass.  Each additional pass you make will slightly burn the top of the balsa leading to tapered cuts. 


---
**Joe Alexander** *June 15, 2018 19:18*

is your machine one with an analog ammeter, or is it one of the digital power indicator rigs? personally i can cut 1/8" birch plywood using 15ma@6mm/s, 100mm/s for cutting is way to fast imo. That is more of an engraving speed.


---
**Scruff Meister** *June 15, 2018 20:48*

Great, thanks. I'll do some more experiments to find a single pass speed.


---
*Imported from [Google+](https://plus.google.com/116126524987588221083/posts/h5B7RLa75F4) &mdash; content and formatting may not be reliable*
