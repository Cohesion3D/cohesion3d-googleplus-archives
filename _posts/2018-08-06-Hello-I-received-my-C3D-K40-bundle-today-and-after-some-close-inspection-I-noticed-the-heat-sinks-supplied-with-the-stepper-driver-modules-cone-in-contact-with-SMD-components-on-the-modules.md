---
layout: post
title: "Hello! I received my C3D K40 bundle today and after some close inspection I noticed the heat sinks supplied with the stepper driver modules cone in contact with SMD components on the modules"
date: August 06, 2018 20:11
category: "C3D Mini Support"
author: "Raymond Zacharias"
---
Hello!  I received my C3D  K40 bundle today and after some close inspection I noticed the heat sinks supplied with the stepper driver modules cone in contact with SMD components on the modules.  The adhesive also allows the heat sink to move in top of the driver chip.  This seems like a poor design as a possible short could result in damage to the module and or board, they can easily slide and come in contact with the header solder joints. I may put a chamfer on the edges of the heat sinks to add some clearance from the SMD components.  I have similar heat sinks on my Ras Pi boards and the adhesive is very firm and does not slide when mounted.  This is more of a heads up on a possible issue with the shipped heat sinks.





**"Raymond Zacharias"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2018 20:22*

Appreciated. I know they do like to move around during shipping :) but once "in open air" they seem to stay where they are.  Are you experiencing something different? 



They slide back if you push them sideways, yes. I would recommend removing them and then reapplying them in the middle of the module. 



The heatsink itself should be anodized so it shouldn't short, but it is a good practice to have them not in contact with the pins.


---
**Griffin Paquette** *August 07, 2018 18:34*

Remember these don’t have any dynamic forces on them so they really should be fine after sticking them on there. This is pretty common with driver heatsinks. I would just make sure the metal doesn’t come in contact with the components and let it be and go engrave to your heart’s content!


---
*Imported from [Google+](https://plus.google.com/112506514800116719679/posts/TKMp4vSRzCZ) &mdash; content and formatting may not be reliable*
