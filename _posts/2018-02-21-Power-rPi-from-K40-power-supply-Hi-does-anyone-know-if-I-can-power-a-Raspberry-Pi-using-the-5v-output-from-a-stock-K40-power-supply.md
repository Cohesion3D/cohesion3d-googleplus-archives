---
layout: post
title: "Power rPi from K40 power supply. Hi, does anyone know if I can power a Raspberry Pi using the 5v output from a stock K40 power supply?"
date: February 21, 2018 16:07
category: "K40 and other Lasers"
author: "Tako Schotanus"
---
Power rPi from K40 power supply.



Hi, does anyone know if I can power a Raspberry Pi using the 5v output from a stock K40 power supply? I want to put it right next to my Cohesion3D and I'm not going to plug in anything into any USB ports (except for the connection to the Cohesion3D board of course) so there should be no excessive drain.





**"Tako Schotanus"**

---
---
**Don Kleinschnitz Jr.** *February 21, 2018 16:13*

That's hard to say, the supply only provides 1 amp maybe 1.5 if you have great cooling at the LPS.



I don't know what a rPI draws and what you have hooked to it.



I wouldn't chance it. Get a small stand alone supply. You might need other DC power for lights and accessories anyway?






---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 16:15*

Very bad idea. 


---
**Jim Fong** *February 21, 2018 16:27*

They recommend 2.5amps for a pi3 however I use a 2amp 5volt Wall wart and seems to be fine.  It really depends on what other USB stuff are also connected. The older model Pi’s require much less power. 



Use a good high output phone charger instead. 


---
**Don Kleinschnitz Jr.** *February 21, 2018 16:39*

**+Jim Fong** that supply definitely cannot handle that!


---
**Tako Schotanus** *February 21, 2018 16:46*

Ok thanks everyone, it's clear to me it's a bad idea heh. Now to see where I can splice in an extra power supply (I don't want the machine itself to have more than 1 cable going to a wall socket)


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 16:47*

Tap the AC going to the lpsu (left screw terminal). 



Don, exactly how bad of an idea is that? 


---
**Jim Fong** *February 21, 2018 16:53*

**+Tako Schotanus** I use a switched power strip which the K40, water pump, air pump, fan, +5/+12/+24 supplies are connected to.  One switch powers everything up.  Also stops me from forgetting to turn on the water pump.  I also added a water flow detector as backup. 


---
**Don Kleinschnitz Jr.** *February 21, 2018 22:19*

**+Ray Kholodovsky** not a problem as long as the input plug, wire and fuse can handle the additional load. The ac load for a 5A dc supply is pretty small.








---
*Imported from [Google+](https://plus.google.com/+TakoSchotanus/posts/UZnrvmmCzx3) &mdash; content and formatting may not be reliable*
