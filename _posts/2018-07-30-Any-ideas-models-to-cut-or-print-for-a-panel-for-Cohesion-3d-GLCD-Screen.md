---
layout: post
title: "Any ideas/models to cut or print for a panel for Cohesion 3d GLCD Screen?"
date: July 30, 2018 14:14
category: "K40 and other Lasers"
author: "Mario Gayoso"
---
Any ideas/models to cut or print for a panel for  Cohesion 3d GLCD Screen?

Also,​ the regular panel that the K40 comes with is awful. Any mods on that?

Thanks in advance!





**"Mario Gayoso"**

---
---
**Tech Bravo (Tech BravoTN)** *July 30, 2018 14:29*

[lasergods.com - Custom K40 C3D Control Panel](https://www.lasergods.com/custom-k40-c3d-control-panel/)




---
**Tech Bravo (Tech BravoTN)** *July 30, 2018 14:30*

if you have the digital version you can pull from this: [lasergods.com - Odin’s Control Panel](https://www.lasergods.com/odins-control-panel/)




---
**Mario Gayoso** *July 30, 2018 17:37*

**+Tech Bravo** Awesome, maybe I can redesign and extrude it in 3D for printing because as today I haven't cut anything yet with my laser cutter! All I have been doing are mods! is that crazy?




---
**Mario Gayoso** *July 30, 2018 19:28*

**+Tech Bravo** What do you think of this? I also saw that is almos 300mm long it will take almos the whole working space to cut in my K40 or print in my CR10 right?

![images/953331b463b44da9ec5ea93bc18c62d3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/953331b463b44da9ec5ea93bc18c62d3.png)


---
**Tech Bravo (Tech BravoTN)** *July 30, 2018 19:36*

The first link is probably closer to what you have. Odin has a smaller panel that i had to modify. The analog one above probably works. I put the odin panel here so you could copy the digitial control panel part into the other panel


---
**Mario Gayoso** *July 31, 2018 03:46*

First test. What do you recommend me to do? Did you modified the file?![images/ac8d81ff71d78658f0d09518100570d0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ac8d81ff71d78658f0d09518100570d0.jpeg)


---
*Imported from [Google+](https://plus.google.com/+MarioGayoso/posts/9kATvuw1U6Y) &mdash; content and formatting may not be reliable*
