---
layout: post
title: "There is now the beginnings of a laser FAQ/ Troubleshooting guide here: Mostly I just started putting in the things I found myself typing over and over again: Why won't the board connect/ LW see it?"
date: May 11, 2017 01:47
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
There is now the beginnings of a laser FAQ/ Troubleshooting guide here:



Mostly I just started putting in the things I found myself typing over and over again:

Why won't the board connect/ LW see it? 

Why won't the laser fire? 

Axis skipping.

And most importantly, the PWM tuning guide that I must have typed a hundred times.  



Now with 40% more detail!



[https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq](https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *May 11, 2017 05:18*

Nice addition Ray.


---
**Kirk Yarina** *May 11, 2017 15:45*

Now you can just answer "RTFM" with a link :)


---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 15:46*

Well, RTFM is Peter's phrasing. I would just say, "Here you go: <link>"


---
**Jason Dorie** *October 11, 2017 00:36*

LMGTFY...


---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2017 00:37*

Way to revive an old thread there. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/6Kh8XYu9Hmn) &mdash; content and formatting may not be reliable*
