---
layout: post
title: "The wiring is very different from my stock controller enough to cause me to hesitate ordering one"
date: March 27, 2017 22:15
category: "K40 and other Lasers"
author: "bbowley2"
---
The wiring is very different from my stock controller enough to cause me to hesitate ordering one. None of the connections on my stock board are identified. I would need some direction for rewiring.

![images/28488d8fd502c12201d4b3a76bed9107.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/28488d8fd502c12201d4b3a76bed9107.jpeg)



**"bbowley2"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2017 22:44*

The connections on the power supply look typical. Can we see a pic of the control board? 


---
**Eric Lien** *March 27, 2017 23:02*

Is he thinking that PSU board is the old controller board?


---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2017 23:10*

Oh dear. That's not what the C3D replaces. 


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 04:12*

**+bbowley2** please see the upgrade instructions here: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions.  : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



It is important to note that the C3D replaces the (probably) green control board monted on the inside of the front panel.  You absolutely need a working power supply before doing this upgrade. 


---
**bbowley2** *March 28, 2017 04:31*

My photo is of the PSB.  I was able to trace the AC to the PSB.  The fuse is above that.  Before I change anything I need to know why my K40 stopped.  Everything I have read about the C3D controller has me interested but I have to know where the problem is first.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 04:34*

Don answered your question in the other thread in the k40 group. It seems your power supply is bad and you need to replace it. 


---
**bbowley2** *March 29, 2017 00:18*

Ray, Can't find Don's answer.


---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2017 00:22*

[plus.google.com - Just went into my shop and tried to turn on my K 40. Nothing. There is power ...](https://plus.google.com/110862182290620222414/posts/5NU44XtLmpn)


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/Hu4hKWS7Zep) &mdash; content and formatting may not be reliable*
