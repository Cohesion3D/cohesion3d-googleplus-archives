---
layout: post
title: "So I got my k40 machine today, Little to no damage"
date: June 05, 2017 03:19
category: "K40 and other Lasers"
author: "Bromide Ligand"
---
So I got my k40 machine today, Little to no damage. Only bent parts are where the screws lock in the openings. The y axis has a bad scratch where the 2nd mirror is mounted. Other than that I got a sweet machine. was $435 Australian.



Now, this machine comes with weird wiring SO I don't wana electrocute myself. It comes with a standard 3 pin male socket like computer psu's. But here, the top /middle pin has a wire going to another terminal at the back of the case. They call this a ground terminal.



The cable they provide is Chinese is is missing its Top/middle pin which is normally the ground. SO would it be ok/safe If I just used a Male with 3 pins cable that  I already have and forget the grounding terminal ?





**"Bromide Ligand"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2017 03:20*

Pictures? 


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2017 03:22*

Also, there's a K40 group where your general questions may reach a larger audience. And we'll be here when you're on to the board upgrade questions. It sounds like you're talking about high voltage wiring, not board upgrade wiring, at this time 


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2017 03:22*

[K40 LASER CUTTING ENGRAVING MACHINE](https://plus.google.com/communities/118113483589382049502?iem=1)


---
**Bromide Ligand** *June 05, 2017 05:10*

oh sweet thanks. I will post some photos here of the stock board and in the k40 group.




---
**Bromide Ligand** *June 05, 2017 05:19*

[https://imgur.com/a/VUS5V](https://imgur.com/a/VUS5V) Looks like a newer version 9 board. Also my solder joints are a lot cleaner than most I have seen and I have a p[redrilled Hole if I ever want to extend the tube to a longer one. Plus the 2 extra power sockets for Pump/exhaust.




---
**Griffin Paquette** *June 05, 2017 17:30*

So, to be clear, you have a cable that indeed does have the ground (3rd pin) connected to the socket end?



And the wire from the middle/3rd pin just wraps around to that ground terminal aka they are connected?


---
**Bromide Ligand** *June 06, 2017 01:49*

Top one came with the laser, The bottom one is the one I have.

![images/06a63e1415fd78e094cced6ecc97687d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/06a63e1415fd78e094cced6ecc97687d.jpeg)


---
**Joe Alexander** *June 06, 2017 05:26*

i say go for the bottom one with ground prong, safer is better.


---
**Bromide Ligand** *June 06, 2017 13:27*

nvm I don't have to ground this.  
{% include youtubePlayer.html id="YvrMeUUzaBo" %}
[youtube.com - Chinese laser cutter tutorial part 1](https://youtu.be/YvrMeUUzaBo?t=318)


---
**Griffin Paquette** *June 06, 2017 15:19*

In a technical sense you don't really need to, but there is still a capacitive coupling to the chassis, so it wouldn't hurt if hooked up properly. 


---
**Joe Alexander** *June 06, 2017 16:18*

that is assuming that your home outlets are also properly grounded, always verify. If you have less resistance than your house ground I recommend using the ground lug to a more local grounding pole. I personally do not use the grounding lug but i have tested the grounding in my garage and its solid(not to mention running it off a GFCI circuit)


---
*Imported from [Google+](https://plus.google.com/108393288879485897084/posts/JqkDSLypfa6) &mdash; content and formatting may not be reliable*
