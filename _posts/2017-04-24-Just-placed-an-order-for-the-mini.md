---
layout: post
title: "Just placed an order for the mini"
date: April 24, 2017 17:07
category: "General Discussion"
author: "Everett Green"
---
Just placed an order for the mini.  Thank you and looking forward to it.





**"Everett Green"**

---
---
**Griffin Paquette** *April 24, 2017 17:19*

Welcome to the C3D user club! Hope you enjoy it!


---
**Everett Green** *April 24, 2017 17:27*

Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 00:45*

Yes welcome my good sir. Feel free to get acquainted with the documentation ahead of time.  


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 02:09*

And install LaserWeb! That's good stuff too. 


---
*Imported from [Google+](https://plus.google.com/118241565715933496178/posts/QUSeq56JHNo) &mdash; content and formatting may not be reliable*
