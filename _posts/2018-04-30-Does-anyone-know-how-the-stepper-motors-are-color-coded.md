---
layout: post
title: "Does anyone know how the stepper motors are color coded?"
date: April 30, 2018 01:10
category: "C3D Mini Support"
author: "james blake"
---
Does anyone know how the stepper motors are color coded?  Mine has 4 wires going to the connectors, 1 is coded blue, red, white, yellow / the other is coded blue, red, yellow, white.  Neither seems right on the cohesion board, neither motor starts when I turn power on and they both move freely.





**"james blake"**

---
---
**james blake** *April 30, 2018 01:15*

The main board was a ms10105 4.87 but it does not give a listing of the pin outs for the connector.


---
**Joe Alexander** *April 30, 2018 05:09*

look up how to test stepper motore, the 4 wires make up 2 phases(making them bipolar) and when 2 wires of the same phase are shorted out to each other the shaft becomes slightly more difficult to turn. this would make phase a or b.


---
**james blake** *April 30, 2018 08:58*

Thanks I will try that.


---
**Don Kleinschnitz Jr.** *April 30, 2018 12:12*

Motor specs in this post  but test as **+Joe Alexander** suggests.



[donsthings.blogspot.com - K40-S Motors](http://donsthings.blogspot.com/2016/06/k40-s-motors.html)


---
**Ray Kholodovsky (Cohesion3D)** *April 30, 2018 19:31*

It doesn't matter.  The board won't home on power up.  Tell it to home from Lightburn - it should move to the rear left. If it moves another way, power down and flip the appropriate cable. 


---
**james blake** *April 30, 2018 19:39*

Thank you, I have not gotten that far yet, I am still getting the wire and connectors to hook them up at this time.  The connectors were wired differently and were too short so I have had to lengthen them and I am adding disconnect connections to make it easier to get then right, the connectors I have will only fit on board 1 way so have to move the wires, they are fat and the driver connection is in the way to turn them around but that is what I thought at first just flip them around.


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/BDHiXEE827f) &mdash; content and formatting may not be reliable*
