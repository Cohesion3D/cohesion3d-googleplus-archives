---
layout: post
title: "I am configuring the cohesion3d remix to a 3-extruder printerboard"
date: December 11, 2016 11:01
category: "General Discussion"
author: "Ren\u00e9 Jurack"
---
I am configuring the cohesion3d remix to a 3-extruder printerboard.



No matter what I do, mosFet3 (afaik Pin 1.22) is always on. Even hitting the reset-button, the red LED stays on. Is it broken or just misconfigured? I can rewire and reprogram the board to make use of this FET, but if I don't use it, it stays on. Hmmm....





**"Ren\u00e9 Jurack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 00:16*

It is a relatively weak pull down resistor on the MOSFET + that pin from the microcontroller not being defined in the smoothie config that is causing this - allowing the MOSFET to be on a tiny amount enough to drive the LED. 

Test #1: hook up a load such as a hotend heater. LED should be off then. 

Test #2: define the pin in config. You can quickly switch this pin out for any of your existing switch modules and it should be off once you save and reset. 


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/65QiRQHnPj3) &mdash; content and formatting may not be reliable*
