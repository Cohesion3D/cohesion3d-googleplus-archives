---
layout: post
title: "Just as a little update since I was having issues before with the Cohesion3D Mini, seems the board has been doing MUCH better now that I have gotten the grbl firmware going with LaserWeb"
date: August 28, 2017 15:42
category: "C3D Mini Support"
author: "Tammy Mink"
---
Just as a little update since I was having issues before with the Cohesion3D Mini, seems the board has been doing MUCH better now that I have gotten the grbl firmware going with LaserWeb. No connection issues, no freezes, no shifting on rasters, seems to run smoother at higher speeds too. I was doing assorted projects all weekend with only trivial issues :)





**"Tammy Mink"**

---
---
**Griffin Paquette** *August 29, 2017 03:42*

Awesome to hear! Just let us know as always if you run into any hiccups


---
**John McKeown** *August 30, 2017 18:37*

Would you mind sharing the gcode settings you are using for GRBL in laserweb ie. homing etc. Reading through the last few posts, I've been  having very similar issues to you. Have managed to get GRBL installed and open in Laserweb but need to fine tune it. Thanks! 




---
**Tammy Mink** *August 30, 2017 19:13*

I believe the machine settings are M4 in the Start gcode, M5 in End Gcode and Home to $H and PWM scale from 1 to 1000. I'll try to remember to take a screenshot when I get home :)




---
**John McKeown** *August 30, 2017 19:45*

Lovely! $H seems to work great. thanks :)


---
**Jason Dorie** *August 31, 2017 06:29*

So Grbl is an option on the Cohesion board (instead of Smoothie)?  Did it take long to set up?  I'd be curious to try this too - my raster speeds in LightBurn are slower than I expected running the Smoothie firmware.


---
**Tammy Mink** *August 31, 2017 11:57*

Yes you can have use grbl-lpc on the Cohesion3D Mini (not sure about the other variations). You can get the firmware file at [https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini) . Basically you copy that file to the miniSD card and reboot the board (just note with the grbl you won't see the minisd card in windows anymore under grbl). If you need to make config changes you need to send them via the command line (like how you would send a home command etc). I found a listing of the config options at [github.com - LaserWeb4](https://github.com/LaserWeb/LaserWeb4/wiki/2.2-Initial-Configuration---GRBL-LPC-1.1) . I didn't need to change anything though I did play with the PWM frequency a bit and the default worked best for me.




---
**Jason Dorie** *August 31, 2017 19:46*

Thanks for the link, Tammy - I'll give that a go this weekend.


---
**Don Kleinschnitz Jr.** *September 02, 2017 12:52*

#K40Smoothie+Grbl


---
**Jason Dorie** *September 02, 2017 17:37*

I have this working in LightBurn now too.  Much faster rasters.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/84NS4kp57Te) &mdash; content and formatting may not be reliable*
