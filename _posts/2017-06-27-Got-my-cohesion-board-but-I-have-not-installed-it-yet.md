---
layout: post
title: "Got my cohesion board, but I have not installed it yet"
date: June 27, 2017 04:24
category: "C3D Mini Support"
author: "Bromide Ligand"
---
Got my cohesion board, but I have not installed it yet. Doing some mods to the chassis first, like changing out all the low quality screws. All off them had their threads stripped... 

Also I would like to add another end stop to my machine. So If I added a new endstop , where would  I be connecting this to on my c3d board? 



This is the mod I am doing: [https://www.thingiverse.com/thing:1534418](https://www.thingiverse.com/thing:1534418)





**"Bromide Ligand"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 27, 2017 16:26*

Where would the endstop be "locating"? And are you keeping the stock endstops? 


---
**Bromide Ligand** *June 28, 2017 01:33*

yea keeping the stock ones and adding a 3rd. as you can see in the photo, X will be the new end stop. Y is moved to the other side and the last endstop will stay stock.

![images/ea0282e751ee69e2abffd0f17e87b345.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ea0282e751ee69e2abffd0f17e87b345.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 01:35*

Ah ok.  I guess you're going for a Y Min endstop then.  So... The stock wiring takes up the X and Y ports of the board.  You can plug this one into Z min port (the 3rd one) and then reassign the pin to designate it as Y min in the config.txt file. 


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 01:48*

I may have misread what you said.  

Let me rephrase: 

The stock K40 has a X min and Y Max endstop.  These are wired to the X and Y endstop ports of the C3D Mini. If you want to add a 3rd or 4th, add them to the physical Z min/ Z max ports and set the correct pin assignments in the config file. 


---
*Imported from [Google+](https://plus.google.com/108393288879485897084/posts/dXbn6cFg97p) &mdash; content and formatting may not be reliable*
