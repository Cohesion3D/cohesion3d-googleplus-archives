---
layout: post
title: "Hi Ray, thank for the excellent support, attached are the first tests I have made with the C3D mini board"
date: October 19, 2017 07:22
category: "C3D Mini Support"
author: "ki ki"
---
Hi Ray, thank for the excellent support, attached are the first tests I have made with the C3D mini board. The help and patience you provided are appreciated. The lighthouse is on some slate, the wildcat is on some of that horrible cupboard backing board. I am very pleased with the results, thanks again for the help.

=)



![images/80efab90c6d34e3d92c9cefc2232b8ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/80efab90c6d34e3d92c9cefc2232b8ce.jpeg)
![images/319502f20a9d1b05dbc78d4ef650ef02.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/319502f20a9d1b05dbc78d4ef650ef02.jpeg)

**"ki ki"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 13:59*

Cool, which software? 


---
**ki ki** *October 19, 2017 14:12*

For these 2 I used laserweb4 but I am working with Zax to figure out how to get T2Laser working correctly.


---
*Imported from [Google+](https://plus.google.com/101815393777110076549/posts/4RErjrKUmBc) &mdash; content and formatting may not be reliable*
