---
layout: post
title: "Just when I thought I had it, I'm back to ruining product"
date: July 09, 2017 23:15
category: "C3D Mini Support"
author: "John Milleker Jr."
---
Just when I thought I had it, I'm back to ruining product. This time on a small gcode file.



However, I think I'm close. I was overshooting to the right on the new board, turned the stepper Driver pot 45° Clockwise. Started overshooting left, moved my Stepper Drive pot counterclockwise and now I'm stepping right again. (I've moved slightly clockwise again and running more tests)



I was told in another post that there's something I can meter out. Does anyone know what that is? I asked but didn't get an answer. Can I check voltage on this pot and find whatever sweet spot I need to be at?

![images/52ccc7b00e08e6d5fc4425a9325da9d1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/52ccc7b00e08e6d5fc4425a9325da9d1.jpeg)



**"John Milleker Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 23:16*

If I recall correctly you have not yet added a separate power supply to your machine yes? 


---
**John Milleker Jr.** *July 09, 2017 23:26*

No, I'm running an external 24v 6a supply, added it at the same time I installed the new board. This is also running the file from the SD Card (using the trick you told me about saving the gcode to the computer first), and unmounting the drive in Windows and completely disconnecting the board from the computer.


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 23:27*

Gotcha. Yeah go a full 90 on the pots (flat facing to the right). 


---
**John Milleker Jr.** *July 09, 2017 23:36*

I was there (90 degrees, facing right) to start and I was getting big steps. Does it make sense that 90° steps big to the right, 45° was stepping small to the right, about 30° was stepping small to the left? It seems like if I can find that sweet spot on the pot, maybe I'll find a spot where I get no steps at all? 



I'm running a test now at somewhere between I'd estimate 30 and 45°, I also want to move the 24v 6a supply to another breaker off of the pumps and extraction fan just for giggles. That'll be the next step.


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 23:39*

Not really, no. The motor needs more current the faster it goes. Once you're on a new power supply, 90 degrees (right) should be fine. You can even go about another 30 or so degrees to fully max it out but be careful not to over turn as then you will end up back at 0 current. 



If this is still causing issues then we start investigating other issues including heat (I've never had the a4988's get hot enough to need a fan, which is why I use them), speeds, and mechanical interference. 


---
**John Milleker Jr.** *July 09, 2017 23:53*

Okay, I'm at about 35-40° now, it just did a  successful print. I moved only the Cohesion3D Mini and steppers to a new breaker and am running another test. I'll crank them to 90° after this test and see where we go. 



The heat sinks get warm, X obviously more so than Y, but not what I'd consider hot. That was one of my tests awhile back, I put equal load on both steppers by doing a diagonal path on my rasters in LaserWeb and the issues were still there.



Speaking of diagonal, I just pulled out a failed print and it failed on both the X and Y. Is this a clue? If I had a problem with just one axis, I don't think it should look like this. Could there be an issue with interpretation? (attached)

![images/b79a1f3263c264133e7e08142d9e4bfb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b79a1f3263c264133e7e08142d9e4bfb.jpeg)


---
**John Milleker Jr.** *July 10, 2017 00:06*

Second test worked with pots at 35-40. I've just cranked the pots to 90° plus a kiss of maybe 10 more degrees. Running the same file again. Will report back on result and how the sinks feel.


---
**John Milleker Jr.** *July 10, 2017 01:38*

Four copy paper test engraves and I've had no issues. That's with the Stepper Driver pots at about 100° and on a different breaker than my pumps/air handler. 



The drivers seemed to get a little warmer, but still not what I would call hot. I may treat them to a little 40mm fan anyway for the longer jobs.



I ran a job through LaserWeb on a new material and when it's done the motors stay locked. I turned off my pumps and air and noticed the motors were whining a little while locked. Not sure if that's a bad thing or not.



Getting dinner and then I think I'm going to send the book again which is what messed up in the first place.


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 01:40*

Yeah, motors stay energized after any sort of movement. The command to turn them off is M18, you can add this to your end gCode if extremely confident, or add a macro button to LW.  They while whine at higher power too, yes. 


---
**John Milleker Jr.** *July 10, 2017 04:09*

Makes sense, I seem to remember my CNC servos doing the same thing but not as noticeable. Good call on the GCode, I like that I can just go into the LCD and power the motors off, I've found myself doing a lot of duplicate work so it's good that the machine is locking down to keep good registration for repeats.



My last test piece (of SIX test runs, all looking good) just went through on this file. Sending my book in next. The only two things I've done was set the pots to about 100° (I've done 90 degrees before with no success) and move the external 24v 6a power supply to a different breaker from my other running systems (water, air assist, vacuum).




---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 04:10*

You can also make it home on power up!  I find it dangerous that a machine move without telling it to explicitly, but it's all configurable and left up to the user's choice. 


---
**John Milleker Jr.** *July 10, 2017 04:47*

Happened again, sheesh. As soon as I put my last book in, I ruin it a little sooner than the first book. And after six good tests all the way through the damn thing. I'm doing nothing different. The sinks on the drivers are hot.. Not scald me hot, but noticeably hotter. I could keep my fingers on both sinks for say, 10 seconds or more before it was uncomfortable.



Should the X Stepper be getting hot too? It's about the same temperature as the sinks.



I'd really hate to rip this thing apart, but maybe I have a funky stepper? Perhaps switching X and Y would give me some love.



Are there any other ideas? Is there some sort of a debug mode? I'm going to let it finish, see where else it screws up and see if it zeros out to the same spot.

![images/8c3dd3a334b31ca731c7ec04c1ac3a0e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8c3dd3a334b31ca731c7ec04c1ac3a0e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 04:52*

Fan blowing on the drivers.  



Making a separate post here or in LW group with the original design files, LW Workspace Export, screenshot of LW, settings, and other relevant information so that others can perhaps look into your issue and see if they find anything of interest. 



Would be curious to hear the results of what you suggested - if the error is repetitive or not. 


---
**John Milleker Jr.** *July 10, 2017 05:22*

It seems so much like hardware. Especially if  I can run so many tests, number 7 fails, and I'm back to running tests and it's far past the problem area with no hiccups on copy paper again. Random issues are certainly the worst.



I'll get a fan on the Cohesion board, it'll be an AC desk fan to start. Not sure I have the life in me to rip out the steppers and switch those, but that may happen tomorrow too. I think that's all I have left to go on.



If I get past those with no love, I'll put together some support files as you mentioned and see if others can burn them.



Thanks **+Ray Kholodovsky**




---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 05:23*

I wouldn't be changing stepper motors. That's rarely the issue. Loose pulleys however are always a possibility. 


---
**John Milleker Jr.** *July 10, 2017 05:31*

That's good, I really didn't want to pull the whole system out. One of the first things I did with this machine was tighten up the belts. I like a nice sproing and tuned them to about my 3D printers' belts.



If anything, they could be too tight, but the machine has never shown any signs of trouble in movement.



Have an plastic housing AC fan in the electronics bay blowing on the cohesion right now and sending the file on the back side of one of the bad books. I'll get a 24v DC solution and make a mount for it to sit over the Cohesion. Thanks again!


---
**John Milleker Jr.** *July 10, 2017 06:23*

Got plenty of cooling directly on the Cohesion and got an immediate failure about 20cm up the piece. Put my finger on the sinks immediately and they were cool. I have a 24v fan coming same day delivery, not the issue but cooling never hurts.



You made me think of something with the belt comment. What if the belt being too tight was causing the motor to drive unnecessarily. That's could cause the stepper to heat up and maybe miss a step randomly and seemingly around the same area.



Loosened the belt a bit. Will be sending through more tests and keep an eye on the stepper heat generated.



Anyone else with a K40, after a rather lengthy job, how hot does your X stepper get on the bottom where the metal part of the motor is?


---
**Claudio Prezzi** *July 10, 2017 07:57*

Here is a video about setting the driver current correctly: 
{% include youtubePlayer.html id="watch" %}
[youtube.com - Setting the Current Limit on Pololu Stepper Motor Driver Carriers](https://www.youtube.com/watch?feature=player_embedded&v=89BHS9hfSUk)


---
**Claudio Prezzi** *July 10, 2017 08:00*

And here is a Link for the A4988: [https://www.pololu.com/product/1182](https://www.pololu.com/product/1182)


---
**John Milleker Jr.** *July 10, 2017 14:58*

That's it, perfect. Thanks **+Claudio Prezzi**!



Once I'm done another test I'll take a voltage reading from the driver and see where I'm at. 




---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/Me5oeJFJtjL) &mdash; content and formatting may not be reliable*
