---
layout: post
title: "Hi folks! We have shipment! FedEx is now in possession of a large box containing many Cohesion3D Mini's and it is headed this way"
date: January 04, 2017 17:28
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Hi folks! We have shipment!  FedEx is now in possession of a large box containing many Cohesion3D Mini's and it is headed this way. I underestimated the transit time, the ETA is currently Wednesday the 11th. We'll be ready to hit the ground running (or rather with all stepper motors at full speed ahead) and get these out to you all as soon as humanely possible. 

Thanks again for everyone's patience, I don't take it lightly. 



Cheers,

Ray





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Stephane Buisson** *January 04, 2017 18:35*

take your time, few hours make no difference to us, but relieve you from a great pressure you put on yourself.


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 18:55*

It's ok, I by no means have a normal sleep schedule. For example, this happened between 3 and 5 am "last night"![images/f3f924807bf3b59312dc7d7e86ef9bd7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f3f924807bf3b59312dc7d7e86ef9bd7.jpeg)


---
**Coherent** *January 05, 2017 21:22*

Outstanding. I hope mine will be in that batch when shipped! But take your time... Relax! Quality is more important than quickness. (Unless you're bleeding and need to get to the hospital... because then the quality of the ride may be less important).


---
**Jarrid Kerns** *January 08, 2017 01:38*

I took the plunge and ordered one. Looks like a great product. 


---
**Bill Keeter** *January 09, 2017 22:14*

Can't wait. Got the rest of my k40 upgraded. Just need this motherboard. :)



Will there be any additional documentation? May be stupid, I worry I'm going to plug somethings in backwards. Like the X and Y. 


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2017 22:36*

Sure,  there are plenty of pictures and assembly info. It will be put on our support site in time for when the boards start shipping. 


---
**Alex Hodge** *January 10, 2017 15:54*

When are you expecting to ship Ray? Looking like this week?


---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 06:15*

There will be an update on this soon **+Alex Hodge** 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/CLUXBqwHSAS) &mdash; content and formatting may not be reliable*
