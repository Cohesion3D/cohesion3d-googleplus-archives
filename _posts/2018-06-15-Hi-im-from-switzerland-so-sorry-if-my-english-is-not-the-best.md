---
layout: post
title: "Hi im from switzerland so sorry if my english is not the best"
date: June 15, 2018 16:45
category: "C3D Mini Support"
author: "pascal putzi"
---
Hi i‘m from switzerland so sorry if my english is not the best. My setup is a windows 10 gamer laptop and lightburn software. I installed it and everything was working fine. At first i run smoothie but changed it to grbl because i‘m more used to it from my other laser. After some testing and calibrating everything was fine and the first test cut with a small rectangle i was ready to go and happy but after a while it lost connection. It was not running any jobs or something i only had lightburn open and connected with the machine and suddenly lost it. now i cant connect to the board and the sd card is unusable. I tried to upload the firmare again, both the original and grbl but it doesnt read the new or the old card. The new one was formated correctly to Fat32 but there wasnt even a change in the smoothie file. The green leds dont blink for even a second. Just the VMOT and 3V3 are blinking in sync with the power supply. The old m2 nano board works fine without a problem.         Thanks in advance for every help!



![images/2ab06ce0ae6f0ec721e2601082c18585.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ab06ce0ae6f0ec721e2601082c18585.jpeg)
![images/628979a7b45931bd644385ccc24f9fee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/628979a7b45931bd644385ccc24f9fee.jpeg)
![images/5ec3f8d8b372abc2c73fbee8b95fe93b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ec3f8d8b372abc2c73fbee8b95fe93b.jpeg)
![images/3979f8f8e811fd2c19874ad14273506b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3979f8f8e811fd2c19874ad14273506b.jpeg)
![images/734299b68b5b52275f1e2b3494aad4db.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/734299b68b5b52275f1e2b3494aad4db.jpeg)
![images/3923d9a0856750844c5a2a34b9979948.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3923d9a0856750844c5a2a34b9979948.jpeg)

**"pascal putzi"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 17:01*

Power off. Disconnect the mini from the LPSU (in particular remove the large power plug from the mini). Turn it on again, does the green led on the psu still fade the same way? 


---
**pascal putzi** *June 15, 2018 17:14*

The green led only fades away when it is connected to the mini. On the old m2 nano board is everything ok. 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 17:17*

Interesting. It is possible that your lpsu is more underpowered than usual.  We do recommend installing a separate “led style” power supply like a 24v 6a to power the board and motors while the original one just drives the laser tube. 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 17:17*

[plus.google.com - I want permit the K40 power supply to only be tasked to power the K40 laser a...](https://plus.google.com/117720577851752736927/posts/emzYWQAAXuW)


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 17:19*

I think the first step should be to power the mini separately from 12/ 24v to verify that you can get “brain activity” from it. 


---
**pascal putzi** *June 15, 2018 17:23*

**+Ray Kholodovsky** hm ok but earlyer everything was working fine and i didnt do more than turning the laser on/off, connecting/disconnecting the usb cable and do some homing calibration. You think when i add extra powersupply then the board will read the sd card again? Or is the board now destroyed?


---
**pascal putzi** *June 15, 2018 17:24*

By the way the old sd card isnt readeable anymore


---
**pascal putzi** *June 15, 2018 20:13*

Ok so now i powered the mini board seperatly but the results are the same. VMOT and 3V3 are still blinking and dont respond with any sd card and firmware. 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 20:15*

Please show me that, how you wired the separate power and how it is behaving now. 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 20:16*

And is the board still mounted inside the machine or removed now? 


---
**pascal putzi** *June 15, 2018 21:19*

Red is 12/24v and black is gnd![images/86895782f28d0e0a858248595318cc13.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/86895782f28d0e0a858248595318cc13.jpeg)


---
**pascal putzi** *June 15, 2018 21:20*

Outside laser![images/e8ccf60341f8c2d16ff8a4d53bc0dffb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e8ccf60341f8c2d16ff8a4d53bc0dffb.jpeg)


---
**pascal putzi** *June 15, 2018 21:21*

VMOT and 3V3 blinking![images/e2d9dbf4c6dc9500f8aa5600d2716e46.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e2d9dbf4c6dc9500f8aa5600d2716e46.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 22:01*

Please send some more pictures of the board.  I am trying to see if any of the components under the green driver modules look damaged. 


---
**pascal putzi** *June 15, 2018 22:17*

**+Ray Kholodovsky** ![images/712a5806be2903f7dc017c84d6e7a5ab.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/712a5806be2903f7dc017c84d6e7a5ab.jpeg)


---
**pascal putzi** *June 15, 2018 22:18*

![images/cbcb4c609e0523c2dc9a4c88e453bef6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cbcb4c609e0523c2dc9a4c88e453bef6.jpeg)


---
**pascal putzi** *June 15, 2018 22:18*

![images/58b61ec071359bc8f37326150a126168.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/58b61ec071359bc8f37326150a126168.jpeg)


---
**pascal putzi** *June 15, 2018 22:18*

![images/1b55b5b62e25c83e6120e790f7eb5a3e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b55b5b62e25c83e6120e790f7eb5a3e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2018 02:45*

Your board appears to have experienced damage - there are physical scars on some of the chips. 



I think you should carefully inspect the wiring in your machine. Either something is up with your power supply, or you might for example have a loose AC wire that is contacting the chassis. 



I personally tested your board before shipping, and I do not believe this level of failure could have happened on its own. 



Please check your machine carefully. 


---
*Imported from [Google+](https://plus.google.com/116309494638777099365/posts/3YH1ebQb3tE) &mdash; content and formatting may not be reliable*
