---
layout: post
title: "Where can I purchase the Cohesion 3D board bundle?"
date: November 01, 2018 12:52
category: "K40 and other Lasers"
author: "Ashra Campbell-Clarke"
---
Where can I purchase the Cohesion 3D board bundle? 





**"Ashra Campbell-Clarke"**

---
---
**Tech Bravo (Tech BravoTN)** *November 01, 2018 15:59*

[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2018 16:04*

You can also get it on eBay if you want to pay more for no reason. 


---
*Imported from [Google+](https://plus.google.com/114192144580399794033/posts/RfppbgkJEpT) &mdash; content and formatting may not be reliable*
