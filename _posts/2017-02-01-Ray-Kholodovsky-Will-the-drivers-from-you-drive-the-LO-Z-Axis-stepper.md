---
layout: post
title: "Ray Kholodovsky . Will the drivers from you drive the LO Z-Axis stepper?"
date: February 01, 2017 21:02
category: "General Discussion"
author: "Tony Sobczak"
---
**+Ray Kholodovsky**. Will the drivers from you drive the LO Z-Axis stepper?





**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 01, 2017 21:14*

Looks like they include a 2 amp motor with that. A4988 may not be able to drive it at full power.  It's a grey area...


---
**Russ “Rsty3914” None** *February 01, 2017 23:56*

I would definitely suggest external stepper driver...I just bought the same a table and it takes alot of power to move it..


---
**John Sturgess** *February 02, 2017 07:15*

Please don't power it off the normal laser psu, this is only rated for 1 amp IIRC. You'll need a 2nd 24v psu to power the C3D mini and steppers. 


---
**Tony Sobczak** *February 02, 2017 16:24*

I've got an external driver, how do I wire it to the card? Also have a 24v supply so that will be no problem using the LPS. 


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 16:56*

Please consult the pinout diagram to find the step dir and enable signals in the socket. You can break those out using jumper wires. 

You'll want to disconnect the laser power supply 24v and gnd from the board, and use the main power in terminal to connect your new 24v psu 


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 16:57*

You should also make sure there is a common ground wire between the 2 power supplies. But I don't want that going thru the board, hence, run a wire. 


---
**Tony Sobczak** *February 02, 2017 19:48*

God I don't know how I missed the labeling.  Must have been have asleep.


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 19:54*

There's a section in the smoothie wiki about how to wire external steppers.  It's more than 3 wires, you also need to do ground for all the - sides if there is a + and - for each signal. In short, find some instructions :)


---
**Tony Sobczak** *February 02, 2017 20:59*

Thank you for your time. 


---
**John Sturgess** *February 03, 2017 10:22*

**+Ray Kholodovsky** As i'm powering my C3D from a 10amp 24V PSU can i use a 8825 driver on my C3D Mini to drive the LO Z table?


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 17:00*

**+John Sturgess** you can try. Be mindful of the orientation of the driver so you don't burn the board. And it will need a lot of cooling. 


---
**Russ “Rsty3914” None** *February 17, 2017 15:42*

When are those boards shipping ?  " Mini 3d ?" 






---
**Ray Kholodovsky (Cohesion3D)** *February 17, 2017 15:43*

Timeframe is end of the month. I'll make a post once the assembler ships them to me, that will be more certain. 


---
**Russ “Rsty3914” None** *February 20, 2017 15:43*

Okay thanks !








---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/XtQk4SRmeoi) &mdash; content and formatting may not be reliable*
