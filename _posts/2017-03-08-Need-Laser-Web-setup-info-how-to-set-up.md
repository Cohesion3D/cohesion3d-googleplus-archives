---
layout: post
title: "Need Laser Web setup info - how to set up"
date: March 08, 2017 17:11
category: "C3D Mini Support"
author: "Allen Russell"
---
Need Laser Web setup info - how to set up





**"Allen Russell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 17:13*

[github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki)


---
**Allen Russell** *March 08, 2017 17:20*

I have laser web installed I need info that has to be filled in the blanks


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 17:22*

[cohesion3d.freshdesk.com - LaserWeb Configuration : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726545-laserweb-configuration)


---
**Allen Russell** *March 08, 2017 20:18*

Ran into problem - RED Light on board, will not go to home

![images/2e9712426e11bad51e62ec414057eb8d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2e9712426e11bad51e62ec414057eb8d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 20:20*

Red light means power on.  Good!

Homing - what are you doing, and what happens? 


---
**Allen Russell** *March 08, 2017 20:21*

setup laser web and tried to home the machine but nothing happens


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 20:26*

Spell it out for me:  "I did <i>_</i>, I expected <i>_</i> to happen, but instead <i>__</i> did/ did not happen.

So far I have no details to help me. 


---
**Allen Russell** *March 08, 2017 20:29*

did basic setup, after that I hit the home button, nothing happened


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 20:31*

Type G28.2 into terminal box at the bottom of the LaserWeb window and press enter. 


---
**Allen Russell** *March 08, 2017 20:42*

nothing happened


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 20:45*

Can you jog around using the arrows?

Are you connected (top bar)?


---
**Allen Russell** *March 21, 2017 22:37*

yes, I can jog with arrows, but when I hit the home button it grinds in the front left corner and try's to keep going so I have to shut it down to stop. Also have GLCD  working but don't know how to use it. 


---
**Ray Kholodovsky (Cohesion3D)** *March 21, 2017 22:39*

If when you home it is moving towards you (to the front) then your Y motor is reversed.  Power down completely including unplugging USB and flip the orientation of the Y motor cable. 


---
**Allen Russell** *March 21, 2017 23:00*

Great that did it, how do you set to cut? is there something I can read. Also how do you use the glcd screen


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/5pKsNzDhKvA) &mdash; content and formatting may not be reliable*
