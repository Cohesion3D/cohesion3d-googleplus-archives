---
layout: post
title: "I'd like to start a repository of benchmark laser jobs that new users can run"
date: February 13, 2017 02:26
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I'd like to start a repository of benchmark laser jobs that new users can run. 



So, if you're up and running, here's the idea.



You have a job that you ran.  I'll use rastering a picture as an example.   

You have a picture of a boat that you engraved using LaserWeb with some DPI, beam diameter, and power settings.  You share those values/ settings and show a picture of the outcome.  You share the raw image, settings, and the gCode file. 



We host a repository of such examples with the files accessible. 



A newcomer is having difficulty dialing in their machine.  They navigate to this repository.  First, they run the image in LaserWeb with their existing settings, whatever those might be.  Then they adjust settings to match yours and run the job again.  Finally, they can run the gCode file you provided.  Now they have a before, in the middle, and after set of engravings they can use to compare quality and performance.  



Please contribute by sending a zip file with all that type of information:

-raw image file 

-gCode output file

-a text description of DPI, beam diameter, power values, what material was used

-a screenshot of the entire LaserWeb window so people can see the actual job setup values.



Email files to info (at) cohesion3d (dot) com





Feedback welcome.

Thanks,

Ray





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Don Kleinschnitz Jr.** *February 13, 2017 13:02*

The repository is a good idea. I notice that we go through a lot of the same questions when trying to bring a new user on board or to troubleshoot a problem. It also would be great for research reasons to know what configurations are working and what are not. 

1. Can we add a copy of the config file. That is where we can see things like the PWM period.

2. Will the "Save Workspace" in LW 4 save all the settings. Otherwise there is a lot of screenshot-ing to do to get all the setting including the CAM Settings for hybrid jobs (cut and engrave)? If its to tedious folks won't do it.

3. Have we considered using Thingiverse, you can upload a design and also the supporting files. I searched and some have already put engravings there. 

May need some guideline like above as to what files to upload and what tags to put on the file. I assume you can upload all the needed file types. 


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:17*

1. Config is concerning. I don't need people thinking configs are interchangeable. We might have people with other boards (like yourself) contributing, and it's a safety concern. I do agree that the PWM period is an interesting data point to have. But that's it. Everything else is individual specific. 

2. Ask in LW group, they might know :)

3. Thingiverse is crowded. I think github is the way to go. I might even be able to make a nice little web page - like a gallery with links - hosted right from within the project. 

Since I can't expect people to know how to work a pull request, they can just email stuff to me. 



Regarding which files - well there are cuts and engravings. So whatever format applies to the vector or image file. 

It comes down to source file, gCode output file, and "way to convey settings". We can ask, hey - what PWM period are you using - there or in a separate poll. With PWM period I just tell people it could be 20 - 200 - 400. Go fiddle. Real scientific, I know. 


---
**Don Kleinschnitz Jr.** *February 13, 2017 13:28*

**+Ray Kholodovsky** 

1. To late people already think configs are interchangeable. I often find that people just use anothers config. to get started, I did. I often work problems back to an incorrect settings. Are you only putting this repository up for C3D? Perhaps I am trying to get to much out of your idea.

2. I can test later if we do this

3. I don't see it as more crowded than any repository would be, search works great to sort to a category. 

Git is too intimidating to most TechNot's, your call if you are going to do all the upload download maintenance. Most people in this space are familiar with Thingiverse and BTW would give your board a huge audience.

............................

Ultimately your idea, your call :).


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:34*

1. No, it's for everyone. I don't care what board or software you use (although LaserWeb would make it easier :) ). Regarding configs, yeah I got started with one of your guys' configs too. But from my perspective that will cause problems for a less tech savvy user that may not realize the safety implications of randomly loading config files from "sources" 

2. Yep. Need some stopgap for sharing settings  while LW3 is main/ remains in use 

3. I see your point about thingiverse. Ok let's do that. Upload to thingiverse and send me a link. Maybe I can make a github backed gallery webpage (so it looks like a normal website with url and everything) to showcase some of them, and that could also hold the list of thingiverse links. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/5Kb8zcC6uTP) &mdash; content and formatting may not be reliable*
