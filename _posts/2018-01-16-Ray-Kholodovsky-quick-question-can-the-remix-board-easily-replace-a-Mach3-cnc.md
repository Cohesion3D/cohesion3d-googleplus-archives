---
layout: post
title: "Ray Kholodovsky quick question, can the remix board easily replace a Mach3 cnc?"
date: January 16, 2018 22:18
category: "C3D Remix Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky** quick question, can the remix board easily replace a Mach3 cnc?.

A friend has just bought 2 cnc machines but they run the Mach3 software and he wants something a bit easier to use so I suggested cnc web along with your board. If this has already been done could you point me in the right direction please buddy.



This is what he has at the moment and I'm guessing it would be possible.

![images/33c53a484853875602ccfe8d3d71b4bb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/33c53a484853875602ccfe8d3d71b4bb.jpeg)



**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 22:22*

I'd need clearer and more pics to be able to figure out where everything is going. 



But fundamentally, that board is just a breakout for external stepper drivers. So with our external stepper adapters should be possible. And Smoothie can control a spindle as well, again depends on how the one in here is controlled.  


---
**Andy Shilling** *January 16, 2018 22:25*

These are what I have for now.



![images/7a62761b907111a0846422d42916de8a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7a62761b907111a0846422d42916de8a.jpeg)


---
**Andy Shilling** *January 16, 2018 22:25*

![images/18d993508efa5134de21e1b561742580.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/18d993508efa5134de21e1b561742580.jpeg)


---
**Andy Shilling** *January 16, 2018 22:26*

![images/0c878891002c4f649fae47ae2cf6e378.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0c878891002c4f649fae47ae2cf6e378.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 22:27*

Yep, those are external steppers. With a C3D and external stepper adapters you'll be able to make the machine move :) 


---
**Andy Shilling** *January 16, 2018 22:29*

Awesome, What are the external stepper adapters are they on your site?




---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 22:30*

Yes... 


---
**Andy Shilling** *January 16, 2018 22:30*

It's ok I found them. We WILL be in touch my friend.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/DDWsj7QB8bU) &mdash; content and formatting may not be reliable*
