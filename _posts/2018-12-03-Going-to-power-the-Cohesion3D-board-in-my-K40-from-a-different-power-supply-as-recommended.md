---
layout: post
title: "Going to power the Cohesion3D board in my K40 from a different power supply (as recommended)"
date: December 03, 2018 00:14
category: "K40 and other Lasers"
author: "Reini Grauer"
---
Going to power the Cohesion3D board in my K40 from a different power supply (as recommended).  Is 3.7A on the 24v rail enough?  I'd love to get this dual voltage one so I can power my laing DDC pump for the cooling loop and additional fans/lighting. 

 Thanks!  [https://www.amazon.com/MEAN-WELL-RD-125-1224-Supply-Output/dp/B005T6HQ3O/ref=sr_1_4?ie=UTF8&qid=1543794763&sr=8-4](https://www.amazon.com/MEAN-WELL-RD-125-1224-Supply-Output/dp/B005T6HQ3O/ref=sr_1_4?ie=UTF8&qid=1543794763&sr=8-4)



The board will also be running a Z height table that is using a 2A stepper (42HSM03) but I doubt I'd be running all 3 steppers at the same time.







**"Reini Grauer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 16:55*

I'd keep them separate, but that's just me, and I'd get a dedicated 6 or 10 amp 24v psu for the board and external drivers to be safe. 


---
*Imported from [Google+](https://plus.google.com/107615990717524166780/posts/enCG8swJ1FZ) &mdash; content and formatting may not be reliable*
