---
layout: post
title: "Hello All, New to K40 laser with Cohesion3D board"
date: July 20, 2017 18:18
category: "General Discussion"
author: "Brian Ching"
---
Hello All,

New to K40 laser with Cohesion3D board. all seems to be installed correctly and after some minor tweaks, I was able to get an image engraved. All looked good except the image was a mirror of what it was supposed to be ie the text was backwards. A Google search indicated that to switch/mirror the x axis, but I have been unable to find where to do that.



Any help would be greatly appreciated!



Thanks,



Brian





**"Brian Ching"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 18:19*

Send a G28.2 command.  Does the head move to the rear left? 

If it moves a different direction then you need to flip the motor connector for that axis 180 degrees with power off. 


---
**Brian Ching** *July 20, 2017 18:27*

Thats awesome, thanks! Sorry for the noob question but thanks for the quick response!




---
**Brian Ching** *July 20, 2017 18:43*

Success!!!!![images/467c5fd007cc6b4d04286e3666e06f49.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/467c5fd007cc6b4d04286e3666e06f49.jpeg)


---
*Imported from [Google+](https://plus.google.com/108079990870910423382/posts/AKTtiEHb2sf) &mdash; content and formatting may not be reliable*
