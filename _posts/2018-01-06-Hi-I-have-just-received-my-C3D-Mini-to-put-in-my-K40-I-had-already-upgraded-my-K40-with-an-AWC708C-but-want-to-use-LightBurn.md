---
layout: post
title: "Hi, I have just received my C3D Mini to put in my K40 (I had already upgraded my K40 with an AWC708C but want to use LightBurn)"
date: January 06, 2018 15:51
category: "C3D Mini Support"
author: "Thomas Kirk"
---
Hi, I have just received my C3D Mini to put in my K40 (I had already upgraded my K40 with an AWC708C but want to use LightBurn).  I have got it all wired up and can test fire the laser from the LCD menu however the steppers don't seem to be moving.

I am using external M415B Drivers which were working on the AWC708C.  I have wired the DIR, PUL & ENA up and took the 5v from the PSU to the OPTO input.  

I have measured with a multimeter and have the following voltages between OPT and:

ENA: 4.6v

DIR: 1.7v

PUL: 4.6v



Am I doing something wrong?





**"Thomas Kirk"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 06, 2018 16:03*

Check the z table guide for an example of config at Cohesion3D.com/start [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Thomas Kirk** *January 06, 2018 16:26*

I think I have solved the stepper problem, it would appear that the enable pin is inverted so I have put a ! after the pin definition in the config file.



The next problem I can see now is that I can test fire the laser from the LCD menu but a job from LightBurn starts the head moving in the right positions but the laser doesn't fire.



I have saved the job to GCode and see that the GCode sends command M4 and not M3 for laser on.



Should I just change the config line "switch.laserfire.input_on_command M3" to M4 or should I change something in LightBurn?




---
**Thomas Kirk** *January 06, 2018 22:44*

Managed to figure it out, in LightBurn I had set the board up to Cohesion3D (GBRL) and should have set it to Cohesion3D (Smoothie).  It seems to be working now although I haven't done any major testing yet, tomorrow I will be sorting the extract out so that I can test properly


---
**Ray Kholodovsky (Cohesion3D)** *January 07, 2018 00:28*

The pins needed to be set as !o (the invert ! needs testing to determine if needed) the o is for open drain. 

Which is why I referred you to the guide. The way you are wired you may realized later that without the o you are not moving properly/ the expected distance 


---
**Thomas Kirk** *January 07, 2018 13:00*

ah I will update my config.  I haven't done any major testing yet so not noticed if it's missing steps. 


---
*Imported from [Google+](https://plus.google.com/110384747776217748025/posts/JFZKU4mkvoj) &mdash; content and formatting may not be reliable*
