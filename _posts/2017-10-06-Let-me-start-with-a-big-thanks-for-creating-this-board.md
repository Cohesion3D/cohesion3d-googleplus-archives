---
layout: post
title: "Let me start with a big thanks for creating this board!"
date: October 06, 2017 18:12
category: "Thank you and other tips"
author: "Jody Roth"
---
Let me start with a big thanks for creating this board!  It has renewed my interest in my K40. Providing the software on the SD is a big help.  My first project was a new panel for the GLCD, etc.  While LaserWeb is a little rough, it is far better than MoshiDraw and I really appreciate being able to design once then raster and engrave in the same job!



I received my Mini and replaced an MS10105 v4.1 which had a 6 pin power/laser connector.  The setup instructions were not very clear on what to do with this specific situation but I eventually figured out where to move the 24v/G/Laser wires.  It might help to add a note on the setup page explaining that these go to Main Power In and P2.5 and clarify that the extra gnd that was paired with the Laser wire doesn't need to be connected. (does it?).  I've attached a pic of the board and connector I had.  If the MOSFET screw connectors were moved over a few millimeters, I could have moved the wires closer together and reused this connector.  Instead, I had to cut the wires and use the appropriate screw connector.  And until I managed to get everything working I was worried that I had just made it very difficult to go back to the original board.  



There's also a picture of my y-axis connection, which always goes on backwards the first time, eh?  



Also want to say that shipping, packaging, and condition were all great!  Keep it up!



![images/b5ce6a51926812af1c09d850e656fa26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b5ce6a51926812af1c09d850e656fa26.jpeg)
![images/3320e9a22163d6f8fa90c907d74e09dd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3320e9a22163d6f8fa90c907d74e09dd.jpeg)
![images/feab0ab209ad01697fc0a5088c7af261.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/feab0ab209ad01697fc0a5088c7af261.jpeg)

**"Jody Roth"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 05:17*

Glad you like it. Is that a Moshi board? 



When you send G28.2 homing, your head should move left back. Assuming this is where your switches are. If not, power down and flip the cable. 



Well noted, I think you may have realized how many times I have typed the bit about 24v, God, and P2.5. Any suggestions on where in the instructions that would make the most sense? 


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 18:33*

Gnd*


---
**Jody Roth** *October 10, 2017 23:45*

I think you could add something after the last image, i.e.



<b>Another possible wiring configuration with a 6-pin or other power connection.</b> Your setup may require routing the 24v/Gnd wires from the power supply to the (Main Power In) Vin screw terminals and the Laser wire to the (P2.5/BED) FET1bed minus terminal. Identify the wires by using the markings from the existing board as a guide or by tracing the wires from their source.



![images/22c83c986cf5a00800751dabac444dbb.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/22c83c986cf5a00800751dabac444dbb.png)


---
*Imported from [Google+](https://plus.google.com/107797353699813204127/posts/JhLrU2nwoaE) &mdash; content and formatting may not be reliable*
