---
layout: post
title: "I see the C3D Mini has a 5V input pin"
date: August 13, 2018 03:24
category: "K40 and other Lasers"
author: "Exploding Lemur"
---
I see the C3D Mini has a 5V input pin.  Is there a way to disable the onboard 24V->5V regulator and use the K40 power supply's 5V output to power the C3D, and is there value in doing so?  (I thought it might be better to reduce the load on the K40's 24V output so it can just power the steppers)





**"Exploding Lemur"**

---
---
**Tech Bravo (Tech BravoTN)** *August 13, 2018 03:32*

no. ultimately the load on the k40 psu wouldn't be any more balanced even if it was possible. the 24 volt in on the C3D Mini supports the on board drivers, etc... the best solution is to get the external power supply offered on the Cohesion3D website.




---
**Joe Alexander** *August 13, 2018 03:57*

the amount of draw from the 24v to the regulator is negligible and waay more reliable than the 5v output from the laser PSU. i would leave it as is.


---
**Exploding Lemur** *August 13, 2018 04:46*

Ok, thanks!


---
*Imported from [Google+](https://plus.google.com/106334870053586601668/posts/fGobaP72ydH) &mdash; content and formatting may not be reliable*
