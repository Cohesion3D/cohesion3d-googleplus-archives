---
layout: post
title: "Will this board work with any other laser cam software other than laserweb?"
date: July 08, 2017 23:26
category: "C3D Mini Support"
author: "laurence champagne"
---
Will this board work with any other laser cam software other than laserweb?

Anyone have a list or recommendations? I'm willing to pay to get away from laserweb.





**"laurence champagne"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 23:28*

Anything that makes gCode.



Visicut, and any CNC CAM software that makes gCode. 



[smoothieware.org - software [Smoothieware]](http://smoothieware.org/software)


---
**Anthony Bolgar** *July 09, 2017 01:31*

Fusion360 will generate compatible gcode.




---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/TqhWCbGgUWX) &mdash; content and formatting may not be reliable*
