---
layout: post
title: "First off Thanks to Ray Kholodovsky. Got my new Mini today and it works great"
date: June 03, 2017 21:19
category: "General Discussion"
author: "Richard Gallatin"
---
First off Thanks to Ray Kholodovsky. Got my new Mini today and it works great. I am trying a photo but it seems to jerk some when I run it. I have got the GLCD hooked up now and I am not sure how to save and load a GCODE from the micro SD. Also there is a full size SD card on the GLCD does that work or do I need to do it on the mini. 





**"Richard Gallatin"**

---
---
**Joe Alexander** *June 03, 2017 21:56*

I believe you can run them from the sd card on the GLCD but make sure that slot is empty on boot up. I have heard some have had issues with the firmware not loading right if a card populates the screen's slot on bootup.


---
**Don Kleinschnitz Jr.** *June 03, 2017 22:18*

Try these:

[smoothieware.org - player [Smoothieware]](http://smoothieware.org/player)

[http://smoothieware.org/printing-from-sd-card?s[]=glcd](http://smoothieware.org/printing-from-sd-card?s%5B%5D=glcd)



"WARNING using the external sdcard for printing from is not recommended, and is NOT supported. Running SPI over long (or even short) cables is problematic, and can cause random hangs and/or corrupted data. I am not aware of a way to fix this other than using differential buffer drivers."



[http://smoothieware.org/panel?s[]=sd&s[]=card&s[]=glcd](http://smoothieware.org/panel?s%5B%5D=sd&s%5B%5D=card&s%5B%5D=glcd)




---
**Ray Kholodovsky (Cohesion3D)** *June 03, 2017 23:55*

The full size SD is not even wired to the Mini,  so one would put the files onto the MicroSD Card in the Mini.  

The 2 ways to do this are either taking the card out and adding the gCode file to it via PC memory card reader... 

Or when you plug the Mini in to the PC over USB Cable...  it'll show up as a drive.  

Then you can use the play command. or the menus on the GLCD to run the file.  


---
**Richard Gallatin** *June 04, 2017 03:04*

OK Thanks I got that to work. Now my next question can I load more than 1 Gcode file on the SD? Also can i give them different file names? 


---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2017 03:05*

Sure and yes. 


---
**Richard Gallatin** *June 04, 2017 03:06*

Thank you Ray for taking the time with me. I know a bunch of the same questions. 




---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2017 03:07*

All good. I need to find a good edition of the answers and add them to the FAQ. 


---
**Joe Alexander** *June 04, 2017 03:09*

im slowly adding them to the spreadsheet in my google drive, thought of a couple more earlier.


---
**Richard Gallatin** *June 04, 2017 03:11*

Well here is another. Only printed 1/8 of picture then stopped now I will not find the USB port. 




---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2017 03:14*

Need picture, screenshot of LW, more details.

If you are running from the MicroSD Card then you do not need USB connected. 

What is the state of the LEDs on the board? 


---
**Richard Gallatin** *June 04, 2017 03:25*

I got it all reset. I am trying again. 


---
**Richard Gallatin** *June 04, 2017 03:33*

Seems to work I will try another picture tomorrow getting late. I am doing just a black  and white logo easy. 


---
**Richard Gallatin** *June 04, 2017 13:33*

Ok so I ran it from micro sd. In LW I have cut rate at 8500 mm/min. Laser diameter at .085. All other settings at default. I have no control over [GLCD.Hard](http://GLCD.Hard) to see looks like L1 solid green and L2 Soild green that is it

![images/ef5bf8a2154f5d24c0b2603b3aa6681f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef5bf8a2154f5d24c0b2603b3aa6681f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2017 13:36*

Try setting the diameter at 0.1 and try again, let me know if the issue persists. 


---
**Richard Gallatin** *June 04, 2017 13:36*

My results start diagonal then went to line, then just stops with 24v red led on and looks like l1 and l2 green led solid. 

![images/83e8ed61594a7801fabde95f9fda3993.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/83e8ed61594a7801fabde95f9fda3993.jpeg)


---
**Richard Gallatin** *June 04, 2017 13:49*

Same thing Ray. Here is my results. Started diagonal. Went back a little to redo diagonal then printed solid line then moved down and statered side to side. 

![images/48998319f16778de0899f6402ffa1ce8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/48998319f16778de0899f6402ffa1ce8.jpeg)


---
**Richard Gallatin** *June 04, 2017 14:01*

Generated GCode. Lose picture and screen grid goes white

![images/645527882b60ede812af4fc6bc499693.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/645527882b60ede812af4fc6bc499693.jpeg)


---
**Richard Gallatin** *June 04, 2017 17:28*

OK got it figured out with alot of help from Ashley. She was huge help. Seems I was not waiting for all the GCODE to transfer to SD before I started printing. 




---
**Ashley M. Kirchner [Norym]** *June 04, 2017 17:35*

<storytime>

Several years ago, while looking up my finish stats on a 10k race, I realized there were two "Ashley Kirchner" listed. One was listed as 'F' and the other as 'M'. I had the faster time by about 20 minutes. But it peaked my interest ... 'Who is this Ashley girl, and why'd she steal MY name!" Turns out, SHE lived about 10 minutes away, and is a high school diving champion.

</storytime>


---
*Imported from [Google+](https://plus.google.com/102728020130316654078/posts/NfFZHVstmqm) &mdash; content and formatting may not be reliable*
