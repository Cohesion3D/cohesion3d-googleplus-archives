---
layout: post
title: "Hello everyone, who has experience, two motors on one axle"
date: October 06, 2018 13:51
category: "C3D Mini Support"
author: "Alex"
---
Hello everyone, who has experience, two motors on one axle. I know the physical connection, I am interested in the solution programmatically. I need two motors on the Y axis



Best rgs

Alex





**"Alex"**

---
---
**LightBurn Software** *October 06, 2018 18:40*

I recall the Smoothieware developers saying that because it was trivial to connect multiple motor drivers to a step/dir output, or connect multiple motors to a single driver, that they intentionally did not support tying two outputs to the same axis. So it’s likely not possible in the software without altering it.


---
**Roberto Fernandez** *October 06, 2018 19:34*

I have a machine with 2 motors on Y axe. If the curren of your 2 motors are minus of 2 amperes, can be connected both on the same connector. It is not the best solution. 

For the second motor you can use one of the free steppers of your board and use it for the second motor. In this case you have 2amperes for each motor.



There is documentation about this in the github smoothieboard web.



See my jumpers.

![images/2c20c141a0622c460a2aa78f2efa8488.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2c20c141a0622c460a2aa78f2efa8488.jpeg)


---
**Alex** *October 10, 2018 04:21*

Hi everyone, I found a software solution:



[https://medium.com/home-wireless/dual-axis-motors-with-smoothieware-b253be848fbc](https://medium.com/home-wireless/dual-axis-motors-with-smoothieware-b253be848fbc)


---
*Imported from [Google+](https://plus.google.com/106697873351656319086/posts/XhfnMgypno9) &mdash; content and formatting may not be reliable*
