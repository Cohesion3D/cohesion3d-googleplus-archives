---
layout: post
title: "Originally shared by Marc Pentenrieder Ray Kholodovsky Hi, my large printer project is making it's first moves ;-) More to come"
date: July 10, 2017 20:58
category: "Show and Tell"
author: "Marc Pentenrieder"
---
<b>Originally shared by Marc Pentenrieder</b>



**+Ray Kholodovsky**​

Hi, my large printer project is making it's first moves ;-)

More to come.





**"Marc Pentenrieder"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 21:01*

I like the counterweight. How about small moves in Z - can you make single layer height steps precisely? 



Are the z servos calibrated now? 



I would build one of these except I don't have a spare room available :) and if I could I would probably need to get a pick n place first. 


---
**Marc Pentenrieder** *July 10, 2017 21:14*

Hi **+Ray Kholodovsky**​, yes I can make precise single layer moves.

I have 2000 steps per mm and a deviation of 3/100mm when moving 1 meter.

All servos should be calibrated now, as they ae the same as z axis.

I am so glad everything is working now. I have to order cable chains now, to get my cables sorted. The x-axis is already ready built, I only have to put it in, when testing of y-axis is ready and cable chains are here.


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 21:14*

Glad to hear it. Looking forward to seeing more of this machine. 


---
**John Milleker Jr.** *July 12, 2017 20:49*

Is this going to be a 3D printer? Laser? Or both? :)




---
**Marc Pentenrieder** *July 12, 2017 21:13*

It is going to be a 3d printer in first place, but i will also use it as a laser, mill ....


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/gyrkMfKHKjx) &mdash; content and formatting may not be reliable*
