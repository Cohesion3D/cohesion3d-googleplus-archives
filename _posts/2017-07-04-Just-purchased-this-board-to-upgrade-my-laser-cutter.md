---
layout: post
title: "Just purchased this board to upgrade my laser cutter"
date: July 04, 2017 19:48
category: "K40 and other Lasers"
author: "Todd Mitchell"
---
Just purchased this board to upgrade my laser cutter.  It has the same m2 nano as the k40.  From viewing the posts, it looks like this does not let you control the output power of the laser so I'm left to use the on-board controls to vary the laser power.



If this is correct, are there any plans support this?  I have the HY-T50 Co2 Laser Power Supply.





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 19:50*

The board does indeed control the laser power, but it doesn't replace the pot. You keep the pot, set the max power on that (like 10-13mA is good) and then the board will set a % of that by pulsing the L wire. Just follow the instructions to install, [cohesion3d.com/start](http://cohesion3d.com/start)


---
**Todd Mitchell** *July 04, 2017 20:34*

Thanks for the prompt reply.  Glad to know I'll be able to vary the power in a single cut.  Really-Really looking forward to dropping LaserDRW


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/RqNBVJBBM8h) &mdash; content and formatting may not be reliable*
