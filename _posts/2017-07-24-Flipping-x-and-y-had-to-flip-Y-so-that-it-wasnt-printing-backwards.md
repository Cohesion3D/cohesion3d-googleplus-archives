---
layout: post
title: "Flipping x and y - had to flip Y so that it wasn't printing backwards"
date: July 24, 2017 19:45
category: "FirmWare and Config."
author: "timne0"
---
Flipping x and y - had to flip Y so that it wasn't printing backwards.  Now I have to dump stuff into the negative y area (so 0,0 is back left as home, front Y is 0,-200)



Is this flipable in software?



config in paste bin [https://pastebin.com/Qfgh4kdz](https://pastebin.com/Qfgh4kdz)





![images/8d583ab0e93f125b78eef9401de21f25.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d583ab0e93f125b78eef9401de21f25.png)



**"timne0"**

---
---
**timne0** *July 24, 2017 19:47*

config in paste bin [pastebin.com - # NOTE Lines must not exceed 132 characters  ## Robot module configurations : ge - Pastebin.com](https://pastebin.com/Qfgh4kdz)


---
**Todd Fleming** *July 26, 2017 03:21*

Try jogging down and click Set Zero


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/59GTCmS3WqZ) &mdash; content and formatting may not be reliable*
