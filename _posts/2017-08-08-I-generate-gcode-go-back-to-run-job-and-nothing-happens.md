---
layout: post
title: "I generate gcode go back to run job and nothing happens?"
date: August 08, 2017 05:27
category: "K40 and other Lasers"
author: "William Kearns"
---
I generate gcode go back to run job and nothing happens? The job runs and completes in no time at all. See attached picture.

![images/8472e3f2eb9eb9314cb230c58ef8cd28.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8472e3f2eb9eb9314cb230c58ef8cd28.jpeg)



**"William Kearns"**

---
---
**Ashley M. Kirchner [Norym]** *August 08, 2017 07:50*

Did you create the operations? Generally this happens when there are no operations assigned. This is LaserWeb specific, please post your questions on their G+ community group.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/58YhYsowGrM) &mdash; content and formatting may not be reliable*
