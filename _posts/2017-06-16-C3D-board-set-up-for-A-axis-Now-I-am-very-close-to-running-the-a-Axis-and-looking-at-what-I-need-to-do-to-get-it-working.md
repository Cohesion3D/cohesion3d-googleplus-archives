---
layout: post
title: "C3D board set up for A axis Now I am very close to running the a Axis and looking at what I need to do to get it working"
date: June 16, 2017 19:41
category: "C3D Mini Support"
author: "E Caswell"
---
C3D board set up for A axis

Now I am very close to running the a Axis and looking at what I need to do to get it working.

Do I need to change anything in the config set-up file?  

Noticed there is nothing referenced for an A axis.

There is also no reference on the GLCD when looking to jog and test the A axis.

I have connected it to the Z axis and all working good so its play time soon.

Any help would be appreciated.







**"E Caswell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 19:58*

Check the description here: 
{% include youtubePlayer.html id="UoGlDKjPGSE" %}
[youtube.com - Laserweb4 K40 Rotary Test](https://www.youtube.com/watch?v=UoGlDKjPGSE)


---
**Jim Fong** *June 16, 2017 20:03*

Rotary jog buttons are now in the latest release.  I'm going to update my video tonight with the config.txt changes needed in the latest Laserweb4




---
**E Caswell** *June 16, 2017 20:26*

Thanks **+Ray Kholodovsky** and **+Jim Fong** for the quick response. Got the latest release but had problems with second file running. so will have to try again.

I had seen the video but wasn't sure if I just downloaded the firmware bin and copy to my card or if I had to do anything in particular for the C3D

Thanks again gents I will have a look.

Interestingly while I was setting up the A and Z steppers I decided to power off the cooling pump, glad I did as the power supply cooling fan isn't running so I need to get that fixed first. :-( 

I will have to have a look through to see if I can install some temp monitoring as I wouldn't have noticed if I hadn't been doing this mod work.  :-O loads of fun the K40 isn't it. 


---
**Jim Fong** *June 16, 2017 21:32*

**+PopsE Cas** Copy the firmware.bin to the sdcard and it will flash the ARM chip the next time it resets    Use the config.txt file found in my video.  That has the rotary stuff added.   Backup your old firmware and config.txt just in case something goes wrong.  



I'll have instructions for changes to config.txt later tonight. 



Update



I updated the above video description to reflect the changes needed in the config.txt file.  The delta_steps_per_mm are now steps per degree.  


---
**E Caswell** *June 18, 2017 15:15*

**+Jim Fong** thanks for the info. However.

I have downloaded both the firmware and the config file. Deleted the old files 

Renamed the new ones accordingly and dropped them on to the sd card. 



Rebooted the laser and nothing happened.

Doesn't recognise the A axis 

Checked the config file and all axis info is in there and copied over right file over. 

Tested my axis in the z and worked fine. 

So I am presuming that it's not recognised anything with the firmware and config text update

I can't open firmware file to check it as yet but will do later hopefully. 



All other aspects of LW work with this firmware and config setup. 



Any thoughts?



Pic below of screen

![images/52160f1582882cbe676d20e060b6ef56.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/52160f1582882cbe676d20e060b6ef56.jpeg)


---
**Jim Fong** *June 18, 2017 16:34*

When it boots up it should have this version if you used my firmware.bin



Jog doesn't list A axis.   It will look like your posted image.  You can only jog from Laserweb4. Make sure you enable "machine a stage"



Settings>>machine>>machine a stage![images/b7770fff8229b3562f6ed4a2d16eb8a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b7770fff8229b3562f6ed4a2d16eb8a7.jpeg)


---
**E Caswell** *June 18, 2017 20:36*

**+Jim Fong** yep, but will still not operate from LW. Tried it numerous times now and won't Jog from LW. 

Will have Better look tomorrow now as Father's Day celebrations getting better of me.

![images/957b692b9d1d8a3ce7b8be55729a5b3c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/957b692b9d1d8a3ce7b8be55729a5b3c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 18, 2017 20:44*

Check the memory card to see if the file on it now is firmware.bin or FIRMWARE.CUR. I think it didn't flash. 


---
**Jim Fong** *June 18, 2017 20:46*

**+PopsE Cas** I will do a md5sum on the file I have to make sure the one I uploaded isn't corrupt somehow.  I'm at work so will do it when I get home tonight. 



The config.txt is configured to use the 4th "A" stepper motor socket.  



First thing to look at when you have a chance.    

Check your rotary stepper motor, it should be enabled and shaft should be locked. 


---
**Jim Fong** *June 19, 2017 02:55*

I double checked the firmware.bin at [filebin.net - Filebin :: bin n5qfsn6bjawpk6p3](https://filebin.net/n5qfsn6bjawpk6p3) 

and it is the same version running on my C3D board



md5sum

d8f3e0dad5779310120681efafad4bbb








---
**E Caswell** *June 19, 2017 09:13*

Thankyou **+Jim Fong**. I downloaded the file again this morning, copied it over and its working superb.

I had to change the steps down to 33 on the config file as mine is a 3:1 ratio.

**+Ray Kholodovsky** I had to turn the pot up on the driver as well to get it to work smoothly, but all working now thanks.

I also checked the file bin / cur and it looked right, but maybe it had not downloaded correctly. Thanks for your input.


---
**Jim Fong** *June 19, 2017 16:49*

**+PopsE Cas** I just compiled a newer firmware from the latest source.  I'll test and upload soon. 



33step/degree??  You sure that is calculated right. 


---
**E Caswell** *June 19, 2017 18:47*

**+Jim Fong** nope. I aren't sure. Hence the reason for me doing a file to see if I can calculate it correctly. I am thinking with what results I have had is it needs to be just a tad lower.

If you can advise otherwise then would appreciate it. 


---
**Jim Fong** *June 19, 2017 19:02*

**+PopsE Cas** assuming you are using 1.8degree (200 full steps), 16microstepping, 3:1 gear reduction 



(200x16x3)/360 = 26.666 steps/degree






---
**E Caswell** *June 19, 2017 19:29*

**+Jim Fong** 

 I was trying to find out my data for my stepper as it was one I had in for another planned job but lost the info. I'm sure it's 1.8 but wasn't sure about the micro stepping as could not remember what it was. (google was my next plan of attack)

So it was a play and see what I get then work out the calcs.

But thanks for the heads up and will test run it and see what I get. 

Thanks again.




---
**E Caswell** *June 26, 2017 20:23*

See this thread for final set up

[plus.google.com - K40 rotary axis up-grade See link to files for my #K40Rotaryaxis up-grade. P...](https://plus.google.com/u/0/106553391794996794059/posts/bt7obpUNAwt)


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/g2kLamgYEnc) &mdash; content and formatting may not be reliable*
