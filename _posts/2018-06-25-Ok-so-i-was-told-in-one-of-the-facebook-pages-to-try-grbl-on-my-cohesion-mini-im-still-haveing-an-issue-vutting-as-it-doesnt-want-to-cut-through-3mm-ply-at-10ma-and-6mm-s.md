---
layout: post
title: "Ok so i was told in one of the facebook pages to try grbl on my cohesion mini, im still haveing an issue vutting as it doesnt want to cut through 3mm ply at 10ma and 6mm/s"
date: June 25, 2018 15:44
category: "FirmWare and Config."
author: "Woodys Creations"
---
Ok so i was told in one of the facebook pages to try grbl on my cohesion mini, im still haveing an issue vutting as it doesnt want to cut through 3mm ply at 10ma and 6mm/s.



Anyhow that aside they said that grbl had a lot of speed improvements and smoother running etc?



What woudl everyone suggest stick witht he stock firmware or go the grbl route?







**"Woodys Creations"**

---
---
**Jim Fong** *June 25, 2018 17:38*

There should be very little difference in vector cutting ability between grbl and smoothieware. 



On my k40, both firmware will cut through 3mm ply using 12ma and about 5-6mm/sec speed.  Depends on the quality of the plywood too. If it is good stuff, I can speed it up or drop laser power.  



If it doesn’t, I would check for dirty lenses, proper laser focus height, mirror adjustment etc. 




---
**Woodys Creations** *June 25, 2018 18:50*

ok so no point changing to grbl really




---
**Jim Fong** *June 25, 2018 19:10*

**+Woodys Creations** grbl does much faster raster engravings than smoothieware.  About 2 or 3x faster. 



Vector cutting of materials is done at much slower speeds.  Smoothieware does have a slight advantage of smoother motion for vector cutting. I don’t really see much difference between the two my cheap k40 when cutting out parts.  On a better laser with good linear rails, you would probably see cleaner cuts in sharp corners with smoothieware. 



Most of the time I use grbl. 


---
*Imported from [Google+](https://plus.google.com/103484599501128783834/posts/BJEi91qVhPJ) &mdash; content and formatting may not be reliable*
