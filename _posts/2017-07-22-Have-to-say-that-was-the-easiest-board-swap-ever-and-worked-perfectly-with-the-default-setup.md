---
layout: post
title: "Have to say that was the easiest board swap ever and worked perfectly with the default setup"
date: July 22, 2017 13:56
category: "General Discussion"
author: "Ken Purcell"
---
Have to say that was the easiest board swap ever and worked perfectly with the default setup. LaserWeb4 was as little more difficult to get working but love it so far. Now my setup works from my Mac, Windows and a VM running on the Mac. 



Just to go further, last night setup a Raspberry Pi 3 running the comm server and can now connect from any computer in my office wirelessly.





**"Ken Purcell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 22, 2017 16:08*

Glad to hear it Ken. 


---
**Travis Sawyer** *August 04, 2017 18:40*

Any tricks, Ken? Lw4 wouldn't see the module on my Pi Zero. I switched over to grbl1.1LPC and I see the connection now, but G28 won't home for me. Should I go back to stock?


---
**Travis Sawyer** *August 04, 2017 18:40*

I will say the board swap was extremely easy!!!


---
**Ken Purcell** *August 04, 2017 18:41*

I didn't think the PI Zero had enough horsepower. I can see the PI 3 slow down on complex curves.




---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 18:42*

G28.2 to home for smoothie. 



May need smoothie drivers depending on your OS. 



A new post in this C3D group or the LaserWeb group will show to someone who knows more about the grbl usage. 


---
**Travis Sawyer** *August 04, 2017 20:36*

Thanks! I will go back to smoothie and try that


---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 20:37*

I missed the part about the pi earlier. I'd try a windows computer with the latest LaserWeb4 first. You need drivers if it's below win10. 

Follow the documentation website link from the website to see how to configure LW4 settings. 


---
**Travis Sawyer** *August 16, 2017 13:50*

I changed out my pi zero for a pi 3 and I'm up and running after swapping my Y stepper cable (it was reversed).



Now to find a material list with speeds and feeds for a place to start from.



![images/5b7f9a55bb3e76ea0dc018785f688b23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5b7f9a55bb3e76ea0dc018785f688b23.jpeg)


---
*Imported from [Google+](https://plus.google.com/+KenPurcell/posts/GK6SzCXnXRx) &mdash; content and formatting may not be reliable*
