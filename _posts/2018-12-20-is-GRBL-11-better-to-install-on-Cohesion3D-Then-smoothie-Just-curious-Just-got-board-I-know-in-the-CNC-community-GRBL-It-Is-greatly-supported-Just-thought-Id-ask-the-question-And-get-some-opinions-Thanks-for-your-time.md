---
layout: post
title: "is GRBL 1.1 better to install on Cohesion3D Then smoothie Just curious Just got board I know in the CNC community GRBL It Is greatly supported Just thought I'd ask the question And get some opinions Thanks for your time"
date: December 20, 2018 21:11
category: "General Discussion"
author: "Timothy \u201cMike\u201d McGuire"
---
is GRBL 1.1 better to install  on Cohesion3D Then smoothie Just curious Just got  board  I know in the CNC community GRBL It Is greatly supported

Just thought I'd ask the question And get some opinions

Thanks for your time





**"Timothy \u201cMike\u201d McGuire"**

---
---
**Joe Alexander** *December 20, 2018 22:42*

GRBL is known to process raster engravings faster than smoothie, but doesn't work with the GLCD screen if you have that installed. Depends on what kind of work you do most, I personally use smoothie and GLCD which allows me to run it from sd card without a computer connection being required.


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/7H8ojue37oY) &mdash; content and formatting may not be reliable*
