---
layout: post
title: "New to the K40 community! have been doing a lot of work with my my eleksmaker A3 and wanted to step up to something more powerful but not spend 5k"
date: October 26, 2018 03:24
category: "General Discussion"
author: "Brent Tucker"
---
New to the K40 community! have been doing a lot of work with my my eleksmaker A3 and wanted to step up to something more powerful but not spend 5k. So glad I stumbled on this community, I love the idea of spending ALOT less but having to do some upgrades/work on your own part to get what you want. All that being said I’ve received my C3D bored and read all the articles for setting up. I ordered the bundle that also came with the power upgrade. It looks like I was suppose to get an SD card? I did not see one in the package, so I need one sent from cohesion or can I just format one and download whatever software onto them myself. 





**"Brent Tucker"**

---
---
**Tech Bravo (Tech BravoTN)** *October 26, 2018 03:30*

You can do it if you like. [https://www.lasergods.com/c3d-standard-firmware-config/](https://www.lasergods.com/c3d-standard-firmware-config/)

[lasergods.com - C3D Standard Firmware & Config](https://www.lasergods.com/c3d-standard-firmware-config/)


---
**Brent Tucker** *October 26, 2018 14:17*

Just went to [lasergods.com](http://lasergods.com) and WOW. A lot of good info in one place! I’ve slowly learned most of the information there but it took me a lot of time googling and looking at threads and you have it all complied and put the information out at a basic level. 



Quick question, I did purchase the 24v power supply but after reading more it seems like that’s only needed if your adding more accessories. If I’m not adding a Z axis do I need to do that upgrade?



Second question. What is the SD card for? Like I said I only know the A3 and this is new to me but there is no SD card for the A3. 



Thanks for your help!!!!


---
**Tech Bravo (Tech BravoTN)** *October 26, 2018 15:06*

thanks **+Brent Tucker**! i think even with the sitewide smart search i need a knowledge base type menu tree but i'm working on it. now we are soliciting people who want to create original content to be authors! and we are launching lasergods academy which will have free and paid help and tutorials, videos, etc... we even got marc Stipo and Sean Murray as instructors! they teach how to engrave photos and it is AMAZING!



So the power supply that comes in the k40 is grossly underpowered and can barely support the stock machine. if adding any extra motors and such is in the plans then, yes, an additional 24v psu is required (demanded LOL). even if you are not adding a z or a axis right now you still did the right thing. that external psu will provide good clean stable power to your machine and the performance and reliability will be a real benefit.



the MicroSD card contains the firmware and config files (for smoothie) or the firmware file only (for grbl). The card ships with a 3 axis smoothie version and the config file is already optimized for the k40 laser. this card must remain in the board for it to function. it runs off of the card.



Let me know if I can be of any more help. have fun!






---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2018 16:36*

The card is in a plastic case inside the bubble wrapped bundle with the board.  I can direct you to where the files are for it. 



So you want to use the C3D to upgrade a diode laser machine?  Those run on 12v. The power brick is 24v. The board will run fine on 24v but you’d still need a 12v supply for the diode laser itself. 


---
**Brent Tucker** *October 26, 2018 16:51*

Hey Ray, sorry if I wasn’t clear. I’m using your C3D to run my K40, which I’m still in the process of upgrading and learning before I fire it up. The only reason I even brought up my diode machine is to give background of what I’m use to. Loving the support and the community here of people sharing info, I’m not as tech savvy as I’d like to be so this is great for helping me learn.


---
*Imported from [Google+](https://plus.google.com/105472717401935675008/posts/ebh8g28KX7k) &mdash; content and formatting may not be reliable*
