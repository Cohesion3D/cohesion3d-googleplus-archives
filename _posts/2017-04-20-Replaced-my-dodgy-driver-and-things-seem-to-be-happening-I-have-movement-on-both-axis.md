---
layout: post
title: "Replaced my dodgy driver and things seem to be happening, I have movement on both axis"
date: April 20, 2017 17:54
category: "C3D Mini Support"
author: "Adam J"
---
Replaced my dodgy driver and things seem to be happening, I have movement on both axis. I'm trying to cut simple shapes on paper for now and something is confusing me, I'm sure it's something simple!



My head homes to the top left corner when I hit home in LW. I hit "set zero" in this position. However, in LaserWeb the blue circle is positioned in the bottom left. When I run a job of cutting simple shapes, the head bangs around while trying to draw the first part of the shape along the top left corner. As this happens in laserweb I see the blue circle moving to the top left, like it's cutting the shape while trying to move to the correct position. Then the rest of the job runs smoothly after this mishap.



If I set my start position on Y as 200, the job runs absolutely lovely. I'm clearly doing something simple wrong, yes, hand me the dunce cap and I'll sit in the corner :-).













**"Adam J"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 17:58*

Yep. 0,0 is the front left corner.  The switches are in the back left. So when it homes it's at 0, 200. But you orient the work file in LW with respect to 0,0. 



Not my best explanation, but this is one of the things people have the most trouble with. 

In short, we align stuff to 0,0. Just because the Chinese chose to put switches in the back of the machine doesn't change that. 


---
**Adam J** *April 20, 2017 18:15*

Ah okay, I got it. I was setting zero in the top left instead and that was messing things up then.


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 18:27*

Correct, you wouldn't do both.  Either home and have some sort of alignment jig to set your work piece to 0, 0   OR 

put your piece anywhere, jog to the lower left corner of the piece, and Set Zero there. 


---
**Adam J** *April 20, 2017 18:41*

Just another quick question, rather than start a new thread I'll ask here...



I'm cutting 3mm acrylic and I used to get through it in a single pass at about 12ma and 8mm/sec. 



Trying with the new install I'm struggling to get through it, cutting at 6mm/sec with power at 65% - My M/A gauge is going way over 20 m/a which isn't healthy, is it?


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 18:46*

That would not be healthy at all.  My question is why.  Did you wire everything per the instructions and leave the pot in play?  What is the pot set to?  If you need 12mA, well the config will limit the board to 80% by default, so we can either set that line commented out, or you can set the pot to 15mA and set a power of 80% in LW.  


---
**Adam J** *April 20, 2017 18:55*

I followed the instructions for the ribbon cable K40, I can't seem to find the link. Can I disconnect the pot, maybe that is the problem? I will post a picture of the install.


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 18:57*

No, you should keep the pot in play. Use the physical test fire switch to see how many mA your pot is set to on the gauge. 

I cannot see a case where the board would cause the machine to fire at a higher mA than what the pot is set to. 


---
**Adam J** *April 20, 2017 19:06*

When I hit fire on the machines switch the needle maxes out...it goes up to 30 mA.



I should mention, I have managed to cut shapes out of paper, running on 20% power in laserweb, the pot was set to 8mA or so.



Here's a few pics of my install just to make sure everyhting looks as it should?

![images/cb5862cf7166eb272186d8e89945deb2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cb5862cf7166eb272186d8e89945deb2.jpeg)


---
**Adam J** *April 20, 2017 19:07*

![images/550fee5ec0958966877bfb8301115e08.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/550fee5ec0958966877bfb8301115e08.jpeg)


---
**Adam J** *April 20, 2017 19:07*

![images/4e499ab8f06e15a37b209e1acd2d1524.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e499ab8f06e15a37b209e1acd2d1524.jpeg)


---
**Adam J** *April 20, 2017 19:07*

![images/e3c4979d7e818732b90f8c319c995876.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e3c4979d7e818732b90f8c319c995876.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 19:07*

The white wire that came with the kit, I see it plugged in to the board.  It should not be used.  Where is the other end of it plugged in? 


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 19:08*

Get rid of that and wire the pot back up. 


---
**Adam J** *April 20, 2017 19:14*

Okay, I'll give it a try. As far as I know I haven't touched the pot wiring...



I just did a test cut at 10% power in LW and was getting about 7mA. Is that about right?


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 19:16*

The pot goes to these screw terminals where your white wire is. Did you disconnect any wires from there when you installed the white cable from my kit?![images/876ce8e17cea107201e2776caecfd8fa.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/876ce8e17cea107201e2776caecfd8fa.png)


---
**Adam J** *April 20, 2017 19:23*

Ah yes, I see where I put electrical tape around two wires, those must be for the pot then! Sorry, I installed the board a few weeks back.


---
**Adam J** *April 20, 2017 19:43*

All done and now it's cutting like a dream, boy is it smoooooth! Thanks for your patience Ray!


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/RzxRBWswgCw) &mdash; content and formatting may not be reliable*
