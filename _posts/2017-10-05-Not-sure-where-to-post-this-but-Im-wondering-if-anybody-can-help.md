---
layout: post
title: "Not sure where to post this but I'm wondering if anybody can help"
date: October 05, 2017 02:32
category: "FirmWare and Config."
author: "Jeff Janzen"
---
Not sure where to post this but I'm wondering if anybody can help.



I recently switched to GRBL-LPC  on the cohesion3d mini. The first version I tried was the 3 axis version found at [cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com).  It worked great but I switched to the beta07_4axes version so I could use M7/M9 to control my air assist.  



Now when I home my laser,  laserweb thinks it's at -300, -2 instead of 0,200.  Also,  "set zero" doesn't work so I have to manually type in the coordinates on the drawing so it centers on the laser head.   When I switch back to the 3 axis version, it works fine again but I would really like to be able to turn air on and off with g-code.



Any suggestions?







**"Jeff Janzen"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 02:35*

Can you link the exact build you got, the what this beta 07 is? 

For latest we are pointing people to the /cprezzi fork on github, there is a released tab. It is linked in my docs site. 

I recall a past build of the 4 axis had homing set up differently than the 3 axis, not sure about the current version. 


---
**Jeff Janzen** *October 05, 2017 02:44*

**+Ray Kholodovsky** I got it from here. It's the latest release.

[github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases)


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 02:51*

I have to tag **+Claudio Prezzi** in to help because I'm not familiar with the grbl side to this extent. Thank you in advance Claudio!


---
**Claudio Prezzi** *October 05, 2017 17:22*

It seems you have an offset in the controllers flash memory. You could try to move as far right and top as you can, then switch the machine off and move the head manually to the bottom left. Then switch on and home.


---
**Jeff Janzen** *October 05, 2017 20:07*

**+Claudio Prezzi** Thanks Claudio! I'll give that a try.

Would the "set zero" button be affected by that as well?


---
**Claudio Prezzi** *October 06, 2017 06:20*

**+Jeff Janzen** Set zero should always work and not be affected by that. 



Did you set $130 and $131 (grbl settings) to the size of your max x and y travel?


---
**Claudio Prezzi** *October 06, 2017 06:30*

Homing sets the machine origin. Set zero moves the work origin to the actual position (by saving the offset to the machine origin). This work offset is sometimes shown in the console at bottom right. It should be 0/0/0 after homing and set zero.

Internally grbl only works with machine coordinates, but in LW we show work coordinates.




---
**Jeff Janzen** *October 06, 2017 12:30*

**+Claudio Prezzi** I haven't had a chance to try your suggestion yet but I will once I get home from work today and report back.  



[plus.google.com - I&#39;ve made a new grbl-LPC version which supports switching of Coolant Floo...](https://plus.google.com/u/0/+ClaudioPrezzi/posts/g647wweNa3t)  



It looks like the zero button wasn't working for Jim Fong either.



These are the settings I used in GRBL if that helps:

$$

 $0=10

 $1=100

 $2=0

 $3=3

 $4=0

 $5=1

 $6=0

 $10=0

 $11=0.010

 $12=0.002

 $13=0

 $20=0

 $21=0

 $22=1

 $23=1

 $24=50.000

 $25=900.000

 $26=250

 $27=2.000

 $30=1000

 $31=0

 $32=1

 $33=5000.000

 $34=0.000

 $35=1.000

 $36=100.000

 $100=160.000

 $101=160.000

 $102=160.000

 $103=160.000

 $110=24000.000

 $111=24000.000

 $112=24000.000

 $113=24000.000

 $120=2500.000

 $121=2500.000

 $122=2500.000

 $123=2500.000

 $130=300.000

 $131=200.000

 $132=50.000

 $133=100.000

 $140=0.600

 $141=0.600

 $142=0.000

 $143=0.000


---
**Claudio Prezzi** *October 06, 2017 13:54*

I will try to replicate that set zero problem.


---
**Jeff Janzen** *October 07, 2017 05:21*

**+Claudio Prezzi** I tried removing that offset the way you described above, and it still shows up at -300, -2. The strange thing is, if I go to the "files" tab and then go to the "control" tab,  the coordinates on the DRO show 0,0 and then as soon as I jog the head, the DRO changes to the -300,-2.



Would it be hard to enable the M7/M9 commands on the 3 axis version found on the docs site?  On that version, the DRO and set zero both work perfect for me. 



I'm very new to all of this and don't know the first thing about compiling software, but if someone could point me to some info on how to do that, I'd be willing to learn.




---
**Claudio Prezzi** *October 07, 2017 11:02*

**+Jeff Janzen** If you change the tabs and come back to the control tab the dro gets initialized and waits for a position message from the machine (which is initiated with the jog command). This has nothing to do with your problem.



I don't know the version of the docs and who the maintaier of this version is. **+Ray Kholodovsky** Can you shine some light on that?


---
**Jeff Janzen** *October 07, 2017 16:08*

**+Claudio Prezzi** It mentions Jim Fong at the bottom, perhaps he compiled it?


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 16:12*

I think it was a custom compile from Jim/ Frank/ that whole chain of people that were originally fiddling with getting it to work on my Mini. More likely Jim, unless Claudio made this. 

I checked my downloads and the original filename was firmware_grbl-lpc_3axis_c3dmini.bin , does that help? 


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 16:14*

Yeah, that's a Jim, please see the comments here: [plus.google.com - Quick question for the GRBL people, have you managed to get homing working? I...](https://plus.google.com/109492805829132327540/posts/ZQsEVo7JPcz)


---
**Jim Fong** *October 07, 2017 16:59*

**+Ray Kholodovsky** that's the 3axis version I compiled with working home and set zero function for the mini. 



The set zero doesn't work on the 4th axis and C3d mini firmware on Claudio grbl-lpc release page.  



I complied the latest source a couple weeks ago to check if it has been fixed and it still doesn't work. 



This older 3 axis version is the one I use on my K40/mini until the current source code gets fixed.  


---
**Jim Fong** *October 07, 2017 17:35*

**+Ray Kholodovsky** the beta07 is on Claudio grbl-lpc release page August 3rd.  


---
**Claudio Prezzi** *October 08, 2017 09:48*

**+Jeff Janzen** I realized the reason for your homing coordinates. The default grbl behaviour for homing is setting the top right corner of the machinespace to zero, independant of where the homing switches are, so all work area is in negative space (like on professional CNC mills). Thats the reason why you get -300/-2 after homing. 

This would not be a problem, if "set zero" would work correctly.



I had already developed a special compile switch to force homing to positive space, so origin is on bottom left, but I didn't activeate it when I was compiling the last relese. I will activate it for the next release.



Now I need to replicate the set zero problem to solve that.


---
**Claudio Prezzi** *October 08, 2017 11:07*

**+Jeff Janzen** Can you please try if "G92 X0 Y0" is setting the coordinates to zero?


---
**Jeff Janzen** *October 08, 2017 13:50*

**+Claudio Prezzi** Yes, it is working, I actually just figured that out yesterday and set up a macro as a work around. 



 On my machine, I put mechanical endstops(NC) on both ends of x, y, and z wired to pins 1.24,1.25,1.26,1.27,1.28,1.29.  It was working well with smoothie, but in GRBL, it didn't work on both ends.  So I disabled hard limits and enable soft limits but even if I use the G92 code to set zero,  the machine must think its still in negative space because it won't let me jog because it thinks it's out of bounds.  For now I just have soft limits disabled as well. 



 Also, when homing, my Z table doesn't home, only x and y.

Is that normal for GRBL?  I don't  need it to home but was curious if that is somehow related to the other problems I've been having.  Thanks for all your help so far!    


---
**Claudio Prezzi** *October 08, 2017 15:00*

**+Jeff Janzen** My grbl-LPC Versions are compiled to only use the negative endstop pins for homing (independant of switch position). I'm wondering how you was able to wire Pin 1.25 and 1.27, as they are not available on the C3d Mini.

Instead with grbl you could connect positive and negative endstops of each axis to the same pin (in series if NC).



The homing sequence is compiled to use X and Y only, because most K40 machines do not have a Z axis.



I have made a new version that forces the machine origin to be at bottom left (instead of top right) and has solved the setZero problem. You could download it at [github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases/tag/beta08_4axes) 


---
**Ray Kholodovsky (Cohesion3D)** *October 08, 2017 15:02*

The 2 max pins (x and y) are available but via small headers as direct gpio. 


---
**Claudio Prezzi** *October 08, 2017 15:03*

**+Jim Fong** Could you try this new version please? It should solve the setZero problem.


---
**Claudio Prezzi** *October 08, 2017 15:07*

**+Ray Kholodovsky** Thank you for the clarification. I didn't notice that.


---
**Jeff Janzen** *October 08, 2017 20:55*

I just tried it and the set zero function now works for me.  My DRO only shows 0,0 no matter where I am and the blue laser marker doesn't move, but I might not have all the settings set up correctly.


---
**Jim Fong** *October 09, 2017 02:12*

**+Claudio Prezzi** set zero works, thanks for fixing that. Homing seems to work the first time.  Both Axis move fine afterwards. When you do a second home, I can’t move the Y axis.  

 

I have to power down and reset board to get Y to work again.  

 


---
**Jim Fong** *October 09, 2017 02:23*

**+Claudio Prezzi** ignore homing problem. looks like I have a hardware issue that I need to figure out. 


---
**Claudio Prezzi** *October 09, 2017 09:32*

**+Jeff Janzen** Check that you have set $10=0. Otherwise the dro will not show anything. See: [https://cncpro.yurl.ch/documentation/initial-configuration/65-grbl-lpc-1-1e](https://cncpro.yurl.ch/documentation/initial-configuration/65-grbl-lpc-1-1e) for details.


---
**Claudio Prezzi** *October 09, 2017 09:35*

You probably need to jog once before you get the correct position on the DRO.


---
**Jeff Janzen** *October 11, 2017 04:21*

**+Claudio Prezzi** Everything seems to be working great now. Thanks for all the help! 



 Is there any plans in the future to enable homing on the Z?  I can see my self wanting to use the g28 code in the future but the location saved to g28 is relative to machine zero and unless a person parks the z table at the same spot before shutting down,  machine zero on Z won't be consistent.  Also you can't have the soft limits enabled either unless you fully lower your Z table everytime you shut down. 


---
**Claudio Prezzi** *October 11, 2017 09:30*

**+Jeff Janzen** I've not planned to compile a special version with Z homing but my grbl-LPC fork is made with easy compile switches (in config.h) for different boards (in cpu_map.h) and machines (in defaults.h).

If you setup your PC to compile grbl-LPC yourself, you can add your own machine profile to defauls.h.


---
**Jeff Janzen** *October 11, 2017 12:02*

**+Claudio Prezzi** This is something I'm definitely going to try to learn as I've never compiled anything before.  Most info I could find on compiling GRBL use some arduino IDE software which I assume is wrong since this is a smoothie based board.  I'll do some more research but I may have some questions down the road if I get stuck:)


---
**Claudio Prezzi** *October 12, 2017 10:43*

It doesn't work with the Arduino IDE. You just need to follow the instructions on my github page: [https://github.com/cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC). Scroll down to the "Build notes".


---
**Claudio Prezzi** *October 12, 2017 10:46*

When you have installed the make tool and ARM embeded toolchain and included both in your path, you can compile the firmware by a simple make command in the main project folder.


---
**Jeff Janzen** *October 12, 2017 20:33*

**+Claudio Prezzi** Thanks, I'll check that out!




---
**Jim Fong** *October 12, 2017 22:08*

**+Jeff Janzen**  if you haven’t seen yet...

[plus.google.com - Compiling grbl-lpc on Windows I'm starting to test out grbl-LPC and wanted ...](https://plus.google.com/114592551347873946745/posts/4zw8XvssFMu)


---
**Claudio Prezzi** *October 13, 2017 07:10*

**+Jim Fong** Thank you Jim. I would like to integrate your guide into my github wiki page. Is that ok for you?


---
**Jim Fong** *October 13, 2017 10:22*

**+Claudio Prezzi** Yes please


---
**Jeff Janzen** *October 13, 2017 12:56*

**+Jim Fong** Thanks for the link.  Is it really important to set up a virtual machine?  You mention not wanting to mess up your build environment so I assume you have other compiling software installed as well which could conflict?



Also, when you mention adding those programs to windows path,  is this correct: 

                      search windows for "advanced system settings"

                      click "environment variables"

                      click on "path" in the system variables box and then "edit"

                      click "new" then add: 

                                                     C:\Program Files\Git\cmd

                                                     C:\Program Files (x86)\GnuWin32\bin 

                                                     C:\Program Files (x86)\GNU Tools ARM Embedded\6 2017-q1-update\bin



I'm using Windows 10

                      






---
**Jim Fong** *October 13, 2017 13:26*

**+Jeff Janzen**  I use a whole bunch of different compilers.  I didn’t know if Gnu arm would conflict with others so best to start out fresh with new VM.  



That’s how to add new path.  


---
**Jeff Janzen** *October 14, 2017 02:38*

**+Claudio Prezzi**If I want x,y, and z to  home at the same time, can I edit defaults.h,   go to the defaults_k40 section and change this line:  

HOMING_CYCLE_0 ((1<<X_AXIS)|(1<<Y_AXIS))

to:

HOMING_CYCLE_0 ((1<<X_AXIS)|(1<<Y_AXIS)|(1<<Z_AXIS))



and then compile?  Or is there a better way?


---
**Claudio Prezzi** *October 14, 2017 09:23*

**+Jeff Janzen** That's the correct way. But I would suggest to copy the whole K40 section to make your own machine profile that you then select in config.h.


---
*Imported from [Google+](https://plus.google.com/100039186463693162052/posts/i65mqPJ8ecX) &mdash; content and formatting may not be reliable*
