---
layout: post
title: "I have an issue with LaserWeb4, I have posted on their group but I figured cross-posting couldn't hurt"
date: October 10, 2017 20:03
category: "K40 and other Lasers"
author: "jeff gee"
---
I have an issue with LaserWeb4, I have posted on their group but I figured cross-posting couldn't hurt. 

I am importing an SVG and for some reason one of the letters shows up with no fill. I am hoping someone has run into this problem before and can direct me to a solution. I am running the latest version of LaserWeb4. 

Dropbox link to the SVG file.

[https://www.dropbox.com/s/g88fh3l3s5heo1t/beware.svg?dl=0](https://www.dropbox.com/s/g88fh3l3s5heo1t/beware.svg?dl=0)

![images/00c9b30249af75b863c6cce0f6f0b08e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/00c9b30249af75b863c6cce0f6f0b08e.jpeg)



**"jeff gee"**

---
---
**Claudio Prezzi** *October 10, 2017 21:49*

I hate cross posts, I don't like to read the same stuff multiple times. 

This is clearly a question for the LW community and not for cohesion.


---
**crispin soFat!** *November 18, 2017 06:16*

cross posting unskilled clipartisans and SVG corruption destroy rainforest.


---
*Imported from [Google+](https://plus.google.com/114869929766524387528/posts/bWE2bg9Qgb4) &mdash; content and formatting may not be reliable*
