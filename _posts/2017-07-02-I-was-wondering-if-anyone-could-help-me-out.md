---
layout: post
title: "I was wondering if anyone could help me out"
date: July 02, 2017 22:40
category: "Laser Web"
author: "laurence champagne"
---
I was wondering if anyone could help me out. I just installed my coehesion mini today on my K40. My problem is that as soon as I turn the board on the stepper motors are fully energized and stay that way. This is causing overheating. I'm using laserweb4 but don't see any options to turn this off.





**"laurence champagne"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 22:47*

Could you post pictures of your board, wiring, LaserWeb window, etc. 


---
**laurence champagne** *July 02, 2017 23:18*

**+Ray Kholodovsky** 

If I hit the reset button on the board it releases the motors. As soon as I run a job command or homing command though it keeps them energized.



![images/fb95850778b1a41535049d99ef2ac245.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fb95850778b1a41535049d99ef2ac245.png)


---
**laurence champagne** *July 02, 2017 23:20*

**+Ray Kholodovsky** 

![images/5b0be75a5f04d5ac9618aa8e6125a07e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5b0be75a5f04d5ac9618aa8e6125a07e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 23:36*

A few thoughts:



Just turn the machine on, literally just flick the power switch.  Don't have the USB Cable connected.  Can you move the head by hand? 



Now connect the USB Cable and just hit connect in LaserWeb.   How about now?  



Now yes, after you "do anything involving movement" the board will keep the motors energized as to not lose its position.  



The command to turn off the motors is M18 I believe.  You could make a Macro button in LaserWeb to do this, and add it to your end gCode for jobs. 



Now, what is the actual temperature of the motors that you are calling over heating?  It is normal for the motors to be warm.  Scalding hot is less ideal.  The question here is what position the current potentiometers on the A4988 drivers are set at - did you adjust them, and where does the flat face - outward, to the right, etc... 






---
**laurence champagne** *July 03, 2017 00:41*

Thanks, the M18 command works and de-energizes the motors. I guess I just wasn't expecting it to keep the motors energized after just simple jogging commands. I'm used to the tinyg and the M4nano that didn't do that and gave you an option to energize the motors.



Thanks for helping get that situated. Now I'm scouring over these boards trying to figure out all my other problems. 







I can't get the laser test button to do anything inside of the laserweb application.






---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 00:42*

You need to configure 2 settings in LW for that software test button to work (test power and interval).  You can also try a G1 X10 S0.6 F600 command to verify that the laser fires while moving. 


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/CfRSnpBhru1) &mdash; content and formatting may not be reliable*
