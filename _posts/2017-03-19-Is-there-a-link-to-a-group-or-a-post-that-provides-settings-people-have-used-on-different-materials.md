---
layout: post
title: "Is there a link to a group or a post that provides settings people have used on different materials?"
date: March 19, 2017 14:08
category: "General Discussion"
author: "floyd923"
---
Is there a link to a group or a post that provides settings people have used on different materials?  As I'm just learning, it would be great place for me to start and tweak from, rather than running blind!  Thanks in advance.





**"floyd923"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2017 14:20*

Have you been to the k40 group? 


---
**floyd923** *March 19, 2017 21:14*

I think I may have just found the one you are referring to, is it "K40 LASER CUTTING ENGRAVING MACHINE"?



I'll poke around in there.




---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2017 21:19*

That's the one.  Read the About Community section. 


---
*Imported from [Google+](https://plus.google.com/107810051940552772349/posts/4gQuvViVUg8) &mdash; content and formatting may not be reliable*
