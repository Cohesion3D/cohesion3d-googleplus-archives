---
layout: post
title: "SOLVED: faulty stepper driver Finally returning to getting my K40 upgraded to Cohesion 3d Mini (thanks for sending a replacement board!)"
date: June 26, 2018 19:10
category: "C3D Mini Support"
author: "David Piercey"
---
SOLVED: faulty stepper driver 



Finally returning to getting my K40 upgraded to Cohesion 3d Mini (thanks for sending a replacement board!).



I can get either my x-axis or y-axis working, but only by swapping the stepper driver.  If one works, the other doesn't.  I've tried swapping in the old stepper drivers from the board that I received first, but had significant probs, but neither of them will work.



Is it likely just a faulty stepper driver (I figure the two old ones fried with the first board perhaps?)?



At the moment, only one of the 4 drivers I have works, and it works in either X or Y axis location.



Note: I did upgrade the firmware to the latest one provided.

Note 2: while I can control the working axis, etc,. via USB, it doesn't show the memory card as  an available drive.









**"David Piercey"**

---
---
**David Piercey** *June 26, 2018 19:24*

![images/6e53c5ec274666ab12b102f3e862563c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e53c5ec274666ab12b102f3e862563c.png)


---
**Anthony Bolgar** *June 27, 2018 13:22*

It sounds like a bad stepper driver. 


---
**David Piercey** *June 27, 2018 15:03*

**+Anthony Bolgar** Thanks...  appreciate the confirmation.  Fingers crossed!




---
**Anthony Bolgar** *June 27, 2018 16:36*

The most recent version of the firmware disables the SD card for reliability issues. This is normal not to see it.




---
**David Piercey** *June 27, 2018 18:14*

**+Anthony Bolgar** Thank you for that info.  One less stress to figure out.



A borrowed stepper driver has things working well that way.




---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 16:15*

Interesting.  I tested your replacement board before shipping it to you.  



Sorry you're having a "very fun" time with all this. 


---
**David Piercey** *June 30, 2018 17:33*

**+Ray Kholodovsky** Life is full of little bumps.... things are working now, a few $ has a bunch of extra stepper drivers coming my way, and the laser cutter's working pretty darn good.  



Decades back, x-ray machines had a tendency to damage certain types of electronics (potentially).  I'm not sure if this is still true, and if so, whether or not Canada Custom's x-raying everything coming in to the country these days, may have cause the issue(s)?  



Regardless, I'm pleased to have things working now, and appreciated your quick responses and solutions.


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 17:37*

Had you already installed a separate 24v psu? The only thing I can think of is the poor quality of the 24v coming out of there with possible power surges and transients. 


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 17:39*

In about 2 weeks I should receive a batch of high quality 24v 4a DC power supply bricks (think laptop charger style). We will “strongly encourage” people to get this with the Mini. 



I’m thinking for the next board, there’s going to be a DC jack to connect it directly, and to not even hook up the 24v from the laser power pigtail. Less problems for me, I hope. 


---
**David Piercey** *June 30, 2018 20:44*

**+Ray Kholodovsky** No new supply as yet...had to get this thing working again before I put more money into it.  Now, though, it's got a new power supply, new tube, and new controller, and works. :) 



When budget allows, I'll check into the 2nd power supply (idea of price, yet?).   Need a Canadian tester? :D



Do you have any diagrams/images of any wiring changes etc. we'd have to do to use the supply you mention?






---
**David Piercey** *June 30, 2018 20:48*

**+Ray Kholodovsky** And great idea to the on-board connector in future.  


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 21:14*

Search this group for "autonomous" and you'll find Monty's post where we show how to wire in a separate 24v psu.  Hoping to do that in the $30-40 range.  You can get a 24v 6a "led style" PSU from China for around that too. 


---
**David Piercey** *June 30, 2018 21:17*

**+Ray Kholodovsky** Thank you, Ray.  




---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 21:21*

What I'm going for is a "PSU Upgrade Bundle" which is the 24v 4a Brick, an adapter that converts that to a screw terminal, and a section of silicone wire to connect that to the Main Power In screw terminal on the Mini. 


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 21:22*

That'll be one price hopefully in the range I said, and then we can bundle the psu brick alone with the new board for a bit cheaper. 


---
**David Piercey** *June 30, 2018 21:44*

I'll wait for yours to arrive then.  Thanks!










---
*Imported from [Google+](https://plus.google.com/114972156206786232028/posts/1khJKRMqSvu) &mdash; content and formatting may not be reliable*
