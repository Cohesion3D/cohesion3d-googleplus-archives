---
layout: post
title: "I've got the C3D board in and wired up as shown in the instructions"
date: June 24, 2017 01:14
category: "C3D Mini Support"
author: "bbowley2"
---
I've got the C3D board in and wired up as shown in the instructions.  My K40 has a ribbon cable that drives the stepper motors.  I plugged the red, blue, yellow, white plug into the X access on the board.  Problem, I don't get any movement on the X access.  I get nothing if I switch to the Y access.  the led lights are doing what they should.  I put the original board back in and I get both X and Y movement.  Any suggestions? 





**"bbowley2"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 24, 2017 01:16*

Picture of the board please? 


---
**Joe Alexander** *June 24, 2017 02:38*

from what I have in my machine doesn't the ribbon cable only drive the x-axis motor and the optical end stop? on mine the y-axis motor wires are separate.


---
**Ashley M. Kirchner [Norym]** *June 24, 2017 03:03*

The ribbon cable only drives the X-stepper and controls the limit switches. The multi-colored plug is for the Y-stepper. Please connect them in their appropriate headers.


---
**bbowley2** *June 24, 2017 19:02*

My C3D board

![images/32c037acd27063f8412ff2c67cbf90b3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/32c037acd27063f8412ff2c67cbf90b3.jpeg)


---
**Ashley M. Kirchner [Norym]** *June 24, 2017 19:07*

Everyone's C3D board looks the same. We need to see it connected in the machine. That is what's different. Please read this post in its entirety: [plus.google.com - A public service announcement: Everyone, we want to help you, really, we do....](https://plus.google.com/u/0/+AshleyMKirchner/posts/DAwQYWRrZnD)


---
**bbowley2** *June 25, 2017 16:37*

Sorry. Here is the board in the K40.

I'm getting Y movement but not X.

![images/437d40cf986f09a70c618ab4b1bb106e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/437d40cf986f09a70c618ab4b1bb106e.jpeg)


---
**Ashley M. Kirchner [Norym]** *June 25, 2017 18:12*

Glancing at your picture, it doesn't appear the board is powered up properly. There is no 24v applied to it. It may be plugged in, but not powered up. There's information missing here. What have you done, how have you tested it, what worked, what didn't. Exact steps you've taken, software used, etc., etc. Basic debugging and explanation of what is happening including what you did. I'm also seeing what appears to be the edge of the LPSU with the C3D sitting on top of wires. This is the perfect setup to introduce a short on the bottom of the board. If you're not planning to mount it properly (maybe because you're still using the old board), then please be mindful of how you handle the board and where you put it down. At the very least place a non-conductive material under the board such as a piece of plastic, wood, cardboard ...


---
**bbowley2** *June 26, 2017 02:55*

The red led is on. The phone camera didn't catch it.

Windows 10

LaserWeb 4 newest download.

After starting LW4 I get all the correct info when I connect,

When I try to run a job the X and Y numbers move but the laser only travels on the Y axcess.  No movement on the X axcess at all although I hear the X servo trying to engage the X transom.  If I run a check size the dot in LW4 moves around the loaded image but the laser only moves in the Y axcess.

I am very careful not to cause a short with the C3D board.  Here is another shot of the C3Din the K40.

![images/60dce8dd0791182666a063620017cceb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/60dce8dd0791182666a063620017cceb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 03:01*

Let's check if the driver in X might have gone bad.  Turn off all power, and unplug USB.  There should be no lights on on the board. Please remove the X driver, and put the Y driver in the X socket.  Please make sure that the orientation is correct as plugging it in backwards will damage everything.  Then please try jogging the machine again, and see if X moves now. 


---
**bbowley2** *June 26, 2017 03:21*

Removed the X driver and replaced it with the Y driver.  

No jog at all.  I put the X driver back in in the Y socket.  Now I only get X movement.  When I run a check size the ball in LW4 moves around the Image but only the X moves now.  No Y movement.


---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 03:46*

That's certainly strange.  The drivers seem to be acting wonky, but sounds like the Mini board is fine. 



Do you have Amazon prime? If so you should be able to get a set of replacement drivers quickly:  [amazon.com - Amazon.com: BIQU A4988 Compatible StepStick Stepper Motor Diver Module with Heat Sink for 3D Printer Controller Ramps 1.4(Pack of 5pcs): Electronics](https://www.amazon.com/dp/B01FFFYVV8/ref=cm_sw_r_cp_api_Zjiuzb7BEBACS)



If not, there are also eBay sellers that ship from the USA, or I can send you some. 



It would be worth replacing both drivers to see if that resolves the issue, and it's good to have some spares. 




---
**bbowley2** *June 26, 2017 04:37*

I will have them tomorrow.  I'll let you know.

Thanks.


---
**bbowley2** *June 27, 2017 13:01*

Put the modules in my C3D mini and everything works like it should.  Thanks.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/1uXifzraa6c) &mdash; content and formatting may not be reliable*
