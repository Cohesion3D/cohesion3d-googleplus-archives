---
layout: post
title: "I just installed the Cohesion board in my K40 last weekend"
date: April 13, 2018 03:35
category: "C3D Mini Support"
author: "Curtis Gineris"
---
I just installed the Cohesion board in my K40 last weekend. I am happy with everything except that the LCD display is difficult to read. I have already tried adjusting the pot and the picture shown here is the best one that I can get. Changing the pot does make the display get worse. The picture shown is the best setting. I think something is wrong with the LCD. 



Everything else is working. All motors work. They home in the right direction. I have used the Jog control on the LCD and moved the steppers. I have not tried running any G-code yet or anything else but the board seems to work and it control things so I don't expect any problems. I will work on it again this weekend.



Any ideas on what might be wrong. I have already removed the connectors and reattached them. The fact that there are characters on the display and the fact that I can control the steppers seems to tell me it is hooked up correctly.



![images/6b9b6d6224bd17172c912065f737b485.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6b9b6d6224bd17172c912065f737b485.jpeg)



**"Curtis Gineris"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 04:38*

Can you try turning the white knob 90 degrees clockwise so the flat faces the right? Between there and a further 45 degrees is where I get my best contrast. No joy there? 



The board is powered from the laser right now? 


---
**Mark Kenworthy** *April 13, 2018 06:48*

**+Ray Kholodovsky** I received the same board recently and mine looks exactly the same, the display looks no better than this picture no matter what the setting of that pot. 

I had a box of parts to do a RAMPS upgrade before I came across the C3D mini, I had completely forgotten that the ramps kit came with an identical display. I swapped it out and the display looks as I would have expected, the pot appears to function properly.

I think you may be shipping a bad batch of displays at the moment.


---
**Nick R** *April 13, 2018 07:23*

I have had the same issue with one of these with my 3d printer - here is a link to the solution [forums.reprap.org - Controller LCD contrast issue](http://forums.reprap.org/read.php?13,448609)




---
**Curtis Gineris** *April 14, 2018 00:00*

**+Nick R** Thanks so much Nick. That was the problem. I added a 100 ohm resistor and now the display is readable. Not really bright but readable. I'm not sure why there is no resistor to begin with. We should not need to modify these boards to get them to work. 



![images/a2192ee9e1825dde8ace2fca3d747e72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a2192ee9e1825dde8ace2fca3d747e72.jpeg)


---
**Curtis Gineris** *April 14, 2018 00:02*

Here is where the resistor goes.

![images/4237fbed7c99c218e52a7fe1c63d4d36.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4237fbed7c99c218e52a7fe1c63d4d36.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2018 00:11*

Thanks for the info, now is the first concern I've heard about the screens having this problem - I'll take a look at the stock we have here, and if you'd like a replacement one sent out just let me know. 


---
**Nick R** *April 14, 2018 00:42*

I think I used a 220ohm one for mine, at the time, came up perfect, and has been working fine for the last year or so.   I just ordered a Cohesion3D mini with one of these screens, and I kinda expected to have to do this mod when it arrived.    Tho, I do have a spare display from a wanhao i3 and noticed Cohesion has an adapter for that display now!  I might order the adapter when I figure out what I want to do with a Z Axis


---
**Jake B** *April 21, 2018 11:34*

Just for the record, one of the reasons I went through the trouble of getting the AZSMZ lcd running was because of the viewing angle of the display of the budget GLCD.  I have to try the resistor.  It seems that this is the fix.


---
**Curtis Gineris** *April 22, 2018 17:07*

I am getting another problem with this display. I don't know if it is related. On initial power on of the k40 the display is garbled as shown in the picture. Pressing reset on the smoothie has no effect. The only way to get the display to normal is to power cycle the k40. This happens almost every time I turn it on. It always recovers after a power cycle however I am worried that someday it won't





![images/8467e74ef07d74b8faeb678f876cbcc4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8467e74ef07d74b8faeb678f876cbcc4.jpeg)


---
**Curtis Gineris** *April 22, 2018 17:17*

**+Ray Kholodovsky** Since I am getting another problem with the display I think I would like another one sent out. I posted a picture in this post of what I am seeing. I need to power cycle to get the display working. This happens every time I turn the k40 on.

![images/1e7ca981b69536279938026a81ce28bc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e7ca981b69536279938026a81ce28bc.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 23, 2018 16:38*

Tthis is due to noise or insufficiency of the stock K40 LPSU - have you installed a separate 24v psu to power the mini  board?  



I'm not objecting, I just do not believe a new screen will solve this particular issue. 


---
**Curtis Gineris** *April 24, 2018 04:32*

**+Ray Kholodovsky** I have a separate PSU that I bought for my Z Axis. How do I replace the connection with the stock K40 PSU but still retain PWM control over laser power? It is an 8 amp PSU so it should be good enough to run everything except the laser itself. I am just not sure how to retain the PWM control if I disconnect the power cable that goes from the stock K40 supply to the smoothie board. The wiring diagram for the Z axis on your site retains the connection to the stock K40.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2018 17:34*

It is shown here: [plus.google.com - I want permit the K40 power supply to only be tasked to power the K40 laser a...](https://plus.google.com/u/0/117720577851752736927/posts/emzYWQAAXuW)


---
*Imported from [Google+](https://plus.google.com/111919513379296848767/posts/8FHF11vFmiT) &mdash; content and formatting may not be reliable*
