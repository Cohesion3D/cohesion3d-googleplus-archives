---
layout: post
title: "Cohesion3d mini in the House!!! Thanks Ray Kholodovsky :)"
date: January 14, 2017 17:09
category: "General Discussion"
author: "Ned Hill"
---
Cohesion3d mini in the House!!!  Thanks **+Ray Kholodovsky** :)

![images/5b15600380bac34064f5c273e7bc4e69.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5b15600380bac34064f5c273e7bc4e69.jpeg)



**"Ned Hill"**

---
---
**Ned Hill** *January 14, 2017 17:10*

Just wish I knew when my GLCD was arriving, ordered it about a month and a half ago from Aliexpress.  


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2017 18:17*

Ned you saw the guide link right?  I'll be around tonight if you need any questions answered. 


---
**Ned Hill** *January 14, 2017 18:29*

Yep I got them **+Ray Kholodovsky** .  Did you get everything straightened out with those of use with the digital power adjustments rather than the pots?  I seem to recall something about the lase being enabled if you manually moved the head?  I'll be digging back through older posts anyway.  Thanks again :)


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2017 23:59*

Well, there's 2 ways we can do this. 

You have a digital power adjustment instead of the pot. The way I recommend involves removing the "pot" from the equation.  If you want to keep the digital adjustment to set the max power, and then use power setting in LaserWeb to operate at some fraction of that, we can wire it that way as well. 


---
**Ned Hill** *January 15, 2017 00:09*

Ahhh ok.  Ditching the digital power display all together and controlling everything through laserweb. Gotcha. Thanks




---
**Chris Sader** *January 17, 2017 19:56*

**+Ray Kholodovsky** Got my kit today! Can you elaborate on how to keep the built in POT functioning so that everything done in laserweb is (Laserweb Power) * (POT %) = Actual Power setting?


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 20:01*

**+Chris Sader** awesome!

Sure thing. In that case, don't use the provided pwm wire at all.  You'll just need to connect L on the psu to a different pin that is hardware pwm capable - I'm looking at the bed MOSFET so that would be the 4th pin from the left on the blue terminals along the bottom edge. 

Do everything else, take some pics, and I'll advise you more. 


---
**Coherent** *January 17, 2017 20:33*

Thanks for the post. I'm hoping folks like you will get it all figured out. Please post all the info and photos! Mine scheduled for today, but I'm in no rush to install it. That way when I get time to put mine in I can just read all the tips and help on this forum and hopefully not screw it up! 


---
**Coherent** *January 17, 2017 20:36*

**+Ned Hill** I thought of ordering a GLCD through Aliex also or even Ebay and then found them on Amazon... yea prime, was here in two days!


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 20:42*

Yeah prime! 


---
**Ned Hill** *January 17, 2017 20:51*

+Coherent Yeah I just ordered the one Ray had recommended.  I ordered it the same time as the mini and figured with the long lead time on the mini they would get here about the same time.


---
**Chris Sader** *January 17, 2017 21:07*

**+Ray Kholodovsky** will do. As for GLCD, I have one of these kits laying around. Think the LCD from it will work with the mini? [https://www.amazon.com/gp/product/B0111ZSS2O/](https://www.amazon.com/gp/product/B0111ZSS2O/)



Also, **+Ned Hill**, sorry for hijacking :)


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 21:20*

Yeah that's the one.  Hey are any of you guys up for doing the conversion today?  


---
**Chris Sader** *January 17, 2017 21:22*

**+Ray Kholodovsky** I am. I've got everything but the L wire hooked up. Unfortunately the LCD doesn't even turn on.


---
**Chris Sader** *January 17, 2017 21:48*

nvm, I'm an idiot. had the cables swapped.


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 21:51*

Ok, well let me know when you're ready and we'll deal with L.  And I need pics of your psu and wiring going to the board.


---
**Chris Sader** *January 17, 2017 21:55*

Mini

![images/95c15a7a62c2e2f78796264c33074683.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/95c15a7a62c2e2f78796264c33074683.jpeg)


---
**Chris Sader** *January 17, 2017 21:57*

PSU



**+Ray Kholodovsky** that stray red wire used to be hooked up to L on the PSU. Those two white and black strays are a fan I want to figure out where to hook up. It's actually a 4-pin PWM fan, so if there's a place to hook that up even better, otherwise I'll just run it off of 12v and GND from the PSU.

![images/eb2c790c65842642f2c588909f609c19.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eb2c790c65842642f2c588909f609c19.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 22:03*

Ok, get a section of wire and run it from L terminal to FET1 Bed -, the 4th pin from the left on the blue screw terminals along the bottom edge.  We can change the config file to reflect this.  Insulate the end of the red wire.  It's not getting plugged back in and we don't want it shorting against anything. 


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 22:06*

And put the heatsinks onto the A4988 drivers :)


---
**Chris Sader** *January 17, 2017 22:37*

**+Ray Kholodovsky** great. Can you tell me what I should change in the config?


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 22:43*

laser_module_pin                              2.4! 



change this to 2.5   



no exclamation at the end.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/KGHcYLM5hjv) &mdash; content and formatting may not be reliable*
