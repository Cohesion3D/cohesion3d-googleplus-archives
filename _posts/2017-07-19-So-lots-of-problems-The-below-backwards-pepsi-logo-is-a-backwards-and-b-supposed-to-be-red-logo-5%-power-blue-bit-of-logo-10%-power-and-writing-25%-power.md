---
layout: post
title: "So, lots of problems! The below backwards pepsi logo is a) backwards and b) supposed to be red logo 5% power, blue bit of logo 10% power and writing 25% power"
date: July 19, 2017 20:18
category: "K40 and other Lasers"
author: "timne0"
---
So, lots of problems! The below backwards pepsi logo is a) backwards and b) supposed to be red logo 5% power, blue bit of logo 10% power and writing 25% power. C) endstops don't appear to be working.  I have connected screw 4th from left to L **+Ray Kholodovsky**​ suggested. I've wired the 24v +/- in the correct places. It cuts, but the power setting is only working through the POT. Any suggestions? I live 155 miles away from the laser and get 2-3 hours a night for 2-3 nights a week and this is the hackspaces only laser.  If there's any magic suggestions I'd very much appreciate it, it was a lot of money to drop on this board specifically for plug and play rastering!



![images/32b25a3f8e559425010178afcb3e66be.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/32b25a3f8e559425010178afcb3e66be.jpeg)
![images/030f1686b795e54e2e13fcdef2ab7352.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/030f1686b795e54e2e13fcdef2ab7352.jpeg)

**"timne0"**

---
---
**Griffin Paquette** *July 19, 2017 20:27*

Let's work through one issue at a time:



Are you using the preconfigured confit file for the K40?



When you hit home are the axis' moving in the directions towards the end stops?




---
**timne0** *July 19, 2017 20:28*

when I hit home, nothing happens.  currently it relies on the soft stops




---
**timne0** *July 19, 2017 20:29*

frankly I'd rather solve the pwm problem because I can use the soft stops and people can still use the laser, rather than it being down again once I'd just got it working with ramps!




---
**Ashley M. Kirchner [Norym]** *July 19, 2017 20:37*

If hitting 'Home' does nothing, then you're missing the configuration in LW4 for that. In Settings, under GCODE, look for the 'GCODE HOMING' box and enter 'G28.2' in it (without the quotes). See this page for configuration settings: [cohesion3d.freshdesk.com - LaserWeb4 Configuration : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration)


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2017 20:45*

A quick note on the endstops, you need to plug them into the X and Y ports which are to the right of the blue screw terminals on the bottom edge.  Sig and Gnd for each.  Then send G28.2  the head should move back left.  If an axis moves the opposite direction, then you need to flip that motor cable. 



Ok, PWM.  You will hear me refer to the contents of of the PWM Tuning section here: [cohesion3d.freshdesk.com - Troubleshooting/ FAQ : Cohesion3D](https://cohesion3d.freshdesk.com/solution/articles/5000734038-troubleshooting-faq) such as when I mention the G1 X10 S0.4 F600 line with various S values and a different X/Y value so it moves to a new place each time. 



Typically, a pwm period value of 200 or 400 does the trick, and you see the different burn levels and readings on the mA meter. 



Typically, we will set the pot at 10-13mA and the S value will set a % proportion of that.  



If this is not working then we have a unique case and I can walk you through the hooking up the 2 wire pwm variant which is closer to how you had it before. 


---
**timne0** *July 20, 2017 08:31*

Ok. I need to do two pin pwm management because I'm just literally burning holes in the wood with little or no definition.


---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 21:01*

Here's the old old guide for how we do 2 wire.  Anyone else that might read this, please please please do not do this without first checking with me about it.  Literally 0.2% of installations have required this. 



[dropbox.com - C3D Mini K40 Laser Installation (Carl's guide v1).pdf](https://www.dropbox.com/s/ywap3yay64812qs/C3D%20Mini%20K40%20Laser%20Installation%20%28Carl%27s%20guide%20v1%29.pdf?dl=0)



So... that shows how to split off the pwm wire.  Center wire goes to IN on the PSU (pot is getting disconnected again).  



In the config we change to this:

laser_module_pin                              2.4!

switch.laserfire.output_pin              2.5



Save, eject, reset the board, etc.



Now you send an M3 as "safety" to enable the laser, and then when you run the G1 X10 S0.6 F600 line the laser should move while firing.  



Adjust the pwm period from 200 to 400 to realize contrast as needed. 



Again, no one else do this please. 


---
**timne0** *July 20, 2017 21:45*

I wish I was simple like everyone else!


---
**timne0** *July 20, 2017 21:45*

I've just left the hackspace will be back next week now.


---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 23:17*

Oh, and M5 at the end to "safety back on" .

That's why M3 is in the start code and M5 is in the end code in the LW4 settings screenshots. 


---
**timne0** *July 24, 2017 18:45*

**+Ray Kholodovsky**



Getting no firing now. not sure why.  Does this match what you've said above?  X and Y sorted.  I'll do Z later...



![images/8f8b357f2698b5d6dc945cc299dc76f4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8f8b357f2698b5d6dc945cc299dc76f4.png)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 18:49*

Save that, eject, and reset or power cycle the board for the new changes to take effect. 



I think it looks good. 



Send M3, then send the G1 X10 S0.6 F600, and it should fire while moving. And then M5 for safety back on. 


---
**timne0** *July 24, 2017 18:50*

yeah that's the bit it's not doing! :)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 18:51*

Wiring pics please.  


---
**timne0** *July 24, 2017 18:54*

![images/bc5a41a23eb5a5f57318551dc77ef982.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc5a41a23eb5a5f57318551dc77ef982.jpeg)


---
**timne0** *July 24, 2017 18:55*

Homing does seem to work! Though Y says -204 (or something)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 18:58*

Briefly saw a pic then it disappeared. Did you change the L and power wiring back? 


---
**timne0** *July 24, 2017 18:59*

yes - put the original power connector back on so all four power blocks are plugged into the board


---
**timne0** *July 24, 2017 19:00*

does the L need to be connected via the main pin?




---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:06*

L does need to be connected for our current wiring setup.  The top pin of the power connector is the same as the screw terminal on the Mini.  Can you make sure that you've reconnected everything correctly?  Otherwise, can you wire L to the 2.5- terminal as before?  



Failing that, can I have you hold L to ground and execute the G1 X10 S0.8 F600 line? 



I was really hoping we would only change one thing at a time here!!! :)


---
**timne0** *July 24, 2017 19:08*

haha



OK I have reconnected L to the 4th pin from the left.  Nada


---
**timne0** *July 24, 2017 19:09*

Is there any way of testing the laser? the laser test button seems to do nothing as well...


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:11*

Have you tried:

hold L to ground and execute the G1 X10 S0.8 F600 line? 



To clarify, this means disconnecting the L wire from the Mini terminal and holding that end to ground. 


---
**timne0** *July 24, 2017 19:12*

yes I did, yes it fired the laser...


---
**timne0** *July 24, 2017 19:12*

wiring the L to pin 4 from left and executing the command did nothing




---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:14*

That's important information to mention!!!



Let's focus on that for a moment: 



This is what I expect should happen:

You hold L to ground.  The laser does not fire. 

You execute the G1 line.  The laser fires while it is moving.  Then the laser stops firing. 



What did you have happen?


---
**timne0** *July 24, 2017 19:15*

that's exactly what happened! Sorry! I did do it just as you were typing the previous comment! :)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:17*

Ok, so this is good news.  It means our issue is localized to the M3 enabling the laser (pulling L to ground). 



HAHA we have to enable switch.laserfire true

I missed that!  Sorry!


---
**timne0** *July 24, 2017 19:24*

Yes! Working! So, top power of S1 gives me 13ma as opposed to 18/21 is there a way to improve that?

![images/d030849c636c72eb2c6a334432c5450d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d030849c636c72eb2c6a334432c5450d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:26*

The max power line of 0.8 change that to 1 



Then there's no limiting happening on the board side of things and it's all laser-dependent. 


---
**timne0** *July 24, 2017 19:28*

aha, OK! And is there a way of flipping the Y axis in software so I'm not doing negative Y numbers?


---
**timne0** *July 24, 2017 19:28*

![images/0d8bcd99f574a287186022372cdb4f9f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d8bcd99f574a287186022372cdb4f9f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:29*

Ok, let's talk about that.   Per default config and k40 switch locations, G28.2 homes to rear left and reports position as 0, 200.  Homing is rear left, 0,0 is front left.  0,0 is the origin in LaserWeb. 



All else should follow accordingly. 


---
**timne0** *July 24, 2017 19:30*

If I go front left, I get 0, -200, origin is 0.0


---
**timne0** *July 24, 2017 19:31*

G28.2 takes me to rear left


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:32*

Start a new thread, post the config.txt file (pastebin or Dropbox) and screenshots of LW settings. 


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:35*

As far as this thread, just fiddle with the pwm period till you're happy with the contrast on rasters. 


---
**timne0** *July 24, 2017 19:58*

Sorry **+Ray Kholodovsky** what's setting is the pwm period? is that the 200/400/600 I was playing with?


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 19:59*

Yes that's the one. 


---
**timne0** *July 31, 2017 17:43*

**+Ray Kholodovsky** does reducing the PWM period increase or decrease contrast?


---
**Ray Kholodovsky (Cohesion3D)** *July 31, 2017 17:58*

Depends on the material and other factors. 



My statement is as we did above, if the period of 200 is not creating enough difference in the burn power levels, we increase it until the machine is responding as expected. If the ability to best discern different power level burns is what we are calling contrast... then my statement supports an increase. But again, people like **+Claudio Prezzi** vary theirs based on cutting vs engraving, material type, etc. 


---
**Claudio Prezzi** *August 01, 2017 11:06*

A PWM period of 200 microseconds (=5000Hz) is a good starting point to get acceptable grayscale and cutting results. Longer periods (up to 1000us) can create finer grayscale graduation, but also needs slower feeds (<100mm/s). Shorter periods (down to 50us) can create more power for cutting (because the LPS overshoots) and allow faster feeds.


---
**Claudio Prezzi** *August 01, 2017 11:11*

It's best to thest the settings for each material with a gray bar (black to white) to find the best parameters. Don't set the feed too height if you want to get decent grayscale. Start with 20-50mm/s (=1200-3000mm/minute).


---
**timne0** *August 01, 2017 12:32*

I've had success with 50us with a power scale of 0 to 11% at 150mm/m


---
**Claudio Prezzi** *August 01, 2017 12:48*

If you still have the pot installed, I would suggest to lower the power on the pot instead of the power range in LW4.



If you set the power range to 0-11%, you also only have 11% of the descrete power stepps, which means you loose maximum amount of gray levels.


---
**timne0** *August 01, 2017 12:52*

No POT - doesn't work on the setup I have.  12%+ at 150mm ends up with burnt images.  I'd have to speed it up and start having bleeding issues.



I have now got 18ma (at 100%) using an S1.0 code.  I changed the water, it was perhaps a little dirtier than it usually is.  Still set at 100% power so odd I'm not getting 21ma, but 18ma is all I want to cut at so no probs there.


---
**Claudio Prezzi** *August 01, 2017 12:55*

I think smoothieware has 10bit PWM resolution, which means 1024 descrete levels (11% corresonds to 112 levels only).


---
**timne0** *August 01, 2017 12:56*

That's probably true, but at 100% it's massively over powered for engraving. The image of a white duck is completely blacked out.


---
**timne0** *August 01, 2017 12:59*

Bottom left is 600mu, bottom right is 100mu (so no obvious change, 0-100%, 1000mm/min). The third duck on the row of three is 0-8% 150mm/m, 2nd is 0-10% and 3rd 0-11%. It's a white duck, so I'd expect to see mostly unburnt regions, but that was the lowest where you could actually see the neck.



![images/7a834120a9b612b0be2781697c01177a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7a834120a9b612b0be2781697c01177a.jpeg)


---
**Claudio Prezzi** *August 03, 2017 12:27*

I suggest to change your setup to use a pot like Ray recommend in his setup documents.


---
**Claudio Prezzi** *August 03, 2017 12:38*

Without a pot to limit maximum current it's realy hard to get decent grayscale images.


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/9SZxy3qhxqq) &mdash; content and formatting may not be reliable*
