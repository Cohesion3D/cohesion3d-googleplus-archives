---
layout: post
title: "Hi everyone. I just got K40 laser and upgraded it with Cohesion3D"
date: October 15, 2018 17:33
category: "C3D Mini Support"
author: "Ali Sureyya Torun"
---
Hi everyone.



I just got K40 laser and upgraded it with Cohesion3D. Its was working great until 2 days ago.



Out of blues the board stop communicating with my computer. I cannot see it in device manager and Lightburn is not controlling it.



I can control the board through GLCD so there seems to be no issues with stepper motors, drivers, laser control etc. I can fire laser from board, can initiate homing and jog it without any problems but there is no USB communication of any kind.



I use a brand new high quality cable with Ferrrite core. I tried it with even different cables and computer to see if there is something wrong with my system but so far no luck.



I checked the board and power cables and all seems normal. Board LEDs are ok, gets power properly. All LEDs are working L2 and L3 are blinking properly. I fastened the board with plastic screws and legs so there is nothing to cause any short circuits or anything related with grounding issue.



Is it possible if there is something wrong of defective with USB connection or electronics of the board?



![images/302e922de5343d0032c3f698b71e2a58.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/302e922de5343d0032c3f698b71e2a58.jpeg)
![images/756c64b34541c4cf54e7217f83b44ae1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/756c64b34541c4cf54e7217f83b44ae1.jpeg)
![images/8a1e376558dce0d5bff4ff26c63fecf0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8a1e376558dce0d5bff4ff26c63fecf0.jpeg)
![images/aa0f5e497d0a567531b873850929b5f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aa0f5e497d0a567531b873850929b5f4.jpeg)
![images/cc56e3d015b35df20b0fada725e1d771.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc56e3d015b35df20b0fada725e1d771.jpeg)
![images/cb59eb33324b5f1be0282e78862e00d3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cb59eb33324b5f1be0282e78862e00d3.jpeg)
![images/586794ccf472a66be78277de659e199a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/586794ccf472a66be78277de659e199a.jpeg)

**"Ali Sureyya Torun"**

---
---
**Tech Bravo (Tech BravoTN)** *October 15, 2018 17:54*

What operating system?


---
**Ali Sureyya Torun** *October 15, 2018 21:21*

Windows 10 64 Bit.


---
**Tech Bravo (Tech BravoTN)** *October 16, 2018 01:06*

Did you choose cohesion3d smoothie in the device setup?


---
**Ali Sureyya Torun** *October 16, 2018 16:04*

**+Tech Bravo** Yes Its all proper settings in Lightburn. Tried to reinstall it even and set it up from scratch but no luck.



The thing is I cannot see the device from device manager at all. I should be able to see it in there at first right?


---
**Tech Bravo (Tech BravoTN)** *October 16, 2018 16:09*

Yes you should see the device in device manager


---
**Ali Sureyya Torun** *October 16, 2018 18:32*

Any suggestions?


---
**Tech Bravo (Tech BravoTN)** *October 16, 2018 20:21*

try this to remove ghost usb devices:

1. go to the bottom of my downloads page [lasergods.com - Downloads - LaserGods Laser Engraver Info Portal](https://www.lasergods.com/downloads/)

2. Unzip the archive

3. unplug the laser from the pc

4. leave all other usb devices plugged in

5. Run ghostbusters

6. check create restore point if you want

7. Select all ghost devices

8. Remove all ghost devices




---
**Ali Sureyya Torun** *October 16, 2018 22:20*

**+Tech Bravo** Will try. Thanks for suggestion.


---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2018 23:07*

I'd also recommend putting new firmware and config files on the memory card to ensure they haven't been corrupted. 

I don't see any pics of your actual board installation, these will help us advise you. 


---
**Ali Sureyya Torun** *October 17, 2018 00:12*

**+Ray Kholodovsky** Hi Ray. I uploaded 17 photos there and if you click View album link just bottom left corner of the images you can see the all the photos along with actual board connections if you meant that. If not I can take full photo of board from top as well to see all of it.



I did also tried to install new firmware to a new sd card but no luck... I just purchased an ethernet adapter and still waiting for it to see if there is also an issue with ethernet connection as well. I suppose ethernet is far more stable than usb connection?


---
**Ali Sureyya Torun** *October 17, 2018 03:27*

I tried to remove ghost devices but nothing changed... Here is the whole board image with connections.

![images/ed210ae611fd2ecc84e0417ac9344d5a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed210ae611fd2ecc84e0417ac9344d5a.jpeg)


---
**Ali Sureyya Torun** *October 17, 2018 03:27*

![images/0634420bc4bb382c9709241f312ef872.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0634420bc4bb382c9709241f312ef872.jpeg)


---
**Ali Sureyya Torun** *October 17, 2018 03:28*

![images/4a11d97f274499828fc2b885b2db7b28.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4a11d97f274499828fc2b885b2db7b28.jpeg)


---
**Tech Bravo (Tech BravoTN)** *October 17, 2018 03:47*

the connections look ok. so you did format the microsd card and reload the smoothie firmware.bin and config.txt files onto it correct? where did you get those files from?




---
**Ali Sureyya Torun** *October 17, 2018 06:35*

**+Tech Bravo**  I formatted another MicroSD card to FAT32 and keep the original one. I got the new files from [https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0).



I dont know if these are correct ones to use...



The thing is USB does not work either with the original firmware came with the board.

[dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2018 16:48*

Can you tell what LEDs do when you power on? 


---
**Ali Sureyya Torun** *October 17, 2018 16:52*

Yes. VMOT, 3V3, L1, L4 are solid green. L2 and L3 are blinking green. There is another LED near Kill button and its solid green as well. 


---
**Ali Sureyya Torun** *October 17, 2018 16:54*

I am really out of ideas now...


---
**Ali Sureyya Torun** *October 18, 2018 16:31*

**+Ray Kholodovsky** Is there any way you guys diagnose the board if I send it over to you too see any way to fix it or at least replace it with new one? I dont want a refund or something. It is a great board that fits my all needs. I am simply sitting over a stone waiting for a solution.



I did also sent a support ticket to you guys but so far I did not get an answer related with it.



I appreciate your time trying to help me to figure it out what is wrong with it btw.


---
**Ali Sureyya Torun** *October 19, 2018 18:38*

Anyone here?


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2018 18:44*

Yeah, sorry, crazy busy here.  Will reply shortly. 


---
**Ali Sureyya Torun** *October 19, 2018 19:32*

**+Ray Kholodovsky** Thanks Ray. Appreciated.


---
**Ali Sureyya Torun** *October 22, 2018 18:23*

Hi Ray. Any news?


---
**Ray Kholodovsky (Cohesion3D)** *October 22, 2018 18:53*

I replied to your email. 


---
**Ali Sureyya Torun** *October 23, 2018 23:40*

**+Ray Kholodovsky** Thank you Ray I got it. I will send the part as soon as possible. Hopefully you can resolve whatever the problem is with card.


---
*Imported from [Google+](https://plus.google.com/107651097210973310424/posts/drsdy99CZN7) &mdash; content and formatting may not be reliable*
