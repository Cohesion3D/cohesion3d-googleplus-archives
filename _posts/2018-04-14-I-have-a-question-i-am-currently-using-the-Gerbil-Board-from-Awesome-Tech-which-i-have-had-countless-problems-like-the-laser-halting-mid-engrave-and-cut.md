---
layout: post
title: "I have a question , i am currently using the Gerbil Board from \"Awesome-Tech\" which i have had countless problems like the laser halting mid engrave and cut"
date: April 14, 2018 13:46
category: "K40 and other Lasers"
author: "Nathan R"
---
I have a question , i am currently using the Gerbil Board from "Awesome-Tech"

which i have had countless problems like the laser halting mid engrave and cut.

and the list goes on .so i guess my question is how reliable is cohesion 3d for the k40 laser ? . in terms of success rate also customer support ?.



the customer support at awesome tech is shit, i have asked multiple questions on how to get the gerbil to run correctly and no replys .



Any feedback would be most appreciated .



kind regards 





**"Nathan R"**

---
---
**Anthony Bolgar** *April 14, 2018 13:52*

I like the 5 C3D mini boards that I have. No real problems other than my own mistakes with them. And great tech support (But please remember that most of the companies you will have dealings with in the laser community are very small companies, you will not have 24/7 tech support, anything less than 48hrs to respond is very acceptable in my opinion). What did you ask Awesometech that you did not get a reply to? I have 2 Gerbil boards and I have them working well. Maybe I can help you to get up and running while you wait for a C3D mini board if that is the direction you want to go.


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2018 13:55*

Just be aware that a good chunk of issues I've been hearing about come from the shit 24v supply in the K40, the original USB Cable the K40 comes with, and noise.   If you replace these (ie: adding a separate 24v power supply to power the board and a high quality usb cable) it seems to solve a lot of issues people experience. 



Then we get into disconnects and raster speed - board comes with Smoothie firmware... this can run a screen and run jobs from the memory card, for untethered operation which is important for quite a few people... but with this overhead there is a cap for raster engraving speed - if you want to go really fast then then you just flash the grbl-lpc firmware onto the same board - no screen, but insanely fast raster performance, and you'll be streaming the job from Lightburn software on your computer to the board over USB Cable. 



There you go, all my cards are on the table. 


---
**Nathan R** *April 15, 2018 01:14*

Thanks guys for your help , okay i got a trial of the lightburn software which i like its easy to use interface ,especially since i am no engineer when it comes to laser equipment . My issue is the laser will stop midway through engrave or cut , and to be honest its really annoying. 





Ray the PSU unit in my K40 is stock ,what do you recommend ?






---
*Imported from [Google+](https://plus.google.com/107472733712638889175/posts/bxC4qdqpLsn) &mdash; content and formatting may not be reliable*
