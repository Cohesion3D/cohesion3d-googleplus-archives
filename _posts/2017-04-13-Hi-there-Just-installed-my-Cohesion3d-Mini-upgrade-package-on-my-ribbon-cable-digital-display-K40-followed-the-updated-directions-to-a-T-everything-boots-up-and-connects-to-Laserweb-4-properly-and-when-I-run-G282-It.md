---
layout: post
title: "Hi there! Just installed my Cohesion3d Mini upgrade package on my ribbon cable / digital display K40, followed the updated directions to a T, everything boots up and connects to Laserweb 4 properly, and when I run G28.2 It"
date: April 13, 2017 00:08
category: "K40 and other Lasers"
author: "Dan Kelley"
---
Hi there! Just installed my Cohesion3d Mini upgrade package on my ribbon cable / digital display K40, followed the updated directions to a T, everything boots up and connects to Laserweb 4 properly, and when I run G28.2 It homes in the Y Axis properly, but then disregards the X Endstop and keeps crashing into the Left end of the X Axis (where it should home. I know the end stop is good, as I just used it minutes before installing the new board, but do you have any ideas +Ray Kholodovsky? Let me know if there are any specific settings/ photos that might help diagnose this. 



Jogging the x and y axis works as it should though.





**"Dan Kelley"**

---
---
**Dan Kelley** *April 13, 2017 00:08*

![images/65d7551ecc0fe77b2c7cf54444cf2b9e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/65d7551ecc0fe77b2c7cf54444cf2b9e.jpeg)


---
**Dan Kelley** *April 13, 2017 00:09*

![images/4e21616a3204aba6af2be5cba5eb9719.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e21616a3204aba6af2be5cba5eb9719.jpeg)


---
**Dan Kelley** *April 13, 2017 00:09*

![images/1b817f1eed74d097129b321fa7f8806f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b817f1eed74d097129b321fa7f8806f.jpeg)


---
**Dan Kelley** *April 13, 2017 00:10*

![images/912142acb757589aff13e39a85c1b355.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/912142acb757589aff13e39a85c1b355.jpeg)


---
**Dan Kelley** *April 13, 2017 00:10*

![images/b8789544e646317d271ca0c37a0a6d68.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8789544e646317d271ca0c37a0a6d68.jpeg)


---
**Dan Kelley** *April 13, 2017 00:10*

![images/ee8dc214b7ed75f45f60ded723dd3e72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ee8dc214b7ed75f45f60ded723dd3e72.jpeg)


---
**Dan Kelley** *April 13, 2017 00:11*

![images/11bdc364142861f3c5f9b2c4b694d9ee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11bdc364142861f3c5f9b2c4b694d9ee.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 00:11*

Send a command M119 with the head at the center. Then move the head to the left and send it again. See if the endstop is being read. It should go between 0 and 1 in the response you get. 


---
**Dan Kelley** *April 13, 2017 00:12*

![images/6eb0037fee2ea048a4db8cba798d9aa1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6eb0037fee2ea048a4db8cba798d9aa1.jpeg)


---
**Dan Kelley** *April 13, 2017 00:16*

Just tried that twice, although I don't see any difference in the output, here is the first and second attempt, which I just repeated with the same results:

M119;



 X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1 ;



 M119;



 X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1 ;


---
**Dan Kelley** *April 13, 2017 00:18*

Y_max bit registers correctly when I move it to the top of the carriage though


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 00:19*

Try the same with Y for fun, you should be seeing the 0 change to a 1. 



Anyways, next step - unplug and replug the ribbon cable, while it's out inspect the contacts for any damage.  



If that doesn't work I send you a new board first thing tomorrow morning. 


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 00:24*

Oh, one more thing.  With the ribbon cable unplugged, I'd like you to try connecting the "Sig" to "Gnd" for the X endstop (the left 2 pins of the white endstop header, please see pinout diagram on docs site). Then try the M119 again. 


---
**Dan Kelley** *April 13, 2017 00:29*

Alright, so powered off and checked everything, no damage to be seen, looks like M119 has not changed after re-running the same tests.

![images/0d3cb37b62c66a6bb462ecd1aa41ca78.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d3cb37b62c66a6bb462ecd1aa41ca78.jpeg)


---
**Dan Kelley** *April 13, 2017 00:31*

Connected Sig to Ground on the X-min** headers as well, and it still does not reflect that in the M119 command, in the meanwhile, is there a g code command for "set current position as home" ?

![images/2919b793c15428ae025a1af5cdab0748.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2919b793c15428ae025a1af5cdab0748.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 00:33*

Yes, G92 X0 Y0 would do that, there is also a "zero" button in LaserWeb's jog panel. 

Please email me (info@cohesion3d.com) and I'm getting a replacement out to you. 


---
**Dan Kelley** *April 13, 2017 00:38*

**+Ray Kholodovsky**

Thanks for the awesome service and feedback. You put a lot of work into your community and such a huge variety of unpredictable machines and it reflects that. I'll send that email out.


---
**Eric Lien** *April 13, 2017 02:02*

**+Dan Kelley** agreed Ray is active, quick to reply, and takes care of his customers. Not much more you can ask for.


---
**Dan Kelley** *April 16, 2017 19:47*

**+Ray Kholodovsky**



So unfortunately it seems that the new board you sent shows all of the same symptoms, letting me know that it is NOT a bad board (always good to hear), and there must be something going on with either my configuration, machine, or end-stop design. Do you by any chance have a schematic of the standard ribbon cable pin-out? I'm just hoping there's some way I can debug this here.


---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 19:54*

**+Dan Kelley** I personally tested that replacement board... can you try the sig-gnd connection test with M119 on the replacement board? I guess for the X endstop is the important bit, the other ones as well. 


---
**Dan Kelley** *April 18, 2017 14:11*

**+Ray Kholodovsky**



I tried the same sig/ground test yesterday, and it produces the same results as the first board. So I can't imagine the problem lies in the board. This looks like it's a software issue now

 I'll reinstall my stock board today if possible and double check the signal coming from the endstop, and try everything on a second computer with a fresh reinstall of everything just as a control. I've got to stop by the post office today, so I can also get the first board shipped back as we agreed.



Is there anyway the pins could be mapping incorrectly in software? Should I try re-flashing smoothie to a different microSD or similar?



Side note; I did try using LaserWeb3, and an alternate USB cable, and it produced the same results, so I can be fairly certain the laserwrb installation isn't the issue.


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 15:13*

Hey Dan... hold onto things for a bit. I'll walk you through some stuff when you're available. 

You should need to do literally nothing in software nor config for M119 to recognize endstops. We test for endstop signal recognition as part of our QA process and that replacement board was personally tested by me, so yeah. Something's up. 


---
**Dan Kelley** *April 18, 2017 16:09*

**+Ray Kholodovsky** 

Sounds good to me, I'll reply here when I'm home and free for the evening.


---
**Dan Kelley** *April 22, 2017 16:51*

**+Ray Kholodovsky**, Alright, so... tested on a newly formatted PC, verified the endstop does work (video attached) just installed dependencies and configured laserweb4 with your board, still no x-endstop detection with m119 (or with endstop manually grounded), any ideas?

![images/c7a4fde892a6d20b6576dde589f818eb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c7a4fde892a6d20b6576dde589f818eb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 22, 2017 17:30*

Don't plug the board in to anything, just USB, run M119 with and without the Sig-Gnd jumper connected.  Like I said We test for this as part of our QA process and that replacement board was working properly when it left here, so let's start with that. 

What does each board do?  


---
**Dan Kelley** *April 22, 2017 18:08*

I fully understand it's not a faulty board issue, so no worries there! 



Progress! without anything connected (no jumper either), the M119 Command returns all Positive values for the endstops; X, Y, and Z are all 1.



 M119

 X_min:1 Y_max:1 Z_min:1 pins- (X)P1.24:1 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1 





With the X min jumper connected, the output changes to:



 M119

 X_min:0 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1 


---
**Ray Kholodovsky (Cohesion3D)** *April 22, 2017 18:11*

Ok great, so we are now on the same ground that the X endstop channel is being read.  

Now, if you plug in just the ribbon cable (and USB), can you also get different 0 and 1 readings with the switch pressed and not pressed? 

Remove the jumper before doing this. 


---
**Dan Kelley** *April 22, 2017 18:16*

Just ribbon cable and usb plugged in (no jumper) yields the same result as our first tests;  X and Y readouts show zero,  only the y endstop registers when moving the carriage to it's homing position (or to each endstop independently)



 M119

 X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1 

 M119

 X_min:0 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1 


---
**Ray Kholodovsky (Cohesion3D)** *April 22, 2017 18:17*

Well then. Can I get pictures of the ribbon cable, both sides of the end of it, and the socket on your stock board?


---
**Dan Kelley** *April 22, 2017 18:50*

Here's a photo dump of the whole end stop assembly, in these photos I've only flipped the electronics over horizontally, so the pins/traces are pretty easy to follow



[imgur.com - K40 Endstop assembly photos.](http://imgur.com/a/FYvV8)



Since I have I have no real hurry to make anything in the coming weeks, if you'd like me to mail some of these pieces over to you I'd be happy to do so.


---
**Dan Kelley** *May 07, 2017 16:35*

**+Ray Kholodovsky**, I've gone ahead wired in a standard Normally closed Limit switch for the X Min, and the M119 Commands now output standard results (Using the Xmin Sig and Gnd pins we previously tested),



Now nothing happens when I press "Home All" or send G28.2 , is there a pin in that  6 pin ribbon cable (now currently unplugged) that previously went to the X endstop that I need to ground or connect to another pin? I noticed that there was a 1kOhm resistor on that endstop board, and Have plenty of SMD resistors I can solder in if need be.



To summarize:

-End-stops both work (X-Min, Y-Max)

-Jogging manually works

-Running a job after manually zeroing works



-G28.2 Command does not produce any noticeable response.


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2017 16:50*

Specifically, when you M119 with the switches open, what is the response? 


---
**Dan Kelley** *May 07, 2017 16:58*

No switches pressed (carriage in middle):



X min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1 





Both switches pressed (carriage in top left corner):



X min:1 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1 


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2017 16:59*

Is your power on? Can you jog around? 


---
**Dan Kelley** *May 07, 2017 17:15*


{% include youtubePlayer.html id="UCBIr6WI8Dc" %}
[youtube.com - K40](https://youtu.be/UCBIr6WI8Dc)



Full rundown of what's going on


---
**Dan Kelley** *May 07, 2017 17:15*

Hah, what do ya know. Just after I took a video of the issue and restarted the machine, I did connected and Re-connected everything again and now it's homing as expected! But to answer your question, yes I could jog around just fine.



Anyway, how should I get that other board shipped back to you? I can drop it off at the post office tomorrow if that works. I have no issue paying the couple bucks to get it back to you tracked/insured.


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2017 17:32*

Emailed you shipping details. So which board is in play now - the original or the replacement? 


---
**Dan Kelley** *May 07, 2017 20:21*

The New board is in play, however, because I did most of the testing on the new board, he "old" board has less than 5 minutes of cutting time on it. The new probably has 3x that. I figure switching the cables and stepper drivers causes the most wear, so after I originally tested and swapped them out, I kept them that way. Does That work for you?


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2017 20:33*

Yep, keep as is and I'll do testing on the old board when it's back here. All good. 


---
*Imported from [Google+](https://plus.google.com/112962396083104042777/posts/XvR7ciyPNd6) &mdash; content and formatting may not be reliable*
