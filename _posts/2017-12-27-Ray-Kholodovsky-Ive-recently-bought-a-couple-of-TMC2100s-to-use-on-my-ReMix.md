---
layout: post
title: "Ray Kholodovsky - I've recently bought a couple of TMC2100s to use on my ReMix;"
date: December 27, 2017 21:14
category: "C3D Remix Support"
author: "Dushyant Ahuja"
---
**+Ray Kholodovsky** - I've recently bought a couple of TMC2100s to use on my ReMix; however not sure what all needs to be changed. I've seen instructions ranging from solder CFG1 to GND and cut the rest of the CFG pins to open MS1 and MS2 jumpers. Not sure what's the best option. 



Also - I'm guess I'll have to change the steps per mm as I'm currently using DRV8825s at 1/32. Let me know. 





**"Dushyant Ahuja"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2017 21:17*

The easiest way that does not involve the microstep traces on the bottom will be:



Solder this wire: [reprap.org](http://reprap.org/mediawiki/images/thumb/3/31/TMC2100_spreadcycle_topview.jpeg/180px-TMC2100_spreadcycle_topview.jpeg)



Then clip the 2 header pins for CFG1 and CFG2. This way they are not connected to VCC via the ReMix, and CFG1 is to Ground.



Calculate Microsteps as if 1/16


---
**Dushyant Ahuja** *December 27, 2017 21:56*

**+Ray Kholodovsky** perfect. Thanks



BTW - I'm in the process of supersizing my printer... not sure why :-)



![images/e607f769461feb7154e7b965582da0b8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e607f769461feb7154e7b965582da0b8.jpeg)


---
**Dushyant Ahuja** *February 01, 2018 20:24*

Thanks - installed both of them on X and Y - and the effect was startling. Didn't realise my motors were making so much noise with the DRV8825s. 

Unfortunately, I started getting skipped steps in the X axis even with the max current (1.7Amp) I could setup. Went back and checked my DRV8825s - and they were set at 2+ Amps. So, went back to the DRV8825s for now. Are there any other drivers I can use?


---
**Ray Kholodovsky (Cohesion3D)** *February 01, 2018 20:27*

I'm not sure I follow:  

You definitely need to cool your drivers.  That means heatsinks and a fan pointed on them. 

You should also use a lower current.  Did you calculate it properly with regard to the voltage measurement of the TMC potentiometer?


---
**Dushyant Ahuja** *February 01, 2018 20:29*

Have heatsinks and fan. All the literature I've read states that 1.7 Amp is the max - so set them at 1.7 Amp - but was getting skilled steps after a few layers. Not sure if that was due to over-heating or just that the motor wasn't able to push the X axis (it is a huge printer!)


---
**Ray Kholodovsky (Cohesion3D)** *February 01, 2018 20:33*

If you have set the driver to spreadycyle as discussed previously...

I would wager that you are overheating.  Try turning the current down and jogging/ doing some air prints.



Do your axes all move smoothly by hand?


---
**Dushyant Ahuja** *February 01, 2018 20:36*

the X axis is pretty heavy - waiting for a full redesign to CoreXY. Will try to adjust again this weekend. Till then am back to the DRV8825s


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/KG2RsTWnGbk) &mdash; content and formatting may not be reliable*
