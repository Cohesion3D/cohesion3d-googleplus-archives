---
layout: post
title: "Looking to see if anyone has a known working gcode file for some sort of picture larger than 5 inches squared that they can send me to test on my machine to see if its my settings or my board thats giving my problems with"
date: April 12, 2017 01:42
category: "K40 and other Lasers"
author: "tyler hallberg"
---
Looking to see if anyone has a known working gcode file for some sort of picture larger than 5 inches squared that they can send me to test on my machine to see if its my settings or my board thats giving my problems with skipping parts. Any help appreciated!





**"tyler hallberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 01:50*

**+Ariel Yahni** **+Alex Krause** **+Ashley M. Kirchner** I could use your help here guys. We need a benchmark "known working" raster - LW workspace and gCode file. So that people can just run the gCode and see if it works. 


---
**Ariel Yahni (UniKpty)** *April 12, 2017 02:01*

Don't have a workspace with me but try this image 

![images/fd27355b85edb5b8431936341fd963d6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd27355b85edb5b8431936341fd963d6.png)


---
**Ashley M. Kirchner [Norym]** *April 12, 2017 02:39*

Hitting machine in T-5 ...


---
**Alex Krause** *April 12, 2017 02:48*

I can look to see if I have something on my Google drive


---
**Alex Krause** *April 12, 2017 02:50*

Can't remember what this is but I'm sure I sent you this gcode file at one point in time **+Ray Kholodovsky**​



[drive.google.com - file.gcode - Google Drive](https://drive.google.com/file/d/0B338aPM_B6R1ZDhnbVBhMGNFY3M/view?usp=drivesdk)


---
**Alex Krause** *April 12, 2017 02:55*

For engraves I usually set my potentiometer to 7-9ma if that helps that would result in an image close to what I engraved at


---
**Ashley M. Kirchner [Norym]** *April 12, 2017 05:08*

Because that's a small PNG file to begin with (in dimensions), I would not recommend scaling that up to the requested 5 inches. It pixelates horribly. I did it at 75x75mm size. This specific picture was done at  8000 cut rate, but the zip file has 4 different gcodes in it, from 7k up to 10k. (and yes, I'm having some issues with light colored stuff, but it's still visible.)



[https://www.dropbox.com/s/d0q3gre13vrwocj/LWLogo%20Test%20CutRates.zip?dl=0](https://www.dropbox.com/s/d0q3gre13vrwocj/LWLogo%20Test%20CutRates.zip?dl=0)

![images/61a50161f7774fbc890a15f7efafd212.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/61a50161f7774fbc890a15f7efafd212.jpeg)


---
**tyler hallberg** *April 12, 2017 06:13*

Thanks for the file Alex. **+Ray Kholodovsky** it still skips, see attached photos 

![images/9bfb57de29ab1294a5afb3c43901c3e5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9bfb57de29ab1294a5afb3c43901c3e5.jpeg)


---
**tyler hallberg** *April 12, 2017 06:14*

![images/d9d13d4ca296353f107c58a752015e92.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d9d13d4ca296353f107c58a752015e92.jpeg)


---
**tyler hallberg** *April 12, 2017 06:14*

![images/d0b2fa2baf4460626014d09bd1d8ab26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d0b2fa2baf4460626014d09bd1d8ab26.jpeg)


---
**tyler hallberg** *April 12, 2017 06:15*

![images/d5056c43022ab085717c5951f3aa2ff6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d5056c43022ab085717c5951f3aa2ff6.jpeg)


---
**Joe Alexander** *April 12, 2017 06:38*

what speed are you running the file at(mm/min or mm/s)? this seems like its either belt slippage to me, or its jerking at the end which would be modifying acceleration values(Not familiar with changing these values so beware). I would check all belts and if snug then try running slower.If you have done all this already then feel free to ignore me, just trying to cover all the bases :)


---
**tyler hallberg** *April 12, 2017 13:22*

Belts have been changed, acceleration has been tried at multiple different values, current has been changed on drivers, drivers have been replaced, put in a second psu to make sure it's getting enough power, speed has been anywhere from 100-500, carriage has been squared, and multiple other things as well


---
**tyler hallberg** *April 16, 2017 03:31*

New Laserweb installed and machine runs a lot better but still skipping. Running out of ideas here. Anyone else have a known  working gcode file?


---
**Ashley M. Kirchner [Norym]** *April 16, 2017 03:52*

My dropbox link above has 4 gcode files in it, for 4 different speeds.


---
**Alex Krause** *April 16, 2017 04:31*

The pulleys are press fit :( I had to make registration marks on my rod and pulley to rule it out 


---
**Alex Krause** *April 16, 2017 04:37*

**+tyler hallberg**​ are you running the board off of the laser PSU?


---
**tyler hallberg** *April 16, 2017 05:24*

Peter you are not getting the point here. Ray wants me to try a few different gcode files before he sends out another board. EVERYTHING has been changed except the board. I have now swapped out motors from my 3d printers, have tried tighter and looser belt tension, sped up and slowed down the speed of engraving, ordered and installed new stepper drivers, got an external PSU to run just the board and motors off of, ran multiple engravings with changing the pots on the drivers, changing settings in config file to everything under the sun, wiring the board in multiple different configurations, re-squared the rails. If its anything larger than say 3 or so inches wide it skips. Solid images or some line drawings. I have ran out of time, money, and ideas on what to test and this was a last hope.


---
**Alex Krause** *April 16, 2017 05:26*

Have you fiddled with the jerk settings? 


---
**tyler hallberg** *April 16, 2017 05:26*

Skipped at two points doing 350mm/s

![images/c6b4942ddb4a6a29ccea2612cf2a0515.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c6b4942ddb4a6a29ccea2612cf2a0515.jpeg)


---
**tyler hallberg** *April 16, 2017 05:27*

Skipped at one point doing 100mm/s

![images/3fc3a7f5830cdaf0b20a6b2668bbde50.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3fc3a7f5830cdaf0b20a6b2668bbde50.jpeg)


---
**tyler hallberg** *April 17, 2017 04:07*

**+Ray Kholodovsky** unsure of what else to try


---
**Alex Krause** *April 17, 2017 04:13*

**+tyler hallberg**​ have you tried running the gcode off the sd card or getting a high quality USB cable? Also I'm not sure how sensitive your boards are to EMF noise **+Ray Kholodovsky**​ could the high voltage be causing issues? I have also heard that some people had issues based off other appliances running off the same source voltage circuits such as your furnace or hot water heater cycling on and off


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 04:21*

With the electrical noise that Alex is describing, usually that's a halt not a skip. Skipping could be a brownout when the power supply can't keep up but you've gotten an external psu. And your current on the driver potentiometers should be high enough. 

Other thoughts: upgrade to latest smoothie cnc firmware? 


---
**tyler hallberg** *April 17, 2017 12:58*

Firmware was updated last week, laser is running off its own circuit in my house, I had it ran last year when I picked up my first 3d printer, so is on its own breaker. Nothing else in house is running minus my computer and some lights when I use it. It's been ran off the SD card Ray sent with the board as well as a high class SD card, as well as trying to run it with multiple different cables to rule that out as well. 


---
**Alex Krause** *April 17, 2017 17:12*

It just dawned on me to ask are you using air assist with a coiled hose? I had some issues with the coiled hose approach that pretty much vanished when I removed it for engraving testing 


---
**Tony Sobczak** *April 18, 2017 23:05*

Following


---
**tyler hallberg** *April 19, 2017 02:03*

not using coiled hose, was one of my first thoughts as well. Removed air assist and still does it.


---
**tyler hallberg** *April 21, 2017 23:16*

Ordered some brand new stepper motors, still happening 


---
**Ray Kholodovsky (Cohesion3D)** *April 21, 2017 23:17*

Interesting. I'll have new stock of boards in the next 2 weeks, can get a new one out to you to see if these things are still happening. 


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/XpyR8YWzsWW) &mdash; content and formatting may not be reliable*
