---
layout: post
title: "Can I use the z-axis stepper driver that came with the C3D to power a z-axis stepper or do I need to go with the external stepper setup?"
date: November 29, 2017 19:03
category: "C3D Mini Support"
author: "Alex Raguini"
---
Can I use the z-axis stepper driver that came with the C3D to power a z-axis stepper or do I need to go with the external stepper setup?  I have a separate power supply for all but driving the laser.





**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 30, 2017 03:01*

It is worth trying.  The A4988 can drive Nema 17's but some Z axis need more power, and at this point, you would use the external driver. 


---
**Alex Raguini** *November 30, 2017 06:04*

I'm planning on building my own z-axis table as soon as I get all the parts sorted out.




---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/4xd8htFMiFs) &mdash; content and formatting may not be reliable*
