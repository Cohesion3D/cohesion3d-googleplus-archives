---
layout: post
title: "Has anyone designed an inclosure for the Mini?"
date: January 15, 2017 04:27
category: "General Discussion"
author: "MendingThings"
---
Has anyone designed an inclosure for the Mini?







**"MendingThings"**

---
---
**Eric Lien** *January 15, 2017 05:19*

I am going to design a compact one like I did for the Remix. Give me a few days. But if you have any interest in one that fits a raspberry Pi3 under the Mini... I have that one available now.



You can see pictures of the Pi3/mini combo case here: [https://plus.google.com/+EricLiensMind/posts/6f1LmCBtJx4](https://plus.google.com/+EricLiensMind/posts/6f1LmCBtJx4)



And here are the STL's: [https://drive.google.com/drive/folders/0B1rU7sHY9d8qZkZncHN4aDFsNms?usp=sharing](https://drive.google.com/drive/folders/0B1rU7sHY9d8qZkZncHN4aDFsNms?usp=sharing)


---
**Chris Sader** *January 17, 2017 21:40*

**+Eric Lien** getting a dead link for the STLs




---
**Eric Lien** *January 17, 2017 22:25*

**+Chris Sader** Just updated the link. Can you let me know if that works?


---
**Eric Lien** *January 17, 2017 22:28*

Here is the Step version as well in case someone want to modify them: [https://drive.google.com/drive/folders/0B1rU7sHY9d8qMXZ0RV9XVFVPM3M?usp=sharing](https://drive.google.com/drive/folders/0B1rU7sHY9d8qMXZ0RV9XVFVPM3M?usp=sharing)


---
**Chris Sader** *January 18, 2017 18:57*

Yep that worked **+Eric Lien** thanks!


---
**Eric Lien** *January 18, 2017 23:20*

**+Chris Sader** glad to help.


---
*Imported from [Google+](https://plus.google.com/107259123708340177433/posts/FG3XPxjR7S2) &mdash; content and formatting may not be reliable*
