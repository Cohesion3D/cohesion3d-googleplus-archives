---
layout: post
title: "I've got a dilemma, What to do with the GLCD display I bought"
date: September 15, 2018 22:42
category: "General Discussion"
author: "Brent Dowell"
---
I've got a dilemma, What to do with the GLCD display I bought.  On the one hand, I really like the way it works with smoothie, and being able to control the machine, jog, the display of position, etc...



On the other hand, I plan on doing a fair amount of raster engraving, and have been having problems with Smoothie due to the speed, etc.  



I really like grbl-lpc and the speed with which it can do the raster engraving.



So the question is do I include the display on my control panel and hope for a solution someday, either Smoothie gets better at raster or grbl-lpc supports the display.  Or do I just punt and stick with grbl and get rid of the display.



I don't really plan on switching firmware back and forth and would just rather stick with one.





**"Brent Dowell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 15, 2018 23:02*

FWIW we’ve spent a considerable amount of time trying to improve the raster performance in Smoothie, and also to add screen support to grbl-lpc. Both have turned out to be non-trivial and we haven’t been able to do it. 


---
**Brent Dowell** *September 15, 2018 23:09*

I have read that and certainly appreciate your efforts. I really like the board so far and have been having a blast with the whole lightburn/C3D combination. I'm just torn between the 2 options.  



I'm actually leaning towards leaving the display on my control panel and using smoothie, as I really don't 'need' the raster engraving speed, and I really do like the advantages it brings to the party.  Plus, it just looks cool.



I can always swap out the SD card if I do need to do a large raster and need to use grbl.  Time to print out a little sd card holder to mount inside the machine, I reckon.


---
**Ray Kholodovsky (Cohesion3D)** *September 15, 2018 23:33*

Appreciated. 



Search Cohesion3D on thingiverse. 


---
**crispin soFat!** *September 16, 2018 21:53*

**+Brent Dowell** Leave it.


---
*Imported from [Google+](https://plus.google.com/117727970671016840575/posts/EydnS1ctLsd) &mdash; content and formatting may not be reliable*
