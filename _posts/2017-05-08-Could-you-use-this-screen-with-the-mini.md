---
layout: post
title: "Could you use this screen with the mini?"
date: May 08, 2017 07:38
category: "C3D Mini Support"
author: "Colin Rowe"
---
Could you use this screen with the mini?

[http://www.3dpmav.com/product/3dprinter3-2-inch-full-color-touch-screenmks-tft32/](http://www.3dpmav.com/product/3dprinter3-2-inch-full-color-touch-screenmks-tft32/)





**"Colin Rowe"**

---
---
**Ashley M. Kirchner [Norym]** *May 08, 2017 08:05*

That question is better suited for the Smoothieware folks as they are the ones who write the firmware that the Cohesion3D board uses.


---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 15:40*

C3D Boards have the Serial header that something like that screen might use.  You're welcome to give it a shot.  


---
**Ax Smith-Laffin** *May 12, 2017 00:35*

Simple answer is yes, it'll work. It's jusy assigning the correct pins in the smoothie config. This display has been used with the MKS SBase board (a chinese smoothieware board) on a few Chinese 3D Printer Kits (See Tevo's Little Monster), so as long as you assign the correct pinout it'll work fine.


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/JFRQjFicJx3) &mdash; content and formatting may not be reliable*
