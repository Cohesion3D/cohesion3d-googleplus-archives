---
layout: post
title: "Current control on top of the K40 vs Laser Power settings in LaserWeb"
date: April 20, 2017 17:30
category: "K40 and other Lasers"
author: "Phillip Meyer"
---
Current control on top of the K40 vs Laser Power settings in LaserWeb.

Should I max out the current control dial on top of my K40 and limit the power used in LaserWeb or should I leave the current control set to it's usual position and use 100% laser power in LaserWeb? (or did I misunderstand this completely)





**"Phillip Meyer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 17:32*

10mA on the pot, then adjust in LW a proportion of that. 


---
**Phillip Meyer** *April 21, 2017 21:30*

Thanks


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/ezR2Ph2CAe3) &mdash; content and formatting may not be reliable*
