---
layout: post
title: "I have some issues with my 12864 smart lcd that im curious if you guys have any ideas or solutions"
date: October 07, 2018 05:27
category: "K40 and other Lasers"
author: "Brian Albers"
---
I have some issues with my 12864 smart lcd that im curious if you guys have any ideas or solutions.  The lcd displays fine until i press down to get to the menus or turn the encoder then the screen will either display gibberish or go blank and stay blank.  If I restart itll work again until i touch the encoder. The lcd will still beep as feedback when i push down on encoder when screen is blank. Its a cohesion board with smoothieware on a k40 laser.







**"Brian Albers"**

---
---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 05:34*

Sounds like the board(s) are in a bind or shorting down on something.... i think... make sure the glcd is not bound up or the mounting hardware isnt squeezing it. Maybe pull the display and let it rest on a nonconductive surface and see if it behaves. The control knob has been problematic for me before on a 3d printer i had. I dropped 1 drop of wd40 down the shaft, spun it awhile and pressed it awhile, then blew it out with canned air to remove excess wd40


---
*Imported from [Google+](https://plus.google.com/109986797182285961346/posts/e6R9GkvYz9a) &mdash; content and formatting may not be reliable*
