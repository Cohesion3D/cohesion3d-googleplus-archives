---
layout: post
title: "Can I use the cohesion3d laser controller on a DIY system using a lightobject 400 X 300 xy stage (x speed 1500mm/s y500mm/s (microstepping set to 3200) A 45 watt tube with a 40 - 60 watt laser power supply?"
date: August 17, 2017 21:12
category: "General Discussion"
author: "Denver Thomas"
---
   Can I use the cohesion3d laser controller on a DIY system using a lightobject 400 X 300  xy stage (x speed 1500mm/s y500mm/s (microstepping set to 3200)

A 45 watt tube with a 40 - 60 watt laser power supply? Or is it just for k40 only.





**"Denver Thomas"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 17, 2017 22:06*

The Cohesion3D will drive a 3D Printer, CNC, or any number of Lasers (CO2 or Diode).  Fundamentally you just need to drive the XY motors and run a PWM signal to the power supply, which the board does.  



I believe that the A4988 drivers in the laser bundle should drive the Nema 17's in this guy: [lightobject.com - High speed PR430 400*300 XY stage DIY laser kit](http://www.lightobject.com/High-speed-PR430-400300-XY-stage-DIY-laser-kit-P1009.aspx) at 1/16 MicroSteps (per your 3200 figure). 



However if you want to go super fast or add a Z table or Rotary, the board supports all this but you will probably need to wire external drivers at this point (like TB6600 or DQ542MA) 



I think you can start with just the laser bundle: [http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



Add the GLCD Adapter and GLCD if you'd like a status screen and the ability to run jobs without needing a computer tethered.  Our users find this way preferable when it comes to reliability on larger more complex jobs.  



You may also want to add a set of our external stepper adapters in case you need to add external drivers to run your machine.



And it doesn't look like the Lightobject XY system comes with endstops,  you may find having some handy.



These instructions will hopefully be helpful and show what all the wiring may look like.  As I mentioned the board does have the k40-specific connectors, but also the normal motor, endstop, power, mosfet, etc... so that you can wire any machine.  



[https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



[https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



Cheers! Let me know if you have any other questions.


---
**bbowley2** *August 18, 2017 14:37*

Do you think the C3D mini will work for this laser?

[amazon.com - Robot Check](https://www.amazon.com/Laser-Cutter-Engraver-Without-Rolling/dp/B00OL0AARI/ref=sr_1_16?s=arts-crafts&ie=UTF8&qid=1503066618&sr=1-16&keywords=60W+Laser+engravers)


---
**Ray Kholodovsky (Cohesion3D)** *August 18, 2017 15:01*

**+bbowley2** 

Typically these might have the ruida controller and external stepper drivers which we have done the conversion for before, so yes that would work. 

It depends entirely on what electronics are inside it though, which can vary. 


---
**bbowley2** *August 18, 2017 21:11*

thx.




---
**bbowley2** *August 19, 2017 01:16*

I got this info from the distributor.  

I can't go back to raster engraving vector files.

I hope your controller will work.



Select the software matches the motherboard

The software can support three kind of motherboards:RDLC430、RDLC320RDC633X.

Both RDLC320 and RDC633X are 

compatible, but RDLC430 is different. So, please choose the corresponding

software with motherboard. Otherwise, it does not work. 


---
**bbowley2** *August 19, 2017 13:17*

Here is the wiring diagram for the 50W laser I want to run with my C3D board and LaserWEB. 

![images/8295a14deadedaca175555076bfe0b3a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8295a14deadedaca175555076bfe0b3a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 20, 2017 19:04*

**+bbowley2** Looks like Ruida and External Stepper Drivers.  I went through this with John Hunter here: [plus.google.com - I just purchased a 100w 700x500 laser. I also have purchased a cohesion3d, a...](https://plus.google.com/u/0/105510960839020211759/posts/f8tjg9TWi7y)



You'll need the laser bundle and the external stepper adapters to wire the external stepper drivers (black boxes) in that machine. 


---
*Imported from [Google+](https://plus.google.com/101806672126327395289/posts/KRmvWvpxrWM) &mdash; content and formatting may not be reliable*
