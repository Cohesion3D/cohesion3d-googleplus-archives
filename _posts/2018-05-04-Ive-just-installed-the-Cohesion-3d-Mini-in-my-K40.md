---
layout: post
title: "I've just installed the Cohesion 3d Mini in my K40"
date: May 04, 2018 23:27
category: "C3D Mini Support"
author: "David Piercey"
---
I've just installed the Cohesion 3d Mini in my K40.  



I've got my LEDS as follows:



VMOT solid Red

3V3 solid green (very hard to see as VMOT red is so bright)

* L1 No light at all

L2 Blinking Green

L3 Blinking Green

L4 Solid Green.



Laser won't fire.

Lightburn can't connect.



The laser in the K40 will not fire.



Without the K40 powered on (only USB), the same things happen, without the VMOT lighting up. 



Using a backup power-supply sees the exact same thing happening.



Sage advice and wisdom is much appreciated!



(My K40 uses the connections shown here: [https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5082656267/original/PzWmQw1xIbx5LEX30uTJ386yVToxe1Tigw.jpg?1494290803](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5082656267/original/PzWmQw1xIbx5LEX30uTJ386yVToxe1Tigw.jpg?1494290803) and I've plugged the 3 plugs in and the USB as shown in this image)







**"David Piercey"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2018 23:54*

Is the memory card in the board?  


---
**David Piercey** *May 05, 2018 00:10*

It is.  



I can't read it at all on my computer, though.






---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 00:11*

What do you mean?


---
**David Piercey** *May 05, 2018 00:12*

The card is in the board.  If I remove it an check it on my computer, it will not read the card.


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 00:16*

Ok, it's possible it got corrupted.  Do you have a different card you can try to use?  Just need to format it FAT32 and put the config and firmware files on it - these are located in a dropbox at the bottom of the install guide. 


---
**David Piercey** *May 05, 2018 00:30*

**+Ray Kholodovsky** I used my 32GB card (not a permanent solution), and now have all lights as expected.



error:Alarm lock is constant in the console, and I can't move anything via lightburn. 


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 00:32*

Are you using the stock K40 USB Cable? Don't.


---
**David Piercey** *May 05, 2018 00:44*

**+Ray Kholodovsky** I've switched between 3 different USB cords, including the stock.



I get the same results: 

![images/6904e79e79668d0a76295bf6b085212a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6904e79e79668d0a76295bf6b085212a.png)


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 00:54*

Send M999 in console to clear the alarm. 


---
**David Piercey** *May 05, 2018 00:54*

**+Ray Kholodovsky** Switching the port from auto to Com 8 (the suggested one) lets me get a bit farther, but still no motion occurring.  I get this in the console, and then nothing while it finishes sending.





![images/221bff178f07fd5e75d56505d2697bf4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/221bff178f07fd5e75d56505d2697bf4.png)


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 01:03*

Change Start From, from current position to absolute coordinates. You may be trying to go out of bounds. You have homed the head, yes? 


---
**David Piercey** *May 05, 2018 01:07*

**+Ray Kholodovsky** I have done this. Rebooted it (powering down, unplugging USB).  



It is intermittently controlling the X position (not by the movement controls in lightburn, though, but by having it START a small design. No Y movement.


---
**David Piercey** *May 05, 2018 01:12*

**+Ray Kholodovsky** My mistake, I can now control my X position in lightburn.



I still have zero motion in my Y positioning (from starting a program or using the direction controls in lightburn).



When I hit HOME, it homes the X, though doesn't recognize when to stop and spends 10-20 seconds vibrating like crazy trying to go beyond where physically possible (moving left).


---
**David Piercey** *May 05, 2018 01:21*

**+Ray Kholodovsky** I have tried absolute positioning.  It does the same thing. Hitting home pushes the head far left (correct), but keeps doing so for that 10+ seconds, even if homes again right after homing once.


---
**David Piercey** *May 05, 2018 01:45*

Reconnecting the stock board, the Y axis works correctly.  Switching back to the cohesion 3d Mini, (with everything powered down), full reboots of computer...



X axis control is also failing.  and home no longer works.


---
**David Piercey** *May 05, 2018 04:03*

Wiring pics (from power supply to C3D Mini) 1/5

![images/96b96f44b76168647b7898293ed2203c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/96b96f44b76168647b7898293ed2203c.jpeg)


---
**David Piercey** *May 05, 2018 04:03*

2/5

![images/3c6b3f1eecf134789e26549fe3498501.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c6b3f1eecf134789e26549fe3498501.jpeg)


---
**David Piercey** *May 05, 2018 04:04*

3/5



![images/f3c14d7584acbb46a63ba8e16b6da524.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f3c14d7584acbb46a63ba8e16b6da524.jpeg)


---
**David Piercey** *May 05, 2018 04:04*

4/5

![images/7d9011738f8138796b13fc97ccd90142.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7d9011738f8138796b13fc97ccd90142.jpeg)


---
**David Piercey** *May 05, 2018 04:05*

5/5

![images/7522ee64f5eac9e43bb5f77d86412c3a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7522ee64f5eac9e43bb5f77d86412c3a.jpeg)


---
**David Piercey** *May 05, 2018 05:44*

After leaving it for a while, powered down, computer, laser cutter, etc, powering back on - there is zero movement possible now using the C3D mini board.



* note: the original stock board works


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2018 18:12*

Yeah, something's up.  We can send you a replacement - please send us an email or fill out the contact form on the website with your order # and reference to this thread.


---
**David Piercey** *May 07, 2018 22:25*

**+Ray Kholodovsky** Thanks for you assistance navigating this - and for providing a solution.






---
*Imported from [Google+](https://plus.google.com/114972156206786232028/posts/ZocNmJoSySp) &mdash; content and formatting may not be reliable*
