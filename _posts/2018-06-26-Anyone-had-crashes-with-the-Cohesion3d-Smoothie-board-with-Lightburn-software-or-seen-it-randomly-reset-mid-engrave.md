---
layout: post
title: "Anyone had crashes with the Cohesion3d Smoothie board with Lightburn software or seen it randomly reset mid-engrave?"
date: June 26, 2018 17:22
category: "K40 and other Lasers"
author: "Chris Garrett"
---
Anyone had crashes with the Cohesion3d Smoothie board with Lightburn software or seen it randomly reset mid-engrave?



I am using the supplied USB cable and SD card.



When it works it is great, but only 1/4 runs complete. When it fails it gets to a part of the engrave then throws the laser head to the bottom of the Y and the stepper grinds until I turn the whole thing off.





**"Chris Garrett"**

---
---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:25*

wow. disconnects have been reported sometimes but not the erratic movements. just to clarify, you say supplied usb; supplied with the k40 or by cohesion3d? 


---
**Chris Garrett** *June 26, 2018 17:29*

**+Tech Bravo** sorry, supplied with the board from Cohesion3d - it's one of the purchase options when you fill the cart


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:32*

and your origin is set at front left in the device setup? the arrows in the move panel jog the head in the correct directions?


---
**Chris Garrett** *June 26, 2018 17:36*

**+Tech Bravo** no, top left 




---
**Chris Garrett** *June 26, 2018 17:36*

**+Chris Garrett** the arrows do jog correctly




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:37*

okay. is this happening with one specific file or different files?


---
**Chris Garrett** *June 26, 2018 17:37*

**+Tech Bravo** two different files, one SVG and the other just squares drawn directly in the Lightburn UI


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:40*

the disconnects have been know to be caused by electrical line noise (capacitor start motors starting, refrigerator compressors turning off, etc...),noise along the usb cable, and other anomalies. how long have you had your c3d mini board?


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:47*

making sure your pc, laser, etc... are on a branch circuit that has no cap start motors, heaters, ac's, and stuff like that can help. mine would reset in the middle of a job randomly for months. i finally figured out that it wasn't random. every time my mini fridge compressor turned off it would kill my job LOL. 



Depending on how old your firmware is, there is a new version that disables the windows mass storage device so your microSD will not show up as a drive in windows anymore. this is rumored to help with weird com issues.


---
**Chris Garrett** *June 26, 2018 17:49*

**+Tech Bravo** just installed it this weekend - I will check out the new firmware! There is a freezer also in the garage, think a UPS will help with line issues?



Thanks!




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:50*

i forgot to ask, is your k40 work area stock (set to 300x200 in lightburn?) and have you made any modifications to the config file at all?


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:51*

i can provide instructions on loading the newest firmware and config. if you like. if your bed size is different that will need to be addressed. 


---
**Chris Garrett** *June 26, 2018 17:53*

**+Tech Bravo** all default as far as I can recall :)




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:56*

a ups possibly would, yes. try unplugging your freezer for awhile and running jobs to see if thats it. should stay frozen for a few hours :) may need to move it to another branch on another leg or even run a dedicated circuit on the opposite leg of what the laser/pc are on. i have had copiers go crazy before and moving to the other phase helped.


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:56*

here is batch 3 firmware and config: [dropbox.com - C3D Mini Laser Batch 3 - Provided Config](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AADLn0lRlfl7f3y-IrX5fVWIa/C3D%20Mini%20Laser%20Batch%203%20-%20Provided%20Config?dl=0)




---
**Chris Garrett** *June 26, 2018 18:00*

**+Tech Bravo** thank you!




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 18:03*

No prob. If you cannot see the microsd card in windows as a drive (with the usb plugged in an machie powered on) then you may already have the newest firmware. The only change was disabling mass storage support.


---
**Chris Garrett** *June 26, 2018 18:11*

**+Tech Bravo** it definitely shows as mass storage because messages were popping up about it :)




---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2018 20:29*

Yes, try that latest batch 3 - it should help reliability. 


---
**Chris Garrett** *June 26, 2018 20:31*

**+Ray Kholodovsky** I just copy the files to a FAT formatted SD card and insert?




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 20:38*

Yes.


---
**Chris Garrett** *June 26, 2018 20:41*

**+Tech Bravo** cool, thank you :)


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 20:42*

Ooops. I'm not Ray. LOL


---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 20:44*

I have this habit of getting a notification, seeing a question, and answering without reading fully or even knowing the answer ;) JK


---
**Chris Garrett** *June 26, 2018 20:50*

**+Tech Bravo** you have been very helpful even if you are not Ray :D


---
*Imported from [Google+](https://plus.google.com/+ChrisGarrett/posts/hVZrQ6hvwZD) &mdash; content and formatting may not be reliable*
