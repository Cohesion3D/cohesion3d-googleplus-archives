---
layout: post
title: "Should have my Cohesion board In two more days"
date: April 08, 2017 10:58
category: "C3D Mini Support"
author: "Brian Keith"
---
Should have my Cohesion board In two more days. I got the hookup for the screen but not sure what screen to get or what exactly that does for me? (Other than screens look cool). Any other gadgets I will watch and see what everyone likes.....







**"Brian Keith"**

---
---
**Eric Lien** *April 08, 2017 12:26*

I think **+Ray Kholodovsky**​ gives some examples here: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions)-



And saw a cool LCD Mount recently here: 



[http://www.thingiverse.com/thing:2228964](http://www.thingiverse.com/thing:2228964)


---
**Eric Lien** *April 08, 2017 12:27*

One advantage of an LCD is you could run the gcode from an SD card and not be tethered to a PC.


---
**Bob Buechler** *April 08, 2017 17:18*

I picked up the GLCD adapter and screen, and it works fine, but I don't really know what value I'm getting out of it. The only screen I get is a laser positioning detail screen. The pot adjusts a percentage indicator of some kind, that I just leave at 100. The machine has a digital power panel which I've set at 80% (max power ceiling to prevent running the tube too high), and from there I control power per job within LW4. ... But what good is the GLCD when LW4 also gives me positioning data? I'm probably just missing something.


---
**Bob Buechler** *April 09, 2017 21:29*

Seems silly, but given that I'm new to these RepRapDiscount GLCD control panels, I didn't actually know that the knob was also a push button. So when the panel powered on and gave me the laser status screen and I had no menu, I didn't know how/if I could do more with it. This morning I decided to play around with it more after thinking about this thread, and tried pushing on the dial knob -- viola! independent control. **+Ray Kholodovsky** -- super basic/n00b thing, but it might be worth adding a line about how the GLCD works in your C3D instruction docs.  


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 21:33*

Noted. 


---
**Brian Keith** *April 14, 2017 10:45*

Also noticed there is a white sticker that says something about remove after washing? Not sure what that is and there is a push button switch next to that. I am in the process of cutting out a mounting board to mount the Glcd and Milliameter is not. I am not sure about a hole for that small push button or the sensor w the sticker ( speaker)




---
*Imported from [Google+](https://plus.google.com/107283426075343411605/posts/9ft5TZSbEHt) &mdash; content and formatting may not be reliable*
