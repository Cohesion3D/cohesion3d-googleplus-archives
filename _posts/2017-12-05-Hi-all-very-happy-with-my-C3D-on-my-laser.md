---
layout: post
title: "Hi all, very happy with my C3D on my laser"
date: December 05, 2017 11:46
category: "FirmWare and Config."
author: "Colin Rowe"
---
Hi all, very happy with my C3D on my laser. I now have a rotary axis and need to connect to the board. I have the breakout board and the microstep driver to run it, I have sorted out the Z and all works OK. Problem is A axis, I have read i need to compile a new firmware and I have tried loads to do it (running win10)

I have some software that I use for my drones and when I click on the bin file it opens up that software. 

Is there a version of your firmware i could download that would activate the A axis. 

Thanks in advance

PS using the firmware that came with the board from you





**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 05, 2017 13:19*

**+Ashley M. Kirchner** do you have a Cnc build for rotary floating around? 



If not I'll dig up one of mine. 


---
**Ray Kholodovsky (Cohesion3D)** *December 05, 2017 18:15*

Courtesy of Ashley: [drive.google.com - Smoothie Firmware - Google Drive](https://drive.google.com/open?id=0B8NExy6DoX2RcV9VaFE1bWduS1U)



There's your firmware file, and start with config-XYZA-no_endstops.txt



Make sure you rename that to config.txt and put it on the card, dismount, reset, etc. 


---
**Colin Rowe** *December 05, 2017 18:15*

Thanks will try tomorrow 


---
**Carl Fisher** *December 05, 2017 19:58*

Ray, I assume this will work for me as well right? 




---
**Ray Kholodovsky (Cohesion3D)** *December 05, 2017 19:59*

Sure why not?


---
**Carl Fisher** *December 05, 2017 20:00*

Just making sure it was fit for my early generation mini :)




---
**Ray Kholodovsky (Cohesion3D)** *December 05, 2017 20:01*

Ah.  This is for the "new" wiring method so if you're running a pwm cable you'll have to fiddle the new config file back into submission


---
**Carl Fisher** *December 05, 2017 20:02*

ok. So firmware good, config file needs some tweaking. Cool.


---
**Colin Rowe** *December 06, 2017 06:53*

OK, SO loaded ok and shows 4 axis on screen when loaded  but "A" is missing of the jog and touch screen, d0 I need to add something in the config file? I use my laser a lot as a stand alone


---
**Colin Rowe** *December 06, 2017 09:02*

I have A working now when connected to laserweb4. I have A active in settings and will step clicking the button.

Problem 1

There is no reading on the screen when A is moving.

I swapped over the motor to Z axis to see if it was a wiring problem and it was not getting feedback, but the readout on z works so I assume it must be in the config file somewhere?



Problem 2

Laserweb4 posts the prog with A active and looks ok, but when I run the program, it does the x move then stops on the first A move, I think its waiting for a signal to tell it where it is.

I assume both problems are related.

Any help please.




---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/cLpMzdn71DJ) &mdash; content and formatting may not be reliable*
