---
layout: post
title: "Ray Kholodovsky on the mini headers for the drivers, is there a preferred A1, A2, B1, B2 for the stepper pinout?"
date: December 12, 2016 02:42
category: "General Discussion"
author: "Carl Fisher"
---
**+Ray Kholodovsky** on the mini headers for the drivers, is there a preferred A1, A2, B1, B2 for the stepper pinout? I'm trying to hook up my rotary in place of the Y-axis just to test and it didn't seem to like that that order. It just stuttered.



I'm wondering if I need to play with the wiring or drop down to a smaller stepper since this thing came with a big Nema23.









**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 02:48*

The pins go B1, A1, A2, B2.

Please try that order.  Alternatively please try a lower speed and make sure the current on the driver is turned up.  


---
**Carl Fisher** *December 12, 2016 02:52*

I was just doing a jog from the control panel at whatever the default speed is. However if that's the pin out then that would explain what I'm seeing.



I'll try to get back out there one night this week.


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 02:52*

You'll likely want to do a manual G0 command with a much lower feedrate, like F60


---
**Carl Fisher** *December 12, 2016 02:53*

Ok, will try it. Thanks.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/QoZ7TMX3z3C) &mdash; content and formatting may not be reliable*
