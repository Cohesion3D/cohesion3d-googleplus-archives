---
layout: post
title: "Are Cohesion3D boards readily available now? Looks like I fried my Smoothieboard shortly after getting it installed and working in my K40"
date: April 10, 2017 12:34
category: "General Discussion"
author: "Kelly Burns"
---
Are Cohesion3D boards readily available now?   Looks like I fried my Smoothieboard shortly after getting it installed and working in my K40.  





**"Kelly Burns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 10, 2017 16:20*

The Mini sold out and is backordered for a few weeks.  ReMix is in stock, and if you already had the Smoothieboard in your K40 then it seems like you've got the wiring already set up that you don't need the "special k40 connectors" on the Mini and could use standard motor and endstop headers (same white connectors as on Smoothieboard) and screw terminals for power and L. 


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/XRJ7hUMcRzH) &mdash; content and formatting may not be reliable*
