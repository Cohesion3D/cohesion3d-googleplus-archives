---
layout: post
title: "What type of connector should I get to hook up to the 4 pin power connection on the mini?"
date: January 19, 2017 12:55
category: "General Discussion"
author: "Nathaniel Swartz"
---
What type of connector should I get to hook up to the 4 pin power connection on the mini?





**"Nathaniel Swartz"**

---
---
**Griffin Paquette** *January 19, 2017 13:54*

Are you using it in a K40 or a printer?


---
**Nathaniel Swartz** *January 19, 2017 14:00*

Sorry, I'm upgrading a K40


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 15:40*

Pictures of the machine power supply and cable in question please? 


---
**Nathaniel Swartz** *January 19, 2017 16:00*

Connector on the right on the power supply:

![images/7853fc9f0bcf32b389c58a66cf1090e4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7853fc9f0bcf32b389c58a66cf1090e4.jpeg)


---
**Nathaniel Swartz** *January 19, 2017 16:01*

This is the moshi board connector.  Currently it's a 6 pin, so I wanted to replace it with a four pin  so it doesn't overhang so much.  I could also just dremel off the two extra spaces.

![images/e19510a58c83ce4151edc0df4b869e56.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e19510a58c83ce4151edc0df4b869e56.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 16:05*

Ok, this is now the 3rd one of these I'm seeing. Can you confirm that we're looking at 24v, gnd, and L coming from the PSU? 


---
**Nathaniel Swartz** *January 19, 2017 16:21*

Yup, that's correct.  The power supply also has a 5v line, but it was never connected


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 16:26*

Great. We don't want 5v anyways, the Mini makes its own. Check out the pinout diagram I posted last night. 

2 options: modify the cable down to 4 pins and set it up per the diagram with those 3 things. 

Or, cut the 6 pin end off and run the wires to the main power input terminal and we can use a different MOSFET like Bed - for the L wire. 


---
**Nathaniel Swartz** *January 19, 2017 16:46*

I guess shaving the 6 connector down is the best way to go, I was just hoping to find the same connector on mouser or digikey and replace the whole cable.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 16:52*

Just please please verify the pin order on both ends. 



Check for JST VH. That's what the 4 pin connectors are. 


---
**Nathaniel Swartz** *January 19, 2017 19:53*

Thanks!  I knew the ground and 24v were flipped, sadly I broke a crimp connector while attempting to remove it form the housing.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 19:58*

Sounds like it's time to cut some wires and use those screw terminals. 

Btw, I've made note of this and for the next version made the L in the power connector a pwm capable pin so either wiring option should just be a simple config change. 

But for now, gotta fiddle with that wire :)


---
**Nathaniel Swartz** *January 19, 2017 21:10*

Gotcha, so I'd just update my config to have 2.5 instead of 2.6 for switch.laserfire.output_pin? I assume 2.6 is the L pin on the k40 power? (Didn't see it

 on the diagram)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 21:12*

Negative.  

You would update the laser pwm pin line which currently reads 2.4!

and change it to 2.5 (without the exclamation).  

The Mini's 4 pin power connector, L is routed to 2.6 thru a mosfet, but yes it is 2.6 fundamentally. 

With that change, the laserfire no longer applies. And you would use the pot to set the current ceiling, and the pulsing going to L would set a proportional percentage of that.


---
**Nathaniel Swartz** *January 19, 2017 21:37*

So I'd remove the 3 pin cable running from pwn on the psu and going to the pwm k40 area on the mini.  Hook back up the original 3 pin cable the runs from the psu to the pot.  then connect the L connection from the psu to 2.5 screw in terminal and update my config for the laser pwm pine line, but leave the laserfire alone as it's not used?  Any advantage to doing this instead of getting a replacement terminal connector and using the standard k40 power pins, and k40 pwm pins on the mini board?


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 21:42*

Sounds right.  

I do recommend using a different mosfet, hence - bed 2.5 for L.  As for the power in, either way is fine.  24v and gnd is the same at both the mini power in terminal and at the k40 style power connector on the mini.


---
**John Sturgess** *January 20, 2017 08:24*

**+Ray Kholodovsky** I'm surprised its only the 3rd, Moshiboards have been in the K40s for years and its only recently (last 2 years ish) theyve started  using the M2 Nanos


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 16:49*

**+John Sturgess** well most of the machines that we tested on were from the last 2 years :)


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/LzJ5GAEjiHW) &mdash; content and formatting may not be reliable*
