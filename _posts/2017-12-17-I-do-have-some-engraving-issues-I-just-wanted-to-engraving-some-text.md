---
layout: post
title: "I do have some engraving issues. I just wanted to engraving some text"
date: December 17, 2017 12:16
category: "K40 and other Lasers"
author: "Oliver Berger"
---
I do have some engraving issues. I just wanted to engraving some text.

The text gets engraved but some letters or parts of some are missing. Mostly the ones at the left and right ends of the text.

I do understand how to cut stuff and that works flawlessly.

But engraving is completely new to me. Also don't understand what the source file format should be. Although i saved my text as svg in inkscape, but in laserweb it doesn't even show. I used then PNG which worked but as i wrote letters are not complete.

Also what which power and speed settings do i have to configure in laserweb ?





**"Oliver Berger"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 17, 2017 19:23*

If you have a file not loading into LaserWeb properly, you can post it to the LaserWeb g+ group to see what they think. 



We will need more details such as pictures of the problem and the file to assist more. 



New software Lightburn will be released in 2 weeks, keep an eye out for that. 


---
**Joe Alexander** *December 17, 2017 23:04*

did you make sure to go object to path after typing in your text? this works for me when that happens.


---
*Imported from [Google+](https://plus.google.com/117529861190108076281/posts/DZ3G3HEQyuM) &mdash; content and formatting may not be reliable*
