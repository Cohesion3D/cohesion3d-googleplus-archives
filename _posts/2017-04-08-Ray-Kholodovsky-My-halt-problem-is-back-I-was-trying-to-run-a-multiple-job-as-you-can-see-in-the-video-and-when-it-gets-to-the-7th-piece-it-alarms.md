---
layout: post
title: "Ray Kholodovsky My halt problem is back, I was trying to run a multiple job as you can see in the video and when it gets to the 7th piece it alarms"
date: April 08, 2017 19:45
category: "C3D Mini Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky** My halt problem is back, I was trying to run a multiple job as you can see in the video and when it gets to the 7th piece it alarms. I have checked everything over as usual but once again I'm at a loss.



I was running this from my mac as up until now I haven't had this problem, I am still on LW3 on here as not managed to get LW4  running; at last check all I got was a white screen.

 

Any ideas where I go with this as everything was fitted new with the C3D ie Psu Endstops, C3D and Tube etc.

[https://drive.google.com/open?id=0B4PVxTUHzQtfc0lSSDRGT1RIeXM](https://drive.google.com/open?id=0B4PVxTUHzQtfc0lSSDRGT1RIeXM)





**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 19:53*

Do you remember this bit: [smoothieware.org - mac-drivers [Smoothieware]](http://smoothieware.org/mac-drivers)



I think we should set you up so that the memory card doesn't mount. This should require a special build of the smoothie firmware and a change in the config.txt 



Bob was explaining this a while back and I tagged you to it or some such. 


---
**Andy Shilling** *April 08, 2017 19:59*

Yes I always unmount the sd card on here now, thats why I presumed I never had the problem. Has there been any updates to the firmware since the first batch were released?



Could it be the stepper motors/ drivers not getting enough power? ie The pots on the A4988s? 


---
**Andy Shilling** *April 08, 2017 20:22*

**+Ray Kholodovsky** I've just been out and tried the unmount command in terminal and and neither of them actually unmount the sd card even with sudo command, I'm not to great with terminal but even I'm sure I did that right.




---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 20:52*

Most certainly.    We use the firmware-cnc.bin here:  [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin)



Please try updating with the latest of that one (download it properly, put it on the card, safely eject, and reset the board). 


---
**Andy Shilling** *April 15, 2017 12:20*

**+Ray Kholodovsky** Hello buddy I'm back off holiday and looking at updating this firmware. Am I right in saying I just need to add the bin file to a sd card and it will self update. 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 15:38*

Latest firmware-cnc.bin, rename to firmware.bin. Put on card, safely eject/ unmount, and reset the board. 


---
**Andy Shilling** *April 15, 2017 16:05*

Lovely thank you buddy. I'll let you know if it makes any difference.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/bBNFbJJH7CU) &mdash; content and formatting may not be reliable*
