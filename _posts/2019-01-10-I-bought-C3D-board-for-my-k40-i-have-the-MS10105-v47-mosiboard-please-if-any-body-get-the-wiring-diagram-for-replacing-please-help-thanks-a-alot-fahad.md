---
layout: post
title: "I bought C3D board for my k40 i have the MS10105 v4.7 mosiboard please if any body get the wiring diagram for replacing please help thanks a alot fahad"
date: January 10, 2019 11:23
category: "General Discussion"
author: "Alalaiwi Trading"
---
I bought C3D board for my k40 i have the MS10105 v4.7 mosiboard 

please if any body get the wiring diagram for replacing please help thanks 

a alot fahad

![images/847ea6b9c472472c4baadb2bddb79bb3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/847ea6b9c472472c4baadb2bddb79bb3.jpeg)



**"Alalaiwi Trading"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2019 16:29*

The same Moshi board can be wired at least 10 different ways. We need to see pictures of the board and wiring in your exact machine. 


---
**Just Josh rfpom** *January 10, 2019 18:33*

Not sure if your board is wired the same way as mine but I did a video on how I swapped my MS10105  v4.6 for the Cohesion 3D LaserBoard. 
{% include youtubePlayer.html id="sFvJXdITaw4" %}
[youtube.com - How to Replace the MS10105 Board (K40)with a Cohesion3D Laserboard - K40 Laser Upgrade](https://www.youtube.com/watch?v=sFvJXdITaw4)

Ray is right though pictures of your setup are best.


---
**Alalaiwi Trading** *January 12, 2019 13:16*

**+Just Josh rfpom** 

![images/016c893d04138b6283179dd10d658e34.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/016c893d04138b6283179dd10d658e34.jpeg)


---
**Alalaiwi Trading** *January 12, 2019 13:19*

Hi thank you for your reply this is the wiring in my k40 thanks 


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 19:21*

Read the install guide at [cohesion3d.com - Getting Started - Cohesion3D](http://cohesion3d.com/start) - you will see similarities to your wiring. 


---
*Imported from [Google+](https://plus.google.com/116298037506357376785/posts/UKdTgN6K6fW) &mdash; content and formatting may not be reliable*
