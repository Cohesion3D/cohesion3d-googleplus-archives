---
layout: post
title: "I just installed the C3D Mini and I'm having an issue where the controller will respond to a few commands from Lightburn, but will get stuck shortly after"
date: February 13, 2018 04:43
category: "C3D Mini Support"
author: "Mike De Haan"
---
I just installed the C3D Mini and I'm having an issue where the controller will respond to a few commands from Lightburn, but will get stuck shortly after. Lightburn says the K40 is "Busy" and will no longer respond to any commands until the system is completely shutdown and restarted.



When I start up, the board has a red led on top, L1 and L4 are solid green, and L2 and L3 are blinking green.



The system will stay alive long enough to "home" and draw half a circle when I submit gcode to it. After that, the system is stuck in "Busy" mode.



I have replaced the USB cable twice to the same effect.



Any advice would be greatly appreciated.





**"Mike De Haan"**

---
---
**Mike De Haan** *February 13, 2018 05:10*

Wiring picture![images/2fd0af1ac2d58a731e7f9f18694dff73.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2fd0af1ac2d58a731e7f9f18694dff73.jpeg)


---
**Mike De Haan** *February 13, 2018 05:13*

Lightburn when the system is stuck.![images/29b484821fda7344ca400c56c3474d10.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/29b484821fda7344ca400c56c3474d10.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 13:25*

What is the make of the computer you are using? What is the operating system? 


---
**Mike De Haan** *February 13, 2018 13:52*

Dell Windows 7 pro


---
**Mike De Haan** *February 13, 2018 14:40*

It appears to be limiting the number or size of gcode it will interpret. If I try to draw a circle, the game is over after about half. If I try to draw several rectangles, I can get several done before it gets stuck again.



I don't know if this limit is the number of gcode instructions, or size of the gcode file....though the two pretty much go hand in hand.


---
**Jim Fong** *February 13, 2018 15:30*

**+Mike De Haan** There is no practical limit with LightBurn gcode generation.  I’ve made raster scans that are hundreds of thousands of lines of gcode with my C3d board.   LightBurn is still in beta so there could be a issue but it has been pretty solid for me.   Makes sure you have the latest version.  



A previous user who had similar issues had changed to a different PC and that had fixed his issue.  You may want to try that if you have one available.  Hard to  narrow down windows USB issues since the many different usb chipsets and drivers that have been made over the years.  


---
**Mike De Haan** *February 13, 2018 15:48*

**+Jim Fong** Right, I'm not implying that Lightburn has a limit. My point is just to demonstrate the behavior of my laser. Something is definitely stopping the rest of the gcode instructions from completing.


---
**Mike De Haan** *February 13, 2018 15:50*

I just tried using LaserWeb but I'm having even less progress there. I can connect to the Cohesion board, but it disconnects after a few seconds with "No supported firmware detected". Perhaps this is a clue?





![images/5fd0b9cfba7294955106bf69e8d5716d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5fd0b9cfba7294955106bf69e8d5716d.png)


---
**Jim Fong** *February 13, 2018 16:34*

**+Mike De Haan** FYI. I had this same disconnect issue when I was using Laserweb about a year ago.  PC was a brand new Dell i7 desktop with Windows7pro.  No matter what I did (reinstall drivers, new USB card etc), I could not get that machine to work with Laserweb.    I knew it wasn’t the C3d card since had no problems with 2 windows7 laptops.  (Dell and HP).  



The dell desktop came with a Win10 upgrade so crossed my fingers and let Windows do the upgrade.  After the install was finished, had no problems whatsoever with connecting to the C3D board.  Weird stuff sometimes.   Same PC now runs LightBurn  


---
**Mike De Haan** *February 13, 2018 20:45*

**+Jim Fong** unfortunately, the only other machine I have is a Windows XP box. I'm having even less luck with that one. It won't even "Home".


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 20:45*

On this Windows 7 machine, you did install the smoothie drivers, right?


---
**Mike De Haan** *February 13, 2018 20:46*

**+Ray Kholodovsky** yes, I installed version 1.1 for win64 Windows 7


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 20:52*

Perhaps grbl-lpc firmware would work better for you... 



[dropbox.com - firmware_grbl-lpc_3axis_c3dmini](https://www.dropbox.com/sh/vjtzgc2y1q9ytd1/AADFZ52Eneu_nDkHSChY7jsla?dl=0)



Just put this file on the card, safely eject it, and reset the board.



Read the full article: [https://cohesion3d.freshdesk.com/solution/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/solution/articles/5000740838-grbl-lpc-for-cohesion3d-mini) including the linked github for notes on the other driver you have to install. 






---
**Mike De Haan** *February 13, 2018 21:19*

I put this firmware on the card, safely ejected, and reset the board. The LEDs did as they were supposed to, but now I cannot access the drive or the board. The serial port is not recognized anymore.



You mentioned notes on the other driver I have to install, but I haven't found anything. I'm guessing I need a new USB driver. My Device Manager is complaining about an unknown USBSerial device. Do you know where I can find this new driver? I tried the following drivers, but they did not work:



[ftdichip.com - Virtual COM Port Drivers](http://www.ftdichip.com/Drivers/VCP.htm)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 21:21*

Search for "driver" on this page: [github.com - cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)



The file is located here: [https://github.com/cprezzi/grbl-LPC/tree/master/VCOM_lib](https://github.com/cprezzi/grbl-LPC/tree/master/VCOM_lib)


---
**Mike De Haan** *February 13, 2018 21:41*

ok...got the driver. I'm still unable to see the sd card drive (MSD), but I was able to connect to the board with LaserBurn. The same problem persists. I can draw part of a circle before the whole system is hosed. I can't even "Home" again when it's in this state.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 21:50*

And one last thing... in Lightburn, Devices, you've now selected Cohesion3D (Grbl) instead of Cohesion3D (Smoothie) like you had before? 


---
**Mike De Haan** *February 13, 2018 21:51*

**+Ray Kholodovsky** correct


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 21:55*

[pattersonsupport.custhelp.com - Disable Power Management on Windows 7 Machines](https://pattersonsupport.custhelp.com/app/answers/detail/a_id/14917/~/disable-power-management-on-windows-7-machines)



Give this a shot. 


---
**Mike De Haan** *February 13, 2018 23:32*

This appears to have done it! I was able to run three successful test runs while clicking "Home" between each run.



My only issue now is the Y-axis is flipped. The system will home to the upper left correctly, but something that should be at the top of the cutting bed is instead cut at the bottom....Any Text I try to engrave is upside-down.



Is there an easy way to flip this?


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 23:36*

Go back to smoothie for now.  There's a dropbox link at the bottom of the instructions with the config and firmware file.  You'll need a microsd card reader to access the card since grbl doesn't show it to the PC like smoothie does. 



And this is a semi-known thing with dell computers, the last bunch of times I've had comms issues reported have all been Dell's and something around power save mode. 



But the actual answer to your question is that you've probably selected the wrong origin in LB Device setup, it should be the bottom left corner. 


---
**Mike De Haan** *February 14, 2018 00:14*

The Y-Axis change worked! Thank you!



I switched back to smoothie, however, the half circle problem came back. (I remembered to switch the profile in LightBurn back to Smoothie).



I switched again to GRBL and the problem went away. I'm not sure what's causing the problem with smoothie, but for now I'm up and running with GRBL at least. Should I be concerned that smoothie is not working? Is GRBL sufficient to move forward with? Am I missing out on anything important?


---
**Ray Kholodovsky (Cohesion3D)** *February 14, 2018 00:18*

GRBL is good, enjoy yourself!


---
**Mike De Haan** *February 14, 2018 00:20*

Thank you so much. I sincerely appreciate your time and patience!


---
**Richard Vowles** *February 18, 2018 09:15*

![images/ad5bc3b320226db8227b6b2d54c42d1a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ad5bc3b320226db8227b6b2d54c42d1a.jpeg)


---
*Imported from [Google+](https://plus.google.com/104087193624871551721/posts/ShH55U7viBf) &mdash; content and formatting may not be reliable*
