---
layout: post
title: "The cuts on my K40 are really wrong this started last night I'm using lightburn and c3d"
date: January 09, 2019 15:14
category: "General Discussion"
author: "Timothy \u201cMike\u201d McGuire"
---
The cuts on my K40 are really wrong this started last night I'm using lightburn and c3d

![images/0389d0682cb3297143ce8583fa6a54d4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0389d0682cb3297143ce8583fa6a54d4.jpeg)



**"Timothy \u201cMike\u201d McGuire"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 17:46*

More info would help, please read the pinned post in this community about how to ask for help effectively. 


---
**James Rivera** *January 09, 2019 18:25*

My first thought whenever I see something like this is: how fast are you trying to go?!? (read: steppers have lost steps)


---
**Ned Hill** *January 09, 2019 19:18*

Looks like it could be a belt or pulley issue.  Check and make sure your belts have enough tension and also check the pulleys to make sure one hasn't become loose.


---
**Timothy “Mike” McGuire** *January 09, 2019 21:30*

I reformatted the SD card and Flash the firmware again it seems to be fine now I'm going to check the belts and pulleys later just to make sure thanks for your quick responses it's appreciated


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/aUzkmtzyLss) &mdash; content and formatting may not be reliable*
