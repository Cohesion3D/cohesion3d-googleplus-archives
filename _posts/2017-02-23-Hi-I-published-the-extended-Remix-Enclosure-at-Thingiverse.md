---
layout: post
title: "Hi, I published the extended Remix Enclosure at Thingiverse"
date: February 23, 2017 17:00
category: "Show and Tell"
author: "Marc Pentenrieder"
---
Hi, I published the extended Remix Enclosure at Thingiverse.

Hope it is useful to you all ;-)



Feedback is always welcome.



[http://www.thingiverse.com/thing:2131630](http://www.thingiverse.com/thing:2131630)



Special thanks to **+Eric Lien** and **+Ray Kholodovsky**





**"Marc Pentenrieder"**

---
---
**Samer Najia** *February 24, 2017 20:27*

This is beautiful!  Gonna make one right away


---
**Marc Pentenrieder** *February 25, 2017 18:32*

**+Samer Najia** please tell me, if everything fits as expected.


---
**Samer Najia** *February 25, 2017 18:35*

Hi Marc, sorry, I ended up not moving forward as my Printrbot wanted 16 hours to print the top part at 20% infill at 80mm/s.  That was far too long and it made more sense to stick with my existing design/remix  

![images/4680dbbed0a87509dbbcecfe338852f9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4680dbbed0a87509dbbcecfe338852f9.jpeg)


---
**Marc Pentenrieder** *February 25, 2017 18:40*

No prob. Yours also look awesome


---
**Samer Najia** *February 25, 2017 18:41*

How long did yours take to print?  What settings did you use


---
**Marc Pentenrieder** *February 25, 2017 19:29*

I didn't print it. My printer is too small and the big one isn't ready ;-(


---
**Samer Najia** *February 25, 2017 20:01*

Bummer.  Anyone try it?


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:01*

Yep, printed the base for Marc. ![images/0b4cec3eae2c54d1615d791376b9bb16.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0b4cec3eae2c54d1615d791376b9bb16.jpeg)


---
**Samer Najia** *February 28, 2017 13:10*

Let me how long the top takes


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:19*

Haven't done the top. 


---
**Allen Russell** *March 01, 2017 23:50*

Great job, what program did you use to make this.


---
**Marc Pentenrieder** *March 02, 2017 20:34*

**+Allen Russell** I am using Fusion 360


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/NJpVubPoBBf) &mdash; content and formatting may not be reliable*
