---
layout: post
title: "Poor engraving quality Today i switched to C3D board"
date: February 25, 2018 14:56
category: "K40 and other Lasers"
author: "J\u0101nis Kar\u0146ickis"
---
Poor engraving quality



Today i switched to C3D board. 

But i have trouble getting good engraving quality.

Added some pictures - before and after C3D upgrade.



Any solution?





![images/2b7bf220caf724bed9b167e90b16fc07.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2b7bf220caf724bed9b167e90b16fc07.jpeg)
![images/ed08fae4697bfa3bf9a4f9406ff71abf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed08fae4697bfa3bf9a4f9406ff71abf.jpeg)

**"J\u0101nis Kar\u0146ickis"**

---
---
**Jim Fong** *February 25, 2018 17:04*

What software are you using to control the laser?


---
**Jānis Karņickis** *February 25, 2018 17:26*

**+Jim Fong** LightBurn


---
**Jim Fong** *February 25, 2018 17:32*

Within LightBurn, there are a few different raster scanning options besides dither.  You may want to try each one.



If you can, post your image and I’ll run a test on my laser.


---
**Jānis Karņickis** *February 25, 2018 18:05*

**+Jim Fong** Here you go

[drive.google.com - test.lbrn](https://drive.google.com/open?id=1c2u0BkCnB-UnlYt4_SDLfZrXoo59JZmB)



It`s not an actual image.

Export from CorelDraw


---
**Jim Fong** *February 25, 2018 18:42*

**+Jānis Karņickis** 



Ok it was a vector scan file.  I stopped it before it finished.  Only mod I made was to lower the speed to 25mm/sec.  the power is a little high so got to much smoke but it came out ok. 



Smoothieware on C3d/lightburn. 



You might want to check to see if your mirrors are clean ![images/ffa220284ec639289a646023838d6695.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ffa220284ec639289a646023838d6695.jpeg)


---
**Jim Fong** *February 25, 2018 18:45*

At faster scanning speeds, the lens may be shaking. Check to see if everything is tight. 


---
**Jānis Karņickis** *February 25, 2018 18:45*

**+Jim Fong** Nice work! Realy looks great.

I will try in few days.




---
**Jānis Karņickis** *March 02, 2018 16:28*

**+Jim Fong** tried today, mirror was shaking and speed to fast.

Thanks agein for the tip!


---
**Jim Fong** *March 02, 2018 17:19*

**+Jānis Karņickis** glad it is working!!


---
*Imported from [Google+](https://plus.google.com/117995752420452028846/posts/7GxEV2gQQJ1) &mdash; content and formatting may not be reliable*
