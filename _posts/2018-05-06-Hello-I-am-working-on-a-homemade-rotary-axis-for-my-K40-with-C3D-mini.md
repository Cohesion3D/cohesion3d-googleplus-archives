---
layout: post
title: "Hello - I am working on a homemade rotary axis for my K40 (with C3D mini)"
date: May 06, 2018 20:36
category: "C3D Mini Support"
author: "Shawn Gano"
---
Hello - I am working on a homemade rotary axis for my K40 (with C3D mini).  I have installed the 4-axis firmware from the directions on the website and have gotten it to move using Lightburn.  But I don't see any A-axis position information on the LCD nor can I find a way to jog the axis from the LCD.  By chance is there a newer version of the firmware that supports the A-axis from the LCD interface?  The main reason I am asking is that I am having a hard time figuring out how to correctly position the A-axis to start the engraving at the desired location on the rotating material (an A readout position would be helpful in figuring this out).  Thanks!





**"Shawn Gano"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2018 20:53*

There is no A jog in smoothie.  



I did some quick reading:



The relevant code is in this folder:



[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/src/modules/utils/panel/screens/cnc)



Here's the layout:

Watch screen is what's normally on the screen. 

MainMenuScreen is the first thing when you click the encoder button.

When you select jog there it is DirectJogScreen - start fiddling here.



If you find AXIS_SELECT here: [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/DirectJogScreen.cpp](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/DirectJogScreen.cpp)  



I think that would be a good starting point. 





JogScreen is the one where you actually do the moving and I do not think any changes are required there.



It might even be possible to add A coordinates to the watchscreen: [https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/WatchScreen.cpp#L118](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/WatchScreen.cpp#L118)



[https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/WatchScreen.cpp#L182](https://github.com/Smoothieware/Smoothieware/blob/edge/src/modules/utils/panel/screens/cnc/WatchScreen.cpp#L182)










---
**Shawn Gano** *May 07, 2018 20:53*

Thanks for the pointers in the firmware code Ray.  I will attempt to figure it out in a different manner but if I need to go that route that will help get me started.


---
*Imported from [Google+](https://plus.google.com/106830536917902704175/posts/3Z5EgHwie15) &mdash; content and formatting may not be reliable*
