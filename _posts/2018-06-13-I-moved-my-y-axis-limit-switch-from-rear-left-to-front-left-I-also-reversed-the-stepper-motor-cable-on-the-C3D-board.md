---
layout: post
title: "I moved my y-axis limit switch from \"rear left\" to \"front left\" I also reversed the stepper motor cable on the C3D board"
date: June 13, 2018 23:42
category: "C3D Mini Support"
author: "David Ewen_P"
---
I moved my y-axis limit switch from "rear left" to "front left"



I also reversed the stepper motor cable on the C3D board.



Both axis home correctly, but the Y-axis won't jog past 100mm and when I jog the X-axis, both X & Y jog.



Can anyone help?



![images/1e5adfaa77b6af36ae84128ba295c651.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e5adfaa77b6af36ae84128ba295c651.png)
![images/84ec81546b3057a042abc85d220572f6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/84ec81546b3057a042abc85d220572f6.png)

**"David Ewen_P"**

---


---
*Imported from [Google+](https://plus.google.com/109639692587843616301/posts/EwAV9cmQYV4) &mdash; content and formatting may not be reliable*
