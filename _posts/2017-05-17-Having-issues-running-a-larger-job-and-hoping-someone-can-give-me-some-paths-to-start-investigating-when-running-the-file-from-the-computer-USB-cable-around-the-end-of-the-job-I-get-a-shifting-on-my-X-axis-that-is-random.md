---
layout: post
title: "Having issues running a larger job and hoping someone can give me some paths to start investigating, when running the file from the computer/USB cable, around the end of the job I get a shifting on my X axis that is random"
date: May 17, 2017 03:07
category: "C3D Mini Support"
author: "John Milleker Jr."
---
Having issues running a larger job and hoping someone can give me some paths to start investigating, when running the file from the computer/USB cable, around the end of the job I get a shifting on my X axis that is random (see two instances in the same cut attached) but always finishes the job. When I run the same file from the SD card, it locks up the Cohesion3D Mini tight and in two cuts, both the same spot each time. Have to kill power, even a reset won't free the board.



At first I thought it might be the heat sinks, they're warm when the problem happens, but I wouldn't say they're hot. One job I left the door of the electronics open and turned on the ceiling fan and it made absolutely no difference where the job locked up.



I've even tried looking in the gcode around the area where the lockup is occurring and there's no apparent corruption or anything that should be causing the freezing. 



If it matters, I'm using an SVG file from Thingiverse, exported as a PNG after editing in Inkscape. I have NOT tried a new file but have no reason to believe my source files are corrupted in any way. Opened in LaserWeb4 and set to laser raster. Power 5, Laser Diameter 0.05 (0.075 causes gaps, I have a HQ lens and mirrors installed) and a cut rate of 150. Trim and Join pixels enabled plus Burn White turned on. I doubt the beam width could be causing it, but I may go back to 0.1. Could having a diameter of 0.075 or 0.05 be causing memory problems?



I thought I timed the job near 30 minutes, but I believe the 25:41 on the frozen LCD is the duration. Can post my config (I don't believe I've changed anything there but I'll check) as well as my GCode. 



Are there any reports of problems with the SD Cards being faulty? I remember getting two 3D printers in a row that had funk cards. Thanks for any insight!



I'm thinking of swapping out the drivers (I have two extra) and trying a new SD card so that I can keep troubleshooting.



![images/9f7c80c73165d8e69e3f54a1ae770055.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9f7c80c73165d8e69e3f54a1ae770055.jpeg)
![images/6c0695ae8187375f7f1e02cd9c4cf344.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c0695ae8187375f7f1e02cd9c4cf344.jpeg)

**"John Milleker Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 03:23*

Let's start with the skipping. The troubleshooting guide describes how to increase the current on the drivers. I would do 45 deg clockwise to start.  


---
**John Milleker Jr.** *May 17, 2017 03:34*

Ray, I did just adjust the pot 45 clockwise just to test it out. I've never had problems thus far and never figured I needed to adjust it.



Also treated the board to a fresh 32GB retail brand SD Card. Even downloaded the latest firmware-cnc from Smoothie and reverted to my backup config file. Running again from the board and will update in about 25 minutes.



The X Axis sounds different, almost dull and muted on certain areas. Might be a good thing. Thanks!




---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 03:44*

Laser diameter is also very small. Usual range is 0.1 - 0.5 but you said something about a different head so ok. 

I haven't had any reports of weird cards. Smoothie is not unknown for sd lockups with some 3d Printing people, but I haven't seen anything here yet.  There's always the new grbl-lpc firmware for this board :) 


---
**John Milleker Jr.** *May 17, 2017 03:56*

The firmware was the same version as you gave me on my new board, even though the website said it was last updated 22 days ago. We're three minutes from the 'trouble spot' on the cut. Crossing my fingers.



I might be just misunderstanding what I'm seeing, but I've really dialed in calibration. Had to install shims under the laser. The new lens and mirrors. When I'm at the right height and engraving a raster at 0.1 I'm leaving spacer lines. Even at 0.075 I've got small lines.


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 03:57*

The firmware is Smoothie CNC build from January. The smoothieware github has a newer version by now, sure. 

The grbl-lpc is a different firmware that the LW crew ported. It will also run on this/ smoothie boards. So options exist. 


---
**John Milleker Jr.** *May 17, 2017 03:59*

Damn, and I was hopeful. Full freeze again. Felt the heat sinks, warm again but not hot.



I only adjusted the pot on X, I guess I should have done Y at the same time. I just matched Y as well and will be running another test. I did order an extra driver and don't have the Z or the extra installed, I could switch them out if this doesn't help. 



This IS the same gcode file, but I don't have anything else that I know taxes whatever is going on enough to cause the issue.

![images/42ea172e1c40e9a91959ea50740729fe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/42ea172e1c40e9a91959ea50740729fe.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 04:01*

It's not the driver heatsinks, you can stop worrying about that. Perhaps try 0.1mm beam size, and/ or a different file, and see what other data points you can gather. 


---
**John Milleker Jr.** *May 17, 2017 04:32*

Yeah, died again two seconds sooner this time. It seems like things start getting a little hesitant around 60%. I'm going to swap out the drivers for giggles and also re-create the file in a different drawing program to generate a new gcode file from. Thanks for the help bud, I'll update steps and hope to be able to give good news soon.



Hoping it's not the beam diameter, keeping that attempt for last.


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 04:33*

Cool. Test more with this. May want to look into the grbl-lpc at some point. Inquire at the LW community. 


---
**John Milleker Jr.** *May 17, 2017 05:09*

Thanks Baked, I'm a sharpness junkie and have yet to learn intentional defocusing. It's on my list though as photo engraving is the main reason I bought the machine.




---
**John Milleker Jr.** *May 17, 2017 05:56*

Fixed.. For now. You guys had it. Driver swap did nothing to fix the issue (but I had to try anyway). Re-creating the graphic to engrave at 0.05 did nothing. Generating the new re-created graphic at 0.1 diameter and I get a successful run. It didn't run nearly as long as the original problem files, but at this point would rather suspect maybe running out of memory. The old gcode files were 6.9 and 7.4mb while the 0.1 diameter file is 2.5mb.



Running the old graphic at 0.1 now. If I get a second wind maybe I'll see if it's a problem with the size of the file vs. the actual parameters.



Thanks guys!



Ray - I failed trying to find a resource that compares Smoothie vs. Grbl-lpc. Why would I want to upgrade to that firmware vs. Smoothie? 


---
**John Milleker Jr.** *May 17, 2017 14:36*

Thanks Peter, I'll look into it! What would the vector workflow look like for a full toned photo engrave? The only reason I was converting this test cut to raster was because I was having errors with the text in Inkscape. I was converting the text to a path but was having inconsistencies with the size.



Over the night I snuck the diameter back down while the file size crept back up and got it down to 0.06mm before failing. I'm suspecting more that there's a memory limitation on the board.



And I'm suspecting more that I'm going to need to wire in a Z-bed so that I can get crisp text, crisp cuts and then defocus to let the feathering of the photo rastered cut do it's job like Baked Bean mentioned.






---
**Joe Alexander** *May 18, 2017 02:20*

from what I have heard grbl-lpc seems to engrave smoother without the "stuttering" that the normal firmware-cnc seems to lag on.  I ported over once to try it but i must have done it wrong as it didnt work.(but it works on my arduino so i know the code is good)


---
**John Milleker Jr.** *May 18, 2017 03:55*

That's good to know, thanks Joe! Engraving is something I want to focus on, so it sounds like something I will move to as soon as I have everything working and a good understanding.


---
**Joe Alexander** *May 18, 2017 04:01*

i do a lot of cutting just tired of setting my machine to 300mm/s(knowing it can handle it) and only getting like 40mm/s on lines with constant intensity changes. 


---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 04:23*

Joe has an MKS of some sort. Not sure if it has digipots or not, I think it might though.  Certainly earning his keep in the C3D Community though :) 


---
**Joe Alexander** *May 18, 2017 04:36*

nope im running a smoothieboard 4xC ray :) standard pot and ammeter with digital voltmeter added to pot. although if you send me a free mini i wouldn't object... ;)


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/UENkSYr7L3d) &mdash; content and formatting may not be reliable*
