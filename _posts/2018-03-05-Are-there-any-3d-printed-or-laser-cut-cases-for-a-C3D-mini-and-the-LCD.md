---
layout: post
title: "Are there any 3d printed or laser cut cases for a C3D mini and the LCD?"
date: March 05, 2018 23:49
category: "General Discussion"
author: "Dar Thw"
---
Are there any 3d printed or laser cut cases for a C3D mini and the LCD?





**"Dar Thw"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 04:56*

We only have this case design for 3D Printing, with a Pi3 in there, but there are drawings and CAD models available of the Mini if you want to whip up your own. 


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 04:56*

[drive.google.com - Mini WIth Pi3 Combo Enclosure - Google Drive](https://drive.google.com/open?id=0B1rU7sHY9d8qczM4dnhMTFZuZDg)


---
*Imported from [Google+](https://plus.google.com/107923441357901969822/posts/FvUScZBBLB6) &mdash; content and formatting may not be reliable*
