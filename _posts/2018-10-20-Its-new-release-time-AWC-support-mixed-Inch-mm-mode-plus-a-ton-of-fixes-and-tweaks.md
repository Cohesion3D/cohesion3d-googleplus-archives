---
layout: post
title: "It's new release time: AWC support, mixed Inch/mm mode, plus a ton of fixes and tweaks"
date: October 20, 2018 04:31
category: "Other Softwares"
author: "LightBurn Software"
---
It's new release time:

AWC support, mixed Inch/mm mode, plus a ton of fixes and tweaks.



[https://lightburnsoftware.com/blogs/news/lightburn-0-8-04-awc-support-mixed-inch-mm-mode-ai-importer-fixes-and-much-more](https://lightburnsoftware.com/blogs/news/lightburn-0-8-04-awc-support-mixed-inch-mm-mode-ai-importer-fixes-and-much-more)





**"LightBurn Software"**

---
---
**ThantiK** *October 20, 2018 13:31*

Nice, just got paid so I'll be actually purchasing this soon :D


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/JXUCkNbzhe6) &mdash; content and formatting may not be reliable*
