---
layout: post
title: "Hey, guys! I just switched to GRBL on my C3D Mini board and its much, much better so far"
date: May 21, 2018 13:39
category: "FirmWare and Config."
author: "Bklyn Ghost"
---
Hey, guys!

I just switched to GRBL on my C3D Mini board and its much, much better so far.

One thing that I haven't fully fund any clear info  is Z bed homing. I am able to move my Z bed up and down without problems, but when I try to home it, Laserweb gives me a a very long error box that the command is not supported by the firmware. Is there a different GRBL firmware that works with a Z end stop? I got mine from [https://github.com/cprezzi/grbl-LPC/releases](https://github.com/cprezzi/grbl-LPC/releases). Also, while I'm here, I had a similar question about Lightburn, I had a trial version and it was pretty good, but it would update laser head coordinates when it feels like it, even when I clicked the "update coordinates" button and I couldn't find a way to home my Z bed even back when I was running Smoothie firmware. I'd like to start using Lightburn software, because it works much smoother than laserweb, but homing Z is a vital part of my operations.  Any suggestions?

By the way, I'm running Windows 7 64 bit



![images/7b05e3550cfdffe7a0945b2b1c19260e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7b05e3550cfdffe7a0945b2b1c19260e.jpeg)



**"Bklyn Ghost"**

---
---
**Jim Fong** *May 21, 2018 13:52*

Custom version of grbl-lpc for xyz homing.  Smoothieware needs a minor configuration change in the config.txt to support z homing.  Check the docs on howto. 



Download and rename to firmware.bin. Copy to sdcard and flash the c3dmini



You may have to change the grbl configuration for z homing direction. It would depend on where your limit switch is located.  Grbl1.1 docs will explain under Homing and Limit switches 





[dropbox.com](http://dropbox.com) - firmware grbl-lpc c3dmini 4axis with XYZ homing.bin


---
**Jim Fong** *May 21, 2018 16:45*

Link



[dropbox.com - firmware grbl-lpc c3dmini 4axis with XYZ homing.bin](https://www.dropbox.com/s/zuhn3varf6zde99/firmware%20grbl-lpc%20c3dmini%204axis%20with%20XYZ%20homing.bin?dl=0)


---
**Bklyn Ghost** *May 21, 2018 17:06*

**+Jim Fong** Thank you so much for the link, I flashed this firmware and the error is gone. I've configured the directions and Z actually moves when I send a homing command, but it completely ignores the limit switch. My switch is mounted to the top of the bed, I know I'm missing something in my settings.  

$$

 $0=10

 $1=255

 $2=0

 $3=3

 $4=0

 $5=1

 $6=0

 $10=0

 $11=0.010

 $12=0.002

 $13=0

 $20=0

 $21=0

 $22=1

 $23=5

 $24=70.000

 $25=100.000

 $26=250

 $27=1.000

 $30=1000

 $31=0

 $32=1

 $33=5000.000

 $34=0.000

 $35=1.000

 $36=100.000

 $100=160.000

 $101=160.000

 $102=3200.000

 $103=160.000

 $110=24000.000

 $111=24000.000

 $112=60.000

 $113=24000.000

 $120=2500.000

 $121=2500.000

 $122=2500.000

 $123=2500.000

 $130=300.000

 $131=200.000

 $132=50.000

 $133=100.000

 $140=0.600

 $141=0.600

 $142=0.000

 $143=0.000


---
**Jim Fong** *May 21, 2018 17:22*

Check to see if the z limit is connected right and if grbl sees it.  Type ? In the console will show the state of the switches 



The default grbl-lpc endstop limit switches is high so it will show

Pn:PXYZA when typing in ? Status 



The respective endstop will disappear from the status when activated low


---
**Bklyn Ghost** *May 21, 2018 18:16*

**+Jim Fong** Just checked it with PuTTY, when no switches are engaged, I'm getting Pn : PZA, When I press the switch on Z table, it returns Pn : PZ, does that mean that GRBL thinks that my Z switch is A? The X and Y show up when pressed.


---
**Jim Fong** *May 21, 2018 18:29*

**+Bklyn Ghost** looks like your z limit is plugged into the A limit connector.  Change it. 


---
**Bklyn Ghost** *May 21, 2018 20:08*

**+Jim Fong**  My board didn't have limit switch pins for A, but I moved the switch from Zmin to Zmax and now it recognizes everything, but now I'm having a trouble because the XY switches are stock K40 ones and the Z came with my C3d board. It appears that XY are Normally open and the Z is normally closed, or vice versa. When I change the value of $5, The homing sequence recognizes the Z switch when pressed and proceeds to look for XY, but it thinks that the switches are already pressed. Is there a way to reverse the pins just for the Z switch?


---
**Jim Fong** *May 21, 2018 20:16*

Change $5=5 and see what happens 



Oh ya no A limit.  I’m at work and don’t have the board in front of me. Going by memory. 


---
**Bklyn Ghost** *May 21, 2018 20:37*

**+Jim Fong** I tried doing that, unfortunately it only accepts 0 and 1. Would I have to modify config.h file? I found this page [github.com - grbl](https://github.com/gnea/grbl/blob/master/grbl/config.h) with advanced settings which seems like exactly what I would need (lines 198-204) But How do I even get to config.h? I don't remember seing that file on the SD card after flashing grbl for the first time.


---
**Jim Fong** *May 21, 2018 20:46*

That’s right it’s all or none.  You can’t mask $5 



The other way is to see if the actual z switch has a normal open connection that you can hook up instead. 



I would have to look at the source code later to see if it can be modified to work for you. 



Any modifications to the config.h would need a re-compile of the source code.  


---
**Bklyn Ghost** *May 21, 2018 22:37*

Yeah, I started digging deeper about modifying the source code and the whole process looks like it would be way beyond my capabilities. I do miss the simplicity of Smoothie in terms of configuring it, but it was not stable enough for rasters for some reason and I've wasted a lot of material/time with it. I don't have any other switches laying around to replace the current ones. I hope you can come up with something, if not I guess I would have to replace the switches with three identical ones. 


---
**Jim Fong** *May 21, 2018 23:12*

**+Bklyn Ghost** if it is a mechanical switch. see if it has 3 terminals on it.  The third one will be the normally open connection. 



If not, it would be easiest to just change the one Z switch to normally open.  Don’t mess with the X and Y limit switches. 


---
**Jim Fong** *May 21, 2018 23:20*

What problems did you have with smoothieware anyway? 


---
**Bklyn Ghost** *May 21, 2018 23:42*

It is one of those red PCB makerbot switches with 4 wires. Looking at the board it has NC and NO printed underneath the connectors, I wonder if I could possibly switch the wires on it, or desolder it from the board and use just that. I don't want to mess with the switch pins unless I am absolutely sure, because from what I've read if connected wrong, it could fry the board. That means I would have to wait for a new board and they are on backorder currently and I use the machine for my business. 

Smoothie did well with vector cuts, but rastering was problematic. The X would shift during engraving, I've tried re-generating the G-code, using different file formats, taping the workpiece down to the table, checked the belt tension, cable, different computer etc. It would work fine for a while and then bam, shifts left right in the middle of the job.  I wasn't using too high of a speed, I don't think somewhere within 250mm/s. The movement was very jerky as well, Grbl is so much smoother, quieter and faster. 


---
**Jim Fong** *May 21, 2018 23:53*

**+Bklyn Ghost** of you can, just move the wire that is connected to the NC terminal to the NO terminal. 






---
**Bklyn Ghost** *May 22, 2018 01:03*

**+Jim Fong** That did the trick!

Turns out the XY endstops were wired as NC and the Z switch is NO. I soldered the wires on the XY switches to NO and voila, she's homing. Now I have to configure homing speeds and all, but that's easy. 

Jim, thank you so much for all of your help. Your time and advice are highly appreciated.


---
**Joe Alexander** *May 22, 2018 14:12*

it might be worth trying smoothie again with the sd card sharing disabled, some have have had luck with smoother engraving and less stuttering this way. theres a line in the config file named disablemsd that needs enabling, will disable the ability to access the sd card through the usb cable.


---
**Jim Fong** *May 22, 2018 18:10*

**+Joe Alexander** you still need a special version of the firmware found here to use the disablemsd config switch or the one I compiled which permanently disables the sdcard access. 





[forum.smoothieware.org - Troubleshooting - Smoothie Project](http://forum.smoothieware.org/troubleshooting#toc28)




---
**Jim Fong** *May 22, 2018 18:13*

My version of the disablemsd



[plus.google.com - Apologize for the long post but need explaining.... I’ve always had random h...](https://plus.google.com/114592551347873946745/posts/CHzEkdbrhKe)


---
*Imported from [Google+](https://plus.google.com/107889273848361712520/posts/TDgy9QCRgeF) &mdash; content and formatting may not be reliable*
