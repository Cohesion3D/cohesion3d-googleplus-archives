---
layout: post
title: "Ray Kholodovsky been a while since I have done anything with your board"
date: September 28, 2017 08:07
category: "C3D Remix Support"
author: "Alex Hayden"
---
**+Ray Kholodovsky**​ been a while since I have done anything with your board. Trying to get it working. It connects fine, I can see the firmware and config files. Changed a few settings in the config file to set limits and such. This is for a 3d printer. Connected to it with pronterface. It will let me heat the hotend but that is all. None of the movement commands move the steppers. I am powering with 12v. Should I switch to 24v? I doubt that will fix my problem. I am fairly sure I have the wiring done correctly. Not sure how to set the pots on the drivers correctly. The way I did it on my old printrbot was send a move command and dial them till I got a good smooth movement. I assume you can turn off the smoothieboard default config for current settings because the driver boards use pots. The only other thing I can think of that could be wrong is maybe the motor wiring. I wired them the same way as my printrbot which I thought should work. It is setup phase 1 then phase 2 but the a and b pins might be out of order. I saw some people talking about measuring the vref to set the pots correctly, how do you do that? These are 1.3a motors. Hmm just thought of another way I could set my pots I have a simple movement board I could tune each one on that then move them back to the ReMix board. I can send you the config file if that will be helpful. I don't think I broke it, but who knows, it is 3am here. 

![images/ac55d602ba1afce2d98c67b7038064c2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ac55d602ba1afce2d98c67b7038064c2.jpeg)



**"Alex Hayden"**

---
---
**Alex Hayden** *September 28, 2017 08:13*

Here is the config and firmware files.

[dropbox.com - COHESION3D](https://www.dropbox.com/sh/g3x7ixm0gvp69wf/AACAYCSPewPcl3a0K0eNW99za?dl=0)


---
**Claudio Prezzi** *September 28, 2017 09:59*

My Remix board starts with alarm state, so I have to cancel the alarm (or home) before I can move the steppers.


---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2017 12:33*

3 independent power inputs on Remix. You should probably power the top block for motors :) 



Please consult the pinout diagram on our docs site 


---
**Alex Hayden** *September 28, 2017 14:11*

Wow I feel dumb. Thanks **+Ray Kholodovsky**​ should have mains power hooked up not mosfet. And mains will power mosfet without me conneting to that power input correct?


---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2017 14:13*

You need all 3 power inputs connected to do all the things.

The top one is the motors and that's where the regulators make the logic voltages from. 

Then the heated bed mosfet has its own input.



Then the bottom one is for the hotend/ fan/ peripheral mosfets.  



If you don't need extreme amounts of current then just run your power input to bed input and use small links of wire to bridge out from there. 


---
**Alex Hayden** *September 28, 2017 14:48*

**+Ray Kholodovsky**​ thanks alot. I got movement. Yaaa. 


---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2017 16:40*

I use spades like so. But it's possible to put one wire on each side of the screw per terminal. 

18awg silicone wire is great for a typical printer where you're at a 10 amp heatbed, and about 6 - 8 amps for everything else. ![images/f9695fba46157f88158bebd96c4b3774.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f9695fba46157f88158bebd96c4b3774.jpeg)


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/dg6LTjsTUEW) &mdash; content and formatting may not be reliable*
