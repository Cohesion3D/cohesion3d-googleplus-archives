---
layout: post
title: "Hi all. Just installed my Cohesion3D board into my Chinese K40 laser cutter"
date: April 16, 2018 04:41
category: "C3D Mini Support"
author: "Cammo T"
---
Hi all.  Just installed my Cohesion3D board into my Chinese K40 laser cutter.

I am having problems with the Lightburn software finding a default home position on start up.  I start up the software, it detects the laser cutter and starts to move the head to the home position which should be rear left. Once it gets there it keeps trying to move the X axis which keeps banging into the Y rail.  After it stops (around 10 seconds) I press the find coordinates in the Lightburn software and it says X=0, Y=200 



Does it sound like I have something configured wrong and or is there an issue with the machines limiter?  Any help would be much appreciated.





**"Cammo T"**

---
---
**Anthony Bolgar** *April 16, 2018 09:12*

Sounds like you have a bad limiit switch+


---
**Cammo T** *April 16, 2018 10:53*

**+Anthony Bolgar** Thanks Anthony, is that something that can me replaced?  As I believe my machine has Optical Endstops.


---
**Anthony Bolgar** *April 16, 2018 10:55*

Search this group for endstops, I think there is a howto for the optical stops problem.




---
**Don Kleinschnitz Jr.** *April 16, 2018 12:26*

**+Cammo T** 

See if this helps. If the endstops are bad its usually that the interposer has hit and damaged the sensor.

Did they work with the stock controller?



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Optical%20Endstops)


---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2018 14:45*

Are you sure?  What it's supposed to be doing is going to the switches then backing off and touching again at a much slower rate.  This can sound like "groaning" and sometimes people think it is hitting the end.  When homing is successfully finished, it should indeed report back that it is at 0, 200 coordinates. 



You can issue the M119 command from Lightburn Console as you press each switch manually to test that they are working/ being read. 


---
**syknarf** *April 16, 2018 21:03*

Coordinates are ok. Lightburn takes 0,0 at front left (As most of the cnc/laser software does), the K40 have the limit switches at rear Left.


---
**Cammo T** *April 17, 2018 12:00*

Thanks guys.  Yes they did work with the stock controller but since then I have modified the machine a bit so I might have bumped/damaged the sensors some how.  It is definitely hitting the gantry.  I will installed some mechanical limit switches and report back.


---
**Frank Broadway** *August 08, 2018 01:12*

**+Cammo T** I am a new user and I have the same problem. What was your solution?


---
*Imported from [Google+](https://plus.google.com/100980986272635435257/posts/MvJpyqV8DdS) &mdash; content and formatting may not be reliable*
