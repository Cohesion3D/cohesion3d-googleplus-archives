---
layout: post
title: "Just really started using grbl and LB"
date: June 12, 2018 01:37
category: "C3D Mini Support"
author: "Tony Sobczak"
---
Just really started using grbl and LB.  I have the LO z axis table which works fine in smoothie.  When I try to get the z axis to move, i get the following error and LB completely locks up.



Starting stream

ALARM:2

On or near line 1:

Stream completed in 0:00

[MSG:Reset to continue]



Only wayI found to continue is close LB and start again.



Here is my configuration:



$$

$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=0.000

$36=100.000

$100=160.000

$101=160.000

$102=160.000

$110=24000.000

$111=24000.000

$112=500.000

$120=2500.000

$121=2500.000

$122=2500.000

$130=300.000

$131=200.000

$132=50.000

$140=0.400

$141=0.600

$142=0.000

ok

$102=3200

ok

$$

$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=0.000

$36=100.000

$100=160.000

$101=160.000

$102=3200.000

$110=24000.000

$111=24000.000

$112=500.000

$120=2500.000

$121=2500.000

$122=2500.000

$130=300.000

$131=200.000

$132=50.000

$140=0.400

$141=0.600

$142=0.000

ok



It did not work with the original value in $102=160.



I believe the stepper is a 42HS03.  Any help will be greatly appreciated.



I'm using the version of gbrl download from the cohesion site.







**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2018 01:39*

Is this an external stepper driver driving the Z bed, as shown in our Z Table article?


---
**Jim Fong** *June 12, 2018 01:53*

Alarm 2 is machine exceeding travel. 



You probably need to change $132 to the z table travel length.  Increase the default 50mm to a larger value. 


---
**Tony Sobczak** *June 12, 2018 15:28*

Yes it is with an external driver. 


---
**Tony Sobczak** *June 15, 2018 22:26*

Changing $132 didn't make a difference.  I tried to move the z several times and could occasionally get it to move - not very often but when it did it moved in the wrong direction. Again it works exactly as it always has using smoothie. When it did move using $102=1600 it only moved .5 mm rather than the 1.0mm set.





$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=0.000

$36=100.000

$100=160.000

$101=160.000

$102=1600.000

$110=24000.000

$111=24000.000

$112=300.000

$120=2500.000

$121=2500.000

$122=2500.000

$130=300.000

$131=200.000

$132=100.000

$140=0.400

$141=0.600

$142=0.000

ok


---
**Tony Sobczak** *June 19, 2018 16:43*

**+Jim Fong** any further ideas?  Any more info I can add?


---
**Jim Fong** *June 20, 2018 17:49*

Since you say the Z is going the wrong direction, you need to modify $3 setting to change the direction.   



Change $102=3200 to double the distance traveled. 



[github.com - grbl](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration)


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/DRxCDawYyxY) &mdash; content and formatting may not be reliable*
