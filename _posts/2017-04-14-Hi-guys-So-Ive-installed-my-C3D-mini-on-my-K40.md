---
layout: post
title: "Hi guys, So I've installed my C3D mini on my K40"
date: April 14, 2017 13:30
category: "C3D Mini Support"
author: "Adam J"
---
Hi guys,



So I've installed my C3D mini on my K40. Board is communicating with LaserWeb4, correct LED's are showing etc. Problem is, I'm getting no movement on my x axis. Y axis seems to jog perfectly fine.  I have the ribbon cable model K40. Any suggestions on what to try? I'm not sure if it's software or hardware related?



Thanks!





**"Adam J"**

---
---
**Adam J** *April 14, 2017 13:53*

Also when I click home in LW the head wants to travel to the bottom left corner instead of upper left?


---
**Joe Alexander** *April 14, 2017 13:54*

are you using G28.2 to home?


---
**Joe Alexander** *April 14, 2017 13:55*

as for stepper maybe the ribbon cable is in backwards?


---
**Adam J** *April 14, 2017 14:10*

![images/0b277b53c095b5b48ca6e474f8d45acb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0b277b53c095b5b48ca6e474f8d45acb.jpeg)


---
**Adam J** *April 14, 2017 14:10*

![images/1da1cd179469f91f280b87eff525a02d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1da1cd179469f91f280b87eff525a02d.jpeg)


---
**Adam J** *April 14, 2017 14:10*

![images/75dafaa68324b011abdcfa92de30fa99.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/75dafaa68324b011abdcfa92de30fa99.jpeg)


---
**Adam J** *April 14, 2017 14:10*

![images/88ca89ccbe3d9cec2860ef3f31b7d7d8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/88ca89ccbe3d9cec2860ef3f31b7d7d8.jpeg)


---
**Adam J** *April 14, 2017 14:12*

G28.2 entered. For some reason now it does try to home to top left but very slowly and only makes it say 5mm before stopping. As you can tell, I fall into the novice / noob category!



Tried swapping the ribbon cable the other way but still no movement on x-axis. 






---
**Adam J** *April 14, 2017 14:14*

Just added a few pictures of my install. Not sure if they will offer any explanation?


---
**Joe Alexander** *April 14, 2017 14:17*

we have all been through this dont feel bad. When you jog in LW does it jog the right direction? and are you using firmware-cnc? I know for mine I had to set home_to_max for my Y-axis to get it to home to top left with G28.2.


---
**Adam J** *April 14, 2017 14:23*

Not sure if this reading helps, saw it mentioned on another thread... Doesn't mean a great deal to me :-)

![images/b84e140d3a294696cd70974455dc3ec5.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b84e140d3a294696cd70974455dc3ec5.png)


---
**Adam J** *April 14, 2017 14:27*

Jogging Y+ moves the head towards me (so from top left corner downwards towards bottom left).



Really appreciate the help Joe. I'm guessing I can check what firmware I'm using by looking on the SD card that's in the C3D?


---
**Adam J** *April 14, 2017 14:50*

Okay so, 



Ribbon cable inserted with the text facing outwards (opposite to above pictures) = Hitting home makes the head move upwards very slowly, moves about 5mm before stopping - No X axis movement at all.



Ribbon cable inserted with text facing inwards to board, (like above pictures) = Hitting home makes the head move downwards (away from tube) speed seems normal but it keeps going and bangs past the carriage limits. No X axis movement at all.



I understand the ribbon cable controls the X-Axis? So not sure what's going on there.



My memory card in my C3D has 2 files:

config.txt

FIRMWARE.cur



I edited the config setting to home_to_max (from home_to_min) but nothing different happens.



Safe to say, I'm not sure what I'm doing :-D




---
**Joe Alexander** *April 14, 2017 14:51*

yea add a ! to your pin on config for the y axis to flip it, that should send it the top left for home. And no worries, I troll Google+ more than I care to admit :P


---
**Joe Alexander** *April 14, 2017 14:55*

yea the reason it bangs against the bottom left is because the limit switch is installed on top left. I resolved this myself by adding min and max endstops. so x should be home_to_min and y home_to_max, and invert the pin for Y with a ! after the pin#. make sense?

   Still doesn't resolve the FFC ribbon cablr issue though, im thinking loose connection or broken lead? since the board is new(assumption) it should be fine.


---
**Adam J** *April 14, 2017 15:02*

Not exactly sure where you mean to add a "!" ?



Are there values that need editing in the cfg file? I thought it was good to go out of box so to speak?


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 15:07*

Please do not make changes to the config file, everything is already as it should be. 



Please power down and swap the green drivers between the X and Y sockets. Then try jogging again. 

The ribbon should be oriented with the metal contacts inward towards the board. 


---
**Joe Alexander** *April 14, 2017 15:08*

Now that Ray is here I'll let him handle this as he is the master of the C3D boards :)


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 15:24*

Thanks Joe. For your reference the Mini laser bundle already comes with CNC firmware and a k40 specific config set. No changes to config should be necessary. If the Y moves towards the front and not the rear for homing G28.2 I ask the user to flip the Y motor cable. Less chance of error that way. 


---
**Adam J** *April 14, 2017 15:34*

Thanks for the response guys, greatly appreciated!



I've done as you suggested Ray and now I'm getting the opposite, only X-axis movement when jogging and no movement at all on Y. Hitting home moves the head to the left.


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 15:35*

Did you get an extra stepper driver in your bundle? 


---
**Adam J** *April 14, 2017 15:44*

 No, just the two. So it's a faulty driver then?



Is this the same type?



[ebay.co.uk - Details about  RepRap 3D Printer A4988 Stepstick Stepper Motor Driver 3M Tape Heatsink](http://www.ebay.co.uk/itm/RepRap-3D-Printer-A4988-Stepstick-Stepper-Motor-Driver-3M-Tape-Heatsink-/262711963603?hash=item3d2ada6fd3:g:x8oAAOSwHMJYIyfx)






---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 15:49*

Yep, that should do the trick. 


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/LZpnfCy9fDM) &mdash; content and formatting may not be reliable*
