---
layout: post
title: "The Cohesion3D Web Store has been around for a year now!"
date: November 24, 2017 18:55
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
The Cohesion3D Web Store has been around for a year now!  We're celebrating with 15% off site-wide at [cohesion3d.com](http://cohesion3d.com) for the entire weekend with coupon code ONEYEAR - will expire Monday Nov 27 at 11:59PM EST. 



See the original post below for more details and to hear about the journey. 



Thank you to everyone that has been a part of the amazing past year. 



Happy Holidays!

-Ray



<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



One year ago, on Thanksgiving evening, I opened the [cohesion3d.com](http://cohesion3d.com) webstore to the public.  



The 6 months leading up to that, I had been making my 32-Bit Control boards the ReMix and the Mini by hand and sending them off to willing testers.  Finally, the first 100 unit production run of the Cohesion3D ReMix board which I had funded on a credit card was set to arrive on Friday, and the Mini board testing was going well in both 3D Printers and K40 Laser Cutters.



I launched the storefront with the ReMix board in stock and pre-orders for the Mini board and the Laser Upgrade Bundle.  Lots of K40 Laser Cutter Upgraders pre-ordered the Mini to get away from the frustrating and limiting software it comes with, to be able to do true grayscale engravings, and use open source and powerful software such as LaserWeb to control their machines. 



We sold out, we shipped, and we sold out again, all while working hard to navigate the challenges of hardware manufacturing, software compatibility, and make for the simplest and best possible user experience to upgrade their machines to the powerful benefits of a Cohesion3D board... And the user feedback has been phenomenal - we've fundamentally allowed people to breathe new life into machines that many found unusable beforehand. 



With all that said, I want to thank everyone that has been along for the ride, including my beta testers, community members, software developers, and customers, for being understanding of all the growing pains of a new small business and helping us get to where we are today. 



In celebration of the web store's 1 year anniversary, and as a further thank you, I am offering 15% off everything on [cohesion3d.com](http://cohesion3d.com) for today, the entire weekend, and Cyber Monday with coupon code ONEYEAR - this will expire Monday Nov 27 at 11:59PM EST.  If you have been thinking about upgrading your machine, a 32 bit controller is a great way to improve performance, and now is a great time to pull the trigger on that. 



As always, you heard it here first.  Please help spread the word, and happy holidays to all!



-Ray







**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Samer Najia** *November 25, 2017 04:01*

Ray, remind again what the PSU you are using is?  I have the same bed I am looking to use for a CoreXY bot I am designing.


---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2017 04:04*

It's a 24v 33a PSU.  The bed heats up super quick, but it's disconcerting running that many amps. The bed itself requires 24v 24a. 


---
**Samer Najia** *November 25, 2017 04:22*

Do you have a link to the PSU by any chance?


---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2017 04:31*

Just eBay, maybe aliexpress. 


---
**Samer Najia** *November 25, 2017 04:37*

Thanks.  I see some on Amazon (expensive) and some on eBay (reasonable).


---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2017 04:38*

Roughly 30 bucks. Probably from china directly. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/QpMXhy5oRA7) &mdash; content and formatting may not be reliable*
