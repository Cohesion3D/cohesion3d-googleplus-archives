---
layout: post
title: "I'm a novice K40 owner bootstrapping my new system with a C3D board..."
date: December 13, 2017 23:28
category: "K40 and other Lasers"
author: "John Plocher"
---
I'm a novice K40 owner bootstrapping my new system with a C3D board...



I want to hook up a cover open switch to disable the laser power when the lid is open, but can't find docs at that level...



Meta question:  Should I be looking on the C3d forum, on the c3d site or on the smoothieboard site?



  -John





**"John Plocher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2017 23:31*

I'd peruse the k40 group. Don and Hakan are legends. The going advice is to wire a door switch into the laser psu to hard-kill the laser. 



You can also route that signal to the board to pause or kill your job, but I recommend this in addition to the hard kill. 


---
**Don Kleinschnitz Jr.** *December 14, 2017 14:05*

Join the Laser Cutting and Engraving Community.



Here is a post from my blog on adding interlocks. I recommend interlocks on the front cover and the laser compartment along with a water flow sensor.

Essentially you wire switches in series with the supplies "laser switch". The exact wiring depends on your machines and LPS vintage.

You can wire them in series from cover to cover or you can bring them to a breakout board like I did.



[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)



If you need more help see you over on the K40 community.  

[K40 LASER CUTTING ENGRAVING MACHINE](https://plus.google.com/communities/118113483589382049502)




---
*Imported from [Google+](https://plus.google.com/+JohnPlocher/posts/RcoDG87Dr8S) &mdash; content and formatting may not be reliable*
