---
layout: post
title: "So. Managed to (almost) get my cad software to output G code that the board understand"
date: February 23, 2017 19:32
category: "General Discussion"
author: "Colin Rowe"
---
So. Managed to (almost) get my cad software to output G code that the board understand.  Just need to alter the start and finish. Nice and smooth. Also changed PWM to 200 so cam now control the power by using S. Getting there slow. Need to mow start to mess with engraving. Thanks for all the help so far. 




**Video content missing for image https://lh3.googleusercontent.com/-pTbkjXaAm0U/WK84zmIwbvI/AAAAAAAAJEE/_ohKKgPJEawZRYJePdYJdrUDV75p2J8NACJoC/s0/20170223_172927.mp4**
![images/1f4d808166783901570e5319a2333efd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f4d808166783901570e5319a2333efd.jpeg)
![images/1026367a7eac72c4c7846fd726a7f76c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1026367a7eac72c4c7846fd726a7f76c.jpeg)

**"Colin Rowe"**

---
---
**Don Kleinschnitz Jr.** *February 23, 2017 20:01*

Try 400 and see if its even better?






---
**Colin Rowe** *February 23, 2017 20:09*

Why. What will that do?. Only been told to change to 200 as was only on or off. Now can get some gradient 


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 20:46*

400 might be better. A few people have had to go that high. It's really unique to the unit. 


---
**Don Kleinschnitz Jr.** *February 23, 2017 21:06*

**+Colin Rowe** I have some recent evidence that 400 or even more may provide better granularity.


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/KTedsE5bsSc) &mdash; content and formatting may not be reliable*
