---
layout: post
title: "Installed my mini in my K40. My old Moshidraw board looks like the \"\"Ribbon Cable + Screw Terminal Power Supply\" Variant\" however, rather than a 4 pin plug header, mine had 6 pins, with GND, 24V, GND and Laser"
date: June 13, 2017 03:08
category: "C3D Mini Support"
author: "Dafydd Roche"
---
Installed my mini in my K40. 

My old Moshidraw board looks like the ""Ribbon Cable + Screw Terminal Power Supply" Variant" however, rather than a 4 pin plug header, mine had 6 pins, with GND, 24V, GND and Laser. 

A little hacking later,  I got the connector trimmed down,  and the signals going in (24V and GND is good!). Motors are moving and endstops all working etc, but the laser wont fire!



I can fire it from the test switch. no issue.

But I can't get the board to fire the laser.



Any ideas where to start? What signals should be going down the 2 wires to the laser? GND and 5V? Or should they be "shorting" together for "ON"?





**"Dafydd Roche"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 03:13*

The L (fire) wire should go to the 2.5 bed - screw terminal (bottom, 4th from the left) 

Board does not need 5v so don't connect that. 

24v and Gnd can go to the main power in screw terminal at top left. 


---
**Dafydd Roche** *June 13, 2017 03:55*

A little more hacked wiring, but it works! There were two green wires and two blues going to the moshiboard (will take a pic from my phone soon and add). The two to the laser were connected to L and GND on the moshidraw board, so I hooked up L to the screw terminal you suggested, and the GND to the GND of the 24V screw terminal, and i can now fire the laser!

Now to config the machine! :)




---
*Imported from [Google+](https://plus.google.com/113228590072160389068/posts/MHPWSCq9Yqr) &mdash; content and formatting may not be reliable*
