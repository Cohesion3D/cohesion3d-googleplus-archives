---
layout: post
title: "How can I tell what revision my Cohesion 3D Mini is running -- and how do I get and install any firmware upgrades (if there are any)?"
date: January 03, 2019 15:16
category: "C3D Mini Support"
author: "Joel Brondos"
---
How can I tell what revision my Cohesion 3D Mini is running -- and how do I get and install any firmware upgrades (if there are any)? Just checking in to see what's the latest and greatest. I've really been enjoying my K40 since I installed the Cohesion 3D about a year ago.





**"Joel Brondos"**

---


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/4T3R3H4SXZf) &mdash; content and formatting may not be reliable*
