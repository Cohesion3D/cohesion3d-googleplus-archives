---
layout: post
title: "I have a older k40 with different connections can someone help me out?"
date: October 27, 2018 18:26
category: "K40 and other Lasers"
author: "Johan van Rijen"
---
I have a older k40 with different connections can someone help me out?



![images/91a8d81d536a48a4b5d1b306d2537dd5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/91a8d81d536a48a4b5d1b306d2537dd5.jpeg)
![images/9da0a9dd7fca654be621337fb49a1a5a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9da0a9dd7fca654be621337fb49a1a5a.jpeg)
![images/487e34e582278e21fa8cc7d5224ef8e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/487e34e582278e21fa8cc7d5224ef8e7.jpeg)

**"Johan van Rijen"**

---
---
**Don Kleinschnitz Jr.** *October 28, 2018 11:38*

What is is you need?


---
**Johan van Rijen** *October 28, 2018 13:10*

Just to know if i do it correct actually. I bought de cohesion with a power suplly so i am using that to power te board. Then i cut the blue wire where it says laser on the board and connect this to the Fet Bet - on the board. Connect the flat cable and i put the CN4 conntector to the Y blue ribbon up?



![images/fd8a933bbbb8160cea55e8770c04dc63.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd8a933bbbb8160cea55e8770c04dc63.jpeg)


---
**Johan van Rijen** *October 28, 2018 16:07*

Ok so far i connected everything but i am not getting laser fire


---
**Johan van Rijen** *October 28, 2018 16:12*

I am using the new power supply and yes laser works when I test it but not when I start a program![images/637c489b26e080c0a18f2bd5f14fa095.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/637c489b26e080c0a18f2bd5f14fa095.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2018 18:39*

I can’t see the entire wiring of the Mini board in your pic but I would assume that you need a common ground from the laser psu to the Mini for lasefire to work. 


---
**Johan van Rijen** *October 28, 2018 18:45*

Ok where would i connect that? (Nice work btw easy swapping out and in with the new) Fet in - is a ground right?


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2018 18:47*

Ground on the Mini board.  Same place as the ground from the power brick, hence "common ground". 


---
**Johan van Rijen** *October 28, 2018 19:06*

I connected the ground from the plug as whell but still the laser doesn't fire. Everything else works as it should![images/07f57398890a30aa18f6b5416515c5f8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07f57398890a30aa18f6b5416515c5f8.jpeg)


---
**Johan van Rijen** *October 28, 2018 19:10*

Does the firmware fire on fet1bed - straight out of the box? Because normally it's not connected there?


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2018 19:14*

Yes. Now send a G1 X10 S1 F600 command thru Lb console 


---
**Johan van Rijen** *October 28, 2018 19:26*

Yes that fires the laser. But not when i start the program with lightburn.


---
**Johan van Rijen** *October 28, 2018 19:57*

Its Alive Its Alive (Run the gcode for testing the laser from your faq again and it started!) Just need to adjust the power a little 

Thnx for the help! Can't wait till the lcd adapter arrives (forgot to order that)


---
**Johan van Rijen** *October 28, 2018 20:02*

Maybe add the ground cable to your install guide. Because i printed the bracket from thingiverse so it would not touch the case. But then it doesnt have the common ground.


---
*Imported from [Google+](https://plus.google.com/112447462371291123033/posts/ErxRy4UtqR6) &mdash; content and formatting may not be reliable*
