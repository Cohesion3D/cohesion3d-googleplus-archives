---
layout: post
title: "Thanks for taking my call this morning Ray Kholodovsky ."
date: May 11, 2017 16:04
category: "Thank you and other tips"
author: "John Milleker Jr."
---
Thanks for taking my call this morning **+Ray Kholodovsky**. It was a pleasure to talk to you.



I was waiting a few months before I pulled the trigger on a new brain for my K40, but I couldn't pass up the price and the ease of connectivity. Not that I really was worrying about the hookup, but I just wasn't ready mentally to hack and splice cables. After all, I just received the K40 yesterday and have already ordered a few hundred in parts upgrades and replacements.



If it's all in pieces, might as well do it right..





**"John Milleker Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/9Sax7QGa9bw) &mdash; content and formatting may not be reliable*
