---
layout: post
title: "Hi guys, I'm having some issues with my cohesion board (using lightburn)"
date: May 15, 2018 21:44
category: "C3D Mini Support"
author: "Ejal Nvt"
---
Hi guys, I'm having some issues with my cohesion board (using lightburn). When I try to cut a svg file the hearts look like in the picture. When I try with the M2 nano board with corellaser all is fine. Could it be the x and y steps per mm setting in the config file? I've tried playing around with accelaration  and the steps per mm but couldnt fix it but I've might used wrong numbers. My machine is a 40x40cm 50 watt co2 laser. 

![images/6870f943aeb4ad8a8d095b6f63469be7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6870f943aeb4ad8a8d095b6f63469be7.jpeg)



**"Ejal Nvt"**

---
---
**Kelly Burns** *May 16, 2018 03:45*

Since it works fine me with other controller it’s not something physical.  IMO, you are losing steps.  Likely due to driver current setting.


---
**Ejal Nvt** *May 16, 2018 10:09*

**+Kelly Burns** i can reproduce the error 100% (tried like 10 times already). So I'm guessing setting (mms/s?). If it was a current issue I expect the problem to be more random at cuts.

 


---
**Kelly Burns** *May 16, 2018 10:30*

If you are loosing micro steps it won’t appear random, it will appear Where the start and end of the cut are supposed to meet.  Looks to me like that heart starts the cut on the left and then goes clockwise around the heart and you are losing steps in X direction.  So they don’t meet.  If it were distorted, then I would agree, but it’s hard to tell with the heart design. Create a perfect circle in LightBurn.  I usually do 25mm.  Run as cut and then as a scan.  If it’s doesn’t come out with  25mm in both directions, then adjust your steps/mm. 



When I have had that issue, it was always something physical with belts or lens, but given your test and success with stock controller I don’t see that being the case.  Unless all other things are not equal.  


---
**Ejal Nvt** *May 16, 2018 10:36*

**+Kelly Burns** I tried a few circles and squares and they all line up, however the square dimentions are not perfect. For instance a 10mm square gives me 10.6 mm on x and 9.9mm on y, but a 5mm square gives me 5mm on x AND 5mm at y. I'll try a 25mm circle next and will post my results. Bu the way I also tried with 5mm/s in stead of 25 mm/s giving the same results on the hearts. Thats why I think it's not a current issue because slowing the stepper motors down to 5mm/s should elininate any current issue. 


---
**Ejal Nvt** *May 16, 2018 10:48*

**+Kelly Burns** so i made the circle and its 25.2mm on x and 24.6 on y but as you can see it did close the loop. On the right you see another heart shape that doesn't close the loop and shifts.. Any thoughts? 

![images/c503eda79aff3797451ed1bca76bd4ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c503eda79aff3797451ed1bca76bd4ce.jpeg)


---
**Kelly Burns** *May 16, 2018 11:58*

I thought i responded....  definitely strange.  The one heart closed the loop.  They don’t look “shifted” they look like they are smaller on the second cut.  I’m thinking something in file.  Can you post the heart file.  Preferably the LightBurn file and the source SVG.  I’ll run them in my K40 Max and see what it does.  It may be something weird in the source


---
**Ejal Nvt** *May 16, 2018 12:47*

**+Kelly Burns** i used two different svg heart files from Google. The last picture i posted is a heart shape with two hearts probably to raster engrave the inside of both hearts. Going nuts here.. 


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2018 13:26*

It’s possible you need more current to the stepper drivers, turn power off and rotate the little pots on the a4988 stepper drivers 45-90 degrees clockwise 


---
**Ejal Nvt** *May 16, 2018 15:46*

**+Ray Kholodovsky** hi ray.. I will try thank you. Would it also help to replace them with 8825's (i have a few spare parts here)?




---
**Ejal Nvt** *May 16, 2018 16:29*

**+Ray Kholodovsky** so i turned the pot on the stepper drvs 90 degrees. While my stepper motors now make much more noise the result is the same so I turned them back again. There is also no active cooling on the drivers so I don't want to overheat them. Anyway it didn't solve the problem. Any other ideas or suggestions? 


---
**Jim Fong** *May 16, 2018 16:47*

Instead of using your heart svg file, create a series of random circles, squares, rectangles with the built in LightBurn tools.  If the vectors cut fine, then I would think it isn’t a hardware problem but something wrong with the original heart svg file instead. 





I guess I missed your second post.....

Step calibration is best done with a larger square than 10mm and then measure with calipers.  I use 100mm square. 


---
**Ejal Nvt** *May 16, 2018 16:49*

**+Jim Fong** jim I've been testing multiple svg files. They cut fine on this machine with the m2 motherboard in combination with corellaser drw. 


---
**Ejal Nvt** *May 16, 2018 16:54*

**+Jim Fong** circles and squares cut okey but x and y are not even. I tried changing the steps per mm settings in the config, however the default is set to 157.575 for x and y and i didn't know exactly to what i should change it so i tried 160.575 and 162.575 etc. Didn't solve the problem. 


---
**Jim Fong** *May 16, 2018 17:17*

**+Ejal Nvt** I’m not sure if the 400x400 machine uses the same belts and pulley size as the 300x200 k40.   



Cut a large 100mm square and post the actual measurements. We can calculate the new step size. 



Make sure the step size is set to the default 157.575 before you begin. 



Post cut image too. 



This may be a hardware configuration error such as to high acceleration for your machine.   Someone with your exact machine may have Better settings. 


---
**Ejal Nvt** *May 16, 2018 17:22*

**+Jim Fong** y = 99.5

![images/f1b92852d6d70a3fe8a931c9097f8c60.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f1b92852d6d70a3fe8a931c9097f8c60.jpeg)


---
**Ejal Nvt** *May 16, 2018 17:22*

X = 100.57

![images/3420384dd0c65a8f36546ffee5e7af92.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3420384dd0c65a8f36546ffee5e7af92.jpeg)


---
**Jim Fong** *May 16, 2018 17:29*

Is this repeatable?  Cut the same square multiple times and get the same measurement?  






---
**Ejal Nvt** *May 16, 2018 17:30*

**+Jim Fong** yes it is. 


---
**Kelly Burns** *May 16, 2018 17:53*

Let’s review...  

1. According to your measurements, your steps/mm is not that far off.   .2 extra on X over 25mm.  

2. Your 10mm test were off by more than your 25mm.  To me that indicates some other issues.  But I would certainly use 25mm  or arger for calibration.  

3. You don’t have the issue with a circle or square cut. So it’s not likely a physical controller or other hardware issue.  

4. You adjusted on voltage pot and saw no improvement. 



This would lead me to believe it is a source file issue.  Not that it’s “BAD”, but it has something in the hearts that affects the Goode being produced by Lightburn. I may be wrong, but worth exploring.  What you are downloading from google is likely the same file shared to multiple sites.  So if LightBurns doesn’t like something in the source, it’s likely to do the same the thing.  It doesn’t really matter that it works with original controller and software.  They process files in a different way.  There no intermediate g-code generation.  



FYI...  You can’t mix Scan/raster engraving and cut in these test.   This is a cutting problem only.  The problem will manifest itself in a different way (or not at all) with raster engraves. 



If you are getting frustrated, then Walk away for a bit.  I have used the stock controllers and crappy software.  Getting Cohesion and Lightburn working will be worth the effort and pain. 





Are you using Smoothieware or Grbl LPC?






---
**Jim Fong** *May 16, 2018 17:56*

**+Ejal Nvt** the new calculated step size if I did my math right. 

Y = 158.366

X = 156.682



Cant hurt to check for loose screws. Try tightening all pulley set screws, Lens/mirrors mounts are tight.  Pulleys especially. I put lock tight on mine to prevent loosening from vibration. 



It’s just kinda odd to have different step values so something else causing this wrong cut length. 


---
**Ejal Nvt** *May 16, 2018 17:57*

**+Kelly Burns** I'm using smoothieware. Can you send me a link to a heart svg file that works for you? So we can eliminate the source file issue :) 


---
**Ejal Nvt** *May 16, 2018 17:58*

**+Jim Fong** can you help me to determine the steps per mm? 


---
**Ejal Nvt** *May 16, 2018 20:08*

**+Jim Fong** thanks for the help. I'll try it in a minute. Care to share the calculations so i can play with it? By the way i took another svg file. Copied it 2 times in lightburn and rotated and resized two of the shapes. The one on the top (left) is the original. 

![images/0528a0bcfb1d92f710fff63237a0f09e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0528a0bcfb1d92f710fff63237a0f09e.jpeg)


---
**Ejal Nvt** *May 16, 2018 20:15*

**+Jim Fong** changed the settings to the ones you provided.. Sadly the same results. 

![images/2bfe6eeb2f73114c2c45b46dce23f250.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2bfe6eeb2f73114c2c45b46dce23f250.jpeg)


---
**Kelly Burns** *May 16, 2018 20:16*



But it’s likely Calibrated 


---
**Ejal Nvt** *May 16, 2018 20:18*

**+Kelly Burns** what does that mean likely calibrated? 


---
**Kelly Burns** *May 16, 2018 20:21*

The one on the left looks good.  Where does that start and end?


---
**Ejal Nvt** *May 16, 2018 20:22*

The only good thing about this problem is that it's very consistent. If it was a loose belt it would slip sometimes but usually not at the same place over and over. Is there any other software i can try in stead of lightburn with the smoothieware fmw? 


---
**Ejal Nvt** *May 16, 2018 20:26*

**+Kelly Burns** the one on the left (first heart) is the original svg. I ctrl c and ctrl v it two times in lightburn. The other two hearts i than resized and turned a little bit. I did this because i suspect symmetrical figures are ok, when i turn the heart it's not symmetrical anymore and the problems start. That's why a circle or square also come out fine i believe. 


---
**Jim Fong** *May 16, 2018 20:29*

**+Ejal Nvt** I didn’t think it would make any difference.  I wanted you to rule that out.



This does seems like a mechanical issue. 

Check to make sure the x & y roll smoothly. Turn off laser first before manually moving the laser head. The laser head should be tight against the rail and roll smoothly.   Crud on the rail can cause this. 


---
**Jim Fong** *May 16, 2018 20:32*

**+Ejal Nvt** ok so circles are fine.  I’m at work so when I get home I’ll run a test using hearts on my laser with LightBurn. Just to make sure there isn’t anything wrong with the program  newer version.   


---
**Kelly Burns** *May 16, 2018 20:37*

I mean that Jim’s numbers will have your scale correct.  So if you do a 100mm square it will be a 100mm square —or at least very close.  



I know you are frustrated, but I don’t think anyone other than you expected the steps/mm to resolve it.  



Hang in there, we will figure it out!  



What you are likely seeing is a hardware issue being amplified due to the way  Smoothie is handling the movements. 



Are you importing the SVG you downloaded directly into LightBurn or into Corel and then exporting to SVG and then into LB


---
**Ejal Nvt** *May 16, 2018 20:42*

**+Kelly Burns** I actually like challenges so this is fun.. :) i use lightburn and only lightburn. With the m2 board i switched to whisperer with Inkscape. Both whisperer and corellaser worked without any problem after I changed the y offset a bit. I'll try the 100mm square again.. Hang on :) 


---
**Kelly Burns** *May 16, 2018 20:46*

The piece of information about copying and pasting and modifying would have been helpful before.  You said the same file works in stock controller but not on C3D and LB.  that info greatly changes the thinking.  



Just post the file so JIM and I can do the same thing.  


---
**Ejal Nvt** *May 16, 2018 20:49*

**+Ejal Nvt** **+Kelly Burns** y = 100.16

![images/f73aa1240de69df020cab3d7c320463e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f73aa1240de69df020cab3d7c320463e.jpeg)


---
**Ejal Nvt** *May 16, 2018 20:49*

**+Kelly Burns** x = 99.92

![images/5ed0167b2d378490d359d3f6d05f223c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ed0167b2d378490d359d3f6d05f223c.jpeg)


---
**Ejal Nvt** *May 16, 2018 20:55*

**+Jim Fong** the x and y move smoothly as always. If it was dirt hindering the rods i wouldn't expect me to be able to reproduce the heart issue. I checked all mechanics and they seem fine, did i mention it works without any problem on the m2 board using corellaser and also using whisperer with Inkscape? 


---
**Kelly Burns** *May 16, 2018 20:57*

I would call that good.  Especially given the hardware.  


---
**Ejal Nvt** *May 16, 2018 21:01*

**+Kelly Burns** agreed.. 99.9 and 100.1 are perfect values for me. So if it's not the mm/s setting, not the current on the drivers, not the belts or motors or pulleys, not the svg files,.. Could it be lightburn? Is there any other software i can try with smoothieware? 


---
**Jim Fong** *May 16, 2018 21:03*

**+Ejal Nvt**  better way is to cut the square completely out and then measure.  There is going some error when you try to place the calipers near the line. 


---
**Ejal Nvt** *May 16, 2018 21:05*

**+Jim Fong** indeed.. I actually put the calipers in the cut. But the calculations were spot on for my usage. It's accurate enough for me. 


---
**Jim Fong** *May 16, 2018 21:05*

**+Ejal Nvt** well since we pretty much ruled out the obvious problems that others have had.  Upload the LightBurn save file to Dropbox or google drive.  I’ll test on my laser 


---
**Ejal Nvt** *May 16, 2018 21:11*

**+Kelly Burns** so i thought let's try rotating a square.. Where the laser starts and stops the lines don't connect cleanly. 

![images/c81412e5b0835714522ab21df5c46515.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c81412e5b0835714522ab21df5c46515.jpeg)


---
**Ejal Nvt** *May 16, 2018 21:14*

Closeup.. 

![images/d444ef0f07f93072c569823f0e54720f.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d444ef0f07f93072c569823f0e54720f.png)


---
**Jim Fong** *May 16, 2018 21:38*

**+Ejal Nvt** save the gcode and post it for the rotated image square. I can quickly tell if LightBurn is generating bad code.  It’s only going to be a few lines. I don’t have my computer here to run LightBurn. 


---
**Ejal Nvt** *May 16, 2018 21:43*

**+Jim Fong** <?xml version="1.0" encoding="UTF-8"?>

<LightBurnProject MaterialHeight="0">

    <CutSetting type="Cut">

        <index Value="1"/>

        <minPower Value="10"/>

        <maxPower Value="10"/>

        <speed Value="40"/>

        <kerf Value="0"/>

        <zOffset Value="0"/>

        <startDelay Value="0"/>

        <endDelay Value="0"/>

        <throughPower Value="0"/>

        <enableCutThrough Value="0"/>

        <priority Value="0"/>

        <frequency Value="20000"/>

        <doOutput Value="1"/>

        <hide Value="0"/>

        <runBlower Value="1"/>

        <numPasses Value="1"/>

        <zPerPass Value="0"/>

        <scanOpt Value="mergeAll"/>

        <bidir Value="1"/>

        <crossHatch Value="0"/>

        <overscan Value="0"/>

        <overscanPercent Value="2.5"/>

        <floodFill Value="0"/>

        <interval Value="0.1"/>

        <angle Value="0"/>

    </CutSetting>

    <Shape Type="Rect" CutIndex="1" W="331" H="83" Cr="0">

        <XForm>0.26416 -0.146605 0.584653 1.05346 74.3616 321.177</XForm>

    </Shape>

</LightBurnProject>




---
**Ejal Nvt** *May 16, 2018 21:44*

**+Jim Fong** ignore the speed setting.. i was testing 5mm/s and 40 and got the same results.




---
**Jim Fong** *May 16, 2018 21:58*

**+Ejal Nvt** in the Lightburn console type in $version 

This will give back the smoothieware build date version. 



Which version of LightBurn latest Is 0.7.01?


---
**Ejal Nvt** *May 16, 2018 22:13*

**+Jim Fong** I'm using lightburn 0701 and smoothieware euh.. $version doesn't work.. Invalid statement. 


---
**Ejal Nvt** *May 16, 2018 22:18*

**+Jim Fong** 

![images/5c995a6060bb737fa3296882191db890.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5c995a6060bb737fa3296882191db890.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2018 22:23*

I'm not following the conversation (away on travel) but it's just version I believe


---
**Ejal Nvt** *May 16, 2018 22:24*

**+Ray Kholodovsky** what does that mean "just version"? 


---
**Jim Fong** *May 16, 2018 22:25*

**+Ray Kholodovsky** what happens when you mix grbl and smoothie command together.  Call it brain fart. 


---
**Jim Fong** *May 16, 2018 23:40*

**+Ejal Nvt** got home and ran the LightBurn save file you posted.  The generated gcode is perfect square with the vertices all touching like it should.  No overlap. 






---
**Jim Fong** *May 17, 2018 00:04*

G00 G17 G40 G21 G54

G91

M3

; Cut @ 40 mm/sec, 10% power

M106

G0X87.44Y0

G1X-87.44Y48.53S0.1F2400

G1X48.53Y87.43

G1X87.43Y-48.52

G1X-48.52Y-87.44

M107

G1S0

M5

G54

; return to starting pos

G0 X-87.44Y0

M2 



I had to shift the square to different position since my laser bed size is smaller than yours.  The start position would be different than yours. 



If you can read gcode, you can see the start and end position of each laser line intersects to form a perfect square. 



I think we can rule out LightBurn generated gcode. 




---
**Ejal Nvt** *May 17, 2018 00:40*

So we eliminated lightburn.. What else could it be? Could it be the stepper drivers? (doesn't sound logical but you never know). It must be a combination of my machine with lightburn and the Cohesion3D board. I still suspect something in the config. What is the latest smoothieware fmw for the Cohesion3D? 


---
**ThantiK** *May 17, 2018 01:01*

I wouldn't rule out a mechanical issue with the pulleys.  Just because one controller works and the other doesn't, it could just be down to acceleration/jerk allowing a pulley to shift back and forth.  If one controller has different acceleration or a better acceleration curve, a sticky pulley could work fine, but on a more aggressive profile, may skip.  I've seen it in the past with my own testing.  Software <i>can</i> expose mechanical issues.


---
**Ejal Nvt** *May 17, 2018 01:07*

**+ThantiK** i get your point. The doubt i have with mechanical issues is that the problem can be replicated 100%. A sticky pulley or too loose or tight belt or most other mechanical issues would cause a bit more random behavior. But this machine is dead on accurate over and over. It's wrong as fck but it is consistent :p everything i cut that is symmetrical comes out perfect. But when one side is shorter than the other the misalignment starts. 


---
**Jim Fong** *May 17, 2018 01:07*

**+Ejal Nvt** if it was stepper drivers issue, it would be very random (such as lost steps) so that can’t be it. Your issue is repeatable.  



I think your running the original Cohesion3d smoothie firmware. Most are running that version. We would of seen this issue long ago if it was a firmware problem. 



This has me perplexed. 


---
**ThantiK** *May 17, 2018 01:35*

**+Ejal Nvt** Sorry - you'd be surprised how regular "random" sticky pulleys can be.  It'd be stupid not to check them anyways.  Takes a couple of minutes to ensure they're fully tightened, and checks off a potential issue regardless.


---
**Ejal Nvt** *May 17, 2018 09:39*

**+ThantiK** thanks for thinking with me. I've just double checked the motors, belts, pulleys and connectors. They are all secure. 


---
**Ejal Nvt** *May 17, 2018 09:41*

**+Jim Fong** me too haha.. Any other ideas? 


---
**Jim Fong** *May 17, 2018 12:45*

**+Ejal Nvt** are you running the lcd  panel screen with smoothieware?  If you are, save the gcode to the sdcard and use the lcd rotary knob to select the gcode file and play it.   This will run the gcode without your PC interaction.  



Let’s see if the gcode runs fine without PC 


---
**Ejal Nvt** *May 17, 2018 14:02*

**+Jim Fong** sadly without the lcd. But i we thinking can i install grbl on the board in stead of smoothieware? 


---
**Jim Fong** *May 17, 2018 15:00*

**+Ejal Nvt** yes you can run grbl-lpc,  try that but if there is something on the pc causing this hiccup, it would probably manifest itself with grbl too.   Can’t hurt to try since I’m all out of reasonable explanations.  Since you have s 400x400 laser, you will need to change the grbl  x and y to match.   I personally prefer grbl over smoothieware.  



$130=400

$131=400



[cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com) - GRBL-LPC for Cohesion3D Mini : Cohesion3D


---
**Ejal Nvt** *May 17, 2018 19:25*

**+Jim Fong** so we're slowly eliminating causes. With grbl on the Cohesion3D i get exactly the same problem. So at least we know it's not smoothieware. I will try another pc, will need to check if i can install two copies of lightburn with my serial number. I'll get back to you. Thanks for helping by the way! 


---
**Ejal Nvt** *May 17, 2018 20:57*

**+Jim Fong** sorry for being an idiot. After flashing grbl I couldnt find the dropbox link to smoothieware. Do you know where I can download it for the cohesion3d mini?




---
**Jim Fong** *May 17, 2018 21:16*

**+Ejal Nvt** [dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)


---
**Ejal Nvt** *May 17, 2018 22:25*

**+Jim Fong** so i tried lightburn with another cable and another pc (after it installed all the windows updates pffff) and the same problem. Could it be the stepper drivers? If so what voltage should I set them (i have many spare drivers including 8825s from building a root3cnc). 


---
**Jim Fong** *May 17, 2018 22:44*

**+Ejal Nvt**  you can try the 8825’s. I don’t know the type of motors in your machine but the k40 laser stepper motors are 1amp. 



I would start there for current setting.  For 1amp current, you set Vref to 0.5volts 



both drivers can handle 30volt input, that’s the max I ever run those pololu type drivers. 



On my Cohesion3d board, I run A4988 for the x&y axis.  The rotary A axis is using a drv8825.  Both work fine on the mini. 


---
**Ejal Nvt** *May 18, 2018 00:26*

**+Jim Fong** and all others. I just cleaned and tightened almost everything in the machine and reinstalled lightburn. Mostly worked on the x axis belt. And guess what.. The problem seems to be solved. I only had the energy to test two small cuts because it's a bit late here but they came out perfect. Tomorrow I'll try some more and keep you posted. I want to thank everyone of you who helped me,thanks!!! 


---
**LightBurn Software** *May 22, 2018 01:03*

I would try dropping your acceleration value down considerably. If it’s very high, even running a cut a slow speed could still lose steps on the bottom of the heart, because it’s basically a 180 degree turn. LaserDRW / CorelLaser / Moshi wont exhibit the same issue because they won’t be using the same motion planner settings.


---
*Imported from [Google+](https://plus.google.com/115761277252076290507/posts/2JYuBkWZvJ4) &mdash; content and formatting may not be reliable*
