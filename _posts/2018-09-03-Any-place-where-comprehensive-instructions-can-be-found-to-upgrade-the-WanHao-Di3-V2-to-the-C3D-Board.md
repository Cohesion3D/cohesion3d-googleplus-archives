---
layout: post
title: "Any place where comprehensive instructions can be found to upgrade the WanHao Di3 V2 to the C3D Board?"
date: September 03, 2018 20:00
category: "3D Printers"
author: "Bill McDaniel"
---
Any place where comprehensive instructions can be found to upgrade the WanHao Di3 V2 to the C3D Board?





**"Bill McDaniel"**

---
---
**Anthony Bolgar** *September 03, 2018 20:48*

Search youtube, there is a video of the swap. I have done 3 already, you will not be dissapointed. 




---
**Ray Kholodovsky (Cohesion3D)** *September 03, 2018 22:32*

Yes it is pretty much localized to YouTube and a few threads here and via email of me helping people with it. 




---
**Bill McDaniel** *September 04, 2018 02:01*

OK. The Practical Printing guy in the vid shows a printed part to mount the board. Not finding that on Thingy, any thinks? Not sure I am going to re-use the case, but just in case. Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2018 02:05*

Like I said, a lot of the resources were never fully documented so feel free to ask away!



[dropbox.com - C3D Di3 Upgrade Resources](https://www.dropbox.com/sh/ubbbgieobmy53qc/AAAg72rSDiJ6yr1u8Q1Be_9Oa?dl=0)


---
**Bill McDaniel** *September 04, 2018 02:07*

Cool - I thought I saw a reference to that a while ago! 


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2018 02:20*

Someone modded that to add a shelf for the Z motor splitter to put a zip tie around that to keep it in...  I just hot glue the z splitter onto the board...


---
**Bill McDaniel** *October 23, 2018 03:02*

Do I need to use the Di3 LCD, or am I able to use a reprapdiscount LCD (I have a spare)? Also, does the board need a cooling fan? I might not use the Di3 box, and either leave the board open or make something custom. 


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 03:21*

Which GLCD Adapter do you have?


---
**Bill McDaniel** *October 23, 2018 03:41*

Also, which way do the stepper drivers get inserted?  I see the half circle drawn on the board - how should the pot on the driver be oriented relative to that? I have upgraded to the TMC 2208's. 




---
**Bill McDaniel** *October 23, 2018 03:50*

**+Ray Kholodovsky**  for the LCD, the reprapdiscount LCD has to 10 pin connectors on the back. You included one in my kit, that I think you said "worked" with the Di3 LCD. Not sure.




---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 04:01*

Is your glcd adapter blue or purple, or do you have both?


---
**Bill McDaniel** *October 23, 2018 17:35*

**+Ray Kholodovsky** Purple


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 18:19*

Then that is the one for the stock Di3 LCD. 


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 18:21*

Where did you get the TMC2208’s from? 


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 18:27*

Here is mine with genuine tmc2208 from digi key. The pot holes are at the bottom. 

I set the pot so the flat is horizontal. 



I use GDT-X9 heatsinks from aliexpress. ![images/9dd01a3759fd92a3e07162a0b838653b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9dd01a3759fd92a3e07162a0b838653b.jpeg)


---
**Bill McDaniel** *October 23, 2018 20:53*

The TMC2208 are MKS from Ali. Came with heat sinks to install




---
**Bill McDaniel** *October 23, 2018 20:56*

OK, ordered the other GLCD adapter. Do I need a diff driver for Z or the extruder? Also, will this board work with OctoPrint? And/or Wifi?


---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2018 21:15*

Ok, I don't know about MKS drivers.  Find the pinout and compare to the Mini board pinout diagram ( [Cohesion3d.com/start](http://Cohesion3d.com/start) and follow the documentation site link from there)

Same drivers for all.  I just let the PSU blow air on the drivers when the box is closed. 



Octoprint should be good. 


---
**Bill McDaniel** *October 23, 2018 21:35*

MKS TMC 2208, same orientation based on pinout & Board- FYI




---
**Bill McDaniel** *October 24, 2018 02:27*

Everything in the Dwg's states 24v, this will work with 12v, right?




---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2018 02:27*

Yes, of course. 24v is the recommend max to use on the board in general. 


---
**Bill McDaniel** *October 24, 2018 03:22*

Just FYI, NOT my design, but looks good and just posted: [thingiverse.com - Cohesion3D Mini box by HCP](https://www.thingiverse.com/thing:3165823)


---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2018 03:45*

Yep, Hakan made that recently for his laser. 


---
**Bill McDaniel** *October 25, 2018 19:55*

Have you already tweaked the config file on the SD card? Or should I DL the latest? Also, I saw a lot of laser settings, assume I can just comment those out (#)?


---
**Ray Kholodovsky (Cohesion3D)** *October 25, 2018 19:57*

You'll want to clear the sd card and put the files on it from the dropbox above.


---
**Bill McDaniel** *October 26, 2018 01:52*

Forgot about those - Thanks!


---
**Bill McDaniel** *October 26, 2018 02:19*

Can the panel buzzer be disabled?




---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2018 02:47*

Show me a picture of your glcd Adapter. 


---
**Bill McDaniel** *October 26, 2018 20:17*

?? Just ordered this and rcvd it today from you: 

![images/f8fd675de850ec7ab140e63c22a26a23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8fd675de850ec7ab140e63c22a26a23.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2018 21:27*

You asked how to turn off the buzzer. It depends on which glcd and thus which adapter you are using. I asked for a pic of your purple adapter so that I could instruct you on what to do. 


---
**Bill McDaniel** *October 26, 2018 21:43*

I am not using the purple adapter anymore, I bought the Blue one from you guys and just rcvd it - hence the pic above. 


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2018 21:57*

I'm still not clear on why you don't want to use the purple adapter with the Di3 screen and want to change that screen out for the reprap style screen. 


---
**Bill McDaniel** *October 27, 2018 18:10*

Because I already had the RepRap screen in a printed housing that will fit where I want to mount it, from a previous build that is now scavenged. Is there a disadvantage to using it? Other than I spent $10 on the adapter?




---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2018 21:07*

You should be ok. 


---
**Bill McDaniel** *November 05, 2018 23:11*

I can move all the steppers except the extruder stepper, even when I heat the extruder to temp. Any suggestions?




---
**Ray Kholodovsky (Cohesion3D)** *November 06, 2018 00:05*

Do you have the screen hooked up? If so show me what it looks like. 


---
**Bill McDaniel** *November 06, 2018 02:13*

**+Ray Kholodovsky** ![images/0edab8570dce27936ab06aa75ae02bc3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0edab8570dce27936ab06aa75ae02bc3.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 06, 2018 03:49*

Ok, that's the proper version of firmware. Can you test a different (like x) motor in the extruder slot?  You can also send the command G1 E10 F600 to test that. 


---
**Bill McDaniel** *November 06, 2018 16:10*

OK, I'll try those.




---
**Bill McDaniel** *November 06, 2018 17:31*

OK, swapped the Y and E, both worked. Redid to the proper connections and all is fine. The Ext was reversed and it is so quiet I think I did not notice it was moving. Reversed the E plug, all is fine now - Thanks!


---
**Bill McDaniel** *November 07, 2018 17:47*

OK, everything seems to be working well. Nicer prints. But I am trying to increase contrast, but it never takes, keeps going back to 0. Via the panel.




---
**Ray Kholodovsky (Cohesion3D)** *November 07, 2018 20:39*

That’s not a config setting for that display. You would turn the trimpot on the bottom left of the screen. 


---
**Bill McDaniel** *November 07, 2018 22:06*

Figures, I already have the panel well buttoned up! Good to know, thanks!




---
**Bill McDaniel** *November 08, 2018 21:25*

Can I print gcode direct from the SD card?




---
**Ray Kholodovsky (Cohesion3D)** *November 08, 2018 21:34*

MicroSD in the board only.  Always have it off before moving that card in out.


---
**Bill McDaniel** *November 08, 2018 22:35*

Yup! Thanks


---
**Bill McDaniel** *December 04, 2018 17:21*

Just got to say: REALLY happy with this upgrade to my Di3!


---
*Imported from [Google+](https://plus.google.com/103222271780985633950/posts/2zjPK8qsNuU) &mdash; content and formatting may not be reliable*
