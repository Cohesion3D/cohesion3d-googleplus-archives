---
layout: post
title: "I have an issue with my new board"
date: May 31, 2018 17:21
category: "C3D Mini Support"
author: "Woodys Creations"
---
I have an issue with my new board.  I had it working although upside down due to me no setting the origin correctly.



But after powering it down and then turning it back on it just sits with a long continuous beep.  the screen is blank and it doesnt register in windows.



I have tried a new Micro sd card with  the firmware and config files on but it still has the same issue.  I took the board out of the k40 and sat it next to the laptop.  Same result with just the usb cable in continous beep and nothing on screen 



Anyone have any ideas?





**"Woodys Creations"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 31, 2018 17:43*

What are the lights on the board doing?


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 17:46*

how are you writing the data to the sd card? what, if any changes have you made besides origin change in lightburn?


---
**Woodys Creations** *May 31, 2018 17:52*

there is just one bright led on nothing else, i took the sd card out put it in the laptop and it wouldnt recognise it so i put a new one in the laptop (sd card adapter) and put the firmware files on. OPut it in the board and same result






---
**Woodys Creations** *May 31, 2018 17:52*

![images/052cb6406f4a3cb9a1ff3c6f5e1c4f33.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/052cb6406f4a3cb9a1ff3c6f5e1c4f33.jpeg)


---
**Woodys Creations** *May 31, 2018 17:53*

![images/f98bb52061203e93cfa5a007e4556b78.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f98bb52061203e93cfa5a007e4556b78.jpeg)


---
**Woodys Creations** *May 31, 2018 17:53*

![images/7fbcfa30b7597c630b918ed0e0560ca6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7fbcfa30b7597c630b918ed0e0560ca6.jpeg)


---
**Woodys Creations** *May 31, 2018 17:53*

![images/669cea8ff616737e8d7de35954e647b4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/669cea8ff616737e8d7de35954e647b4.jpeg)


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 18:12*

okay power off the laser and unplug the usb cable. then disconnect everything (y axis, x axis/endstop ribbon cable, microSD card, glcd ribbon cables, etc...) then, with the laser still powered off, reconnect the usb and report what happens (beeps, LED status, etc...)


---
**Woodys Creations** *May 31, 2018 18:15*

Constant bleep little green light is lit 


---
**Woodys Creations** *May 31, 2018 18:17*

![images/af26da35eb602592683d63d402ee1acc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/af26da35eb602592683d63d402ee1acc.jpeg)


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 18:22*

did you remove the microSD card as mentioned?


---
**Woodys Creations** *May 31, 2018 18:23*

Yes that's with everything taken off the board 


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 18:26*

looks like i see it in the socket. unplug usb (power down) remove the microSD card, reconnect the usb and report the status


---
**Woodys Creations** *May 31, 2018 18:28*

![images/c93f188acd9b9d271caf3f9c32c2d81e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c93f188acd9b9d271caf3f9c32c2d81e.jpeg)


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 18:35*

if the microSD is out of the board and the only thing connected is the USB connected to your PC, and the status doesn't change, then you may want unmount the board, visually inspect it, and place it on a non conductive surface for further diagnostics.


---
**Woodys Creations** *May 31, 2018 18:39*

Still the same ![images/832e98c0556ddbb02a81311c0ff7c86a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/832e98c0556ddbb02a81311c0ff7c86a.jpeg)


---
**Woodys Creations** *May 31, 2018 18:40*

![images/440c68d842994e43787b51b4951d3919.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/440c68d842994e43787b51b4951d3919.jpeg)


---
**Woodys Creations** *May 31, 2018 18:40*

![images/39d07a440e72bab2ddc667a532044491.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39d07a440e72bab2ddc667a532044491.jpeg)


---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 18:42*

ok it looks like you pulled the stepper drivers off too. in one image you still had the glcd attached. try powering the bare board, with absolutely nothing attached whatsoever, to the usb and see if you get a different result


---
**Woodys Creations** *May 31, 2018 18:45*

Same result just sits with a green light on 


---
**Woodys Creations** *June 01, 2018 14:40*

Hi is there anythign further i can test?




---
**Ray Kholodovsky (Cohesion3D)** *June 02, 2018 00:58*

Please email us via the contact form on the website, mention this thread and your order #. 


---
**Woodys Creations** *June 02, 2018 18:58*

Sweet done that 


---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2018 20:46*

**+Woodys Creations** Sean I’ve replied to your emails, just want to make sure you’ve received the responses. 


---
**Woodys Creations** *June 04, 2018 21:05*

No sorry ive just got one saying you had replied but nothing before that 


---
*Imported from [Google+](https://plus.google.com/103484599501128783834/posts/RUmggmtfdWb) &mdash; content and formatting may not be reliable*
