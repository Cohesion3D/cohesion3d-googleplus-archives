---
layout: post
title: "Just finished wiring up my C3Dremix for a D-bot"
date: January 23, 2017 01:16
category: "General Discussion"
author: "Nector Rivera"
---
Just finished wiring up my C3Dremix for a D-bot. Ran into some problems with the thermistors. I have them wired into 0.26 (bed) and 0.25 (hot end), updated the config file and reset the board. Not getting readings on either at the moment. I'm guessing theres something up with the config file but cant seem to figure out what exactly. Can anyone lend a hand?  [http://pastebin.com/CeqKy9kD](http://pastebin.com/CeqKy9kD)





**"Nector Rivera"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 01:42*

Designators such as on line 130 seem off. Can you please save that config, put it to the side, and put the stock smoothie config on the card/ memory device? The bed and first therm should show right away. 


---
**Nector Rivera** *January 23, 2017 02:22*

It's working now. Thanks Ray!


---
*Imported from [Google+](https://plus.google.com/105426156438363508116/posts/c4JTyG2iBGo) &mdash; content and formatting may not be reliable*
