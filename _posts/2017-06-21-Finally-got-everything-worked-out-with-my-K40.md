---
layout: post
title: "Finally got everything worked out with my K40"
date: June 21, 2017 20:29
category: "K40 and other Lasers"
author: "John Hunter"
---
Finally got everything worked out with my K40.  I am amazed with the Cohesion3d.  LOVE IT!!!  It really changes the K40.   Thank you so much!  I am looking at a possible upgrade in the future to one of the chinese 100 watt lasers cutters.  Will one of the Cohesion3d board swap in?  







**"John Hunter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 20:36*

Glad to hear it. 

A lot of those larger machines have external stepper drivers, so you just need our adapters instead of the A4988 motor drivers.  



See here: [plus.google.com - Hello all, My control unit is no longer responding to RD works (it's a china ...](https://plus.google.com/113017472460485580353/posts/gYmDLZuyLut)



And if you have pics feel free to send them over for verification. 


---
**John Hunter** *June 22, 2017 10:53*

Thank you for your response.  If i end up going with another laser i will definitely put another cohesion board in it.  




---
*Imported from [Google+](https://plus.google.com/105510960839020211759/posts/h9p2pN59Dbw) &mdash; content and formatting may not be reliable*
