---
layout: post
title: "Hello C3D on K40 with Smoothieware is working great, but I was trying to install Grbl, but Windows 7 does not recognize it"
date: November 10, 2017 18:31
category: "FirmWare and Config."
author: "bojan repic"
---
Hello



C3D on K40 with Smoothieware is working great, but I was trying to install Grbl, but Windows 7 does not recognize it. I read that Grbl needs different Windows driver than Smoothie. Where can I find driver for windows 7?



Thank you for all great work.



Bojan





**"bojan repic"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 18:33*

[https://github.com/cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)



As I read it, they want VCOM_lib/usbser.inf to be installed. 

I think it would be similar to the manual install procedure shown here: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)


---
**Andy Shilling** *November 10, 2017 22:07*

+Bojan RRepic I found this worked for me last night after having the same problem. Tip came from a post in the K40 community.



Anyone else that may have issues with this down the line and finds this post, here are some detailed instructions to create the .env file in windows [cmd.exe](http://cmd.exe)



-Go to your LaserWeb install directory: You will need to show hidden files and folders to get there. OR: right click on your LaserWeb app shortcut>click properties>then click the "Open File Location" button



-Right click some blank space in the folder and select New>Text Document



-Open the new text document and add the following line without the quotes:

"RESET_ON_CONNECT=1"



-Save the document



-Click on Windows Start menu, type cmd, and press enter. This should open a command prompt window



-Type "ren " without quotation marks. that's ren followed by a space. Do not press enter yet.



-Drag the text document that you created earlier into the command prompt window. It should now look something like:

C:\Users\yourcomputerusername>ren "C:\yourcomputerusername\AppData\Local\Programs\[lw.comm](http://lw.comm)-server"



-After the string add a space then type ".env" without the quotes. Then press enter and it should rename your file to ".env".


---
**bojan repic** *November 11, 2017 17:18*

Thank you, got GRBL working.

Had to install driver and .env file.


---
**Andy Shilling** *November 11, 2017 17:26*

**+Bojan Repic**​ that's great news, I have no idea what the .env files does but it worked for me.


---
*Imported from [Google+](https://plus.google.com/115068391387492902728/posts/eTtLKaz9FiX) &mdash; content and formatting may not be reliable*
