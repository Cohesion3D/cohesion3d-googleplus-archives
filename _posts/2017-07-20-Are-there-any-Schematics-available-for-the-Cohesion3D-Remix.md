---
layout: post
title: "Are there any Schematics available for the Cohesion3D Remix?"
date: July 20, 2017 06:00
category: "C3D Remix Support"
author: "Stephen Edwards"
---
Are there any Schematics available for the Cohesion3D Remix? Specifcally how are the MS1,MS2 and MS3 lines on the steppers hooked up, if at all and how can we change the microstep values from the Firmware?



Thanks.





**"Stephen Edwards"**

---
---
**Eric Lien** *July 20, 2017 10:00*

Microstep is set via solder jumper on the underside:

![images/8db05bbf1b2fcce82f14454546649fdb.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8db05bbf1b2fcce82f14454546649fdb.png)


---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 13:09*

There are trace/ solder jumpers on the bottom of the board. By default all 3 of those lines are connected, thus your driver will operate at its maximum microstepping possible (1/16 for a4988, 1/32 for drv8825, etc). It is possible to cut the connecting trace(s) to change this if a specific situation demands it. However, most of the time you would just adjust your steps per mm in config to match the microsteps your driver is running at. 


---
**Stephen Edwards** *July 20, 2017 20:38*

Thanks. Unfortunately adjusting the steps per mm is not an option unless this field is a float. Currently my X Axis is 0.5mm out over 100mm and the fix lays between 40 and 41mm per step so was looking at other options. 

Not sure if this is the correct way to fix this issue or maybe there is an option in openpnp that I have still yet to discover.


---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 20:45*

So the issue is that your axis is moving slightly more or less than you expect it to?  



Steps per mm in the config.txt can most certainly have decimals. I don't know how many max but 2 - 3 decimal digits should be most certainly enough. 



Doing an axis calibration as you described and adjusting the steps per mm is exactly how it's done in 3d Printing. 


---
**Stephen Edwards** *July 20, 2017 22:19*

Oh ok. Cool. I'll give it a try. Thanks




---
*Imported from [Google+](https://plus.google.com/111514068291904993170/posts/amXUp3cZVNm) &mdash; content and formatting may not be reliable*
