---
layout: post
title: "Hi All! Long time! Has anyone successfully installed Marlin on your c3d?"
date: May 27, 2018 22:58
category: "FirmWare and Config."
author: "Todd Mitchell"
---
Hi All!  Long time!



Has anyone successfully installed Marlin on your c3d?  That firmware offers a bed skew adjustment which I need to resolve issues w/ my laser cutter.  If you have installed it, any tips/guides?



Thanks!





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 27, 2018 23:13*

**+Thomas Moore** was the developer that handled the C3D support in Marlin. 



You pretty much need to use PlatformIO to do the compile, and there’s a inf file in the GitHub somewhere called ReArm driver - that’s what installs to the PC. 


---
**Todd Mitchell** *May 27, 2018 23:21*

cheers.  I'll give it a go then




---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/eK99ZvsyP6A) &mdash; content and formatting may not be reliable*
