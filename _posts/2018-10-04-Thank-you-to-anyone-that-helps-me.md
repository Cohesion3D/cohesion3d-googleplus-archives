---
layout: post
title: "Thank you to anyone that helps me"
date: October 04, 2018 00:31
category: "FirmWare and Config."
author: "Frank Broadway"
---
Thank you to anyone that helps me. I am setting up my rotary and I need a little help with the settings. I have read the manual and did my on research before I posted my call for help. I have a cohesion3D smoothie board and a DIY rotary from Thingiverse attached to A-axis. I have attached photos of my Lightburn rotary settings and a photo of my rotary. In LB I drew a 10mm x 10mm box and started the job. The laser makes a 10mm line with the X-axis however the A-axis rotates a lot more than 10mm. So I end up with a long rectangle. I think I need to update the config.txt file for delta_steps_per_mm but I am not sure what is the correct number. The delta_steps_per_mm is currently set to 100.



![images/a25ae749c1a39cf7c8d04e3cc00ec1a2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a25ae749c1a39cf7c8d04e3cc00ec1a2.png)
![images/d07f39ff88ac326107a73b97c05217d4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d07f39ff88ac326107a73b97c05217d4.png)

**"Frank Broadway"**

---
---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 01:10*

i think the HM laser FB page will be more responsive so i have been responding there :)




---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 02:22*

hey frank. i'm back on this thread. can you snap a closeup of your motors id tag or provide the exact model  of the motor please?


---
**Frank Broadway** *October 08, 2018 20:00*

**+Tech Bravo** Sorry I did not realize you had replied to my comment. Thank you for taking time to help others. I have solved the problem. The solution required me to change the delta_steps from 100 to 21. I was able to determine 21 by  drawing a box in lightburn and then measuring the cut box. To measure a photocopied a ruler and cut it out so it would wrap around the object. 


---
**Tech Bravo (Tech BravoTN)** *October 08, 2018 20:02*

**+Frank Broadway** Cool! glad you got it sorted




---
*Imported from [Google+](https://plus.google.com/+FrankBroadway/posts/NuwrsuNqCjf) &mdash; content and formatting may not be reliable*
