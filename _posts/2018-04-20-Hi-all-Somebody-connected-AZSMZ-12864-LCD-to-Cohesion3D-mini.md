---
layout: post
title: "Hi all Somebody connected AZSMZ 12864 LCD to Cohesion3D mini?"
date: April 20, 2018 15:15
category: "C3D Mini Support"
author: "Alex"
---
Hi all

Somebody connected AZSMZ 12864 LCD to Cohesion3D mini?





**"Alex"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2018 18:24*

Yes, check for post by **+Jake B** 


---
**Alex** *April 20, 2018 20:07*

Thanks Ray, i'm try connect to mini  but now not have result ((

![images/6e9f02dfd004adbbdf9fec0b14dcdbf2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e9f02dfd004adbbdf9fec0b14dcdbf2.jpeg)


---
**Alex** *April 20, 2018 20:47*

**+Ray Kholodovsky** I don't know what to think, try connect use config and nothing

panel.enable														true	

panel.lcd 															viki2	

panel.spi_channel 											0			

panel.spi_cs_pin 												0.16	

panel.encoder_a_pin											3.25!^

panel.encoder_b_pin											3.26!^

panel.click_button_pin									1.30!^

panel.buzz_pin													0.27o	

panel.back_button_pin										2.11!^

panel.a0_pin														0.28	

panel.contrast													19 		

panel.encoder_resolution 								4     



![images/1f8b73246544722e63e3b58e347f9af3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f8b73246544722e63e3b58e347f9af3.jpeg)


---
**Alex** *April 20, 2018 22:09*

It's all right, my mistake ) I have forgotten SCK

![images/93fc97d8b9e048d6e123451c50bea6d6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/93fc97d8b9e048d6e123451c50bea6d6.jpeg)


---
*Imported from [Google+](https://plus.google.com/106697873351656319086/posts/KBdwb3tTwpX) &mdash; content and formatting may not be reliable*
