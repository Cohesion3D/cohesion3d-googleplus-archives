---
layout: post
title: "I've had my K40 for a year and upgraded to the C3D in January"
date: October 31, 2017 05:57
category: "General Discussion"
author: "Bill Keeter"
---
I've had my K40 for a year and upgraded to the C3D in January. I'm now thinking of building a 60watt laser. Could the C3D handle running a larger laser with a larger bed (~24"x18")?



I'm thinking of building the FreeBurn on Openbuild. Just using standard extruded aluminum and Vslot for the actual X and Y. Then hoping Nema17 motors could handle movement since it's V-slot. Has anyone done this already? thoughts?





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 16:32*

Sure. I'd recommend getting some TMC2208 stepstick drivers which are higher quality. They can drive Nema17's and even some Nema23's and are powerful and quiet. 


---
**Griffin Paquette** *October 31, 2017 17:39*

Agreed with **+Ray Kholodovsky** the 2208’s are an excellent choice


---
**Bill Keeter** *October 31, 2017 20:19*

**+Ray Kholodovsky** Okay, so just higher quality drivers to plug into the C3D? And I could still use the C3D Mini? Very cool. Now to figuring out the BOM list for my variant of this Openbuild.


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 20:21*

Yep, should be able to do that with the Mini.  You'll want to cool them well (Heatsinks + Fan). 


---
**Bill Keeter** *October 31, 2017 20:35*

**+Ray Kholodovsky** Is there away to by-pass the MicroSD card slot and wire a new one to the PCB? Even just a couple inches so I could make a SD card slot on the surface of the laser case?


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 20:35*

That's more of a general recommendation, by the way.  The drivers we provide are more than adequate to run Nema 17's.  I just think the $20 to get 2 TMC2208 drivers is a huge improvement in regard to noise, max power/ speed, and quality. 


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 20:42*

Use a MicroSD Card extender. Should be a few bucks on Ebay.  


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/2EWx7uuUapF) &mdash; content and formatting may not be reliable*
