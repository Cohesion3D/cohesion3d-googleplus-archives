---
layout: post
title: "Did I fry my laser tube ."
date: February 19, 2018 03:28
category: "K40 and other Lasers"
author: "Joel Brondos"
---
Did I fry my laser tube . . . or a circuit?



I noticed that it wasn't cutting as well and attempting to adjust the K40 potentiometer for current didn't increase/decrease smoothly. I also noted that the temp on my CW-3000 industrial chiller (plenty of water in it) had risen to 25C (when it usually ran around 19C).



But on the K40 board's digital display (GLCD), I also tried setting the laser to 100% -- though I have to admit, I don't know how to interpret the data on that GLCD . . . or how the potentiometer on the GLCD relates to the Current Potentiometer.



When I use the Test Switch, the laser does not fire and the analog Amp meter does not show any current. I had noticed while fine tuning the current potentiometer during a project that adjusting the knob did not increase or decrease the amperage smoothly -- readings seemed to "jump" from 15 amps to 22 amps if at all.



The stepper motors still seem to work fine . . . LightBurn instructions still executing efficiently through the C3D board.





**"Joel Brondos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 00:27*

No idea, and since no one else has responded I'd recommend asking this in the general k40 group. 



The KNOB on the GLCD is to navigate the menus on it and the % you see there is a FEEDRATE override: ie run at 107% = I'm running slightly faster than Lightburn thinks I am. 



The bottom line shows the S value the laser is currently firing it, and the top is coordinates.  All pretty straightforward if you ask me, as long as you ignore everything you don't understand, which is what I'm doing for the entire right column. 


---
**Joel Brondos** *February 22, 2018 01:37*

**+Ray Kholodovsky** Apologies for being a newb, but is FEEDRATE the rate at which the code is being sent to the stepper motors? 



And what is the "S" value?



The GLCD has MENUS? Interesting . . . :)



P.S. - Yes, Don in the general K40 group has been a huge help. I think it is just replacing the power supply which, thankfully, is HALF the price of a laser tube.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/Egkni223YxH) &mdash; content and formatting may not be reliable*
