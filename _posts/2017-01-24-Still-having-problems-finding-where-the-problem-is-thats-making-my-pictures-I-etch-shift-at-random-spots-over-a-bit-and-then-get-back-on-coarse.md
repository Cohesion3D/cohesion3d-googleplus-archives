---
layout: post
title: "Still having problems finding where the problem is thats making my pictures I etch shift at random spots over a bit and then get back on coarse"
date: January 24, 2017 06:07
category: "General Discussion"
author: "tyler hallberg"
---
Still having problems finding where the problem is thats making my pictures I etch shift at random spots over a bit and then get back on coarse. Any ideas? I have tried leaving the machine along while engraving, tried slowing down the stepper motors. Not sure if I should ask somewhere else. this is from Laserweb and running a cohesion board. Thanks in advanced!

![images/8712d838e028e9256b69a2f94c47d66c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8712d838e028e9256b69a2f94c47d66c.jpeg)



**"tyler hallberg"**

---
---
**Ariel Yahni (UniKpty)** *January 24, 2017 12:52*

Does it happens with all images? Btw this result is amazing


---
**tyler hallberg** *January 24, 2017 14:00*

I've tried 3 different pictures all same results. 


---
**Don Kleinschnitz Jr.** *January 24, 2017 15:20*

**+tyler hallberg** does it occur in exactly the same place if you reprint this image? 

I would look for lousy connections in the X stepper path.




---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 15:30*

Acceleration may be too high. 

I think I kept the default 3000 in the config file but I've had to use 2000 on my k40. 


---
**tyler hallberg** *January 24, 2017 15:39*

I'll try both, I havnt tried doing the same picture twice, has been different pictures all times. I'll try the same picture as well as dropping the acceleration down as well


---
**tyler hallberg** *January 25, 2017 06:29*

Same photo and dropped the acceleration to 2000, still off but not as severe. 


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 15:04*

Try 1000 just to be thorough. Same photo again. 


---
**tyler hallberg** *January 25, 2017 15:10*

I'll try that tonight when I get home 


---
**tyler hallberg** *January 26, 2017 03:02*

Still missing in spots, even at 1000. Back to the drawing board


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 05:50*

Tyler, can you confirm what "missing in spots" means?  Are you still getting the offsets like in the original pic of this thread? More pics are great, as always :)


---
**tyler hallberg** *January 26, 2017 05:54*

still getting the offset, random places, not the same places on any photos, little less at 1000 and 2000 acceleration but still there


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 05:57*

And apologies if you've already answered this one, but what speeds are you running at? If you could screenshot the laserweb job window into here, would be great. 


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/7CBZSik31sV) &mdash; content and formatting may not be reliable*
