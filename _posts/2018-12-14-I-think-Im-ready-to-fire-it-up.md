---
layout: post
title: "I think I'm ready to fire it up..."
date: December 14, 2018 21:37
category: "General Discussion"
author: "Tricia R"
---
I think I'm ready to fire it up... I just wanted the official okee dokee to get going? Let me know! 

![images/1e6c29b9cf565229e793e27b5bd33f6b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e6c29b9cf565229e793e27b5bd33f6b.jpeg)



**"Tricia R"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 21:41*

First time firing it up?  Hang on... show me more pics... is the board isolated completely from the laser frame? 


---
**Tricia R** *December 14, 2018 21:42*

Yes it is completely insulated, I attached a spare piece of ceramic 


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 21:45*

It seems like you have used the power brick to power the external driver but not the board itself? 



Do this, including the last step of clipping the top wire. 



[cohesion3d.freshdesk.com - K40 Additional 24v Power Supply Upgrade Guide : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000783636-k40-additional-24v-power-supply-upgrade-guide)



Disconnect the external stepper adapter on A right now.  I'll show you a pic of how to run power wires to all of them later. 


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 21:45*

I recommend starting with just that. 


---
*Imported from [Google+](https://plus.google.com/107433814983162842539/posts/CwXNZ68z6iz) &mdash; content and formatting may not be reliable*
