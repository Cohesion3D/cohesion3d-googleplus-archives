---
layout: post
title: "Anyone out there know how long a wait to get the LightBurn GCode License Key after it is purchased?"
date: February 21, 2018 19:48
category: "General Discussion"
author: "bbowley2"
---
Anyone out there know how long a wait to get the LightBurn GCode License Key after it is purchased?





**"bbowley2"**

---
---
**LightBurn Software** *February 21, 2018 19:52*

Your key was sent to your @[cox.net](http://cox.net) email address yesterday at 9:04am.


---
**Sebastian Szafran** *February 21, 2018 22:50*

I received mine within seconds after payment.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/9rFRy9gKQMH) &mdash; content and formatting may not be reliable*
