---
layout: post
title: "Any reason why I can't use Cohesion 3D upgarde on an 80 watt laser cutter that currently has an M2 board?"
date: May 04, 2018 23:31
category: "K40 and other Lasers"
author: "Pets and Ponds Customer Service"
---
Any reason why I can't use Cohesion 3D upgarde on an 80 watt laser cutter that currently has an M2 board?



Thanks!





**"Pets and Ponds Customer Service"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 00:17*

Make sure the Power Supply is 24v and that the wiring looks like what is shown at [cohesion3d.com/start](http://cohesion3d.com/start)


---
**Pets and Ponds Customer Service** *May 05, 2018 13:48*

**+Ray Kholodovsky** 

Thanks Ray.  My laser pulse frequency can not be adjusted.  With the use of your hardware and the correct software will it be possible to control the frequency?


---
**LightBurn Software** *May 05, 2018 20:33*

**+Pets and Ponds Customer Service** It will allow you to control the power output level through PWM, is that what you mean?


---
**Pets and Ponds Customer Service** *May 05, 2018 21:12*

**+LightBurn Software** 

I am looking to be able to control the laser pulses per inch of travel.  I think most laser machines have the ability to do this but mine does not. 


---
**LightBurn Software** *May 05, 2018 21:15*

Most laser machines don't, until you get to the Epilog / Trotec / GCC level. Lower level controllers will dynamically adjust the power output level with the speed of the machine, but they don't control the rate of output fire like the really high end machines do. Is there a specific reason you feel you need this feature?


---
**Pets and Ponds Customer Service** *May 05, 2018 21:23*

**+LightBurn Software** 

I cut a variety of materials (thin rubber, acrylic , wood, card stock etc.) and having the ability to control PPI can reduce charring on wood, create perforations on card stock for folding etc.  Not a deal breaker if I can't do it though.  :)   Lightburn will be a huge upgrade from the supplied Corel Draw software and the M2 board.


---
**LightBurn Software** *May 05, 2018 21:26*

LightBurn does now have a perforation cut setting, but it's not the same as true PPI control.  I've only seen that on higher end controllers.


---
**Pets and Ponds Customer Service** *May 05, 2018 22:13*

**+Ray Kholodovsky** 

My wiring does not appear to match any of the images I see.  There are a total of 3 wire groups connected to my M2 board.  #1 - 4 wires going to the y stepper motor  #2 - ribbon cable going to a small board attached to the x stepper.  That board has wiring to the x stepper and limit switches #3 a thin gauge 3 wire feeding 24 volts to the board.  red to black = 24 volts, red to blue 20 volts black to blue 4 volts.  With some continuity between the blue and grounded frame of the machine.   Will this be compatible with your board Ray?



FYI my computers can not connect the the machine at this time.  The last time I ran a job as soon as the laser fired everything shut off for a split second.   The computer that was attached via USB cable is cooked and will no longer turn on.  I found that the ground bolt was not touching any bare metal due to paint on the frame.  I fixed that, but also found other components  that had no continuity to frame (therefore not grounded once main ground was fixed) again due to paint preventing contact to the frame.  Everything is working at this time except the connection to a computer.  Not sure what caused this but hope fixing the ground will prevent it in the future...

![images/4488e18a6b2010918d9ec03cb4098952.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4488e18a6b2010918d9ec03cb4098952.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2018 18:05*

Please read the instructions again.  This is the ribbon cable variant of wiring.  It is covered.


---
**Pets and Ponds Customer Service** *May 07, 2018 18:16*

**+Ray Kholodovsky** 

Thanks Ray.  Just waiting on my replacement M2 to arrive.  If it works I will be upgrading to your board.


---
**Pets and Ponds Customer Service** *June 13, 2018 02:08*

OK my new M2 nano board worked so I ordered and installed your board today.  

<s>Homing works properly with lightburn (back left)</s>

<s>-X jog works properly with lightburn but Y jog is reversed</s>

<s>-laser will fire when using the test fire function on the new GLCD, but laser will not fire during cut or scan while running a program in lightburn.  I have tried various power settings</s>

 if i run a scan rather than a cut the laser head stops moving after 3 or 4 passes, tried waiting over a minute a few times, tried pressing the "stop" button in light burn, but have to power the laser down manually or it just sits there and says busy

- have set bed size to 600 x 400mm but can jog the head past the 600 x 400 bed size until it crashes



Help please.

Thanks!




---
**Pets and Ponds Customer Service** *June 13, 2018 02:10*

**+Pets and Ponds Customer Service** 

not sure why the first half of my last post is struck through.  It was not like that until I posted it.  All information is the post is valid.


---
**LightBurn Software** *June 13, 2018 02:22*

Scanning (particularly an image) produces MUCH more gcode than simple vector cuts, and is dramatically more demanding on the processor, so if you have any sort of issue at all, scanning is more likely to show it.  Things to check:



- Good quality USB cable (not the blue one that comes with the K40)



- Combined DPI / speed value that won't emit more than 800 gcodes per second (if using Smoothieware) - this is 250dpi @ 80mm/sec



Running the K40 & C3D board on a power circuit that is free of noise (high-wattage chillers produce line noise, and can cause communication issues)


---
**Pets and Ponds Customer Service** *June 13, 2018 13:44*

**+LightBurn Software** Tried the supplied USB cable that came with the new board and my own USB cable, neither worked.  I was able to run 350mm/s and 1000 dpi with the old board on scans with no problem.  If I can't do that with the new board I will probably take it out and use the M2 board again.  There is nothing but the machine, the ventilation fan and a few lights on the circuit.



I am using windows 10.  Did not download Smoothieware as I understood it was not required with Windows 10.


---
**LightBurn Software** *June 13, 2018 13:48*

Smoothieware is the firmware installed on the board by default. GRBL-LPC is an alternative firmware that runs significantly faster and will hit your 350mm/sec speed without issue.  Your laser isn’t capable of 1000dpi - it’s true resolution is about 250 to 300 without some very special lenses, so you don’t gain by going much higher, and are likely to over-burn because the beam paths overlap.


---
**Pets and Ponds Customer Service** *June 13, 2018 13:57*

**+LightBurn Software** 

I will see if I can try GRBL-LPS then.  80mm/s is too slow regardless of the dpi the laser can reproduce, however any other software I used with the M2 board was able to handle the higher speeds and DPI, so I am surprised that the upgrade is less capable.



Any ideas on the other issues I am having or is that something the crew at Cohesion would be best to reply to?



Thanks!


---
**LightBurn Software** *June 13, 2018 14:18*

The limit isn’t 80mm/sec, it’s 800 gcodes per second, because Smoothieware is made for cutting, not engraving. 254dpi @ 80mm/sec is 800 gcodes/sec, max.  Similarly 127dpi @ 160mm/sec is also 800 gcodes/sec.



For your other issues, yes, C3D would be better able to help you, though you may find using GRBL-LPC helps some there too.


---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2018 21:43*

Just use the grbl-lpc firmware if you need to raster fast. It’s on the Cohesion3D.com/start page. [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Pets and Ponds Customer Service** *June 13, 2018 21:51*

**+Ray Kholodovsky** OK.  Thanks.

What about the other issue mentioned earlier - see below for a recap

Homing works properly with lightburn (back left)

<s>X jog works properly with lightburn but Y jog is reversed</s>

<s>-laser will fire when using the test fire function on the new GLCD, but laser will not fire during cut or scan while running a program in lightburn.  I have tried various power settings</s>

 have set bed size to 600 x 400mm but can jog the head past the 600 x 400 bed size until it crashes


---
**Pets and Ponds Customer Service** *June 15, 2018 02:26*

Still waiting for answers to previous questions/problems related to stock firmware so went ahead any tried grbl-lpc firmware.  Now the laser fires, but I have a blank screen on the GLCD and all jobs come out upside down.  Homing works as it did with stock firmware and jogging in the Y direction is still reversed.  Laser makes a much more high pitched sound when firing than it did with stock board.  I assume the pulse frequency is higher?  Still using Lightburn.  Please advise.  


---
**LightBurn Software** *June 15, 2018 03:07*

- GLCD is not supported by GRBL



- Y reversed generally means you've set your origin incorrectly. The most common mistake is thinking Home is Origin - they're not the same.  Origin is usually lower left.



- Stock board doesn't pulse at all - it's just "on" which is one of the bigger reasons to the get the C3D.



- Smoothieware doesn't enforce soft limits, so jogging/crashing is possible.  GRBL does enforce soft limits.  If you use the Click-to-move feature in LightBurn, it clips the coordinate to the working area, so you'll never jog out of it.


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 03:16*

To add,

You’d have to change values for your work size both in Lightburn and on the board’s config. Grbl does this with $ commands in the console. 

The C3D does pulse the laser so you may have to optimize that. 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 03:17*

[github.com - grbl](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration)



Look at $130 and $131


---
**Pets and Ponds Customer Service** *June 15, 2018 12:31*

**+Ray Kholodovsky** 

Thanks for all the tips gentlemen!   I will look into them.  

Any ideas on getting my GLCD working with grbl-lpc firmware?


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 12:36*

That is a current limitation of the product: you can run Smoothie and all the “amenities” - like the screen and the memory card, but it can’t raster too fast. Grbl is more barebones, can’t run the screen, but can raster very fast. 


---
**Pets and Ponds Customer Service** *June 15, 2018 15:11*

**+Ray Kholodovsky** 

So if I want to be able to raster at any kind of reasonable speed I can't use the GLCD.  At this point I think I would be best to put the M2Nano board back in and use the K40 whisperer software, which was fast, reliable and easy to use.  Is there any solution that can raster at least 3200 gcodes per second and use use the GLCD? 


---
*Imported from [Google+](https://plus.google.com/104356262526050403266/posts/M57RSVCLAMr) &mdash; content and formatting may not be reliable*
