---
layout: post
title: "Bay Area Maker Faire is next week, Fri - Sun May 18-20th!"
date: May 11, 2018 22:58
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Bay Area Maker Faire is next week, Fri - Sun May 18-20th! We’ll be there, Cohesion3D will have a booth somewhere inside. 

Reply if you’re coming. 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Step Cia** *May 12, 2018 17:00*

I'll keep an eye out for your booth ;)


---
**Pete Prodoehl** *May 14, 2018 03:04*

I’ll be there!


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2018 03:12*

Zone 2, most likely with the 3D Printers.  Look for the K40 and the Cohesion3D Banner. Come chat!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/anTvp7ggB9J) &mdash; content and formatting may not be reliable*
