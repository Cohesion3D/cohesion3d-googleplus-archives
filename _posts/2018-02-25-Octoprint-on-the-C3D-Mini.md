---
layout: post
title: "Octoprint on the C3D Mini !!!!!!!!!!!!!!!!!!!!!!!!"
date: February 25, 2018 03:04
category: "General Discussion"
author: "Tech Bravo (Tech BravoTN)"
---
Octoprint on the C3D Mini !!!!!!!!!!!!!!!!!!!!!!!!   [https://www.everythinglasers.com/groups/cohesion3d-controllers/forum/topic/octoprint-on-the-c3d-mini/](https://www.everythinglasers.com/groups/cohesion3d-controllers/forum/topic/octoprint-on-the-c3d-mini/)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2018 03:14*

A few points:

Technically it's Octoprint with C3D, not Octoprint on C3D. The pi is required. :) 



While you can upload via octo to the memory card in the C3D (by dragging your file onto the right half of the Octoprint window) this is slow. 

By dragging it onto the left half you are actually uploading to the Octoprint installation (i.e. the card in the pi) and that seems to be what is actually happening here (and what you should be doing). 



Edit Jim's quote to capitalize G28 and G28.2 - important not to spread bad habits. 


---
**Tech Bravo (Tech BravoTN)** *February 25, 2018 03:36*

fixed it! :)


---
**Jim Fong** *February 25, 2018 17:56*

Not a bad habit!  Lol  the majority of the hobby gcode machine controllers I use and most industrial controllers don’t care about upper/lower case gcode formatting.  LinuxCNC, Mach, Fanuc, UCCNC, grbl etc.......



The original rs-274 gcode specification even states that input is case insensitive.  



It’s Smoothieware that wants to be different.




---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/Rigktc1LAgW) &mdash; content and formatting may not be reliable*
