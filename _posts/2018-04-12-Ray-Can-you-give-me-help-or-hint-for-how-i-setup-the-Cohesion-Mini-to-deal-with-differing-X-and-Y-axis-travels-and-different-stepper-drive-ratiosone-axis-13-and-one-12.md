---
layout: post
title: "Ray, Can you give me help or hint for how i setup the Cohesion Mini to deal with differing X and Y axis travels and different stepper drive ratios(one axis 1:3 and one 1:2)"
date: April 12, 2018 18:57
category: "FirmWare and Config."
author: "Steven Whitecoff"
---
Ray,

Can you give me help or hint for how i setup the Cohesion Mini to deal with differing X and Y axis travels and different stepper drive ratios(one axis 1:3 and one 1:2). I see in the config.txt settings for "steps per MM"...is that all and how would I come up with that for my hardware?





**"Steven Whitecoff"**

---
---
**Chapman Baetzel** *April 12, 2018 19:24*

I'm not Ray, but you can use jprusa's calculator ([www.prusaprinters.org/calculator](http://www.prusaprinters.org/calculator)) to find your steps per mm if you're using standard components(GT2 belts or whatever). Or if you're using a custom driveline, put in a guess value of steps per mm, then tell the printer to move 100mm.  Measure how far it actually moved and do the math.  steps = current steps * (expected/actual)  It told the printer to move 100mm. if it moved only 25 mm, your correct steps will be 100/25 * whatever your guess value was.  X and Y steps are stored separately in smoothieware, and you can also Use gcode M92 to set them.  M92 X<steps> Y<steps>


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2018 20:48*

Chapman doesn't play me on TV either, but he's exactly right.  :)



I will add one thing that if you want individual acceleration values for X and Y we can do that with the latest firmware build and new config file.  Start with the above ^ first. 


---
**Steven Whitecoff** *April 12, 2018 23:12*

Color me stupid, no reduction ratio mentioned only tooth count without specifying what it refers to. 


---
**Chapman Baetzel** *April 12, 2018 23:34*

Tooth count is the number of teeth on your drive gear.  ex: [images-na.ssl-images-amazon.com](https://images-na.ssl-images-amazon.com/images/I/61vmG-cUhlL._SL1200_.jpg) that is a 16 tooth drive gear.  If using with a 1:2 drive ratio I would double the steps per mm.    Prusa's calculator says 100 steps per mm for a 16T gear, so i'd try 200.  try moving 10mm.  If it goes 40mm, then the ratio is going the other way, and I'd  use 50 steps per mm. Reduction ratio for stepper drive doesn't have all the information needed to calculate the steps per mm, which is what the config file needs to move the printer the correct distance.  


---
**Chapman Baetzel** *April 12, 2018 23:45*

If your printer doesn't use pulleys and belts, perhaps describing the drive mechanism would help.  I had a screw drive on my large-format printer for a while(went back to pulley/belt) and the steps per mm I calculated didn't match due to torsion in the printed parts for the ball bearing threadless screw drive.  I got an accurate steps-per-mm by measuring test moves and correcting the result based on the formula in my first response until it matched to within my measureable tolerance.  

The printer controller doesn't care about drive ratio.  It tells the motor to 'go this many steps.'  How many steps is determined by the gcode, which is based on a model file in mm(usually).  So its told to move a number of mm and then does the calculations based on configured steps per mm to tell the motors how many steps to move and how fast.  Thats a brief high-level explanation.  there's a lot more going on there but i don't fully understand it.  But i'm fairly confident that what I just said is correct.  


---
**Steven Whitecoff** *April 13, 2018 02:08*

Thanks Chapman, I get it now, the tooth count determines the working diameter of the pulley so it equates to how far the belt is pulled per revolution. I don't like the idea of measure in principal because with a 1400x900mm system a tiny error can really add up. However I guess its also true I could command a LOT of steps so it moves a long distance which would reduce the potential error. My biggest concern is having a setup that produces a step count like 44.44. 44.44 x3 =133.32 that strikes me as an odd number if its not divisible by say 16 micro steps to equal a whole number. I have to look up my stepper spec as I'm not sure if its 1.8 or .9*/step


---
**Chapman Baetzel** *April 13, 2018 12:59*

unless you're cutting power to the motors(such as a screw drive Z axis on a 3d printer) then having a non-whole-number step interval shouldn't matter.  stepper motors can hold position at a microstep interval as long as they have power.  I doubt that any 3d printer has a steps per mm thats a whole number of microsteps.  I can't speak to the underlying firmware calculations, but I've had step per mm values that weren't whole numbers(extruder) and the resulting motion was accurate.  Test move, measure, correct, repeat as necessary.



I understand your concern with measurement.  I would still want to measure and test the calculated value on something that big.  maybe using a known 1-meter object.  Position the toolhead at one end and move to the other end. 

1.8 or .9 step is easy enough to figure out.  Assume 1.8 and do a test move.  If it only went half as far as you told it to, you're steppers are .9


---
**Steven Whitecoff** *April 13, 2018 13:00*

So next question is if you are running a geared setup then multiplying the steps by the ratio gives actual steps per mm. Would I be correct that the best situation for that is where you get a nice round number- in my 1:3 axis that would be 200 step/mm. But my other axis has odd gearing 24:38 which gives an odd number 105.55545. Seems to me you would end up with a small error in resolving that into motion. With for example 900mm travel at 200step/mm it takes 180,000 step full travel and assuming  rounding 105.55545 to 105.55 180,000 x 0.000545 =99.81 which strikes me as a lot of error. Of course that is a mixed example but still a lot of steps in 900mm 




---
**Steven Whitecoff** *April 13, 2018 13:03*

![images/1eacd41e7132779199d8da8a31cd5cbd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1eacd41e7132779199d8da8a31cd5cbd.jpeg)


---
**Chapman Baetzel** *April 13, 2018 15:13*

I believe the answer to this is beyond my depth of understanding.   I looked for smoothie documentation as to how many decimal points the steps per mm value can use, but i see default configurations with up to 5 places after the decmal, so you probably don't have to round.   



My approach would be to just plug in the number and test it.  I spend more time calibrating my printer than I did designing and building it. Calibration is key.  Start small.  50mm.  Can it move 50mm accurately?  how about 100?  200? etc.  Refine the calibration as you measure the movement.   Any mechanical system is going to have error.  Even your calculated value could give movement off by a little due to belt stretch, gear backlash, dust on the rails.


---
**Chapman Baetzel** *April 13, 2018 15:23*

Also, I thought a little more about your example.  Thats 99.81 steps of error over 900mm.  A little less than 1mm after moving almost a meter.  Worst case scenario.  


---
**Steven Whitecoff** *April 13, 2018 16:33*

To me, almost 100 steps is 1/3 revolution of pulley at 1.2* step= 300 steps/revolution. 

Turning my 1:3 geared motor 1/3 turn moves the laser head approximately 0.25". 

In reality with 24/38 gearing(which I am trying to get the supplier replace with 1:3) its fewer steps but bigger ones. So perhaps more than 0.25" 



Regardless with the other possibilities for error, I do not think it is wise to use a low and odd ball gearing, plus it moves the 1400mm axis which is very heavy so the gearing will reduce loads. 



My thought is if both axis run the same gearing , stepper errors will be equal so while absolute position may not be perfect without physical calibration but for example, a circle would still be perfectly  round vs my current gearing which might draw a 900mm circle 1/4" towards being an ellipse.

FWIW, I'm running servo style feedback steppers so they will at least never fail to make the proper number of steps. Not the same as measuring head position but hopefully less prone to dropped step etc.


---
**Chapman Baetzel** *April 13, 2018 17:13*

if 100 steps moves the toolhead(laser) .25"(6.35mm) then your steps per mm is 15.74803.  Its movement of the toolhead in mm based on steps of the motor that the controller needs to know about.   Whatever goes on mechanically in between the stepper and the toolhead isn't really important to the controller when it comes to steps per mm.  

 a controller with differing steps per mm x and y <b>should</b> be able to still give you a correct circle.  That is the exact function for which the motion controller is designed. I had a Y carriage with different steps than X for a while, and it gave me properly squared prints.  I stopped using it because the wear on the threadless screw caused it to get out of calibration, but when calibrated, it worked properly. 


---
**Steven Whitecoff** *April 13, 2018 21:14*

Chapman you are correct I'm merely saying odd gearing and any lack of precision in the math will make calibration a necessity where as having steps/gearing etc the same means just setting the config would get you perfect or near perfect motion. I prefer something like that to something that is dependant on calibration to not be off by amounts like I've calculated as possible.




---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/DjMgA3XWCrz) &mdash; content and formatting may not be reliable*
