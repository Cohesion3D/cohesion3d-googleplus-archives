---
layout: post
title: "any one have a good way to convert dxf files to a usable file"
date: February 06, 2018 23:06
category: "K40 and other Lasers"
author: "Timothy Lathen"
---
any one have a good way to convert dxf files to a usable file. every time I import the file light burn scrambles the text 







**"Timothy Lathen"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2018 23:43*

**+LightBurn Software**



Email the file to the developer and they can take a look.


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2018 23:48*

**+Timothy Lathen** also please ask me your other questions here, it will be faster than going through the order messages system.  


---
**LightBurn Software** *February 07, 2018 00:47*

You’ll need to export the file with text converted to curves. There’s very little information available for the online formatting used by their text. If you provide a sample file and a screen shot of how it should look, it’s possible we could improve the import, but converting the text to curves prior to export will always be your best option.


---
**Timothy Lathen** *February 07, 2018 18:34*

I exploded the text and it made them objects then printed to a SVG file but they were not closed so I can't use scan Tried importing into inkscape but it still doesn't recognize the text. 

Here is the file 

[drive.google.com - Plackards K40 2.dxf](https://drive.google.com/open?id=16sIAlq-3XmWHFoJpDgP9tz2iYJg1ybOt)




---
**LightBurn Software** *February 07, 2018 18:36*

When you import the exploded one, select everything and press Alt-J to join the shapes. We’re going to be beefing up the auto-join to work in import very soon. DXF files are often stored as disconnected shapes, so they’re not a great format to use for this reason.


---
**LightBurn Software** *February 07, 2018 18:37*

We’ll check the file though - thank you.


---
**Timothy Lathen** *February 07, 2018 19:06*

[drive.google.com - K40 Try3.dxf](https://drive.google.com/open?id=1L1pToLsl0WSkDsLvroy56b9t87ocvhiV)


---
**Timothy Lathen** *February 07, 2018 19:07*

**+Timothy Lathen** 


---
**Timothy Lathen** *February 07, 2018 19:07*

This is a version with the text still intact 


---
**Timothy Lathen** *February 07, 2018 19:14*

ok I'll try the ctrl -J 


---
**Timothy Lathen** *February 07, 2018 19:24*

ok I'll try the 

[drive.google.com - Plackards K40.lbrn](https://drive.google.com/open?id=1grP7_sg9bSWi9ZSjaLozLEbRsCQgMFnW)


---
**Timothy Lathen** *February 07, 2018 19:28*

I used explodetext command in Draftsight then print to svg file with no formatting then it seems to work once in lightburn alt -J but still 

![images/a89577345a5709e35ec350a0bae0a462.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a89577345a5709e35ec350a0bae0a462.jpeg)


---
**LightBurn Software** *February 07, 2018 19:34*

I’ll be able to check your file after work this evening. You can post your most recent version here. The message you’re getting could mean some shapes are simply not closed - try using the “edit -> Auto Close” function (Alt-C) to see if that helps.


---
**Timothy Lathen** *February 08, 2018 02:55*

**+LightBurn Software** 

I tried that The last version will cut using cut but the lettering doesn't come out all that great. Not sure why the lettering isn't closing. Thank's so much 


---
**LightBurn Software** *February 08, 2018 03:14*

After importing, ungrouping, and using the Auto-Join, what remains are the blue lines which are set to scan, but should be set to cut (they're just lines) and the exclamation points in the hazard triangles - They have a line that overlaps itself, and therefore could not be automatically joined.


---
**LightBurn Software** *February 08, 2018 03:26*

One additional comment - You mentioned the letters didn't look good. You've set the scan interval to 0.15mm, which is only 170 DPI. That's quite coarse - for text of this size I suggest at least 0.1mm, possibly 0.08mm for the scan interval.  0.08mm is quite clean.


---
**Timothy Lathen** *February 08, 2018 05:02*

ok thanks I'll try that 


---
*Imported from [Google+](https://plus.google.com/+TimothyLathen/posts/9QBew2ABZAL) &mdash; content and formatting may not be reliable*
