---
layout: post
title: "Got the z table hooked up but nothing happens when I try and jog it from LW4 what should i be looking for on the board and the controller as far as lights go?"
date: August 07, 2017 04:53
category: "C3D Mini Support"
author: "William Kearns"
---
Got the z table hooked up but nothing happens when I try and jog it from LW4 what should i be looking for on the board and the controller as far as lights go?





**"William Kearns"**

---
---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:02*

Need more info. What z-table? How is it wired in? Pictures?


---
**William Kearns** *August 07, 2017 05:15*

**+Ashley M. Kirchner** ![images/e83592dfbc64c5f792afee8eb4979377.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e83592dfbc64c5f792afee8eb4979377.jpeg)


---
**William Kearns** *August 07, 2017 05:16*

![images/5ab2c954e782d0c542a8f3c11ecfe144.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ab2c954e782d0c542a8f3c11ecfe144.jpeg)


---
**William Kearns** *August 07, 2017 05:16*

![images/e99b8f31b48b49661397e5c758a024f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e99b8f31b48b49661397e5c758a024f0.jpeg)


---
**William Kearns** *August 07, 2017 05:16*

![images/5a2ffd17f3eaa931e4a8df4af9a705cd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5a2ffd17f3eaa931e4a8df4af9a705cd.jpeg)


---
**William Kearns** *August 07, 2017 05:17*

![images/d6a874feb2765e85b8e45755a239e8be.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d6a874feb2765e85b8e45755a239e8be.jpeg)


---
**William Kearns** *August 07, 2017 05:17*

![images/1444fbb68476a98e1952d831846fc02f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1444fbb68476a98e1952d831846fc02f.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:22*

Looks like you followed my guide. Did you also make the config changes as stated in the guide?


---
**William Kearns** *August 07, 2017 05:30*

yes I copied and pasted them from the guide. 


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:34*

Ok, just going through checks here. Is the external PSU powered up? Are the green LEDs on the external driver on?


---
**William Kearns** *August 07, 2017 05:37*

it appears to be the light is green on the external PSU and i noticed there is only one light switched on the controller. There are two green ones right next to each other


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:44*

Ok, yeah, one is power, the other is an alarm. Ok, can you dump your entire config.txt into a pastebin ([pastebin.com](http://pastebin.com)) and send me the URL for it?


---
**William Kearns** *August 07, 2017 05:46*

yes will send later on Monday. thank you for looking! closing up shop now


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:52*

Cheers!


---
**William Kearns** *August 08, 2017 02:52*

[pastebin.com - # NOTE Lines must not exceed 132 characters  ## Robot module configurations : ge - Pastebin.com](https://pastebin.com/4TZ9zMQ1)



did i do that right?


---
**Ashley M. Kirchner [Norym]** *August 08, 2017 03:07*

Ok, so all of that looks ok at first glance. Next, in LW, did you enable the Z Stage?


---
**William Kearns** *August 08, 2017 03:08*

i keep getting an alarm lock? when i jog the z axis it started to move then stopped?


---
**William Kearns** *August 08, 2017 03:09*

![images/1f59e9d6b7993df6b268f66f2fe5678d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f59e9d6b7993df6b268f66f2fe5678d.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 08, 2017 03:14*

what happens if you clear the alarm, then jog again?


---
**William Kearns** *August 08, 2017 03:16*

just figured out the clear alarm. The motor turns but just barley @ its jerky put jog at 100mm and it maybe turns a quater of a turn. Is there any other setting i might be missing?




---
**Ashley M. Kirchner [Norym]** *August 08, 2017 03:26*

I need to see what your LW settings are. At 0.1mm it should move just a little bit, but at 100mm it should be spinning fast and smooth.


---
**William Kearns** *August 08, 2017 03:32*

doesnt turn at all just a high pitch hiss almost when the command is sent to it.

![images/bc6096455429a5a85d776fca1a976960.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc6096455429a5a85d776fca1a976960.jpeg)


---
**William Kearns** *August 08, 2017 03:35*

could it be motor current anyway to adjust that?


---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 03:36*

Set the red dip switches of the tb6600 for 1 amp and 1/16 micro stepping. Carefully note which way is on for them. 


---
**William Kearns** *August 08, 2017 03:38*

This is how it's set now![images/4bc72228170940f954f79bf2c4bc25f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4bc72228170940f954f79bf2c4bc25f0.jpeg)


---
**William Kearns** *August 08, 2017 04:13*

Set it to this and it makes a super light squeaking noise and it does not turn. ![images/f8f7c0f8bb31a2c34176329c39c96ea9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8f7c0f8bb31a2c34176329c39c96ea9.jpeg)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/hRrcXaA4zXs) &mdash; content and formatting may not be reliable*
