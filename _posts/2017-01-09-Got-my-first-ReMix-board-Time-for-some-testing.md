---
layout: post
title: "Got my first ReMix board. Time for some testing"
date: January 09, 2017 16:55
category: "General Discussion"
author: "Alex Hayden"
---
Got my first ReMix board. Time for some testing. Thanks **+Ray Kholodovsky**​.

![images/5ec56c5adcdf6847631344d766286864.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ec56c5adcdf6847631344d766286864.jpeg)



**"Alex Hayden"**

---
---
**Dushyant Ahuja** *January 09, 2017 17:01*

It's a great board. Check this out. 

[3dprinterchat.com - Smoothiefy your 3D Printer](https://goo.gl/17dKiM)

Smoothiefy your 3D Printer - 3D Printer Chat


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2017 17:12*

Make sure to cool those DRV8825's, they run hot!

We have a case design with a fan mount, it should be on the support site at [cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com)


---
**ThantiK** *January 09, 2017 20:55*

Those DRV8825's are going to give you moire effects if it's going on the delta you posted about.  You're better off with the A5984s: [http://www.panucatt.com/product_p/sd5984.htm](http://www.panucatt.com/product_p/sd5984.htm) -- they also do 1/32 stepping, and they also do 2A max output, except - they won't leave flaws in your prints.


---
**Dushyant Ahuja** *January 09, 2017 23:56*

**+Ray Kholodovsky** I couldn't find any case design; can you please let me know where exactly


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 02:26*

**+Dushyant Ahuja** Should be up now.  Please check the link again.


---
**Alex Hayden** *January 10, 2017 16:39*

**+ThantiK**​ they are actually going in a core XY setup. But what are moire effects?


---
**ThantiK** *January 10, 2017 16:46*

**+Alex Hayden** the DRV8825s have a positioning error when used with low inductance motors: [https://hackaday.com/2016/08/29/how-accurate-is-microstepping-really/](https://hackaday.com/2016/08/29/how-accurate-is-microstepping-really/)


---
**Alex Hayden** *January 10, 2017 17:27*

**+ThantiK**​​ after reading all the comments, they say a small mod of jumping decay pin to 5v fixes the issue because this makes it fast decay instead of the stock auto decay mode. The auto is causing the spike which results in the skip.﻿ after further digging found this. [http://cabristor.blogspot.com/2015/02/drv8825-missing-steps.html?m=1](http://cabristor.blogspot.com/2015/02/drv8825-missing-steps.html?m=1)

So no to the fast decay but yes to some diodes in each phase of your motor. This is for running low voltage steppers with a larger power supply voltage. 


---
**ThantiK** *January 10, 2017 19:56*

Just buy drivers that work, instead of trying to hack the DRV8825s into working imho.


---
**Dushyant Ahuja** *January 11, 2017 03:10*

**+ThantiK** Would the TMC2100 be better - they cost about the same as the SD5984s?


---
**ThantiK** *January 11, 2017 03:19*

The TMC2100s have a tendency to skip in silentstep mode - and they have far less amperage handling capability.  2A vs 1.2A (IIRC)


---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 03:20*

You would use them in spread cycle. Which is the "almost silent" / kinda quiet mode. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 16:44*

Quick update - I got the TMC2100 SilentStepSticks and have them running the X and Y (so far) of a large printer in Spreadcycle mode.  The results are great - I'm getting more strength than A4988 and none of the whine or thermal throttling issues of the DRV8825.  And this is still at 12v, I haven't updated the machine to 24v power yet.  But seeing as how my volcano can't keep the heat up, that's next on the list. 


---
**ThantiK** *January 23, 2017 16:47*

**+Ray Kholodovsky**, I suspect some of our issues with the TMC2100 were power supply noise related.  They really are great chips in spread-cycle mode.


---
**ThantiK** *January 23, 2017 16:48*

**+Ray Kholodovsky** Also -- there's a modified version or...version 2?  Of that chip that doesn't use the internal regulator for logic.  This keeps the chip even cooler.


---
**Alex Hayden** *January 23, 2017 16:48*

That sounds great.


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 16:50*

**+ThantiK** there is a 5v version that I think does that.  It's in their github repo but I haven't seen it available for sale much.  

Yeah, those are the things on our improvements list.  


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 16:52*

Here's a pic of them for X and Y. Still using DRV's on Z and E. There's a 120mm cooler master <s>cyclone</s> fan blowing down on them. ![images/5bf345ef2616c03ccca299bb28324380.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5bf345ef2616c03ccca299bb28324380.jpeg)


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/QHx5G9a2QGP) &mdash; content and formatting may not be reliable*
