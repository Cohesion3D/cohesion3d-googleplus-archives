---
layout: post
title: "Lasers and bricks, oh my! Originally shared by Bob Damato Making a few commemorative bricks with my Laser!"
date: August 10, 2018 04:43
category: "K40 and other Lasers"
author: "Jonas Rullo"
---
Lasers and bricks, oh my!



<b>Originally shared by Bob Damato</b>



Making a few commemorative bricks with my Laser! 





![images/9f8cc1aa578f245bd4c38168b65cd895.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9f8cc1aa578f245bd4c38168b65cd895.jpeg)



**"Jonas Rullo"**

---


---
*Imported from [Google+](https://plus.google.com/115922912883670862956/posts/4vxWTpcibhL) &mdash; content and formatting may not be reliable*
