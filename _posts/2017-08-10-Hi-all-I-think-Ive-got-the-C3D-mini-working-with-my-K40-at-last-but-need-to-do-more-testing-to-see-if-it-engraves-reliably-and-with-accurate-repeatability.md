---
layout: post
title: "Hi all. I think I've got the C3D mini working with my K40 at last but need to do more testing to see if it engraves reliably and with accurate repeatability"
date: August 10, 2017 18:42
category: "C3D Mini Support"
author: "Kma Jack"
---
Hi all. I think I've got the C3D mini working with my K40 at last but need to do more testing to see if it engraves reliably and with accurate repeatability. I am now using grbl firmware which hopefully will keep on working. 



Thank you to all who helped me through the frustration of getting it work.



Can someone please tell me how I get the PWM working? Do I need to connect new wire/s or do anything else? 



TIA



Eric







**"Kma Jack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 10, 2017 20:11*

It works straight away. On the documentation --> k40 --> troubleshooting/ FAQ page is a section on pwm testing and tuning. Essentially you run this line G1 X10 S0.6 F600 with different S values between 0 and 1. And different x values so it moves to a new place each time. 



I'm not sure if it's different for grbl, I think I saw Todd mention the range was 0-1000 instead of 0-1....


---
**Arion McCartney** *August 11, 2017 05:10*

0-1000 for GRBL-LPC. Search GRBL-LPC and you'll see some of my photos and settings I've used. I've tried to post my settings in all the photos I've posted for future reference and to possibly help others. Might help you. 


---
**Kma Jack** *August 11, 2017 09:50*

Thanks for the replies guys. I crated a cgoce for a photo and checked it.The values for S does change in the gcode but the laser is actually firing at the power setting that I set it to on the K40's LCD screen. What that means is that, I set the power to 0-35 in LW4 and 70% on the LCD screen. With that setting I was expecting a light engraving but got a dark one. I tried it again and manually changed power on the LCD screen as it engraved. I could see the difference where I changed the power. I started at 70%, let it engrave for about 10mm and went down to 60% for 10mm, then to 50, to 40, 30 and 20 and could see the engraving getting lighter as I changed the power. So the laser is actually firing at the strength set on the LCD and not at the S values set in the gcode.



Did I do something wrong or is the PWM not working as it should be?




---
**Kma Jack** *August 11, 2017 10:48*

**+Arion McCartney**, I searched for GRBL_PLC but couldn't find any of your posts, any chance you could post links to a couple of them please? 



Thanks






---
**Arion McCartney** *August 11, 2017 13:44*

**+Kma Jack** Sorry,  for whatever reason I assumed this was in the laserweb google plus group.  Here are some links.  Go about half way down on the last link and you'll see a photo of a test I did with different  PWM values in GRBL (helped me a lot).  The first two links I believe were done at 5000Hz.  This setting is $33 (PWM frequency) in the GRBL-LPC config .[https://plus.google.com/110543831642314750018/posts/aP8STtoPoDh](https://plus.google.com/110543831642314750018/posts/aP8STtoPoDh)

[https://plus.google.com/110543831642314750018/posts/37L8MA1XsPY](https://plus.google.com/110543831642314750018/posts/37L8MA1XsPY)

[https://plus.google.com/101442607030198502072/posts/G9q5ois38RF](https://plus.google.com/101442607030198502072/posts/G9q5ois38RF)


---
**Kma Jack** *August 11, 2017 14:07*

**+Arion McCartney**, thanks for the links but I won't need them, any more as I have taken the C3D mini board out and will not be using it again. I have had enough of the problems with it and decided to go back to the stock board that came with the K40, at least I can get some work done with that instead of all this try try this and try that. 



Sorry, not having a go at anyone just really frustrated that after my son spent so much money on buying the board as a present from me it turned out to be a piece of crap! 



Thanks for you help anyway, much appreciate it




---
**Ray Kholodovsky (Cohesion3D)** *August 11, 2017 14:17*

The setting Arion described is the grbl equivalent of the "pwm tuning" in out FAQ I referred you to earlier.  You change this value a few times until you do indeed see the full contrast of lights and darks in your output. 


---
**Kma Jack** *August 11, 2017 14:39*

**+Ray Kholodovsky**, if that was the only problem with the C3D I'd still have it in the K40 but what about all the other problems? What do I do about them? Why doesn't the card work like it is supposed to but has caused me and loads of others so much frustration? 



I thought I had it working last night apart from the PWM issue but when I went to use it this morning for a job that doesn't require PWM it started playing up again and not starting the job where it was supposed to. 



I've really had enough of it Ray and have taken the stupid thing out. My son bought me the card so that I can get better results and make my work a bit easier but instead it had caused me nothing but headaches and frustration. 



This card is not an upgrade Ray, it's a very bad downgrade! 

Sorry if you don't like hearing is but that is the truth!


---
**Claudio Prezzi** *August 12, 2017 07:47*

**+Kma Jack** Sorry to say that, but I belief it's better that you stop fiddling around and go back to the original board. 



But don't blame the board or the software for your lack of patience! 



I don't know <b>any</b> user that was not able to get it to work with patience and the help of the community. And they are all happy to have made it!


---
**Arion McCartney** *August 12, 2017 08:08*

**+Claudio Prezzi**​ couldn't have said it better.  **+Kma Jack**​ I find your comments about Ray's board quite rude and I'm sure they are not appreciated. Myself and many other users have been using Ray's board successfully for quite some time now. Just because you can't get it working doesn't mean the board is a downgrade by any means. In fact, the board is quite an upgrade compared to the stock board. It just sounds like the technical level may be a bit over your head. I hope no other users or potential users get turned off or discouraged by your extreme comments here. The great thing about Ray's board and LaserWeb is the community that surrounds them. People here are willing to help, and try to help.  


---
**Kma Jack** *August 12, 2017 09:17*

Patience? For something that's supposed to be a 20 minute work? 

How long is the patience supposed to last? 



You only have to look at the form and see how many people are having problems.



People are willing to help, I have no problem with that and appreciate it very much but there comes a time when enough is enough.



My son bought me the C3D to make it easier for me and so that I can connect a rotary table to do more things but as it turns out I have to spend weeks and weeks just to get the board to work and then start thinking about the rotary table which could bring more problems with it!



Patience I have loads but there has to be a end to everything and my patience is at  the end. 



This board is being sold as a 20 minute swap, it is anything but that, it is full of problems. 



**+Arion McCartney**, let's talk about "technical" levels. Is this board only for those who have a high level of "technical skills"? Not everyone has a high level of technical skill like some people who use the cohesion board/s, what about them, are they better not buying it than? I thought this board was for everyone, sorry if I misunderstood that!





I thought that the whole point of this board was to make it easy and better for the users and not demand weeks and weeks of patience. 



I am glad some people have got theirs working but I have given up on mine as have some others I have found out since. 



A good board without a proper working software is worth nothing! 



You have no idea how much patience I've got or what I've achieved in my life so to me it seems like you are just 



What's wrong with wanting something to work with lot less problems and within the timeframe it is claimed it will? 



My son didn't spend £150 so I can spend weeks and weeks in support forum, he bought me something to make my laser better which it didn't!


---
**Arion McCartney** *August 12, 2017 17:04*

**+Kma Jack** You are right, I should not assume "technical level" and I do apologize for that.  I will leave it at that.  Good day.


---
**Kma Jack** *August 12, 2017 18:00*

**+Arion McCartney**. actually you did a worse disservice than my rant to the board and the owner/s by bringing the "technical level" in to play. People below a certain technical level needed for this board might just stay away from it. So maybe in the future you should think twice before you say anything or just stay out altogether so as not to make things worse. That's just a friendly suggestion and not said in anger to get at you, hope you understand that. 



My anger/frustration is aimed at the board and it's makers and not at anyone else who have nothing to do with the company who makes the board. 



Apology accepted. 



Good day


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/6etQg59goky) &mdash; content and formatting may not be reliable*
