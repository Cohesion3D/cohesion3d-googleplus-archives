---
layout: post
title: "I have your power brick supplying both the stepper & the board now"
date: December 14, 2018 22:16
category: "C3D Mini Support"
author: "Tricia R"
---
I have your power brick supplying  both the stepper & the board  now. Blue, clipped. Now okay? 

![images/f499d34df75ae04aa3991f6274fd1c41.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f499d34df75ae04aa3991f6274fd1c41.jpeg)



**"Tricia R"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 22:34*

Presumably.  I can't see everything in your pics based on angle and zoom. 


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 22:36*

Did you set the switches on the external driver as shown in the Z table/ rotary guide? 


---
**Tricia R** *December 14, 2018 22:37*

This help? ![images/5c0d12fce04e8c640b3e9dfc253e9484.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5c0d12fce04e8c640b3e9dfc253e9484.jpeg)


---
**Tricia R** *December 14, 2018 22:42*

Yes the switches are set accordingly 


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 22:46*

Cool.  That covers everything I can see. 


---
*Imported from [Google+](https://plus.google.com/107433814983162842539/posts/QVdH5AXvUWn) &mdash; content and formatting may not be reliable*
