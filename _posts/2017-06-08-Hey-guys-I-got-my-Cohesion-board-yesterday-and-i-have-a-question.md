---
layout: post
title: "Hey guys! I got my Cohesion board yesterday and i have a question"
date: June 08, 2017 03:02
category: "General Discussion"
author: "Tommy MD"
---
Hey guys! I got my Cohesion board yesterday and i have a question. Is this sound normal ? It dosnt do it with the control board that came with the laser, only when i plug in the cohesion one and use laserweb.




{% include youtubePlayer.html id="txcGR_VKx5c" %}
[https://youtu.be/txcGR_VKx5c](https://youtu.be/txcGR_VKx5c)





**"Tommy MD"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 03:05*

Sounds like normal stepper music to me :) possibly because of higher current driving them, which is a good thing. 


---
**Tommy MD** *June 08, 2017 03:13*

So it's normal ? Is there a way to make the homing command faster ? It really only do it when it's going slow.




---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 03:18*

Everything sounds like what I'm used to, yeah. 



You can fiddle with the smoothie config regarding homing rates. Note that at the end it does a back off and touch again at a slower rate. This is all on purpose. 


---
**Ashley M. Kirchner [Norym]** *June 08, 2017 03:30*

As Ray said, it's a slower, more precise homing. With the stock controller, it homes at a much faster rate and it's not nearly as precise. With the C3D board, when it hits the limit switch, it backs off 5mm, then re-homes again at a much slower rate, for a more precise mark. All in the name of science ... or beer.


---
**Tommy MD** *June 08, 2017 03:33*

I just go into the config.txt, change it, save and start the machine ?


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 03:35*

Not sure what you're changing, but:

Change, save, safe eject/ dismount drive, and reset board. 


---
**Tommy MD** *June 08, 2017 03:36*

How do i reset the board ? Sorry for all those question.


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 03:38*

Little yellow button top right corner of board. 


---
**Tommy MD** *June 08, 2017 03:45*

Found it thx Ray! I did change the homing "fast" speed and it fixed the noise! 


---
**Tommy MD** *June 08, 2017 05:39*

One last question haha. Is it normal i feel my laser is less powerful ? With the m2 nano board i can cut tru some stuff with specific that won't work in laserweb ? take like 3-4 pass with the same setting in laserweb.


---
**Claudio Prezzi** *June 08, 2017 09:04*

What you hear is just mechanical resonance, that depends on steprate and other things like stepper current. More current means more torque but also more noise.

Usualy I set the current as low as possible for still geting enough torque and not loosing stepps.


---
**Claudio Prezzi** *June 08, 2017 09:07*

You should get the same cutting power as with m2 nano if everything is correctly configured. Check that max PWM value is set the same in smoothieware config and LW4.


---
**Joe Alexander** *June 08, 2017 10:29*

sounds like my machine did until i had a stepper die on me(x-axis). the one i replaced it with is whisper quiet in comparison(went from sounding like yours to about 1/8 the amount of sound), still have to try replacing the y-axis also to see if i can eliminate nearly all of the noise. forget the number but it ended with 0404s, its a common one used in 3d printers etc.

BTW not saying yours sound like their dying, just what the stock ones sound like.


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 14:42*

Config caps at 0.8, you can change the laser max power line to 1 


---
*Imported from [Google+](https://plus.google.com/117729529567281765798/posts/aaocQpkohtU) &mdash; content and formatting may not be reliable*
