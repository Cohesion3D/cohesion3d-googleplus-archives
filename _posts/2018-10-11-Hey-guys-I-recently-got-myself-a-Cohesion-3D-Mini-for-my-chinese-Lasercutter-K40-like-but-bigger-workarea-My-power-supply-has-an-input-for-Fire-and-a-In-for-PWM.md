---
layout: post
title: "Hey guys, I recently got myself a Cohesion 3D Mini for my chinese Lasercutter (K40 like but bigger workarea) My power supply has an input for Fire and a In for PWM"
date: October 11, 2018 09:17
category: "C3D Mini Support"
author: "Lukas Bachschwell"
---
Hey guys,  I recently got myself a Cohesion 3D Mini for my chinese Lasercutter (K40 like but bigger workarea)



My power supply has an input for Fire and a In for PWM. I am using the K40 connector and Pin 2.5 as the laser_module_ttl_pin, and I want to use Pin 2.4 as the pwm pin to control the IN on the power supply. 



My problem is that I had weird results when I connect the mid contact of the "K40 pwm header" directly to the IN (Control works, but the laser does not even turn on under 5% and so on...)



Yesterday I tried to drive the PWM over an Optocoupler, but I completely failed to wire it up correctly so the output is able to drive the LED...  It is a bit annoying but I was not able to find the board schematic of the Mosfet 4 section anywhere, and the pin-out diagram does not have all connections described, so can somebody tell me how this section is intended to be connected ?!



Thanks





**"Lukas Bachschwell"**

---
---
**Anthony Bolgar** *October 11, 2018 12:08*

Standard on upgrading Chinese power supply is to pulse "L" (or fire)with the PWM, just use a pot across 5V and IN to use as a max current limiter (Set and forget). **+Don Kleinschnitz Jr.** can elaborate more if you have any questions, he is the one that did all the testing on the PSU's to figure out the optimal way of controlling them.




---
**Lukas Bachschwell** *October 11, 2018 13:20*

Thanks for the response, yes I got that, but since I do have 2 inputs and all the options are there in software, why not use it in a more elegant way? My question is just about the circuit of the Mosfet4 part, is there any schematic to that or anything? I could not even find the pinnames for the K40 pwm header anywhere... ?


---
**Don Kleinschnitz Jr.** *October 11, 2018 13:22*

Post a picture of your LPS please.


---
**Lukas Bachschwell** *October 11, 2018 13:27*

Hey, I am not home right now, but it is more or less the same as this one here 
{% include youtubePlayer.html id="pWvv7TFcK5Q" %}
[youtube.com - How to use a CO2 Laser Power Supply](https://www.youtube.com/watch?v=pWvv7TFcK5Q)




---
**Don Kleinschnitz Jr.** *October 11, 2018 14:29*

Look at this blog you will find schematics and pinouts for everything.

[donsthings.blogspot.com - Don's Things](https://donsthings.blogspot.com/)



The C3D schematic is on the 3CD website.



Although some sources on the internet suggest using the IN for PWM it requires special interface considerations to drive it properly.



Turns out you need manual control of the lasers power so that you can account for laser deterioration over time and other practical factors.



For this reason we normally control the IN with a pot for intensity and PWM control the L pin on the LPS.  Where the L is on the LPS depends  on exactly what connectors you have on your supply. There are multiple vintages. 

This approach is explained on my blog and the exact connections for the C3D are on their website.



When you can, post a picture of the LPS with its connectors.


---
**Lukas Bachschwell** *October 11, 2018 19:14*

Hey don, please prove me wrong but on your blog (that I already know) there is no schematic of the C3D Mini, and there is also NO schematic on the C3D knowledge base. There only is the INCOMPLETE pinout diagram of the mini. My question is not really related to the wiring on the PSU side, I am interested how Mosfet 4 is intended to be used on the C3D Mini. All That I can see is a Gate input that is connected to the center pins of the two headers (mosfet4 and K40 pwm header) and that either means that I do not understand the schematic of the mini correctly or that It does not make sense...





"the IN for PWM it requires special interface considerations to drive it properly."



Yes that might be, my plan is simple: I want the signal going to that point to look exactly the same as the signal coming from my stock digital power panel.



And as for the poweradjusting: if I ever want to adjust this I would do it in the Smoothie config, not with a pot...



So do you have details about the schematic of the C3D Mini?






---
**Anthony Bolgar** *October 11, 2018 19:30*

Maybe at [github.com - Cohesion3D](https://github.com/Cohesion3D)


---
**Don Kleinschnitz Jr.** *October 11, 2018 19:33*

**+Lukas Bachschwell** 

The C3D schematics used to be there? **+Ray Kholodovsky** can get them for you I imagine.



<i>Yes that might be, my plan is simple: I want the signal going to that point to look exactly the same as the signal coming from my stock digital power panel.</i>



The LPS needs a pwm signal to control power from firmware. The digital panel does NOT output a pwm signal, it is a static DC voltage controlled by the panel setting. Therefore you cannot simply implement a function ...  <i>to look exactly the same as the signal coming from my stock digital power panel</i> 

........ apples and oranges.



Using the standard schema of control you get manual and software control [depending on the software you use].




---
**Lukas Bachschwell** *October 12, 2018 17:35*

Yes please, some scematics of the mini would really help.



No Don, these are neither apples, nor oranges, this is a simple PWM Signal comming from my panel (set to 50.0 in the case of this picture)





![images/07d985ce88854d7b215e1808bd2add2d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07d985ce88854d7b215e1808bd2add2d.jpeg)


---
**Lukas Bachschwell** *October 12, 2018 17:36*

![images/184bb2d3f2b998ee807d6204bb1f4e40.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/184bb2d3f2b998ee807d6204bb1f4e40.jpeg)


---
**Lukas Bachschwell** *October 12, 2018 17:42*

**+Anthony Bolgar** thanks for the github link, but on this repository I could also not find the right scematics. The only ones in there are called Cohesion ReMini and look like a board that I think does not exist currently anymore...

[github.com - Cohesion3D/COR3D-Mini](https://github.com/Cohesion3D/COR3D-Mini)


---
**Don Kleinschnitz Jr.** *October 13, 2018 01:11*

Well now ... that's not a signal I have seen on a standard K40 panel :)! The PWM signal from the C3D can mimic this if you get the voltage levels compatible.


---
**Anthony Bolgar** *October 13, 2018 01:15*

Where did you connect the leads to pick up that signal? As Don stated, that is unlike any K40 signal we have seen. Could you take some pics of the panel and the data connection points. I am very curious. Would love to see if the standard panel I am used to could be rewired to allow that output to the PSU.


---
**Don Kleinschnitz Jr.** *October 13, 2018 02:42*

**+Anthony Bolgar** I'm guessing it is on the IN pin of the LPS. The digital panel could control power with a PWM signal on IN just as well as a static 0-5V signal. If the voltage levels were correct.

In this way the panel controls the overall power and the L pin adds the  digital power. Actual power =  PWM (IN) * PWM(L).

I do not see any obvious advantage to PWM(IN)?


---
**Don Kleinschnitz Jr.** *October 13, 2018 17:20*

"Just when you thought it was safe to go in the water"



 **+Lukas Bachschwell** **+Anthony Bolgar**



**+Lukas Bachschwell** great catch! You are absolutely right! These digital panels output a PWM control on "IN". Your K40 is NOT an exception and as you claimed these are not "Apples and Oranges". I never thought to look at it with a scope....



I updated this post with  details.

 

[https://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html](https://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)



I traced the output circuit:

[https://photos.app.goo.gl/nvCi5QEZDGWo736y7](https://photos.app.goo.gl/nvCi5QEZDGWo736y7) 



I still would recommend using the "L" pin for power control from the C3D. It will do exactly what the "IN" output is doing only under GCODE control. Further perspective is in the post.

![images/8bbd55dca7e632dbc13923e560b1af2a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8bbd55dca7e632dbc13923e560b1af2a.jpeg)


---
**Don Kleinschnitz Jr.** *October 13, 2018 18:59*

**+Lukas Bachschwell** I found this old zip so I do not know how current it is or if its your board.

I will leave this shared for a few days.

Check in with **+Ray Kholodovsky** who could update you.



[drive.google.com - C3D Mini rev2-1.zip](https://drive.google.com/file/d/1jtxa4DkRlmLGVcKcFpXTbGGwVR_D8tYc/view?usp=sharing)


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 19:22*

C3D Mini Design Files: [https://www.dropbox.com/sh/z69pps4bkr04ken/AACz2QzB9jaMkkMLR_4QOTc0a?dl=0](https://www.dropbox.com/sh/z69pps4bkr04ken/AACz2QzB9jaMkkMLR_4QOTc0a?dl=0)


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 19:28*

Ok, here goes:

When I first started doing C3D we did what you are asking about.  Replaced the pot with the "K40 PWM" Signal going to IN on the LPSU.  The second signal was to L and this turned the laser on at the beginning of the job then it was all up to IN to control the laser on and off, and L turned off at the end of the job.  Just like an arming switch.  You can even see the old guide here: [dropbox.com - C3D Mini K40 Laser Installation (Carl's guide v1) (1).pdf](https://www.dropbox.com/s/tjmmsitugpzj79k/C3D%20Mini%20K40%20Laser%20Installation%20%28Carl%27s%20guide%20v1%29%20%281%29.pdf?dl=0)



This did not go well. Grayscale resolution was bad, laser could turn on unexpectedly when board booted and pins were floating for a few seconds, etc...



The way it works now is as described above.  Pot stays in play on IN to set the power maximum and you can turn this down to just as high as you need so we have the maximum grayscale range to work in. 



This is how the last 1200 boards have been wired. Please just do this. 


---
**Lukas Bachschwell** *October 24, 2018 19:18*

Hey guys, thanks for the responses and sorry for not yet reporting back. So the solution I now ended up with is pretty straight forward: I am now using the servo output to control the IN via pwm. Since the voltage levels are different I am using a hex4050 Non-Inverting buffer IC that gets its 5V supply from the LaserPSU. Additionally I am using the pin on  the K40 Header to drive the LO input of the PSU.

In the smoothie config I have set this up so this pin is the Laser TTL Pin



As to your concerns **+Ray Kholodovsky**: As long as the pin that would usually control the laser (the Laser TTL pin in my setup) does not do anything strange this setup should be fine as well, I have not yet experienced strange behaviours



The setup works pretty well and the power values behave very similar as when I use the original digital panel, also the signal on the osciloscope looks pretty much the same.



Meanwhile I discovered today that I really should recalibrate the mirrors =P



Thanks for the schematics though I will take a look at them, especially because I seem to have killed the mosfet on pin 2.4, not sure why, but also not important 



Thanks all for your help :-)


---
*Imported from [Google+](https://plus.google.com/+lbsfilm/posts/RpyEX38D18p) &mdash; content and formatting may not be reliable*
