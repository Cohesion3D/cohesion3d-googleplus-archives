---
layout: post
title: "Hi I have a question. This is a picture of my stock M2 Nano board in my K40 laser"
date: March 14, 2018 18:54
category: "C3D Mini Support"
author: "Tim Phillips"
---
Hi    I have a question. This is a picture of my stock M2 Nano board in my K40 laser. Do the X Y stepper motor connections look right? Looks like the yellow and white wires are in the wrong place on the one on the right in the picture. Just wanted to make sure before installing new board.   Thanks.

![images/0107b907df66582d08787dd7cb6b9323.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0107b907df66582d08787dd7cb6b9323.jpeg)



**"Tim Phillips"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2018 19:11*

Wire colors don't matter, plug orientation does. And if you get it wrong your axis will move the wrong way so just flip it.  

And along these lines, the origin in LB is the FRONT LEFT.  


---
*Imported from [Google+](https://plus.google.com/108559807399850911893/posts/MCaATxR1Jsx) &mdash; content and formatting may not be reliable*
