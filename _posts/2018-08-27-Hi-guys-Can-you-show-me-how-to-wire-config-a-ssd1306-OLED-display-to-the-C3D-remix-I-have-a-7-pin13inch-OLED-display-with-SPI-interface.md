---
layout: post
title: "Hi guys Can you show me how to wire/config a ssd1306 OLED display to the C3D remix I have a 7 pin1.3inch OLED display with SPI interface"
date: August 27, 2018 02:30
category: "General Discussion"
author: "Xiaojun Liu"
---
Hi guys，



Can you show me how to wire/config a ssd1306 OLED display to the C3D remix？



I have a 7 pin，1.3inch OLED display with SPI interface. These 7 pins are GND VCC D0 D1 RES DC CS. The display is shown in the following pic. I connected these pins to the exp1 and exp2 pins on C3D board，and write the config.txt file，as pic 2 and 3 showed，but screen didnt work. Then I changed the RES pin to the 2.11 pin in ext2，changed config.txt file accordingly，as pic 4，5 showed，but didn't help. 



Unfortunately I couldn't find any good instructions on this one:( Only saw some talented guys have succeeded, like **+René Jurack** and **+Ray Kholodovsky** etc. I’ve tried my best to gather and understand the pieces of information they left. But I may understand or do things wrong. 



Any help is appreciated!



BR

XJ 



![images/5dccc7bc2fdcd32b76f703a69ea3ef2f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5dccc7bc2fdcd32b76f703a69ea3ef2f.jpeg)
![images/499ee27d6ff0c6448013090ecf36ab41.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/499ee27d6ff0c6448013090ecf36ab41.jpeg)
![images/98a862b7288299d329a88401113285b1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/98a862b7288299d329a88401113285b1.png)
![images/ef15748125113573162eb315f415840f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef15748125113573162eb315f415840f.jpeg)
![images/f138f6e875e739277c3a8e3ad843f5f9.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f138f6e875e739277c3a8e3ad843f5f9.png)

**"Xiaojun Liu"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2018 02:45*

I have some pics in an old chat from a year ago, I may be able to dig them up for you.  It looks like your LCD has 7 header pins, so this is good, that should be the correct one. Please ping back if you do not hear from me in a day or so. 


---
**Xiaojun Liu** *August 27, 2018 02:55*

**+Ray Kholodovsky** Yes, I've tried my best to understand your old post, but unfortunately that thread ends up with no documented instructions :-( And I'm not  familiar electronics. I think smoothieware's wiki page (panel section) should be improved too **+Arthur Wolf** .


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2018 03:11*

So you already consulted this: [plus.google.com - Has anyone tried an SSD1306 OLED LCD (http://s.aliexpress.com/QNzQRnaQ) with ...](https://plus.google.com/+RayKholodovsky/posts/bFSjtEa8qbx) ?


---
**Xiaojun Liu** *August 27, 2018 03:16*

**+Ray Kholodovsky** Yes:) 


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2018 03:58*

The main thing is the a0 pin which is kind of vague in my thread, but it looks like you did what I would do. 


---
**Xiaojun Liu** *August 27, 2018 04:41*

**+Ray Kholodovsky** I am also not sure if I connected D0 D1 right. It is more easier to correct if I just messed up with the pins, I’m hoping it is the case. I was blindly wiring them, following all the guides I could find. Did not really certain about what are those pins meaning. 


---
**Xiaojun Liu** *August 28, 2018 13:29*

Today I tried another 6pin SSD1306 display，the pins are DC，RES，D1，D0，VCC，GND. I connected DC to P0.28，RES to P2.11 in EXP2. Then I got something but unreadable. I think this might be useful. **+Ray Kholodovsky** have you figured out how to wire the 7pin SPI？![images/3bc15b7a88fe5fa7a6a3fa2731675387.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3bc15b7a88fe5fa7a6a3fa2731675387.jpeg)


---
**Xiaojun Liu** *August 28, 2018 14:17*

SSD1306 Datasheet shows the following:



RES#: This pin is reset signal input. When the pin is pulled LOW, initialization of the chip is executed. Keep this pin HIGH (i.e. connected to VDD) during normal operation.





CS#: This pin is the chip select input. (active LOW)





D/C#: This is Data/Command control pin. When it is pulled HIGH (i.e. connect to VDD), the data at D[7:0] is treated as data. When it is pulled LOW, the data at D[7:0] will be transferred to the command register.



In I2C mode, this pin acts as SA0 for slave address selection. 



When 3-wire serial interface is selected, this pin must be connected to Vss.



 

![images/aedf674a8e5ef809dcf0c770257e2ba3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aedf674a8e5ef809dcf0c770257e2ba3.png)


---
*Imported from [Google+](https://plus.google.com/+XiaojunLiu/posts/eHQUfev3CWZ) &mdash; content and formatting may not be reliable*
