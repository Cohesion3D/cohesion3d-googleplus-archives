---
layout: post
title: "I have a K40, 3d Cohesion, windows 7 and the latest version of Lightburn"
date: December 16, 2018 20:25
category: "K40 and other Lasers"
author: "John Cahall"
---
I have a K40, 3d Cohesion, windows 7 and the latest version of Lightburn. I just want to engrave jpg pix of my grandkids on wood. Running out of time for Christmas. Please either point me in the right direction on where to find or give me the most common settings on both my laser and in lightburn. Thank you very much.





**"John Cahall"**

---
---
**John Cahall** *December 16, 2018 20:28*

Also do I need to change  the color digital photo to grayscale? I have gimp and I have Photoshop elements. Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *December 16, 2018 20:37*

Jarvis dither, 4% power, 250 DPI, 80 mm/sec.  

Try that one or 4% power, 400 DPI, Newsprint dither, 100 mm/sec.


---
**John Cahall** *December 17, 2018 02:42*

**+Ray Kholodovsky** Thank you! 


---
**John Cahall** *December 17, 2018 13:03*

Do I need to convert it to b&w bmp first? 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2018 20:28*

You should not have to, no. 


---
*Imported from [Google+](https://plus.google.com/106691667969444964893/posts/fzM5uBszdEm) &mdash; content and formatting may not be reliable*
