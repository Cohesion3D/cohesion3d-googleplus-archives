---
layout: post
title: "This is the right orientation of the LAN Module in the Remix Board"
date: January 10, 2017 18:00
category: "General Discussion"
author: "Marc Pentenrieder"
---
This is the right orientation of the LAN Module in the Remix Board😀

![images/6630c60a8efd9ba86d3e936ff5878703.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6630c60a8efd9ba86d3e936ff5878703.jpeg)



**"Marc Pentenrieder"**

---
---
**Marc Pentenrieder** *January 10, 2017 18:12*

**+Øyvind Amundsen**​ Hi, the orientation on your pictures is also wrong. Turn the lan module by 180 degrees.


---
**Øyvind Amundsen** *January 10, 2017 18:16*

**+Marc Pentenrieder** yes - have not started on the Ethernet testing yet - inserted it like I found in an image, but I'll fix it before I start testing - thank you.

Do you plan to use it in a 3D printer or CNC milling machine? 


---
**Marc Pentenrieder** *January 10, 2017 18:23*

**+Øyvind Amundsen** Hmmm and i used your pictures first ;-D

I will use it on a big self build "machine", which i will use for 3d printing, but also planning on cnc milling. Building is a lot of work and i already crashed one axes by mistake when testing movement without endstops ;-(    But it is already repaired ;-)  I attached a photo of the unfinished "machine", whith a friend who helps me building.

![images/6c04550e93699a56e668da570e460acc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c04550e93699a56e668da570e460acc.jpeg)


---
**Øyvind Amundsen** *January 10, 2017 18:28*

**+Marc Pentenrieder** seems like a great project 👍🏻


---
**Marc Pentenrieder** *January 10, 2017 18:34*

Thanks, yes it is a geat project. Perhaps we can help each other controlling/configuring the servo motors ?! I am completely new to this, but still learning ;-)


---
**Øyvind Amundsen** *January 10, 2017 18:40*

**+Marc Pentenrieder** I use external stepper drivers and stepper motors in my build. Have not used a smoothie based controller so I'm new to this - but I'll try to help if I can. 


---
**Steve Anken** *January 10, 2017 19:09*

Servos are pretty easy and take step and dir like steppers but you tune them separately. Look at Clearpath for the new integrated servos so you don't need a lot of wiring from controller to the motor. You don't need end stop switches because they can use hard stops with force sensing. We also have a build going for a 9'x5' machine but only have the frame and parts. If you use a Z servo you will need a break for when the power is off. Here's a video of someone's build and there are others out there that show off the speed, torque and lack of noise compared to steppers.




{% include youtubePlayer.html id="V0biZJSnepU" %}
[youtube.com - Clear Path Servos CNC Router 750 IPM](https://www.youtube.com/watch?v=V0biZJSnepU)




---
**Øyvind Amundsen** *January 10, 2017 19:43*

What software do you plan to run on the computer when using it as CNC?  I consider bCNC




---
**Marc Pentenrieder** *January 10, 2017 19:46*

First i thought mach3 or perhaps fusion360 direct, but i dont know and will decide when its time. First I want to get printing ready ;-)


---
**Øyvind Amundsen** *January 10, 2017 19:49*

I plan to use Fusion for Cad/Cam. I do not know if mach3 support smoothieware based controllers


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 20:34*

I'm glad the ReMix power users have met each other. I have heard positive things about both Fusion CAM and Mach3 software working with smoothie.  I have not run either personally. For Mach3 there was one thread where Mark Cooper explained you just have to tell it to use the correct serial port or something like that. 


---
**Øyvind Amundsen** *January 10, 2017 20:36*

**+Ray Kholodovsky** more info about Mach3 and smoothie would be great 👍🏻 


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 20:38*

This is the thread I was referring to: [groups.google.com - Mach3 and Smoothieboard](https://groups.google.com/forum/m/#!topic/smoothieware-support/jDiA5W6H-Jc)






---
**Jim Fong** *January 11, 2017 01:58*

Mach3 itself cannot control grbl/smoothie.  However the same windows PC can run one of the many compatible serial/USB gcode drip feed programs. 



Normally Mach3 sends out step/direction commands out via a parallel port.  There are many compatible USB/Ethernet motion controllers that can be used instead of the parallel port but each requires a Mach3 device driver to make it work.   



There is no grbl/smoothie device driver available  for Mach3 that I know of.   



Since grbl/smoothie are stand alone motion controllers there is no reason why you would need Mach3 in the first place. 










---
**Jim Fong** *January 11, 2017 02:08*

**+Marc Pentenrieder**  I run servos on several machines.  I'm waiting for my cohesion board....any questions about servo system, please ask. I run brushed Geckodrives, brushless Applied Motion, AMC, Copley, Parker Compumotors. I even have a couple of Tecknic servo motors but not any Clearpath ones yet. 


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/hupmg7L3jJJ) &mdash; content and formatting may not be reliable*
