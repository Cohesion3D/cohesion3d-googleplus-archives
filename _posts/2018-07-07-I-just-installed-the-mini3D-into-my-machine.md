---
layout: post
title: "I just installed the mini3D into my machine"
date: July 07, 2018 18:16
category: "C3D Mini Support"
author: "K. J. VanWormer"
---
I just installed the mini3D into my machine. My motors will move, but the laser isn't firing. When running a test fire on both the panel and the power supply, it fired just fine. What'd I miss? 





**"K. J. VanWormer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2018 18:47*

Impossible to say without pictures of your wiring and more details. 


---
**K. J. VanWormer** *July 08, 2018 19:21*

Alright, here's the pics of my wiring... ![images/fb0eeb8145d16b6f642f1bc5e85c653d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fb0eeb8145d16b6f642f1bc5e85c653d.jpeg)


---
**K. J. VanWormer** *July 08, 2018 19:21*

![images/da60b451c6a0c1b348812bd617531069.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/da60b451c6a0c1b348812bd617531069.jpeg)


---
**K. J. VanWormer** *July 08, 2018 19:21*

![images/87a91f96fb832feb35b74d1ec3ffa8b4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/87a91f96fb832feb35b74d1ec3ffa8b4.jpeg)


---
**K. J. VanWormer** *July 08, 2018 19:22*

What additional details are needed?


---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2018 20:37*

Looks good to me. Do this: 



Remove the blue wire from the mini board and touch it to ground - presumably the brown wire. See if that fires your laser. 


---
**K. J. VanWormer** *July 08, 2018 23:54*

I just tried touching thr laser wire (blue) to the ground, and yes, it fired. 


---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2018 23:57*

Then you should be good.  Put it back to that screw terminal. 



Power back up, connect in Lightburn, make sure you can jog around.  Go back to 0,0 then send this command in console



G1 X10 S0.8 F600

This should move to X10 (hence don't be at X10 already) while firing. 


---
**K. J. VanWormer** *July 09, 2018 00:11*

Alright. Plugged that command in. No fire. 


---
**K. J. VanWormer** *July 09, 2018 00:19*

Whoa. Nevermind. We're cooking! 


---
*Imported from [Google+](https://plus.google.com/101105211923721675912/posts/d7YsEn4kF1C) &mdash; content and formatting may not be reliable*
