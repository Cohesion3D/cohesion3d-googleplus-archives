---
layout: post
title: "I want to use the mosfets on the Cohesion 3D mini"
date: May 05, 2017 07:54
category: "C3D Mini Support"
author: "Andy Knuts"
---
I want to use the mosfets on the Cohesion 3D mini. There's a FET IN connector and two mosfet output connectors. 

If I want to feed 12V to the connected devices, should I just connect a 12V psu to the FET IN connector and the devices to the outputs?

Or will I have to set any jumpers like with a smoothieboard.. I can't seem to find much info about that for Cohesion3D boards..





**"Andy Knuts"**

---
---
**Griffin Paquette** *May 05, 2017 11:36*

Are you wanting a constant on 12V or are you looking for a switchable current using the mosfets?


---
**Griffin Paquette** *May 05, 2017 12:21*

If you want a constant 12V, you would either have to have the fet drain all the time or use one of the constant headers that are on the board.


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2017 13:34*

There are no jumpers. There is very simply a main power in (this is what the k40 power connector is doing) and a fet in for the 2 large mosfets in the bottom left area. Now, the left one, 2.5, is used for L pwm. So don't use that. But you can use the right one, 2.7. What do you want to power? 


---
**Andy Knuts** *May 06, 2017 07:00*

I actually want to control 2 things -> 1 relay for enabling/disabling air-assist pump and second: enable/disable red laser using a 12v-3v DC-DC converter that powers the 3v red line lasers...


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2017 02:36*

You should probably use the larger 2.7 mosfet for the relay. And don't forget to put a fly back diode in with it across the terminals of the relay coil. 

For the laser perhaps you could an external mosfet circuit powered by one of the small mosfets at the top right or a raw io pin of the board. It really depends on how much current the red diode takes. 


---
**Andy Knuts** *May 07, 2017 18:04*

Is there any documentation available on the available raw io pins and where they are located on the pcb?




---
**Andy Knuts** *May 12, 2017 16:47*

?




---
**Ray Kholodovsky (Cohesion3D)** *May 12, 2017 16:50*

I thought I answered this... 

There is the pinout diagram for the mini on the docs site.  The available gpio pins are: 

the XMX and YMX pins, the "servo header", and Step Dir En pins from any available stepper driver sockets you are not using. As discussed the 2 small mosfets 2.4 and 2.6 at the top might also be feasible for your intended purpose. 


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/PgdgksCohXG) &mdash; content and formatting may not be reliable*
