---
layout: post
title: "New K40 Upgrade Instructions for the Cohesion3D Mini are finally up"
date: March 14, 2017 19:24
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
New K40 Upgrade Instructions for the Cohesion3D Mini are finally up.  Please see here and provide feedback!



[https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Bob Buechler** *March 15, 2017 01:43*

I've completed these steps along with pics from a different thread for the digital front panel. Got it and LW4 installed and configured. I'm able to connect via USB and the motors are taking jog commands. 



Unfortunately, it tries to home to a location outside of my stops. I see there's a macro button for setting Zero, but before I monkey with it too much and break my belts, I wanted to stop and ask if there's a homing procedure somewhere that I just haven't found yet that I should be following?


---
**Bob Buechler** *March 15, 2017 01:43*

Also: Once the machine enters an Alarm state, is the only way to clear it a reset or a power cycle?


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 01:46*

1. It seems to Alarm when you connect.  LW sends some various gcodes to figure out what the controller type is, Smoothie doesn't like some of these, and voila, it halts.  Just click the #1 option in the LW window to clear and proceed.   If other circumstances, explain context please.

2. Homing should take you to the back left.  If it's going a different way, your motors may be reversed in which case power down and flip the appropriate X or Y cable. 


---
**Bob Buechler** *March 15, 2017 02:04*

1. I can't find a #1 option.

2. When I use the manual jog commands it moves in the correct directions. So what I did was manually move them to where zero actually is (the very start of both axes) and tried the set zero button. That moved the purple dot to zero, which seemed to work as I'd expect. 




---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:06*

This in LW3: [cloud.githubusercontent.com](https://cloud.githubusercontent.com/assets/7695323/20649394/c2328440-b4c7-11e6-9e5f-ac7e3ba6c043.PNG)


---
**Bob Buechler** *March 15, 2017 02:10*

Hmmm... it might be that I need to flip that cable... when I manually jog the laser into the cutting area, LW4 thinks I'm in negative Y.


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:12*

Using a terminal interface (LW3 has this at the bottom, don't know about LW4, can use pronterface or something), send the G28.2 command. Head should move back left to touch off on the limit switches.  If it moves the opposite way, power down and flip the cable.  


---
**Bob Buechler** *March 15, 2017 02:12*

re: clearing alarm, LW4 doesn't seem to throw that window anymore. At least it isn't for me. I found a button in the Jog screen that lets you clear the alarm now though. And that did work.


---
**Bob Buechler** *March 15, 2017 02:16*

G28.2 moves it up and to the left, but the last few mm of movement were very slow. Is that normal?


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:17*

It should hit, back off, and touch off again at a slower speed.  Seems fine to me.  As long as it eventually stops and doesn't grind. (People described the exact same thing as, well it's grinding and not stopping!!!! while in fact it was just doing the touch off at a slower speed again!)




---
**Bob Buechler** *March 15, 2017 02:19*

yeah that seems to be the behavior I'm seeing. Now how to tell LW4 that I'm actually not in negative Y? heh.


---
**Bob Buechler** *March 15, 2017 02:29*

Here's what LW looks like when I manually jog the laser out into the bed:

![images/1ff75bf63f4ac1ad26d87c70f6369475.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1ff75bf63f4ac1ad26d87c70f6369475.png)


---
**Bob Buechler** *March 15, 2017 02:31*

X is correct, by Y is inverted. But only in LW. The directional jog buttons all make the laser head move in their corresponding directions, as you'd expect.


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:32*

Ok, so when you home, the Y value is 200 because it homes to max. Seems like LW4 may not be updating that after homing.  I don't know if an M114 would do it.  Please post this in the LW group, add that screenshot and description, and a link to this thread.  #ProperTroubleshooting :)


---
**Bob Buechler** *March 15, 2017 02:35*

ahh... interesting. When I try to run that loaded job with Y inverted, it tries to move the laser up into what it thinks is positive Y, which smashes it into the Y end stop. 


---
**Bob Buechler** *March 15, 2017 02:38*

Okiedoke! Will do. :) Thank you! 


---
**Bob Buechler** *March 15, 2017 03:33*

Thanks again for your help, Ray. Hopefully these shots help your documentation efforts. 

Here's my before shot with the ribbon cable configuration.

![images/38bb4b76434fecae50f2879568924b2d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/38bb4b76434fecae50f2879568924b2d.jpeg)


---
**Bob Buechler** *March 15, 2017 03:34*

Here's the installed shot, minus the power cable:

![images/7686ffb6914f1e2c3c4f4ccff8e68c16.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7686ffb6914f1e2c3c4f4ccff8e68c16.jpeg)


---
**Bob Buechler** *March 15, 2017 03:34*

I'll try to get a decent shot with all three cables installed.


---
**Kelly S** *March 21, 2017 02:56*

Reads good, do not forget the optional LAN module.  :) 


---
**Ray Kholodovsky (Cohesion3D)** *March 21, 2017 02:57*

**+Kelly S** good call, will try to remember to add a note about it. 


---
**joe mckee** *March 24, 2017 14:54*

Was the laser cable included in the write up ? 




---
**Ray Kholodovsky (Cohesion3D)** *March 24, 2017 16:25*

**+joe mckee** in this new guide it specifies not to hook it up/ use it. 

I still include the cable with the kits just in case, but overall you've heard me say it enough times by this point. :)


---
**Bob Buechler** *March 24, 2017 21:44*

Finally got around to taking a post-install shot. It's pretty hard to get an angle with all the connections visible.

![images/00c4242153f12cd137c4a1b129a8f6e8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/00c4242153f12cd137c4a1b129a8f6e8.jpeg)


---
**Kelly S** *March 26, 2017 12:39*

Did I miss something?  What about the laser cable?  **+Ray Kholodovsky** Is it not required somehow?


---
**Redvers Davies** *March 28, 2017 22:57*

Likewise curious as to the rational behind the lack of PWM cable.... I guess as a new member I've not heard **+Ray Kholodovsky** talk about it enough yet :-)



I'll start plowing the history and hopefully I'll get lucky.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 22:59*

Yeah, arguments have been made that keeping the pot in play and using it to set the ceiling allows for better grayscale output during raster. 

It's in about 50 comments on way earlier threads in the group. 

Please see the new documentation, it mentions to disregard the pwm cable in the bundle and keep the pot in play. 


---
**Kelly S** *March 29, 2017 00:52*

Lol I will have to look but I have a good grasp on all the settings I need to cut items ( I do mostly cuts) but if it helps with greyscale I will have to check it out.  


---
**Bob Buechler** *March 29, 2017 01:21*

**+Kelly S** I'll mostly be doing cuts too, so I'd appreciate any knowledge sharing. :)


---
**Kelly S** *March 29, 2017 01:26*

**+Bob Buechler** currently trying to get LW4 working but cannot seem to connect, so may just stay with LW3 lol.   What information are you seeking?




---
**Bob Buechler** *March 29, 2017 03:48*

just anything you figure out in LW3/4 along the way that helps you make clean cuts. At **+Peter van der Walt**'s recommendation, I abandoned LW3 for LW4 almost immediately (though, to be fair, I didn't own a K40 until LW4 was exiting beta, so that wasn't too difficult on me). 



Oddly though, I didn't have any trouble getting LW4 to work for me. What issues are you encountering? 


---
**Redvers Davies** *March 29, 2017 07:03*

Is the Cohesion board in combination with the tube in the K40 capable of doing pulsing to gain that extra 'bite'?




---
**Russ “Rsty3914” None** *April 11, 2017 03:26*

Is the pwm working, would u still use the Pot to regulate max ?


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 03:29*

**+Russ None** With the pot kept in play, it sets the maximum power aka the current ceiling.  And the % you set in LaserWeb will be a proportion of that. 


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 03:29*

**+Redvers Davies** I'm sorry I missed your inquiry.  Could you clarify what you meant? 


---
**Russ “Rsty3914” None** *April 11, 2017 03:31*

Ok, thanks...just verifying...


---
**Russ “Rsty3914” None** *April 11, 2017 03:36*

Also has anyone closed the loop with a water flow switch too tube ? Can it be processed by C3d ??


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 03:38*

There are designs out there for an arduino with display to monitor your flow, temps, etc.... The C3D does not process these items, it is busy controlling your laser :), it should be a separate board doing the monitoring. If you'd like you can possibly run 1 wire back from the arduino to the C3D to pause or kill the job if a red flag comes up. 


---
**Redvers Davies** *April 11, 2017 05:32*

**+Ray Kholodovsky** If you look at the power output of a Laser it emits most of its energy at the initial activation.  As such, some Laser manufacturers actually pulse their lasers to get a great ERP.



In hindsight, this effect may not exist on the type of CO2 tube that I have and may be limited only to RF lasers as used by epilog et al.


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 05:34*

The board/ smoothie doesn't explicitly do this, not to the ends of "getting an extra kick" out of the tube anyways. There are parameters for minimum power to the tube and trickle power, that's about it. And that's more about keeping the tube activated which is the opposite of what you're suggesting. 

Hope this helps. 


---
**E Caswell** *April 12, 2017 20:41*

**+Ray Kholodovsky** with the digital board machines, what output settings should I need to set it at to get the best results? 60, 70, 80 or 99%? 


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 20:44*

It's dependent.  With the pot I say 10mA .  10mA is a good starting value because that's still enough to cut stuff but it won't drown out the PWM from L when raster engraving.  But if you're doing lighter engraving then you might be able to get deeper grayscales by going down, like to 5mA. 

Anyways, I'd start at 60% and adjust accordingly.  I just explained why "best" will vary depending on what you want to do. 


---
**Dan Kelley** *April 13, 2017 00:02*

Hi there! Just installed my Cohesion3d Mini upgrade package on my ribbon cable / digital display K40, followed these directions to a T, everything boots up and connects to Laserweb 4 properly, and when I run G28.2 It homes in the Y Axis properly, but then disregards the X Endstop and keeps crashing into the Left end of the X Axis (where it should home. I know the end stop is good, as I just used it minutes before installing the new board, but do you have any ideas **+Ray Kholodovsky**? Let me know if there are any specific settings/ photos that might help diagnose this. 



Jogging the x and y axis works as it should though.



Edit: Moved to new post here [https://plus.google.com/112962396083104042777/posts/XvR7ciyPNd6](https://plus.google.com/112962396083104042777/posts/XvR7ciyPNd6)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 00:03*

Hey Dan.  Do me a favor and make a new post in this group with this writing, and I'd like to see pictures of your board wiring please. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/G67nBZcvPnx) &mdash; content and formatting may not be reliable*
