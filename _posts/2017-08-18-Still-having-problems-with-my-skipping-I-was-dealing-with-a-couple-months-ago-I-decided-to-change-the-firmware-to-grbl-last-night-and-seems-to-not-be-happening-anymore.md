---
layout: post
title: "Still having problems with my skipping I was dealing with a couple months ago, I decided to change the firmware to grbl last night and seems to not be happening anymore"
date: August 18, 2017 02:12
category: "FirmWare and Config."
author: "tyler hallberg"
---
Still having problems with my skipping I was dealing with a couple months ago, I decided to change the firmware to grbl last night and seems to not be happening anymore. Only problem is my laser has no adjustment in power. The directions say to connect to mosfet 2.5, in smoothie my PWM wire is 2.4! i unplugged it and moved to 2.5 and nothing. Just all laser all the time. Anyone who has gone it care to shed some light on how they got their wiring hooked up?





**"tyler hallberg"**

---
---
**Ashley M. Kirchner [Norym]** *August 18, 2017 02:40*

Keep that wire on 2.4 (for now) please. Take a look at the pots that are on the stepper drivers. By default, the flat part of the pot should align with the edge of the board. If you are running from the stock LPSU in the machine, you can turn the pot 45 degrees clockwise to increase the current to the stepper. That's as far as you should go before the LPSU starts to cause brownouts. If your stepper motors are running off of a separate 24V PSU, you can safely turn the pots a full 90 degrees clockwise.


---
**tyler hallberg** *August 18, 2017 03:35*

The 2.4 does nothing in grbl as far as I can see. Sorry this post was a bit confusing. Me and Ray ran out of ideas on the skipping problem so I switched to grbl and skipping is gone. Everything has been upgraded from the stepper chips, external psu, stepper motors, and everything else. I am just trying to get the raster to work now. grbl is running laser off or on nothing in between.


---
**Jim Fong** *August 18, 2017 03:40*

See this thread on setting up the laser. 



[plus.google.com - GRBL-LPC is working great, but not firing the laser. Reading tons of reports...](https://plus.google.com/+JohnMillekerJr/posts/9Cy34iFdqEa?iem=4&gpawv=1&hl=en-US)


---
**Ashley M. Kirchner [Norym]** *August 18, 2017 03:47*

Ah, was missing info there. Didn't know you had already gone through debugging with Ray.


---
**tyler hallberg** *August 18, 2017 04:09*

Jim that helped me with getting the laser to fire but nothing to help with the rastering problem


---
**Jim Fong** *August 18, 2017 04:41*

**+tyler hallberg** what is the Laserweb4 PWM MAX S setting at?  It has to match what is grbl-lpc $30 setting 


---
**Ray Kholodovsky (Cohesion3D)** *August 18, 2017 11:05*

Which boards are you on?  Didn't you have one of the originals v2.2 and then I sent you a v2.3?  



Either way best way to proceed is to have the machine's pot connected and the L wire to 2.5-  screw terminal #4 from the bottom left. Then it's strictly a config concern. 



So... start with making sure the L wire is hooked up to 2.5, and put the new firmware and config.txt from dropbox on the card (I am putting you back on smoothie with latest 2.5 config for a moment).



[dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)



Then try G1 lines to check how firing works. 



Once this is working normally, see if you get skipping on this one... 



I can also have you try firmware-cnc-latest from here:

[https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin)



(rename to firmware.bin and put on card then reset). 



And finally if you still have skipping then go to grbl-lpc, but at least you'll have resolved firing via smoothie ^^.



GRBL doesn't use a config.txt, it's all configured via those $## commands, but things like the 2.5 pin for pwm are hardcoded and would need to be recompiled if need be.  



In short: 

If you replaced the pot with the pwm wire for a specific reason and I can't remember, try the latest smoothies ^^.

If you can put the pot back and run just L to 2.5, do so.  



Pics of wiring will probably be necessary to further assist. 



<b>Disclaimer to anyone else reading this, this is super specific stuff with a lot of backstory, do not jump to doing any of this.</b> 


---
**tyler hallberg** *August 19, 2017 01:27*

**+Jim Fong**  both at 1000


---
**Jim Fong** *August 19, 2017 18:00*

**+tyler hallberg** Can you explain in detail what the raster problem you have now.  Trying to figure out what works.....


---
**tyler hallberg** *August 26, 2017 21:00*

The board is not controlling the laser power, its either on or off.


---
**tyler hallberg** *August 26, 2017 21:02*

![images/756d0447d07943e70a2042395da65ecb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/756d0447d07943e70a2042395da65ecb.jpeg)


---
**tyler hallberg** *August 26, 2017 21:04*

This is my PSU for the laser. When I was running smoothie I had the power wire ran to the middle of the 3 white connector. Ive tried putting that at 2.5 for the grbl and nothing. just on or off no shades


---
**tyler hallberg** *October 07, 2017 20:31*

I have since reverted back to smoothie and the updated firmware. engraving shades are spot on once I wired the pot back in. I am still having issued with it shifting mid print and coming out of alignment 


---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 20:34*

Then it has to be mechanical, unless your a4988's are overheating, which is rare but can happen if you're full 90 degrees or more on the pot.  Fan? 


---
**tyler hallberg** *October 07, 2017 21:08*

Ive  changed everything from the motors to the drivers. Ill try running a fan on them to see if they are shutting themselves down.


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/DMEYFQ9Qdgv) &mdash; content and formatting may not be reliable*
