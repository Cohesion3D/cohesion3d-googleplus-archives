---
layout: post
title: "I have just received the Cohesion3D package that includes LightBurn and files on a mini sd"
date: July 03, 2018 16:28
category: "C3D Mini Support"
author: "John Cahall"
---
I have just received the Cohesion3D package that includes LightBurn and files on a mini sd. The sd card is cracked and unreadable. I contacted company and I have downloaded the software  from the dropbox link. The K40 Upgrade Bundle comes with Smoothieware CNC Firmware and a K40 Laser-Specific configuration file on the included MicroSD Card. Here are those files: [https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)

 Do I copy all of the files from there to a new sd card and I'm ready to go? Please inform. Newby thank you!





**"John Cahall"**

---
---
**Joe Alexander** *July 03, 2018 17:19*

copy the files from the folder batch 3 onto the sd card, should only be the firmware.bin and config.txt


---
**John Cahall** *July 03, 2018 17:30*

**+Joe Alexander** Thank you Joe!


---
**Tech Bravo (Tech BravoTN)** *July 03, 2018 18:08*

Go into the batch 3 folder. There will be a [firmware.bin](http://firmware.bin) file and a [config.txt](http://config.txt) file. Download both of those files and put those files on the card (you may want to format the card first. The file system need to be FAT)


---
*Imported from [Google+](https://plus.google.com/106691667969444964893/posts/KYLQ6qh6NZ7) &mdash; content and formatting may not be reliable*
