---
layout: post
title: "C3d Mini  Setup My laser machine has 2 Model numbers 1st 1 is KH40W from vender, 2nd one KH-3020 on machine, need you to check this before I power up, I made setup pictures as I went on"
date: March 08, 2017 03:21
category: "C3D Mini Support"
author: "Allen Russell"
---
C3d Mini – Setup

My laser machine has 2 Model numbers 1st 1 is KH40W from vender, 2nd one KH-3020 on machine, need you to check this before I power up, I made setup pictures as I went on. This is how I did it.







**"Allen Russell"**

---
---
**Allen Russell** *March 08, 2017 03:21*

Tag and mark Power Cable and Remove

![images/883e45af22bb46fb0a6cb2e78eef28a3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/883e45af22bb46fb0a6cb2e78eef28a3.jpeg)


---
**Allen Russell** *March 08, 2017 03:22*

Remove Y AXIS Cable

![images/7637485bc7c8dc090f831816315c7694.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7637485bc7c8dc090f831816315c7694.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 03:22*

I did see you posted pics at the end of the other thread. 

I'd need to see pics of the psu and the wiring going to the C3D to confirm stuff. 

The model numbers don't mean anything. 


---
**Allen Russell** *March 08, 2017 03:23*

Tag and mark Ribbon Cable then Remove

![images/512ce2b44a4af0171cf29b4bc01e1215.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/512ce2b44a4af0171cf29b4bc01e1215.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 03:23*

Ah, I see you are posting pics here. I shall wait :) 


---
**Allen Russell** *March 08, 2017 03:23*

Drill Holes and move Old board back for Backup then add C3D mini in front position then replace panel, ready for cables

![images/0ba811b062bf887c328944e0a95c12bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0ba811b062bf887c328944e0a95c12bd.jpeg)


---
**Allen Russell** *March 08, 2017 03:24*

Connect Y AXIS cable


---
**Allen Russell** *March 08, 2017 03:24*

![images/f1230fae8582a83ff8de89f13d2b3737.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f1230fae8582a83ff8de89f13d2b3737.jpeg)


---
**Allen Russell** *March 08, 2017 03:25*

Add Ribbon Cable

![images/074be83164cb8aae35b6051d0200d7dc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/074be83164cb8aae35b6051d0200d7dc.jpeg)


---
**Allen Russell** *March 08, 2017 03:25*

Add Power Cable as marked

![images/fd72396b0628af3315be69be5d7402ae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd72396b0628af3315be69be5d7402ae.jpeg)


---
**Allen Russell** *March 08, 2017 03:26*

Finished

![images/3768d223bcd229adb77899aebf38e417.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3768d223bcd229adb77899aebf38e417.jpeg)


---
**Allen Russell** *March 08, 2017 03:27*

So is this ready to turn on?


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 03:28*

Yeah. I'm good with the power cable orientation.  


---
**Steven Whitecoff** *March 08, 2017 03:31*

**+Ray Kholodovsky** and **+Allen Russell**..cabling colors etc match my machine....suggest you look at my threadwhere Ray got me going., "Blue KH-3-20 Digital Panel" below.

At least it will get your cables connected, so far I've not seen any pictures of our wiring till your post.

[Cohesion3D](https://plus.google.com/communities/116261877707124667493/stream/f3cf6d8d-47c7-4c04-93a4-0653f2893279)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 03:32*

Steven I think you just posted the general group link not a specific thread. 


---
**Steven Whitecoff** *March 08, 2017 03:35*

yep posted a link  to my post, thats what I got...edit it to the name... you guys are too fast






---
**Steven Whitecoff** *March 08, 2017 03:44*

![images/47f5a90823c3ef1862d92bfa5e064632.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/47f5a90823c3ef1862d92bfa5e064632.jpeg)


---
**Steven Whitecoff** *March 08, 2017 03:44*

![images/10d311201865ff263831f2dc3e861af8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/10d311201865ff263831f2dc3e861af8.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 03:47*

You may notice that your y motor connectors are facing opposite ways. One may go the wrong way. Only one way to find out... Home with a finger on the power button. It should go to the back. 


---
**Steven Whitecoff** *March 08, 2017 15:20*

If I understand you, having the Y cable will cause it to not home or have a different home? Both Nano and Mini are homing top left as connected. If I flip the connector it will have be partiallly under the driver and prevent full seating of the driver. Just went to verify its still homing(change the home parameter since yesterday according to your guide but I'm sure I cut etc after that), but Laserweb is down right now.


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 15:38*

If it's homing top left then all is good. 


---
**Steven Whitecoff** *March 09, 2017 03:10*

**+Ray Kholodovsky**, I checked and I am wrong. With G28 its bottom right AND sitting over a cover plate. I reversed the cable and that did not change it and +Y jogs go down which means belt slipping at the stop. So I returned the cable to original orientation and used $H, now its all happy again. Different wiring on my type of machine or something else? 


---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 03:12*

G28.2 not G28


---
**Steven Whitecoff** *March 09, 2017 03:36*

How odd, its working with G28.2, the odd part is when I typed G, it showed me G28.2 as an previous entry. So it would seem I had it right yesterday. I'll keep an eye on it but think its going to be good. 


---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 03:43*

IN CNC FIRMWARE: 

G28 moves to what the machine thinks is 0,0 

G28.2 actually goes to hit the limit switches 


---
**Georg Petritsch** *April 09, 2017 17:14*

Hi! 

I have also exact this machine, and the problem is the pwm cable. is there any workaround to replace the pot with the pwm signal from the board? because without that i guess there is no way to get a nice engraving result from laserweb.


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 17:16*

**+Georg Petritsch** I would like to clarify. Is the problem the pot? 

Yes we can replace the pot with a pwm cable (I include the cable for situations like this) but you might want to try replacing the pot first. 


---
**Georg Petritsch** *April 09, 2017 17:28*

I am not sure if the pot is the problem, i just thought i could remove it and use the pwm cable instead to control the laser power from software. 

is there any shematic available where to connect the cable right?


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 17:35*

I can direct you, sure. But I think first I think we should get pwm working. Please start a new thread here with the symptoms described and we will assist. 


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/TfiJPuYAdAn) &mdash; content and formatting may not be reliable*
