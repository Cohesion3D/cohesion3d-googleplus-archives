---
layout: post
title: "I bought a cohesion 3d laser board with the LCD upgrade in December"
date: January 26, 2019 21:56
category: "K40 and other Lasers"
author: "Enrique Villa"
---
I bought a cohesion 3d laser board with the LCD upgrade in December. I got to installing it today and my board is displays like the image attached with a line across the display. Is this damaged or is my wiring lose? 



![images/47c7cafca6c506cb06537a988377d837.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/47c7cafca6c506cb06537a988377d837.jpeg)
![images/c56804ed0b703926eeb7703749396e3f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c56804ed0b703926eeb7703749396e3f.jpeg)

**"Enrique Villa"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2019 23:58*

I would need you to check if your wiring is loose. 



Please try to flip the cables around so both are going down on the screen, as shown in the guide for the Mini here: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions. : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542)



Let me know if the issues persists. 


---
**Enrique Villa** *January 28, 2019 06:41*

Hi Ray, I flipped the cables around and plugged everything and the issue still persists. Seems like the board is damaged. 


---
**Ray Kholodovsky (Cohesion3D)** *January 28, 2019 16:54*

 **+Enrique Villa** fill out the contact form with your order # and shipping address, reference this thread, and I’ll send you a replacement GLCD screen. 


---
**Enrique Villa** *February 01, 2019 18:45*

**+Ray Kholodovsky** Hi Ray, I submitted the form but I have not received any confirmation email. Was there any error on my part that on the form that I need to correct?



Thank you,

Enrique. 




---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2019 15:11*

**+Enrique Villa** I have not gotten any email from you, can you try the form again please or email directly to info at cohesion3d dot com


---
**Enrique Villa** *February 05, 2019 22:52*

**+Ray Kholodovsky** Hi Ray, I got a confirmation that I sent the contact us form to my email. I am about to place an order for an external stepper since I want to connect a z table. Can we include the replacement LCD in the shipping for this order? I would also like to purchase the external stepper adapter to connect to the laser board but can not locate it in the store, do you have a link for this? Thank you. 


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2019 22:58*

I shipped your replacement screen today, was going to email you tonight.  It is already in the mail. 


---
**Enrique Villa** *February 05, 2019 23:01*

**+Ray Kholodovsky** Sorry about that, I just decided to add the table earlier today. I figured it could be lumped in the cost of shipping the new order. Does the website carry the external stepper adapter to connect to the laser board? I am going to order the driver today. 


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2019 23:04*

What are you sorry for? :) 



What z table exactly?  G+ is going away soon so please make a post in the [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](http://forum.cohesion3d.com) with the details of what you want to do and we will discuss there - do not order anything yet.


---
**Enrique Villa** *February 05, 2019 23:09*

I will seek information on the lightobject ztable. I saw a post in the documents section. But I am looking at buying the cable in the picture I am attaching. 

![images/b5c4f4d291e5f5f69137fc2a0b464d6f.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b5c4f4d291e5f5f69137fc2a0b464d6f.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2019 23:15*

For the LightObject Z Table you can just use the driver that is built in to the LaserBoard.  It's covered on the forum :) 


---
*Imported from [Google+](https://plus.google.com/117315762947831355449/posts/GxxFCxTJypS) &mdash; content and formatting may not be reliable*
