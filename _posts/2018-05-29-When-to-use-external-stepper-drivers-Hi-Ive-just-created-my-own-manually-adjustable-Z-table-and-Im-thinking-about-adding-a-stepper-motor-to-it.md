---
layout: post
title: "When to use external stepper drivers? Hi, I've just created my own manually adjustable Z-table and I'm thinking about adding a stepper motor to it"
date: May 29, 2018 16:02
category: "K40 and other Lasers"
author: "Tako Schotanus"
---
<b>When to use external stepper drivers?</b>



Hi, I've just created my own manually adjustable Z-table and I'm thinking about adding a stepper motor to it. I still have some extruder and x/y motors from an old 3D printer that I could use. But I see that the Cohension website specifically mentions the external stepper drivers for Z-tables, so perhaps what I want to do is outside the specs of the normal drivers? How can I figure that out?





**"Tako Schotanus"**

---
---
**Tech Bravo (Tech BravoTN)** *May 29, 2018 16:35*

the A4988 Stepper Motor Drivers are supposed to handle 2A per pole but many have found that they are a bit underpowered for z tables and rotaries. External, self powered drivers are far better since z axis mechanisms typically require quite a bit of current. 


---
**Jim Fong** *May 29, 2018 16:40*

The external stepper driver was recommended because the stepper motor used in most Commercial Z tables required higher current than Pololu A4988, drv8825 etc drivers can reliably run. 



If the stepper motor you have can reliably move your custom table with a Pololu plug in driver, then use it.  


---
**Tako Schotanus** *May 29, 2018 19:53*

Ok, so I can just try it without any problems and see if things work? Thanks!


---
*Imported from [Google+](https://plus.google.com/+TakoSchotanus/posts/TQXP9uVNpqu) &mdash; content and formatting may not be reliable*
