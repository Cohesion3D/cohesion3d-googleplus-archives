---
layout: post
title: "Been a long time between update's, some might remember my board had a lot of glue on it and all seems well it is powering on"
date: May 24, 2018 08:38
category: "C3D Remix Support"
author: "Robert Hubeek"
---
Been a long time between update's, some might remember my board had a lot of glue on it and all seems well it is powering on. how ever my screen is not working. What is the best way to go about this? i read in one place to cut pins out but would much rather do something firmware related to fix this 



Cohesion3D remix with genuine GLCD and short ribbon cable

![images/b16d5a4d3b13e36189c5b4ac3a79bf23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b16d5a4d3b13e36189c5b4ac3a79bf23.jpeg)



**"Robert Hubeek"**

---
---
**Panayiotis Savva** *May 24, 2018 09:49*

Install Marlin 2.0


---
**Robert Hubeek** *May 24, 2018 11:11*

**+Panayiotis Savva** any reason why I would do that on a smoothieware based board?  Noob here so be gentle lol


---
**Panayiotis Savva** *May 24, 2018 11:18*

 The RRD GLCD works on the LPC on Marlin 2.0 :)


---
**Ray Kholodovsky (Cohesion3D)** *May 24, 2018 13:36*

Is the glcd (panel) enabled in config? 


---
**Robert Hubeek** *May 24, 2018 23:45*

Here is a picture, it is enabled 

![images/77436af2fd18b72a07bfc59267a27375.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/77436af2fd18b72a07bfc59267a27375.png)


---
**George Allen** *May 25, 2018 13:40*

What kind of glue do you use on your PCB boards? 


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 13:45*

**+Robert Hubeek** can you show pictures of all the wiring please? 


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 13:47*

**+George Allen** no glue. He got one of the very first remix boards (#0003 is I recall correctly). That first batch had some flux on the bottom of the boards from poor/ improper cleaning which manifested as a dry white residue. 


---
**George Allen** *May 25, 2018 13:51*

Ah


---
**Robert Hubeek** *May 25, 2018 23:36*

**+Ray Kholodovsky** 

![images/925d881e2f9d5f3f1fc0d30f67a0985c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/925d881e2f9d5f3f1fc0d30f67a0985c.jpeg)


---
**Robert Hubeek** *May 25, 2018 23:36*

![images/fdcaea2db304a7c6a65174977c310000.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fdcaea2db304a7c6a65174977c310000.jpeg)


---
**Robert Hubeek** *May 25, 2018 23:37*

Also tried this setup.  [3dprinterchat.com - Smoothiefy your 3D Printer &#x7c; 3D Printer Chat](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/) 


---
**Robert Hubeek** *May 25, 2018 23:38*

![images/972ac559180a5a803b707d1495eaaabd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/972ac559180a5a803b707d1495eaaabd.jpeg)


---
**Robert Hubeek** *May 25, 2018 23:44*

I tried muiltiple combinations of the above with the three cable's as i read some people with the GLCD had to put there ones in upside down and so on. but i still get the same blank screen




---
**George Allen** *May 25, 2018 23:52*

**+Robert Hubeek** I’ve been where you are. I never got mine exactly like I wanted it, but I did get the LCD to eventually come up. You have to fix it in marlin. Where did you purchase your LCD and what model do you have?


---
**George Allen** *May 25, 2018 23:56*

I may not be able to help, as I was using a RAMBo board and tried using another type of Chinese board maker something (proprietary board).


---
**George Allen** *May 26, 2018 00:03*

I also got the feeling that sometimes. When making the ribbon cables, some manufacturers may accidentally either get the 2 cables mixed up, or clamp them in upside down. It’s easy to do as I began making my own, you can easily get them backwards, if you aren’t careful.


---
**George Allen** *May 26, 2018 00:07*

I had to change mine on Marlin using Arduino. If you aren’t familiar with that, it may be challenging.


---
**Robert Hubeek** *May 26, 2018 03:38*

I want to stick with smoothieware and cohesion remix. Not go to Arduino and marlin. 


---
**George Allen** *May 26, 2018 04:00*

**+Robert Hubeek** Yes, I would stick with those as well. Maybe they are running different firmware. I would just listen to Ray. I didn’t mean to confuse you.


---
**Cris Hawkins** *May 26, 2018 18:47*

**+Robert Hubeek** I remember reading about an issue with some GLCD board's contrast adjustment. There should be a trimmer pot in the lower left hand corner on the front of the PC board. Some of them are apparently missing or installed incorrectly. If the contrast is faulty, the GLCD may be operating properly but you cannot see it. I suggest you search for contrast issues (I hate G+ and haven't figured how to search quickly).


---
**Robert Hubeek** *May 27, 2018 00:16*

![images/982315a5ff04680d09b503789e134cd3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/982315a5ff04680d09b503789e134cd3.jpeg)


---
**Robert Hubeek** *May 27, 2018 11:14*

Link to firmware file 

[technical.ciscokids.com.au - www.technical.ciscokids.com.au/upload/FIRMWARE.CUR](http://www.technical.ciscokids.com.au/upload/FIRMWARE.CUR)


---
**Robert Hubeek** *May 27, 2018 11:14*

link to config

[technical.ciscokids.com.au - www.technical.ciscokids.com.au/upload/config.txt](http://www.technical.ciscokids.com.au/upload/config.txt)


---
**Robert Hubeek** *May 30, 2018 07:28*

is there anything silly, like i must have everything connected before it works correctly? 


---
**Robert Hubeek** *May 30, 2018 07:30*

**+Dushyant Ahuja**, how did you go? considering im also cobblebot with cohesion remix like yourself 


---
**Robert Hubeek** *June 04, 2018 09:43*

Has anyone got the screen working on there Remix and ok to share the config file? 


---
**Robert Hubeek** *June 04, 2018 11:44*

**+Ray Kholodovsky** can you confirm the pinouts are correct for this wiring diagram of the remix? reason i ask is i cant seem to find anything that matches the item. 

![images/658f6c6b6293b35fb981b4350d51e733.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/658f6c6b6293b35fb981b4350d51e733.png)


---
**Robert Hubeek** *July 03, 2018 10:41*

![images/ec92a82b3fe8baa3a675c5f316448d1a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec92a82b3fe8baa3a675c5f316448d1a.jpeg)


---
**Robert Hubeek** *July 03, 2018 10:43*

Got it working. 


---
*Imported from [Google+](https://plus.google.com/+RobertHubeek/posts/7NP1K761o1T) &mdash; content and formatting may not be reliable*
