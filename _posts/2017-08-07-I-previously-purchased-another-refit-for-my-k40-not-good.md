---
layout: post
title: "I previously purchased another refit for my k40,,, not good"
date: August 07, 2017 13:09
category: "K40 and other Lasers"
author: "walter kennison"
---
I previously purchased another refit for my k40,,, not good.

it required the removal of the power connector and the endstop connector.



I need to replace these,, can you recommend part numbers or the proper terminology for these connectors.





**"walter kennison"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 07, 2017 13:29*

You should be able to hook up power directly to screw terminals (24v, gnd, and L). You should hook up endstop wiring directly to X and Y endstop ports using Molex KK plugs (3 pin). 


---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 00:20*

Also what was the previous retrofit you had? 


---
**walter kennison** *August 09, 2017 00:16*

was an ebay purchase,,, will try to track or will take pic when I'm back in the shop


---
**walter kennison** *August 11, 2017 12:53*

![images/bdfd105fc3c7a3187b19dbabe2ceddc8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bdfd105fc3c7a3187b19dbabe2ceddc8.jpeg)


---
*Imported from [Google+](https://plus.google.com/100623545956840271503/posts/RUYhCc91xsy) &mdash; content and formatting may not be reliable*
