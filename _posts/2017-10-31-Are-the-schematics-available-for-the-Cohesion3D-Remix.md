---
layout: post
title: "Are the schematics available for the Cohesion3D Remix?"
date: October 31, 2017 18:15
category: "C3D Remix Support"
author: "Chris Kelley"
---
Are the schematics available for the Cohesion3D Remix?



I have one that is being used to run an OpenPNP pick-and-place machine. It worked fine several months ago (the last time I had a chance to work on it), but now it refuses to boot up or be recognized by any computer.



With nothing but the USB cable connected, all that happens is that the 3.3V regulator gets hot and the LPC1769 and Schottky diode next to the USB jack get warm. After several seconds, the VMOT LED will begin to very dimly glow. No other LEDs light.



With only 12V connected to the main power in, the 3.3V regulator gets really hot really fast, as does the LPC1769. The VMOT LED goes on fully, immediately. No other LEDs light.



I could do more troubleshooting if I could look at the schematic.





**"Chris Kelley"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 18:21*

[dropbox.com - C3D ReMix Design Files](https://www.dropbox.com/sh/sp9kfoxc4ihlqrw/AAApjO8Mn0raxKPxMYFKgFWWa?dl=0)



Do you have some pics of the wiring to the board (if you haven't disconnected it all yet) - and the board in its present state?


---
**Chris Kelley** *October 31, 2017 19:19*

Thanks for the response (and schematics), Ray.



It is mostly disconnected, but I'll add a picture anyway.



The multi-colored ribbon on the right takes the step/direction signals to external motor drives. The ribbon on the bottom left is for the limit switches and E-stop signal. For testing, the other ends of both cables are disconnected. Not shown/connected are the 24V wires to the FETs that are used to actuate air solenoids.



I suspect a power issue on the board. With 12V supplied to the board, the output of the 3.3V regulator reads at about 1.9V, while on USB power, it reads 1.3V. So either the regulator has failed or something else is drawing a lot of current and dropping the voltage.

![images/1858bb818266f2c306ffe94cc0a4bc96.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1858bb818266f2c306ffe94cc0a4bc96.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 19:36*

Simplest answer is that either the AMS3.3-1117 has fried, or the LPC1769 has. I've seen it a few times on the Mini board (much larger sample size) where the LPC has developed a short internally causing a pin to connect 3.3v to Gnd. Upon dissection, the AMS was still ok, it was auto-shutting off. But if yours is getting hot, 



Please check for continuity between the Vin, 5v, and 3.3v each to Gnd.

Then please disconnect everything from the board and try again. 



Also, I noticed that someone else from your company has ordered a new ReMix today.  Usually I would offer to get the board back and either fix it or replace it.  Is this a time sensitive application?  How can I best get you up and running again?






---
**Chris Kelley** *October 31, 2017 20:07*

Ok, Vin to Gnd: ~4M Ohms (power supply disconnected)

5v (pin 2 of 5v reg) to Gnd: ~9K Ohms

3.3v (pin 2 of 3.3v reg) to Gnd: ~1 Ohm



Everything disconnected: No change



Yeah, my boss went ahead an ordered another board. (I'm not exactly sure how "time sensitive" it is, sooner is better of course, but I'm just the guy who was told to make the the pick-and-place work.) 



I think we have some ZLDO1117 3.3v regs on-hand, so unless those readings prove that it's the LPC that died, I can swap out the reg in the morning.


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2017 20:11*

Easiest way to find out will be to remove the AMS and check continuity between 3.3v and Gnd again.  With everything else disconnected from the board. 

I'll get the new board out right away. 


---
**Chris Kelley** *November 01, 2017 11:07*

Removed the AMS and 3.3v to Gnd was still ~1 Ohm, so I guess it's the LPC. 



I'll ask the boss-man what he wants to do with this board. We might wan't to send it back for repair once the new one gets here. It's a neat board, I wouldn't mind having a spare around to play with.


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 18:28*

I included a 2nd board in your shipment to replace this one. 


---
*Imported from [Google+](https://plus.google.com/102018072928928614248/posts/QoMaKcys2Rp) &mdash; content and formatting may not be reliable*
