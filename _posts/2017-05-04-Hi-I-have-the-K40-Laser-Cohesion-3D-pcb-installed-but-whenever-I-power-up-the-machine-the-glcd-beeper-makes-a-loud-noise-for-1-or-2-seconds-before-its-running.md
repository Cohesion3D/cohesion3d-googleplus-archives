---
layout: post
title: "Hi, I have the K40 Laser + Cohesion 3D pcb installed but whenever I power up the machine the glcd beeper makes a loud noise for 1 or 2 seconds before it's running"
date: May 04, 2017 14:56
category: "C3D Mini Support"
author: "Andy Knuts"
---
Hi,

I have the K40 Laser + Cohesion 3D pcb installed but whenever I power up the machine the glcd beeper makes a loud noise for 1 or 2 seconds before it's running. Also, whenever I move the laser head with my hands, when the machine is powered off, the glcd lights up and the beeper makes that annoying noise too..



Is there a way to stop this? (besides removing the beeper :p)





**"Andy Knuts"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 14:59*

Yes, the buzzer is known to beep for a few seconds while the board is booting. There is a way to disable the buzzer entirely by cutting a trace at the bottom of the C3D board but seeing as how people have destroyed boards by improperly doing this I do not recommend it. 



Yes, when you move the head the motors act as generators and backfeed power to the board. Good way to burn a stepper driver. Solution: don't do that. 


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 17:09*

**+Alex Skoruppa** it is specific to the Mini and the pin choices.  I used the Viki header that Panucatt uses on the Mini.  The buzzer pin available for the RRD GLCD is 0.27 which is an i2c pin - open drain only - and with pullup.  So by default it is high and takes until smoothie boots (I estimate 4 seconds) before it goes off. 



On mini the buzz pin should be set as 0.27o


---
**Joe Alexander** *May 04, 2017 17:19*

I put a drop of glue on my piezo buzzer instead of cutting leads, it muffles it quite well(about 10% of the original volume) but I bow to rays experience here as it is his board.


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 17:29*

And this is why we like having Joe around. Exactly the right amount of insight as a "theoretical C3D user". Keep it up! :)


---
**Andy Knuts** *May 05, 2017 07:50*

Thanks ;)


---
**Andy Shilling** *May 08, 2017 22:09*

**+Ray Kholodovsky** I've just seen this post and want to know if your first comment was aimed at me (GRRRR) lol.



I'm pleased to say I have run a few jobs tonight with the repaired board and everything worked as it should and I had no halts what so ever.  I'm a very happy man now it seem this has been sorted, I've even managed to get LW4 onto a Pi and leave that plugged in as a server to the K40. Whoop Whoop look at me go.


---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 22:13*

Glad to hear it! 


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/4ApcMfTWU9C) &mdash; content and formatting may not be reliable*
