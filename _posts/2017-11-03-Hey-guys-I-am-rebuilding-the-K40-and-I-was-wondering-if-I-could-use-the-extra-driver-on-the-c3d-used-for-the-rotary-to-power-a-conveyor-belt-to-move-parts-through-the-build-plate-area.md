---
layout: post
title: "Hey guys, I am rebuilding the K40 and I was wondering if I could use the extra driver on the c3d used for the rotary to power a conveyor belt to move parts through the build plate area?"
date: November 03, 2017 01:43
category: "K40 and other Lasers"
author: "Abe Fouhy"
---
Hey guys,



I am rebuilding the K40 and I was wondering if I could use the extra driver on the c3d used for the rotary to power a conveyor belt to move parts through the build plate area? I am thinking about cutting a door in the back and front so I can etch oars or driftwood and then have the rotary drivers move it back in forth into the xy area. Is this possible?





**"Abe Fouhy"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 03, 2017 01:45*

In theory, perhaps. 



You would just use a gCode to tell it to move at the end of the job. 


---
**Abe Fouhy** *November 03, 2017 02:33*

**+Ray Kholodovsky** That's what I was thinking too, that is super cool! I will see how that mod progresses and I'll give you and update!


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/iA6zSZCof1Z) &mdash; content and formatting may not be reliable*
