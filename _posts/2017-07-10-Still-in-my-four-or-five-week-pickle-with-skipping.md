---
layout: post
title: "Still in my four or five week pickle with skipping"
date: July 10, 2017 22:24
category: "C3D Mini Support"
author: "John Milleker Jr."
---
Still in my four or five week pickle with skipping. **+Ray Kholodovsky** has been a big help and I think has exhausted every possibility so far for me. He had asked me to make a new post with my problem in hopes of someone else maybe having some insight on this, maybe trying to run it, etc.. Maybe seeing something wrong in my Laserweb settings or the gcode. Really grasping at straws now and not sure where to turn to.



Story thus far. Have had two Cohesion3D Mini boards. Tried different steppers, pot settings from 0 to 30, 45 90 and now I'm at 100°. Installed a 24v 6a external power supply (and also used the one in the machine). Plugged it in a different breaker in my house from the ventilation/air/water fans/pumps. Put a fan directly on the Cohesion board. Hell, put a heat sink on the X stepper driver. (I would have switched out the driver if I could get the damn wire bundle unplugged and if the damn gear wasn't installed without a screw). Have tightened and loosened my belt. Run from the PC and straight from the SD Card. No love. I thought that I was getting somewhere with the pots, every time I seemed to change them I would start going left or right. Now at 100 degrees I'm getting left and right. Small steps and large.



At the end of my rope, any help greatly appreciated. Right now it's got to be in the 24v external power supply, to the Cohesion3D Mini board or the wires and steppers. I've taken nearly everything out of the loop. So far what's common is the motor, wires, laserweb. I've even tried different files and versions of laserweb. I have instructions that **+Claudio Prezzi** sent me on using a meter to fine tune the pots on the drivers but I haven't figured out how to calculate the voltage I need yet. Not that I know what these Chinese steppers would want.



Any help greatly, greatly appreciated!



The Zip File: [http://johnmilleker.com/upload/columbia.zip](http://johnmilleker.com/upload/columbia.zip)



![images/003bb81635fb04f07f257d3bb299c7de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/003bb81635fb04f07f257d3bb299c7de.jpeg)
![images/2d21f30ddb617a8e287245a0bc59cb04.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d21f30ddb617a8e287245a0bc59cb04.jpeg)
![images/b6f7f15a5087320fc873cfa3f154e170.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6f7f15a5087320fc873cfa3f154e170.jpeg)
![images/36858ac4c67978e5e2b82d7a6b7e3302.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/36858ac4c67978e5e2b82d7a6b7e3302.jpeg)
![images/3b70641ec3e3616d2c0564455ce19ed0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b70641ec3e3616d2c0564455ce19ed0.jpeg)

**"John Milleker Jr."**

---
---
**Joe Alexander** *July 10, 2017 22:37*

question: have you attempted the file with both standard firmware for smoothie and grbl-lpc? I've heard that grbl can be more responsive with rasters which may remedy this issue. And I'm sorry that your having soo many issues, it can be very disheartening.


---
**John Milleker Jr.** *July 10, 2017 22:56*

I have not **+Joe Alexander**, with my old board that stopped working I never could get any of the four firmwares working. Homing didn't work on one of the axes. 



Could have been the old board, could have been me. I'll try it, but I hate posting when I get in a bind. It's been so much lately.




---
**Alex Krause** *July 10, 2017 22:58*

How fast are you engraving at... And are you running with air assist with a coiled hose?


---
**Griffin Paquette** *July 10, 2017 23:06*

Perhaps drop your jerk and accel?


---
**John Milleker Jr.** *July 11, 2017 00:23*

**+Alex Krause** - Engraving at 250mm/s, I see others using the Cohesion3DMini to get up to 400mm/s so figure this conservative value should be fine. Plus, usually after I swear at the machine and threaten hitting it with a hammer I can get five or six great jobs on paper. Then I put in my material and it skips. 



I am running air assist but not a coiled hose, regular tube with a drag chain. At no point do I think my drag chain is having an issue but I see where you're getting at. I may have to disconnect it and run without air and see, though I have never heard it bind up and it moves freely all around the bed. Thanks for your thoughts!



**+Griffin Paquette** - Thanks! I've taken the default acceleration of 3000 down to 1000, even the jerk from the default of 0.05 to 0.01. Didn't seem to make a difference in the frequency or severity of the issue. Do you think I took them down far enough to test?


---
**Alex Krause** *July 11, 2017 00:26*

Also... Are you using 16x or 32x microstepping?


---
**John Milleker Jr.** *July 11, 2017 00:32*

**+Alex Krause** - I'm not entirely sure. Where would I find that setting? I've checked my config.txt and not seeing any value reference it. I can only assume I'm running whatever the default is.


---
**Alex Krause** *July 11, 2017 00:35*

With smoothie the microstamping isn't a firmware setting it's Hardware in the stepper drivers you're using


---
**John Milleker Jr.** *July 11, 2017 00:40*

**+Alex Krause** - Ahhh gotcha, thanks for your patience. I'm not really sure what most of this stuff is set to, why or how. I'm using the drivers that Ray packages with the Cohesion3DMini board, A4988. I just googled the driver and it seems (I may be and probably am wrong) that depending on how the board they're plugged in it could be any variety of microsteps from 1x to 16x. I have no adjustments on them anyway except for the pot.




---
**Don Kleinschnitz Jr.** *July 11, 2017 00:43*

Does it do this in the same place on the image every time?

Does it only do this on this file/image?

I assume the material is affixed to the table...had to ask!






---
**John Milleker Jr.** *July 11, 2017 00:48*

Thanks **+Don Kleinschnitz** - Not in the same exact place every time, close but once in a blue moon it'll skip in a different spot, skip the other way or even skip a few times up the image. My record is six good engraves between bad engraves.This is the forth or fifth image it's done it on in the last few weeks.



Making sure the material isn't moving is a good point, I did check that! Even though I've learned to waste copy paper on my tests, I usually run these on slate. And it happens on those too.


---
**Griffin Paquette** *July 11, 2017 01:03*

Those should be plenty. Is it just with this specific image? Sometime some images/files create a specific resonance that causes steppers to skip. I've had it happen with 3D prints from time to time. 


---
**Don Kleinschnitz Jr.** *July 11, 2017 01:31*

**+John Milleker Jr.**  tough one....

I think I would put a DVM on the 24vdc and look for a droop, might be hard to catch though.  mmmm.


---
**Douglas Pearless** *July 11, 2017 03:53*

Two things to check, how hot are the stepper motors getting, likewise the stepper chips themselves. The stepper chips could be going into thermal shutdown or clipping 


---
**Andy Shilling** *July 11, 2017 10:35*

I know it doesn't sound great but have you tried running the image at 90°, I've had issues with big files and rotating has allowed me to run some of them. Absolutely no idea why this has worked by the way.


---
**John Milleker Jr.** *July 11, 2017 14:04*

**+Douglas Pearless** - Thanks for the thoughts. After a job the stepper heat sinks get to the point where I can put my finger on them for a ten count before it gets too hot and I need to remove my finger. The body of the stepper gets very warm, but not to the point I can't keep my finger on it. The heat is consistent no matter if I have a failed job or six in a row trying to get it to fail.



With that said however, I've installed a Stepper heat sink with thermal paste and it seems cooler, I've also installed a fan on the Cohesion3DMini board to the point where after several jobs, the stepper sinks don't even feel warm to the touch. Neither seemed to help and actually, with more cooling I was getting more failed tests. Coincidence, but what that tells me is that cooling didn't help. I've kept the cooling installed anyway because it can't hurt.



**+Andy Shilling** - I've send some landscape oriented jobs that have failed far up top. Are you running the stock Cohesion3DMini and Smoothie firmware? What are you considering big files and are we talking raster engraves? Were you getting the same issues with skipping on the X Axis? My file sizes that have failed have been as low as 6MB, which is a simple 150mmx100mm raster at 300DPI.


---
**Don Kleinschnitz Jr.** *July 11, 2017 15:14*

Seems to me that the problem is at the stepper driver or mechanical interface. 



<b>Logic</b>

Since the image shifts position (strangely to the left) and then stays there hints to me that the software did not create the error. Rather the stepper did not go where it was told and the software is out of sync with the motors actual position. Steppers are open loop so the software does not know where the motor actually is. It assumes it went where it was told.



<b>Trying to read what the image is telling us</b> 

...The image always shifts to the left and then slightly corrects to the right but never fully corrects once it fails.



...The slight correction to the right may be a clue. I doubt this is software, more likely an error in the drive/mechanics that returned  closer to its previous position. 



...The error occurs exactly on a scan boundary, that is it does not miss position in the middle of a scan. Clue, mostly happens in change of direction. What is special about change in direction: stress on belts and pulleys, end-stop checking?



...The Error is intermittent and not image or machine setting specific.



<b>Troubleshooting</b>

.....Look for things that can get the software and mechanical out of sync....



<b>Mechanical resistance that keeps the gantry from properly moving</b>

... a loose or slipping pulley

... a damaged belt

... an intermittent mechanical resistance

... an intermittent motor mount



<b>Not enough drive to the motor</b>

... loose motor connection

... marginal power supply capacity

... marginal power wiring size

... intermittently faulty stepper driver or motor



<b>An intermittent or noisy end-stop</b> 

... the switch is telling the software it is further to the left than it should be. This assumes that the software looks for the x end-stop on  each scan for the left edge. I don't know if it does? 

... intermittent end-stop wiring 

... noisy end-stop sensor

... faulty end-stop opto-coupler



<b>Things to try</b>

... Create a Gcode file that stresses the electro-mechanical subsystem in hopes that it will exacerbate and therefore expose the problem and a repeatable set of conditions. 

... A set of gcodes that marks at the beginning of the scan and then accelerates to the end of the scan where it marks again and then reverses direction to repeat another scan etc etc. 

... If you use a gcode file you can change the speed of the scan to see if you can find a place where it fails repeatedly 

... Put a DVM on the 24V supply looking for droop. 

... This is ugly: pull out the gantry assy and inspect all belts, pulleys, set screws and end_stop+motor connections



It would be helpful if you could post a set of failing images that clearly show both vertical edges and annotate them with your settings.




---
**Andy Shilling** *July 11, 2017 15:26*

+John Milleker JJr. I think mine was a similar size but I'll try and find it when I'm home. Yes my setup is C3D with stock firmware on.



My step out wasn't as big as yours but running it skirt and in landscape worked for me. It was as though running it that Ray it had more time to process the g- code which doesn't make any sense as there could be more variants running it in landscape.



**+Don Kleinschnitz**​ is there a code that couldbe used that we place in random positions to make the laser home whilst running a large job? Ie regaining it's true location.


---
**Don Kleinschnitz Jr.** *July 11, 2017 15:41*

**+Andy Shilling** If I understand your questions intent I am not convinced that these problems are caused by faulty code and therefore needs a code workaround. Changing the firmware is not for the faint of heart...



There are many users running large images successfully. That said I have learned that in K40 adventures.... never say never!



Electro-mechanical problems can look like software problems especially when they <b>happen</b> to excite resonances. Changing the driving image can also look like a solution when it "happens" to avoid that resonance.



<b>Happens</b> = <i>Murphys law</i> 



Its best to create a simple-as-possible test that stresses the subsystem and that can give us better insights. 



For a long time I have had a To-do to create simple Gcode tests that stresses the K40 electro-optical-mechanical subsystems as a troubleshooting tool. These tools should send simple Gcode source tests from a "Gcode" sender to help eliminate some potential CAD/CAM software bugs. 



Ya! another item on my "some day" .... to do list :(.


---
**Andy Shilling** *July 11, 2017 15:50*

**+Don Kleinschnitz**​ No I'm not saying a problem within the code as this would show up as a to bigger move but if within the code we put something like g28.2 would that not help if the stepper was stepping to far every so often or an endstop being weird.



I'm thinking not a full homing but maybe a split second 0,0 reset if at all possible. I understand the error isn't on the same point everytime but maybe doing this before every 100th part of the g-code could help keep things on track.



Does that make more sense?


---
**John Milleker Jr.** *July 11, 2017 16:06*

**+Don Kleinschnitz** - "Changing the firmware is not for the faint of heart...", no truer words have been spoken!



**+Andy Shilling** - Another thing I've thought about, which would let things cool down or maybe even catch up, whatever the issue, would be to insert pauses. With that said, I don't know what pauses Smoothie's firmware would allow.



On a side note, now that many have helped me with the right firmware.bin to download and the settings in the config and on laserweb, I have grbl-lpc running. If <b>I</b> can do it (with help), anyone can do it. I'm on test four of ten, I've never gotten past six and so far so good. I'm not celebrating yet though, every time I do the thing starts with the skipping again.




---
**Don Kleinschnitz Jr.** *July 11, 2017 16:11*

**+Andy Shilling** it does make sense for maybe a test but not for real work as you would have to hack every job....



I still think we need a simple test of moves that can max stress the unit.



BTW does your machine do(still do) this? 


---
**Don Kleinschnitz Jr.** *July 11, 2017 16:13*

**+John Milleker Jr.** 

Confused:

Did you change over to GRBL?



<i>I'm on test four of ten, I've never gotten past six and so far so good. I'm not celebrating yet though, every time I do the thing starts with the skipping again.</i>



Is the above statement saying that it is still "skipping" with GRBL?


---
**Andy Shilling** *July 11, 2017 16:14*

**+John Milleker Jr.**​ Ok let me know how you get on I've not give down tree grbl route yet as I've been doing small jobs that require cutting, saying that I've never had any type of issue what do ever when cutting even if the bed is full of peices to cut.


---
**Andy Shilling** *July 11, 2017 16:17*

**+Don Kleinschnitz**​ I haven't done anything large for a while but I can try and create the problem when I get home if I still have the original file.



In regards to injecting a pause into the code why do you think we tag smart people like yourself in, we're just the monkeys that use what you create 😂


---
**John Milleker Jr.** *July 11, 2017 16:18*

**+Don Kleinschnitz** - Sorry, yes. I'm on GRBL-LPC and currently haven't had any of my skipping issues. I'm running the job I've had problems with a total of ten times. I've just finished test five with no issues.



**+Andy Shilling** - I'll post here once my tests are done. If it works and you'd like help I'll be happy to pass along the help others have given me regarding setting it up. The only down side at the moment is that I can't get the serial driver running on Windows 8.1 Pro and if you're using the LCD panel, it doesn't work yet on  this build. You also have to feed your jobs through LaserWeb with a USB connection.


---
**Andy Shilling** *July 11, 2017 16:24*

**+John Milleker Jr.**​ Yes please I will call upon your help in the near future if that's ok. I am set up running LW on my Mac and using a pi as the server so I don't know how different that will be.


---
**John Milleker Jr.** *July 11, 2017 16:31*

**+Andy Shilling** - That's something I want to do, run all of this through a PI so I don't have to worry about Microsoft ruining a job because it thinks I need another useless update!




---
**Don Kleinschnitz Jr.** *July 11, 2017 16:36*

**+John Milleker Jr.** so 1.) it really is a software problem (gulp) OR 2.) there is a machine problem that GRBL ignores, such as looking at end-stops on scans. 


---
**John Milleker Jr.** *July 11, 2017 16:41*

**+Don Kleinschnitz** - Ain't that a pain? I'm happy that this might be a fix for large raster engraves, but it's frustrating that after all this, we still can't put a finger on what the issue could have been.



Test Six passed, one thing I should note is that after these engraves the stepper motor is much cooler and the driver sinks (I've removed my fan cooling) are nowhere near as hot. I've also bumped the speed up to 300mm/s. 


---
**Jim Fong** *July 11, 2017 16:47*

**+John Milleker Jr.** at the higher speed the stepper motor is running at, there is not much torque with these small nema17 motors.  Any slight binding can cause mis steps to happen.  Drag chain can cause that and may be the problem.  I would remove that and see if it helps.  I only run a small coiled air hose.  


---
**John Milleker Jr.** *July 11, 2017 16:56*

**+Jim Fong** - I tried to simulate binding to see if that's posslble with my configuration. I tried holding the gantry, preventing my drag chain from moving and heck, I even turned off the laser, tried to pinch the belt where it's around the gear of the servo motor on the K40 and nothing. When I turned on the laser again it was in the same line it was before. I get that these motors are weak, but I feel that if my drag chain were to bind up, it would be my chain breaking while the motor happily kept buzzing along! 



I just completed test seven using GRBL-LPC with no issues. I want to get to ten before I celebrate and call it a victory though! 



I think **+Don Kleinschnitz** hit the nail on the head with GRBL-LPC overcoming some sort of hurdle with my stepper motors that I just so happen to have. I doubt it's the software, I doubt it's the Cohesion3DMini board as I'm not the first one to do large sized laser engraves. 


---
**Don Kleinschnitz Jr.** *July 11, 2017 17:00*

**+John Milleker Jr.** what tests are you referring to? Are they gcode source tests?



BTW are you using the stock K40 end-stops?


---
**John Milleker Jr.** *July 11, 2017 17:14*

**+Don Kleinschnitz** - The tests I'm talking about is the file I attached at the beginning of this post. I never had issues with any of my material tests or even large vectors with laser fill, I thought it was because of the sheer file size of the rasters.



I am using the stock end-stops. Never had an issue with them, but they were pretty poorly soldered when I got them and have since beefed that connection up.


---
**Don Kleinschnitz Jr.** *July 11, 2017 17:39*

I just verified with the smoothie firmware folks that the firmware does <b>not</b> resync on x scans. Making the xstop a less likely candidate. 


---
**John Milleker Jr.** *July 11, 2017 19:14*

What is that in laymans' terms? Does that mean it re-syncs with what it feels is the 0,0 set on the machine? Does Grbl do that?



If it makes a difference, when I was having issues I would set 0,0 and hit laser test to burn a tiny dot. I could then send the job and it would lose registration. When I sent it back to 0,0 the end of job dot would be as far off of my begin job dot by however far the skip was.



After my tenth test of the image on the original post with GRBL-LPC, I sent the image on wood. Looks good and I'm officially celebrating. Still have to work with the settings as I'm getting dark on the left and right (over scan I believe) but I'm happy.



With that said, I'd still love to see Cohesion3DMini and Smoothieware working. I don't see much of a difference in the grbl operation of the laser. 300 mm/s seems to be the happy maximum for my machine,  I bumped it up to 400mm/s and it started immediately skipping. Maybe in the future I'll try to fine tune it, but for now I'm going to leave it be and get some stuff sent out I've been wanting to promote. If there's ever an update to Smoothie that seems like it might fix the issue, I'll gladly re-flash and be a guinea pig.

![images/dc7b3f9a2344548f89ec2ffc704d04bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dc7b3f9a2344548f89ec2ffc704d04bd.jpeg)


---
**Don Kleinschnitz Jr.** *July 11, 2017 22:02*

**+John Milleker Jr.** by re-sync I mean checking the X end-stop and resetting the position when the carriage is at the left. I assumed this might be a way to insure that the firmware has a known postion at the start of each scan. Apparently neither smoothieware nor GRBL does such and if ever the actual position achieved does not match what the software told it, then the software is out of sync until the next home.



I have no idea why smothieware manifests this problem and GRBL does not, in this case I doubt we will never know....



I still don't think its a firmware defect but the results do not support that hunch. 



Glad your fixed.


---
**Andy Shilling** *July 11, 2017 22:09*

**+John Milleker Jr.**​ Looks great glad you got it working, I am unable to find my old file at the moment but once I do I hope to be able to replicate the problem and give **+Don Kleinschnitz**​ some working out to do again, he seems to enjoy it.


---
**Don Kleinschnitz Jr.** *July 11, 2017 22:15*

**+Andy Shilling** drives me nuts when things do not work as designed and we have no clue why! Usually these things will come back later when you least want them to ..... 


---
**Andy Shilling** *July 11, 2017 22:21*

**+Don Kleinschnitz**​ I've just realised John uploaded his files so I will run his G-code tomorrow and also create my own to compare the two and see if I can't give you some info.


---
**Douglas Pearless** *July 11, 2017 22:29*

I work with the firmware at a source code level and the speeds you are talking about are well within its capabilities.  The most likely issues are mechanical or setup of the driver chip itself, where Smoothie thinks it is achieving something (like a move from A to B at x mm/s) and the hardware is not achieving that.  Given your pictures it is likely that one of the steppers has stalled, Smoothie has kept driving the motor and it was not moving, and hence everything after that is offset by a fixed amount.  It could be that there is insufficient current to the stepper motor driver chip and something mechanical is just enough to cause it to miss steps by being unable to drive the motor properly and then the mechanical impediment goes away and Smoothie thinks it is somewhere when it is not.  It is most likely related to just one axis from your pictures.  Check the current settings on the POT on the driver boards themselves (say 0.7V, check the Smoothie online documentation) , do NOT set it too high otherwise the motor will draw to much current and itself overheat, or the driver chip overheats and shutdown and again Smoothie keeps driving it.  Do you have heatsinks on the driver chip itself?  Does the driver chips get hot (i.e. too hot to touch, but do not burn yourself!).  I have read back through your posts and I am not clear which driver chips you are using (A4988 or DRV8825).


---
**John Milleker Jr.** *July 12, 2017 17:09*

Thanks **+Don Kleinschnitz**, **+Andy Shilling** and **+Douglas Pearless**! 



Don, that's what I worry about. Happily buzzing along and whoops. Skipping again. I made three of my engravings yesterday after the ten paper test and they looked perfect. Over Scan on Laserweb really evened out my edge problems which I'm sure can probably be ironed out with fine tuning my acceleration and such, if I even wanted to.



If you can, share your files Andy - I'll run your job on GRBL too. If it's not something we can replicate, I'll even flash back to Smoothie (or I might have sent my old board back to Ray for replacement by then) and see if I can get it to show similar problems. Maybe with two of us, we can help the firmware brains find what's going on. What K40 do you have? I have the Orion Motor Tech white chassis type with red doors model. 



Douglas, I haven't gotten in and checked my actual voltage on the drivers. They're the A4988 ones that Ray supplies with the Cohesion3DMini. I have heat sinks on them and have tried multiples since I bought extras as I do want to go A and Z axes one day. The sinks have been getting hot, about a ten second touch before it was too much hot. I've put air on them and after a long job got them to stick around to room temperature. I've also put a heat sink on the X axis stepper with thermal paste. It wasn't getting as hot as the steppers, I could touch the hot bottom chassis and keep my hands on there. Thanks for the help! At the moment I'm running GRBL-LPC with no issues whatsoever (crossing my fingers), but want to help as much as I can iron out whatever is causing this. I may venture back to Smoothie if the bug, wherever it is, can be resolved.


---
**Andy Shilling** *July 12, 2017 18:47*

So I can't find the particular file I had problems with so I have started on your's **+John Milleker Jr.** for some reason when I open your gCode file this happens and I don't know how to move it into a workable area. I have just started running my own Gcode file now @ 100mm/s to start and I made the image 100x 162. I will update in a while.

![images/b55aef8469c785f87f65b66d3789cdf0.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b55aef8469c785f87f65b66d3789cdf0.png)


---
**John Milleker Jr.** *July 12, 2017 19:10*

**+Andy Shilling** - The file was made for books and it was easier to put 0,0 there where I had a sharp corner.



If you go to Files in Laserweb and then click on the document name under Documents (ColumbiaNotebook.... I believe) a window in the work area will show up with measurements. By changing the Min X to 0 it should put the file so that the bottom left is on 0,0. Min Y should already be on 0.




---
**Andy Shilling** *July 12, 2017 19:28*

**+John Milleker Jr.** I don't think it works like that because its a Gcode file, I have nothing in the documents section.

I have just had a small step out running at 150mm/s so I have saved the gcode and I'm now running a 200mm/s version. I will upload the files etc in a bit once Ive finished.

**+Don Kleinschnitz** do you know how I can move the Gcode over within LW?


---
**John Milleker Jr.** *July 12, 2017 19:43*

**+Andy Shilling** - The JSON file is there as is the original graphic, I didn't know you could adjust things like the speed with just the gcode.



You could always set the your machine's 0,0 to the right and then send the gcode, right?


---
**Andy Shilling** *July 12, 2017 19:48*

**+John Milleker Jr.** no Sorry I'm creating the gcode everytime just leaving the everything the same just increasing the speed. I've just had a larger step out @ 250mm/s. I'm now runnig @ 300 then plan to redo the 250 using the same gcode. Lets see if it produces the step in the same spot.


---
**Andy Shilling** *July 12, 2017 21:25*

OK I have run 6 test pieces from 100mm/s up to 300mm/s in 50mm/s increments. As you can see I had a very small step out on 150mm/s in line with the man's neck, next was 250mm/s version 1which was bang on the centre of the wheel and the last one was at 300mm/s at the top of the wheel.



Nothing but the speed was changed within LW and all but the 100mm/s G-code are attached in the link below. I also added a couple of vids to show the lasers stuttering whilst producing the file ( maybe down to Pwm speed).



After I ran these I went back and run the G-code from the 200mm/s image again and this time it completed without any issue. I am happy to continue trying these files to see where this goes but I feel it might help to try and find another file of similar size and style.



**+Don Kleinschnitz** do you have any ideas on what we could try next? I cant help thinking we are on the right track by putting a pause or 0,0 in to allow it to catch up, although that clearly couldn't be to final fix. Is there a G-code viewer that shows the laser route similar to how Slic3r can show a layer range?



[drive.google.com - Laser - Google Drive](https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)


---
**John Milleker Jr.** *July 12, 2017 21:43*

Glad I'm not crazy, **+Andy Shilling** - Just when you think it's good, it fails. And when you think it's going to fail, it's good.



Faster makes it fail worse, but sometimes when I'm running 50-100mm/s just to crank out a few jobs it still takes a big jump now and then ruining the piece.


---
**Douglas Pearless** *July 12, 2017 21:45*

Can you post your smoothie CONFIG file off the SD Card?



Do you have the Smoothie USB “drive” mounted when you print?



Cheers

Douglas


---
**Andy Shilling** *July 12, 2017 21:49*

**+Douglas Pearless**​ I will try and do it tomorrow as it's 11pm here now. I have cut the usb power pad on the bottom of the board so it doesn't mount now, I run LW through a pi server  using a shielded usb lead.


---
**Don Kleinschnitz Jr.** *July 13, 2017 03:50*

What is the acceleration set to ?


---
**John Milleker Jr.** *July 13, 2017 04:32*

When I had issues with the Smoothie I didn't change anything from the default Config.txt. That lists Acceleration at 3000 and Jerk as 0.05.



For me, I tried with and without the drive mounted and even during my extensive testing moved solely to the SD card. Generate gcode from Laserweb to my computer and then copy it to the SD card (tip from Ray). I would then properly eject the drive and unplug the cable.



Because GRBL-LPC doesn't have LCD or SD support yet, I've been sending faster jobs to it from my Windows 10 PC. It's survived power savings modes including a sleep. I moved the mouse and it picked right back up. And also survived well, Windows.


---
**Don Kleinschnitz Jr.** *July 13, 2017 14:24*

**+Andy Shilling** some <b>LONG</b> musings on your posted info:



1. Your video shows it marking from bottom up, did I see that right? This means that the shift is to the right not the left?

2. Does this only fail when marking going that direction?

3. I was a bit surprised how jerky, with pauses, the motion was, is that normal? Is that the trough-put problem that everyone is talking about?



<b>Thinking out loud about movement</b>

[careful this may make your head hurt]. 



The shift is from left to right on a scan boundary only. Meaning that it never skips in the middle of a scan. 

This means that randomly on a certain scan the head is positioned to start the scan to far to the right. 



<b>How would it have gotten further to the right?</b>

The error occurs on a scan boundary. That means the error is at the start of a scan going either right or left [marking in both directions]. 



<b>Error scenarios</b>

a.) Marking going left the head erroneously moved up one scan and right before it started the next right going scan.

b.)Marking going right the head erroneously moved up one scan and right before the left going scan.



In both cases a&b the stepper would have to have made an erroneous move up and right. That kind of positive move is not caused by mechanical error. Mechanical error would delay movement not accelerate it!



_My head hurts-



Further consider the scanning process;

.... images going right, 

moves up one scan 

then moves left for the first mark. 

... images going left, 

then moves up one scan

 then images going right.



<b>Is this telling us that the error occurs at the end of a scan?</b>

<b>What is wrong with this analysis and did I miss something?</b>



I cannot think of a mechanical reason that would cause this which leads me to believe that an erroneous scan start position is randomly occurring.

This could be from: 

...Gcode scan start errors

...Firmware x position counter errors.



If we make the above assumptions these are questions to answer:

4. Is the Gcode regenerated each run? If so, there could be a random error in the Gcode that shows up. 

5. What software is being used to generate the Gcode?



<b>Tests we can run if 4 is true</b>

A.) Run the image until you get a failure and then capture that Gcode

Regenerate and run a good image. Capture that Gcode. Compare the two files to see if there are scan start position differences.

B.) run Gcode (for a failed sample) in an interpreter to see if the shift occurs.



I lost track of the configuration of **+Andy Shilling** and **+John Milleker Jr.** tests. I.E. what speed, acceleration, file and gcode generator is used for each set of tests. A matrix that shows this might help keep us on track. 



I ran the 300.gcode through this viewer and did not see a shift.**+Andy Shilling** I assume this was the same Gcode that created the failure in your video???

[https://photos.app.goo.gl/mzAWQRKLm2ZPEhtP2](https://photos.app.goo.gl/mzAWQRKLm2ZPEhtP2)



[cncedit.com - Untitled Page](http://www.cncedit.com/basicviewer/default.aspx?VersionInfo=1.0.0.41)




---
**John Milleker Jr.** *July 13, 2017 15:56*

**+Don Kleinschnitz** - With Andy's answers too, maybe we can get to something. Here are my answers to your questions to him as I created the files and have lots of brain exploding (and probably useless) data available.



1. When I originally generated the files LaserWeb decided to work on them from a bottom to top orientation. I have a K40 and the homing switches are at top left, but when I zero Laserweb it shuffles to the bottom left. A little odd, but that was fine for me.



2. Direction shifts changed. They don't always go right. If I take the pots on the A4688 down my shifts went left. I took the pots up and they went right. I incorrectly thought if I could fine tune them just perfect maybe that would go away, but every now and then a job that seemed to go one way or the other or a shorter skip distance would just go against that conclusion. Typically though, they went right.



3.I really don't have much to comment on Andy's video. Jerkiness and pausing seems to be the norm. I get a lot of that with GRBL too running Laserweb from the computer but plan on going Raspberry Pi this weekend. Though Andy is using a PI as well. I wonder if that can be helped by cranking up the baud rate.



4. When I ran the some 30-40? total tests over the few days I was wrestling with it I used the same gcode file which was saved on the SD card. When I first had this issue I was doing the same. I don't believe Laserweb generates the gcode every time unless you hit generate. I would doubt it's a gcode error since with that untouched gcode file can generate random skips.



5. LaserWeb, for better or worse generates the gcode. If it were gcode issues I wouldn't be surprised considering how wonky LaserWeb seems. I've toyed with VisiCut and wanted to punch myself in the face. Even started looking at raster to gcode generators to get something new in there but switched firmwares too quickly to really go any further.



My Configuration, Hardware: [Amazon.com](http://Amazon.com) purchased Orion Motor Tech White Chassis, Red doors K40 40w Laser. Stepper motor details "Smooth" Type 17HA704Y-15A1 FH170228. Cohesion3DMini with A4688 Stepper Drivers w/heat sink and 24v fan for cooling. External 24v 6a power supply. Using several SD cards and USB cables, however major test was not hooked to PC, just run from SD card.



Software: Both Windows 8.1 and Windows 10, LaserWeb (several versions, but latest for the last major test). The only large photos I've sent were generated with a friend online that has PhotoGrav, I'd like to see if it's worth a purchase. Saved to BMP for import to LaserWeb. Acceleration was kept default, however I did change it around on my first C3DMini board which had issues. So for my tests 3000 acceleration, 0.05 jerk and 200mm/s speed. 


---
**Don Kleinschnitz Jr.** *July 13, 2017 16:16*

More clues in the search for truth...



<i>2. Direction shifts changed. They don't always go right. If I take the pots on the A4688 down my shifts went left. I took the pots up and they went right. I incorrectly thought if I could fine tune them just perfect maybe that would go away, but every now and then a job that seemed to go one way or the other or a shorter skip distance would just go against that conclusion. Typically though, they went right.</i>



Now my head really hurts :). I can fathom why adjusting steppers down might cause them to take to long to move but why would adjusting them up cause them to over travel. Remembering this only does this intermittently...... mmmm



Are both of you using C3D....? I think yes!



I can't get out of my head that nothing fails during in-scan moves only at the transition. In that case normally the Y axis is moved a small amount up and the x either does not move or slightly the opposite direction from its last pixel. ... Yet this error is a big shift!



**+Andy Shilling** can we try and get a video of it failing :)!


---
**John Milleker Jr.** *July 13, 2017 16:28*

**+Don Kleinschnitz** - Ray thought it was just coincidence. Especially since pot adjustments seemed to affect the outcome, but still suffered from random skips going in different directions and lengths.



Side note, I've blown through about ten high resolution and large 8x12 slate pieces with changing nothing but the firmware. Not even the hint of a problem. Top notch results. Run from LaserWeb, Windows 10 laptop that kept going into power save mode and even went to sleep a time or two, so I've been anything but gentle with feeding it data. I've been trying to break it, but haven't gone over 300mm/s yet.




---
**Andy Shilling** *July 13, 2017 16:45*

**+Don Kleinschnitz**​​ **+John Milleker Jr.**​​ Sorry guys I'm not able to help out tonight, something has come up. Here's what I can give for now. 



1. Running C3D now previous was moshi board.



2. I replaced optical endstops to mechanical ( still in the original positions)



3. Stock firmware (untouched albeit pwn set to 200)



4. Always steps to the right for me ( Don yes you were correct starts bottom left rasters up and to the right)



5. I'm running LW on pi as a server but use my Mac to generate g-code and send through to machine.



I'll try adding more to this when I get chance but hope this helps for now.


---
**John Milleker Jr.** *July 14, 2017 04:07*

**+Andy Shilling** - No problem bud. Thanks for recommending the Pi BTW. Just installed Laserweb to a Raspberry Pi 3 and loving it so far. Need to test it on some big files to make sure the RPi can handle it. So far so good though!


---
**Andy Shilling** *July 14, 2017 15:55*

**+John Milleker Jr.** No problem I find it makes life so much easier on the Pi.



**+Don Kleinschnitz** Right I am here ready to go, Firstly yes the 300 G-code was the biggest fail for me. I will try and set up a good action cam in a while and run the code again in the hope we can actually see the error as it happens.



Are there any other tests I could do for you with this file?

maybe somebody else could offer up a Gcode made in another piece of software if that is possible?


---
**Don Kleinschnitz Jr.** *July 14, 2017 16:20*

**+Andy Shilling** I checked the gcode and cannot find an error and the simulator does not show one either.

The only thing I can think of that may help is a close up of the image exactly where the step occurs....


---
**Andy Shilling** *July 14, 2017 16:25*

**+Don Kleinschnitz** ok I'll do the best I can in a moment,I'll see if I have  macro lense thing for my phone and see how well it comes out. I'm just charging up my cam then I'll run another couple of test with it on.




---
**Andy Shilling** *July 17, 2017 19:30*

+Don KKleinschnitz **+John Milleker Jr.**​Sorry for the delay in the tests, daughter was taken into hospital so everything stopped. She's fine and I'm back on it.



I'm just running a third test with the 300 G-code file, all have been filmed and two have errors on them.



In fact as I type this the third has now got two errors in it but my cam speed recording😡. 



I will upload the videos in a bit and the new rasters also. Let's hope something shows up in the videos.


---
**John Milleker Jr.** *July 17, 2017 19:33*

Thanks **+Andy Shilling**, glad to hear your daughter is OK!


---
**Andy Shilling** *July 17, 2017 20:12*

Right here goes.



Images are uploaded Videos are on their way, can you see the Latest folder I have added to the original?



First run the image stepped out to the right @ 2min 14 seconds but this step was only about 2mm but was still present, went on to finish the image fine. 13mins to complete



Second run was uneventful  but only took 10 minutes to complete no stepouts.



Third run ( video stopped short of the second step out)

first major step out was at 13 seconds and steps out 5mm.

Second error was actually my first ever step in ( looking at the position I would say around 5 mins in) and this returned by 2mm. this also took 10 minutes to complete.



Having watched the third video I can not say there is anything that jumps out as to why it errors, even in this major way. **+Don Kleinschnitz** does that bit of software you linked give a timed response to the running g-code? I'm on a mac so can't install it.



What's next? 


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/j5jeb8Magbx) &mdash; content and formatting may not be reliable*
