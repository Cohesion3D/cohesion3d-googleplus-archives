---
layout: post
title: "I've set up my K40 with C3D Mini as per the online doco"
date: August 02, 2017 08:28
category: "C3D Mini Support"
author: "Simon Cook"
---
I've set up my K40 with C3D Mini as per the online doco. I have second variant shown at the bottom of the page. Everything works except PWM. The laser is always firing at 100%. I've tried changing laser_module_pwm_period which curiously was initially set at 20 not 200 in config.txt to 200, 400 and 600. After reset it still fired at 100% as you can see. What should I look at next?

![images/593b799b287afde55bf52c79aa5b7771.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/593b799b287afde55bf52c79aa5b7771.jpeg)



**"Simon Cook"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 11:19*

Hmm... initially at 20. Do you have one of my first batch boards from around the new year? 


---
**Simon Cook** *August 02, 2017 11:42*

That would be about right. I ordered it in Jan and then broke my right hand and haven't been able to do much until now. 


---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 11:50*

Ok. Grab the new firmware and config file from Dropbox link at bottom of instructions, put those on the memory card. 



Can I see a picture of your board and wiring? 


---
**Simon Cook** *August 02, 2017 12:16*

Hope this shows what you want to see ...

![images/e2ed49da04e792249d3393061134dac4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e2ed49da04e792249d3393061134dac4.jpeg)


---
**Simon Cook** *August 02, 2017 12:17*

New firmware and config.txt results in no firing from the laser at all at any %age power. 


---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 12:22*

Correct. We need to rewire L. That's the green wire all the way at the right end of the psu. I suggest disconnecting it from the psu screw terminal and using another piece of wire to connect that to the 2.5- which is the 4th screw terminal from the left along the bottom row. 


---
**Simon Cook** *August 02, 2017 12:52*

Just to be sure I have this right I hookup the marked PSU port to the marked C3DMini port after removed the existing PSU wire?

![images/627e000ba74c3cb82609ea84d0c1e1f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/627e000ba74c3cb82609ea84d0c1e1f0.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 13:06*

Yep, pull the existing wire and connect the 2 things you marked. Also insulate the end of the green wire you pulled. Send another pic when it's wired to confirm. 


---
**Simon Cook** *August 02, 2017 14:18*

I only had black hookup on hand at a decent gauge which makes it a little hard to see.

![images/93976adde374f46f9bcd97e0caf16d01.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/93976adde374f46f9bcd97e0caf16d01.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 14:19*

Works for me.  


---
**Simon Cook** *August 02, 2017 14:59*

That's nailed it. Working as expected. Thanks for your awesome support. Is there a source for what's what on the PSU? Are all K40 PSUs the same? 


---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2017 15:08*

Yeah, search the k40 groups or look at **+Don Kleinschnitz** blog. 


---
**Simon Cook** *August 02, 2017 15:10*

I had looked there previously ... obviously need to look harder :) Thanks again.


---
**Don Kleinschnitz Jr.** *August 02, 2017 15:16*

**+Simon Cook** 

[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)

[http://donsthings.blogspot.com/search/label/K40%20Laser%20Power%20Supply](http://donsthings.blogspot.com/search/label/K40%20Laser%20Power%20Supply)


---
*Imported from [Google+](https://plus.google.com/101890222799169927378/posts/CsFJNdHUKXn) &mdash; content and formatting may not be reliable*
