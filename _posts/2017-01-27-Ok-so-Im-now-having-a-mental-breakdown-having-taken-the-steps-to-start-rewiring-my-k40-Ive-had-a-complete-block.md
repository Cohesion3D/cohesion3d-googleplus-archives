---
layout: post
title: "Ok so I'm now having a mental breakdown, having taken the steps to start rewiring my k40 I've had a complete block"
date: January 27, 2017 15:52
category: "General Discussion"
author: "Andy Shilling"
---
Ok so I'm now having a mental breakdown, having taken the steps to start rewiring my k40 I've had a complete block. Below are photos of my old PSU and high voltage board or what ever that monstrosity is.



I was sure I had it all sorted out but now I don't. I've sorted 240v in and run extra plugs for the air assist and cooling, but now it's time to start on the C3D. 



New PSU is green terminal type.



24v,G,L all sorted (5v not needed)

Pmw connects to (in) between 5v and ground



Now what do I need from P+ G K- K+ ? 



Also if it's not answered in the above how do i wire in the pot if needed and the ammeter. I have a digital voltage display as well.





Please save me from myself as I've at here for 3 hours now and not progressed at all lol.





![images/043956eb7723d3c6e2156701d6406550.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/043956eb7723d3c6e2156701d6406550.jpeg)
![images/217c88f2e53b1851df0e994b5b0d849f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/217c88f2e53b1851df0e994b5b0d849f.jpeg)

**"Andy Shilling"**

---
---
**Andy Shilling** *January 27, 2017 15:58*

AAARRRRRRGHGHGHGH just sat back and had another look so I'm now thinking P+ and G are for arming the laser, K- and K+ are for test fire. Is this correct?




---
**Andy Shilling** *January 27, 2017 16:20*

**+Peter van der Walt**​ I have replaced that PSU but because this one is so old I believe this is why I'm having the problems getting my head round the rewire.


---
**Andy Shilling** *January 27, 2017 16:30*

I thought a volt meter was a better idea than the ammeter to get consistent settings or am I getting them mixed up. Also I hadn't thought about the HV return where the bloody hell does that return to lol.


---
**Don Kleinschnitz Jr.** *January 27, 2017 16:39*

**+Andy Shilling** I'm with **+Peter van der Walt** I would use this LPS for a doorstop :). Must be the prototype of the POS we are all using.

But if you must: 

P+ and G are usually the interlock circuit

K+,K- is the fire circuit.

<s>-----------------------</s>

You mention a new PS with green connectors, not this one?

You are installing a new one but not the one above?

The wiring of an new one depends on that type.



Here is a diagram for one type do the terminals look like this. Post picture and source of purchase.



[https://goo.gl/photos/WP7a8Szm6GNToXeK9](https://goo.gl/photos/WP7a8Szm6GNToXeK9)



<s>-------------------------</s>

Look here for more info on my conversion, some may apply to you.

There are  links in this index to much of what you are looking for including schematics?



[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
**Andy Shilling** *January 27, 2017 16:59*

**+Peter van der Walt**​ **+Don Kleinschnitz**​ Sorry guys I made this all very confusing didn't I. I am replacing a moshi board and the PSU pictured above in original post with a new C3D mini  and the PSU pictured below. Obviously having no markings on the original PSU to tell me what is what I am limited to what I have worked out regarding the new wiring. 



I hope this helps clear it up both I do thank you for your help and patience.

![images/cf965c17cf87353bbdaf157fc355b06f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cf965c17cf87353bbdaf157fc355b06f.jpeg)


---
**Andy Shilling** *January 27, 2017 17:09*

On the 6 pin is P+ G K- K+ G IN 5+ if that means anything to you. I can't believe I've got from being pretty sure how to wire it to looking at it like it's is from another planet.


---
**Andy Shilling** *January 27, 2017 17:14*

Could we at least say it's a Porsche then lol


---
**Andy Shilling** *January 27, 2017 17:23*

Ok thanks **+Peter van der Walt**​. SUV really? lol


---
**Andy Shilling** *January 27, 2017 18:00*

**+Peter van der Walt**

This is the post that put the idea of a digital voltmeter in my head, does this not work then?



[https://plus.google.com/+KonstantinosFilosofou/posts/GQXMU6rv6MZ](https://plus.google.com/+KonstantinosFilosofou/posts/GQXMU6rv6MZ)


---
**Don Kleinschnitz Jr.** *January 27, 2017 18:03*

**+Andy Shilling** for that supply I think its: 

[photos.google.com - New photo by Don Kleinschnitz](https://goo.gl/photos/HrmXkjqamPY4WAqs5)



However, unlike this picture shows I only support PWM connections to "L" for PWM.




---
**Andy Shilling** *January 27, 2017 18:11*

**+Don Kleinschnitz**​ you absolute diamond that looks to be what I need. The markings for the PSU I have are under the green terminal blocks but I'm sure everything is as it should be so I can copy this. I guessing mine is the she was round as I'm guessing that is a - for the laser HV return tucked up under the corner of the terminal.

![images/db667c0bd291dc80028a6e14a1e827c9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db667c0bd291dc80028a6e14a1e827c9.jpeg)


---
**Don Kleinschnitz Jr.** *January 27, 2017 18:45*

**+Andy Shilling** one ground it for the frame (safety) and one is for the laser cathode return from the meter.



Check with an ohmmeter to see that both are gnd to the case of the LPS.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/a5zVrJkyfK9) &mdash; content and formatting may not be reliable*
