---
layout: post
title: "Will the power supply you sell for the board work in Australia (220-240V)?"
date: October 03, 2018 01:19
category: "C3D Mini Support"
author: "Kieron Nolan"
---
Will the power supply you sell for the board work in Australia (220-240V)?





**"Kieron Nolan"**

---
---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:26*

the external power supply in the cohesion3d store has a rated input range from 100vAC to 240vAC. It does ship with a standard non polarized US power plug . here is an image of the power plug terminations:

![images/0043ecc650774f9d052d9ef22624e601.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0043ecc650774f9d052d9ef22624e601.jpeg)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:27*

you would just need one of these:

![images/b6e65109cab1e772670cab985d18807a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6e65109cab1e772670cab985d18807a.jpeg)


---
**Kieron Nolan** *October 03, 2018 01:32*

Fantastic, thanks


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:33*

No problem. Enjoy!


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2018 02:26*

Precisely! 


---
*Imported from [Google+](https://plus.google.com/113795912126148646501/posts/XLoU1okQ8QJ) &mdash; content and formatting may not be reliable*
