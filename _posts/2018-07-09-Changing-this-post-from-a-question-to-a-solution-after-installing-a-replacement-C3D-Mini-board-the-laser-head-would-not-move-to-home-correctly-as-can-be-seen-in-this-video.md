---
layout: post
title: "Changing this post from a question to a solution -- after installing a replacement C3D Mini board, the laser head would not move to home correctly, as can be seen in this video"
date: July 09, 2018 16:50
category: "FirmWare and Config."
author: "Joel Brondos"
---
Changing this post from a question to a solution -- after installing a replacement C3D Mini board, the laser head would not move to home correctly, as can be seen in this video. However, a reminder from LightBurn insturcted me that perhaps it was as simple as having plugged in the Y-stepper motor connection reversed. DOH! Perhaps others can learn from my mistakes . . .


**Video content missing for image https://lh3.googleusercontent.com/-IBUOkE1rswQ/W0OSY80d_XI/AAAAAAAA_7c/7x7pCnslOckXONcWi-kFcb9Dbo3BO6ogwCJoC/s0/VID_20180709_093257693.mp4**
![images/7d524260299b2b50fbc04ffa8f3f33a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7d524260299b2b50fbc04ffa8f3f33a7.jpeg)



**"Joel Brondos"**

---
---
**Tech Bravo (Tech BravoTN)** *July 09, 2018 16:54*

This may not help but i will not be available to assist until later this evening. 
{% include youtubePlayer.html id="2An1tZPR5tA" %}
[youtube.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://youtu.be/2An1tZPR5tA)


---
**Joel Brondos** *July 09, 2018 17:02*

Ach, I just had the Y-stepper motor connection reversed. DOH!


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/dX1yDRGxVBW) &mdash; content and formatting may not be reliable*
