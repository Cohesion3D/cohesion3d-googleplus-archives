---
layout: post
title: "Loving my K40 (thanks to Cohesion3D) so much, and it's been such a earner in my shop that it's time to go bigger"
date: December 11, 2017 01:19
category: "K40 and other Lasers"
author: "John Milleker Jr."
---
Loving my K40 (thanks to Cohesion3D) so much, and it's been such a earner in my shop that it's time to go bigger. 



Can any Cohesion3D users personally recommend an 80w machine? I'm looking at the Mophorn 80w machine, currently at my budget of $3k on Amazon. I of course want to run GRBL-LPC (Cohesion it at all possible) on it, will the LCD panel continue to work or does that disappear? Not like I use one now with GRBL.



I currently use LaserWeb on a Raspberry Pi and love it. Maybe LightBurn will change that, not sure.





**"John Milleker Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2017 01:44*

GRBL-LPC = no GLCD Screen.  Smoothie raster engraving should work properly (ie: no stuttering when running over USB) with Lightburn. 



I looked up the machine you named, I think it's this:  



[plus.google.com - I just purchased a 100w 700x500 laser. I also have purchased a cohesion3d, a...](https://plus.google.com/105510960839020211759/posts/f8tjg9TWi7y)



So we've already documented a rewire of it to C3D.  



I believe that machine has a Ruida controller by default (I'm going off of what I saw in John's rework and some ebay listing pics) - which Lightburn also talks to.  So you can do the C3D Upgrade if you want, or just pay for the higher tier of Lightburn that will talk to these machines too. 



In case you do want to do the C3D rework, let me know and I'll let you know which components you'll need exactly.  



My personal thought process is if I needed a larger machine I would want to buy the largest bed size with the cheapest/ worst electronics (ie; another M2Nano, not a Ruida) and handle the electronics rework myself. 


---
**John Milleker Jr.** *December 11, 2017 02:21*

Thanks Ray! That's great news. It does look like the same machine. I should have guessed, same chassis and doo-dads, different manufacturer.



Sounds like I have a bunch of great options. Controller itself and LCD access with the pro version of LightBurn. I certainly need the bigger size, not exactly the 80w, but for only a few hundred more, why not. 



And it's Amazon, which I know I'm paying $500 more going through them and not eBay, but the purchase protection makes it worth it for me. I also have this other C3D that I need to exchange when things slow down for you. Maybe we can put that towards what the new machine would need.



I just may be pulling that trigger soon. I hope LightBurn comes out as intended January 1st. Thanks again!


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2017 02:33*

 I'd just make sure the payment is going on a good credit card and I'd be relying on that protection if I had any issue.



I personally don't believe that the increase in purchase protection is worth $500. But, I understand everyone has different considerations. 



We refer to this one as the 80w red and black. It's not unknown. Pretty sure I've seen it in the FB Laser group, the big general one. 



Meanwhile, I just found out the high school in my town has a BOSS laser. It's a different red and black :) - first difference is where the decimal place is. 



![images/ad8e3bb55075d61c2d166a0006a11f75.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ad8e3bb55075d61c2d166a0006a11f75.jpeg)


---
**William Kearns** *December 24, 2017 05:15*

Ray is that BOSS a descent machine?


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2017 05:21*

Interesting story.   Based off my reading of the big laser group on Fb, I would have said yes - it's in the top 3 brand names I hear a lot (you know, the ones in the class below Epilog/ Universal).  Also the teachers seem to like it. 



That said, I then found out that it's running a Ruida controller (ish) and that the software is a reskin of RDWorks.  Which **+LightBurn Software** is writing a better replacement to... So with that in mind, I think you can get a Chinese "red and black" like John is saying above for 1/3 of the price and get the same thing.  He could tell you firsthand, I'm just a guy with an underused K40 :)



And he can also tell you about Lightburn with the DSP vs some potential hiccups that would warrant a C3D switchover.  I am gathering the information.


---
**LightBurn Software** *December 24, 2017 05:28*

Boss does use the Ruida controller, but they build their equipment in the US, or at the very least refurb/upgrade it, and they offer local support. So if that stuff is important to you, Boss has a good reputation. If you’re comfortable doing your own maintenance and using community support, you can save a bunch of money buying a Chinese import and dealing with stuff yourself. It’s not for everyone, but it’s worked out for me.


---
**John Milleker Jr.** *December 24, 2017 05:38*

It's good to hear that someone is doing the dirty work for consumers, but like **+LightBurn Software** said. If you're a tinkerer, it's actually quite enjoyable, being too heavy just to heave out the window every time things go wrong helps, but once it's set, they're amazing machines.



Several thousands saved for the headaches and I learn a lot about the machine and machines in general? Only you can decide if it's worth it. But I will say that I feel more confident in my abilities to fix and hopefully find mechanical issues. Some electrical too.


---
**William Kearns** *December 24, 2017 05:42*

I have the same issue as you John. The little K40 has been a huge hit and a great start for me but need to go bigger! 


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2017 05:45*

I think someone told me they got the Chinese red and black off eBay, during off season, for around 1800. If that's the case it's actually pretty appealing to hack on. That said, I've always maintained that I want to get a larger work area with crappier electronics machine for the same price and then put a new controller in. 


---
**LightBurn Software** *December 24, 2017 05:46*

Mine is a 700mm x 500mm, 100w. It was $2500 USD, delivered. Obviously I’m a tinkerer too, but that’s a big price difference and pays for lots of upgrades and spare parts if necessary.


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2017 05:48*

Oh, if you paid 2500 then I'm mistaken. I think I've seen other machines, blue, with a larger work area an M2Nano for 1800, possibly. 


---
**John Milleker Jr.** *December 24, 2017 05:49*

Indeed William, I bought the branded 'Mophorn' 80w 20x28" on Amazon for $3k shipped from California to me in Maryland. As Ray says, it's just called the generic red/black. Was here in a week and some days through Roadrunner shipping.  Came in great condition other than the Z bed wires knocked loose from the up/down buttons. No biggie, just slip them back on.



That's still expensive for what you could get on eBay and a little patience, but my justification was if the thing comes DOA I felt as if I had a little more protection power with Amazon. Hearing that every now and then people struggle with foreign sellers to get things fixed.



I never expected the K40 to be as good as it was, nor laser products so popular. With everyone getting 3D Printers and hobby CNC machines, I wonder if Laser will ever become a staple in homes with the amount of support needed. Cooling, Ventilation and that whole 'could burn your house down in a blink' sort of thing.


---
**LightBurn Software** *December 24, 2017 05:51*

Wattage matters - I’ve seen smaller or lower power versions in the $1800 range (like 60w).


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2017 05:53*

Very true, and I didn't say anything because I simply have no memory of whether I was looking at a 60 or 80w. Probably wasn't a 100w. 


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/fPJqfCEHNLw) &mdash; content and formatting may not be reliable*
