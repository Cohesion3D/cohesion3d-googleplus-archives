---
layout: post
title: "What software is recommended for the chinese k40?"
date: September 29, 2017 16:39
category: "K40 and other Lasers"
author: "your patio"
---
What software is recommended for the chinese k40? I have an old Windows XP laptop, will it be compatible?





**"your patio"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 29, 2017 16:42*

The main software we recommend is LaserWeb4. The bigger concern would be the graphics capability, I would think that such an old computer might no be able to handle the WebGL requirements. You can install it and find out. 



You can start looking at the instructions at Cohesion3D.com/start[cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
*Imported from [Google+](https://plus.google.com/100544602349675438828/posts/YR7K2dGKGyG) &mdash; content and formatting may not be reliable*
