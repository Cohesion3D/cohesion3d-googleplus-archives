---
layout: post
title: "keep having issue getting my z axis motor up and running"
date: August 14, 2017 05:26
category: "K40 and other Lasers"
author: "William Kearns"
---
keep having issue getting my z axis motor up and running. It just twitches and barely moves. I have the voltage set to 2 amps and 16 micro steps. any thoughts?





**"William Kearns"**

---
---
**Cris Hawkins** *August 14, 2017 06:14*

Voltage is volts, CURRENT is amps. We need more information about your setup. Is your Z axis a commercially available product or DIY?




---
**William Kearns** *August 14, 2017 06:16*

 It's a lightobject table with cohesion 3D mini and 24 volt external ps


---
**Ray Kholodovsky (Cohesion3D)** *August 14, 2017 07:53*

Pictures of wiring please, video of trying to jog it? Any chance you have a spare stepper motor you can use for testing? 


---
**Kevin Lease** *August 14, 2017 11:40*

What model stepper controller


---
**Kevin Lease** *August 14, 2017 11:57*

The light object stepper driver I tried first works on 5v logic works fine with a separate controller that puts out 5v but not with c3d I think because dir stp Enable output from c3d is 3.3v 

I had success with light object z axis with tb6600 driver which can work with arduino or c3d voltage


---
**Ray Kholodovsky (Cohesion3D)** *August 14, 2017 12:09*

**+Kevin Lease** our wiring plan (which has a step by step guide on the docs site) uses 5v high side and dir step en - is hooked up to the C3D which pulls it low. Hence 5v operation. 


---
**Kevin Lease** *August 14, 2017 12:18*

Yes I followed the guide you have on the site

It works perfectly for me with tb6600 which accepts 3.3v logic but not with two other 5v logic steppers I tried

But i am not an expert just sharing my experience


---
**Ray Kholodovsky (Cohesion3D)** *August 14, 2017 12:21*



Oh I see now. It tripped me up because you said 3.3v, which made me think some other wiring was used. Technically our wiring plan does use 5v, so that a range of drivers should work on it. Would be curious to hear your experiences at a later time. 


---
**William Kearns** *August 15, 2017 13:26*

I did try another stepper same results. ![images/049ede2684015e7808ceaddd875e5080.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/049ede2684015e7808ceaddd875e5080.jpeg)


---
**William Kearns** *August 15, 2017 13:26*

![images/3972d6a0f57d28976cde8af559a4bfc9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3972d6a0f57d28976cde8af559a4bfc9.jpeg)


---
**William Kearns** *August 15, 2017 13:26*

![images/937909600597f8a5e9d05245c6c064dd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/937909600597f8a5e9d05245c6c064dd.jpeg)


---
**William Kearns** *August 15, 2017 13:26*

![images/1e7177d8bbd0a4f8386093650dd5850b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e7177d8bbd0a4f8386093650dd5850b.jpeg)


---
**William Kearns** *August 15, 2017 13:27*

![images/60a435bb7c7efa016f3cc5e0fa0603d6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/60a435bb7c7efa016f3cc5e0fa0603d6.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 15:15*

Try to swap in another TB6600 driver. I believe you got 2 from me. 


---
**William Kearns** *August 15, 2017 20:52*

I did will try the other one and update


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/JyjjkX6VSir) &mdash; content and formatting may not be reliable*
