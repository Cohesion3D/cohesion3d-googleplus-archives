---
layout: post
title: "Ok. These are sent to try me"
date: May 04, 2017 18:19
category: "C3D Mini Support"
author: "Colin Rowe"
---
Ok. These are sent to try me. 

New psu in new laser. Psu is same as one in my old laser. 

Only difference is  its digital. No pot. Switch or a meter. 

So. Wire up same as old psu but put the 5v in G and k+ into correct place on psu. The k+ is for test fire.

There is no laser switch so I add a micro switch that I will interlock with lid later.

Power up and connect to computer.

All good so far. Home the laser from reprap. Nice and smooth now.

Press test button. Nothing.

Open up and when I press test button red L led lights up but no laser fire.

Ok I think.  Not closed interlock. I close interlock and laser fires....

Nothing else pressed.

Ran a prog with interlock off and the red led on psu pulse so looks like a signal getting there but laser not fire.

Press interlock and laser turns on but on all time so just burns a black shape.

Press test button and it fires without the interlock. 

The power works as you can still adjust it.

Not sure why this is happening.

I think I have it all hooked up correct.

4th one in to L.

Photos here. 

So near yet so far. Think it must be a silly mistake on wires.

It jogs correctly nd moves where it should. Just the laser on off



![images/e047695661a9fbc822b83032279d68a0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e047695661a9fbc822b83032279d68a0.jpeg)
![images/a85ee889c26b4516a24e17e3927d3c46.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a85ee889c26b4516a24e17e3927d3c46.jpeg)
![images/e9fff6d556e40cd8e11edc369a166fe0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e9fff6d556e40cd8e11edc369a166fe0.jpeg)
![images/df8da86509dcf27ead0650714bc082b0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/df8da86509dcf27ead0650714bc082b0.jpeg)
![images/54f984339d9277cb3db128a85568b4c5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/54f984339d9277cb3db128a85568b4c5.jpeg)
![images/2ad4bb06fb9acfaf730132987876abe8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ad4bb06fb9acfaf730132987876abe8.jpeg)

**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 18:21*

L should go to terminal 4 not 3  go one over to the right. Let's start there. 


---
**Colin Rowe** *May 04, 2017 18:22*

Yes I moved after I took photo made no difference 


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 18:25*

Can you help me understand the issue better? 

Red test fire button works now? But the laser on off is still an issue, so are you trying to fire it from somewhere else now?  Test fire button in LW?  During job from LW? 


---
**Colin Rowe** *May 04, 2017 18:43*

Test fire on psu works


---
**Colin Rowe** *May 04, 2017 18:44*

Test fire button on k40 lights up when pressed but no laser


---
**Colin Rowe** *May 04, 2017 18:44*

Test fire on laserweb. Nothing


---
**Colin Rowe** *May 04, 2017 18:46*

Run prog and laser led on the psu is pulsing but laser does not fire. If I close the interlock it just fires at the % it's set at does not turn off just on all time


---
**Colin Rowe** *May 04, 2017 18:48*

I will try and disconnect everything bar the 220v in and the L- then try to see if just pressing interlock fires laser. If it does I think it may be the psu at fault


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 18:49*

The test fire button in LW requires settings to be configured before it will work.  The % and duration. 



Try just sending a G1 X10 S0.6 F600 line in terminal.  The head should move while firing. 


---
**Colin Rowe** *May 04, 2017 18:50*

Nope

 I have run some progs I know work and it moves but laser not fireing 


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 18:51*

Ok.  Pull the L wire from the board and touch it to ground.  Does the laser fire? 


---
**Colin Rowe** *May 04, 2017 18:52*

Not at shop now will try tomorrow 


---
**Colin Rowe** *May 04, 2017 18:52*

Ground on board


---
**Colin Rowe** *May 04, 2017 18:52*

Or ground on psu


---
**HP Persson** *May 04, 2017 23:26*

Sure you wired the digital panel correctly?

Here is how it´s connected.



From 5V on PSU to 5V on panel

From IN on PSU to IN on panel

From GND on PSU to G on panel

From L on PSU (middle connector) to P- on panel



GND and P+ on PSU should be connected to each other, either button, limit switch in the lid or similar (laser enable)

![images/baad5e8debe3c5af548f08a2277a1329.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/baad5e8debe3c5af548f08a2277a1329.jpeg)


---
**Colin Rowe** *May 05, 2017 04:58*

Yes checked it with multimeter. It's all correct. When I press button the led on panel lights. The led on the psu lights. But the laser does not fire


---
**Colin Rowe** *May 05, 2017 05:39*

Its not P- on panel its K+, there are only 4 wires but seen 5 if you look at m2nano online. this is a picture of mine





![images/06f2980416db8fec1d9272cea4bba5d7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/06f2980416db8fec1d9272cea4bba5d7.jpeg)


---
**HP Persson** *May 05, 2017 10:00*

Ah, older type of panel, different markings.

In my chart above, P is K.

K- is never used, it´s a ground it takes that from G.



Put a small cable between P+ and GND on your PSU (empty slots), without this you cannot get it to fire. Only by the button, which has a internal connection for the P+ pulling it to ground.

If the laser fires continuously you have something else wrong where L is pulled to GND.






---
**Colin Rowe** *May 05, 2017 10:22*

thanks, will look later today.

My plan is as follows.

hook up 220v and earth

connect switch between GND and P+

(Gnd and P+ are for safety switch ?)

Connect L- via a annalog meter.

nothing else.

If i close GND and P+ nothing should happen, only make the laser live for firing. If that works than its something else causing it






---
**HP Persson** *May 05, 2017 10:47*

Nothing happens, it just allows the PSU to take signal from L to fire the tube.

If the firing is constant when bridging check the L lines.



P+ and GND can be used to a button, lid switch, flow switch or similar to "enable" the tube, when bridged PSU can fire the tube trough the controller.

Digital panels has laser enable on a button, but it has to be bridged on the PSU to work (or as a lid safety).



Both L lines are connected internally, if it fires constant, try removing the L to the digital board to see if it stops.


---
**Colin Rowe** *May 05, 2017 11:02*

![images/1b37f2e4ba340ce956d05fbc5c7686a1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b37f2e4ba340ce956d05fbc5c7686a1.jpeg)


---
**HP Persson** *May 05, 2017 11:07*

Oh, full power, then it´s the three cables to the right in middle PSU connector.

IN, 5V and GND, measure the value on them (IN and 5V) both when P+ and GND is bridged, and when it´s not.

Make sure once more, that IN on PSU goes to IN on digital panel.

Set power on the panel to 30 or 40% (whatever, just not 0 or 100 :) )




---
**Colin Rowe** *May 05, 2017 11:09*

ok will check later  but it does alter the power from panel




---
**HP Persson** *May 05, 2017 11:12*

**+Colin Rowe** Is the change visible on the mA-meter when you lower it on the panel ? If it does - it works.



On the movie it looked like it was controlling the tube, but just had full power? 

If you make a square and a circle inside it, does it control the tube or is the beam always on making a mess?



Edit: the problem is beam is always on, or beam is always 100% power?


---
**Colin Rowe** *May 05, 2017 11:26*

yes it changes on the meter

I run a prog i know works and the red led flickers like its getting a pwm signal but laser is not firing.

I close the switch on p+ & ground and laser fires at 100% of what it is set to (not full power) but its just on, not on off to raster image

as in video


---
**HP Persson** *May 05, 2017 11:29*

Ah, allright. Was hard to see what it was doing in the video :)

There is a L-problem, i would beleive it´s the L going to the digital panel.

On the PSU, remove the L white cable, middle connector - and try again.


---
**Colin Rowe** *May 05, 2017 11:36*

Will do. At work at moment.




---
**Colin Rowe** *May 05, 2017 17:45*

SUCCESS, will document problems, just to say that it took a while to sort and it was the wires. Thanks for all your help 


---
**Colin Rowe** *May 06, 2017 21:48*

A quick report on what I had to do to make the laser work.

I put in new PSU and it was marked up as standard.

GND P+ L GND IN 5v

problem was that when i connect switch to GND and P+ the laser fired as if i were pressing test (not good)

in the end after a lot of checking and altering wires I found that if i connected up correct way for all other wires then connected the laser arm switch to P+  and L everything works as should, laser runs ok in prog and Pot settings work along with the test fire button.

No idea why its like this, but it works.

I wull post the make of the PSU in case anyone else gets same problem.

PS the 90 degree turn on the pot fixed the Y axis.

Now just have to get lw4 running on pc, for some reason it will not load any files?




---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/TdU3sk1gm91) &mdash; content and formatting may not be reliable*
