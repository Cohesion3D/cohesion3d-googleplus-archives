---
layout: post
title: "So I finally was able to get back to the laser cutter"
date: October 04, 2017 04:33
category: "C3D Mini Support"
author: "Erik McClain"
---
So I finally was able to get back to the laser cutter.  



I am still having a couple issues with trying raster a gray scale image.



First, no matter what I do it does not seem to PWM the laser,  its on 100% on everything but white, on white is is off.  Essentially laser is acting as if it is black and white.  I have tried many different settings and in the sim it seems like power PWMing.



Second If I just do a big black square everything seems to move smoothly, I have the steppers dialed in.  When try to do a gray scale raster, it seems to jerk a bit as it travels through the cutting section, almost as if its is trying to change speed.  I have taken a brief look at the Gcode and I am not seeing and changes to velocity.



It would seem to me that the two issues are linked some how.  Any tips?



Here is a snippit of the Gcode if that helps in anyway 



G21         ; Set units to mm

G90         ; Absolute positioning

M3           ; Turn on Laser



; Pass 0



; First Move

G0 X8.15 Y181.51 ; Generated by LaserWeb (lw.raster-to-gcode.js)

; Size       : 0 x 0 mm

; PPI        : x: 300 - y: 300

; PPM        : x: 0.0846666667 - y: 0.0846666667

; Tool diam. : 0.1 mm

; Feed rate  : 12000 mm/min

; Beam range : 0 to 1

; Beam power : 0 to 100 %

; Options    : trimLine, joinPixel, burnWhite



G1 F12000



G1 X8.15 Y181.51 S0.0000

X8.20 S0.0039

X8.30 S0.0196

X8.40 S0.0118

X8.50 S0.0235

X8.60 S0.0392

X8.70 S0.0118

X8.75 S0.0157

Y181.61 S0.0000

X8.70 S0.0118

X8.60 S0.1294

X8.50 S0.3216

X8.40 S0.2824

X8.30 S0.0588

X8.20 S0.0039

X8.15 S0.0196

X6.15 Y181.71 S0.0000

X6.20 S0.0039

X6.30 S0.0196

X6.40 S0.0275

X6.45 S0.0078

X6.95 S0.0000

X7.15 S0.0039

X7.75 S0.0000

; stripped: S0.0627

X8.05 S0.0000

X8.10 S0.0157

X8.20 S0.0078

X8.30 S0.1176

X8.40 S0.4314

X8.50 S0.7294

X8.60 S0.7725

X8.70 S0.5765

X8.75 S0.1882

X8.95 S0.0000

; stripped: S0.0078

X9.15 S0.0000

X9.30 S0.0078

X9.40 S0.0039

X9.45 S0.0353

X9.65 S0.0000

X9.70 S0.0039

X9.80 S0.0157

X9.85 S0.0039

X10.05 S0.0000

X10.10 S0.0392

X10.30 S0.0078

X10.40 S0.0157

X10.60 S0.0039

X10.70 S0.0118

X10.85 S0.0078

Y181.81 S0.0000





**"Erik McClain"**

---
---
**Erik McClain** *October 04, 2017 04:37*

![images/a6bca818e5115271bb2fce9a1e0de156.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a6bca818e5115271bb2fce9a1e0de156.jpeg)


---
**Erik McClain** *October 04, 2017 04:38*

![images/266b10d79984f60be540a38368d3b915.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/266b10d79984f60be540a38368d3b915.jpeg)


---
**Erik McClain** *October 04, 2017 04:38*

![images/ad51cbfade70b20525983386d88a26a1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ad51cbfade70b20525983386d88a26a1.jpeg)


---
**Erik McClain** *October 04, 2017 04:39*

![images/6dde5dac4b43acde7648ffecd3d764b8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6dde5dac4b43acde7648ffecd3d764b8.jpeg)


---
**Erik McClain** *October 04, 2017 04:40*

![images/b94e3eeb126e5920fdae9ffefaef3739.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b94e3eeb126e5920fdae9ffefaef3739.jpeg)


---
**Erik McClain** *October 04, 2017 04:40*

![images/aa81aba23b99d785f090e7f47abb030f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aa81aba23b99d785f090e7f47abb030f.jpeg)


---
**Joe Alexander** *October 04, 2017 04:45*

the smoothie firmware is known to stutter with greyscale images( causing the "jerking" look). You could load grbl-lpc which is known to be much better at engraving images with less stuttering. And if your still using the current knob on the front( like most do) then the controller will actuate it at  the intensity set in laserweb4 (note that the max output is whatever the knob is set at. does that help?


---
**Erik McClain** *October 04, 2017 04:48*

I will give it a go, have a link by any chance? If not I will figure it out


---
**Joe Alexander** *October 04, 2017 04:52*

[github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases) found it


---
**Erik McClain** *October 04, 2017 04:56*

Awesome!  Thanks a lot.  I have done ton of work upgradeing the optics, fume extraction electronics ect.  Just want it to work now lol.


---
**Joe Alexander** *October 04, 2017 05:02*

i hear ya on that, just got mine back up after it popped the laser psu( and subsequently the following one but I got a free replacement for that one). next I fear my tube will do as I have had to turn it up another 10% for the same 15ma output.


---
**Anthony Bolgar** *October 04, 2017 05:29*

Make sure that the PWM is set to between 200 and 400 in your config file. Some machines need it as high as 400, most work in the 200-300 range.


---
**Erik McClain** *October 04, 2017 09:32*

Is that the config it comes with or the grbl


---
**Joe Alexander** *October 04, 2017 10:14*

that would be the smoothie config but grbl-lpc does have a similar setting, just not 100% sure what its called. Had i gotten grbl-lpc to work reliably on my smoothieboard 4x I would know what it is :)


---
**Joe Alexander** *October 04, 2017 10:16*

$33, its right on the main page lol makes sense


---
**Erik McClain** *October 04, 2017 12:02*

Awesome, I will try to update that in smoothie first probably before I try and switch to grbl-lpc.  Then I will make the switch so I can compare the two.  With any luck I will have the time tonight.  Thanks!


---
**Anthony Bolgar** *October 04, 2017 12:39*

Hope it all works out for you.


---
**Erik McClain** *October 04, 2017 17:12*

Just to be clear I drop firmware.bin on the root of the SD card,  cycle power on the Mini thats it?



Does it use a config file, if so is the the same as smoothie?  I haven't really found much about how you configure it.


---
**Joe Alexander** *October 04, 2017 17:31*

yea just the firmware.bin and I dont believe it needs the config file for grbl. you can config it through laserweb4's console using the $$ commands. Also if its not registering try disconnecting the usb connection then reconnect it, and/or reboot the comp so it detects the change. that was my issue with it for me, connection was weird at first. Still need to try again :)


---
**Claudio Prezzi** *October 04, 2017 17:44*

12'000mm/min at 0.1mm beam diameter is much to fast for smoothiewares "streaming" protocol together with LaserWeb4. Try 1'000mm/min to get smooth movements. With grbl-LPC, you should be able to go up to 8-10k.

And you should check your cabling if grayscale (PWM) is not working.


---
**Erik McClain** *October 04, 2017 18:16*

Ah, that's really really slow for engraving.  With the old board 200-300mm/s a sec (12000-18000 mm/min) is what I did everything at.  



1000 mm/min will take an eternity some of the pieces took up to 20 min at 300mm/s



Would running off the sd card be better?



I did notice that if I did a solid black cube that it ran smooth, makes sense now, as that solid object would have a lot less instructions  


---
**Anthony Bolgar** *October 04, 2017 18:33*

I run a stock nano m2 board right now on my 60W and it engraves at 350 mm/s smoothly (21,000 mm/minute) Until **+Erik McClain** just pointed out the speed, I never realized that there was such a big difference.


---
**Erik McClain** *October 04, 2017 18:50*

**+Anthony Bolgar** can you give me more info on your setup up I am curious, did you make a blog or anything?


---
**Anthony Bolgar** *October 04, 2017 19:13*

My 60W is a brand new laser, only had it a couple of weeks. Everything on it is still stock, I am using scorchworks K40Whisperer software with it right now.


---
**Erik McClain** *October 04, 2017 19:19*

Ah interesting never heard of scorch works, be sure to load up some examples of what it can do :) 


---
**Anthony Bolgar** *October 04, 2017 19:38*

Google it, he reverse engineered the nano protocol and made a simple interface that does not require the dongle.




---
**Erik McClain** *October 04, 2017 20:14*

I did, just curious to see the results of someone not the creator :)


---
**Anthony Bolgar** *October 04, 2017 21:56*

Will try to post some pics tonight and maybe a quick video showing the raster speed of 350mm/s .  I did try rastering at 400mm/s and it started to skip and bog out.


---
**Erik McClain** *October 05, 2017 02:01*

Ok, I have tried 200, 250, 300 and 400.  Still not getting any PWM



To change the number, I edit the config.txt then reboot the board?  Or is there more to it then that?



laser_module_pwm_period                       200              # this sets the pwm frequency as the period in microseconds



I am reviewing the wiring, I am not sure which wire actually controls PWM I assumed it would be pulsing the signal turning the laser on and off, or is there more to it?


---
**Anthony Bolgar** *October 05, 2017 02:25*

Your PWM output should be connected to L on the LPS. Changing the value is as you described, edit it, save it and reboot.


---
**Erik McClain** *October 05, 2017 02:43*

This is the photo from the manual I added arrows to what I believe you are referring to

![images/0cfeae1a3432e5a726d68ef460fa976d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0cfeae1a3432e5a726d68ef460fa976d.jpeg)


---
**Erik McClain** *October 05, 2017 02:43*

![images/8ee468b81ffe5cd0a59f0633e9b40547.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ee468b81ffe5cd0a59f0633e9b40547.png)


---
**Erik McClain** *October 05, 2017 02:44*

![images/026acd1d4fe6d7b4778483c3b29156f7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/026acd1d4fe6d7b4778483c3b29156f7.png)


---
**Erik McClain** *October 05, 2017 02:46*

I have mine wired just like the instructions, however there is this on the board and in the instructions this header is not used



![images/c796bde260066a3b985fbaf12f224197.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c796bde260066a3b985fbaf12f224197.png)


---
**Erik McClain** *October 05, 2017 02:48*

So are you saying that the instructions on how to wire the board is wrong and that I need to wire the center pin of the PWM headed to the "L" which is the green wire in the first photo?


---
**Anthony Bolgar** *October 05, 2017 02:51*

Nope, the instructions are correct, that PWM header is no longer used. I am out of ideas, **+Ray Kholodovsky** do you have any ideas?


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 02:53*

Did you change to grbl or are you still on Smoothie firmware? 


---
**Erik McClain** *October 05, 2017 02:57*

still smoothie at the moment


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 03:00*

Cool.  I don't think I've heard of a pwm period of more than 400 being required.  



Have you seen our pwm tuning guide in the FAQ?  Do you have a pot and mA meter on your machine? If so, running those G1 lines with the different S values and observing the mA gauge would be a clearer indication of what your machine is and is not responding to. 10mA is a good pot value for this. 



Also, if you do adjust the pot, the laser power responds to that? 


---
**Erik McClain** *October 05, 2017 03:01*

I would like to have the default working proper before I switch to yet another unknown so I can have a baseline for comparison.  I slowed my raster speed to 25mm/sec it seems to be relativly smooth


---
**Erik McClain** *October 05, 2017 03:03*

ok I can give that a go, I did not see the tuning guide yet


---
**Erik McClain** *October 05, 2017 03:26*

ok 200 no change, so I put it at 400  PWM and I am getting something



at 100% I have 10mA. at 10% I have about 7mA  I can also hear the difference when the laser activates.  



I will try a few other PWM values to see if I can get a bigger range then 3ma

  


---
**Erik McClain** *October 05, 2017 03:35*

So it seems for about the first 5 passes (I add a pass at .001 pwm) I get a steady 1mA increase form 5mA to 10mA.  After .5 ish no change


---
**Erik McClain** *October 05, 2017 03:41*

at 600 PWM, my low was about .3-.4 mA and maxed at about .6-.7 PWM



at 1000 PWM my low is .2 mA and maxes out at .9 PWM



I am going to try a raster now


---
**Erik McClain** *October 05, 2017 04:11*

Thanks everyone! I wouldn't say this raster it's an overwhelming success, but definitely in the right direction.



0 PWM seems to still activate the laser that's why there is a bar at the bottom,  by lowering pot I was able to tune it out but the laser is barley activating.  Some experimenting tomorrow to dial in the PWM further should start to give better results 



(I also need to get the wood fixtured at the right height, this was well below the focal point, new lens to learn)



![images/850458fca5eca7e26fe32ebb0bdd2c4b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/850458fca5eca7e26fe32ebb0bdd2c4b.jpeg)


---
**Erik McClain** *October 05, 2017 13:00*

(Still smoothie)



If anyone is still reading this,  any idea why the white is still burning?  the laser is turning on and off just fine but when it goes to do the raster 0 PWM is still firing, I would have assumed that the signal would be flat 0 when the PWM is that low.



I will explore more on my own tonight but if there are any obvious things to check please let me know.



I am going to try more PWM frequencies tonight see where the saturation point is if I have the time.  Also I will be editing the test to include an S of 0 to make sure its not the image some how setting a minimum. 



p.s. I tried it the burn white on and off, that's where there are two dark bands in the photo




---
**Claudio Prezzi** *October 05, 2017 17:02*

Check that you have "laser_module_maximum_power 1.0" and "laser_module_minimum_power 0.0" in the config file (or these lines marked out with #) and also "PWM MAX S VALUE=1" in LW4.

Then, limit the max. power with the pot.



I do also need to go as low as possible with the pot to get decent grayscale and not burn too much.


---
**Erik McClain** *October 05, 2017 17:07*

Kk thanks I will double check all those.



Yeah even at 2mA I am having a head time not charing the wood at 25mm/sec


---
**Claudio Prezzi** *October 05, 2017 17:08*

And check that the "white" background of the picture is white, not light gray. If it's light gray, you can increase brigthness and contrast in the Laser Raster operation. Also check that you use 0 to 100% as the Power Range.


---
**Erik McClain** *October 05, 2017 17:08*

Kk will do.  I think some of the charing is due to being lower then the focal point


---
**Claudio Prezzi** *October 05, 2017 17:13*

**+Anthony Bolgar** Please don't forget that the stock moshi board is not able to vary the laser power! Instead, it does black and white dithering. This is much faster but no real grayscale.


---
**Erik McClain** *October 05, 2017 17:18*

so if i switched to black and white dithering like the old board would I be able to hit the 300mm/sec engraving (18000mm/min)



I will test this later, so if your not sure the limits no worries



when you have to make 40 trophies 25mm/sec is agony lol but for the occasional project that needs it, I can deal with it.  I will try grbl at some point too and test its limits too once I have this under control


---
**Erik McClain** *October 05, 2017 17:20*

Oh and speaking of switching can I just swap sd cards back and forth or do have to reload the .bin every time I switch? (not that I plan on doing this many times)


---
**Claudio Prezzi** *October 05, 2017 17:34*

**+Erik McClain** With smoothieware you will not get much faster. The reason is a not so optimal usb communication of smoothieware. With grbl-LPC, you can reach 200mm/s. 

The moshi board gets faster because it uses a totaly different communication with less overhead but is proprietary.


---
**Claudio Prezzi** *October 05, 2017 17:36*

You can swap the SD card as much as you like. Only config.txt is needed, no firmware file.


---
**Claudio Prezzi** *October 05, 2017 17:39*

Or did you mean switching between firmwares? 

Every time the board boots, it checks if there is a firmware.bin on the card. If so, it flashes the firmware and reboots.


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 17:39*

The card just needs to be present at boot, and as Claudio said just the config.txt file is absolutely needed. 

I do caution against putting the card in while the board is powered on, put it in sideways a bit, contacts short, card is fried, or just corruption. Only a few cases out of hundreds but I've still seen these edge cases happen. 


---
**Claudio Prezzi** *October 05, 2017 17:41*

That's the reason why the board deletes the finmware.bin after flashing, otherwise it would get in a loop of flashing the firmware.


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 17:42*

Renames to FIRMWARE.CUR after flashing. You only need to put the firmware file back if you are going between Smoothie and grbl, as those are different firmwares that need to be flashed. 


---
**Erik McClain** *October 05, 2017 18:09*

Ok that is kind of what I thought might be happening there.



Ok, one last question this one will show my ignorance lol.  Its been a long time since I have programmed a micro controller, but the gcode file isn't that big, why can it just buffer the whole code?  That and transferring a file via USB is in megabytes a second so doesn't seem to be a volume of data what part is the the real bottle neck?  Would reading from an SD card be faster?


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 18:13*

If you are on Smoothie, my stance is to put the file on the sd card and run it that way. If you have the glcd you can run via the menus there, if not then you can use the play command (google Smoothie player) via LW gCode terminal or use Pronterface which has a nice SD print feature. 


---
**Erik McClain** *October 05, 2017 18:16*

Gottcha, thanks this give me plenty to explore for a while thanks again for everyones help.


---
**Erik McClain** *October 06, 2017 09:59*

So good news all around. 



I put my PWM at 1500 up from 1000, now I have the full range of S Values.  Is there any downside that anyone can think of having such a high PWM?



I figured out why the "white" was burning.  I used laser web to scale up the image as the one I have is really low resolution.  This caused artifacts in the white space that wasn't visible in laser web but has enough grey that super low values of around S0.001 were popping up in the GCode uping the brightness seemed to help remove it.



The extra 5 seconds it takes to put the file on the SD card and run it is very much worth it.  I was able to triple my feed rate to 75mm/sec for rastering, this is still a little slow but I am much happier I will see if there are any other things I can do to get faster.





![images/802b72412c57f7ae322fa47aa198212c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/802b72412c57f7ae322fa47aa198212c.jpeg)


---
**Claudio Prezzi** *October 06, 2017 13:49*

The only downside of such a long PWM period is, that it is limiting the max speed or decreasing pixel resolution. 



A value of 1500 means 1.5ms for a single pwm pulse (=min. pixel duration). So 0.1mm pixelsize at 1.5ms per pixel means 15ms/mm or 66.7mm/s! If you drive faster, your picture gets blurred.


---
**Erik McClain** *October 06, 2017 14:05*

Ah that is an  odd way to go about it, I had assumed it was a frequency setting, I didn't pay close enough attention, it does say period lol



I guess I should revisit it some to see how low I can go to get a reasonable power range.


---
**Erik McClain** *October 06, 2017 14:23*

I am not sure if a little blurring is a bad thing,  I might call it blending rather then blurring but you have a good point.  I would say an ideal would be at least two periods per pixel, which would put my period at 666.... (Humm conicidence? lol)



I will see what I can do about that,  the problem is that the my low range almost halved between 600 and 1000, from about 4mA to .2mA



Maybe I am wrong but I would think that better image resolution will come form low end.  I will run some trials.



I should be able to go down to 1000 easily if I only let the PWM get up to .9,  I will have to find the sweet spot.


---
*Imported from [Google+](https://plus.google.com/102506401249278564565/posts/Rwk1S435Sm8) &mdash; content and formatting may not be reliable*
