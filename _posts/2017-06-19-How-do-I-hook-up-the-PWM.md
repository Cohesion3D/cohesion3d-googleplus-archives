---
layout: post
title: "How do I hook up the PWM?"
date: June 19, 2017 01:22
category: "C3D Mini Support"
author: "Jeremy Proffitt"
---
How do I hook up the PWM?  I mean 3 pin cable to 3 pin cable on the  power supply where it says G IN 5V right?  and change the line in the config.txt.  I don't have to do anything with DG or K+K- just leave them connected to the display?



I did find I had to supply G and +5v back to the display, but that was just a couple of jumpers.  



Test fire works fine on the panel, not on the laser web, although I do have control of the motors.



Thoughts?

![images/982f21300721fbf2613099144151c51f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/982f21300721fbf2613099144151c51f.jpeg)



**"Jeremy Proffitt"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 01:24*

We pulse L so as per the instructions, you don't use the cable that was included in the kit. 


---
**Jeremy Proffitt** *June 19, 2017 01:26*

And don't change the PWM pin in config.txt?




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 01:27*

Nope. Should be 2.5. 


---
**Jeremy Proffitt** *June 19, 2017 01:38*

Okay, corrected that.  Are their instructions for settings in lw4, I tried G1 X10 S0.1 F600 and test fire, nothing from LW4, but can test fire from the laser's console




---
**Jeremy Proffitt** *June 19, 2017 01:39*

I shut everything down and unplugged the usb when making the config change.


---
**Jeremy Proffitt** *June 19, 2017 01:39*

Are their screen shots for lw4?




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 01:47*

1. Please get the proper firmware and config files again, dropbox link at bottom of instructions. Put those on the MicroSD Card (delete old ones first), safely eject the drive, reset the board. Watch LEDs count up in binary as firmware flashes.

2. Try the G1 line with a value of S0.4 and S1




---
**Jeremy Proffitt** *June 19, 2017 02:02*

Okay, it fired from the GCode, but still not from the test.  Is there a LW4 config file?  Or do I set it to Smoothieboard?




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 02:04*

Yes, you have to configure settings in LW for that test button to work. I think it's been covered in this group before. I haven't written any official instructions for LW4 yet. If you can't find the details let me know and I'll dig it up. 


---
**Jeremy Proffitt** *June 19, 2017 02:07*

I will dig in tomorrow.  This has been a huge help.  Hardware is good and software I can handle 


---
**Claudio Prezzi** *June 19, 2017 06:21*

For settings in LW4 check [cncpro.co - Live Jogging](http://cncpro.co/index.php/33-documentation/initial-configuration/laserweb-configuration/14-gcode)


---
**Jeremy Proffitt** *June 25, 2017 14:42*

ok




---
*Imported from [Google+](https://plus.google.com/104075028378884449894/posts/NZjDf51eZkh) &mdash; content and formatting may not be reliable*
