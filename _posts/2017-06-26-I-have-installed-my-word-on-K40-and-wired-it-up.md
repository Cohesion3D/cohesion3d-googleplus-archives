---
layout: post
title: "I have installed my word on K40 and wired it up"
date: June 26, 2017 15:57
category: "C3D Mini Support"
author: "Peter Stirk"
---
I have installed my word on K40 and wired it up.

Using laser web 4 with recommended config files.

So far cannot get laser to home in any direction but will go to 0/0



![images/ee928173a7639d222401c31c36c95db2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ee928173a7639d222401c31c36c95db2.jpeg)
![images/d08fe9f64a9c82d7750a309089f0c181.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d08fe9f64a9c82d7750a309089f0c181.jpeg)
![images/2ded567c05fbdf9ad6d14adca0af4cb1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ded567c05fbdf9ad6d14adca0af4cb1.jpeg)

**"Peter Stirk"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 16:13*

Hi Peter, can you send a M119 gcode command in LaserWeb terminal (bottom right) and see the responses?  

You should see a 0 for a switch that is not pressed, and a 1 for a switch that is pressed.  



I think your using this type of switch has inverted behavior compared to the stock switch being used in the k40.  Easy fix in config if so. 



Also, the command we want for homing (actually go and press switch) is G28.2


---
**Peter Stirk** *June 26, 2017 16:23*

Ok entered M119 no response from machine . Hit jog and head moved  Have previously entered G28.2 in Laserweb settings as web site suggested. 


---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 16:25*

Screenshot of the entire LaserWeb window please?  

Capital M of M119   ?


---
**Peter Stirk** *June 26, 2017 16:42*

Yes Used M119   

![images/59ab3a02a2adba26805334b5d3a31c4a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/59ab3a02a2adba26805334b5d3a31c4a.png)


---
**Ashley M. Kirchner [Norym]** *June 26, 2017 17:17*

The command to put in the gcode box is 'G28.2', you missed the 'G' there.


---
**Peter Stirk** *June 26, 2017 17:20*

Ok I also just had a problem with smote board drivers trying to reload them now.....If this works do i need to invert in config file if so which lines do i add ! too




---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 17:39*

Let's do one thing at a time, the M119 will tell us whether invert is needed. 

Sounds like you are having some communication issues, let's get the drivers resolved first.  What Operating System? 


---
**Peter Stirk** *June 26, 2017 17:54*

win 7 working on it!


---
**Peter Stirk** *June 26, 2017 18:27*

Ok Drivers loaded and tried M119 with no feed back. So i have changed config file and inverted switches.. So its working great now. If i use jog it will still pile into the sides but I guess I cannot change this?  Thanks for your help and advice any  more is welcomed ..Thank you




---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 18:30*

There's no "soft limits", no.  So if you try to go past a switch it will do so.  If that's what you're referring to.  



But if you send a G28.2 will it go to the switches and stop? 



Let's figure out why you can't get M119. 



Try M114 please and show the screenshot. 



What USB Cable are you using? 


---
**Peter Stirk** *June 26, 2017 18:46*

If i type in G28.2 the laser now homes  and stops correctly...If i type in M119 or M114 I get no response at all..The usb cable is standard usb i used before the install but not the chinese one i was sent




---
**Ray Kholodovsky (Cohesion3D)** *June 26, 2017 18:47*

Ok. Sounds good. 


---
**Peter Stirk** *June 26, 2017 18:54*

Changed usb cable makes no diff to commands M119 or M114

So unless It stops working or does some thing strange I will take this as a fix! Thanks again


---
**Tim Anderson** *June 27, 2017 17:44*

Just got mine tonight and trying to install it and getting the same thing, no response to M119 or M114 but G28.2 tries to send the head out of the back ha ha



Have swapped the switch type round and enabled the debounce line also, still not behaving.


---
**Ray Kholodovsky (Cohesion3D)** *June 27, 2017 17:47*

Ok, so maybe something changed in LaserWeb. 

The thing is though, Peter changed his machine with endstops.

The config file included works for the stock endstops. If you're homing the wrong way you need to flip the motor orientation. 


---
**Peter Stirk** *June 27, 2017 21:39*

I have downgraded my laser web to earlier version of V4 as th eimport function  for art work was an issue but the M119 etc still did not work I think maybe version 3 works??? I have had usb disconnection issues that are prob linked to win 7 and software ...So I will be upgrading to ethernet adapter. Once I inverted the endstop configs (powered) and changer the motor direction the software can now get a handle on workspace dimensions...




---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 01:57*

The go-to answer is to get Pronterface, which should certainly show the responses for M119 etc..  But it sounds like you are good to go. 



Regarding comms, there are a few things to look into:



1. Whether the exe driver worked (typically, the fact that you can connect and talk to the board would mean that it did, but some people have issues with the exe in which case uninstall and do a manual install of the driver).  



2. Putting the gcode job file on the Mini's MicroSD card and running it from there, probably using the GLCD.  For higher detail jobs this is a preferred way to ensure reliability. 



3. Using a "proper" USB cable ie one that has ferrites on the ends, etc... 


---
*Imported from [Google+](https://plus.google.com/101746683149411797795/posts/QdDENV9qVQa) &mdash; content and formatting may not be reliable*
