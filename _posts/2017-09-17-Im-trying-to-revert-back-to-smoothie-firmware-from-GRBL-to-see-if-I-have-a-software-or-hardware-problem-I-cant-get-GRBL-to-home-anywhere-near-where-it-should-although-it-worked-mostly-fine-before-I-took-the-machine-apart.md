---
layout: post
title: "I'm trying to revert back to smoothie firmware from GRBL to see if I have a software or hardware problem.( I can't get GRBL to home anywhere near where it should, although it worked mostly fine before I took the machine apart"
date: September 17, 2017 23:28
category: "FirmWare and Config."
author: "John McKeown"
---
I'm trying to revert back to smoothie firmware from GRBL to see if I have a software or hardware problem.( I can't get GRBL to home anywhere near where it should, although it worked mostly fine before I took the machine apart today) I've added the firmware.bin file from the Dropbox link and reset the board several times. I've reformatted the SD card and added the config and .bin files again. But each time I open Laserweb and connect to the machine, it reads a grbl connection. Do I need to change something else? I have no workable controller right now unless I go back to the stock board. 





**"John McKeown"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 17, 2017 23:53*

Can you format this card fat32 and try again or try using a different microsd card? 



Are you powering up the laser or plugging in USB? 



Do you have a GLCD connected?



What we expect: 

Put firmware.bin onto sd card, and plug the card into the Mini with the board powered off.

Power the board on. 

Watch the LEDs count up (in binary) to signify the firmware is flashing, then resume their usual blinking pattern once done. 

On the card, name changes to FIRMWARE.CUR

If USB is plugged in, the drive should start showing up again. 



Your earlier post seems to have dissappeared.  I think g+ is behaving very strange today. 






---
**Claudio Prezzi** *September 18, 2017 08:46*

Don't forget to put the smoothie [config.txt](http://config.txt) on the sd card too!


---
**John McKeown** *September 18, 2017 12:19*

That seems to have worked. I don't think I did anything different this morning but it's worked!Thanks guys!




---
**John McKeown** *September 18, 2017 12:42*

OK. it's connected and seeing the smoothie firmware but there is no response from the machine to any commands. The machine has power and fires manually. The axes can be moved by hand, as if they are not engaged.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 13:09*

Did Smoothie work on this machine before? Is the config file on the card? 



What are the details of the machine? 


---
**John McKeown** *September 18, 2017 13:35*

Yes, Smoothie worked before and GRBL worked intermittently before I stripped the machine. I have fat32 formatted the SD card and added the config.txt file and the firmware.bin file from the Dropbox link. Laserweb now recognises the firmware and is connected. However there is no movement from the machine which is an analogue style blue K40. 


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 13:36*

Try Pronterface. 

My first debugging step before all else is to try a new sd card. 


---
**John McKeown** *September 18, 2017 13:37*

I have uses Don's G Code settings but haven't changed anything in the config


---
**John McKeown** *September 18, 2017 13:51*

OK, I'll try a new SD card and then try Pronterface 


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 13:54*

Try Pronterface first.  


---
**John McKeown** *September 18, 2017 14:25*

Getting no response from Pronterface either. I'm not familiar with it so may be missing something. I've connected and tried the jogging buttons.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 14:29*

Send M119 do you see a response? 


---
**John McKeown** *September 18, 2017 14:37*

No response


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 14:37*

New sd card. 


---
**John McKeown** *September 18, 2017 14:38*

It is a new one, I did that first as I didn't see your message before.


---
**John McKeown** *September 18, 2017 14:38*

I think i need to take the gantry apart again and recheck the wiring maybe?


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 14:39*

Ah. Just disconnect everything from the board and plug in just USB. See if the leds count up as discussed before. 


---
**John McKeown** *September 18, 2017 14:49*

Tried to send a video. Think it's gone to main discussion . Board lights up but doesn't seem to be counting up.


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 14:50*

Yep. It's not flashing. Everything is disconnected? There's a firmware.bin on the card (still)? 


---
**John McKeown** *September 18, 2017 14:53*

Yes, firmware.bin is still there. Everything disconnected aprt from USB


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 14:56*

Try powering it from the laser now instead of USB. Plug in just the power connector. 


---
**John McKeown** *September 18, 2017 14:59*

The same flashing LED, but with reds power light on. No count up


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 15:05*

Please try once more by formatting the card and using a "fresh" firmware by pressing the download button here: [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-cnc.bin)



Please put this onto the card by itself.  The flash pattern will be different without a config.txt present, but again our goal is the counting up of LEDs.


---
**John McKeown** *September 18, 2017 15:21*

Done, lights flashing but doesn't appear to be a count up


---
**John McKeown** *September 18, 2017 15:24*

the same when powered from USB and laser


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2017 16:11*

Honestly, my best tip is to try a few more cards.  I'm not aware of what  causes this aside from poor quality/ corrupted microsd cards. 


---
**John McKeown** *September 18, 2017 17:02*

OK, I only have three sd cards, one brand new and good quality, one that came with the board and a third one which is good quality too. I will format and try the third now but otherwise I guess I'll have to go back to the stock board and see if that connects.


---
**John McKeown** *September 18, 2017 17:33*

Third card didn't work either :/ I've flashed GRBL again without any problem and it jogs the machine. The problem is that it limits the movements to an area not corresponding to my laser bed. Do I need to change something in the config? I spent hours yesterday looking for a solution to this so I'm stumped.


---
**John McKeown** *September 18, 2017 18:01*

OK. Very strange. After several disconnects, the problem seems to have rectified itself. Anyone else experienced this (Double post on Laserweb forum)


---
**Don Sommer** *September 20, 2017 14:14*

Mine is very temperamental about flash cards  I have 10 but only 2 will make it flash but all work in the  cooler ok


---
**John McKeown** *September 20, 2017 15:23*

I've flashed back to GRBL Lpc no problem, it just won't flash Smoothie. GRBL is working ok though so it's ok. Now my LPS has blown though :D It's never ending!


---
*Imported from [Google+](https://plus.google.com/108648609732667106559/posts/Wt5rDuQGxEt) &mdash; content and formatting may not be reliable*
