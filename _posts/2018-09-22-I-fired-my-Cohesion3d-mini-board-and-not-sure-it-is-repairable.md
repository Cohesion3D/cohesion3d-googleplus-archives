---
layout: post
title: "I fired my Cohesion3d mini board and not sure it is repairable"
date: September 22, 2018 16:21
category: "C3D Mini Support"
author: "adstaggs"
---
I fired my Cohesion3d mini board and not sure it is repairable. I connect it to a 24v power supply.



![images/9a65c1a90f57f4e5594de0b70bab118f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9a65c1a90f57f4e5594de0b70bab118f.jpeg)
![images/f235da2575368919d7d6435af15c0204.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f235da2575368919d7d6435af15c0204.jpeg)
![images/e95951708aa5399eb717fbf71ff61610.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e95951708aa5399eb717fbf71ff61610.jpeg)
![images/252abc55894729a20959821a9ee6c183.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/252abc55894729a20959821a9ee6c183.jpeg)

**"adstaggs"**

---
---
**Griffin Paquette** *September 22, 2018 21:21*

Can you give more details? The board is 24V capable but it seems like there was more done than just that. 


---
**adstaggs** *September 23, 2018 01:42*

this how it was connect at the time, but It was in the machine with plastic studs so it was not touching any metal. 

![images/04068d93b9c01e61e167f116c5e5f1d4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/04068d93b9c01e61e167f116c5e5f1d4.jpeg)


---
**Joe Alexander** *September 23, 2018 02:15*

if that was how it was wired when you blew it how did the A axis cap blow with nothing connected to it? and I see no nylon standoffs, the nylon spacers that i can see on the board mounted in the pic are useless if you use a metal screw through it...


---
**adstaggs** *September 23, 2018 02:41*

it had the nylon spacers and was mounted in the machine, this is a picture of it out of the machine but was the way the wiring was at the time. Everything was working with the board but when I would run lightburn it would stop in the middle of the job so lightburn support said I needed a 24v ps for the board only, so that is what I was doing at the time. 


---
**Joe Alexander** *September 23, 2018 03:33*

ah gotcha, either way there looks to be a substantial amount of damage done. Definitely not promising ;(


---
**adstaggs** *September 23, 2018 03:50*

so i guess my options are purchase another cohesion3d mini or look for another cheaper board. 


---
**Joe Alexander** *September 23, 2018 03:57*

probably but its hard to find a good one other than the C3D in that price range. I browse many K40 groups and it is by far the best option at this time. Even I use one.


---
**Jim Fong** *September 23, 2018 11:10*

Check the 24volt power supply output with a meter to see if it is in fact 24volts.  Don’t go blowing up a new board if the supply is faulty. 


---
**Raymond Zacharias** *September 25, 2018 21:12*

Make sure the stepper driver heat sinks are not touching any components on the board.  Mine were contacting components because the adhesive is flexible allowing the heat sink to float around on the chip.  I purchased some smaller sinks on ebay 6.35mm x 3.18mm  as they fit much better than the ones supplied.


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2018 22:21*

WOW.  Lots of good advice here, worth looking into, let's figure out how this happened. 


---
**adstaggs** *September 27, 2018 15:06*

MY power supply check OK , nothing was touching to cause it but I decided to purchase something cheaper, I know it not be as good but cost is a factor. Thanks for all the advice. 


---
*Imported from [Google+](https://plus.google.com/116460242075106049086/posts/2W8wVGvES1E) &mdash; content and formatting may not be reliable*
