---
layout: post
title: "Hi, Im at an impasse here and need to ask some questionssome will be stupidto make sure Im understanding what Im doing"
date: May 17, 2017 01:04
category: "C3D Mini Support"
author: "Steve Clark"
---
Hi, I’m at an impasse here and need to ask some questions…some will be stupid…to make sure I’m understanding what I’m doing. I am not seeing the answers in cncpro though they may be there and I just do not get it.



Details:



K40 – C3D – win10 –LW4



Just installed the C3D kit with looks good things all seem to work like they suppose to.



LaserWorks4 is up and running I just don’t seem to be.



My questions:



1)	What do I select for ‘machine ID’ ? Am I suppose to pick one of the defaults or create my own for the K40 conversion?



2)	The instructions don’t seem to have caught up with the new version 4. I'm not being critical and I understand it's a lot of work doing this stuff. There are settings I don’t quite understand.



 Example: The instructions on cncpro say that Travel Moves is a required set to 100mm/s however I cannot find that on the LW4 Settings page.  This area was a challenge and still is for me as I’m not sure I’m doing it correctly…



PWM MAX S VALUE 1 ( THIS IS STRIAGHT FORWARD)

CHECK-SIZE POWER    % (NOT SURE WHAT THIS MEANS)

TOOL TEST POWER  20 % (I BELIEVE I UNDERSTAND THIS)

TOOL TEST DURATION   5 MS (I’M NOT SURE WHAT THIS IS SUPPOSE TO MEAN YOUR OPTIONS SEEM TO BE 1-9 ONLY)



3)	I’m not understanding this relationship between the old ‘Current Knob’ and the LW Control Laser Test button. If I turn the knob to about 8 ma the LW does this…

 

"Server error: Writing to COM port (GetOverlappedResult): File not found Machine disconnected Machine disconnected No serial ports found on server!"



If it is at zero then nothing happens.







**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 01:19*

Fair point, about time I got that article caught up for LW4.  



1. I think people are just selecting the included k40 profile in LW4, at least for starters. 



2.  Go to all the links here: [http://cncpro.co/index.php/33-documentation/initial-configuration/laserweb-configuration](http://cncpro.co/index.php/33-documentation/initial-configuration/laserweb-configuration)



Then gCode settings page explains, at least somewhat, what that set of options you wrote above, are. 



3: There's nothing that turning the physical knob would have to do with the board <--> LaserWeb.  More likely than not you got a communications disconnect for whatever unrelated reason at the same time.   

What happens is you set the power max on the knob, say 10mA, and then you set a proportion of that with the % power in LW when you are setting up your job.  This ensure you have the greatest grayscale range when doing engraving. 


---
**Steve Clark** *May 17, 2017 02:27*

Thanks for the quick response Ray. I not knowledgeable enough to understand which of the options listed in Machine ID is the K40?



Options:



Generic GRBL



Generic Smoothie Machine



Emblaser



Fabkit



 I could guess it to be the Smoothie? I just don’t know for sure. 



I did print out and use the link you mentioned but without a general knowledge background in this stuff I’m never sure that I’m inputting the right information unless  it's a near duplicate format.




---
**Steve Clark** *May 17, 2017 02:35*

BTW Ray I just realized I may be asking you questions that should be on the LaserWorks site. I got hung up on the Laser test button. There is no way I can get the current knob above 8 or 9 ma without the error message and condition I displayed above. I've tried at least 6 or 7 times and the same condition occurs.


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 02:40*

Which laser test button? The test fire one in LaserWeb needs some of those options you mentioned configured before it will work. 



So if you literally turn the knob up and see LW disconnect I'd investigate the wiring and psu to make sure something isn't screwed up. 


---
**Steve Clark** *May 17, 2017 03:07*

The one in LW  under ‘control’  and ‘test laser’ is what I am firing. The old ‘current knob’ on the k40 is what I am setting  ma with.

Again, my lack of knowledge what is "psu"? The power supply unit? 



![images/c8e32338aa26354da725f4ddca4bd915.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8e32338aa26354da725f4ddca4bd915.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 03:46*

Yes, that button in LW is what I referred to in above explanation. 

Yes, power supply. 

Your board is wired properly. 



If increasing the knob causes you to experience symptoms, then you should investigate. I would write a description about that and post it in the k40 group for the knowledgeable folks such as Don to advise you. 


---
**Steve Clark** *May 17, 2017 04:17*

Thanks Ron I'll do that. Before I go what Machine ID do I use starting out?



Generic GRBL



Generic Smoothie Machine



Emblaser



Fabkit






---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 04:17*

Smoothie would be the place to start. 


---
**Joe Alexander** *May 17, 2017 04:32*

note that those profiles are only rough presets made by the developers. They affect machine size, beam diameter, etc. I started with smoothie but then created my own and saved my settings that way. kinda like a macro profile :) I use the following settings:

machine size 320 x 220

beam diameter 0.1

homing g28.2

test power 50%

test duration 25ms

pwm 1

artwork load top left (looks like a dice showing five)

soon ill be adding a relay for ais assist then ill add the gcode to that field also. I would try similar settings and maybe a different or fresh gcode file. Hope this helps!


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 04:33*

Our config requires that you set size as 300x200. Everything else is negotiable. 


---
**Steve Clark** *May 17, 2017 04:47*

**+Ray Kholodovsky** Thanks


---
**Steve Clark** *May 17, 2017 06:23*

Thanks Joe. I'll compare those with what I have and adjust. That should give me a starting point.






---
**Steve Clark** *May 17, 2017 17:14*

I get replacing the USB cable. If that doesn’t solve the problem what first would you look at to shield? The power cable?


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/5i1LXbSLbtL) &mdash; content and formatting may not be reliable*
