---
layout: post
title: "Ray Kholodovsky after having two more halts tonight on stupid little jobs I decided to try cutting the usb pwr pads on the underside"
date: April 25, 2017 21:18
category: "C3D Mini Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky** after having two more halts tonight on stupid little jobs I decided to try cutting the usb pwr pads on the underside. I now have no display on the lcd and although the board connects to LW4 I can not home or run any jobs.



It's late here now so I'll resolder it tomorrow but can you see any reason I would have this problem other than by cutting the pads I've lost power to the sd card and therefor the system can not start up hence the back light on the display but no actual information.





**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 21:20*

Well don't solder it back just yet. Turn on the laser. 

Does the red led VMOT on the board turn on? 

Now how about the green LEDS below it, what do they do? 


---
**Andy Shilling** *April 25, 2017 21:25*

Just checked, the red led is on and if I'm seeing it right the there is a green next to it on as well then two rapid flashing and a final green solid. The sd also now doesn't mount.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 21:39*

Does the backlight of the LCD turn on? So it's just no text on it?


---
**Andy Shilling** *April 25, 2017 21:44*

yes backlight on and that's it I've unplugged it and swapped the leads around as I had to unplug it to cut the trace/pad.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 21:45*

Can you get a clear picture of the back of the PCB area in question and also indicate from where to where you made the cut? 


---
**Andy Shilling** *April 25, 2017 21:49*

Not at the moment as workshop is all locked up but it was the pad that looks a little like an H with USB PWR printed above it, I just cut through the narrowest point and tested for continuity before refitting.

 


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 21:51*

That is the idea... 

I don't want to alarm you but right above that (in blue territory) are the traces for the sd card so if you cut too far up that would explain the issues you are having. I would say solder it back and see if that fixes anything. 


---
**Andy Shilling** *April 25, 2017 21:54*

I will get on it tomorrow when I'm home from work and report back, I'm sure I only cut the pad but I take it if I did cut the trace there are points I could resolder to correct this?


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 21:55*

For the USB PWR, it is a solder jumper so you would resolder the pads (sides of the H, if you will) to each other with a solder blob connecting them. 


---
**Andy Shilling** *April 25, 2017 22:01*

Sorry I meant the traces if I have caught them.




---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 22:11*

The SD traces - I do not see any way to repair those, no. 


---
**Andy Shilling** *April 25, 2017 22:12*

Ok here's hoping, fill you in tomorrow.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 22:16*

Best of luck, fingers crossed. 


---
**Andy Shilling** *April 26, 2017 21:26*

**+Ray Kholodovsky**​ well what a fun evening. I bet nobody could think of any more names than I called myself when I realised I'd cut other traces. I've just finished repairing it and once the hot glue sets over the solder joints I'll go out and test it.



Note to all be very careful when cutting bits on a pcb😱


---
**E Caswell** *April 26, 2017 21:49*

**+Andy Shilling** that's why I said cut the cable and disconnect the VCC.. at least it would have been a simple cable change :-(


---
**Andy Shilling** *April 26, 2017 21:53*

O well it's turned out ok just meant I needed to polish up on my soldering skills. 



Everything is back up and running and I now have no power going in via usb but if after all this I still get the halt I'm going to cry lol.


---
**E Caswell** *April 26, 2017 22:04*

**+Andy Shilling** I'm confident it will be good Andy, either that or your soldering skills ain't what they should be. :-)  

only thing to remember is to make sure windows 10'takes no action when it connects.. 


---
**Andy Shilling** *April 26, 2017 22:06*

**+PopsE Cas** Thanks I did see there is a script on mac that can stop the card mounting I wonder if there is a similar thing for windows?




---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2017 22:09*

There's a firmware build for not mounting the MicroSD Card... Combined with setting the relevant (MSD) line in config to true...   But one thing at a time... I prefer having the CNC firmware as is. 


---
**Andy Shilling** *April 26, 2017 22:10*

I'll stick with whats working at the moment especially as I couldn't get the script to work on my mac anyway. See I break everything I touch.


---
**E Caswell** *May 09, 2017 10:34*

**+Andy Shilling** Would be interested to see if you found that this worked? any up-date?




---
**Andy Shilling** *May 09, 2017 15:28*

**+PopsE Cas**​ it seems that after cutting tracks and having to do the repair I am now able to do a full job without any halts. Thanks to you and the people that helped figure this out your end, I'm now running the laser through a raspberry pi server and actually getting time to finish complete jobs.


---
**E Caswell** *May 10, 2017 09:11*

**+Andy Shilling** brilliant. I now have a "Data" only cable connected and the 3.0 USB Pcie card and get no drop outs even with the big band saw stopping and starting. Even the dust extractor will run now. :-) **+Ray Kholodovsky** is it worth capturing this somewhere Ray?  May not be a common problem but at least with the ones that are having it can at least try one of these options.


---
**Andy Shilling** *May 10, 2017 09:36*

Great news buddy, I'm now wondering if we are the first two in the uk to have these boards and there is some issue tied in to our 240v and ground supplies the board doesn't like. **+Ray Kholodovsky**​ is that possible?


---
**E Caswell** *May 10, 2017 10:13*

**+Andy Shilling** **+Ray Kholodovsky**  After speaking to the retired electronics engineer he said it was a common problem when he saw the importing of the IBM machines from USA, they had many problems with EMF interference and had to add the Caps to improve stability. That was before USB had even been developed. it was amazing as he went straight to the problem and knew it existed. He thought it was common knowledge from board manufacturers and had been resolved.

Maybe something Ray needs to look at in the next development stage as I'm sure we aren't going to be the only ones using 240v 

I have e-mailed all this info through to Ray o I'm sure he is aware of it.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/ZsgKRhZWSMP) &mdash; content and formatting may not be reliable*
