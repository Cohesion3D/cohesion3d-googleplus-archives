---
layout: post
title: "Just want to know if I have changed the file correct for way this is wired"
date: January 31, 2017 18:50
category: "General Discussion"
author: "Colin Rowe"
---
Just want to know if I have changed the file correct for way this is wired.

Picture is not of my laser, just someone who has wired the same.





![images/67382376ebb1d79c13e577306b839a5d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/67382376ebb1d79c13e577306b839a5d.png)
![images/0bfec4179be0b29bf2ba15275c7f7827.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0bfec4179be0b29bf2ba15275c7f7827.png)

**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 18:52*

Remove the exclamation.  Should be just 2.5


---
**Colin Rowe** *January 31, 2017 19:12*

OK thanks, do i leave laserfire output pin at 2.6?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 19:13*

Yeah you can leave that. It's no longer being used though, in this wiring type. 


---
**Colin Rowe** *January 31, 2017 19:19*

great stuff, hope to do a quick test tomorrow




---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/FFb8Z1wGGFD) &mdash; content and formatting may not be reliable*
