---
layout: post
title: "Hi Guys I am in the process of switching over to C3D i have an external driver and power supply"
date: July 31, 2018 17:09
category: "C3D Mini Support"
author: "John Camfield"
---
Hi Guys

I am in the process of switching over to C3D i have an external driver and power supply. My question is what do i have to do to the firmware? on the web site it says about the con fig for a z table and if your doing both, i only want to use a rotary, so do i just do the very last bit on con fig "a". I am not the best with changing the files or con fig set up. Any help would be great. 





**"John Camfield"**

---
---
**Tech Bravo (Tech BravoTN)** *July 31, 2018 17:14*

and i am assuming that you have followed, to a tee, the documentation provided here: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**John Camfield** *July 31, 2018 17:16*

Hi



OK so will have to get an adaptor for the micro card. Before I can do anything else.

Thanks for the fast reply





Sent from Mail for Windows 10


---
**Tech Bravo (Tech BravoTN)** *July 31, 2018 17:23*

The best way is to set up as if you were using a z and a rotary. then just connect the rotary. i can break down the directions. you will need to be able to read/write a microSD card directly from your computer using a reader and/or adapter.


---
**Tech Bravo (Tech BravoTN)** *July 31, 2018 17:51*

i have a step by step article prepared. i am getting it verified for accuracy and then i will post it for you shortly. i hopefully made it quick and painless :)


---
**John Camfield** *July 31, 2018 18:01*

Your a star!! Would be a nightmare me trying to mess with con fig files.





Sent from Mail for Windows 10


---
**Tech Bravo (Tech BravoTN)** *July 31, 2018 18:09*

Here ya go: [lasergods.com - Configuring 4 Axis Firmware & Config File On The Cohesion3D Mini](https://www.lasergods.com/configuring-4-axis-firmware-config-file-on-the-cohesion3d-mini/)


---
**John Camfield** *July 31, 2018 19:43*

Thanks that’s great so I just copy the 3 files. That’s so much better, thanks again for your help.



Sent from Mail for Windows 10


---
*Imported from [Google+](https://plus.google.com/103234827681964755838/posts/TuDmaW2Neoa) &mdash; content and formatting may not be reliable*
