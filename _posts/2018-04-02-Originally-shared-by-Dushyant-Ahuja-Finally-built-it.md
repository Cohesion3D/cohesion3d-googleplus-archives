---
layout: post
title: "Originally shared by Dushyant Ahuja Finally built it"
date: April 02, 2018 20:30
category: "Show and Tell"
author: "Dushyant Ahuja"
---
<b>Originally shared by Dushyant Ahuja</b>



Finally built it. Inspired by Bruce Shapiro's Sisyphus Table ([http://www.sisyphus-industries.com/](http://www.sisyphus-industries.com/))

Absolutely fell in love with it when we saw it in Maker Faire New York in 2016. 

Couldn't have happened without **+Ray Kholodovsky**'s **+Cohesion3D** Mini (that he sent to me when he heard about the idea). Instructable to come. 



[https://photos.app.goo.gl/DhEPv5P0F3c7Wfrl2](https://photos.app.goo.gl/DhEPv5P0F3c7Wfrl2)




**Video content missing for image https://lh3.googleusercontent.com/-eTQM3xhym_A/WsKSj2VQX_I/AAAAAAABlUs/38amW3b8DWMESAYjV-h2fI_BiMeknKVvwCJoC/s0/VID_20180402_211640.mp4**
![images/f31a1e26e0e03d6676aca56ee226fe8f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f31a1e26e0e03d6676aca56ee226fe8f.jpeg)



**"Dushyant Ahuja"**

---


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/LzGZR8PKv7x) &mdash; content and formatting may not be reliable*
