---
layout: post
title: "Have HM Rotary installed on the A Port of the Cohesion Mini board"
date: January 09, 2019 12:42
category: "FirmWare and Config."
author: "Phillip Bell"
---
Have HM Rotary installed on the A Port of the Cohesion Mini board. Issued G0 A10 nothing happens. Have installed on the micro card firmware for 4 axes and a config.txt for the same additional axes. I tried unsuccessfully to see if Lightburn would display the axes it sees like XYZA.  Issued M114  in console of Lightburn and it shows only XYZ, no A . The Rotary has been proven good. Does the M114 report the A port if active? Do I need a heat sink plugged into the Cohesion board A port? 





**"Phillip Bell"**

---
---
**Phillip Bell** *January 09, 2019 14:03*

When I installed the new 4 axes firmware and config.txt I did it on a new 32 gb memory chip. The original was a 4 gb chip. After the Cohesion Mini boots it displays my 2 files in a browser. Didn't do that with the original files. Does the Cohesion Mini have a limit on the size of the memory chip and could that make possible for my system not to recognize use the files? The firmware.bin is changed to FIRMWARE.CUR.  So it sees something.....I'm about read to go outside and beat my head on the sidewalk to see if that would fix it.


---
**Phillip Bell** *January 09, 2019 15:05*

What does it mean to "Flash the config.txt firmware.bin files" ?  I have formatted my 32 gb sdcard to FAT32 and have copied the precompiled versions of the 4 axes config.txt and firmware.bin to it. Is there more to be done? Flash?


---
**Phillip Bell** *January 09, 2019 18:14*

How to find out if the A port on the Cohesion Mini board is bad. I don't think it is being seen by anything..... nothing port A connected works. What is plugged in has been proved good. Just not when you plug into the A Port. 


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 18:18*

Have you followed the rotary instructions exactly? 



Show pictures of your board and wiring. 


---
**Phillip Bell** *January 09, 2019 21:27*

**+Ray Kholodovsky** Yes


---
**Phillip Bell** *January 09, 2019 21:35*

**+Ray Kholodovsky** 

![images/83b3ccf8afc75b2ef65eee3f7a8d6f6d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/83b3ccf8afc75b2ef65eee3f7a8d6f6d.jpeg)


---
**Phillip Bell** *January 09, 2019 21:43*

![images/ff41eb9793e3bdbf9700657db0c66e18.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ff41eb9793e3bdbf9700657db0c66e18.jpeg)


---
**Phillip Bell** *January 09, 2019 21:45*

![images/47bcc4512150f25120f11004a22cf947.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/47bcc4512150f25120f11004a22cf947.jpeg)


---
**Phillip Bell** *January 09, 2019 21:46*

The connector plugging to the A port came from the HM Rotary shown in pictures


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 21:47*

According to your pictures you have not followed the instructions.




---
**Phillip Bell** *January 09, 2019 22:16*

And how is that? 




---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 22:20*

Our guide on how to wire the rotary is linked at [cohesion3d.com - Getting Started - Cohesion3D](http://cohesion3d.com/start)




---
**Phillip Bell** *January 09, 2019 22:21*

I cannot prove that the A port even exists. ALl the wire to the exterior port is useless if the port cannot be see or recognized. I have wired all those devices for an exterior port. The are not installed to the mini because the rotary is supposed to run without  all that. It should at least show up as something recognizable. The rotary should turn as shown if I a G0 A100 command from the lightburn console. It does not. 


---
**Phillip Bell** *January 09, 2019 22:27*

Did you read what I have written? I had questions that I would like to have answered. I would like to prove the mini board is not crapped out. X Y and Z seem to work ok. How CAN I PROVE THAT THE A PORT IS OK?


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 23:04*

Since all the steppers that spin have a driver plugged in next to them that helps the spinning, I'm not sure why you expect the rotary motor to spin without any driver hooked up to the A port. Again, the way to do that is shown in that guide I linked you above. You currently have nothing plugged into the A port. 



And we test all the boards before shipping, so it all worked when it left here. 


---
**Tech Bravo (Tech BravoTN)** *January 09, 2019 23:56*

I see at least 2 things from the images.



First, though it is not rotary related, the heatsink on one of the on-board drivers is crooked and can short down the driver. It needs to be straightened.



 Second, it looks like you have the stepper motor on the rotary connected to a header on the c3d mini. You need a driver to drive the motor.



Your x and y axes have on-board drivers (the things with the heatsinks). 



Then on the z axis header you have an external stepper driver adapter (which is used to interface an external driver module)



So randy's rotary has a big solid stepper motor and an external driver is needed as well as an external 24v psu (if not already in place). The k40 psu cannot reliably drive all of that equipment.



The way it works is: the external stepper driver adapter goes in the a header, then it is wired to an external stepper driver, and the rotary stepper motor is connected to the driver.



This is covered in great detail in the z axis/rotary documentation.



Additionally, the smoothie firmware for the mini was changed quite a while ago to disable windows mass storage (meaning you should not be able to access the files on the card without inserting the sd card into the computer directly). 


---
**Jim Fong** *January 10, 2019 03:10*

To answer your original question, 

you need to type in M114.3 to include the position for the A axis.



Typing version in the console will show what smoothieware version is running.  It should say CNC BUILD 4 axis.  



You need a stepper driver plugged into the A port.  






---
*Imported from [Google+](https://plus.google.com/110671692842123504744/posts/G2Cw1dS9mWe) &mdash; content and formatting may not be reliable*
