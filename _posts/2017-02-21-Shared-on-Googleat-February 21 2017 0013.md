---
layout: post
title: "Shared on February 21, 2017 00:13...\n"
date: February 21, 2017 00:13
category: "FirmWare and Config."
author: "Kostas Filosofou"
---






**"Kostas Filosofou"**

---
---
**Kostas Filosofou** *February 21, 2017 00:34*

So as for the drivers I ment 0.5v not 5v ... my false. 



For the rest I will not continue .. I just hate the smart guy's...But not hard feelings.


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 00:48*

Right. So what I was saying earlier - need to see your gcode tab in settings, and what beam diameter you are using. 


---
**Kostas Filosofou** *February 21, 2017 00:57*

**+Ray Kholodovsky** ok give me a few hours please when i get back to home I send you the gcode.


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 00:59*

gCode Tab in Settings.  A Screenshot of this.  Please do not send me your actual gcode output/ file.


---
**Kostas Filosofou** *February 21, 2017 01:03*

**+Ray Kholodovsky** :) yes I got it..


---
**Kostas Filosofou** *February 21, 2017 17:34*

**+Ray Kholodovsky**  I make some progress .. i sent you the info you asked and below my progress .. i play a little with the settings and with the laser beam diameter (and some tuning with the drivers) and i get better results then.. 

![images/7dd0af95e634030161950e24a552bcf9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7dd0af95e634030161950e24a552bcf9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 17:35*

Ok. Laser beam diameter is smaller than we usually see. We usually see 0.1 to 0.5 range. 


---
**Kostas Filosofou** *February 21, 2017 17:38*

Second Try 

Laser Beam : 0.14

Light :400mm/s

Dark: 300mm/s

Min: 10%  

Max: 60%

300dpi



![images/7eaf06ab3592c4506330888a05798f9b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7eaf06ab3592c4506330888a05798f9b.jpeg)


---
**Kostas Filosofou** *February 21, 2017 17:39*

Third Try 

Laser Beam : 0.15

Light :700mm/s

Dark: 650mm/s

Min: 10%  

Max: 40%

300dpi



![images/61940205bd9069263c286f2960cd2222.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/61940205bd9069263c286f2960cd2222.jpeg)


---
**Kostas Filosofou** *February 21, 2017 17:40*

fourth Try 

Laser Beam : 0.15

Light :700mm/s

Dark: 900mm/s

Min: 10%  

Max: 40%

300dpi



![images/57e270af1788c5a1a5798d9ecaf20030.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57e270af1788c5a1a5798d9ecaf20030.jpeg)


---
**Kostas Filosofou** *February 21, 2017 17:40*

![images/334289a36b41d3cd70503223a8e5c865.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/334289a36b41d3cd70503223a8e5c865.jpeg)


---
**Kostas Filosofou** *February 21, 2017 17:44*

the lower that gets in the piece is getting darker.. that is alignment problem  ?


---
**Kostas Filosofou** *February 21, 2017 17:52*

**+Ray Kholodovsky**​ and a last thing after a while I start the engraving my display turned to Chinese ... Have you any idea why?

![images/1df8e39e620ab76ccdb7cdffc1221b26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1df8e39e620ab76ccdb7cdffc1221b26.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 17:58*

Alignment, material surface uneven, any number of issues. 



Just press the click button once or twice and it should go back to normal... It's scrambled from static. Maybe wrap the LCD cables in foil or something.  


---
**Kostas Filosofou** *February 21, 2017 18:03*

**+Ray Kholodovsky** ok thank you very much.. I look to it for the rest..


---
**Ronald Whittington** *March 26, 2017 17:34*

My GLCD screen is often scrambled on boot up, like the one shown here, two thick lines or random LCD on/offs..not one letter or symbol, more like a bar code.

Sometimes I have to boot the machine several times, main power or USB power makes no differnce, so I wondered about Firmware, I am running Smoothie that came installed on the card, but also see that the firmware cnc-bin is mentioned a lot, should I upgrade or update?




---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/JRg8iGgw6a5) &mdash; content and formatting may not be reliable*
