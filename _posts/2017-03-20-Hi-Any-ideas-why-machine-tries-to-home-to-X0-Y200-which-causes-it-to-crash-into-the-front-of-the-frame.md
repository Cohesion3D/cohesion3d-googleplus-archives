---
layout: post
title: "Hi! Any ideas why machine tries to home to X0 Y200 which causes it to crash into the front of the frame?"
date: March 20, 2017 18:26
category: "K40 and other Lasers"
author: "Brad Reed"
---
Hi! 



Any ideas why machine tries to home to X0 Y200 which causes it to crash into the front of the frame? Even If I move it to X0 Y0 using the LCD this happens. I'm using the C3D mini and the K40.



Also, what acceleration settings are you all using?



Thanks for the response on my last post (I can't seem to reply to it?)



Brad





**"Brad Reed"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 18:39*

The Y endstop is at the back of the machine, and the origin 0,0 is the front left.  Thus homing is to the rear, and it would be at the 0, 200 position when this happens.  This is all proper.  



Assuming you haven't messed with the endstops/ homing section of the config, then you need to power down and flip your Y cable. 

[dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)


---
**Brad Reed** *March 20, 2017 20:29*

Oh no worries.



My machine just caught fire and destroyed my room...



I'm absolutely gutted




---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 20:35*

What happened? 


---
**Brad Reed** *March 20, 2017 21:13*

It had finished cutting so I turned it off and went to get tea..



Got back to a distraught family and a fire damaged room.



The embers must have caught light and set the orange polycarbonate on fire, then God knows what happened.



The whole room needs redecorating. 





[https://plus.google.com/photos/...](https://profiles.google.com/photos/105285651935261086449/albums/6399692019104865441/6399692023027301442)


---
**Brad Reed** *March 20, 2017 21:13*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/105285651935261086449/albums/6399692128235932721/6399692126929321426)


---
**Brad Reed** *March 20, 2017 21:14*

The black smoke got everywhere

[https://plus.google.com/photos/...](https://profiles.google.com/photos/105285651935261086449/albums/6399692283583590273/6399692287115267186)


---
**Brad Reed** *March 20, 2017 21:14*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/105285651935261086449/albums/6399692385805066353/6399692388270577314)


---
**Brad Reed** *March 20, 2017 21:15*

All of the electronics survived though so I will be selling them soon 


---
*Imported from [Google+](https://plus.google.com/105285651935261086449/posts/7n5guT71r7L) &mdash; content and formatting may not be reliable*
