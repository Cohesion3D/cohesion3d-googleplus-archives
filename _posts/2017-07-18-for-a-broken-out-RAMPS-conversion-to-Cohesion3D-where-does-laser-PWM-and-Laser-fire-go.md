---
layout: post
title: "for a broken out RAMPS conversion to Cohesion3D, where does laser PWM and Laser fire go?"
date: July 18, 2017 19:06
category: "C3D Mini Support"
author: "timne0"
---
for a broken out RAMPS conversion to Cohesion3D, where does laser PWM and Laser fire go?



On the ribbon cable, where does ribbon cable 6 (GND) and 3 (+5v) go?



Other than that... I'm pretty sure I've got it wired right.  Assuming 2B for X goes next to the X...

![images/20f5f2de4ecce266eb9c50604c5de6da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/20f5f2de4ecce266eb9c50604c5de6da.jpeg)



**"timne0"**

---
---
**timne0** *July 18, 2017 19:09*

I'm assuming I'm supposed to put pin 5 and 4 on their respective signal cables.l and not 5v

![images/abab5c4d7224c1345c95e29fc814b58f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/abab5c4d7224c1345c95e29fc814b58f.jpeg)


---
**timne0** *July 18, 2017 20:08*

OK so looking at this I don't need to connect the "laser fire" pin off the power supply, and reading posts down here, PWM is not done through the PWM pin, it's done through the potentiometer which is set at 85%? of power (about 18ma).  Still trying to work out where 6 and 3 go


---
**Griffin Paquette** *July 18, 2017 20:44*

Have you checked out the C3D mini's pin breakout schematic?


---
**timne0** *July 18, 2017 20:45*

Hi Griffin, I found and got most things connected up... I think I've got 6 and 3 in the right places because I'm already lasering things, but at 100% power it seems...


---
**timne0** *July 18, 2017 21:01*

Does the Laser PWM pin goto 2.4 (center)?




---
**timne0** *July 18, 2017 21:09*

mine is a two wire configuration.  It requires a signal from the board to modulate the PWM.  Or it did on ramps.


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2017 21:34*

We can do 2 wire like you have but the default way is the 1 wire.  It requires the pot to be connected to IN as it was originally (in your diagram this is Gnd, LaserPWM, and 5v on the LaserPSU). And the actual pulsing is from the boards' 2.5 - screw terminal (bottom row 4th from the left) to L on the PSU.  



Also run 24v and Gnd to the Main Power in at top left screw terminal. 



Set the pot at like 10-13mA and then run G1 X10 S0.4 F600 lines with different S values 0-1 (and change the x value so it'll move each time) and hopefully observe different burn levels on the material and different readings on the mA meter. 



If not, please show pictures of wiring and other details about your setup. 


---
**timne0** *July 18, 2017 22:08*

OK I have rewired the power, but I didn't notice any difference in the power levels.  I've increased the laser_module_pwm_period   to 400 and there's a small difference between 0.1 and 1 of about 4ma.  How high should I set the PWM period? until I'm getting 1ma at 0.1?




---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2017 22:10*

400 is usually the most it takes to start seeing the pwm performance. 



Can we see pics of your wiring? 


---
**timne0** *July 18, 2017 22:12*

I've increased it to 600 and there's a definite drop to 4ma at 0.1.  The pot is definitely misbehaving because it's been a bugger to set it at 10ma


---
**timne0** *July 18, 2017 22:13*

![images/dbb8b93e5464a98685e4efbd0221ea3e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dbb8b93e5464a98685e4efbd0221ea3e.jpeg)


---
**timne0** *July 18, 2017 22:13*

It's a bleeding state due to the ribbon cable break out which was done about a year and half ago. It works, but it's not pretty. 


---
**timne0** *July 18, 2017 22:15*

should I set the pot to 100% now and leave it at 400s?


---
**Claudio Prezzi** *July 19, 2017 09:48*

You should set the pot to the power you need to get the darkest point in your engraving with 100% pwm. Basically you use the pot to limit the power, so the pwm can use it's maximum dynamic range from 0-100%.


---
**timne0** *July 19, 2017 12:22*

**+Claudio Prezzi** Will do, I saw the software limit of 80% power, I don't want to damage the tube so I'll set at 85% and see what sort of current I pull first.  I may still need to change the setting for the 400 to 600 to get enough of a difference between greys.


---
**timne0** *July 19, 2017 19:33*

Cannot get it to control power via laserweb.  Can't even get laser web to do rastering... any idea what's going wrong here? image is 360dpi

![images/e693cbdd335f01b7defcd0220be5a3c6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e693cbdd335f01b7defcd0220be5a3c6.png)


---
**Claudio Prezzi** *July 20, 2017 08:30*

Don't use the "Filters/Trace" button (trace will convert your image to b/w vector), just select the image and click "Create Single", which should generate a "Laser Raster" operation, then set the params and click "Generate".


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/M6TkvxkrkAK) &mdash; content and formatting may not be reliable*
