---
layout: post
title: "I bought the digital panel version of the K40, which does not come with the analog ammeter"
date: March 19, 2017 03:23
category: "C3D Mini Support"
author: "Bob Buechler"
---
I bought the digital panel version of the K40, which does not come with the analog ammeter. I'm realizing now that I have a basically functional system that I really need to see this data to know if I'm overdriving my tube.



Before I just blindly buy an analog ammeter off Amazon and install it, I thought I should ask here if the C3D Mini is already monitoring this, perhaps? And if not, can it, and if so, what's the best way to integrate it?









**"Bob Buechler"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2017 03:28*

The board just sends the pwm signal per its calculations (rasters, throttle down making sure corners aren't overburned). 



It doesn't know what the pot/ digital panel is doing.  



**+Don Kleinschnitz** has at some point covered pot replacement with a nice high quality.  



I would also say this:  I do include the PWM cable for replacing the pot  and a bunch of us used to run machines this way.  Maybe it's worth giving a shot, and let us know how it turns out.   It would be essentially wiring that cable, and a few tweaks to the config.  


---
**Don Kleinschnitz Jr.** *March 19, 2017 03:37*

The analog miliampmeter can and should be added as a separate item and circuit. 



The digital power control can be kept.



Although the voltage is low on the cathode side I would not connect it into the controller.

You could also use a digital miliampmeter but I like the analog as it's easy to see small and rapid changes.


---
**Bob Buechler** *March 19, 2017 05:38*

Thanks **+Ray Kholodovsky**​ and **+Don Kleinschnitz**​. I've gone ahead and ordered an analog ammeter and won't integrate it with the controller.



Don, can you point me in the direction of the pot replacement docs Ray mentioned?


---
**Don Kleinschnitz Jr.** *March 19, 2017 13:39*

**+Bob Buechler** bookmark this site as it may answer future questions:

[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



The specific post is here: [http://donsthings.blogspot.com/search/label/K40%20Current%20Calibration%20Pot](http://donsthings.blogspot.com/search/label/K40%20Current%20Calibration%20Pot)



I ordered this meter for my HV lab but have not installed it yet. 



[https://www.amazon.com/gp/product/B0050GIN2Q/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B0050GIN2Q/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)


---
**Bob Buechler** *March 19, 2017 16:43*

**+Don Kleinschnitz**​​ On your blog you state the following:



"The ammeter must be connected to the negative electrode of laser tube. When it is working under over high current for long term, the negative pole will appear light yellow and the life span will be shortened rapidly."



Does this mean I should just connect the ammeter in-line on the negative (black) wire running between the tube and the PSU?



"To protect dust from going into the insulation sheath, please wrap it with plastic film."



Which insulation sheath?




---
**Don Kleinschnitz Jr.** *March 19, 2017 17:13*

Can you give me a link to my post? :)



In any case the milli-amp meter should be in series with the tubes cathode and the L pin on the power supply's AC connector. Make sure it is on the leftmost connector (L-) 


---
**Bob Buechler** *March 19, 2017 17:47*

**+Don Kleinschnitz** [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search?q=Ammeter+) then search the page for 'ammeter'


---
**Don Kleinschnitz Jr.** *March 19, 2017 19:55*

That passage is from Raci's installation instructions. The sheith is the connection to the cathode. You won't be taking that off.


---
**Bob Buechler** *March 20, 2017 05:51*

**+Don Kleinschnitz** ahh. I was searching for ammeter installation instructions before asking in the group. Know of any, by chance?


---
**Don Kleinschnitz Jr.** *March 20, 2017 12:20*

**+Bob Buechler** I don't have one on my blog but after you verify  these instructions twork I will move it there.....



....................................

.... Adding a milliamp meter to your machine.....



Adding a milliamp laser current meter to your machine is easy. Typically the lasers cathode is wired to the laser power supplies L- pin and this meter will be placed in series with that wire.

....................

1. Pre-check:

1.1 Verify that with the laser enabled when you push the test function the laser fires.

1.2 Power down and unplug the mains from your machine

2. Find the wire that connects to the cathode (the end the light comes out of) end of the laser tube. Often its a black or green wire. 

3. Trace that wire to its other end which should be connected to ground at the laser power supply (LPS). Note: do not simply ground the cathode or the () side of the meter to the frame. The lasers current must return to the LPS itself. 

4. Find the lasers ground port on the LPS. Its usually called L and its the leftmost connection on the leftmost connector.  Do not confuse this with the L that is on the rightmost connector with the DC voltages.

5. Remove the existing wire from L- and verify with an ohmmeter that that the pin (L-) is connected to the FG pin in the LPS and that both of those pins (L- & FG) are connected to the frame of the machine.  There should be close to 0 resistance to ground (frame) on these pins. Note: this is a good time to test that there is 0 ohms to GND at the frame pin on the mains connector.

6. We want to reroute the existing wiring (that went from the tubes cathode to the LPS) to the (+) side of the meter. Do such by pulling the L- end of the wire that connects to LPS out of the harness enough to reroute it to the + side of the meters terminals. Note: Unless absolutely necessary do NOT disconnect the wire from the cathode as that is a protracted process to replace.  When this step is finished the wire that previously was routed from the cathode to the L- is now rerouted to the + side of the meter.

7. Connect the wire of step #6 to the meters + terminal with an appropriate terminating terminal. Usually the meter has threaded studs with nuts and washers. I recommend using a ring tongue terminal soldered to the wire. 

[https://www.amazon.com/50pcs-RV1-25-6-Tongue-Insulated-Terminals/dp/B00K85BR7C/ref=sr_1_2?ie=UTF8&qid=1490010283&sr=8-2&keywords=ring+tongue+terminal](https://www.amazon.com/50pcs-RV1-25-6-Tongue-Insulated-Terminals/dp/B00K85BR7C/ref=sr_1_2?ie=UTF8&qid=1490010283&sr=8-2&keywords=ring+tongue+terminal).



It is common for the meters terminals not to be marked. If not marked start by connecting this wire to the left terminal of the meter (looking from the back), its a guess.

8. Get a new piece of wire that is long enough to route back to the LPS (L-) pin from the () of the meter. 

8.1 Use the same size or larger wire (yes it matters) and the same color if possible (colors do not matter but will be easier to trace later). 8.2 Connect this wire from the meters () terminal to the L- of the LPS.  8.3 Terminate the meter end with a ring tongue like step #7. For the  LPS end use a crimped pin of the correct wire size:

 [https://www.amazon.com/1-0mm2-Insulated-Cord-Ferrules-Terminal/dp/B0143YOTZ0/ref=pd_sim_469_3?_encoding=UTF8&pd_rd_i=B0143YOTZ0&pd_rd_r=EPA6TWHHQ8PTQB1FSW0Z&pd_rd_w=bWjE0&pd_rd_wg=YGjbv&psc=1&refRID=EPA6TWHHQ8PTQB1FSW0Z](https://www.amazon.com/1-0mm2-Insulated-Cord-Ferrules-Terminal/dp/B0143YOTZ0/ref=pd_sim_469_3?_encoding=UTF8&pd_rd_i=B0143YOTZ0&pd_rd_r=EPA6TWHHQ8PTQB1FSW0Z&pd_rd_w=bWjE0&pd_rd_wg=YGjbv&psc=1&refRID=EPA6TWHHQ8PTQB1FSW0Z)



If you do not have the ability to crimp a pin at least strip back and tin the wire with solder. Insert the wire into the L- terminal and tighten securely. 

9. After insuring that you have not shorted anything with shards of wire etc prepare to return power to the machine. As a rule I vacuum my machine in the area I have been working with a crevice tool. Be careful not to create a static charge.

10. Return power to the machine with your hand on the switch in case of smoke. No smoke? Then proceed.

11. Turn the power adjustment pot (or digital control) to about 1/3 or less of its range. In case the meter is in backward we do not want to stress it. 

12. To test the meter enable the laser and then push the test switch while watching for movement in the meters needle. The meter should read the lasers current and you are done.

13. If no movement is noticeable on the meter these things could be wrong:

13.1 An error in the wiring, recheck using the steps above.

13.2 The meter is in backward. Swap the wires on the back of the meter and return to step to step 10.

13.3 The laser is not firing, check to see if the tube ionizes.

13.4 If you cannot get it to work post a picture of all of the above connections and wiring with my G+ address.



[amazon.com - Amazon.com: 50pcs RV1.25-6 Ring Tongue Type Insulated Terminals Red for AWG 22-16: Home Improvement](https://www.amazon.com/50pcs-RV1-25-6-Tongue-Insulated-Terminals/dp/B00K85BR7C/ref=sr_1_2?ie=UTF8&qid=1490010283&sr=8-2&keywords=ring+tongue+terminal)


---
**Bob Buechler** *March 23, 2017 17:44*

I'm a visual thinker, so to try and validate my understanding of this procedure, I started by making a crude diagram of the before and after. If anything here is fundamentally wrong, I'd like to correct it first before going into deeper detail. 



Before:

![images/625df9c03f550c5f4bd63bff358f930f.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/625df9c03f550c5f4bd63bff358f930f.png)


---
**Bob Buechler** *March 23, 2017 17:45*

After:

![images/cac9de2fb35045a3e0f4d300f642f724.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cac9de2fb35045a3e0f4d300f642f724.png)


---
**Bob Buechler** *March 23, 2017 17:46*

Note: Just realized I should have said LPS instead of LSU. I'll correct that on my next iteration.




---
**Don Kleinschnitz Jr.** *March 24, 2017 10:02*

**+Bob Buechler** Yup! A nuance, you do not actually have to connect frame ground to L- as it is connected internally but what you have will not hurt anything. Good pictures if you are ok with it, I will add them to the blog with the write up.


---
**Bob Buechler** *March 24, 2017 17:19*

Yeah, I didn't know how to represent that in the diagram without it getting messy. I'll probably revamp it a bit to make that clearer. Glad to know I'm understanding it so far. 



One thing I discovered when my ammeter arrived from amazon, is that the posts aren't labelled. Since it's wired in-line with just the negative wire, perhaps it doesn't matter?


---
**Bob Buechler** *March 24, 2017 17:23*

Pic: (sorry for the blur, I took this shot so I could accurately measure it to cut the necessary holes in my control panel lid for mounting) 



[photos.google.com - New photo by Bob Buechler](https://goo.gl/photos/WzKPKR2dHQp3xQcAA)


---
**Don Kleinschnitz Jr.** *March 24, 2017 17:30*

**+Bob Buechler** I think I warned that it may not be labeled in the instructions:). I made a guess and suggested what to do if it's in backward. I think I have the same on let me see if I can test today. 


---
**Bob Buechler** *March 24, 2017 18:12*

ahhh right. I totally forgot you did cover that case. My bad. 


---
**Don Kleinschnitz Jr.** *March 25, 2017 00:38*

**+Bob Buechler** NP .... hey I just looked closely at my meter and guess what, there is a - right under the right post looking at it from the back.

It is hard to see if not at the correct angle.



This is the meter I got:

 [https://goo.gl/photos/RPpnLjDBHBBwE3fc7](https://goo.gl/photos/RPpnLjDBHBBwE3fc7)



![images/b67e87f71fd5a269819f96b71f662318.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b67e87f71fd5a269819f96b71f662318.jpeg)


---
**Bob Buechler** *March 26, 2017 18:50*

**+Don Kleinschnitz** wow. How did I miss that? Just checked mine and sure enough, there it is. 


---
**Bob Buechler** *March 29, 2017 19:24*

**+Don Kleinschnitz** Just a quick update. This past week I used a dremel to cut out a hole in the detachable control panel for the ammeter and got it mounted. I'm leery to proceed with the wiring changes because after reading your instructions, I now know I need to be careful to use good quality terminals and solder them, instead of just using a manual crimp. Also, the only terminals I have on hand right now are cheap ones with short wire tubes. 



I've also never soldered one of those before, so I'm not exactly sure where to place the solder... do I just sort of cap off the terminal with solder on the end? Do I do that before or after I crimp it? I don't have an expensive crimping tool either, just one of these: [https://www.amazon.com/gp/product/B000JNNWQ2/](https://www.amazon.com/gp/product/B000JNNWQ2/) -- I also have one of these somewhere, but they look about the same from a crimping perspective:

[https://www.amazon.com/gp/product/B000OQ21CA/](https://www.amazon.com/gp/product/B000OQ21CA/)



In any case, I could use a few pointers for how exactly these should be soldered. 


---
**Don Kleinschnitz Jr.** *March 30, 2017 13:55*

**+Bob Buechler** 

I solder these type terminals because I have had problems with crimps corroding and/or vibrating loose. Theory is that if you crimp correctly this will not happen however soldering insures it does not.



Prepare the wire:

Strip the wire back far enough so that the bare end inserts fully into the barrel to its end



Heatshrink:

Cut and slide over the wire a piece of heat-shrink that will cover the barrel of the terminal after soldering. The plastic cover may or may not come off or loose from heating. Most of the time I remove the plastic before starting.



Solder

Put the ring tongue on a heat restive surface or in a clamp. I lay mine flat on a piece of 600 grit sandpaper (the surface is heat resistant). Insert the wire through the barrel. Press with the iron on the  ring tongue side of the terminal and heat while applying solder until the barrel of the terminal fills up. Depending on the size it may take a fair amount of heat. Don't put so much solder on it that it flows around the ring as that will impede attaching it to screws. 



Finish

Clean flux from the terminal and slide the heat shrink up over the barrel and shrink it over the plastic if its still there. Sometimes the plastic falls off or needs to be cut off. 



Some say this is overkill because its a pain to do but I have never had one fail over years of use.


---
**Bob Buechler** *March 30, 2017 14:47*

The existing wire coming off the tube, that plugs into the L- port on the LPS, has a pin terminal on it. Do I cut that off and install a new ring terminal?


---
**Don Kleinschnitz Jr.** *March 30, 2017 20:25*

**+Bob Buechler** pin terminals are ok if you use the correct crimper. 

I don't think a ring terminal will go into the green terminal block will it??

I assume you are talking about the LPS end of that wire?


---
**Bob Buechler** *March 30, 2017 20:50*

Correct. The instructions state to remove that wire from the LPS and attach it to the positive terminal on the meter. To do that, I'd need to change the terminal to a ring, right?


---
**Don Kleinschnitz Jr.** *March 30, 2017 21:20*

**+Bob Buechler** ah see your point.

I don't know how long your cathode wire is or where your meter is.



Ideally you want to cut the wire in a way that the meter can physically be placed in series with it leaving the cathode and the LPS end terminations alone. Just putting ring tongues where you cut it to connect the the meter. i.e The meter is placed in series with the current wire.



If the wire is not long enough cut the cathode wire long enough to reach the meter and put a ring tongue on it and connect it to + of the meter. Get another wire that is long enough to reach the LPS and put a ring tongue on it connected to the - side of the meter. Put a pin terminal on the LPS end.



Don't have a pin terminal and crimper? Alternatively tin the wire with a liberal amount of solder and insert and screw that into the LPS terminal.  



If you want to keep the pin terminal you can splice it and a section of wire to a longer piece just insure you solder properly and cover it with shrink wrap.



Hope that is not to confusing :)


---
**Bob Buechler** *March 30, 2017 22:00*

Nope. not too confusing. :) I'll check the lengths tonight. It's currently wrapped in with all the other stock wiring so that added length will probably give me the slack I need to be able to comfortably open the lid (which has the meter on it) and still be able to splice it in. Should work fine. 


---
**Bob Buechler** *April 09, 2017 21:13*

**+Don Kleinschnitz** Took the plunge and wired up the ammeter using the instructions you posted above. I ended up salvaging the pin terminal by snipping off the end of the wire, soldering it onto the new wire I added that runs from the L- port on the LPS to the ammeter and covering it in shrink tubing. While I was in there, I confirmed there was internal continuity between the L- and GND ports on the LPS. 



From there I installed two new ring terminals on the negative wire coming off the tube and the new cable I installed into the LPS using crimps and solder, as instructed. Hooked up the negative wire from the tube to the positive post on the meter, and the LPS cable onto the negative post. 



Powered it up, no smoke. :) Test fire -- movement on the meter and a successful lasing. Looks like victory so far. Thank you very much for helping me, and for drafting up those instructions. I think this thread has great source material for a blog article on the subject.



  Sidenote: My lowest lasing power on my machine is 14% on the digital panel, which (on my machine at least) translates to ~5mA on the ammeter. To figure out my max power setting on the digital panel, I stepped up the power percentage on the digital panel by 10% until the meter got above 20mA, and then I stepped the power down by 1% until it settled at exactly 20mA on a sustained test fire. On my machine, that max is only 49% on the panel! 



That suggests to me that NOT having an ammeter on the digital-only machines is a recipe for blown tubes and we should consider recommending that digital-only owners add these as a priority upgrade. Previous to installing the meter, I thought I was playing it safe by setting the ceiling at 80%.   


---
**Don Kleinschnitz Jr.** *April 09, 2017 23:18*

[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)


---
**Bob Buechler** *April 09, 2017 23:31*

Looks good, **+Don Kleinschnitz**​​. Here is an updated diagram that (hopefully) denotes that L- and GND should be internally bridged: [drive.google.com - K40-Ammeter.png - Google Drive](https://drive.google.com/file/d/0B8jLpTsfTtT_YW1YVldGWmN4d3M/view?usp=drivesdk)


---
**Don Kleinschnitz Jr.** *April 10, 2017 10:05*

**+Bob Buechler** thanks for the update .....Added!


---
**Craig “Insane Cheese” Antos** *April 18, 2017 11:04*

I did this the other day and get about 10mA at 40% and around 15 at 75%, thinking my tube might be on it's way out. Which is weird, because until I installed the ammeter I was about to cut 2mm acrylic at 24% and 3mm/s, now I have to push to over 40%. Could the ammeter be causing this?


---
**Don Kleinschnitz Jr.** *April 18, 2017 13:02*

**+Craig Antos** Not sure what the % values are related. % of what value? I am guessing since you are installing a meter you do not know what current it took to cut 2mm before?



All that said I doubt that your meter is causing a problem but I never say never with a K40. 

.......

If I estimate the relationship between current and %, it suggests that you are trying to cut at 7.7 ma which seems low. The laser ionizes at about 4 ma so the bottom part of the scale may be nonlinear. 



75% = 15

40% = 10

30% = 8.6

24% = 7.7

20% = 7.1

10% = 5.7



.......

The other day **+Chris Menchion** and I tested the meter out of circuit to see if it read correctly and it does. BTW do not use a DVM to measure current instead of the milli-amp meter it blew **+Chris Menchion** 's up? No idea why so don't do it until I figure out what happened.

......

You can check.

1. The "IN" pin for 5VDC at max pot settings and 0VDC at min setting. 

2. Insure that the meter - side is really grounded to the -L on the LPS and that is grounded to the frame.





3. ......... a cheap test for power levels with and without meter installed .........

3.a Cut a thicker (4mm) piece of acrylic (so it does not penetrate) at the speed and power in question. Cut as close to the edge as possible but not directly on the edge. Do not move the pot from this point on in this test.

3.b Put that sample aside

3.c Move the wire on the - lug of the meter to the + lug of the meter. essentially taking the meter out of the circuit. You could also use an alligator clip to short the meter lugs together.

3.d Cut another sample the same way and with the same power and speed as 3.a 

4. For each sample. Polish the edge of the sample on the side nearest the cut so you can clearly see the cut through the edge . Using a loop examine the depth of cut and see if the sample from 3.d cuts deeper than the cut made in 3.a.


---
**Craig “Insane Cheese” Antos** *April 18, 2017 13:28*

Oh sorry. I have a digital panel, which is why I installed the ammeter. Mines an analog meter. Is it worth loosing the panel and switching to a pot? 



I'll recheck the grounding and I'll try that cutting test. I've got some 4mm in clear, but I suspect it isn't cast acrylic. I've never cut it to find out.



All I did when installing the meter was cut the existing - from the tube and add the meter. I might reterminate the lugs and see if that helps, might have a poor joint. I probably should have checked all of this with my multi meter before 🤔






---
**Don Kleinschnitz Jr.** *April 18, 2017 13:44*

**+Craig Antos** terminating the lugs might help, as a minimum it will make it more reliable. 



The pot is in some ways a matter of preference. I prefer the pot and the associate DVM because I can see the actual voltage on the LPS.  We don't really know what voltage is being applied to the "IN" with the digital control, although I guess it could be measured.



You can see from this thread that others seem to like the conversion away from digital to analog control. 



[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/cggqu9Rjh85) &mdash; content and formatting may not be reliable*
