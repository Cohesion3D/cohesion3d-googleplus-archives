---
layout: post
title: "As per a recommendation to upgrade my K40W board to the cohension3d mini board in order for me to use the Lightburn software"
date: July 10, 2018 18:18
category: "K40 and other Lasers"
author: "Ashra Campbell-Clarke"
---
As per a recommendation to upgrade my K40W board to the cohension3d mini board in order for me to use the Lightburn software.  I am posting pics of my board to ensure that the swap can work.  Any helpful suggestion will be  appreciated.



Thank you. 



![images/bc3f88ad153a2eae12593f1cd3296658.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc3f88ad153a2eae12593f1cd3296658.jpeg)
![images/b73ffac119f22b00979ece1396b56a4f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b73ffac119f22b00979ece1396b56a4f.jpeg)
![images/624b11dbdff16bf5401f851eacb505a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/624b11dbdff16bf5401f851eacb505a7.jpeg)
![images/24ae7241c99e98038efacb28fa473bd8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/24ae7241c99e98038efacb28fa473bd8.jpeg)
![images/20fbe7a5951bb4e792211439118c2742.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/20fbe7a5951bb4e792211439118c2742.jpeg)
![images/3b72d86c2f20c4c3873c4d87367e0ec2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b72d86c2f20c4c3873c4d87367e0ec2.jpeg)
![images/32dbfd57b244152282a4ac81fa32988b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/32dbfd57b244152282a4ac81fa32988b.jpeg)

**"Ashra Campbell-Clarke"**

---
---
**Tech Bravo (Tech BravoTN)** *July 10, 2018 18:41*

The swap will work :) and you will pleased. Follow every step in the documentation. There are a few things that get missed easily like taking good pictures before starting so you know where wires went, making sure origin is set correctly, and a few others. It is all covered. I installed mine in a bout 10 minutes.


---
**Ashra Campbell-Clarke** *July 10, 2018 22:02*

Thank you.  I will let you know how it goes.


---
**Ashra Campbell-Clarke** *July 10, 2018 22:06*

Do I need  1 or 2 of these 

External Stepper Adapter (For Z Table/ Rotary):

External Stepper Driver (For Z Table/ Rotary):


---
**Tech Bravo (Tech BravoTN)** *July 10, 2018 22:42*

If you have a stock k40 and do not have a motorized z axis then you only need what comes in the bundle.


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2018 01:59*

The items you asked about are necessary if you wish to add a Z table or Rotary or both to your machine.   



However, we do recommend adding the USB Cable and Power Supply Upgrade Kit - the stock USB Cable from the K40 gives problems.  The Power Supply Upgrade kit is a separate high quality 24v power brick to power your board and motors - this increases reliability a lot. 


---
**bojan repic** *July 11, 2018 15:15*

I am using C3D on same machine for over 6 months now and am very happy with it. I also added separate 24V 6A power supply as Ray suggested.


---
*Imported from [Google+](https://plus.google.com/114192144580399794033/posts/8cpR9HqdB49) &mdash; content and formatting may not be reliable*
