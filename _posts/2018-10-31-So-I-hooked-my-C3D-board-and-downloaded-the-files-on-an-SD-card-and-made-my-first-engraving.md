---
layout: post
title: "So I hooked my C3D board and downloaded the files on an SD card and made my first engraving!"
date: October 31, 2018 01:47
category: "General Discussion"
author: "Brent Tucker"
---
So I hooked my C3D board and downloaded the files on an SD card and made my first engraving! It came out ok, I need to get familiar with the speed and power settings for the wood I’m using. However when I tried another piece I came into an issue. First off, Lightburn doesn’t recognize the k40 on the “auto” setting of devices. No big deal, so I hit the drop down menu and the only port is gives me is COM3 but my k40 is actually plugged into COM4. So I unplugged the USB and plugged it back in and it eventually gave me COM4 as an option and as soon as I hit COM4 I could here the K40 connect. I jogged the machine to the spot I wanted to start and it jogged just fine so I know it talking. When I hit “start” for the machine to start engraving the head moved to the starting point and froze there, the laser never turned on. Lightburn thinks the engraver is going I assume because it says busy under “laser” and the timer starts to increase. However the bar never gets fuller and the percentage at the end of the bar stays at 0%. At those point the software seems frozen, I hit the “stop” button but can no longer jog the laser. The only way to get coms with the laser again is to disconnect and shut everything down and try again. When I do try again everything works fine up until the point I hit “start” then everything freezes after the laser moves into its initial position. Yes the laser works when I hit the laser test switch. 





**"Brent Tucker"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2018 02:31*

Please show pics of your board, mounting, wiring, and cables. 


---
**Brent Tucker** *October 31, 2018 03:54*

It’s set in there for the pic because I took it out to do the 24v power upgrade to it but haven’t completed that upgrade yet. I also have the upgraded usb from you. Hope this helps and thanks!

[https://plus.google.com/photos/...](https://profiles.google.com/photos/105472717401935675008/albums/6618364605289733441/6618364602754074738)


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2018 15:32*

You should isolate the board from the frame using nylon standoffs/ a plastic plate. There’s a note about this in the instructions. 


---
*Imported from [Google+](https://plus.google.com/105472717401935675008/posts/9NkAJSbHgUW) &mdash; content and formatting may not be reliable*
