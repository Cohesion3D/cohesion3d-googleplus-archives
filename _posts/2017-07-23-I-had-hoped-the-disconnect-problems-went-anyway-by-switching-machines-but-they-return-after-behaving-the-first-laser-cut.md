---
layout: post
title: "I had hoped the disconnect problems went anyway by switching machines but they return after behaving the first laser cut"
date: July 23, 2017 16:22
category: "C3D Mini Support"
author: "Tammy Mink"
---
I had hoped the disconnect problems went anyway by switching machines but they return after behaving the first laser cut. They don't seem as frequent yet and don't seem directly tied to firing the laser but still LaserWeb keeps randomly disconnecting from the Cohesion3D mini board. 





**"Tammy Mink"**

---
---
**Jorge Robles** *July 23, 2017 16:37*

Check usb cable and ports usb2/usb3


---
**Tammy Mink** *July 23, 2017 17:11*

Bought 3 new USB cables already..the most recent having ferrites on both ends. Tried every port on my PC (USB 2.0)


---
**Jorge Robles** *July 23, 2017 17:47*

[github.com - Random issue:   machine disconnection - reconnect - queue continues when clicking jog · Issue #40 · LaserWeb/lw.comm-server](https://github.com/LaserWeb/lw.comm-server/issues/40)


---
**Tammy Mink** *July 24, 2017 12:09*

Based on what I read this I am out of luck with this since , shame since I wasted so money on trying to get this to work (the main board w the GLCD and 3 new USB cords, power supply) . I admit I haven't done anything with the power supply, but that was for an issue with a previous machine. I had mostly gotten this because it was supposed to be a simple drop in replacement for my K40 but I guess I just wasn't in luck. I'll have to toss my card in a box and stash it away and maybe some day in the future I can revisit this and maybe there will be a solution then.  Only other idea I can think of, since this is an issue between LaserWeb and the CohesionMini...are other other programs like LaserWeb I can try? 


---
**Jorge Robles** *July 24, 2017 12:17*

Both **+Ray Kholodovsky**​ and Laserweb team (me included) are working on a solution ;)


---
**Tammy Mink** *July 24, 2017 12:26*

I am sorry I am just quite disheartened about the issue, I don’t mean to be a p.i.t.a.. To be fair the first K40 machine I made had boatloads of problems unrelated to the Cohesion3D Mini in addition to this issue, but at least my second one seems electrically sound thus far. For now my stock board is working just fine and I can work on other upgrades and things such as aligning my gantry and mirrors. 




---
**Tammy Mink** *July 24, 2017 12:43*

I do have another Win 7 PC I can try if I get ambitious to try again but since I have my 3D printer on that one at the moment I was trying to not have them on the same PC in case I wanted to run both at once.  The one the K40 is currently on is a quad core Dell... but the other is one I put together from parts and is years older and is only a dual core. I don't see it working better but who knows.


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 12:55*

Are you able to save the gCode file, copy it to the MicroSD card, and run it via the GLCD Menus? 

Does this yield in a completed job? 


---
**Tammy Mink** *July 24, 2017 13:05*

I admit I didn't test that on this machine before I took the card back out after it disconnected randomly (though seems to work fine besides that). I suspect it can, even on the old machine it could until the power supply got really crazy just before it died. I can test that aspect out in the near future. As a side note in case it helps troubleshooting,  for both the machines I had; When it disconnects I have to close out of LaserWeb and I have to unplug and replug the USB cord form main board, I sometimes have to do this a few times before I can get LaserWeb to connect again to the board. On the topic of power supplies, the new one looks a tiny but different...I'll dig up a picture.




---
**Tammy Mink** *July 24, 2017 13:07*

Not the Cohesion3D board but the mainboard it came with

![images/f8c03a35496c7b035294c85be80084c3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8c03a35496c7b035294c85be80084c3.jpeg)


---
**Tammy Mink** *July 24, 2017 13:08*

Power supply area

![images/fafa4f15a80f136a351227aa591a5bcb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fafa4f15a80f136a351227aa591a5bcb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 13:11*

Yep, this is the ribbon cable + screw terminal psu variant, also very common and the Mini supports it. 



To be perfectly honest, most of our power users are indeed using that copy the file onto the MicroSD card and run via GLCD approach. It is well agreed that this is more reliable than a USB connection. 

That said, there is also a grbl-lpc firmware alternative for the Mini/ other Smoothie boards which improves USB streaming from LW significantly. 


---
**Tammy Mink** *July 24, 2017 13:20*

I do like having the option of having the GLCD for large jobs (and I do like the GLCD) but I really don't want to go through the extra steps and time for a 3 minute cut and I often work in a smaller scale.  I didn't think this upgrade would make my USB connection unusable, I am hoping a solution will eventually be found so I didn't waste so much money. 




---
**Tammy Mink** *July 24, 2017 13:34*

If I went to this grbl-lpc firmware as opposed to the smoothieboard, what features would be lost? I assumed there was a reason you chose the smoothieboard firmware over the grbl-lpc. Would it still have grayscale engraving and be able to use the GLCD? 




---
**Jorge Robles** *July 24, 2017 13:36*

I'm afraid there's no glcd nor sd so far, but raster can go about 300% faster without stuttering.


---
**Jorge Robles** *July 24, 2017 13:36*

**+Todd Fleming**​


---
**Ray Kholodovsky (Cohesion3D)** *July 24, 2017 13:42*

Grayscale absolutely, no glcd support though. 



Smoothie is more configurable as far as glcd, additional inputs/ outputs for accessories/ control buttons. 



Grbl-lpc was ported by the LW devs much more recently when an agreement could not be reached between them and the Smoothie folks over how to handle fast communications properly. It is much more "raw" and is simply a blazing fast firmware with no bells and whistles. 



My decision chart is pretty much:  do you have a glcd? Copy the file to the card and run it via glcd menu. 



No glcd? Go grbl-lpc. 



There is also a way to start the job using the smoothie play command in terminal. And you know that you can just copy your gCode to the drive that shows up when you plug in USB, you don't actually have to remove the card, right? 



We are hoping that building this (smoothie file manager sort of thing) into LW will be a fix, essentially expediting the process of copying the file to the card for you and presenting you with a list of files on the card/ board available to run with a play option for each. 


---
**Tammy Mink** *July 24, 2017 14:07*

The smoothie file manager built into LaserWeb type of thing does sound like a decent compromise to the USB issue. Maybe if I give it a few months/years they will add that feature, I just need to remember to check eventually. I guess for now stock board and Corelaser for me.


---
**Tammy Mink** *July 25, 2017 13:00*

Can you point me at any instructions for installing GRBL on my Cohesion3D Mini board in case I got that way (I assume I could always switch back to Smoothie when they work some issues). I will miss the GLCD though, that thing was neat. I suppose you likely have no idea when they might add the smoothieboard, file manager to LaserWeb, if it something they are working on or just a “in theory” ..if it in the range of weeks, months years or potentially never…I know such things are VERY hard to gauge. 




---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 13:02*

Cohesion3d.com --> documentation --> k40... there's a whole section there and grbl-lpc should be an article. [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3d.com)


---
**Jorge Robles** *July 25, 2017 13:02*

[cncpro.co - Live Jogging](http://cncpro.co/index.php/65-documentation/initial-configuration/firmware/grbl-lpc-1-1e)


---
**Tammy Mink** *July 25, 2017 13:08*

Thank you for the links!




---
**Tammy Mink** *July 25, 2017 13:12*

Switching looks quite simple, and I can easily save\rename the old smoothieboard file for later use. If nothing else it maybe worth it for me to try this just to see if the USB connection becomes stable or not. 




---
**David Cantrell** *July 30, 2017 23:32*

I believe I have the same issue as Tammy. I have finally worked through the issues I was having with the tube and have been able to use LW4 to send jobs to the laser over USB, but have also been running into disconnect issues when to job is 60 - 70 % done. I have tried two different PCs and different cables. Besides saving the gcode to the SD, is there anything else I can do? If you guys find a fix for this in firmware, please let me know.


---
**David Cantrell** *July 30, 2017 23:34*

**+Tammy Mink** Were you able to swap the firmware for grbl-lpc? Did it help with the disconnects?


---
**David Cantrell** *August 01, 2017 19:00*

I tried Grbl but ran into a lot of hoops to jump through getting it set up, so I gave up. I ended up switching back to Smoothie at the latest build (june 21) and have found it to be more stable, without the disconnects I was dealing with before. You might want to see if updating your Smoothie firmware fixes your issue.


---
**Tammy Mink** *August 07, 2017 21:38*

I have not hooked up my Cohesion3D Mini again as of yet. I might try updating to the smoothie firmware. Are there instructions for uploading the smoothie on the Cohesion3D mini? Did you have to do any other configuration to make it work with the board?




---
**Tammy Mink** *August 07, 2017 22:18*

I figured out how to update my smoothieboard...now to see what happens :)


---
**Tammy Mink** *August 07, 2017 22:41*

I have the Cohesion3D mini board installed again. I installed the newer firmware.bin. I left the config file. I'm having problems homing. If I choose home all axis from the GLCD it homes fine. If I run G28.2 the Y axis goes backwards.


---
**David Cantrell** *August 07, 2017 23:49*

I do not do anything with homing. I turn the laser off and manually move the head to the front left corner and click the "set zero" button in laserweb. I have 3-D printed a mount to allow me to move the end stop so that it will home to the front left corner. Most CNC machines home to the front left so moving there and stops will make this laser run more normally.


---
**David Cantrell** *August 07, 2017 23:50*

If yours is already homing to the front left you may want to leave it that way and move the end stops to that corner.


---
**Tammy Mink** *August 08, 2017 09:33*

I don't really want to move my end stops because too much can go wrong if I mess with it along with if I have to use the stock board it will confuse it. I was just hoping this would allow me to use my Cohesion3D mini board without having the disconnects  or going to grbl and losing support for the GLCD. I guess for now back to the stock board again. 




---
**Tammy Mink** *August 08, 2017 11:22*

**+Ray Kholodovsky** Anything particular that you know of that I need to do to get the newest Smoothie firmware working correctly on the Cohesion3D mini board ?


---
**David Cantrell** *August 08, 2017 12:26*

If you do get a disconnect, you can reconnect and hit the run button in laserweb and the job will continue where it left off, so you haven't lost anything


---
**Tammy Mink** *August 08, 2017 13:49*

At least on the original combination of LaserWeb/Cohesion3D I couldn't continue a disconnected session. Is this something new to this smoothie firmware? 




---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 14:34*

**+Tammy Mink** I would recommend formatting the sd card and placing the firmware and config from the Dropbox in the bottom of the instructions onto the card. Then run the board and when you send a G28.2 it should move rear left. If it moves a different direction then you need to flip the respective motor cable. 



Then you can try to put a latest firmware file on there. You need to grab the firmware-cnc.bin from Smoothieware github, rename to firmware.bin, put it on the card and reset the board. Watch the LEDs count up in binary on first boot as firmware flashes. Try G28.2 again - should move to the same directions. 


---
**Tammy Mink** *August 08, 2017 14:49*

I can try a reinstall and start over etc. On the GLCD if I choose "Home All Axis" is does home correctly, it's just if I do it via LaserWeb a G28.2 that the Y is backwards. Is the GLCDs "Home All Axis" a different gcode command then the G28.2 or the same? If not maybe I could just use whatever command "Home All Axis" is instead of G28.2. I suppose I would also need to check even if that will work depending on what direction the laser "prints".




---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 14:50*

I'm not sure, the process I described should settle all doubt. You can try the GLCD menu option as well after and let me know if it also behaves opposite the G28.2 command. 


---
**Tammy Mink** *August 08, 2017 14:57*

This only happened with the updated firmware (I believe), the firmware the CohesionMini3d came with seemed to be in sync. I will format, reinstall, test and then switch it to the newest firmware and see what happens after all that once I get the time and report back. 




---
**Tammy Mink** *August 08, 2017 23:21*

After a diversion in unsuccessfuly trying to install the grbl.. I'm trying the new smoothieware again. I was sure this time that homing worked on the GLCD and in LaserWeb with the stock firmware for Cohesion3D mini. When I went with the newer smoothieware the GLCD homes it correctly but the y-axis is reversed in LaserWeb.


---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 02:20*

For Smoothie... Can you specifically state that after the latest firmware flashed, G28.2 moves you in the opposite direction than before?  



I've reviewed the Smoothie upgrade notes and see no mention of an X or Y axis being flipped (only Extruder, and this happened some time ago): [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md)



That said, if G28.2 is indeed moving you away from the endstop, it would be prudent to flip the cable orientation for that axis to reverse the motor. 


---
**Tammy Mink** *August 09, 2017 09:41*

Yes I'm certain it's flipping this time, works fine with your dropped box version of the firmware but that one the USB connection is useless but I go to the new version, Y goes backwards. I can flip them but if I do home all axis from the GLCD then it going to go backwards when I use the GLCD which will make running jobs from the minisd card on the GLCD problematic. I should be able to use the GLCD and the USB, I shouldn't have to choose one or the other.  If it matters from the smoothie firmware I went to [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin) and I tried firmware_latest and firmware (renaming the first one of course to the proper name). Am I trying the right ones?


---
**Tammy Mink** *August 10, 2017 12:04*

**+Ray Kholodovsky** Any ideas how I can get the GLCD homing and LaserWeb homing going in the same direction with the newest smoothie firmware? Is there some setting in the program of the GLCD? Changing the wires won't work because then the issues flops. Should I try reinstalling LaserWeb, or recreate the machines profile, or switch some setting on the GLCD? 


---
**Ray Kholodovsky (Cohesion3D)** *August 10, 2017 16:49*

Seems like a few people are experiencing this, I will look into it and try to reproduce.


---
**Tammy Mink** *August 11, 2017 13:21*

**+Ray Kholodovsky** I was just tinkering around and it seems using G28 instead of G28.2 for the homing code in LaserWeb seems to work and gets the GLCD and LaserWeb homing in the same direction. Still have to test everything else and tinker with it and see what happens.


---
**Tammy Mink** *August 11, 2017 13:32*

Seems it runs but the laser isn't turning on. What gcode do I use in the tool on and off section of LaserWeb? I tried M3 and M4 for laser on and neither worked.


---
**Tammy Mink** *August 11, 2017 13:38*

I'm going to try using the firmware-cnc-latest from the Smoothie firmware to see what happens


---
**Tammy Mink** *August 11, 2017 13:56*

Now that I'm using the cnc version of the software it homes fine with G28.2  but still not activing the laser.  


---
**Ray Kholodovsky (Cohesion3D)** *August 11, 2017 16:38*

Ok, yes you need CNC Firmware for G28.2 to home to the switches.



Can you tell me more about not activating the laser? 



Send a command G1 X10 S0.6 F600 what happens? 


---
**Tammy Mink** *August 11, 2017 21:50*

I reflashed the newest cnc version of the firmware. I think the laser not firing is on the LaserWeb side. From the GLCD test fire works, right now from LaserWeb the test fire works but if I go to cut a simple circle it goes though the motions and doesn't fire the laser. That command does make the laser come on and engrave a short line. I tried direct from USB and via the GLCD... it's just wierd. Right now in LaserWeb I have nothing in the tool on and off, should something be there? In the gcode I don't see anything like it telling the laser to fire.


---
**Tammy Mink** *August 11, 2017 21:51*

G21         ; Set units to mm

G90         ; Absolute positioning

G28.2

;

; Operation:    0

; Type:         Laser Cut

; Paths:        1

; Passes:       1

; Cut rate:     48 mm/min

;





; Pass 0



; Pass 0 Path 0

G0 X38.60 Y117.17

G1 X42.22 Y119.01 S100.00 F48

G1 X45.10 Y121.89

G1 X46.95 Y125.51

G1 X47.58 Y129.53

G1 X46.95 Y133.55

G1 X45.10 Y137.17

G1 X42.22 Y140.05

G1 X38.60 Y141.90

G1 X34.58 Y142.53

G1 X30.57 Y141.90

G1 X26.94 Y140.05

G1 X24.07 Y137.17

G1 X22.22 Y133.55

G1 X21.58 Y129.53

G1 X22.22 Y125.51

G1 X24.07 Y121.89

G1 X26.94 Y119.01

G1 X30.57 Y117.17

G1 X34.58 Y116.53

G1 X38.60 Y117.17

G1 X38.60 Y117.17

M2          ; End

G28.2




---
**Ray Kholodovsky (Cohesion3D)** *August 11, 2017 21:52*

Aha!  See the S100?  That might be the issue.  For smoothie we need the range to be 0-1.  In LW settings there's a max pwm value field, please check that this is 1. 


---
**Tammy Mink** *August 11, 2017 21:53*

I added the sample gcode incase you notice something I didn't since I am not exactly sure what to look fire firing the laser


---
**Tammy Mink** *August 11, 2017 21:58*

That's wierd..I lowered my power of the cutting to 60% and it turned on. 80% nope...79% yes. I'm only at 13.5 ma on my laser and ran it on the stock board even a little higher a whole bunch.






---
**Tammy Mink** *August 11, 2017 22:06*

I changed in the config "laser_module_maximum_power " from 0.8 to 1.0 and now it will work at 99% and below. Can't seem to get it to allow 100%


---
**Ray Kholodovsky (Cohesion3D)** *August 11, 2017 22:11*

Specifically, can you check in <b>LaserWeb settings</b> --> gcode tab  --> "MAX PWM S VALUE" and verify what that value is?


---
**Tammy Mink** *August 11, 2017 22:19*

Newest issue is the laser is squeezing when it's on. For this machine this is the first time it has happened hasn't once with the stock board.


---
**Tammy Mink** *August 11, 2017 22:21*

Also seems to be firing lighty in parts of the raster that are white like it's not fully turning off.


---
**Tammy Mink** *August 11, 2017 22:32*

I threw my stock board back in to check on the squeeling just in case, no squeeling out of the stock board when the laser fires.


---
**Tammy Mink** *August 11, 2017 23:54*

MAX PWM S VALUE says 100




---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 00:00*

Yes, for smoothie you need to change that to 1 


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/WroJybFMGiz) &mdash; content and formatting may not be reliable*
