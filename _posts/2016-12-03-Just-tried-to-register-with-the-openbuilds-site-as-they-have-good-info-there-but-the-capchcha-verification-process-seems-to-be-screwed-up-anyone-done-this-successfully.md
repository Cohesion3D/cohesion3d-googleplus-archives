---
layout: post
title: "Just tried to register with the openbuilds site as they have good info there - but the capchcha verification process seems to be screwed up - anyone done this successfully?"
date: December 03, 2016 20:03
category: "General Discussion"
author: "greg greene"
---
Just tried to register with the openbuilds site as they have good info there - but the capchcha verification process seems to be screwed up - anyone done this successfully?





**"greg greene"**

---
---
**Ashley M. Kirchner [Norym]** *December 03, 2016 22:08*

If you're talking about [openbuilds.org](http://openbuilds.org), I just registered without any issues.


---
**greg greene** *December 03, 2016 22:18*

thanks - I'll try again


---
**greg greene** *December 03, 2016 23:22*

Worked but only with Explorer - but not with Chrome - strange .......


---
**Ashley M. Kirchner [Norym]** *December 04, 2016 01:19*

I used Chrome on my end. 


---
**greg greene** *December 04, 2016 02:04*

My chrome has been doing weird things like that - time to upgrade maybe

Something to do with the Cache I understand


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/DeSXVXcPqJZ) &mdash; content and formatting may not be reliable*
