---
layout: post
title: "I installed Ubuntu to run my K40 but it won't let me connect to the machine"
date: August 08, 2017 18:58
category: "Laser Web"
author: "Kma Jack"
---
I installed Ubuntu to run my K40 but it won't let me connect to the machine. I keep getting "Server Error: Permission Denied, cannot open /dev/ttys6" or any other "tty" choices when I try to connect via USB.

Please see pic.



Can anyone tell me how to overcome this permission thing please?



TIA



![images/d6e9e4c7671acee7855ef2e9219bd3b6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d6e9e4c7671acee7855ef2e9219bd3b6.jpeg)



**"Kma Jack"**

---
---
**Stephen Baird** *August 08, 2017 19:39*

You can also try "sudo dmesg | grep tty" to help reduce the amount of stuff you have to comb through to find the right device.


---
**Kma Jack** *August 08, 2017 19:52*

Hi **+Georg Mill**, thank you for replying.

There is no "/dev/ttyUSB0" or any other USB in the list', ACM0 is but I still get the same error when I choose that.



I did as you said and found the smoothie board in the list. 

I have taken a photo of the list where all the tty and ACM0 are listed. How to I change t he permission to be able to use them? 





![images/3d0b739d2085650405116783902c5e25.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d0b739d2085650405116783902c5e25.jpeg)


---
**Kma Jack** *August 08, 2017 19:59*

Hi **+Stephen Baird**, wished I'd seen your message earlier, would have saved me a lot of time. Thanks, will save that for future use




---
**Kma Jack** *August 08, 2017 21:06*

I've done it guys. I've added myself to the "dialout" group and now I can connect to the laser. 



Thanks for your help




---
**Kma Jack** *August 09, 2017 02:05*

Is there another secure way of doing it? 



Actually, I am having anything but "fun" LOL.

I can connect to the board now but still have a lot to fix and learn.



For starters, I tried engraving and found that the laser stops at very single line for about a second or two and then carries on, no idea why so need to find out. 



I am not sure if the PWN is working. When I was engraving, I had to turn the power up on the machines own LCD screen using the push pads. If I turned the power down less than 30% it just didn't engrave, I couldn't even see the laser firing. This could be my settings though, will play around a bit later on today and try work it out. 



Another thing is that I'd like to set top left corner to be the home position, need to find out if that is at all possible.






---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/VgVfnAaGcnj) &mdash; content and formatting may not be reliable*
