---
layout: post
title: "Hi, I fired up the C3D Mini and installed LaserWeb4 following the instructions on the website"
date: August 05, 2017 21:09
category: "K40 and other Lasers"
author: "Kma Jack"
---
Hi, I fired up the C3D Mini and installed LaserWeb4 following the instructions on the website. 



I can connect to the board but when I try to jog I get the message "No supported firmware detected Closing ComPort Com6" or it disconnects after a while even if I do nothing. 



Do I have to install the firmware and if so how please? 



TIA

 

Eric





**"Kma Jack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:15*

What operating system, what USB cable, screenshot of error.... 


---
**Kma Jack** *August 05, 2017 21:22*

I am using Win7 and a normal USB printer cable. I've downloaded and installed the USB driver for it





![images/f09359db4b1a1b5c979336952ee9a0aa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f09359db4b1a1b5c979336952ee9a0aa.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:23*

Maybe the driver didn't work in which case you should uninstall it, and do the manual install procedure. 


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:23*

[smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:24*

Does your "my computer" show a drive with the firmware and config file on it? 


---
**Kma Jack** *August 05, 2017 21:28*

I did install the driver manually and there is no exclamation mark in Device Manager. That is the driver I installed



I haven't connected the SD card that has those files on it


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:29*

Well there we go! You need the card plugged into the board at all times. 


---
**Kma Jack** *August 05, 2017 21:32*

Ah ok, didn't know that. So the firmware is on the SD card and not on the board itself, is that correct?




---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 21:35*

There's a config file on the card which the board needs every time it starts up. Hence, keep the card in the board and only take it out if you want to put a job file on it. 


---
**Kma Jack** *August 05, 2017 22:01*

It's working now, thank you very much for your help


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/27N8M29TkqS) &mdash; content and formatting may not be reliable*
