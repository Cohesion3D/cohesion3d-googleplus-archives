---
layout: post
title: "Ray Kholodovsky , I'm thinking of getting a Cohesion3D mini laser kit for my K40"
date: May 29, 2018 16:09
category: "C3D Mini Support"
author: "Scruff Meister"
---
**+Ray Kholodovsky**, I'm thinking of getting a Cohesion3D mini laser kit for my K40. Is the stock C3D Mini with Smoothie able to control the laser power directly or will I still use the potentiometer on the control panel for that?



What I am hoping to be able to do is create Gcode files that will vector cut a number of small parts from balsa wood, but also engrave lightly on each part a number using a much lower power setting so that they can be easily identified. Is this possible with the C3D?



Thanks!





**"Scruff Meister"**

---
---
**Tech Bravo (Tech BravoTN)** *May 29, 2018 16:23*

the c3d mini does have pwm laser control so yes, you can cut and etch in one job and it will handle the varying power settings. you would use the pot as a "current limiter' and not a power control once the c3d mini is installed


---
**Scruff Meister** *May 29, 2018 17:15*

**+Tech Bravo** Thanks, do you know if this works "out of the box" just by connecting the K40 cables to the relevant connectors on the C3D or will I need to make additional wiring harnesses/modify the connections to my PSU?


---
**Tech Bravo (Tech BravoTN)** *May 29, 2018 17:19*

it is designed to be a direct drop-in replacement for the m2 nano. i literally installed in my completely stock k40 in about 15 minutes with no modifications whatsoever. there are a few variations between the k40's but all of the common configurations are supported in the documentation. so, in short, yes it works right out of the box.


---
**Scruff Meister** *May 29, 2018 17:36*

Thank you!


---
*Imported from [Google+](https://plus.google.com/116126524987588221083/posts/b2gftffBtfD) &mdash; content and formatting may not be reliable*
