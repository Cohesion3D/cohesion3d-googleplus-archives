---
layout: post
title: "+Ray Kholodovsky - I remember reading recently that K40 owners gain 256 grayscale with the C3D upgrade (is that right?)..."
date: March 14, 2017 17:47
category: "C3D Mini Support"
author: "Bob Buechler"
---
+Ray Kholodovsky - I remember reading recently that K40 owners gain 256 grayscale with the C3D upgrade (is that right?)... but I don't know what that means. :/ Can you give me an example of where this would come in handy, and/or a pic of something that was made leveraging it?





**"Bob Buechler"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 18:10*

This is what Peter is saying: [LaserWeb/CNCWeb](https://plus.google.com/u/0/communities/115879488566665599508/stream/42389162-9502-4dda-a15b-30e91c9ebd28)



I found more in the K40 community but no good way to sort them, just search for 'raster' and scroll. 


---
**Bob Buechler** *March 14, 2017 18:31*

okay so to summarize what you're saying is that proper grayscale capabilities refer to greatly increased detail when raster engraving. Is that generally accurate?




---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 18:34*

It's the grayscale itself.  Otherwise you would have on/ off at a single power level.  That's how you do cutting, not engraving.



Exception: dithering which makes your engravings look like a dot matrix printer, and that's the only way stock electronics k40 will do raster. 



But if you want the simpler answer, then yes, what you said.  Upgrade = variable laser control and better engraving quality. 


---
**Bob Buechler** *March 14, 2017 18:43*

ahh okay. Thanks. Total newbie question, I know. But it's important to get the glossary straight, especially early on.


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 18:45*

Yeah, no disagreement with that. Feel free to ask anything else. 


---
**Arion McCartney** *March 15, 2017 04:26*

**+Ray Kholodovsky**​ This question made me think of another... I've been researching how to edit photos for engraving and most have a step for BMP- dither which covers the image into dots. Is this step necessary to get good results with the C3D mini in the K40? Should I skip the dither conversion as best practice? Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 04:36*

Absolutely not.  Dithering is a stock k40 thing. For real engraving you want the "original" as in non-dithered.


---
**Arion McCartney** *March 15, 2017 04:37*

**+Ray Kholodovsky**​ Well that clears up why some of my engraving tests look sub par! Thank you!


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 04:47*

Dithered is shit. That's why the upgrade exists. 


---
**Arion McCartney** *March 15, 2017 05:01*

Don't know why that never crossed my mind.... Oh well, now I know. 👍


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/fk73y5dfWq2) &mdash; content and formatting may not be reliable*
