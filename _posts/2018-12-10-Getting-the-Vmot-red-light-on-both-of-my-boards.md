---
layout: post
title: "Getting the Vmot red light on both of my boards"
date: December 10, 2018 16:56
category: "C3D Mini Support"
author: "julian contain"
---
Getting the Vmot red light on both of my boards.  Sd card seems to be fine.  I saw someone else had the same problem with theirs and having a z axis installed.  Wondering if that's the problem eventually?  It always moved up or down in laserweb right before cuts so I just disabled it in laserweb but left it plugged in.





**"julian contain"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 10, 2018 16:59*

Pictures please and more details. 



[lasergods.com - Cohesion3D LED Indicator Status](https://lasergods.com/cohesion3d-led-indicator-status/)


---
**julian contain** *December 10, 2018 17:14*

Its definitely the board failure. The first board I think was my fault so I ordered another and it was fine until literally 5 mins before a lady dropped off wood for me to cut :/ I think I'll just not do a z axis in the future. 

![images/2498c21770ef76c0fe996efb60c0aa40.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2498c21770ef76c0fe996efb60c0aa40.jpeg)


---
**julian contain** *December 10, 2018 18:09*

So I need to get a board here ASAP.  I can pay for an overnight cross ship maybe? I even looked online to see if I could just buy another and overnight it though it seems like now theres a mandatory power supply upgrade?


---
**Ray Kholodovsky (Cohesion3D)** *December 10, 2018 18:23*

There are a few things to discuss. 

First, what Z table are you using, and what are the specs of the motor? 

I can't see from your picture if you have installed a separate 24v power supply to power the board or are attempting to draw all this power from the stock LaserPSU. It can't handle it. 

Potentially neither can an A4988 driver, and reviewing order records it doesn't look like you got the additional drivers from C3D, so I don't know if they were bad. 



With all this considered we have been recommending external drivers and a separate 24v PSU to power the board as shown in the z table guide at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) 



The new LaserBoard has 4 built in drivers so you can run Z and A up to a certain current of 1.2 amps (again this is why I ask about what table and motor it uses) directly on board.  And yes, we have a high quality power brick to power the board that is required because trying to power all this from the stock LPSU and not causing that to explode is pretty close to a miracle. 






---
**julian contain** *December 10, 2018 18:47*

It's a diy z table.  Not at home now so I can't take a pic.  



[amazon.com - Robot Check](https://www.amazon.com/Stepper-Nema11-Bipolar-8-5oz-Single/dp/B00PNEPF4O/ref=sr_1_3?ie=UTF8&qid=1544467530&sr=8-3&keywords=nema11)



Is the motor.  There wasn't an additional power supply added.  As for the stepper drivers I've ordered two of the cohesion3d boards and they each come with 2 drivers so I have 4.



When I did have the z enabled in laserweb the x and y would act strangely slow or extremely fast so I just disabled it in software and didnt have any problems till this morning.




---
**Ray Kholodovsky (Cohesion3D)** *December 10, 2018 20:53*

For absolute clarity, that's a Nema 11, which is very small, while most of the time a Nema 17 would be used for such applications. Is it correct that you are using a Nema 11 for your DIY Z Table? 



We have been highly recommending adding a separate power supply to power the board along with the external stepper driver, all of which I told you when you emailed 5 months ago after blowing the first board. 



Some combination of the stock laser power supply's 24v rail being insufficient to provide the amount of power required, or a power spike from said stock laser power supply, could surely cause the board to fry. 



All this is why we now ship boards with a high quality 24v power brick.



The new LaserBoard, when used properly, is isolated to protect from said spikes, has the 4 drivers to run XY + Z table + A rotary, and that high quality power brick that we talked about plugs right into. 



I have also added a USPS Express shipping option to the store.  Keep in mind that we cannot process orders instantly or ship them after the post office is closed so it would be shipped next business day and USPS states 1-2 business days transit time for most destinations. 




---
**julian contain** *December 10, 2018 21:29*

Hey that's great that the new boards are properly isolated but I did already buy one due to a mistake I made but I dont feel like this some crazy mistake on my part. You recommend doing an external driver and power supply and I asked if it might work on a smaller motor and you said "maybe". I tried it, didnt work all that well, and just left it disabled and went back to using my manual bed that same day. 

I'm guessing you're not planning on repairing this one?  


---
**julian contain** *December 10, 2018 22:48*

Actually forgot I made a post of the diy z [https://www.instagram.com/p/BmP3TtrBQ1H/?utm_source=ig_share_sheet&igshid=olbdwyuscbyx](https://www.instagram.com/p/BmP3TtrBQ1H/?utm_source=ig_share_sheet&igshid=olbdwyuscbyx)


---
**julian contain** *December 11, 2018 18:15*

Heya, I placed an order with the express shipping but I'm just gonna cancel it.  Order number is  #1762


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 18:23*

Caught me right on time, was getting ready to ship yours.  Cancelled and refunded.  Best of luck in your future endeavors. 


---
*Imported from [Google+](https://plus.google.com/116932289299679953939/posts/dswe3Gox2dk) &mdash; content and formatting may not be reliable*
