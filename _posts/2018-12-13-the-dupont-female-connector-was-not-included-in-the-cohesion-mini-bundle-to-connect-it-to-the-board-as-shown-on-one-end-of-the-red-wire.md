---
layout: post
title: "the dupont female connector was not included in the cohesion mini bundle to connect it to the board, as shown on one end of the red wire"
date: December 13, 2018 23:30
category: "C3D Mini Support"
author: "Tricia R"
---
the dupont female connector was not included in the cohesion mini bundle to connect it to the board, as shown on one end of the red wire. please help.





**"Tricia R"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2018 23:39*

That’s correct, it was not. The z table/ rotary guide demonstrates how to wire everything but expects that the customer provide their own wires - a number of them are required. 


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2018 01:56*

That said, I do have all the wires here that one might need and could offer a wiring pack on the website if you need. 


---
*Imported from [Google+](https://plus.google.com/107433814983162842539/posts/WncsapnKsaU) &mdash; content and formatting may not be reliable*
