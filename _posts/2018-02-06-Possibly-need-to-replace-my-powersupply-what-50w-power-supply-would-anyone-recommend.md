---
layout: post
title: "Possibly need to replace my powersupply, what 50w power supply would anyone recommend?"
date: February 06, 2018 09:51
category: "General Discussion"
author: "timne0"
---
Possibly need to replace my powersupply, what 50w power supply would anyone recommend? I've been looking on aliexpress and was thinking something like this: [https://www.aliexpress.com/item/Co2-Laser-Power-Supply-60W/32581390817.html](https://www.aliexpress.com/item/Co2-Laser-Power-Supply-60W/32581390817.html)



I'm in the UK





**"timne0"**

---
---
**Paul Mott** *February 06, 2018 10:28*

Looks good to me.


---
**timne0** *February 06, 2018 11:23*

How would I set it up for Cohesion 3D? Ideally want an easily connected/compatable one - the last power supply is a complete PITA.


---
**Don Kleinschnitz Jr.** *February 06, 2018 12:42*

It depends on what your conversion looks like and what supply you are replacing.

Where do you get 24VDC for the C3D? If you are using the LPS's 24Vdc you will have to get an external supply. 



Check in over at the K40 Laser Engraving Community a few have upgraded to higher wattage.

**+Andy Shilling**, **+Chuck Comito** **+Phillip Conroy**


---
**timne0** *February 06, 2018 13:48*

Thanks Don. I've just ordered a 24v supply for the zbed stepper. That ought to do the job?


---
**Don Kleinschnitz Jr.** *February 06, 2018 14:00*

Hard for me to say without knowing your conversions configuration.




---
**timne0** *February 06, 2018 14:17*

I'll try take some pics tonight


---
**timne0** *February 21, 2018 21:58*

The +5v and -5v![images/483bac29e15db47be473ca7ab9f03a23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/483bac29e15db47be473ca7ab9f03a23.jpeg)


---
**timne0** *February 21, 2018 21:59*

Two other cables to the board that aren't steppers ![images/4cfca58965b0602a0fa310029dee0939.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4cfca58965b0602a0fa310029dee0939.jpeg)


---
**timne0** *February 21, 2018 21:59*

Current power supply![images/c83393b062e3457bb2f2477b69f4f445.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c83393b062e3457bb2f2477b69f4f445.jpeg)


---
**timne0** *February 21, 2018 22:00*

![images/2a1c1ac68831ccd214340aa9ac9138d5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2a1c1ac68831ccd214340aa9ac9138d5.jpeg)


---
**timne0** *February 24, 2018 08:02*

**+Don Kleinschnitz** does that help? 24v supply just arrived so will try that first.


---
**Don Kleinschnitz Jr.** *February 24, 2018 13:59*

I found a drawing for converting a stock machine to that same supply. In your case your are using a C3D instead of a nano so there are some differences outlined below.



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/m1hP6ZM2XKnJDgws1)



.................................

For your setup replace the M2 with the wiring you have for your C3D.

 

<b>If I understand your setup:</b>



Ignore the 5V wiring from the LPS to the Nano. You do not want to use that 5V for the C3D.



Keep these wires short and sizable with good connections:

... Wire the +24V PS to the C3D

... Wire ground from the 24VDC to the C3D ground

... Wire FG ground from the LPS to a screw on the frame

... Wire frame ground from the 24V PS to the same a screw on the frame 



<b>Laser control</b>

Wire the connection that used to go from the C3D to L on the old supply to TL on the new one. 

Ideally use a twisted pair and carry ground from the C3D to the ground on the LPS.



If this is to confusing let me know and I will draw a diagram specifically for your setup. 




---
**timne0** *February 27, 2018 14:57*

Excellent, seems like perfectly good buy in that case.  I'll do some cutting tomorrow and see what happens.  I probably need a 50w for the 50w tube anyway.


---
**timne0** *February 28, 2018 19:23*

ah, which one is the L line on my current supply I wonder! :)


---
**Don Kleinschnitz Jr.** *February 28, 2018 21:33*

I cannot see the labels on the connectors but it should be the rightmost pin on the rightmost connector.

It's labeled on the pcb.


---
**timne0** *February 28, 2018 21:34*

don't worry, done, see youtube vid on other thread.  Got mixed up


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/2BbKReZ9txp) &mdash; content and formatting may not be reliable*
