---
layout: post
title: "I just replaced my old K40 with a new one with the digital control panel"
date: December 20, 2017 18:44
category: "K40 and other Lasers"
author: "Tony Sobczak"
---
I just replaced my old K40 with a new one with the digital control panel. Wired it up just like the old one and everything seems to be fine except that the laser is on during moves.  It's a very faint line but deep enough to be difficult to sand out.  Any ideas out there where I may have gone wrong??



TIA





**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 19:20*

Pictures?


---
**Tony Sobczak** *December 20, 2017 19:32*

P2.5 to L1 on PSU.![images/262e61643b03ff00d302b2c663678c0a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/262e61643b03ff00d302b2c663678c0a.jpeg)


---
**Tony Sobczak** *December 20, 2017 19:35*

24V wire from PSU not used. Comes from external unit. It engraves, cuts and does everything it should but fires on moves.  What others photos would you like.![images/cef8ac6aac318722693297886abfb3b7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cef8ac6aac318722693297886abfb3b7.jpeg)


---
**Tony Sobczak** *December 20, 2017 20:55*

Problem resolved. Cause was my repetitive dyslexic dumbassedness review of my completed work. P25 was actually open. Work's like a champ now.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/TJMUmxKD4wc) &mdash; content and formatting may not be reliable*
