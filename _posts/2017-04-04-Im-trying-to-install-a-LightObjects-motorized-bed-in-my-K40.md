---
layout: post
title: "I'm trying to install a LightObjects motorized bed in my K40"
date: April 04, 2017 02:40
category: "K40 and other Lasers"
author: "Peter Grace"
---
I'm trying to install a LightObjects motorized bed in my K40.  I'm pretty sure I've got the stepper wired up properly, but I can't get motion out of the motors.  I'm checking my assumptions before digging deeper: in regards to the smoothie config that ships with the C3d Mini K40 upgrade kit, does it ship with z-axis enabled to the point where jogging the z-axis from the LCD should produce motion of the z-axis stepper, or is there more config required in the smoothie settings (beyond setting proper steps/mm?)  This is my first foray into Smoothie, I'm much more familiar with Marlin.





**"Peter Grace"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 02:45*

First step, hook up a loose stepper or the k40 y stepper to the Z socket and try jogging that. See if you get motion. There's a grey area regarding whether an integrated driver can provide enough current - the LO z table is fairly hefty. 



The stock PSU definitely can't handle it and you should get a separate 24v psu to power the board. 



Other thoughts: more current on the A4988 (turn pot 90 degrees CW so the flat faces right). 



Config should have Z enabled for sure, I didn't change anything from the stock config there. 



Make sure you save, safely eject, and reset the board after changing the config. 


---
**Peter Grace** *April 04, 2017 02:53*

If I wanted to mount an external TB6600 stepper driver to handle the Z, are there dedicated ports to grab the enable/step/dir on the mini, or should I jumper from the A4988 socket?


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 02:55*

External driver is certainly the other option, the A4988 socket is where the step dir en pins are, and we make an adapter to make the breakout easy if you need it. 


---
**Don Kleinschnitz Jr.** *April 04, 2017 03:04*

This post may be helpful:

[donsthings.blogspot.com - K40-S Lift Table Integration](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)


---
*Imported from [Google+](https://plus.google.com/+PeterGrace2013/posts/7QAAagNLUTB) &mdash; content and formatting may not be reliable*
