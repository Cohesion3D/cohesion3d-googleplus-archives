---
layout: post
title: "Hi - ive seen quit a few conflicting photos of wiring for the Cohesion K40 laser can someone help me confirm mine looks good?"
date: February 01, 2017 10:09
category: "General Discussion"
author: "Alec Short"
---
Hi - ive seen quit a few conflicting photos of wiring for the Cohesion K40 laser can someone help me confirm mine looks good? too scared too power up... 



My laser config settings below







![images/f4062da2e037da9189ce69bcb7112577.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f4062da2e037da9189ce69bcb7112577.jpeg)
![images/11eb69597bae6674f7828306c79383c2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11eb69597bae6674f7828306c79383c2.jpeg)

**"Alec Short"**

---
---
**John Sturgess** *February 01, 2017 10:26*

You want the next pin along, Fet -(as shown in the picture uploaded by Colin Rowe recently) for the laser trigger wire and you need to change the laser module pin from "2.4!" to "2.5" (no exclamation mark), you may also need to change pwm period to 200 if you notice you can't vary the power through LaserWeb.


---
*Imported from [Google+](https://plus.google.com/109973479751741687593/posts/QUczShvkW43) &mdash; content and formatting may not be reliable*
