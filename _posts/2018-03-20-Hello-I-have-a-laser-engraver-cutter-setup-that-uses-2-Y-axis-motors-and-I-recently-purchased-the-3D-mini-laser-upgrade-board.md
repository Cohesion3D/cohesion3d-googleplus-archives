---
layout: post
title: "Hello! I have a laser engraver/cutter setup that uses 2 Y axis motors and I recently purchased the 3D mini laser upgrade board"
date: March 20, 2018 01:46
category: "C3D Mini Support"
author: "Lewis Richards"
---
Hello!

I have a laser engraver/cutter setup that uses 2 Y axis motors and I recently purchased the 3D mini laser upgrade board. I was wondering if anyone has a similar setup and how or has anyone figured out how to connect 2 Y axis motors ?





**"Lewis Richards"**

---
---
**LightBurn Software** *March 20, 2018 19:00*

There isn’t a way that I’m aware of to slave them in software, however you can wire the motor coils in parallel to the same driver, as long as the driver is rated to handle the required current. Alternately, you wire two external drivers to the same step / direction signals and run one motor per driver.


---
**Lewis Richards** *March 20, 2018 23:47*

The two external driver idea sounds like it might be the best way to go. Thank you, I will give that a shot.


---
*Imported from [Google+](https://plus.google.com/113844851691518181561/posts/ZtvhcZGgCCU) &mdash; content and formatting may not be reliable*
