---
layout: post
title: "Installed LightBurn but is not doing anything"
date: January 21, 2019 21:52
category: "Other Softwares"
author: "Allen Russell"
---
Installed LightBurn but is not doing anything. Do I have to purchase a License before it will work with Cohesion3D Mini ?





**"Allen Russell"**

---
---
**LightBurn Software** *January 21, 2019 22:18*

What do you mean by "It is not doing anything"?  You might need to be more specific.  You don't have to purchase a license to use the 30 day trial.


---
**Allen Russell** *January 21, 2019 23:04*

**+LightBurn Software** Installed Lightburn but is not doing anything, does not show that I am connected to it. Does not move in any direction.


---
**Allen Russell** *January 21, 2019 23:05*

**+Allen Russell** reinstalled all software and nothing


---
**LightBurn Software** *January 21, 2019 23:06*

Which board do you have, and have you been through the device setup, and selected a COM port?


---
**Allen Russell** *January 21, 2019 23:15*

**+LightBurn Software** I have the Cohesion3D Mini, have not done a device setup, and selected a COM port?


---
**Allen Russell** *January 21, 2019 23:16*

**+Allen Russell** I did device setup with lightburn


---
**LightBurn Software** *January 21, 2019 23:19*

When you plug the USB cable in, does the computer make the "ding" to tell you it found something, and do you see the SD card mounted?  If you click the drop-down box for the COM port, do you see any ports listed, as shown here?  If not, then the board isn't properly set up.  Is the SD card in the board?  If not, then there'd be no firmware for it, and it wouldn't start up.

![images/ba4586f2e258258531c19fe9697908c1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ba4586f2e258258531c19fe9697908c1.png)


---
**Allen Russell** *January 21, 2019 23:24*

**+LightBurn Software** sd card is mounted , software does not give me an option to choose com port 


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 23:40*

Do you have any other software like LaserWeb open? That would block. Restart and open just LightBurn and try again. 


---
**LightBurn Software** *January 21, 2019 23:42*

"Software does not give me an option to choose a COM port" - Do you mean that all you see in that drop-down is "(Auto)"?  If that's true, then you likely need to install a driver.  If there are any COM ports in the system, used or not, they'll show up in that list.


---
**Allen Russell** *January 22, 2019 02:03*

**+LightBurn Software** Do you mean that all you see in that drop-down is "(Auto)"? YES and no I don'y have any other programs running. If I need a driver what is it called ?


---
**Allen Russell** *January 22, 2019 18:06*

OK, I got it running but when I hit the home button it goes all the way to the top left Corner and grinds. Need help, is there something I have to change in the config file ? thank you in advance 


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 18:08*

I don't know if you've changed the board config before.  It might be worth grabbing the latest config and firmware files from the bottom of our install guide.   Power down completely and plug in your Y motor cable the other way. It should move to the rear left when homing.


---
**Allen Russell** *January 22, 2019 18:24*

ok thanks , Ill give it a try


---
**Allen Russell** *January 22, 2019 18:40*

**+Allen Russell** OK, I only have one cable and I turned it around and tried it it was a no go. I put cable back and took pictures, here they are

![images/b452cab8babd3c3368aef5c8c8175f95.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b452cab8babd3c3368aef5c8c8175f95.jpeg)


---
**Allen Russell** *January 22, 2019 18:40*

**+Allen Russell** 

![images/801defe712030aad74b5a7ef5b0d6a99.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/801defe712030aad74b5a7ef5b0d6a99.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 23:20*

If you flipped the Y motor lead,  the Y should move to the opposite direction when homing and homing should succeed. 


---
**Allen Russell** *January 23, 2019 03:40*

OK, Thanks


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/Snovjbs9is4) &mdash; content and formatting may not be reliable*
