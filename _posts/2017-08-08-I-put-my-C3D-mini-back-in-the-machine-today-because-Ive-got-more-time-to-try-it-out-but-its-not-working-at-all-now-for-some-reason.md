---
layout: post
title: "I put my C3D mini back in the machine today because I've got more time to try it out but it's not working at all now for some reason"
date: August 08, 2017 11:52
category: "C3D Mini Support"
author: "Kma Jack"
---
I put my C3D mini back in the machine today because I've got more time to try it out but it's not working at all now for some reason.



I have a solid red led, top and bottom leds solid and the middle two flashing. 

I can connect to the board and it finds the firmware but now it won't jog, test fire or do anything at all. 



I have read both the C3D mini and LaserWen instruction and did everything accordingly but getting nowhere. 



I've been trying it for a couple of hours now, making sure all the settings are correct and cables connected properly but still nothing. 



Am I missing something? 



Why won't the machine even jog when LaserWeb finds it and connects to it?



Someone help please.



TIA 





**"Kma Jack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 14:36*

What USB cable are you using? 

What is the operating system of your machine? 

Can we see a screenshot of LaserWeb? 


---
**Kma Jack** *August 08, 2017 14:41*

Hi Ray, thanks for the reply.

I am using Win7 and the USB cable from my 3D printer. 

I can't make any screen shots at t he moment as I am installing Ubuntu in the hope that I'll have better luck with it


---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 14:43*

Ok, make sure it's a good quality cable and not one of the crap blue ones that come with the Chinese machines. 


---
**Kma Jack** *August 08, 2017 19:08*

Hi Ray, I am still having problems. I installed the latest version of Ubuntu and LW4 but am stuck on "permissions", it won't let me connect to the machine. I have changed the USB cable to a better one but made no difference.  I've posted in the LE forum hoping someone can help




---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2017 21:20*

I've just reviewed the other thread, is this portion resolved now?


---
**Kma Jack** *August 08, 2017 21:22*

Yes, it is Ray, thank you. 

I do have another problem but will leave it for another day




---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/38aX3nBpWcL) &mdash; content and formatting may not be reliable*
