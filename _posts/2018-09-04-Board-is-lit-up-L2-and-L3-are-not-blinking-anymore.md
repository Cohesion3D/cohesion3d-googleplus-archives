---
layout: post
title: "Board is lit up. L2 and L3 are not blinking anymore"
date: September 04, 2018 00:04
category: "C3D Mini Support"
author: "Lashley Guyton"
---
Board is lit up. L2 and L3 are not blinking anymore. All are solid. Computer does not read device anymore. What do I need to do?  Nothing happened just turned it on today and this is what I got. 





**"Lashley Guyton"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2018 23:13*

I suspect your memory card has become corrupted.  Try putting it in a reader and into your computer to see if it shows up and you can see the config and firmware files on it. 


---
**Lashley Guyton** *September 05, 2018 19:57*

**+Ray Kholodovsky** that was it sir. Thank you very much for the help. 


---
*Imported from [Google+](https://plus.google.com/114884907345259366479/posts/QCrwavYdRSh) &mdash; content and formatting may not be reliable*
