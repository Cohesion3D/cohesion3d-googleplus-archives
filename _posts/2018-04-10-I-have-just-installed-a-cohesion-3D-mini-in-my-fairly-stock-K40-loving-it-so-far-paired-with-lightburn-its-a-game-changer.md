---
layout: post
title: "I have just installed a cohesion 3D mini in my fairly stock K40, loving it so far, paired with lightburn its a game changer"
date: April 10, 2018 09:27
category: "K40 and other Lasers"
author: "Mark Kenworthy"
---
I have just installed a cohesion 3D mini in my fairly stock K40, loving it so far, paired with lightburn it’s a game changer. 



Something I want to try to do is have the board+lightburn control my air assist system via a relay. 



The only reservation I have with this plan at the moment is that due to limited power outlets in my workshop the laser and the air assist pump are one the same outlet. When I switch on the air assist the c3d mini seems to hiccup, windows tells me again that it sees a new storage device and sometimes it interrupts the cut. If this gets synchronised to the start of each cut it could give me issues. 



My reaction is that I want to stick a nice solid cap across the 24v input to the c3d to try to smooth it a bit. 



Is this a bad idea? Anyone got any other ideas?



Power supply is the original one. 





**"Mark Kenworthy"**

---
---
**Don Kleinschnitz Jr.** *April 10, 2018 12:41*

When you say original supply do you mean the 24 on the LPS?



It sounds like your AC source is drooping when the pump load is added.

I would measure the voltage at the outlet and verify how much the AC is drooping. 

The cap might work on the 24V DC supply but I would worry that if the AC is drooping enough other connected devices will also be impacted like the PC driving it and the LPS. 



I would try and get rid of the droop. The droop suggests that either the pump is a huge load or you are on a branch of the AC source that cannot handle this load. I have seen this kind of droop in my shop when I start my table saw at the end of an extension cord. 


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2018 04:57*

Situation is three-fold: 



Board experiences disconnect: ok, try the NOMSD firmware build (Jim Fong posted here recently), make sure you are using a high quality USB cable...



Stock 24v in the k40 is utterly horrible and we recommend people install a separate 24v psu to power the board more reliably. 



Noise when AC appliance is turned on: it is not unheard of for problems to happen when one's fridge or air compressor on the same AC circuit turns on. 


---
**Mark Kenworthy** *April 12, 2018 10:22*

**+Ray Kholodovsky** Thanks Ray, what sort of 24v power supply should I be looking at? is 50W enough? more?


---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2018 19:11*

We recommend 24v 6a.  One of the LED Style ones off ebay would do it. 


---
*Imported from [Google+](https://plus.google.com/105379149839333709549/posts/g2zR87whBu1) &mdash; content and formatting may not be reliable*
