---
layout: post
title: "Hello. Yesterday I installed my new C3D mini board into my K40 laser"
date: January 15, 2018 21:19
category: "C3D Mini Support"
author: "Mark Kirkwold"
---
Hello. Yesterday I installed my new C3D mini board into my K40 laser. I got it to the point where it would home correctly and I did a test cut of a square out of a piece of cardboard, so I think the wiring is OK. (I am happy to go over that, I just don't think that is the problem.) 



Today I think I am experiencing an early board failure. This morning I commanded a home sequence. Both motors jerked and the board stopped responding. I reset the board, etc, nothing worked. On power-up the red LED would come on (<b>much</b> brighter then when powered by USB only), the first green LED would blink and then nothing but the red LED. 



I suspected the SD card, so I removed it and plugged it into an SD card reader on my computer. The computer recognized the reader, but saw no card. The SD card appears to be dead. So, I replaced that SD card with one from another Smoothie board. I put the config and FW files from your dropbox onto that SD card; reinstalled. With just USB power I get a red led and 4 green leds lit, but no blinking. The computer does not enumerate any usb objects. Turning on the main power (24V supply to the board) I get only the red led (bright) and nothing else. 



Any ideas?







**"Mark Kirkwold"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2018 21:22*

Can you post some pictures of your wiring and board installation please?


---
**Mark Kirkwold** *January 15, 2018 22:13*

[drive.google.com - k40_wireing.jpg](https://drive.google.com/open?id=1YLgoYcpu4_GZYpP6Q6T2EF2RFHy8IwPW)

The red wires are 24V, GND and Laser fire.


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 14:01*

Please email us (info at Cohesion3D dot com) with your order # and reference to this, and we'll get you sorted out. 


---
*Imported from [Google+](https://plus.google.com/107985881342308921169/posts/6NbhJxGSrgu) &mdash; content and formatting may not be reliable*
