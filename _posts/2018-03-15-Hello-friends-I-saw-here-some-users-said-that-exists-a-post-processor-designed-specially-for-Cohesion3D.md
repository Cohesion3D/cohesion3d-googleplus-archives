---
layout: post
title: "Hello friends. I saw here ( ) some users said that exists a post processor designed specially for Cohesion3D ."
date: March 15, 2018 03:39
category: "General Discussion"
author: "Antonio Hern\u00e1ndez"
---
Hello friends. I saw here ([https://benboxlaser.us/index.php?topic=2983.0](https://benboxlaser.us/index.php?topic=2983.0)) some users said that exists a post processor designed specially for Cohesion3D . Where I can find it ?. I want to try to export some drawings with that post processor. If exists... does it output gcodes for A B and C  rotatory axes ?





**"Antonio Hern\u00e1ndez"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2018 16:00*

Since your question is about T2 laser perhaps it would be productive to ask in that forum. A grand total of 3 people use T2 that hang out here, and I am not one of them :) 


---
**Antonio Hernández** *March 15, 2018 20:36*

Ok Ray, thanks for the info !.


---
**Anthony Bolgar** *March 16, 2018 13:12*

Your best option would be to purchase Lightburn for $30. It is designed to work with the C3D mini board (and many others) and is miles ahead of any other laser software you are currently using. There is a 30 day free trial, and can be purchased on Rays site.


---
**Antonio Hernández** *March 16, 2018 14:21*

Ok. I'll check that program and see the specs. Thanks for the info !


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2018 19:07*

To be clear, rotary support in Lightburn is still being worked on - it's close - but not publicly released yet.  That would be for A axis.  



I am not aware of any program that does more than that - nor how you would do this on a laser.  



XYZABC multi-axis machining - programs that make gcode for this are rare and expensive.  Fusion 360 might be an exception. 


---
**Antonio Hernández** *March 16, 2018 19:23*

I saw an smoothieware post processor published over Autodesk site. Fusion360 could use smoothie post processor and create gcode files for CNC and Laser machines (only for machines based on Smoothieware). Apparently it works for x, y and z without problems. Other users found and suggest the use another post processor over ArtCAM (probotics arc...) to work with smoothie (it gives better results than autodesk smoothie post processor, one user said that). Autodesk post processor(to be used with Fusion360) It's not updated to work for A, B and C axis. I was wondering which post processor (and what machine's manufacturer) could fit to use it's post processor and do some testings using rotatory axis over smoothieware based boards. (maybe AC or BC as fourth and fifth rotatory axes, depending on machine's design). CamBAM with generic post processor also could do the job (only for 3 axes). Maybe a generic post processor for 4 and 5 axes could do the job ?. I hope that...


---
*Imported from [Google+](https://plus.google.com/107343842763021715447/posts/6ejaA8J5pxt) &mdash; content and formatting may not be reliable*
