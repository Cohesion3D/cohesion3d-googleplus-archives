---
layout: post
title: "Printed this on my upsized Cobblebot: Scaled up to 750mm x 250mm x 200mm Sunlu Gold PLA 0.4mm Nozzle 0.32mm layer height vase mode Took about 40 hours to print"
date: May 17, 2018 19:39
category: "Show and Tell"
author: "Dushyant Ahuja"
---
Printed this on my upsized Cobblebot: 

[https://www.thingiverse.com/thing:1546001/#files](https://www.thingiverse.com/thing:1546001/#files)

Scaled up to 750mm x 250mm x 200mm

Sunlu Gold PLA

0.4mm Nozzle

0.32mm layer height vase mode

Took about 40 hours to print

![images/2f11d1d8b64677d036f32438ce1d46d7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2f11d1d8b64677d036f32438ce1d46d7.jpeg)



**"Dushyant Ahuja"**

---


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/BGgFPaww42Y) &mdash; content and formatting may not be reliable*
