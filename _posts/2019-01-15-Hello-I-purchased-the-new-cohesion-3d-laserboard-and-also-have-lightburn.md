---
layout: post
title: "Hello, I purchased the new cohesion 3d laserboard and also have lightburn"
date: January 15, 2019 19:17
category: "C3D Remix Support"
author: "giuseppe Ilmago"
---
Hello,



I purchased the new cohesion 3d laserboard and also have lightburn.  For some reason everything looks like it is installed correctly and lightburn reads the laser, but when I go to print, the laser worked for a few seconds and then stopped.  I have attached a picture of the output.  A few things:



1) the laser works when I press the laser test button

2)I checked this board prior to sending and tried sending the code G1 X10 S0.6 F600 throught Lightburn and it did not move the head or send the laser

3)the laser initially worked and then stopped working, so I don't think it is a cohesion problem but probably a signal or something

4) I saw it could be the potentiometer, i tested it with a multimeter and it says it's working, but I am not sure why when I send the code G1 X10 S0.6 F600 I get nothing

5) As far as the wire knot with the red and yellow, I have it connected directly to potentiometer, with Gerbil I needed it connected differently, but i triple checked all my connections and they all seem fine

6) I sent a picture of the glass I was engraving, you can see it began to work, and works when I press the laser test button, but not on it's own



Any help would be great!

Giuseppe



![images/ec24c30cf37bdd8ffa6e5376990d9848.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec24c30cf37bdd8ffa6e5376990d9848.jpeg)
![images/d65147395bb4149de9898c1fda1e64b7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d65147395bb4149de9898c1fda1e64b7.jpeg)
![images/c55aa45768f58a1dc71748efef3e10a3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c55aa45768f58a1dc71748efef3e10a3.jpeg)
![images/ebb89e89df73753226f5ff70aaecf677.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ebb89e89df73753226f5ff70aaecf677.jpeg)

**"giuseppe Ilmago"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2019 19:37*

My understanding is that the gerbil board replaces the pot. The C3D does not. You would need the pot present and wired as it is on stock machine in order for the LaserBoard to work properly. I also see white wires going to the blue wire harness. Please elaborate on what all of this is. It does not look like what a stock K40 machine does.


---
**giuseppe Ilmago** *January 16, 2019 00:07*

Hello, the pot was reconnected properly as a stock, it just has that wire knot which I wanted to inform you, because it looks strange. As far as the white wires, that is just a reflection and they are not going in the blue harness.


---
**giuseppe Ilmago** *January 16, 2019 00:09*

But that still leaves me with the issue that the laser started working, and then faded away and will not work any longer, unless I press the laser test button


---
**giuseppe Ilmago** *January 16, 2019 00:32*

![images/1634eadc1b3b745aabf05f1f9489ed76.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1634eadc1b3b745aabf05f1f9489ed76.jpeg)


---
**giuseppe Ilmago** *January 16, 2019 00:33*

this just to show that it is jumped exactly where it should be as stock

![images/57fcc144b10e3198b2e8300627878c0b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57fcc144b10e3198b2e8300627878c0b.jpeg)


---
**giuseppe Ilmago** *January 16, 2019 00:35*

Just a few more pictures to clarify the connections. I don't think so much it's the board but maybe something strange going on

![images/1a8f540e72ec3282c4e1f470f95da313.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1a8f540e72ec3282c4e1f470f95da313.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2019 00:36*

Are the G1 lines still firing the laser a little bit and then it fades or does the laser not fire at all from the board now?  


---
**giuseppe Ilmago** *January 16, 2019 00:55*

no it is not firing at all.  On the initial use it fired a bit and then faded, but now it will not fire at all


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2019 01:21*

Maybe the L wire is loose in the large socket with the blue wires.  You can unplug the plug from the board and touch L to GND to see if that fires the laser.  That is what the board is doing. 


---
**giuseppe Ilmago** *January 16, 2019 19:52*

Thank you for that, it was the issue about the laser not firing so we got that part fixed.  Unfortunately the laser is still stopping after a few seconds of firing.  I have attached a video to show.  

![images/ff928a7951bdd40d8e3ebe67b50e0efb](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ff928a7951bdd40d8e3ebe67b50e0efb)


---
**giuseppe Ilmago** *January 19, 2019 01:09*

Just wanted to check if you or anyone else may be able to help figure out what's causing this issue in the video above? 


---
*Imported from [Google+](https://plus.google.com/118036785421071885491/posts/TnFcMshzQHh) &mdash; content and formatting may not be reliable*
