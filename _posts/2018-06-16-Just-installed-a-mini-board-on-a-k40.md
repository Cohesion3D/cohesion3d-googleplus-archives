---
layout: post
title: "Just installed a mini board on a k40"
date: June 16, 2018 22:26
category: "C3D Mini Support"
author: "Ernest Washburn"
---
Just installed a mini board on a k40. Prints upside down bottom left instead of the designation of top left in LightBurn.



Otherwise everything looks good. 



What did I do wrong? 



Thanks, 





**"Ernest Washburn"**

---
---
**LightBurn Software** *June 16, 2018 22:36*

Probably what the instructions told you not to, and told LightBurn that the origin is in the upper left, instead of the lower left.



![images/e7460c40828b632ad356bebf6fec17c3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e7460c40828b632ad356bebf6fec17c3.png)


---
**Ernest Washburn** *June 16, 2018 23:49*

**+LightBurn Software** yup, thanks. 


---
**Ernest Washburn** *June 16, 2018 23:49*

**+Ernest Washburn** hahaha, instructions. 


---
**Ernest Washburn** *June 16, 2018 23:56*

**+Ernest Washburn** this upgrade and LightBurn is a game changer for me. Might save me $1000s setting up my shop. 


---
**J edd** *June 18, 2018 14:29*

So, just for me (with the thick head), for the K40 Laser the origin should be Left Front or Left Rear. I selected the smoothie, however, when I choose the default orgin and Lightburn attempts to go to the left rear of the cutting area, it goes all the way to the end and rattles as there is no stops and I have to turn off the K40 Laser.


---
**LightBurn Software** *June 18, 2018 20:49*

**+J edd** a K40 does have limit switches at the rear left. If your machine is hitting those and not stopping it’s likely the “home on startup” behavior and a bad switch or loose wire.


---
**J edd** *June 22, 2018 10:16*

I sorted out the problem, The limit switch is there, however my LightObject air assist nozzle is preventing (hitting rear bar) them from being hit. Everything is working now.




---
*Imported from [Google+](https://plus.google.com/115773505743356991966/posts/BptPrJ2SU8W) &mdash; content and formatting may not be reliable*
