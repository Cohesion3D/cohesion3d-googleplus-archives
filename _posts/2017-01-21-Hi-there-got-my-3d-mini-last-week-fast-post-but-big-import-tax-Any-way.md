---
layout: post
title: "Hi there, got my 3d mini last week (fast post but big import tax) Any way"
date: January 21, 2017 15:36
category: "General Discussion"
author: "Colin Rowe"
---
Hi there, got my 3d mini last week (fast post but big import tax)

Any way. I have a K40 with a Moshi MS101050 V4.7 board.

Just looking how to hook it all up and followed the post that has been put up.

BUT. connectors on both board and on PSU are totaly different and I dont have a clue what does what and what goes where.

Please have a look at the pictures and see if any one has done this conversion before?

Thanks Colin



![images/e6d7308b912175c4510f13e8108e17e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e6d7308b912175c4510f13e8108e17e7.jpeg)
![images/0eccad4317b3668356d2c5f65694a02d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0eccad4317b3668356d2c5f65694a02d.jpeg)
![images/95f6ea52fddcfc7d5418f42b2e9222ee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/95f6ea52fddcfc7d5418f42b2e9222ee.jpeg)
![images/bd28c9ff5c360307417a0ca616fe3852.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bd28c9ff5c360307417a0ca616fe3852.jpeg)
![images/3bc797b7c19ea1ec531fa9e050342221.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3bc797b7c19ea1ec531fa9e050342221.jpeg)
![images/26c58a005cab9c0a2b46f7b1c70acf3f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26c58a005cab9c0a2b46f7b1c70acf3f.jpeg)
![images/01078aec19db120d07d543002bae1f88.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/01078aec19db120d07d543002bae1f88.jpeg)

**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 15:50*

Will take a look tonight. You can check the group, there are a few other people with red boards. 


---
**Colin Rowe** *January 21, 2017 15:50*

Thanks


---
**Andy Shilling** *January 21, 2017 17:11*

I have the same board as you. my psu was different though. I have now got my new psu and will be picking up the C3D from the post office tomorrow. I was just going to rewire everything ie adding new switches for water pump, air assist etc. 



the connections on your moshi board are all labeled, when you unplug them, the bottom plug CN4 is the y axis stepper motor, the middle connector is CN5 are the +24 and +5v  to run the board and  connect back to the laser and the ribbon is for the x stepper motor and optical endstops.



I'm not sure if that will help you but I wont be starting my upgrade for a few days yet. Anything else I can help with just ask and I'll try my best.






---
**Colin Rowe** *January 21, 2017 17:28*

Thanks. Will not be doing it for a while.  Will be putting in water sensor and lights. Along with air. Also need to sort out z axis table. 


---
**Andy Shilling** *January 21, 2017 17:33*

Ok once i've started I will come back and post how I get on. Hopefully I wont need to bug **+Ray Kholodovsky** to much lol




---
**John Sturgess** *January 21, 2017 17:47*

Have a look at my post further down the page, my machine is also fitted with a moshiboard. My mini arrived today and i hope to get it fitted tomorrow, i'm going to cut the ends off the CN5 connector (moshiboard end) and put the 24V & GND onto the main power terminals and the connect the "L" to the Bed- 2.5 terminal. The 5V isn't needed so i'll remove that wire altogether.


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 18:09*

Bed- is 2.5 make sure you take note of that and adjust the pin in config from 2.4! to 2.5 accordingly. 


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 04:21*

Ok, I'm seeing a ribbon cable, a 4 pin, and a 6 pin. The ribbon should pop in directly to Mini, the 4 pin is probably a motor so that goes in the Y motor output. John has the right idea regarding the 6 pin "CN5". You need to pull 24v and gnd from there to feed to the main power in terminal on mini. Then you need to connect L to the -bed 2.5 terminal. Finally you'll need to make a change in the config.txt to change the laser pin from 2.4! to 2.5

Based on the other experiences we've had, I think you should not hook up the PWM cable. Keep the pot hooked up as it was, and with that config line change, you should be all set. 


---
**Colin Rowe** *January 22, 2017 08:49*

Ok. First bit makes sense.  What is L?  And what is -bed 2.5 also I assume POT is the large thing that I have seen bolted to side of some lasers. Mine does nor have that.


---
**Andy Shilling** *January 22, 2017 09:04*

I'm guessing L is the trigger from the C3D back to the Psu and the - bed 2.5 terminal is on the Mosfet terminal, if you refer to the pinout diagram its on the bottom labelled Mosfet or FET1Bed




---
**Andy Shilling** *January 22, 2017 09:07*

O and Pot will be the potentiometer you have to control the laser power originally next to the ampmeter.



corrections welcomed for the more experienced guys.


---
**Colin Rowe** *January 22, 2017 09:12*

Thanks people. Will have a look and hope not to release any magic blue smoke. (It's a bugger to get it  back in after it comes out)


---
**Colin Rowe** *January 22, 2017 09:15*

Sorry for questions but if I leave the pot on (adjusts power) will that reduce the power across the laser like an ovwrride 

as power is set by S in G code.?


---
**Andy Shilling** *January 22, 2017 09:17*

That my friend is above my knowledge, you will have to wait for one of the big guns to come answer that one. Sorry


---
**John Sturgess** *January 22, 2017 11:09*

The potentiometer will effectively set the maximum power in this scenario, press the test fire button and adjust the pot until you are happy with the reading. Anything in the code will now be a proportion of that value. 


---
**Colin Rowe** *January 22, 2017 17:48*

found this, all looks correct

![images/b0d7512c2b80d3b92c99b3bb29a9e539.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0d7512c2b80d3b92c99b3bb29a9e539.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 17:31*

Hey Colin,

So to recap,

ribbon cable goes to Mini.

remaining motor cable goes to Mini.  That should be the Y axis.

You've correctly identified 24v, Gnd (G), and L in the terminal block on the right, with L being the latest pin.  You can cut the 6 pin connector and use those wires, or just unscrew the wires from the terminal on PSU and run your own basic wires.  24v and Gnd need to go to the main power in terminal on Mini, and L needs to go to the -bed 2.5 on Mini.  

John's answer is correct regarding "pot sets the ceiling, software sets a proportion of that value".


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/934HEF3Gi73) &mdash; content and formatting may not be reliable*
