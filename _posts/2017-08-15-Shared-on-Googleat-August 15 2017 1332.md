---
layout: post
title: "Shared on August 15, 2017 13:32...\n"
date: August 15, 2017 13:32
category: "K40 and other Lasers"
author: "William Kearns"
---


![images/e8c336f1f0786743804e092736ba3558.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e8c336f1f0786743804e092736ba3558.jpeg)



**"William Kearns"**

---
---
**Don Kleinschnitz Jr.** *August 15, 2017 13:49*

??? and the video posted an error.


---
**Griffin Paquette** *August 15, 2017 20:51*

Can we get some context??


---
**William Kearns** *August 15, 2017 20:54*

It went with a post I made previously but could not get the video to load through my phone when replying to a post. It's a z axis motor in a lightobject z table that barley moves when jogged. Second motor in


---
**David Komando** *August 15, 2017 21:07*

I own this thing and it's a giant FAIL!  However, to just get it working I had to pop all the wires out and change their order then it all worked as well as it could.  


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 21:45*

Oh yeah, this is a good point. You need to identify the coil pairs and put them in that order into the tb6600 in a different order than they are on the motor side plug. 


---
**William Kearns** *August 15, 2017 23:13*

I swapped the motor out with one from steppermotor online. Ok is that something I missed on the doc site on the web site?


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 23:21*

Least likely that the motor itself is bad.  

As said, need to look up the datasheet for a stepper and wire the A1 B+ and A- B1 (or notated as A1 B1 A2 B2) accordingly as per the notations on TB6600, or the TB is bad. Ashley and I have run into a few of those.  Also worth confirming that you've set up the config.txt correctly ( !o at the end of the step dir en pins for gamma). 






---
**Griffin Paquette** *August 16, 2017 01:29*

Like Ray said check the coiled pairs. If you have a multimeter you can check for continuity across the wires to find the two pairs. The two that are connected are a pair and should be wired as such.


---
**William Kearns** *August 16, 2017 04:38*

ok started to get movement by changing the config file to up the limits on the z axis but nothing still like it should be? And I switched out the tb6600 for another one i had on hand. Also I dont get any movement as well untill i change the microsteps to 2/B micro step 400 pulse/rev the motor has been replaced with a 17HS19-2004s1


---
**William Kearns** *August 16, 2017 05:12*

Should I just and build this z table out to run manually and not through the board and LW4?


---
**William Kearns** *August 16, 2017 23:13*

**+Ray Kholodovsky**  Ray any other settings that might be tweeked to get it to move faster. It moves but at this rate would take a year to move the table up and down 


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2017 23:36*

If it moves, that's a good sign. Sounds like you just need to adjust your steps per mm for your table and max speed/ max feedrate for gamma in config.txt

Then increase your Z travel speed in the LW to a higher value. 


---
**William Kearns** *August 17, 2017 02:42*

#z_junction_deviation what does this do?


---
**Ray Kholodovsky (Cohesion3D)** *August 17, 2017 11:06*

That's the one you don't touch. 


---
**David Komando** *August 17, 2017 13:40*

I had to loosen mine just to have it move. Ended up more frustrated prior to me even having a z-table. I am currently in the process of building my own scissor z-table.


---
**William Kearns** *August 17, 2017 17:22*

hmmm thininking I might need to build one out of lead screws


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/ZAgUrgHpShA) &mdash; content and formatting may not be reliable*
