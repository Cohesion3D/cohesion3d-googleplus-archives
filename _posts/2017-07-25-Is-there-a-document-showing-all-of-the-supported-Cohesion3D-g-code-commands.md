---
layout: post
title: "Is there a document showing all of the supported Cohesion3D g-code commands?"
date: July 25, 2017 15:47
category: "C3D Mini Support"
author: "Ralph Freshour"
---
Is there a document showing all of the supported Cohesion3D g-code commands?





**"Ralph Freshour"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 16:22*

Smoothieware --> supported gCodes



Also please post here with your wiring question and I will be able to reply faster. Need to see pictures of everything and better understand what you are trying to accomplish. (Can use 24v laser power supply to power the Mini, not an external 12v supply) 


---
**Ralph Freshour** *July 25, 2017 16:55*

I am the developer of BenCutLaser, a CAM software program for laser machines. While BCL was initially designed to support low power laser diode machines, I am expanding support to include CO2 laser machines. I recently completed work to support the Smoothie board. So I think what you're saying is that, if I use the same framework, the same Smoothie g-code commands for the Cohesion3D Mini board should work the same.


---
**Ashley M. Kirchner [Norym]** *July 25, 2017 20:20*

C3D Mini board runs Smoothieware's firmware. So your thinking is correct.


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 21:35*

Right.  The C3D runs Smoothie. 



There is also a case for grbl-lpc firmware being used by some C3D/ Smoothie hardware owners with LaserWeb.  It was ported by the LaserWeb devs.



This has to do with communications limitations/ programming differences of opinion between the Smoothie and LW devs.  In short, streaming a raster from LW to smoothie over USB does seem to experience a stutter due to commands not getting processed fast enough. Grbl-lpc can raster blazing fast over usb. 



I'd be curious if you can implement raster to smoothie over USB (as opposed to the current methodology of copy file to sd card and run from there - either via glcd menu or the play command). The way to do this is the [fast-stream.py](http://fast-stream.py) script in smoothieware github, and potentially configuring a second serial channel in config on which you could query ? to get position and send emergency commands, while the main channel is getting hammered with raster gcode lines. 


---
**Ralph Freshour** *July 25, 2017 21:46*

BenCutLaser implements the raster to controller board method at all times. However, I only have the C3D board and I don't have a CO2 laser machine to test out your request.



As soon as one of my users starts to use BCL with their C3D CO2 laser, I'll have an answer for you. But I suspect it will work fine. The Smoothie co-developer told me their preferred method is the same: raster to controller board with one g-code statement at a time...and that is exactly how BCL works.




---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 21:49*

You don't need a co2 machine. Just a motion frame. Send a raster. See if the motion in x stutters.


---
**Ralph Freshour** *July 25, 2017 22:15*

I'm working right now on adding photo and image engraving to BCL (about 90% finished). Give me a day or two to add the fractional laser power format (S0.50) for images and then I should be able to do the test for you and report.


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 22:16*

Cool. 


---
*Imported from [Google+](https://plus.google.com/107278089739264680170/posts/24YgZZSntGp) &mdash; content and formatting may not be reliable*
