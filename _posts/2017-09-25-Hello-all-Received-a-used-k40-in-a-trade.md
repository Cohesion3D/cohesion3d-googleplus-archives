---
layout: post
title: "Hello all. Received a used k40 in a trade"
date: September 25, 2017 08:14
category: "K40 and other Lasers"
author: "Keebler Rockwell"
---
Hello all.   Received a used k40 in a trade.  The previous owner cut the bottom out, assuming to add the lightobject rotary that was included. 

It had a m2nano board in which i have already swapped over to a cohesion board with additional stepper drivers and lcd upgrade.  Tested movement and homing, the k40 homes and moves correctly.   Have not tested the tube yet, but here is my question.....



Will the lightobject rotary work with cohesion board with no modification?   Plug into axis and enable in laserweb? And adjust the gcode in settings?  



Looking at adding the lightobject z table as well.  I found the walkthrough on that.  



Thank you in advance. 









**"Keebler Rockwell"**

---
---
**Joe Alexander** *September 25, 2017 09:30*

if i recall the lightobject table has a motor that draws more power than the pololu style chips generally can handle but that's what the 4-pin header is for. break it out to an external driver and it should work.


---
**Ashley M. Kirchner [Norym]** *September 25, 2017 14:43*

[cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**Ray Kholodovsky (Cohesion3D)** *September 25, 2017 19:35*

Exactly, the LO table and rotary take more current than the A4988 driver can provide.  You also need to get an external power supply because the one in the K40 is barely powerful enough to power itself, much less any additions. It's all in that article Ashley linked. 



We do have the external stepper adapters listed on the web store, and I have some TB6600 drivers here as well, so if you'd like to get those from me please just let me know. 


---
*Imported from [Google+](https://plus.google.com/113806793515492117334/posts/bq3rhZhbeHj) &mdash; content and formatting may not be reliable*
