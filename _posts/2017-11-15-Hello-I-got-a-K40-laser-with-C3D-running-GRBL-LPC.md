---
layout: post
title: "Hello I got a K40 laser with C3D running GRBL-LPC"
date: November 15, 2017 14:12
category: "General Discussion"
author: "bojan repic"
---
Hello



I got a K40 laser with C3D running GRBL-LPC.

With laser engraving and cut rate set to 250mm/sec the laser stops for a fraction of the second many times on the engraving part.

Where laser just moves but does not cut it moves smoothly without stopping.

I am running LW4, latest version 991.

I am runing it on Win 7 64 bit on 2 core processor.

Even if I slow down speed to 150mm/sec it is the same.

Any ideas?



Thank you

Bojan





**"bojan repic"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2017 17:21*

The stuttering has been known to happen with the smoothie firmware and LaserWeb, but should not happen between LaserWeb and Grbl-LPC on the Cohesion3D Board. 



What USB Cable are you using?  If it is the one that came with the k40 please get another one of decent quality, and preferably with ferrites on the ends to rule out any noise. 



Try the Lightburn software when it is release in the coming (weeks).   It uses the proper communications protocols so to not have any stuttering on Smoothie and especially not GRBL-LPC.


---
**Jim Fong** *November 15, 2017 17:51*

Please Post a Video of the stuttering, the gcode lw4 file, graphics file and your grbl-Lpc settings.  Grbl-lpc is able to handle much faster raster engravings but without knowing more, it will be difficult to find a solution.  


---
**bojan repic** *November 15, 2017 19:50*

I am not using K40 USB cable but another, hope it is better one.

I got same problem with Smoothie and GRBL.

I am waiting for Lightburn to come out.

Laser is stopping on the parts where is text to engrave, on solid lines it is ok. I am engraving stamp, so it is burning everything but text.


---
*Imported from [Google+](https://plus.google.com/115068391387492902728/posts/gzMteGjWvCh) &mdash; content and formatting may not be reliable*
