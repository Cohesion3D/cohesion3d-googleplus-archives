---
layout: post
title: "just done a file to engrave and the feed came out minus..."
date: March 03, 2017 17:51
category: "Laser Web"
author: "Colin Rowe"
---
just done a file to engrave and the feed came out minus...

no idea why and its done it a few times, have to save the g code and do a mass edit.





**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 17:51*

Please clarify. 


---
**Colin Rowe** *March 03, 2017 18:23*

![images/39e2998d6c6bf4a3072b5d2851edad9e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39e2998d6c6bf4a3072b5d2851edad9e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 18:27*

Show settings please. 


---
**Colin Rowe** *March 03, 2017 18:30*

Sorry back home now. But did a few little jobs ok then imported a jog and just set white speed to 60 and black to 40 then clicked generate gcode. 


---
**Colin Rowe** *March 03, 2017 18:30*

Feeds were minus


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/fg5ijoNY1JR) &mdash; content and formatting may not be reliable*
