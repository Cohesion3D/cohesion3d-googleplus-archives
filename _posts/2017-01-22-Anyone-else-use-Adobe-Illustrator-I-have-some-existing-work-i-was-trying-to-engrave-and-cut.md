---
layout: post
title: "Anyone else use Adobe Illustrator? I have some existing work i was trying to engrave and cut"
date: January 22, 2017 02:49
category: "General Discussion"
author: "Bill Keeter"
---
Anyone else use Adobe Illustrator? I have some existing work i was trying to engrave and cut. So I converted the engraving layer to BMP and cut to SVG. 



I'm having some trouble with the new setup. See the photo below. That weird "text" is supposed to be an engraved URL into a sheet of acrylic. First I tried 300 dpi BMP and then tried 150. Is there a setting I forgot or maybe something in the smoothie config?



FYI, it's cutting the SVG file okay.



Before you ask, yes I was engraving this same part without issue with the original board.



![images/4d5e248d39a106a8aab2acb25002d5b7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4d5e248d39a106a8aab2acb25002d5b7.jpeg)



**"Bill Keeter"**

---
---
**Ariel Yahni (UniKpty)** *January 22, 2017 03:05*

**+Bill Keeter**​ I can only think in adjusting power and speed. I assume you are using  LaserWeb?


---
**Bill Keeter** *January 22, 2017 03:14*

**+Ariel Yahni** yeah, you're probably right. Easy to miss something when you're a noob to a tool.



![images/90ec113c9332655cbea3de8ded665586.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/90ec113c9332655cbea3de8ded665586.jpeg)


---
**Ariel Yahni (UniKpty)** *January 22, 2017 03:17*

Each file in LW will need an operation. And each operation will show a set of options. Also each file will be a TAB at the top of grid


---
**Ariel Yahni (UniKpty)** *January 22, 2017 03:18*

Also i see you have 100 % min y max. If you image is all black then you could leave it as min 0


---
**Bill Keeter** *January 22, 2017 03:27*

**+Ariel Yahni** That was the original issue. If I left Min at 0 then small text didn't even cause the laser to fire as it traveled over it. Even though it's black text. Very strange. Could it be an anti-alias issue in the BMP? Or some other setting when i'm converting the vector text to BMP in Illustrator?


---
**Alex Krause** *January 22, 2017 03:39*

What is your beam diameter set in the settings?


---
**Alex Krause** *January 22, 2017 03:40*

Also is the text black that you are trying to engrave or a shade of gray


---
**Bill Keeter** *January 22, 2017 03:45*

I've tried the gambit. 0.5 to 0.1. The photo above is 0.1. Which from closer inspection is obviously too small.


---
**Ariel Yahni (UniKpty)** *January 22, 2017 03:48*

0.15 it's a sweet spot for me. It should fire 100% if it's totally black. Maybe try it with a png


---
**Alex Krause** *January 22, 2017 03:53*

Besides the controller did you add any other upgrades such as an air assist nozzle or change your focal lens?


---
**Bill Keeter** *January 22, 2017 05:13*

**+Peter van der Walt** Yeah, I started with 300 dpi. Then tried 150 and 72. 



I just spend the last 30 minutes exporting different versions. With alias, without, interlace, whatever. Also tried 200 and 400 feedrate. Then played with the power min and max.



If I run a 300 dpi file. i'm getting this (see pic)



Now wondering if it's a power issue. Maybe I need to add the pot back into the loop for PWM.

![images/00b1f0a5c5a099fb03b7435567fd36a5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/00b1f0a5c5a099fb03b7435567fd36a5.jpeg)


---
**Bill Keeter** *January 22, 2017 05:26*

**+Alex Krause** I did upgrade to an adjustable head, focus lens and air assist. But those were working last week before I swapped boards. So I assume those shouldn't be an issue here.


---
**Claudio Prezzi** *January 22, 2017 12:01*

I would also try it with a much slower feed rate (like 50mm/s)


---
**Bill Keeter** *January 22, 2017 19:39*

**+Claudio Prezzi** yeah, that did the trick!. I left the default feed rate settings of 20mm/s and it engraved beautifully. Well other than running at 100% power. Which cut right throw the acrylic. Guess I'll have to see how fast I can run the feedrate and keep a clear image. Anyway glad it's working now. 



Thanks everyone for helping out. 


---
**Ariel Yahni (UniKpty)** *January 22, 2017 19:43*

**+Bill Keeter**​ you should be able to go faster, acrylic is really easy to mark


---
**Alex Krause** *January 22, 2017 20:26*

Also I mark acrylic with the paper peeled off the side I'm engraving on


---
**Bill Keeter** *January 22, 2017 20:55*

**+Alex Krause** It depends on the project for me. If i'm doing a deep engrave, logos etc, I leave the paper on. So you get a clean surface when you remove the paper afterwards. If i'm doing something detailed, photos etc, then yes I remove the paper first.


---
**Alex Krause** *January 22, 2017 21:27*

I apply a liquid mask if doing a deep engrave... Dawn dish soap


---
**Bill Keeter** *January 24, 2017 04:06*

**+Ray Kholodovsky** Just wanted to follow up that I put the pot back in and ran the L wire to bed 2.5. This fixed my engraving issues. I can now run at 400mm/s and engrave a clear image. I did discover that my laser diameter is around 0.1mm (I have the saite cutter head)



In LaserWeb I have to keep the power at 30%+ or some finer details in the BMP don't cause the laser to fire. Then adjust the pot to the correct mA.


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 04:09*

Wonderful.  I'm going to start recommending that method from now on.  Just need to document it...


---
**Alex Krause** *January 24, 2017 04:12*

**+Bill Keeter**​ you can up the "tickle" power just a bit to get the use of more range 


---
**Bill Keeter** *January 24, 2017 04:18*

**+Alex Krause** I'm assuming it's laser_module_minimum_power. What would be a good number to bump up to? 0.1 or 0.2?


---
**Alex Krause** *January 24, 2017 04:19*

I think I did mine around .05 - .08 or it will leave a faint burn while making rapid moves... 


---
**Alex Krause** *January 24, 2017 04:20*

You can dial it in by running a 0-100% test image


---
**Alex Krause** *January 24, 2017 04:22*

![images/9f71238b910bd3658c2bd522d49bd04a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9f71238b910bd3658c2bd522d49bd04a.jpeg)


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/87PBcT7jNe5) &mdash; content and formatting may not be reliable*
