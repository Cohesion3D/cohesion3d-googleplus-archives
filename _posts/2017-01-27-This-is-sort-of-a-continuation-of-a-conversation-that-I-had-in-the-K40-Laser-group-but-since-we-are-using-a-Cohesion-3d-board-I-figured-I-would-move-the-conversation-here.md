---
layout: post
title: "This is sort of a continuation of a conversation that I had in the K40 Laser group, but since we are using a Cohesion 3d board, I figured I would move the conversation here"
date: January 27, 2017 10:01
category: "General Discussion"
author: "Ryan Branch"
---
This is sort of a continuation of a conversation that I had in the K40 Laser group, but since we are using a Cohesion 3d board, I figured I would move the conversation here. 



Basically the first problem that I had was that I couldn't get our laser to fire below 30%. Basically smoothie wouldn't send any current to the laser unless it was set to 30% or greater. 29% or lower, and no current would be sent to it.



The solution was that our minimum power setting in the smoothie config was set to 0.0. After I set it to 0.1 though, it was finally starting to fire below 30%. I think it wouldn't go any lower than 20% though. I could be wrong about that, I will have to check next time I am near the laser. I still don't know why that solved it though. If anyone has any insight to this, I would love to know more!



The current problem that I am running into has to do with the raster engraving. I tried doing a bunchnof test cuts and rasters at different settings (see image below) and I noticed that there was a steep drop off in power, instead of a casual increase. You can notice that I would basically get nothing, then a very light raster, and finally the rest would just be completely black. With the rectangular test pieces, each square was a 10% power difference from the next one. I think I did a 5% power difference as well on one of those test pieces. 



The other etchings near the center top was actually a Jpeg grayscale image. It had 5 different shaded bars. The large one was through Laserweb, and it got 3 of the 5. Although the really light one isn't fully engraved. Then the other ones that look like black lines were using the same image but with Visicut. (I was trying to see if it was a gcode problem, but it wasn't). That one just kind of shot through the wood and then had a really light raster for the rest. 



What do you think could cause the wierd raster? Are Co2 lasers power not very linear? When Rastering, should we adjust the speed and the power? I always thought when you raster something you should leave the speed the same and just adjust the power output, but if the difference between 20% is a very light gray and 25-30% is black, then I am assuming that we would need to adjust the speed as well.? Could this maybe have something to do with the Power scale setting in smoothie? Sorry for all the questions 😔



**+Ray Kholodovsky**​ In the other thread we also talked about how we have our k40 wired up. We have an early prototype of the Cohesion mini board so our wiring will be alot different than most, but I attached some pictures below to show how we have it set up. 



Basically we have L from the power supply going into a header near the power input(The one with the pink and purple cables), along with the center wiper of the pot, which connects to the same header onto a pin labeled "pwm". The pot has been removed however. I don't have a pinout to this particular board so I am not exactly sure what pin that pwm is going to, though I am assuming it is probably going to pin 2.4, unless the L is going to pin 2.4. 



For some reason when I hooked the pot back up and made the changes to make l run off pin 2.5 the laser didn't fire at all, but I am willing to try again in case I wired it wrong. I also removed the blue "pwm" pin from the connector when I wired the pot back in. I assumed the pot would be overriding that.



![images/1566e540ce1452af357bf0490f9f3aac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1566e540ce1452af357bf0490f9f3aac.jpeg)
![images/0f5e76789d76f4b1bd119ebe9e3a4f89.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0f5e76789d76f4b1bd119ebe9e3a4f89.jpeg)
![images/3f168b0966f70666a596e3163d5c1164.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f168b0966f70666a596e3163d5c1164.jpeg)
![images/fb812ae3a16053b4063a910ff3a24e2d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fb812ae3a16053b4063a910ff3a24e2d.jpeg)
![images/6d1485afaaaba0689976facbe5de866f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d1485afaaaba0689976facbe5de866f.jpeg)

**"Ryan Branch"**

---
---
**Ryan Branch** *January 27, 2017 10:02*

Link to the other conversation [https://plus.google.com/101842495658501740623/posts/dE4k3aAVhkn](https://plus.google.com/101842495658501740623/posts/dE4k3aAVhkn)


---
**Don Kleinschnitz Jr.** *January 27, 2017 11:38*

**+Ryan Branch** I'm pretty confused at this point. Can you provide a wiring diagram as to what you now have for the PWM circuit.


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 17:17*

Don they have a 2 wire setup with the pot swapped out. Functionally the same circuit as on the new board - level shifted 5v from 2.4 for PWM and 2.6 controlling L. This is what the board used to look like around summertime. 

In order to run just L (with the pot back in play) to 2.5 you would have to run a wire to bed - which should be the 4th pin from the left of that 6 wide green terminal block on the mini. 


---
**Robin Sieders** *January 27, 2017 22:04*

**+Ryan Branch** Would you be able to provide that file you're using for testing?


---
**Ryan Branch** *January 27, 2017 23:21*

**+Ray Kholodovsky**​ Yeah that's exactly what I did. I also disconnected the blue pwm pin on that header. I am not sure why I couldn't get the laser to fire, but I might try hooking it up again that way to make sure I did it right the first time. **+Robin Sieders**​ Yeah, the one that looks like a little bank check?


---
**Robin Sieders** *January 27, 2017 23:45*

**+Ryan Branch** That's the one. If you don't mind


---
**Ray Kholodovsky (Cohesion3D)** *January 28, 2017 01:30*

Ryan, so yeah, you disconnected the blue wire (from the psu terminal I presume) but did you plug the wire(s) from the pot back in? 

And you ran L (rightmost pink wire) to "FET-" on your board as discussed earlier, and changed the config laser pin from 2.4! to 2.5



All that and no joy?


---
**Ryan Branch** *January 28, 2017 08:13*

**+Robin Sieders** [github.com - K40-Laser-Cutter](https://github.com/haqnmaq/K40-Laser-Cutter/tree/master/Test%20Cuts)


---
**Ryan Branch** *January 28, 2017 08:17*

**+Ray Kholodovsky** Yep, I did exactly that with no luck. I put the pot in various positions and ran the gcode again without any luck. I checked the pot with a multimeter as well, but it was functioning correctly. Next time I am near the laser cutter though, I will try that again. Maybe it was a combo of the smoothieware problem and the way we have it set up. 



I mean technically the laser cutter works just fine, I am just trying to get the raster to behave a little differently and not have such a steep difference between 20% and 30%.


---
**Ryan Branch** *January 28, 2017 11:14*

I think I might know the problem to the raster problem. I looked through **+Don Kleinschnitz** Blog and G+ posts, and I noticed that our pwm value in the config is very low. We have it set to 20us instead of the 200us that Don recommends. I will try changing that when I am near the laser again. Based on the graph he just posted as well, it seems to echo those results at our current pwm value.



I also noticed that with your config file Don, you have a bunch of "switch.laserfire. etc" settings. Are those basically an override of the manual laser switch on the k40? Is there any benefit to having those in there? 



BTW, your posts have been a huge help. Thanks Don!



Here is a link to our current config. [drive.google.com - config.txt - Google Drive](https://drive.google.com/file/d/0B8ZXvH-aGzn9M2d4bHhMUWFrN3M/view?usp=sharing)




---
**Don Kleinschnitz Jr.** *January 28, 2017 13:59*

**+Ryan Branch** delighted my work is doing good.... and hope that soon you will get the doppelgangers out of your machine.



Donations always accepted to buy more power supplies, parts and such for research: 



My goals:

.....Set up a testing bench for PWM HV control research. 



......Get better digital control of the K40 supply while finding out why they fail.



.......A repair guide and parts list (although I haven't yet convinced myself it can be safe).



.......A wiring guide for multiple supply types



........A vendor list.



........A DIY testing device that is safe and a HV safety manual.



Some day I will actually use my K40 to make parts for the many ideas I have.

..........................

The switch.fire settings I left in from the original config I plagiarized. I do not use it and haven't found a reason to yet? I doubt this has anything to do with your problem.


---
**Wolfmanjm** *January 29, 2017 00:02*

FWIW I prefer to use the existing fire switch on the physical K40 and not one controlled by Smoothie. DItto for the enable switch on the K40.  I highly recommend following Dons instructions to put switches on the doors to disable everything if a door is open.


---
**Don Kleinschnitz Jr.** *January 29, 2017 00:33*

I live in fear that careless K40 folks will have unintentional retinal surgery or have a permanent EM pulse RESET.


---
**Bill Keeter** *January 30, 2017 04:44*

**+Ryan Branch** Thanks for sharing about the 20us needing to be 200us. I was trying to figure out why everything was engraving too deep.


---
**Ryan Branch** *January 30, 2017 16:08*

**+Bill Keeter** I actually just tried it with the 200us and unfortunately I got the same results as before. Did it work for yours?


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 16:09*

Someone had to do 400 before seeing changes...


---
**Bill Keeter** *January 30, 2017 16:37*

**+Ryan Branch** Yeah, it seemed to help. I need to do more testing tonight but yesterday I was able to leave the pot at about 12mA and do a decent engrave. Before if I didn't manually put the pot MUCH lower I would get a 1+ mm deep engrave into the acrylic.


---
**Don Kleinschnitz Jr.** *January 30, 2017 16:44*

**+Bill Keeter** along with the PWM period the pots power has to be set low enough as such it does not saturate the Smoothies PWM control.


---
**Bill Keeter** *January 30, 2017 16:51*

**+Don Kleinschnitz** I should have specified. When my PWM period was 20us my engrave setting was set at 20% power in LW. It was still cutting 1mm into 3mm acrylic. Well if I left the pot at 12 mA.


---
**Ryan Branch** *January 30, 2017 21:23*

**+Ray Kholodovsky**​ I tried wiring the pot up again along with running L to the pin 2.5, but I still didn't have any luck. I took a photo for good measure. The white wire goes from L to pin 2.5. Not sure why it isn't working for us. Maybe the power supply? 



**+Bill Keeter**​ I also did switch the config to 200us from the 20us I had before. I honestly couldn't tell much of a difference, but I left it at that and plaued with some settings in laserweb to mixed results.

![images/565d4c76edcf767a363e0eac986f35e9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/565d4c76edcf767a363e0eac986f35e9.jpeg)


---
**Wolfmanjm** *January 30, 2017 21:55*

L needs to go to the ground of a mosfet so it is open collector (or open drain) a bare pin probably will not work.


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 21:57*

His white wire is going to to - of the MOSFET which is controlled by pin 2.5.  If you just touch that wire to ground, will the laser fire? 


---
**Ryan Branch** *January 30, 2017 21:58*

I agree that wiring the the pwm signal to L will be the best option, I just can't figure out how to get our power supply to work. I wonder if I can wire the current pwm (the blue wire on that 3pin connector) to L instead of N alomg with re-connecting the pot. I would think that would accomplish the same thing, just with using pin 2.4 instead of pin 2.5. The reason I am thinking thay is that I know pin 2.4 works, not sure about pin 2.5 though on our board. Is pin 2.4 controlled by a similar FET? Do you forsee any problems with this method **+Ray Kholodovsky**​?


---
**Don Kleinschnitz Jr.** *January 30, 2017 22:14*

**+Ryan Branch** what **+Ray Kholodovsky** said. Start by disconnecting the white wire from the C3D and gnd it to see if the laser fires. Assumes all interlocks etc are closed with laser enabled.



If not the LPS "L" is not working.



If it fires.....



Reconnect the white wire & insure that there is a low true pulse on L. Do you have  a scope?


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 22:38*

Don this one is up to you. 2.4 is MOSFET controlled and a hardware PWM pin. The only thing is that it has a pull-up to 5v. If you set the config to 2.4 without the ! You should be able to do L on that pin. When it is off the psu will see 5v on L. 


---
**Don Kleinschnitz Jr.** *January 30, 2017 22:47*

**+Ray Kholodovsky** not sure what "Up to me" means :)

Having a pullup on the driving side may work but if there is a - voltage difference between the 5VDC in the LPS and the C3D 5VDC the LPS may turn on when its supposed to be off.

The L pin is the cathode of an LED and if it draws enough current it may enable the supply. The L interface is not really digital and has no hysteresis. 

Can you disconnect the Pullup?


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 22:49*

The pullup is a tightly positioned 0603 and no, I would not recommend "disconnecting" it.  Anyways that sounds like a maybe it will work from Don, so give it a shot :)


---
**Don Kleinschnitz Jr.** *January 30, 2017 22:52*

**+Ray Kholodovsky** if it does work consider that changes in the two 5VDC power supplies due to loads could cause the laser to fire. 

You may find out it fires on power transitions to be careful.


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 22:57*

Hence why I am having people use a different mosfet pin all the other times :)  Ryan be careful.


---
**Wolfmanjm** *January 30, 2017 23:45*

On my genuine smoothieboard I have L connected to the gnd of the 2.4 mosfet no pullups, and it works fine. The L fires when Low it seems so no ! is needed either.


---
**Ryan Branch** *January 31, 2017 00:57*

**+Don Kleinschnitz**​ **+Ray Kholodovsky**​ Thanks for the feedback. I will give these a try next time I am at the Makerspace. Unfortunately I just cant est things immediately like if it was at my house. We do have a scope at the space. Try connecting it to pin 2.5 to see if there is a pwm signal?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 00:59*

Yeah 2.5 is connecting and disconnecting to gnd so you could use any other positive voltage as the reference. 


---
**Don Kleinschnitz Jr.** *January 31, 2017 01:37*

**+Ryan Branch** definitely connect to L, that will eliminate the mystery. Should be a low going pulse that varies with smoothie PWM  DF.


---
**Ryan Branch** *February 02, 2017 16:58*

Sorry for the inactivity. I did some troubleshooting on Tuesday. The power supply is good. When I touch L to gnd it fires and the pot does control the power of the laser just fine. I still wasn't able to get pin 2.5 to fire the laser however. I put a scope on pin 2.5 as well, but wasn't able see any signal. I am going to try again later today however with a different scope. The mosfet looks fine visually (no scorch marks), so I am not going to rule out user error (me) yet. I might try setting the laser pin to the hotend mosfet on the board to see if it is that mosfet. Pin 2.7 I think. Then try the same troubleshooting process. 



I will let you know what I find out!


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 17:01*

Backup your config. Format the card. Put a clean config from smoothie's github on. Might be a good time to update the firmware as well. 


---
**Ryan Branch** *February 04, 2017 18:03*

So unfortunately I didn't have any luck with pin 2.7 either. I think for now I am just going to leave it wired as is. Maybe someday we will upgrade to a production unit mini, and I might swap it then. For now though we are getting good cuts and ok rasters. Enough that our members can get good use out of it. Thanks for your help **+Ray Kholodovsky**​ **+Wolfmanjm**​ **+Don Kleinschnitz**​


---
*Imported from [Google+](https://plus.google.com/101842495658501740623/posts/3D3RksJ5ZQa) &mdash; content and formatting may not be reliable*
