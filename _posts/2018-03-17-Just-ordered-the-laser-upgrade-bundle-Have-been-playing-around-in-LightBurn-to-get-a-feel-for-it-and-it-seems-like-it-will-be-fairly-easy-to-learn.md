---
layout: post
title: "Just ordered the laser upgrade bundle! Have been playing around in LightBurn to get a feel for it and it seems like it will be fairly easy to learn"
date: March 17, 2018 09:49
category: "General Discussion"
author: "Jamie Richards"
---
Just ordered the laser upgrade bundle!  Have been playing around in LightBurn to get a feel for it and it seems like it will be fairly easy to learn.  Can't wait for the controller to get here so I can do some actual grayscale instead of dithering!  Not having very good results dithering photos on acrylic for edge lit pieces, but it was doing ok on wood.  K40 Whisperer is an excellent program for the stock controller, but it's time to move up!



Wonder why they changed the red (cut) in inkscape to blue and vector the red?  I'll adapt!





**"Jamie Richards"**

---


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/NWpaHM3HEU7) &mdash; content and formatting may not be reliable*
