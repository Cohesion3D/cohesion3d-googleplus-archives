---
layout: post
title: "Just a heads up for anyone using LaserWeb 3, and having issues getting your laser to fire after the upgrade"
date: January 18, 2017 04:23
category: "General Discussion"
author: "Robin Sieders"
---
Just a heads up for anyone using LaserWeb 3, and having issues getting your laser to fire after the upgrade. You need to add M3 to your start gcode and M5 to the end gcode.





**"Robin Sieders"**

---
---
**tyler hallberg** *January 18, 2017 04:25*

Wish that worked for me, still no firing on my end. Hoping someone has upgraded the new machines with the digital display to one of these.


---
**Robin Sieders** *January 18, 2017 04:28*

will do, thanks for the heads up


---
**Claudio Prezzi** *January 18, 2017 08:28*

**+Peter van der Walt** Doesn't the fire command do this itself?

But I relized a "problem" with the Laser Test Power setting. On Smoothie, this value is %, on Grbl, it is S-Value.


---
**Claudio Prezzi** *January 18, 2017 08:34*

Ok, I will add this to the code.


---
**Claudio Prezzi** *January 18, 2017 09:00*

Ok, it's in. Could someone with a two pin configuration test it please!


---
**Claudio Prezzi** *January 18, 2017 09:01*

Need to do a "git pull" first.


---
*Imported from [Google+](https://plus.google.com/+RobinSieders/posts/XmYtS4sYGa7) &mdash; content and formatting may not be reliable*
