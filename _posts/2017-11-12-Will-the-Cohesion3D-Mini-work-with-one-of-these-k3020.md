---
layout: post
title: "Will the Cohesion3D Mini work with one of these k3020?"
date: November 12, 2017 19:09
category: "C3D Mini Support"
author: "Tammy Mink"
---
Will the Cohesion3D Mini work with one of these k3020? I got my new (used old one) and hooked it up like the old laser and seems it can't move it laser and when it homes it goes crazy. It seems to be the ribbon type setup.



![images/e68c768eaf7fc952e4cefae1cbd204af.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e68c768eaf7fc952e4cefae1cbd204af.jpeg)
![images/4f44743295989a51d5f21e5163220b15.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f44743295989a51d5f21e5163220b15.jpeg)

**"Tammy Mink"**

---
---
**Tammy Mink** *November 12, 2017 20:50*

I suppose I might might very well be doing something wrong..or something but I figured I would inquire and include pictures to cover my bases. If you need more pictures just let me know.


---
**Tammy Mink** *November 12, 2017 22:24*

It is sort of acting like there is no endstops, but given it's the ribbon cable type I am It sure I could have gotten it wrong (blue facing outward)


---
**Anthony Bolgar** *November 12, 2017 22:28*

Metal facing in, blue facing out is correct.


---
**Tammy Mink** *November 13, 2017 01:08*

I don't if it's the board (this was the same one from the fire, but appeared undamaged) but seems I can't get to read the USB port, I managed to get it to go once but now not at all. Even when it did read the USB port homing was all wonky. At the moment I can only get the red light to go on the Cohesion3D mini...going to try putting the m2nano board in there to see what happens..maybe my Cohesion3d mini is fried




---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 01:39*

First please disconnect the ribbon. 

Try jogging the Y motor. Then move that motor plug over to X socket and try jogging X. 


---
**Tammy Mink** *November 13, 2017 01:46*

Nope not the motherboard, I put in the m2nano and I can't get past it trying to home... horribly grinding all the while. Not sure if it can see the usb... because it won't stop trying to home. It's almost like the endstop is bad. I have a physical endstop with the lever I could put in there but I don't think it's this kind and have no idea how to fix that. Anyways not the Cohesion3D minis fault as far as I can tell so I'll stop posting here. I guess I shouldn't have gotten my hopes up...best I can do now is try and check all the wire connections again .


---
**Tammy Mink** *November 13, 2017 01:51*

**+Ray Kholodovsky** I'll give that a try tomorrow night. 


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 01:55*

If you can jog fine.. and you hook up the ribbon and can jog everything fine... then just put the head to the origin for the time being. 

You might Lightburn to not home on connect. 


---
**Tammy Mink** *November 13, 2017 02:44*

Definitely can't jog. USB won't detect (tried other cables). I did get it briefly to go but that's when trying to home on LightBurn, LaserWeb or K40 Whisper(couldn't jog ) or in the case of the M2nano just turning it on makes it just keeps trying to home forever. 


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 02:46*

To be clear, can't jog with the stock board either? 


---
**Tammy Mink** *November 13, 2017 03:11*

Nope because it keeps trying to home when I turn it on, I took the ribbon off and got it to connect in LaserDrw I think but couldn't jog thier either (but maybe not connecting the ribbon confused it). 


---
**Tammy Mink** *November 13, 2017 16:34*

At best by playing with the wires I could get the Y axis to jog using the M2Mnano board from my old machine. If while it's working pull the connection it of course doesn't move as expected but even if I switch it to the X-axis while it's all on the y-axis still moves. Maybe the machine is incapable or there  is something with the a-axis wires, motor or something. Not the Cohesion3D mini fault clearly though.


---
**Arjen Jonkhart** *November 14, 2017 16:15*

When I had the horrible grinding it was because the Y axis was at home position, and still trying to move up. Have you tried reversing the 4 pin wire?


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/Y3tSwBg7mgt) &mdash; content and formatting may not be reliable*
