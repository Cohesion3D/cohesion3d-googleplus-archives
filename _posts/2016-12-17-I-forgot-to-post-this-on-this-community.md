---
layout: post
title: "I forgot to post this on this community"
date: December 17, 2016 21:00
category: "General Discussion"
author: "Dushyant Ahuja"
---
I forgot to post this on this community. 

[https://goo.gl/17dKiM](https://goo.gl/17dKiM)

Smoothiefy your 3D Printer | 3D PRINTER CHAT





**"Dushyant Ahuja"**

---
---
**Richard Vowles** *December 18, 2016 01:27*

I have an Ultimaker Original +, is it worth it? What capability does the infrastructure need to allow a smoothie board to be worthwhile?


---
**Dushyant Ahuja** *December 18, 2016 11:26*

**+Richard Vowles** upgrading 8-bit boards to a 32-bit board. The 8-bit boards basically have no capacity left over for complex calculations like calculating acceleration at each step or compensating correctly for an uneven bed. The motion planning is great and the movements are "Smooth" and give great finishes on perimeters and corners. I know Marlin and Repetier have auto bed leveling - but I never got it to work. On Smoothie it was a piece of cake. 


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/dVbi8pp14CP) &mdash; content and formatting may not be reliable*
