---
layout: post
title: "Q. re: bed size setting and stepper movement not in synch while the cut size is OK"
date: January 21, 2019 00:48
category: "K40 and other Lasers"
author: "Em W"
---
Q. re: bed size setting and stepper movement not in synch while the cut size is OK.



Hi, Hope someone can give me some troubleshooting advise.



The video will demonstrate the problem and show what is working and what isn't. 



Thank you ahead of time.



Sorry to bother but tagged both Cohesion and Lightburn since I don't know if an Sd config or software issue or user knowledge issue.



**+Ray Kholodovsky**

**+LightBurn Software**




{% include youtubePlayer.html id="IfI-ZuM1uEI" %}
[http://youtu.be/IfI-ZuM1uEI](http://youtu.be/IfI-ZuM1uEI) 





**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:36*

You need to Home then just jog and count to figure out the max travel for each axis, particularly the Y. 



Whatever you set in LightBurn you must also set in the config file, particularly the Y. 



[lasergods.com - Setting Custom Bed Size For The Cohesion3d Mini](https://lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)


---
**Em W** *January 21, 2019 19:13*

**+Ray Kholodovsky** I understand what you are saying, except can you let me know exactly what line of code to change for the max travel setting you talk about. 



Would that be this line, for example: 



beta_max_rate                                30000.0          # mm/min



And when you say jog to count: the default is 10 set in the distance field in the "move" section of Lightburn. Is this is ten steps per ? (not at laser so can't jog right now to measure the distance this move makes) and then count how many time it moves and then multiply by the 10 to get the number of steps to = beta_max_rate , if that is even the one to change? Although in thinking this, I don't think it would come anywhere near the 30000 for beta_max_rate. So yeah, not tracking with this yet.



Great info, thanks!


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 02:21*

That guide I linked shows it exactly.


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/W6jQ5q8zHgM) &mdash; content and formatting may not be reliable*
