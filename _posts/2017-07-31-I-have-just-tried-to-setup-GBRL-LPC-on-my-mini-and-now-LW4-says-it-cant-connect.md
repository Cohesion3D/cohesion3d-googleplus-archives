---
layout: post
title: "I have just tried to setup GBRL-LPC on my mini and now LW4 says it cant connect"
date: July 31, 2017 15:41
category: "FirmWare and Config."
author: "David Cantrell"
---
I have just tried to setup GBRL-LPC on my mini and now LW4 says it cant connect. I turned the board completely off, copied firmware.bin and restarted the board. What do I do now? Is 115200 the correct baud rate? How do I recover if there was a bad flash? Can I rename the firmware.cur that was on the card when I got it back to firmware.bin to reflash Smoothie?

![images/519dff0bfb03d2d1e7f1f8d3cce74e2e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/519dff0bfb03d2d1e7f1f8d3cce74e2e.png)



**"David Cantrell"**

---
---
**Todd Fleming** *July 31, 2017 16:49*

Which one did you flash? Only some of the grbl-lpc releases support the Cohesion3D board.


---
**David Cantrell** *July 31, 2017 16:52*

I flashed the one from this post [cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)




---
**Todd Fleming** *July 31, 2017 16:55*

Odd; that one should have worked. Any baud rate will work; grbl-lpc's driver ignores it. Can you connect to it using a terminal? (e.g. PuTTY on Windows machines)


---
**David Cantrell** *July 31, 2017 17:48*

Yes, Putty does talk to the board. It replies with:



Grbl 1.1f ['$' for help]

[MSG:'$H'|'$X' to unlock]



Grbl 1.1f ['$' for help]

[MSG:'$H'|'$X' to unlock]




---
**David Cantrell** *July 31, 2017 17:49*

Are there any setting changes I need to make on the LW4 side?




---
**Todd Fleming** *July 31, 2017 17:51*

It should just see it. What LW version?


---
**David Cantrell** *July 31, 2017 17:53*

![images/c3d824342129048e8ff786e2e9872fa0.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c3d824342129048e8ff786e2e9872fa0.png)


---
**Todd Fleming** *July 31, 2017 17:55*

Newest. **+Claudio Prezzi** any ideas?


---
**David Cantrell** *July 31, 2017 21:26*

Just to see what happened, I used Putty to tell Grbl to "Unlock", whatever that means. When I try to connect I get a different error.



Grbl 1.1f ['$' for help]

[MSG:'$H'|'$X' to unlock]



Grbl 1.1f ['$' for help]

[MSG:'$H'|'$X' to unlock]

[HLP:$$ $# $G $I $N $x=val $Nx=line $J=line $SLP $C $X $H ~ ! ? ctrl-x]

ok

[MSG:Caution: Unlocked]

ok


---
**David Cantrell** *July 31, 2017 21:27*

![images/fd9b91e154223d35723c7fd04ef4ab6e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd9b91e154223d35723c7fd04ef4ab6e.png)


---
**Todd Fleming** *July 31, 2017 21:31*

Windows doesn't allow 2 apps to open the same COM port at the same time.


---
**David Cantrell** *July 31, 2017 21:33*

**+Todd Fleming** I just realized the same thing. Without Putty running it does what it was doing  before. Also, I just re-flashed the grbl and it is doing the same thing.


---
**Todd Fleming** *July 31, 2017 21:36*

It looks like something's going wrong with LW4's firmware detection on your machine. Claudio knows the most about that part.


---
**David Cantrell** *July 31, 2017 21:44*

Looks like I need to set GRBL_WAIT_TIME=10 in a .env file. Whatever that means. [github.com - No supported firmware detected. Closing port · Issue #322 · LaserWeb/LaserWeb4](https://github.com/LaserWeb/LaserWeb4/issues/322)




---
**Todd Fleming** *July 31, 2017 21:50*

I'd be shocked if that fixes it. That works around a really off-the-wall board that takes a few seconds to respond to the initial connect.


---
**David Cantrell** *July 31, 2017 21:56*

The info at the bottom of this post seems to have fixed it for Sam. I am going to try it and see. [plus.google.com - Sam Bakker - Google+](https://plus.google.com/118006001052877031825/posts/7Zr6puorqbe)


---
**David Cantrell** *July 31, 2017 22:07*

Yay, it seems to work! It connected!!!


---
**David Cantrell** *July 31, 2017 23:18*

I have run into another error which is "ALARM: 2 - G-code motion target exceeds machine travel. Machine position safely retained. Alarm may be unlocked."


---
**David Cantrell** *July 31, 2017 23:18*

![images/f3b28399c83a73c76431de308b381cb8.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f3b28399c83a73c76431de308b381cb8.png)


---
**Todd Fleming** *July 31, 2017 23:51*

$130, $131, $132 control the limits. Or set $20=0 to disable that check.


---
**David Cantrell** *August 01, 2017 01:13*

**+Todd Fleming** Thanks for the help on this. I hope I am almost there. I sent $20=0 via putty and that allowed the job to run, sort of. Do I set that permanently somehow or do I send that at the beginning of every job? Do I add that to "Gcode Start" in settings? 



The next issue I have run into is that the laser is  not firing. The head moves around like it thinks it is cutting but there is nothing burning. How do I trouble shoot why the laser is not firing?


---
**Todd Fleming** *August 01, 2017 01:19*

grbl-lpc writes $ settings to on-chip flash as soon as you send them. laser: is it hooked up to the mini's normal laser pin?


---
**David Cantrell** *August 01, 2017 01:28*

Yes. It is the K 40 with the ribbon cable. All three connectors are plugged into the C3d. It was cutting just fine under smoothie, smoothie was just getting disconnects mid job so I thought I would try Grbl and see if that allowed the computer to remain connected.


---
**Todd Fleming** *August 01, 2017 01:32*

Post your grbl config. $$ does this.


---
**Todd Fleming** *August 01, 2017 01:35*

Also follow [http://cncpro.co/index.php/65-documentation/initial-configuration/firmware/grbl-lpc-1-1e/40-configure-grbl-1-1e](http://cncpro.co/index.php/65-documentation/initial-configuration/firmware/grbl-lpc-1-1e/40-configure-grbl-1-1e) 


---
**Todd Fleming** *August 01, 2017 01:40*

Make sure settings->gcode start has M4, remove M2 from gcode end, and clear out tool on and tool off.


---
**David Cantrell** *August 01, 2017 02:51*

My grbl config is the same as what is listed on the link you gave.

$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=0

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=0.000

$36=100.000

$100=160.000

$101=160.000

$102=160.000

$110=24000.000

$111=24000.000

$112=500.000

$120=2500.000

$121=2500.000

$122=2500.000

$130=300.000

$131=200.000

$132=50.000

$140=0.400

$141=0.600

$142=0.000






---
**Claudio Prezzi** *August 01, 2017 10:52*

**+David Cantrell** $30=1000 sets max pwm value to 1000 (which is grbl default). Please check that your setting in LW4 (settings/gcode/PWM MAX S VALUE) is the same.


---
**David Cantrell** *August 01, 2017 14:44*

The PWM MAX S VALUE  in LW4 was set to 1,  changed it to 1000. It already had $30=1000. THe laser still does not fire.


---
**David Cantrell** *August 01, 2017 18:56*

I ended up switching back to Smoothie at the latest build and have found it to be more stable, without the disconnects I was dealing with before.


---
**Claudio Prezzi** *August 03, 2017 12:40*

Ok, smoothie now fears to be replaced and gives it's best ;)


---
*Imported from [Google+](https://plus.google.com/107911974344505492651/posts/13irTQrQDjs) &mdash; content and formatting may not be reliable*
