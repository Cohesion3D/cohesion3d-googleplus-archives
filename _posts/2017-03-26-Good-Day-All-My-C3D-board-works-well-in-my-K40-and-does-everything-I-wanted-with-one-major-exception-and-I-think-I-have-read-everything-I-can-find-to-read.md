---
layout: post
title: "Good Day All My C3D board works well in my K40, and does everything I wanted, with one major exception, and I think I have read everything I can find to read"
date: March 26, 2017 19:24
category: "FirmWare and Config."
author: "Ronald Whittington"
---
Good Day All

My C3D board works well in my K40, and does everything I wanted, with one major exception, and I think I have read everything I can find to read.

All of my images are revered on the material being cut or engraved.

I have reveresed connections on the X and Y

I have taken the ! symbol on and off the the step_dir_pin (Smoothie owner for a couple of years).

I home to 0,200 like it should, starts cut/engrave at 0,0 as it should, goes the correct direction when working.

I saw one person ask about this in the community and did not see that the thread continued, so I am looking for that answer.





**"Ronald Whittington"**

---
---
**Ronald Whittington** *March 27, 2017 11:55*

Thank you, I was able to resolve my reversed images by reconnecting X and Y steppers, reinserting ! symbol, and resetting LW3 to indicate your suggestions. Every other laser I have assembled homed to 0,0 and also started work flow at 0,0..so I was a bit confused at that, and apparently over thought it also :-)   Thanks for the quick reply. And I really like the C3D upgrade, the difference is obvious and well worth the cost. 

Now on to find out how to cut and engrave in one operation..hmmm let me see...


---
*Imported from [Google+](https://plus.google.com/+RonaldWhittington/posts/WRzPBinmn1v) &mdash; content and formatting may not be reliable*
