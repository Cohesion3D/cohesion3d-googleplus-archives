---
layout: post
title: "Can someone point me in the right direction as to what settings will affect edges of engravings being burnt more than the center of engravings?"
date: April 04, 2017 03:17
category: "FirmWare and Config."
author: "Arion McCartney"
---
Can someone point me in the right direction as to what settings will affect edges of engravings being burnt more than the center of engravings?  I have read that the smoothie firmware will adjust power level based on how fast the laser head is moving.  It doesn't seem to be very effective for my engravings.  All of the engravings in the image are Laser Fill Path in LaserWeb4.  Also you can see the drastic difference in color between the split M and the actual M.  The M along with the split lines are all one vector outline with Laser Fill Path to engrave it.  Settings I used are pot at 10mA and LW4 at 40% for the fill path operation.  Thanks for any help!!  

![images/b6bbe6231cab5ad370d78112c2d6f1b6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6bbe6231cab5ad370d78112c2d6f1b6.jpeg)



**"Arion McCartney"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 03:21*

I think **+Ashley M. Kirchner** has dealt with this. I think it might be trim pixels or some other such function? 


---
**Arion McCartney** *April 04, 2017 03:25*

**+Ray Kholodovsky** The operation is not a raster operation.  I only have trim pixel option in raster operation.  Is there a setting in the firmware to adjust this?  Thanks.




---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 03:28*

I think the LW devs will have more knowledge on the specifics of this than I do. Would you mind reposting in the LaserWeb group? 



I'm not aware of any specifics in firmware that affect this. 


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 03:31*

You know, to be perfectly honest with you, I can not get it to NOT do that in RASTER mode, however in pathfill mode I haven't had any issues. I burn at 10%, 0.1 beam width, 3000mm/min. HOWEVER, I have not done a large area like that of the letter M in yours, so it's possible I will see the same thing. Give me a bit and I'll try.


---
**Arion McCartney** *April 04, 2017 03:35*

**+Ray Kholodovsky** Posted in LW group.  Thanks.  **+Ashley M. Kirchner**  Thanks for the response.  I might try lowering my power and speed a bit like yours.  What is your pot mA set at?  Thanks.




---
**Ashley M. Kirchner [Norym]** *April 04, 2017 03:37*

No pot. That's why I mentioned 10% power output. That's set in LW.


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 03:38*

Yeah Ashley has a "uniquely behaving" laser hence the alternative wiring.


---
**Arion McCartney** *April 04, 2017 03:49*

I see.  


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 04:08*

Oh it's "unique" alright, it's "uniquely" mine. :) Anyway, visual inspection as the laser is doing a pathfill and I'm not seeing the darker edges. But, I'm likely not using the same settings you are. For one, the default line width is 0.2, and I run at 0.1. I'm going to run a second pass at 0.2 when this first one finishes.


---
**Arion McCartney** *April 04, 2017 04:36*

**+Ashley M. Kirchner** I am running at .1 also.  Posted in the LaserWeb group and was told to try adjusting acceleration in smoothie config.  

Thanks for testing that!


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 04:46*

I didn't make any accel changes in my config. However, saying that all these K40 machines are identical and you may just be locked up in the psychiatric ward.


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 04:50*

The left one is done at 0.1mm line distance, 15% power, 3000mm/min. The right one is as 0.2mm, everything else the same. Then I did what I normally do which is to sand the wood smooth. The right one is way too shallow (and parts are nearly sanded of), you can not feel the engraving. The left one you can still feel the depth differences. Image is 150x160mm

![images/b0bed4ca092296e33d061d8523fe141b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0bed4ca092296e33d061d8523fe141b.jpeg)


---
**Arion McCartney** *April 04, 2017 04:54*

**+Ashley M. Kirchner** The one on the left looks really good.  I'll have to figure this out.  Seems as though short distances are ok, but long distances with the laser are the issue.  Thanks again for testing!!




---
**Ashley M. Kirchner [Norym]** *April 04, 2017 05:02*

Well, the upper part of the frame is one long run left to right and back. It's all consistent on mine.


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 05:03*

And you can clearly tell the difference between the 0.2 and 0.1mm

![images/1354989ec111172272f8e67186c88023.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1354989ec111172272f8e67186c88023.jpeg)


---
**Arion McCartney** *April 04, 2017 05:05*

Yeah. Yours looks good compared to my long runs.  Just seems to not engrave as deep as the shorter ones. :-( I do appreciate your effort in testing. Now I know it can be better on my machine​. Just have to figure it out.


---
**Joe Alexander** *April 04, 2017 05:38*

What is your pulse length set at in your config file? i think default is 20 but I find for engraving I get smoother results at 200(some people go up to 400). 20 had more of this "varying intensity" burning that I see in your pic.**+Ashley M. Kirchner** nice engraves btw!


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 06:05*

Set at the default 200. At least my config was set to that when I got it.


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/9VAeuky46zj) &mdash; content and formatting may not be reliable*
