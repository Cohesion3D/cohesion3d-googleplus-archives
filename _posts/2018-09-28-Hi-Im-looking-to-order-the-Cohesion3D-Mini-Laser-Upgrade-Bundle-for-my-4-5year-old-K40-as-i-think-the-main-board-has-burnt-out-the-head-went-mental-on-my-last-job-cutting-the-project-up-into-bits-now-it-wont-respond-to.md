---
layout: post
title: "Hi, I'm looking to order the Cohesion3D Mini Laser Upgrade Bundle for my 4-5year old K40 as (i think) the main board has burnt out (the head went mental on my last job, cutting the project up into bits, now it wont respond to"
date: September 28, 2018 10:14
category: "K40 and other Lasers"
author: "Stephen Smith (CLOCKWORKLIME)"
---
Hi,



I'm looking to order the Cohesion3D Mini Laser Upgrade Bundle for my 4-5year old K40 as (i think) the main board has burnt out (the head went mental on my last job, cutting the project up into bits, now it wont respond to any input or home when switch on/reset). Plus it would be nice not to have to work around the buggy moshidraw software constantly.



I just wanted to check that i have not missed or misunderstood anything. Am i correct in assuming that this bundle replaces the main board (currently moshi 2013) and is compatible with the remaining hardware? E.g optical end stops,  stepper motors laser control etc?



Thanks for the clarification,



Steve





**"Stephen Smith (CLOCKWORKLIME)"**

---
---
**Joe Alexander** *September 28, 2018 10:20*

That is correct. The other main part recommended is a 24v power supply, which is also sold in the store.(mainly due to the 24v from the laser psu is vastly underrated at 1amp)


---
**Stephen Smith (CLOCKWORKLIME)** *September 28, 2018 10:29*

**+Joe Alexander** Cheers for the clarification. So you would say the additional power supply is a necessity? I currently have a PC PSU installed within the K40 to power additional fans/light etc. to i should be able to just that if needed.


---
**Joe Alexander** *September 28, 2018 10:52*

it might work but the control board and steppers run nominally at 24v and most pc psu's are 12v range. I'd spring for the extra psu just to cover my bases u know?


---
**Stephen Smith (CLOCKWORKLIME)** *September 28, 2018 11:00*

**+Joe Alexander** Sounds like good advice. think i'll end up picking up a 24v supply from this side of the pond tho, saves a little on inport etc. Thanks again for the advice, really appreciate it.




---
**Joe Alexander** *September 28, 2018 11:01*

not a problem :)


---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2018 18:00*

There might be some other considerations to account for, I would recommend posting pics of your machine, board, and laser power supply here. 


---
*Imported from [Google+](https://plus.google.com/110925455515724744415/posts/GiVDYxy5zSM) &mdash; content and formatting may not be reliable*
