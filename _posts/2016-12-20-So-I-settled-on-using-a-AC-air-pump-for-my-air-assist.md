---
layout: post
title: "So I settled on using a AC air pump for my air assist"
date: December 20, 2016 16:24
category: "General Discussion"
author: "Kelly S"
---
So I settled on using a AC air pump for my air assist.   Anyone have recommendations on a switch I can can attach to the C3D board to toggle a AC component via gcode and how one would go about setting it up?  Thanks much.





**"Kelly S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 17:33*

Use an AC SSR and use the "extruder" MOSFET to control it. (The rightmost pair of the green terminals)


---
**Antonio Garcia** *December 20, 2016 18:47*

**+Kelly S** has to take some pictures of that machine, when you finish all your upgrades!!! I´m still waiting for my C3D, so i hope after Christmas i can start with those kind of upgrades :) 


---
**Kelly S** *December 20, 2016 19:04*

**+Antonio Garcia** oh I will.  :D


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 19:05*

FedEx came a bit earlier than expected. Only the 2 samples :) testing now. ![images/50a8c279ea07c661ce1752a0118ee06e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/50a8c279ea07c661ce1752a0118ee06e.jpeg)


---
**Kelly S** *December 20, 2016 19:11*

Awesome **+Ray Kholodovsky**ay, look forward to your results so I can duplicate them in my build.  


---
**Don Kleinschnitz Jr.** *December 20, 2016 19:24*

An SSR is the best choice but some AC devices will not work with a SSR. For example, my vacuum blower will not and I have no idea why but it will not run full speed with an SCR. 

So I went with a 4x relay board with optically isolated inputs, electrically more noisy but I know it will work with all devices AC or DC.



An open collector/drain input to these boards allows multiple sources to turn on the relays at the same time such as smoothie and a control panel switch. 



Check out my 

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html) 

.......for schematics and details.



[amazon.com - Amazon.com: SainSmart 4-Channel Relay Module: Cell Phones & Accessories](https://www.amazon.com/gp/product/B0057OC5O8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Kelly S** *December 20, 2016 20:12*

I looked but did not see exactly how you implemented the 4-Channel Relay Module

 am I blind?  :P **+Don Kleinschnitz**


---
**Don Kleinschnitz Jr.** *December 21, 2016 01:30*

**+Kelly S** its here in the schematics:

look at the "power Control" tab:



[http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/cbifB8UG9aq) &mdash; content and formatting may not be reliable*
