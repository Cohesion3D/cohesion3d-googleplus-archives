---
layout: post
title: "I have seen some requests for/ gotten questions about how the board can control accessories - air assist, pumps, fans, laser pointers, etc"
date: April 10, 2018 22:20
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I have seen some requests for/ gotten questions about how the board can control accessories - air assist, pumps, fans, laser pointers, etc. 



I mean, we've got extra pins and you can configure them however you want, just need to wire some stuff. 



I'd like to know specifics though: 

Do you want to control something - if so what? 

Are you already controlling something - if so what? 

How - relay, mosfet, etc? 

Do you want to be able to control this thing from software? 



Thanks! 



PS: any feature requests now would be the time :)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jonathan Davis (Leo Lion)** *April 10, 2018 22:25*

Plug and play functionality would be nice or the option to run it over a wifi network. 


---
**Griffin Paquette** *April 11, 2018 01:46*

**+Jonathan Davis** it is plug and play for the K40. It can’t work for every machine right off the bat but for the K40 it’s a great dropin. 



I agree. Would love to see dedicated onboard WiFi on the mini. 


---
**Marc Pentenrieder** *April 11, 2018 08:10*

I would have more RAM ;-)


---
**Richard Vowles** *April 11, 2018 18:29*

Add water flow sensor... That's first on my list.


---
**Jonathan Davis (Leo Lion)** *April 11, 2018 18:30*

**+Griffin Paquette** Well with my system I am using a air cooled Eleks maker that was bought from GearBest


---
**ThantiK** *April 11, 2018 19:05*

I wonder how feasible it would be to have a couple mosfets with selectable input voltage via jumper. 3.3/5v @ 2a


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2018 19:07*

All FETS are N channel so they switch the - of the load.  Hook up + to whatever voltage you want. 


---
**Mark Kenworthy** *June 01, 2018 22:58*

Hi Ray, I am planning on using a 5v relay from an Arduino kit to power on and off my air pump with each cut. I know it’s possible to do this, just not sure the best way. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/FP9WeLGLfaR) &mdash; content and formatting may not be reliable*
