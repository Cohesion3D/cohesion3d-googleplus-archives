---
layout: post
title: "Any plan to upgrade the smoothieware to the latest build for the C3D mini (K40)?"
date: March 09, 2018 16:53
category: "K40 and other Lasers"
author: "Mike Hill"
---
Any plan to upgrade the smoothieware to the latest build for the C3D mini (K40)? I noticed that the updates on the GLCD are more intuitive...





**"Mike Hill"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2018 16:56*

It's easier for me to maintain the one build and config that we know works.  If you want to grab the latest firmware-cnc build from smoothieware github repo, you're welcome to do so. 


---
**Mike Hill** *March 09, 2018 17:24*

I should use the cnc build or the K40? Tried the firmware-latest.bin and didn't work as the x and y seemed to be reversed and acted a bit wonky. Probably just a config issue.



And totally understand configuration management concerns!


---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2018 17:25*

We use firmware-cnc.bin 


---
**Mike Hill** *March 09, 2018 17:26*

Thanks for the help. I'll give it a try and post back.


---
**Jake B** *March 14, 2018 11:16*

**+Mike Hill** interested in your results.  Being new to Smoothie, I was surprised that the GLCD was really just used as a glorified text display.  Coming from the under powered Atmel 2860-based 3d printer controllers, I had expected that smoothie would have more power to render nicer displays and menus.


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2018 16:21*

In the non-cnc firmware (ie: regular) there are icons for things more akin to what you'd see on Marlin/ Repetier Firmware.  


---
**Mike Hill** *March 17, 2018 19:12*

I updated to the latest version of smoothieware-cnc and have not had any problems. I first used the wrong version (non-cnc). It had a cleaner display in my opinion, but x/y acted wonky and I didn't have time to investigate. 


---
*Imported from [Google+](https://plus.google.com/+MikeHillDesign/posts/Qis9teM5Prt) &mdash; content and formatting may not be reliable*
