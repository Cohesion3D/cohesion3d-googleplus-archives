---
layout: post
title: "I'm not quite sure what caused the issue, but the microSD card in my C3D ReMix can no longer be initialized and is unreadable by my Mac"
date: January 21, 2018 05:08
category: "C3D Remix Support"
author: "Mike Hall"
---
I'm not quite sure what caused the issue, but the microSD card in my C3D ReMix can no longer be initialized and is unreadable by my Mac. I tried formatting the card, but that fails. Where can I find the appropriate firmware for the ReMix so I can try a different microSD?





**"Mike Hall"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:12*

Do you happen to have a copy of the config file we made? 



The stuff everyone is running on the Mini is at the bottom of here in a dropbox link: 

 [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions. : Cohesion3D](https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)


---
**Mike Hall** *January 21, 2018 05:15*

Yes, I was able to find the config file, as it was still open in Sublime.


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:17*

Make copies of that, stat!



I mean, it might help that I remember things like your laser pwm pin is 2.7 , but the less we have to use my memory, the better.


---
**Mike Hall** *January 21, 2018 05:26*

I did! Thanks… back up and running once more.


---
*Imported from [Google+](https://plus.google.com/106337213390028821874/posts/beAWMTTryoc) &mdash; content and formatting may not be reliable*
