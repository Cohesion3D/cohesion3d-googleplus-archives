---
layout: post
title: "I occasionally switch from grbl to smoothie"
date: October 11, 2018 18:18
category: "C3D Mini Support"
author: "Tony Sobczak"
---
I occasionally switch from grbl to smoothie.  Is there a way to not have to re-enter some of the settings to correct values when I switch back to GRBL?  GRBL downloaded from the C3D website has values that have to be modified.  I have to re-enter the following every time I switch back.





$25=6000.00

$100=157.575

$101=157.575

$102=3200.00

$112=500.000

$132=75.000



Is there a way to avoid having to do that?







**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2018 18:30*

You’d have to compile grbl-lpc with your values 


---
**Tech Bravo (Tech BravoTN)** *October 11, 2018 18:38*

try setting up a macro button in lightburn (right click a button in the console panel and it will allow edit)





![images/ac3f576834e2e8e1ba0068dbe0a9537c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ac3f576834e2e8e1ba0068dbe0a9537c.png)


---
**Tony Sobczak** *October 11, 2018 21:09*

**+Ray Kholodovsky** Are there specific instructions for compiling for the c3d rather than arduino?




---
**Tony Sobczak** *October 11, 2018 21:12*

Found directions from Jim Fong




---
**James L Phillips** *October 11, 2018 23:38*

I am curious can you use notepad++ to compile the bin file?


---
**James L Phillips** *October 12, 2018 00:18*

You don't have to use a compiler program? Just edit the settings and save it as a bin file?


---
**Tech Bravo (Tech BravoTN)** *October 12, 2018 00:20*

Sorry, i didn't read. I see compile now... i responded incorrectly. I removed the innacurate information. :)


---
**Tony Sobczak** *October 17, 2018 17:11*

My attempt at compiling failed miserably. 


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/MBrAk8JEcbE) &mdash; content and formatting may not be reliable*
