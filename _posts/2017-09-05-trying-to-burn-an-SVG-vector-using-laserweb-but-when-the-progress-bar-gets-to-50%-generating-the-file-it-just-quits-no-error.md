---
layout: post
title: "trying to burn an SVG vector using laserweb, but when the progress bar gets to 50% generating the file, it just quits, no error"
date: September 05, 2017 18:40
category: "Laser Web"
author: "timne0"
---
trying to burn an SVG vector using laserweb, but when the progress bar gets to 50% generating the file, it just quits, no error.  Is there a limit on the number of nodes or instructions it can handle? 



Other error is the bed area with the X/Y axis suddenly goes blank when doing JPGs...



Happy to post SVG if someone wants to have a go encoding it...



laserweb:

 Frontend: 4.0.989

 Backend: 4.0.115





**"timne0"**

---
---
**timne0** *September 05, 2017 18:48*

[github.com - SVG conversion to g-code fails at 50% · Issue #409 · LaserWeb/LaserWeb4](https://github.com/LaserWeb/LaserWeb4/issues/409)


---
**Ariel Yahni (UniKpty)** *September 05, 2017 19:24*

Worked for me

![images/c4d15008a5a23fd910bdaa67e037d9ac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c4d15008a5a23fd910bdaa67e037d9ac.jpeg)


---
**timne0** *September 05, 2017 19:41*

There's no error as per se, the g-code file looks fully formed with end points etc but when you run the simulator you can see only 50% of the file encodes? 


---
**Ariel Yahni (UniKpty)** *September 05, 2017 19:47*

**+timne0**​ I'm not sure what you mean. I generated the code and reviewed the gcode and it's seems to be complete.



Did you wait for it to finish? On mine it takes less then a minute but that could avry depending on the machine


---
**Ariel Yahni (UniKpty)** *September 05, 2017 19:49*

Also make sure to read the tags on the GitHub issue above as you failed to properly post the issue


---
**timne0** *September 05, 2017 19:49*

Mine took less than a minute, shows finish headers etc and appears complete. But when I run the file, it cuts out at about 50%. Do you see the progress bar go all the way to 100%?


---
**Ariel Yahni (UniKpty)** *September 05, 2017 19:53*

External gcode viewer

![images/c360739df762543d290e1a2cf1281670.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c360739df762543d290e1a2cf1281670.jpeg)


---
**timne0** *September 05, 2017 20:27*

Gah. Ok will setup another machine. I even reinstalled the other. No idea why it's crashing out then.


---
**Ashley M. Kirchner [Norym]** *September 05, 2017 22:18*

Same here, imported, generated gcode based on a low power cut. No issues.


---
**crispin soFat!** *September 06, 2017 03:19*

some conversion processes may produce an over-abundance of points? The article below is support for a specific software tool but the illustrations inform the issue, generally.



[tcicorp.com - Converting Adobe Illustrator Splines for the Best Possible Cutting Path &#x7c; TCI Corp](http://tcicorp.com/converting-adobe-illustrator-splines-for-the-best-possible-cutting-path/)


---
**Ariel Yahni (UniKpty)** *September 06, 2017 03:21*

Yes huge amount of point is trouble for LW


---
**Ashley M. Kirchner [Norym]** *September 06, 2017 03:22*

He's using Inkscape ... and as shown, there's nothing wrong with the file as two of us were able to open and process is successfully using the same LaserWeb he's using.


---
**Ariel Yahni (UniKpty)** *September 06, 2017 03:25*

**+Ashley M. Kirchner** agreed but is  important to note this for future users, ive seen my fare share of similar situations where paths are composed of multiple points


---
**timne0** *September 06, 2017 07:26*

It was hand traced, but I've been unable to get a g-code file working with my version of laser web. I'm going to fresh  install it on another computer (both windows 10) to see whether there's still a problem and I can isolate it to a particular computer.


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/8XjqeDFnLku) &mdash; content and formatting may not be reliable*
