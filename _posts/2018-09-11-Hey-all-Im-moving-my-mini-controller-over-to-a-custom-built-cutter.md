---
layout: post
title: "Hey all Im moving my mini controller over to a custom built cutter"
date: September 11, 2018 01:06
category: "C3D Mini Support"
author: "Chad Traywick"
---
Hey all Im moving my mini controller over to a custom built cutter. 



Is there anything in the smoothie config I need to change, when changing over to mechanical end stops? End stops are still both in the LEFT rear. Do I need to change the alpha and Beta max travel when homing to say 950 on both?



Also my cut area is now 24in or 609.6mm in the Y axis and 36in or 914.4mm in the X axis, is there anything that needs to be changed? The max area is slightly larger but this is the max area I need to keep to.



Also not sure of the offset in the X and Y yet to place the cutter at zero after homing. What is the offset in smoothie called?



Im used to Marlin but smoothie is completely alien to me.





**"Chad Traywick"**

---
---
**Tech Bravo (Tech BravoTN)** *September 11, 2018 01:10*

Id say max travel may need to reflect the new bed size. Bed size info here. [https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/](https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)

[lasergods.com - Setting Custom Bed Size For The Cohesion3d Mini - LaserGods Laser Engraver Info Portal](http://lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)


---
**Tech Bravo (Tech BravoTN)** *September 11, 2018 01:15*

Here is more on config. [http://smoothieware.org/configuration-options](http://smoothieware.org/configuration-options)


---
**Chad Traywick** *September 11, 2018 01:43*

Awesome! Thanks so much!


---
*Imported from [Google+](https://plus.google.com/101895536937391772080/posts/3bJriM72tnc) &mdash; content and formatting may not be reliable*
