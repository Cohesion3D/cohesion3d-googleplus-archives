---
layout: post
title: "I installed a LCD display to the board, the Smart Display I used to have on my 3d Printer"
date: June 28, 2017 04:58
category: "K40 and other Lasers"
author: "Lee lim"
---
I installed a LCD display to the board, the Smart Display I used to have on my 3d Printer.  All of the text is dark instead of bright, also when I try to adjust the contrast from the screen it does not stay, and reverts back to 0.  I added panel.contrast  9 and panel.reverse to the config file and see no change in behavior.  What can I do to get the LCD to be view able?





**"Lee lim"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 13:14*

Which screen? Only the GLCD is supported, not the text based 2004. 


---
**Lee lim** *June 28, 2017 19:31*

It is the GLCD




---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 19:36*

No changes to the config are required, it should work right out of the box.  



It is possible that your GLCD is defective, has backwards connectors, or that the EXP1 and EXP2 are connected in the wrong order.  You can check against the pics in the install guide. 


---
**Lee lim** *June 28, 2017 23:41*

I swapped the leads, screen it blank.  I put the LCD back on the 3d printer and it works fine.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 23:42*

Pictures please.  


---
**Lee lim** *June 29, 2017 03:04*

![images/d7505d9a358e14ec50ef19db4b45e09a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d7505d9a358e14ec50ef19db4b45e09a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2017 03:09*

Back of the display pic please. 


---
**Lee lim** *June 29, 2017 05:38*

![images/e71e07191187d14cf4a8a837f24ee4e1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e71e07191187d14cf4a8a837f24ee4e1.jpeg)


---
**Lee lim** *June 29, 2017 05:38*

![images/5ff56a91fc8d4608732d457c204381f1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ff56a91fc8d4608732d457c204381f1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2017 21:34*

Well I remember seeing some pics here earlier, not sure why they disappeared now. 

So..  You've got text on the screen. That means both things are working.   You need to set the contrast by turning the physical pot at the lower left, try clockwise 90 degrees so that the flat faces right. 


---
**Lee lim** *June 30, 2017 01:32*

I thought the text was supposed to be in white,  I turned the pot to where it makes the text most visible.


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 01:39*

The text is white, at least on all the screens I've ever used. 


---
**Lee lim** *June 30, 2017 05:13*

So why not with my board? It's white o  the printer


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 13:21*

Not sure. Like I said turning the contrast pot to the right 90 degrees has always given me white readable text. 


---
**Lee lim** *July 04, 2017 14:26*

Any reason why [panel.reverse](http://panel.reverse) = true or [panel.contrasr](http://panel.contrasr)  9 has no effect 


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 14:29*

Sure, it is because there are different types of screens in Smoothie, and this one does not support those commands. 


---
*Imported from [Google+](https://plus.google.com/109926073411939667229/posts/HwavwBkRZLp) &mdash; content and formatting may not be reliable*
