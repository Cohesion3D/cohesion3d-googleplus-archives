---
layout: post
title: "Could anyone tell me if the Mini board has some free pins/ports that could be used for things like the cover being open/closed and detecting water flow and things like that?"
date: January 16, 2018 10:29
category: "General Discussion"
author: "Tako Schotanus"
---
Could anyone tell me if the Mini board has some free pins/ports that could be used for things like the cover being open/closed and detecting water flow and things like that?





**"Tako Schotanus"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 13:58*

Free pins? Absolutely. 



Interlocks should be handled separately, perhaps with a separate arduino, it should cutoff the laser beam enable first, and then it can also be wired to pause the job on the Mini. 


---
**Anthony Bolgar** *January 16, 2018 14:56*

Relying on an arduino to control the door interlocks is not the best way to do it. Interlocks should be wired in such a way that a software/hardware glitch on the arduino would not stop the interlock from working. I have almost finished my Safety system design and the arduino is only monitoring and reading the state of the interlocks. It is easy to do as the limit switch used has a NO and a NC contact on it. SO if the cover is opened, the interlock circuit is broken and the laser will not fire but the NC pin is connected to the arduino and is pulled low or high (our choice) and the arduino displays a warning on an LCD that the cover is open.


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 14:57*

Agreed. A door switch should directly disable the laser. 

An arduino can read it and other sensors like temp and flow, and possibly pause the C3D.


---
**Tako Schotanus** *January 16, 2018 15:22*

My idea was definitely to wire the interlocks to the limit switch,  but I'm wondering why I would need a separate arduino just for that? Isn't that something that could be handled directly by the Mini?


---
**James Rivera** *January 17, 2018 18:17*

**+Tako Schotanus** Wild guess on my part: you want the laser to be cutoff in real time with no delays. So a physical switch or a real-time dedicated micro controller (Arduino) is better because the C3D may be busy. Again, just a guess. I’m a complete novice.


---
**Tako Schotanus** *January 17, 2018 20:02*

Hehe, I'm not making myself clear. What I mean is that I was thinking about just putting the interlock switches in the very same circuit with that switch that enables the laser. THat would do the trick, right? (From the safety perspective). What I then want to do is just have the Mini monitoring that circuit to see if it's open or closed. Isn't that possible without having an extra arduino is what I'm wondering. Especially because I'd like to show that status on the LCD.


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2018 20:05*

Yes for the switch. The point of the arduino comment was for more complex things that require monitoring and logic to read such as the flow or temp sensors. 


---
**Tako Schotanus** *January 18, 2018 00:20*

Aha, so you're saying the Mini itself isn't capable of that?


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 00:22*

There's no real "interlock" functionality written into the firmware. The best I can get you is the one pin configured to pause a job you are running. 

It won't cut the beam off right away, so you have to hard wire. 



There's also no capability for reading those other sensors. 



So if you need to handle a bunch of those things, a single arduino is a way to go. 


---
**Tako Schotanus** *January 18, 2018 00:27*

My first idea was to add that functionality to the firmware, that's why I asked if the Mini had free inputs that could be used for that. Having a separate Arduino just seems overkill somehow. But perhaps I need to look into a single Arduino solution instead. Need to study this some more 😊


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 00:36*

I see. There are available endstop inputs (high voltage input protected) and many more available gpio pins. I will be happy to advise further if you would like to work on this. We have long discussed a new module for Smoothie called interlocks which essentially needs to act as an AND between a bunch of inputs and pause the job if any of them read out of bounds. 

That and something to read those sensors we discussed, for example flow will usually tick a certain amount of times per time, and you want to make sure this is on track, etc. 


---
**Tako Schotanus** *February 28, 2018 12:32*

In the end I simple put the door switch (and the water pressure sensor) in the "Laser Switch" circuit. Much easier because I really don't need detection anyway (although being able to do so and that the software warns you would be nice of course)


---
*Imported from [Google+](https://plus.google.com/+TakoSchotanus/posts/48Xh1yk5tnG) &mdash; content and formatting may not be reliable*
