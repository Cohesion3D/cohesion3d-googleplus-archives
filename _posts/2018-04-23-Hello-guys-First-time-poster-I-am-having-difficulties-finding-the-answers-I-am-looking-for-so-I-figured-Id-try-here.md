---
layout: post
title: "Hello guys, First-time poster. I am having difficulties finding the answers I am looking for so I figured I'd try here"
date: April 23, 2018 20:27
category: "C3D Mini Support"
author: "Michael Howland"
---
Hello guys, First-time poster. I am having difficulties finding the answers I am looking for so I figured I'd try here. 



I have adopted a blue and wite Chinese 50-watt laser that blew the laser tube due to overheating. It currently has the Rdc644xg system in it. This unit is firing the laser during rapid moves and the mainboard is suspected. I have already replaced the power supply along with the new tube that definitely needed changing. 



My questions are.



How much of a difference is switching this machine to Cohesion as the K40?



Is there a way to read the firmware without having a cohesion board like a 3D printer? This would answer many questions if I could. 



Is there a list of control options that can be accessed on the display somewhere? (Also likely can be found in the firmware if I could read it)



I plan to reuse the existing 24v psu, external stepper drivers (so I'll be getting the breakouts), and if at all possible the hal endstops but if I have to I'll switch to mechanical. I have a rotary table but it is run by unplugging the Y-Axis and plugging in the table. Any help would be much appreciated as I need to get this machine running so I'd like to order a mainboard soon. I just am trying to figure out if it is worth the extra $200 to replace the existing board. 



Thanks, Guys and Gals.





**"Michael Howland"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 23, 2018 20:31*

It sounds like you have a Ruida - this is compatible with Lightburn software as is the C3D... you'd need to buy the $80 DSP version of Lightburn instead of the $30 version... but otherwise... are you hitting a limitation or hoping to gain something by doing the switch?  



See if the trial works with your machine: 



[cohesion3d.com - Lightburn Software](http://cohesion3d.com/lightburn-software/)


---
**Ray Kholodovsky (Cohesion3D)** *April 23, 2018 20:38*

Oh, I may have missed the line about the "mainboard is suspect".  



But I'd still recommend answering the questions I posed and trying Lightburn with the Ruida. 



Here's how to rewire it: [plus.google.com - I just purchased a 100w 700x500 laser. I also have purchased a cohesion3d, a...](https://plus.google.com/105510960839020211759/posts/f8tjg9TWi7y)



And this article explains how to use the external stepper adapters: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



The C3D supports a Z table + Rotary natively without re-plugging anything.  You do need to configure it in firmware, as the guide shows. 



I don't know what you mean by "read the firmware". 


---
**Michael Howland** *April 23, 2018 21:02*

Reading the firmware was meant ot mean is there a way to look at the setting like you can with Marlin or Repetier for 3D printing. Is there a configuration environment? 



As for Lightburn I fully intend to use it as it is the only one I have found that is available on Linux. (No Habla Windows LOL). I am using the trial now and what one I buy is dependent on what mainboard I go with. 



As far as limits/benefits go. Thats what I am trying to figure out. Minus the Cost benefit, and being opensource I am trying to weigh pros and cons. Can the cohesion board regulate the laser output during engraving like the Ruida can?



I'm going to check out the article you linked now but wanted to reply first.


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2018 02:02*

It is explained well here: [plus.google.com - Has anyone tried this with one of the larger laser cutters, such as the 50W, ...](https://plus.google.com/113862922597413295359/posts/9UAw4JurvE6)



Yes, you definitely get PWM control for greyscale and variable cut powers and combinations thereof. 



Smoothie has a config.txt file on the board's memory card where the steps per mm and other settings are stored. 

Grbl-lpc is set via $$ commands via the console and then the board remembers them. 

Smoothie will drive a GLCD screen (and also you can put job files on the memory card and run them so you don't need a computer tethered) but it hits an upper speed limit with raster engraving due to the overhead of these other things.  GRBL-LPC does not run a screen or memory card and is blazing fast.   



In short, if you want to raster fast, you'll put grbl-lpc firmware on this board and your laser will be tethered to your computer with a USB cable streaming the job from Lightburn. 


---
**Michael Howland** *April 24, 2018 03:22*

Thanks! Turns out I was searching for the wrong questions. The links you provided answered much of my questions. I just ordered a board, few stepper breakouts and some other small things along with lightburn. Guess I'll dive right in once everything shows up. This cutter will be a great addition to my fleet of 3D printers and cnc plasma cutter.




---
*Imported from [Google+](https://plus.google.com/+ErnestHowland/posts/Fx7ecHF2GJs) &mdash; content and formatting may not be reliable*
