---
layout: post
title: "I brand new to this, having just bought a bog standard K40"
date: February 08, 2018 17:58
category: "C3D Mini Support"
author: "Colin Davies"
---
I brand new to this, having just bought a bog standard K40. I wish to check that the Cohesion3D mini is compatible with my board. I'm sorry if this has been answered before.

Regards

Colin

![images/34245f91079599b90c1706c0dc4eca06.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/34245f91079599b90c1706c0dc4eca06.png)



**"Colin Davies"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 18:01*

Yep, this looks just like ribbon cable version and covered in our instructions.


---
**Colin Davies** *February 08, 2018 18:08*

Thank  you Ray for your very prompt reply.


---
**John Plocher** *February 10, 2018 17:37*

**+Ray Kholodovsky** Suggestion for the C3D docs:  A couple of before & after pictures from this perspective would be a tremendous help, especially if they showed all the individual wires in each connector, for both/all the variations of nano board...



The current top down photos leave just a bit much to the fearful newbee imagination :-)



**+Colin Davies** - if you could take another pix with the cable/wire colors visible in the bottom right hand connector (bend the wires down a bit by hand...) this would be a great "before" shot.


---
**Colin Davies** *February 13, 2018 07:31*

**+John Plocher**Will do


---
**Colin Davies** *February 13, 2018 20:13*

Is that what you wanted?

![images/e0facd9b8558b3b7366129a0a672cc8e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e0facd9b8558b3b7366129a0a672cc8e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 20:15*

That's a good one. 


---
*Imported from [Google+](https://plus.google.com/+ColinDavies/posts/LJNQEe9xYxQ) &mdash; content and formatting may not be reliable*
