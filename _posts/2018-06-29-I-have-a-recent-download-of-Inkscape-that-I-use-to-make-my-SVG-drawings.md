---
layout: post
title: "I have a recent download of Inkscape that I use to make my SVG drawings"
date: June 29, 2018 02:11
category: "General Discussion"
author: "Pets and Ponds Customer Service"
---
I have a recent download of Inkscape that I use to make my SVG drawings.  Even if I resize page to content Lightburn sees a much larger page than it should and I have to delete phantom parts of the page that are not visible.  Files are saved as plain SVG.



Also I have can not scan open shapes.  I have opened the exact same SVG files in a few other laser software programs and have had no issue scanning the same drawings.  I have tried using Close Paths, Auto-Join and Optimize Shapes with no luck.  If I cut rather than scan it works just fine.  Is this something than can be solved?





**"Pets and Ponds Customer Service"**

---
---
**LightBurn Software** *June 30, 2018 00:24*

Can you zip a file you’re having trouble with and send it by email? Developer at LightBurnSoftware.


---
**Pets and Ponds Customer Service** *July 05, 2018 01:24*

**+LightBurn Software**

Did you receive the file I emailed?


---
**LightBurn Software** *July 05, 2018 01:27*

I don't believe I did, no.


---
**Pets and Ponds Customer Service** *July 05, 2018 14:38*

How do I send you an email  then?


---
**Pets and Ponds Customer Service** *July 05, 2018 21:40*

**+LightBurn Software** I have now purchased a full license, downloaded and installed the newest version and have the same issues.  Now when I open some files I get a messages telling me the file contains unsupported font, even though there is no font in the file.  I need to get some jobs done for a client.  If you can prioritize this I would really appreciate it.  


---
**LightBurn Software** *July 05, 2018 23:34*

I have no messages from you in any inbox aside from the order confirmation.  Zip up a file or two that you're having issues with and send them to developer at [lightburnsoftware.com](http://lightburnsoftware.com) and I'll have a look. The "unsupported font" isn't likely font, but text objects - we don't import those because InkScape and other packages have much more complicated options than we support - convert the text objects to paths in InkScape before exporting.



LightBurn also imports <b>everything</b> in the source file, and doesn't care about page boundaries, so if you have things on layers that are hidden or outside the page, you'll need to remove those in the source file for the moment. We would like to remove the need for that, but it's not supported for now.


---
**Pets and Ponds Customer Service** *July 06, 2018 01:23*

**+LightBurn Software** Email has been sent.  No hidden layers.  No text objects in the one file and no un-traced font in the other.  The font that is traced imports fine.  :)


---
*Imported from [Google+](https://plus.google.com/104356262526050403266/posts/g9FirXoTFHH) &mdash; content and formatting may not be reliable*
