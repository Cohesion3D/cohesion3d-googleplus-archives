---
layout: post
title: "Installed the cohesion 3D mini but the mini doesn't control the laser power"
date: March 16, 2018 14:37
category: "C3D Mini Support"
author: "Dominic Notaro"
---
Installed the cohesion 3D mini but the mini doesn't control the laser power. There is a digital display interfaced to the laser power supply I've included a picture.



![images/4800853166c6f5b9927c2407d0484896.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4800853166c6f5b9927c2407d0484896.jpeg)
![images/ef28e00be4bd6f3a1b6cea71cca6c502.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef28e00be4bd6f3a1b6cea71cca6c502.jpeg)

**"Dominic Notaro"**

---
---
**Don Kleinschnitz Jr.** *March 16, 2018 15:24*

Insure that you are not exposed to the beam ...then...



When you push the test button located down on the laser power supply... fire the laser?


---
**Dominic Notaro** *March 17, 2018 03:52*

**+Don Kleinschnitz** yes the test push button on the digital display does the test fire. The digital display sontrols the laser power in percent and I turn the laser on and off from the display. If the digital display is set for 20 percent power and I use lightburn and set the power level greater than 20 percent the power level stays the same as set by the digital display.


---
**Don Kleinschnitz Jr.** *March 17, 2018 10:45*

By this statement: <i>Installed the cohesion 3D mini but the mini doesn't control the laser power.</i> You don't mean that the laser does not fire at all just that the <i>Digital Panel</i> controls the lasers max power?



If you have the digital panel controlling IN and L being controlled by the C3D then that is the way it should work. The pot controls the max power and the C3D controls the power within the set point of the pot.


---
**Dominic Notaro** *March 17, 2018 13:25*

I will check to see if that is the case. I'll set the percent using the digital display then vary the level using CD3 within that range. Let you know the result.


---
**Wild Bill** *March 18, 2018 14:52*

I have a K40 with that same digital display and a C3D. That display will set the MAX power that the laser will work at so if you set 20% in the digital display - the levels in LightBurn will be from 0 up to the MAX that you set in the digital display. What I do is set set the display to the MAX power I ever want the laser to work at, then work within that range by setting levels in LightBurn. Hope this is clear.



The display on the machine is really just a digital POT. It connects to the power supply the same as a real POT.


---
*Imported from [Google+](https://plus.google.com/110871768046015591697/posts/S5Xs6JG5QZP) &mdash; content and formatting may not be reliable*
