---
layout: post
title: "LaserPro Explorer II?? We belatedly discovered that the X and Y motors are DC motors with rotary encoders ."
date: March 27, 2018 10:57
category: "C3D Mini Support"
author: "luca santo"
---
LaserPro Explorer II??

We belatedly discovered that the X and Y motors are DC motors with rotary encoders .

you can mount the card. on these engines ??





**"luca santo"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2018 15:21*

Nope. Can they take a step + direction signal as input. 


---
**Jim Fong** *March 27, 2018 15:34*

Plus the Laser Power supply needs to be PWM compatible too.  




---
**luca santo** *March 27, 2018 15:42*

ok I should change the engines that engines ...

which ones are most suitable?






---
**Jim Fong** *March 27, 2018 16:52*

[https://fablabsd.wiki.zoho.com/faceq/LaserPro-Explorer-II.html](https://fablabsd.wiki.zoho.com/faceq/LaserPro-Explorer-II.html)



Looks to be a expensive and difficult retrofit.   New servo drivers etc. 


---
**luca santo** *March 27, 2018 16:57*



I move the k40  laser engines with electronics Cohesion3D


---
**Jim Fong** *March 27, 2018 17:22*

Very few here have experience with upgrading servo motor systems. Are they brushed or brushless? What type of encoders etc. Is the laser power supply PWM compatible?  



If you don’t know these answers, your best bet is to find someone local who is knowledgeable enough about servo motion to help you. 






---
**luca santo** *March 27, 2018 17:25*

**+Jim Fong** 

I already have a k40 running with c3d

I wanted to spot everything and use a larger work area


---
*Imported from [Google+](https://plus.google.com/107713082808937594920/posts/GJznVVrEnN1) &mdash; content and formatting may not be reliable*
