---
layout: post
title: "So got my board today. Got everything wired in...but the power"
date: May 08, 2017 23:19
category: "C3D Mini Support"
author: "Jesse Blanchard"
---
So got my board today.  Got everything wired in...but the power.  My power connector has 6 "holes" on the female side and only 4 are filled but there is a gap between the pairs.  What order should they be in?  Its 24v, 5v, grnd, and L.  The other wire unwired as yet is the jumper to bypass the pot.  Am I correct that it should be in the j4 position?





**"Jesse Blanchard"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 23:24*

Pinout diagram: [cohesion3d.freshdesk.com - Cohesion3D Mini Pinout Diagram : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)



People have had success rekeying the connector, I personally prefer to rewire directly to the "Main Power In" screw terminals for 24v and Gnd, and to P2.5 (the 4th terminal from left at the bottom) for L.   This board does not need 5v from the PSU, it makes its own. 



Nothing else - do not use the provided white cable (this is for legacy installations), do not disconnect the pot, and do not change any other wiring. Just need 24v, gnd, and L to go to the board. 


---
**Jesse Blanchard** *May 08, 2017 23:35*

Oh, that's awesome.  Yeah I much prefer the screw in connection!  When I get this done I'll post a pic.  Do you mind double checking it for me?


---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 23:36*

Absolutely.  In fact, take all the pics and I'll add them to the instructions.  I'm adding the ribbon cable variant possible. 


---
**Jesse Blanchard** *May 09, 2017 14:23*

Done

![images/f68e6e8c4ab9176af2ab3fc94665fbea.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f68e6e8c4ab9176af2ab3fc94665fbea.jpeg)


---
**Jesse Blanchard** *May 09, 2017 14:23*

![images/aee83bf3ea9fa3f31d3d017c137be3d2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aee83bf3ea9fa3f31d3d017c137be3d2.jpeg)


---
**Jesse Blanchard** *May 09, 2017 14:23*

![images/ce4be231084299dda7477c282428accb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ce4be231084299dda7477c282428accb.jpeg)


---
**Jesse Blanchard** *May 09, 2017 14:24*

![images/f21be4a461902a7947df2e2286e8dce1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f21be4a461902a7947df2e2286e8dce1.jpeg)


---
**Jesse Blanchard** *May 09, 2017 14:24*

![images/c8dfc77c9a65ae2b644c32dd6236bfa3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8dfc77c9a65ae2b644c32dd6236bfa3.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 15:55*

Nope. Power is going to the wrong place.  You want MAIN power in. And I said not to use the white cable that comes with the kit. 


---
**Jesse Blanchard** *May 09, 2017 16:20*

Ok.  I will redo and repost...


---
**Jesse Blanchard** *May 09, 2017 16:28*

Ok so main 24v and ground are​ wrong. Is L in the right place?  And if I don't use the cable does the pot need to remain plugged in?


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 16:28*

You want 24v and Gnd going to the screw terminal in the top left. Make sure the polarity is correct. 



L is in the right place. 



Yes, please plug the pot back in. 


---
**Jesse Blanchard** *May 09, 2017 16:31*

![images/c135fc52d2115e979775fb87581f7a07.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c135fc52d2115e979775fb87581f7a07.jpeg)


---
**Jesse Blanchard** *May 09, 2017 16:31*

![images/db12650a446ffad00d06fb2abb2468fd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db12650a446ffad00d06fb2abb2468fd.jpeg)


---
**Jesse Blanchard** *May 09, 2017 16:31*

How about that?


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 16:33*

Nice. I can't confirm the polarity of 24v and gnd with both wires being the same color. As long as you're confident with that...


---
**Jesse Blanchard** *May 09, 2017 16:35*

Yeah they are the same EXACT color.  Let me mark them to help with the guide...






---
**Jesse Blanchard** *May 09, 2017 16:39*

![images/1f90b1542d641f9fb71d11009712d0e2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f90b1542d641f9fb71d11009712d0e2.jpeg)


---
**Jesse Blanchard** *May 09, 2017 16:39*




---
**Jesse Blanchard** *May 09, 2017 17:57*

Ray Kholodovsky, Is that right?  How does the board control the laser output?




---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 18:00*

Looks good, to the best of my ability to see it. (legal disclaimer-ing)



It pulses L to do variable power control.  You're all set to proceed. 


---
**Jesse Blanchard** *May 09, 2017 18:12*

So there is no PWM? Or does it pulse L at different intensities


---
**Jesse Blanchard** *May 09, 2017 18:13*

Intensities*


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 18:13*

Yeah, that. 


---
**Jesse Blanchard** *May 09, 2017 18:15*

LOL... Ok.  So do I need to set the pot at any particular point or will it affect the laser intensity at all?


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 18:16*

Start at 10mA


---
*Imported from [Google+](https://plus.google.com/118439095325336573510/posts/64sHsNXE3iV) &mdash; content and formatting may not be reliable*
