---
layout: post
title: "Looking at building my own laser from openbuilds/lightobject parts"
date: May 18, 2017 14:07
category: "General Discussion"
author: "Alan Dyke"
---
Looking at building my own laser from openbuilds/lightobject parts. I see a lot of recommendations for Cohesion systems in my facebook group for people looking to upgrade a Chinese import with a junk controller - but I'd be starting fresh here.



Since it's not plug-and-play for my wiring system, I'm wondering how easy this thing is going to be to wire up to a normal system and get running? For what it's worth, I've built an X-Carve with the GRBL shield and run a MPCNC with a Ramps board, so some of this isn't quite new to me. 



Is this the right control system for me? I do have to say I like the idea of using grbl and gcode since I'm familiar with using inkscape w/ plugins and inventables' Easel tool and modifying gcode that comes out to work with my diode laser.







**"Alan Dyke"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 14:34*

Hi Alan. The Cohesion3D boards are full on controllers for CNC/ 3D Printer/ Lasers - so in addition to the k40 drop in capability they have all the motor, endstop, and mosfet ports you'll find anywhere else. 

They run Smoothie. Start with that. It's highly configurable and lots of fun. You can then flash the new grbl-lpc that the LaserWeb developers came up with when you need really high performance. 


---
**Alan Dyke** *May 18, 2017 14:38*

Thanks for the response Ray. Is there any soldering required for the non-drop in? How complicated is the wiring? Since I just started looking into Cohesion3d just a few moments ago, I haven't quite dug around to see if there's wiring diagrams out there for the non-drop in solution, but I assume there are somewhere. What drivers are recommended for 1 Nema17 (X), 2 Nema23 (Y, Z) ?




---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 14:45*

Go to Cohesion3D.com and follow the documentation tab to find wiring diagrams, etc... it's the same no soldering connections as in any other board. 

We provide A4988 drivers and are also a fan of the Trinamic TMC2208 stepsticks (silent!) The latter can do more current but needs active cooling. Nema 23 can vary a lot. You might be able to drive a small one with one of those drivers. But more likely, you should get the external stepper adapter and get external stepper drivers for the 23's. 


---
**Alan Dyke** *May 18, 2017 14:54*

I very much appreciate your responsiveness here! I am presuming the laser control here is TTL and not PWM? Maybe I should start another thread, but I'm also trying to figure out what laser power supply to buy, and I'm looking at the RECI DY10, and hoping everything's compatible there. Is there a list of known compatible stepper drivers? I was looking at some sainsmart ones on amazon for about $40 ea when I was looking at getting an AWC708c lite controller.




---
**Alan Dyke** *May 18, 2017 14:55*

Oh, and given all that I've said so far - am I looking at the Remix? Or the mini for what I want to do?




---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 15:06*

Uh, the words TTL and PWM in this context don't mean what you think.  Short story, I don't care which it is, you can do it. 



The boards takes StepSticks. Plain and simple.  A4988 are cheap and work, DRV8825 suck with a passion (my opinion, shared by many), and Trinamics are incredible with the latest 2208 taking the lead. 



External Stepper Drivers - TB6600 are cheap, DQ542MA are good, and Gecko/ Leadshine/ other name brand are expensive.  Get the external stepper adapters so you can easily wire to those. 



Here's the jist: the Mini is the one that drops into the K40.  It has 4 motor ports and some pins for expansion.  The ReMix has 6 motor ports and every possible expansion you can think of.  Plus, the ReMix has the GLCD (screen) connectors integrated and memory card comes included.  With Mini these things add up a bit. 



So ultimately, depends on what you want to do, either will work.  



Proposed Shopping List:

Cohesion3D Board (ReMix or Mini). 

If MIni need to add GLCD Adapter and MicroSD Card. 

A4988 drivers and External Stepper Adapters. 

Graphic LCD. 



Elsewhere: external stepper adapter - China (ali/ ebay) for the TB6600, or openbuilds for the DQ542MA. 


---
**Alan Dyke** *May 18, 2017 16:18*

Much thanks. In regards to TTL/PWM - I was specifically talking about the part about sending signal to the laser power to tell it to fire

. I saw the mini has a port for K40 PWM, but in the diagram for the Remix, I didn't see the ouput to the laser PSU and got a bit confused.






---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 16:21*

Yeah, that's a level shifted to 5v thing, ReMix has a similar header if you really need it, but we don't use that.  We pulse L on the laser psu, which does the same thing.  Technically it's digital but it does pwm, hence me saying context :)  Like I said, you're good.  


---
**Alan Dyke** *May 18, 2017 16:23*

Awesome. Adding this to the BOM and skipping the DSP controller :)




---
**Anthony Bolgar** *May 18, 2017 17:12*

Smart choice **+Alan Dyke** , I have 2 **+Ray Kholodovsky** boards and I can attest to their ease of use and quality. And Ray has excellent customer support.You will not be disappointed. 


---
**Alan Dyke** *May 18, 2017 17:13*

 I feel well supported already. :)




---
**Thomas Lean** *May 19, 2017 14:33*

I love mine, works like a champ!


---
*Imported from [Google+](https://plus.google.com/106984809512754077184/posts/YJHyZsgk7Fy) &mdash; content and formatting may not be reliable*
