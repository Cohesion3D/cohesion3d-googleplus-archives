---
layout: post
title: "I'm stumped. So I was dumb and broke my last board"
date: July 17, 2017 22:51
category: "C3D Mini Support"
author: "Tony Marrocco"
---
I'm stumped. 



So I was dumb and broke my last board. Got my new one in today and have it all installed the exact same way as my old one but now my Y axis doesn't respond at all and my glcd looks a bit different (not the same information on the screen). I've gone over the wires a dozen times... so I don't know.  I didn't order a new memory card, using my old one. 



This is on a k40.



![images/5777cb3ba3ce81cca669367c7b5f7896.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5777cb3ba3ce81cca669367c7b5f7896.jpeg)
![images/b858bb4f80d0a5d1dc924e70b2a68d15.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b858bb4f80d0a5d1dc924e70b2a68d15.jpeg)
![images/3793a00c3ed93bccb110ffc6c7137983.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3793a00c3ed93bccb110ffc6c7137983.jpeg)

**"Tony Marrocco"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 22:53*

Could it be your driver is bad? With power off try swapping the X and Y driver locations.



Also how'd you break the old one? 


---
**Tony Marrocco** *July 17, 2017 23:02*

**+Ray Kholodovsky** The small green boards, tried that. Do you have any clue as to why the   screen looks so much different ? It was full of text previously.



I broke it by putting too much pressure on the Ethernet port and it broke off a tiny piece next to it. The laser still worked perfectly  of I loaded stuff through the SD card. I'll attach a pic of what I broke on the old one. If you look at those 4 little bars right behind the port you'll see the bottom 2 are gone. 



I mean maybe it's a possibility I broke the drivers during the new install...

![images/ef336f17496ad0f1c82031f57f3bf3e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef336f17496ad0f1c82031f57f3bf3e7.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 23:05*

Screen's the easy part. Regular vs CNC firmware. Load the firmware.bin onto the MicroSD card (you can get this from the Dropbox link at the bottom of the k40 upgrade instructions). Then reset the board. 


---
**Tony Marrocco** *July 17, 2017 23:15*

**+Ray Kholodovsky** Sorry Ray, you're so right. I swore I switched them but I must have put them right back in their same spots. Welp, time to put another in to you haha. 



Thanks ! 



Also... is the .bin supposed to disappear after reset ? I just don't see it on the card anymore (tried it before) but I could have done that wrong too lol.


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 23:19*

Yeah, the firmware.bin flashes to the board and becomes FIRMWARE.CUR.  So you need to flash the new board as I described.  Then the GLCD should show the CNC screen you are used to. 



I'm very confused now about what's going on now with the drivers, please explain.  


---
**Tony Marrocco** *July 17, 2017 23:20*

I switched the driver boards (must have put them back in their same place before) and the x axis is now not responding so it must be the board, yes ?


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 23:22*

Yes, it the problem follows the driver module and now the Y works then you need a new driver module. A4988 cheap on eBay or Amazon. 


---
**Kelly Burns** *July 19, 2017 02:44*

Thy are really cheap.  Buy multiples so you are never out of business again ;)


---
**Kelly Burns** *July 19, 2017 02:45*

If you just need one to get you by, let me know.  


---
**Kelly Burns** *July 19, 2017 02:50*

What you broke off are just a couple of resistors.  Should be easy to fix the old one so hang onto it.  I can fix it for you, but should be easy to find someone locally. 


---
**Tony Marrocco** *July 19, 2017 20:07*

**+Kelly Burns** I think they are long gone but I'll keep the board. Thanks for the input! The new parts got here today... made sure to order extra haha. 


---
*Imported from [Google+](https://plus.google.com/112686049554441919913/posts/RkSCv6RXWhm) &mdash; content and formatting may not be reliable*
