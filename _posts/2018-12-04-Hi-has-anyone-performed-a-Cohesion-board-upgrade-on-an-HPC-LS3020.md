---
layout: post
title: "Hi, has anyone performed a Cohesion board upgrade on an HPC LS3020?"
date: December 04, 2018 16:45
category: "General Discussion"
author: "Kris Heath"
---
Hi, has anyone performed a Cohesion board upgrade on an HPC LS3020? It has a V8.1 board at the moment and there's no rotary attachment or z-axis movement involved, so it's only 4 wires - power in, power out, ribbon for X-motor and a JST for the Y-motor. Just wondering how much work / rewiring is involved swapping the boards and putting a new PSU in. Thanks.





**"Kris Heath"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 16:50*

I mean it looks familar and that sounds familiar... Show me pics of your electronics and wiring though...


---
**Kris Heath** *December 04, 2018 17:18*

This is the board, from left to right:

USB-B out, power in at the top in green, power out at the top right, then the X-axis ribbon and Y-axis JST at the bottom right. I'll try and post a couple of close ups too.

![images/1be9ac7109adf405531b789b2b77f7dc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1be9ac7109adf405531b789b2b77f7dc.jpeg)


---
**Kris Heath** *December 04, 2018 17:21*

This is the wiring to the PSU - sure you can see what's there, the far right is the main power connector, with feeds in from and out to the board, 24, GND, 5V and L.

![images/84a5f143dd7e12051814b506e879fa2f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/84a5f143dd7e12051814b506e879fa2f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 19:46*

Please zoom out and show more angles and the whole lpsu


---
**Kris Heath** *December 05, 2018 15:00*

The machine casing...

![images/394cf626e3ec4445bda7f2fb31394480.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/394cf626e3ec4445bda7f2fb31394480.jpeg)


---
**Kris Heath** *December 05, 2018 15:01*

The PSU and board

![images/477e61175cf620b7ed67e076d7829654.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/477e61175cf620b7ed67e076d7829654.jpeg)


---
**Kris Heath** *December 05, 2018 15:03*

PSU

![images/3e844a95bdbb8c73f133524b2e4bf3fe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3e844a95bdbb8c73f133524b2e4bf3fe.jpeg)


---
**Kris Heath** *December 05, 2018 15:04*

The board

![images/c0984826d6af3fb7f3cebf3ca9a98597.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c0984826d6af3fb7f3cebf3ca9a98597.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2018 06:28*

The ribbon and Y motor connector do not concern me. 

There is a remaining connector J4 and the power input screw terminal.  You would have to trace all these wires to verify what they are.  If I had to guess, I would say that J4 contains the laser fire signal.  There might be some experimenting and rewiring involved to get everything working. 



The new LaserBoard supports multiple laser control types so we can make a number of wiring options work and has its own power supply to ensure stable operation.  Might also be worth grabbing a pack of crimp connectors to have, just in case. 


---
*Imported from [Google+](https://plus.google.com/112493650283963335135/posts/QhYCE2ahwbo) &mdash; content and formatting may not be reliable*
