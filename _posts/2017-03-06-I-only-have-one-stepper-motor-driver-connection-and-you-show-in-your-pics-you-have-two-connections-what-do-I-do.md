---
layout: post
title: "I only have one stepper motor driver connection and you show in your pics you have two connections what do I do?"
date: March 06, 2017 18:45
category: "C3D Mini Support"
author: "Allen Russell"
---
I only have one stepper motor driver connection and you show in your pics you have two connections what do I do?

Here is a Pic

I have a K40 Middle Man Board, should I use it?



![images/a3a502d74cc14ad79c21d2ea8458e425.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a3a502d74cc14ad79c21d2ea8458e425.jpeg)



**"Allen Russell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 18:48*

This is the other type of K40 wiring setup.  No problem, we've got you covered.  There's a ribbon cable connector on the C3D. 

The one you have circled is the Y motor, I believe, plug it in accordingly. 

As for the power connector (big white connector) take note of how it is plugged in to the stock board and do the same at the new board.  Send a pic before powering up. 


---
**Alex Hodge** *March 06, 2017 18:57*

Like Ray said, your second stepper is actually wired via the ribbon cable (endstop too).


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 18:59*

Endstops, plural, are in the ribbon actually. So you only need to worry about what you see in front of you and the C3D should have a corresponding port for each. 



I actually need pics of your machine type for the documentation so if you can take pics and send them over, would be much appreciated. 


---
**Allen Russell** *March 06, 2017 19:15*

OK, Thanks. I do that after I get back from store, Thanks


---
**Alex Hodge** *March 06, 2017 19:20*

Sorry yes, that was my typo. :) If you follow the ribbon cable back, you'll find the PCB on the x axis where your endstops and stepper are connected and combined into that one ribbon cable.


---
**Allen Russell** *March 07, 2017 01:54*

Model# is KH40W - Here are some pics

![images/b8a06f0b182e57262b7956f5059c6839.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8a06f0b182e57262b7956f5059c6839.jpeg)


---
**Allen Russell** *March 07, 2017 01:54*

![images/6d9721065850ee2bd2ee515cb61a8c7d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d9721065850ee2bd2ee515cb61a8c7d.jpeg)


---
**Allen Russell** *March 07, 2017 01:55*

![images/e00a0d07af5848ce11f99027edfdb4a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e00a0d07af5848ce11f99027edfdb4a7.jpeg)


---
**Allen Russell** *March 07, 2017 01:55*

![images/8d44fd6317c34053bda6a5592a66c2eb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d44fd6317c34053bda6a5592a66c2eb.jpeg)


---
**Allen Russell** *March 07, 2017 01:55*

![images/67e216f38bd02900e10871bba8879c92.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/67e216f38bd02900e10871bba8879c92.jpeg)


---
**Allen Russell** *March 07, 2017 01:56*

![images/6182c7d3a451013d00fb82cda978364a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6182c7d3a451013d00fb82cda978364a.jpeg)


---
**Allen Russell** *March 07, 2017 01:56*

![images/b5459cc084af3865bddb4bc8c1f41cf4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b5459cc084af3865bddb4bc8c1f41cf4.jpeg)


---
**Allen Russell** *March 07, 2017 01:56*

![images/8614f3bef789577feabb5c65491fbf4b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8614f3bef789577feabb5c65491fbf4b.jpeg)


---
**Allen Russell** *March 07, 2017 01:56*

![images/c3bb8ab0cb9f6d351c9d429450360341.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c3bb8ab0cb9f6d351c9d429450360341.jpeg)


---
**Allen Russell** *March 07, 2017 01:57*

![images/4e39d3eba79c3d3a93d78ad6a1d714f6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e39d3eba79c3d3a93d78ad6a1d714f6.jpeg)


---
**Allen Russell** *March 07, 2017 01:57*

UPGRADES

![images/cc198d461598ebbfdfed5129e549409e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc198d461598ebbfdfed5129e549409e.jpeg)


---
**Allen Russell** *March 07, 2017 01:58*

![images/9b902eaa98fb6deb6b026a0553087340.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9b902eaa98fb6deb6b026a0553087340.jpeg)


---
**Allen Russell** *March 07, 2017 01:58*

![images/076cc0ce2d4730fee877930b8d1affac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/076cc0ce2d4730fee877930b8d1affac.jpeg)


---
**Allen Russell** *March 07, 2017 01:59*

That's it so far

![images/3c80930a7d0d037713d18ec33b3222b1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c80930a7d0d037713d18ec33b3222b1.jpeg)


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/a2GULUi6paY) &mdash; content and formatting may not be reliable*
