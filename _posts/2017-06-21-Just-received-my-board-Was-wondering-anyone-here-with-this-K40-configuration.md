---
layout: post
title: "Just received my board. Was wondering, anyone here with this K40 configuration?"
date: June 21, 2017 21:17
category: "C3D Mini Support"
author: "Eric S"
---
Just received my board.



Was wondering, anyone here with this K40 configuration?



Basically the connector is 6 pins (4 used) but also as opposed to the other setup in the documentation, mine is using the 4 outer outputs of my power supply (and not the same color coding).



I could always figure out the pinout myself and match it to 

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram) but if someone already did this or knows the answer right away, that could save me from a potential error :)





![images/d9df19cd38e1612d775de17ca20114e6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d9df19cd38e1612d775de17ca20114e6.jpeg)



**"Eric S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 21:24*

Yes, this 6 pin connector has come up a few times, do you have a M2NANO (green board) or Moshi (red board)? 



We want 3 wires and 3 wires only: 

24v and Gnd to the Main Power In Screw Terminal (top left of the Mini) and L to the 2.5 - which is the 4th from the left of the bottom row of screw terminals.  



Consult a diagram of this psu pinout, meter the wires, etc...


---
**Eric S** *June 21, 2017 21:31*

Omg, just noticed the layout is on the silkscreen of the K40 PCB so I'll test for 24/gnd and should be set. 



Thats the board I currently use.  Thanks for the quick reply.

![images/b5dbe56bba0b8c98c95f3557f224710f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b5dbe56bba0b8c98c95f3557f224710f.jpeg)


---
**Eric S** *June 21, 2017 22:34*

Got the same LCD issue as some people stated earlier (haven't plugged my PC/usb in yet).  Tried cycling 3 times with that issue.  



Led activity is okay.  



Isn't the board supposed to initialize/home XY on power up or I have to manually do it in LaserWeb?  I was expecting some sort of init.

![images/7bb4e5a2c2150da63ef9dc986bf49e5d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7bb4e5a2c2150da63ef9dc986bf49e5d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 22:43*

Nope, no init activity. G28.2 to home in LW. 



If you can't get the screen to work, ever, then I would suggest getting a replacement. 

The scrambling does seem to happen sometimes and a power cycle should fix it. 


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 22:45*

A 3rd option, and this is an overall concern, is that the k40 power supply isn't providing enough juice for the screen to boot/ some other power concern. 



People do eventually need to get a separate 24v power supply to power the motion/ controller of their machines, figured I'd mention it now. 


---
**Eric S** *June 21, 2017 22:58*

If K40 supply is an issue I'll check it out, I have a lab power supply, I'll test it out.  Did you get PCB issues causing the same behaviour?


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 23:01*

I have not yet had a defect with the C3D Mini that caused the GLCD to not work.  We have had some  people get screens that don't work when they sourced themselves, this is a more common situation.  



I am, however, open to all things that might happen. 



If you can try powering over USB,  and via a different psu, this would be great. 


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 23:02*

On a side note, when people buy the GLCD from us it gets tested, this is what I've been doing for the last few hours :) ![images/8ad44ce45ecb30348a134a8e56f5a010.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ad44ce45ecb30348a134a8e56f5a010.jpeg)


---
**Eric S** *June 21, 2017 23:04*

Do I need to short the USB Power pads under the board for USB powering?  


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 23:05*

No need to touch anything there, there's a trace connecting that by default. 


---
**Joe Alexander** *June 21, 2017 23:18*

if you bought a GLCD yourself some have an issue with the connectors being reversed(i snipped off the key on the cable ends and made them reversible and then clearly marked them. alternatively you could re-solder the connections but thats a lot more work). You come across that issue with the C3D ray?



  And I have that same laser PSU the 6pin connector is pretty easy. let 2 are interlock/water protection, 3 right should be the potentiometer(G IN and 5V). the other middle pin is the L line(internally linked to the L on the far right, this one usually goes to the front test button).


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 23:18*

What issue Joe? 


---
**Joe Alexander** *June 21, 2017 23:43*

with some generic GLCD's having their connectors installed upside down? I assume that's why you test yours ahead of time :)


---
**Ray Kholodovsky (Cohesion3D)** *June 21, 2017 23:44*

Ah, not any of the ones I've gotten.  Haven't found a single issue yet, tested about 60/ 110 so far. 


---
**Eric S** *June 22, 2017 03:16*

LCD working, Laser working, printing fine!



If I plug only USB to the board there's no activity (no lights on the board or LCD) once 24V kicks in everything lights up.  



Didn't have to re-route the 24v to mosfet or anything like that, got Vin, Gnd, Laser out attached to the board, thats it. Had to reverse the Y cabling (I've followed the color direction in the installation docs, but  its wrong on my model).



Laserweb is a tad hungry on ressources, but no complains so far, I need to recalibrate everything on the optics side (new mirror and lenses) and install my Z-table, but so far everything seems to work, really happy overal.



Havent switched to lab power supply yet, got excited seeing it working and wanted to zap something asap to see greyscale processing... omg... loving it!



Only got the LCD issue one more time so far.  I'll try to reproduce with a bench power supply this weekend and report back.



Thanks everyone! 


---
*Imported from [Google+](https://plus.google.com/113547869567623989457/posts/hik3puSXS8r) &mdash; content and formatting may not be reliable*
