---
layout: post
title: "Do any of you have the LightObject Z-Table installed?"
date: December 30, 2017 00:33
category: "CNC"
author: "Joel Brondos"
---
Do any of you have the LightObject Z-Table installed? 



1) Are you satisfied with it?



2) Was it difficult to wire to the C3D controller?



3) I built a spring-loaded table modeled on a YouTube video -- and it's all right, but I guess I'm still working on mirror calibration issues at the right extremes of the work area together with various thicknesses of stock.









**"Joel Brondos"**

---
---
**Don Kleinschnitz Jr.** *December 30, 2017 02:44*

Yes....


---
**David Komando** *December 30, 2017 03:06*

Nope!


---
**Russ “Rsty3914” None** *December 30, 2017 05:12*

Not very good...

Stripped out tgreading...cheap bearings 

Everything is so tight...razor sharp edges..


---
**Russ “Rsty3914” None** *December 30, 2017 05:13*

Good concept but poorly manufactured


---
**Don Kleinschnitz Jr.** *December 30, 2017 11:13*

I only had one problem in that a shaft was bent but they promptly sent me a new one.

There was some tweaking required to get smooth operation.

Mine is plenty robust and has worked fine although I am not a heavy user.



I built my electronics totally separate from the smoothie controller and kept the unit modular. Although I did not integrate it with smoothie others have.

Information on both are on my blog....


---
**Steve Clark** *January 10, 2018 04:35*

Mine works about how I would expect It to. I have not had any problems with it.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/XYP8qyJu9h7) &mdash; content and formatting may not be reliable*
