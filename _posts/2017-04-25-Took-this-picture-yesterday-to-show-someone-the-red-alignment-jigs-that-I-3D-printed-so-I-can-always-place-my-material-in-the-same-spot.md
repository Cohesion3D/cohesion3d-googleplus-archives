---
layout: post
title: "Took this picture yesterday to show someone the (red) alignment jigs that I 3D printed so I can always place my material in the same spot"
date: April 25, 2017 21:06
category: "Show and Tell"
author: "Ashley M. Kirchner [Norym]"
---
Took this picture yesterday to show someone the (red) alignment jigs that I 3D printed so I can always place my material in the same spot. I had just finished engraving this image as a test. After having all sorts of problems sending raster files out of LW to the C3D, I was quite happy to have figured out what was going on and can finally get a proper gradient engraving.) No dithering, no halftoning, just flat out grayscale image.

![images/80238bb2f6a040df1f01757a77c5443f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/80238bb2f6a040df1f01757a77c5443f.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Marc Miller** *April 25, 2017 21:21*

Looks like you have things dialed in nicely!


---
**Ashley M. Kirchner [Norym]** *April 25, 2017 21:34*

Yeah, I'm still fiddling with it. Part of the dailing in is also figuring out a baseline for the image itself. Too dark and it just won't come out nice, and too light you end up with a lot of blown out areas. So there's adjustments to be made on both sides.


---
**Marc Miller** *April 25, 2017 23:08*

I see, yes that makes sense.


---
**crispin soFat!** *June 10, 2017 23:31*

looks good!

perhaps experiment with creating a custom curve profile for your grayscale images, one specific for output to laser. Linear curve from black to white doesn't usually give the best quality reproduction,  as you've discovered. 

Set the black point, set the white point, and then dip or boost the midtones to suit. Mess with your Levels and Curves until you get the tonal quality you want. You may also need to shift the lightest (1-12% range) and the darkest (88-99% range) areas slightly towards the center to get a good pop, as all that midtone needs a little bit of contrast from your established black and white  points or it will likely appear muddy in reproduction.


---
**crispin soFat!** *June 10, 2017 23:33*

don't squish the range too much or your gradients will likely show banding.



good looking fish!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/YYV4WYHtV4X) &mdash; content and formatting may not be reliable*
