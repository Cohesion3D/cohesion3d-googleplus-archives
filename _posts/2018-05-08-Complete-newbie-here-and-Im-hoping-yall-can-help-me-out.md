---
layout: post
title: "Complete newbie here, and Im hoping yall can help me out"
date: May 08, 2018 17:32
category: "C3D Mini Support"
author: "Lisa R"
---
Complete newbie here, and Im hoping yall can help me out.  I bought a laser second hand, and bought a cohesion3d to put in (assuming it was stock).  Well, the old owner had a board from chriscircuits (to run Mach3) and I am clueless as to what he did and how to make it adapt to the new cohesion board.  I know nothing about hooking up connections, but it looks like he spliced wires together, and the 4-pin connector that needs to go to the cohesion board has too many holes and doesnt fit, plus it looks like he skipped a hole.  Please help! 



![images/eb510e1441e8b913927132530cf1535b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eb510e1441e8b913927132530cf1535b.jpeg)
![images/eabd199d5b4685600c1ce905c4a9a95f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eabd199d5b4685600c1ce905c4a9a95f.jpeg)
![images/8a4313608c7c0e0bc7a6d15d7f6ff7ac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8a4313608c7c0e0bc7a6d15d7f6ff7ac.jpeg)
![images/6c590ea1b869f711154e887d1c7860bb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c590ea1b869f711154e887d1c7860bb.jpeg)
![images/919dd4509a63a917e1f12dcbc36ec2f7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/919dd4509a63a917e1f12dcbc36ec2f7.jpeg)
![images/a7e58b39c02d4d4952e5254e4bd308ed.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a7e58b39c02d4d4952e5254e4bd308ed.jpeg)
![images/aab16a7b10399e4276489700674e41ac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aab16a7b10399e4276489700674e41ac.jpeg)

**"Lisa R"**

---
---
**Dennis P** *May 08, 2018 18:10*

I am not an expert, but what you are dealing with is totally fixable. It will take a  little bit to sort through it. 

Those splices are kind of ugly- how comfortable are you with soldering and the like? 

How are you at reading schematics? 

[https://www.dropbox.com/s/nre8qm3cicjqt7i/Laser%20Converter%20SCH%20V1_2.pdf](https://www.dropbox.com/s/nre8qm3cicjqt7i/Laser%20Converter%20SCH%20V1_2.pdf)

Connector J6 looks like a power distribution block for 5V & 24V power. Its on page 2. 

The rest of the layout is fairly well documented so that you can transpose the old board to Ray's board. 






---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2018 19:59*

That would be the 6 pin connector older boards used... The last picture in the install guide at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) shows it... 

And also you can search this group for "moshi" - that's the old board that had these connectors.






---
*Imported from [Google+](https://plus.google.com/100279602287100387746/posts/EsqGPyUMJYs) &mdash; content and formatting may not be reliable*
