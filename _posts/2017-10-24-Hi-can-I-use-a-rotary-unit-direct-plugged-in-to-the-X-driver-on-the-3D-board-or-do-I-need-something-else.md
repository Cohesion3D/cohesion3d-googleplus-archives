---
layout: post
title: "Hi, can I use a rotary unit direct plugged in to the X driver on the 3D board or do I need something else?"
date: October 24, 2017 11:18
category: "K40 and other Lasers"
author: "warbird uk"
---
Hi, can I use a rotary unit direct plugged in to the X driver on the 3D board or do I need something else?





**"warbird uk"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2017 13:34*

A rotary plugged into A is supported (dedicated rotary support). 

Please read: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)




---
**warbird uk** *October 27, 2017 14:46*

Hi Ray, so I can plug the lead from the rotary stepper motor straight into the 'A' socket on the 3D mini board? (just making sure before I wreck anything!)


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2017 14:59*

The guide I linked shows <b>not</b> to do that. 

What rotary do you have? Do you have a driver in the A socket? 



Pictures and more information are needed if you would like to do something that is different from the guide I linked. 


---
**warbird uk** *October 27, 2017 17:39*

No, i have no driver, what will I need from you?


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2017 17:50*

It really depends.  You could probably just get a DRV8825 driver locally and try that. Just don't plug it into the board the wrong way or you'll blow everything.  Our pinout diagram on the documentation site shows the proper way to plug in the A4988 and the DRV8825. 



It's a higher power driver than the A4988 that we provide, but it needs to be cooled so you'll need a fan blowing on it. 

And it might be enough to drive your rotary, or it might not be, I'm really not sure since I don't know what rotary you have and you haven't told me. 


---
*Imported from [Google+](https://plus.google.com/110687014905709087952/posts/8ojyMsatnMz) &mdash; content and formatting may not be reliable*
