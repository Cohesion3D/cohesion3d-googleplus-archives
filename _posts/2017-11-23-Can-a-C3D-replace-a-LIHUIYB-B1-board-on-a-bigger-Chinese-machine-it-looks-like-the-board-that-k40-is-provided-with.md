---
layout: post
title: "Can a C3D replace a LIHUIYB B1 board on a bigger Chinese machine, it looks like the board that k40 is provided with ???"
date: November 23, 2017 13:11
category: "General Discussion"
author: "TheLandbo"
---
Can a C3D replace a LIHUIYB B1 board on a bigger Chinese machine, it looks like the board that k40 is provided with ???



Regards Leif.





![images/66ec0c0056bcbcbe708ce5512c69e501.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/66ec0c0056bcbcbe708ce5512c69e501.jpeg)



**"TheLandbo"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2017 13:15*

Is that a stock picture or do you actually have the machine? I need to know how many volts the PSU is and see the exact wiring. 



The answer is, of course you can do it, because all we need to do is drive some motors, read some switches, and send pulses to the laser psu... but it may not be as drop in as you are used to with the k40 because sometimes these machines come with 36v and you'll need to wire external drivers.


---
**TheLandbo** *November 23, 2017 14:05*

**+Ray Kholodovsky** Thanks for reply Ray.



No, do not have the machine. The picture is taken from the papers over an machine I intend to buy. I think the connections are very similar to the old board in my K40.



The control panel reminiscent of what's is on a "digital" k40 but there are two more buttons to raise/lower the cutting table.



Do not know how many volts there are for the board and stepper motors. The machine has a bed of 100 * 60 cm.



Should new stepper motor drivers to be purchased, it is also to survive.  ;-)



Regards Leif.


---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2017 14:10*

The reason I asked is because that picture does not show a power connector to the M2 board. 

I have also seen where these machines have a 36v psu. 

To use the onboard stepstick drivers on the C3D, and in general, we need 12-24v. 24v is best. The board cannot take much above that. 



So, buy machine, be prepared to use external driver such as TB6600 along with our external stepper adapter. Wiring example is shown on our Documentation site, under k40 z table and rotary wiring article. 


---
**TheLandbo** *November 23, 2017 14:14*

I can see on another picture, that 36 volt is printed on the PSU connector.  



Regards Leif.


---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2017 14:16*

Ok, it can be done with C3D Mini + External Stepper Adapter + External Driver like TB6600. And will need some other way to power the board, USB or small 12v power brick, many ways to do that. 


---
**TheLandbo** *November 23, 2017 14:25*

**+Ray Kholodovsky** Tnx for the information Ray. I will study the connection under k40 z table and rotary wiring article.  :)



Regards Leif.


---
*Imported from [Google+](https://plus.google.com/109955553605838159775/posts/Rv7s95z7Wtd) &mdash; content and formatting may not be reliable*
