---
layout: post
title: "Has anyone tried this with one of the larger laser cutters, such as the 50W, 60W or 80W ones seen on eBay?"
date: October 02, 2017 19:48
category: "K40 and other Lasers"
author: "Minh-Kiet Callies"
---
Has anyone tried this with one of the larger laser cutters, such as the 50W, 60W or 80W ones seen on eBay?





**"Minh-Kiet Callies"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2017 19:51*

Yep, some of the 50w blue and white ones have identical electronics, and search this group for "John Hunter" to see a thread about upgrading a 100w. When you get bigger they eventually start using external black box stepper drivers for which we have the "external stepper adapter". 


---
**Jason Dorie** *October 02, 2017 21:28*

I have one of those lasers you're referring to, and a C3D-based K40.  The big machine has a Ruida DSP controller in it that I really like, but the only real benefit I've found is faster handling of raster image data - I can raster on my big machine at 400mm/sec without issue, but Grbl and especially Smoothie can't handle those speeds with high DPI.



That said, those speeds don't produce good contrast, so that's not really a benefit.  In addition, my DSP controller has a limited job size of about 100mb, and no ability to stream jobs on the fly - Everything gets uploaded into host memory and then executed, so any job larger than the non-expandable memory has to be manually split - with large images that produces a seam.  With a C3D, you can host with SD cards of several GB in size, or you can stream an arbitrarily large job, and you can use any software capable of generating GCode instead of being locked in to basically one piece of vendor software.  (I hate it that much that I'm writing a new one).



I'm glossing over a few things here - Larger DSPs, like the Ruida, support multiple laser heads (up to 4), feed-through mechanisms, and a number of other options like dwell-based cut-through for metals, and so on.  A typical hobbyist won't use those, so I only casually mention them here.


---
**Anthony Bolgar** *October 03, 2017 17:36*

I will be upgrading my 5040 60W laser to a C3D mini this week. I will document the procedure as much as possible so Ray can add it to the documents site.


---
**Anthony Bolgar** *October 03, 2017 19:13*

The stock controller in my 5040 ( a nano) lets me raster at 350mm/s without any issues. I still have not found the upper limit of speed, will be trying 400, 450 and 500 mm/s to see how well it performs.


---
**Anthony Bolgar** *October 03, 2017 23:02*

Update - 350 seems to be the max with the nano, it stutters bad at 400




---
*Imported from [Google+](https://plus.google.com/113862922597413295359/posts/9UAw4JurvE6) &mdash; content and formatting may not be reliable*
