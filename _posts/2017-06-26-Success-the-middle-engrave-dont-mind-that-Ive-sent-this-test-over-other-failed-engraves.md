---
layout: post
title: "Success (the middle engrave, don't mind that I've sent this test over other failed engraves)!"
date: June 26, 2017 23:35
category: "C3D Mini Support"
author: "John Milleker Jr."
---
Success (the middle engrave, don't mind that I've sent this test over other failed engraves)! Previously I was having issues with my C3D Mini shifting on files and locking up when running from the SD card. A new board later I was no longer locking up, but I was still having a shift in detailed raster engravings. The new board did something other than what the old board did, after it shifted right, it eventually corrected itself. Which I thought was weird.



Then Ray and several others on the group helped point me into a new power supply. After the failure (center bottom) in this photo, I disconnected everything, hooked up a new 24v 6A power supply and ran a test. It was quite the nail biter, but the entire engrave was successful at a nice speed of 250mm/s when with the old setup I had to go under 100mm/s and even then I had shifting issues.



I can't say my problems are fixed completely, however this is the first time I've been able to engrave this file. Pictured below is one sheet of about four or five that I was extensively troubleshooting with.

![images/0e0cb77371a9067a3f5ef9429e3d9f75.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0e0cb77371a9067a3f5ef9429e3d9f75.jpeg)



**"John Milleker Jr."**

---
---
**John Milleker Jr.** *June 27, 2017 00:47*

Isn't that how it goes, boy did I speak too soon. Now that I'm trying to send the same file for my final piece, I'm getting shifting the opposite way (left) now and pretty far. About 2mm.



Adjusted the pots on the drivers 90° clockwise and running more tests. All tests at this point are with the file on the SD Card, exported from LaserWeb to the computer and then copied to the SD card. The SD card is then dismounted and the USB cable unplugged.


---
**Claudio Prezzi** *June 27, 2017 08:18*

Is that a raster image or vector? Where does the shifting happen? On the corner when the head changes direction on in mid line?



If it's on direction change, you should try to decrease the accelleraton (to like halve).



Also, changing the driver pot 90° is massive. Usually 10° already changes 0.5A. You should calculate the correct voltage on the pot (against gnd) and set it accordingly with a voltmeter.


---
**John Milleker Jr.** *June 27, 2017 20:01*

Thanks Claudio.



Vectors are typically rock solid, they were with the old board. After the 90 degree change I started having issues with Vector fills. Shifting in the wrong direction (left instead of always shifting right).



I moved the pots back to 45 degrees and have so far been having better luck.



The biggest issues I have are rasters, I assume it's on the direction change. Typically around the same area it'll overshoot or undershoot and start a few mm off. Issues seem to be better from what Ray had recommended. Copy the file to the SD Card and unmount the device in Windows. I've also found good luck with just unhooking the USB cable too.



I still have the occasional issue, but I'm getting through full jobs a good amount of time, vs, never. It's likely fine tuning at this point which is much more hopeful than my old board that just locked up tight.



Is there a writeup on the best way to calculate for fine tuning that pot?


---
**timne0** *July 18, 2017 21:25*

hey John, do you have this sorted yet? tuning pots you need a multimeter.  Can't remember the voltage on this, but basically you should set it at 0.4v and try that. 

 Then turn it up little by little until they work. 

  I think I tuned mine to 0.4v. 

 Need to do this again on the ones on the cohesion board as they're sounding crunchy again. 

[forums.reprap.org - Optimum Current for A4988 Drivers](http://forums.reprap.org/read.php?262,404150)


---
**John Milleker Jr.** *July 19, 2017 13:33*

Thanks **+timne0**! I'll bookmark that page for next time I'm in there. 



My fix turned out to be moving to GRBL-LPC. Shared my files and had another gentleman try them and he had issues with the Smoothie firmware too. Changing to GRBL was done with minimal craziness and I've been burning rasters from a Windows machine/Laserweb for about a week now.


---
**timne0** *July 19, 2017 13:45*

Ha, I moved to smoothie because of the problems I had getting it to work with my ramps board. I'll be annoyed to say the least if none of this works any better. I was having similar problems to you, but with rastered text only.


---
**John Milleker Jr.** *July 19, 2017 13:56*

Glad to hear you have something that works! GRBL isn't without its' issues, but I guess that's what we get for trying to piece together our own machines. :)




---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/JQEQvDTnwKv) &mdash; content and formatting may not be reliable*
