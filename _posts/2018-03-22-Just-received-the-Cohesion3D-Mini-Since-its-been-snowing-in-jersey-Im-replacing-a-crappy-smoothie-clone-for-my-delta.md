---
layout: post
title: "Just received the Cohesion3D Mini. Since it's been snowing in jersey, I'm replacing a crappy smoothie clone for my delta"
date: March 22, 2018 00:20
category: "C3D Mini Support"
author: "Carlos Porto"
---
Just received the Cohesion3D Mini. Since it's been snowing in jersey, I'm replacing a crappy smoothie clone for my delta. I didn't notice an example config.txt file for 3d printers on the documentation site, Didn't really feel like hunting down the pins on the pin-schematic. I did find something on the forum from months ago. Is there something more current?



Thanks





**"Carlos Porto"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2018 00:25*

Literally grab the latest firmware.bin and config file from smoothieware site and GitHub, follow instructions to config. Can send you the files I have for my MKVS delta. 


---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2018 00:25*

![images/bf0d5743814efc7a4dbff52d53ee0f80.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bf0d5743814efc7a4dbff52d53ee0f80.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2018 00:26*

The pins are exactly the same as on smoothieboard which is why I always say to start with their stock config file. 


---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2018 00:30*

Just send me a German hacksaw. 


---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2018 00:30*

**+René Jurack** would understand :) 


---
**Carlos Porto** *March 22, 2018 00:40*

Actually he put a tariff on imported German hacksaws. ;)






---
**René Jurack** *March 22, 2018 05:37*

![images/04cb7918c32890691e2d9ac19a8bf8d1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/04cb7918c32890691e2d9ac19a8bf8d1.png)


---
*Imported from [Google+](https://plus.google.com/112207937847873893334/posts/EQ3h6DSvYBp) &mdash; content and formatting may not be reliable*
