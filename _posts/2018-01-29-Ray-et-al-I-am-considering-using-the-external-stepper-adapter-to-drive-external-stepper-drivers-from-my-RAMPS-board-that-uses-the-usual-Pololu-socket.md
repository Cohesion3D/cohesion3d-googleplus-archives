---
layout: post
title: "Ray et al: I am considering using the external stepper adapter to drive external stepper drivers from my RAMPS board that uses the usual Pololu socket"
date: January 29, 2018 02:34
category: "General Discussion"
author: "Dennis P"
---
Ray et al:



I am considering using the external stepper adapter to drive external stepper drivers from my RAMPS board that uses the usual Pololu socket.



Is there a data sheet or wiring schematic available?  I am wondering which pins it uses. 



Is there a provision to take +5V from VDD? Which ground pin does it use?



Any other thoughts or tips on using these? 



Thanks



Dennis







**"Dennis P"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 29, 2018 02:37*

Ramps (if arduino is under it) will put out 5v signals. So if your stepper driver supports it hook those up to + and - to gnd. 


---
**Dennis P** *January 29, 2018 03:15*

you mean VDD and Ground on the adapter?  That is why I was asking about the ground pin.  


---
**Ray Kholodovsky (Cohesion3D)** *January 29, 2018 03:16*

There is a Ground on the adapter. There is no VDD on the adapter. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/84C1e42VuBr) &mdash; content and formatting may not be reliable*
