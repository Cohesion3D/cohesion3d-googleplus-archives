---
layout: post
title: "Can not get the laser to do anything"
date: March 19, 2017 22:14
category: "General Discussion"
author: "John Austin"
---
Can not get the laser to do anything.

Installed board 

downloaded [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)

installed ready to use

plug usb into computer

windows driver installer appears run then get fail to install



open laserweb4

click on comms set to usb, com3, budrate not sure what it supposed to be but set at 19200

setting 

generaic smoothie

a video of the board lights

[https://drive.google.com/file/d/0B85-ky4YBcqJbjQ0VktyUjJGU0E/view?usp=sharing](https://drive.google.com/file/d/0B85-ky4YBcqJbjQ0VktyUjJGU0E/view?usp=sharing)



![images/1d4019cb9afc8b7a7c20c1a4fc6b07da.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d4019cb9afc8b7a7c20c1a4fc6b07da.png)
![images/18d16da9106e5cd466d177ca48bae3f0.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/18d16da9106e5cd466d177ca48bae3f0.png)
![images/718bbce91c351ed638e99b975edc9979.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/718bbce91c351ed638e99b975edc9979.png)
![images/2ae7bd8583d155ed43d58cfcf42bde6c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ae7bd8583d155ed43d58cfcf42bde6c.png)

**"John Austin"**

---
---
**Ashley M. Kirchner [Norym]** *March 20, 2017 01:57*

I ran into this failed driver installation as well. I removed the v1.1 drivers, and went the manual route with the zipped v1.0 drivers. Works like a charm.


---
**John Austin** *March 20, 2017 01:59*

K I'll try that


---
**John Austin** *March 20, 2017 02:07*

Ill have to look for them later this week.




---
**Ashley M. Kirchner [Norym]** *March 20, 2017 02:12*

Same place you got the v1.1 ones, same page.


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 02:13*

It's the 1.0 zip here: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)



Other notes: baud should be 115200, all looks good with the board functionality from your video, and you may want to start with LaserWeb3 and get everything rolling there before moving to LW4.


---
**John Austin** *March 20, 2017 02:18*

Do i still have to go through the the cmd page to load laserweb3? 


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 02:19*

There's a script file in the folder to start it for you.  But you do have to do the CMD way to install it the first time. 


---
**John Austin** *March 20, 2017 02:20*

OK I will try it again. Thanks 


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 04:13*

**+Peter van der Walt** at the end of our k40 upgrade support article: 



LaserWeb3 is the current stable build.  Instructions for install are in the Wiki here: [https://github.com/LaserWeb/LaserWeb3/wiki](https://github.com/LaserWeb/LaserWeb3/wiki)

LaserWeb4 is currently in development and needs testing.  It has installers here: [https://github.com/LaserWeb/LaserWeb4-Binaries](https://github.com/LaserWeb/LaserWeb4-Binaries) and documentation here [http://cncpro.co/](http://cncpro.co/).



[cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions.  : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



has something changed? 


---
**John Austin** *March 27, 2017 22:00*

Sorry haven't gotten back to this, just got out of the hospital.  I uninstalled everything and re installed.

But keep getting server error file not found.

[photos.google.com - Untitled](https://goo.gl/photos/jFv3JM51kXDSjCtBA)




---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/DJjFs9nrHAg) &mdash; content and formatting may not be reliable*
