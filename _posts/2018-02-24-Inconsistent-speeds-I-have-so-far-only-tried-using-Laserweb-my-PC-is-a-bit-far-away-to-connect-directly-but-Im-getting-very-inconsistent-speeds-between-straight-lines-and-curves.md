---
layout: post
title: "Inconsistent speeds. I have so far only tried using Laserweb (my PC is a bit far away to connect directly) but I'm getting very inconsistent speeds between straight lines and curves"
date: February 24, 2018 14:52
category: "Laser Web"
author: "Tako Schotanus"
---
Inconsistent speeds.



I have so far only tried using Laserweb (my PC is a bit far away to connect directly) but I'm getting very inconsistent speeds between straight lines and curves. Which means that the power I dial in just never is correct, it's either correct for the straight lines and it will just char/evaporate/burn anything on the curves, or it's correct for the curves and it won't even have cut halfway through on the straight lines.



Am I doing something wrong here?





**"Tako Schotanus"**

---
---
**Mike De Haan** *February 24, 2018 15:56*

Are the speeds consistent between vertical and horizontal straight lines?


---
**Tako Schotanus** *February 28, 2018 12:30*

Sorry, for not replying before but I had my machine taken apart for modifications :)



So I just tested it and no, horizontal lines, vertical lines and any other orientation seems the same (printed squares and stars to test). But the moment it's curved (was printing a simple crescent moon) the cutter slows down and even stutters and turns off the laser for fractions of a second.


---
**Tako Schotanus** *February 28, 2018 12:53*

Hmm, although if I cut just a simple plain circle it's not so bad, so perhaps there's something in the design file or the way it gets converted to gcode...


---
*Imported from [Google+](https://plus.google.com/+TakoSchotanus/posts/8sfvboeVru4) &mdash; content and formatting may not be reliable*
