---
layout: post
title: "Hi, please see image. I am hoping you can tell me where the orange wire and the blue wire from my FSL Gen 5 attaches to on the C3D"
date: November 17, 2018 18:48
category: "C3D Mini Support"
author: "Em W"
---
Hi, please see image. I am hoping you can tell me where the orange wire and the blue wire from my FSL Gen 5 attaches to on the C3D. Currently the FSL controller has an:



Orange wire from the laser PSU "L" to the FSL controller board AND then



Blue wire from the FSL controller board to the laser Tube (and I intend to put in a mA meter, so hopefully this is how it goes. This is how the Gen 5 schematic graciously supplied by Tech Bravo has it configured for Gen 5)  



I did find the FET1Bed (-) assumption ? from their K40 install directions for "another possible wiring config" C3D section.



Anyway, the image should make simple what my question is, hopefully:-) Thank you in advance!













**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2018 18:51*

Need to see the pics of the actual LPSU. 


---
**Em W** *November 17, 2018 20:49*

**+Ray Kholodovsky** Thank you for the reply. This should help [emsplace.com](https://www.emsplace.com/fsl-gen5/images/psu-fsl.jpg) 


---
**Em W** *November 17, 2018 21:20*

Since the FSL does not have a mA meter or a separate PWM as hardware, I would like to add a mA meter. For the PWM, I suppose this can be address later? Part of this discussion does make mention of the mA per the schematic, and as I move into software later I'm sure that power levels (how to) will come up:-) Trying to step this off one thing at a time. Thank you


---
**Ray Kholodovsky (Cohesion3D)** *November 18, 2018 02:17*

Can you send the actual raw pics? I have trouble with the collages. I like them, I just can’t work only off of them. 


---
**Em W** *November 18, 2018 19:22*

**+Ray Kholodovsky** image [emsplace.com](https://www.emsplace.com/fsl-gen5/images/lpsu.jpg) Note: that the test button next to the red diode not well seen says "text" instead of "test"


---
**Em W** *November 18, 2018 20:05*

AND very helpful (should answer a lot of questions:-) is this FSL Gen 5 schematic [lasergods.com - FSL Gen.5 Hobby Laser Schematic](https://lasergods.com/fsl-gen-5-hobby-laser-unofficial-schematic/) . it will show you how it is configured. Also, shows the LPSU. This is basically how mine is except mine didn't come with the water sensor or mA 


---
**Em W** *November 26, 2018 16:35*

**+Ray Kholodovsky** Hello, still waiting for a response to my post from the customer service support department. Supplied the asked for photo. Thank you!


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2018 02:11*

Ok, so the orange L you have indicated is not what we want.  We do not want the high voltage wire going to the tube. 



Here's what we want.  You need to take the hot glue off your laserpsu so that we can see exactly which wires are going where. 



In the picture I have attached is L, the active low line to fire the laser. we want this to go to "p2.5 bed-" if you see our pinout diagram/ install guide. This is the 4th screw terminal from the left on the bottom of the board. 









![images/312a1e0adffa752d7bb0f635c5432741.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/312a1e0adffa752d7bb0f635c5432741.png)


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2018 02:12*

You also need a common ground from the LPSU (use the neighboring pin G and connect that to the Ground on the C3D).  



Do this, show me pics of your wiring, don't power anything up yet. 


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/5u69muFtykx) &mdash; content and formatting may not be reliable*
