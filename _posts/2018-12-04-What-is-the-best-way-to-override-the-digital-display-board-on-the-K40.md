---
layout: post
title: "What is the best way to override the digital display board on the K40?"
date: December 04, 2018 17:29
category: "K40 and other Lasers"
author: "Nicholas Zastrow"
---
What is the best way to override the digital display board on the K40? I just upgraded to the C3D board and want to control power only through software. Is that possible, or do I need to leave that board connected?





**"Nicholas Zastrow"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 17:34*

You can't override it because you need something to set the maximum power which is going to the LPSU.  I highly recommend installing a pot, but you could also connect IN to 5v on the LPSU. By doing this you limit the grayscale resolution you can achieve. 


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 17:35*

You would also severely reduce the life of your tube if you ran at 100% power in the software then. 




---
**Nicholas Zastrow** *December 04, 2018 17:41*

**+Ray Kholodovsky** Is there a difference between 100% laser power (from the digital board) and setting 80% in software versus 80% board power and 100% in software?



What are the recommended electronics if I want to just replace that board with a current meter and a potentiometer? Trying to streamline the panel and I don't really trust the power readout. 



This board is super nice by the way, it makes the K40 run a billion times better.


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 17:49*

Bummer, our benchmark was at least a trillion.  Maybe the new board will be better...



Anyways, it's just a fraction of a fraction:

Software % is the board pwm multiplied by the % set on the pot/ digital panel.  



The kicker however is the % range the board pwm operates at, if you have a pot you can get more bits out of a narrow window which improves your grayscale resolution.  I've explained this better before, but you get the idea? 



If you get it, great, but I have to account for everyone else. My options are keep the cap in config or remove it and start selling replacement laser tubes because people will urn at 100% of 100% and burn their tubes. 


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2018 17:50*

**+Tech Bravo** you got some nice lasergods articles to link?  



I have some nice pots on the way to stock, and mA meters should be on the shopping list. 


---
**Tech Bravo (Tech BravoTN)** *December 04, 2018 18:00*

**+Ray Kholodovsky** i think a rough idea in the diagram for the fsl but its hard to understand if you 've never seen it...... but i will make one this evening :)


---
**Tech Bravo (Tech BravoTN)** *December 04, 2018 18:03*

**+Don Kleinschnitz Jr.** already has documentation on this: [donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)




---
**Tech Bravo (Tech BravoTN)** *December 04, 2018 18:03*

and this: [donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)




---
**Nicholas Zastrow** *December 04, 2018 18:05*

**+Tech Bravo** **+Ray Kholodovsky** Many thanks! I spent a while searching and couldn't find the info, this is perfect!


---
*Imported from [Google+](https://plus.google.com/112699782942754315401/posts/XHet8o4qfzd) &mdash; content and formatting may not be reliable*
