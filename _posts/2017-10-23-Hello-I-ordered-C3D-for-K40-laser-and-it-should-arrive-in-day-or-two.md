---
layout: post
title: "Hello I ordered C3D for K40 laser and it should arrive in day or two"
date: October 23, 2017 17:17
category: "General Discussion"
author: "bojan repic"
---
Hello



I ordered C3D for K40 laser and it should arrive in day or two.

My question is: 

I am going to add a power supply for C3D and would like to know if 24V 2A will be ok, or should I get more powerful one?

And should I connect ground from new PS to Laser PS or leave it separate?



Thank you



Bojan





**"bojan repic"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2017 17:21*

You should get a 24v 6a. You should wire it to +24v and gnd on the board's "main power in" terminal. 

You should leave the +24v from the k40 disconnected. But the ground needs to stay connected/ shared. 


---
**bojan repic** *October 23, 2017 17:42*

Ok, will get 6A. Thank you.


---
*Imported from [Google+](https://plus.google.com/115068391387492902728/posts/Zn28UAeheff) &mdash; content and formatting may not be reliable*
