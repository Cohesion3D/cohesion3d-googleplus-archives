---
layout: post
title: "Hello I have a Cohesion3d mini and a K40 laser"
date: December 15, 2017 04:43
category: "C3D Mini Support"
author: "Benjamin Mitchell"
---
Hello I have a Cohesion3d mini and a K40 laser.  My PSU is a bit different than the ones I have seen here and on the website.  I got everything to work except the PWM for the laser. I think the laser wire from the power supply is supposed to connect to the center conductor of the K40 PWM connector above the servo connector.  Is that true?  When I connected it up the laser is always on. Any thoughts? Do I need to do something different with the firmware?





**"Benjamin Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 15, 2017 14:42*

Pictures of your wiring and connections please 


---
*Imported from [Google+](https://plus.google.com/109785533751129403590/posts/QZW5mBVB4tc) &mdash; content and formatting may not be reliable*
