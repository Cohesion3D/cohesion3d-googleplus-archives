---
layout: post
title: "Originally shared by Vincent Bernolin Hello everybody Ive updates my k40 with the 3dcohesion mini"
date: March 15, 2018 15:39
category: "C3D Mini Support"
author: "Vincent Bernolin"
---
<b>Originally shared by Vincent Bernolin</b>



Hello everybody



I’ve updates my k40 with the 3dcohesion mini. Sure the motors run 10 times better with this driver. However I’ve got a strange issue. 

At start the machine is homing normally, the head goes to the limit switches at the rear left of the machine . My X Y home is at the front left. The travel of machine is 560x400

Then, in the « move » panel, I click on the « get position «  and I can read X:0.000 Y:200.000. 

Where does come from this 200 ? I was not able to fix this. The software considers the head is at half the travel on the Y axis, as you can see on the screen capture. In reality the head is parked at rear left as you can see on the pic

As a result I lose half the working surface. And if I try to go farther than Y200, of course the head bumps in the structure



Any idea welcome 



Thank you





![images/8b5f174b3caf1ca95de5c0e0e9b83bcc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8b5f174b3caf1ca95de5c0e0e9b83bcc.jpeg)
![images/3970284cd8d90af609b79afdce54130c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3970284cd8d90af609b79afdce54130c.jpeg)

**"Vincent Bernolin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2018 15:57*

200 is the Y size of the K40 and the limit switch is at the rear. 



What machine is this?


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2018 16:11*

Anyways assuming the homing switches are in the rear and left, you need to change alpha max and beta max to your sizes in the board's config.txt file. Then save, dismount, reset the board. And then change the values in Lb Setup as well. 


---
**Vincent Bernolin** *March 15, 2018 18:42*

The machine is imported from china, a LY6040 if I remember well. If I understrand what you are telling me, the config .txtx is on the SD card on the board and the LB setup is accessible via the setup menuin he software ? 




---
**Vincent Bernolin** *March 15, 2018 18:46*

Is there any way to reset the board or simply dewire is enough ? Thanks by

 advance


---
**Vincent Bernolin** *March 15, 2018 18:47*

By the way the driver is surpringly effective, the rapid moves are really fast 


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2018 18:47*

Config is on the board and the second one is the Device Setup in Lighburn Software.



Small yellow button at corner of board near the lights. 


---
**Vincent Bernolin** *March 15, 2018 21:36*

A last question. Should I reset the board after reinstalling the SD card or before? Sorry , I wasn't able to find the answer googling everywhere. Thanks


---
**Jim Fong** *March 15, 2018 21:36*

**+Vincent Bernolin** just type in reset in the LightBurn console window. The c3d will reboot. 



Reset board after any configuration changes to config.txt. It will reboot and apply the new settings.  Sdcard must be  inserted first. 



If you don’t know already, the sdcard shows up as a USB drive under Windows.  I use a text editor to modify the config.txt and save, then reset. 



No really reason to remove the sdcard unless you want to install different firmware like grbl-lpc. 


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2018 22:02*

Personally I would not insert/ remove the sd card with the board on.



The board shows the SD Card when you plug it in over USB cable.  If you want to remove the SD Card, do it with the power off.  Then you do not need to reset the board, just plug the card back in then turn the board on. 


---
**Vincent Bernolin** *March 15, 2018 22:11*

ok well noted, thanks to all




---
**Vincent Bernolin** *March 16, 2018 08:41*

It worked. I’ve got now full access to the surface of machine . Thanks again


---
*Imported from [Google+](https://plus.google.com/109023396074615225526/posts/fWByjgfVqtb) &mdash; content and formatting may not be reliable*
