---
layout: post
title: "Shared on August 22, 2018 22:44...\n"
date: August 22, 2018 22:44
category: "K40 and other Lasers"
author: "Trevor Hinze"
---


![images/8ba75bc771cc029a2dcbed7155005c9b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ba75bc771cc029a2dcbed7155005c9b.jpeg)



**"Trevor Hinze"**

---
---
**Marc Miller** *August 22, 2018 22:54*

Very nice looking panel.  Thank you for showing it with everything installed.



What kind of knob is that on the lower left for "Laser Adjust"?  I don't think I have ever seen anything like that before.


---
**Trevor Hinze** *August 23, 2018 03:37*

**+Marc Miller** I replaced the stock pot with a 10 turn pot, and a knob with numbers that count up from zero to 999.  It prob wont really get much ise simce the cohesion controlls the laser output but basically it will allow for very fine adjistment of laser output while at the same time making it easy to get repeatable results down the road.


---
*Imported from [Google+](https://plus.google.com/106981345031249926577/posts/igZJZvbErHK) &mdash; content and formatting may not be reliable*
