---
layout: post
title: "a full dat messing on Sunday. Managed to get some good tests done using Laserweb3"
date: February 20, 2017 11:12
category: "General Discussion"
author: "Colin Rowe"
---
a full dat messing on Sunday.

Managed to get some good tests done using Laserweb3.

Still starts up in ERROR mode and have to reset it before i can do anyting.

Also need a better usb because keep getting EMI errors

Question for programmers.

I make programs in Mastercam and all run great on works machines but if i import the Gcode prog into laserweb3 it does not like the G02/G03.

Can i import direct to SD card on the board and would they run from there. I think i saw somewhere about G02/G02 working with smoothie.

Thanks



![images/1db979b4bacb68e77dcc9b1fb6527556.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1db979b4bacb68e77dcc9b1fb6527556.jpeg)
![images/cba4569478bc7627f24f142d83a2539d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cba4569478bc7627f24f142d83a2539d.jpeg)
![images/7d6b27b6a3092308873ca7406f94d89f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7d6b27b6a3092308873ca7406f94d89f.jpeg)

**"Colin Rowe"**

---
---
**Colin Rowe** *February 20, 2017 12:04*

thanks for all replies getting there slow but sure


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 01:01*

Last I checked it should be just G2 and G3

Smoothie interprets them fine. 


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/3ZDTZ7kAdjF) &mdash; content and formatting may not be reliable*
