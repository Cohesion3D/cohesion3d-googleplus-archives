---
layout: post
title: "Hi all. New to the laser world and not much of an electronics person so please bear with me on this"
date: October 27, 2018 03:17
category: "C3D Mini Support"
author: "Drew Martel"
---
Hi all. New to the laser world and not much of an electronics person so please bear with me on this.  Here's my issue.  Installed my C3D tonight.  When I turn the machine it will not go to home.  My computer does recognize the board and has installed the drivers, but lightburn does not recognize it at this point.  Any help is appreciated and again... not an electronics guy so explaining like i'm 5 is much appreciated.  Thank you!

![images/febe0d2af57adbe4eeb297d213ddf25a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/febe0d2af57adbe4eeb297d213ddf25a.jpeg)



**"Drew Martel"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2018 03:35*

It won’t Home on boot and you might need drivers. What OS? 


---
*Imported from [Google+](https://plus.google.com/104209078976577713137/posts/AWuhCPFd61a) &mdash; content and formatting may not be reliable*
