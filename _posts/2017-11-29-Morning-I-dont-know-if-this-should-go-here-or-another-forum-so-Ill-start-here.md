---
layout: post
title: "Morning. I don't know if this should go here or another forum so I'll start here"
date: November 29, 2017 19:01
category: "General Discussion"
author: "Alex Raguini"
---
Morning.  I don't know if this should go here or another forum so I'll start here.



I am planning on installing a z-table to a k40 running C3D.  I've read the docs about adding a z-axis table and know the software I'm using will support it.



My question is how do I connect manual switches to move the table up and down and retain the ability to use software switches?  I'm not sure how to do that.





**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 30, 2017 03:06*

You will want to read:

[smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch)



and consult the pinout diagram:[https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)



The short answer is you will want to find 2 free GPIO pins - I am going to assume that you will actually use Z MIN and Z MAX endstop ports for limit switches so you don't overshoot and crash your bed - so XMAX and Y MAX would be easy choices. 



The zplus10 example on the smoothie switch page is actually very perfect.  Increments of 1 - 5 would probably be more appropriate. 



You can also define commands to add to the GLCD under the "custom" menu.  One of those should/ might be to home Z (again assuming you will add limit switches). 



Finally, use the M119 command well. 


---
**Don Kleinschnitz Jr.** *November 30, 2017 05:58*

This may help:[http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)



[donsthings.blogspot.com - K40-S Lift Table Integration](http://donsthings.blogspot.com/2016/11/k40-s-lift-table-integration.html)


---
**Alex Raguini** *November 30, 2017 06:08*

Thanks for all the information.




---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/aoa8JaTA7pk) &mdash; content and formatting may not be reliable*
