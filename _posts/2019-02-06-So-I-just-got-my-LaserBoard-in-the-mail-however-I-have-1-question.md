---
layout: post
title: "So I just got my LaserBoard in the mail, however I have 1 question..."
date: February 06, 2019 22:09
category: "K40 and other Lasers"
author: "Sean Cherven"
---
So I just got my LaserBoard in the mail, however I have 1 question... 



How do I hook it up so I have Variable Laser Power on my K40 Machine? 

Is there a PWM wire I have to connect somewhere?





**"Sean Cherven"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2019 22:18*

G+ is shutting down and we are moving to [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](http://forum.cohesion3d.com) 



Please post your question again there and we will be happy to help. 


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/WPeTeuEjWAR) &mdash; content and formatting may not be reliable*
