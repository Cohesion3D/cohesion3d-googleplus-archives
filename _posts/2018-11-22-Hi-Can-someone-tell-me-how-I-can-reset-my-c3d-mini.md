---
layout: post
title: "Hi, Can someone tell me how I can reset my c3d mini?"
date: November 22, 2018 18:23
category: "General Discussion"
author: "John McKeown"
---
Hi, Can someone tell me how I can reset my c3d mini? Reset isn't working and power up doesn't seem to work either. I'm trying to use GRBL or if I could reset, I'd use Smoothie. Anything that might actually work would be good. Last time I tried this, I tried several different, good quality mini SD cards and none made any difference. I've been trying to diagnose problems with running my laser but am starting to think that the problem might be with the control board itself. It is prbably user error, but I have exhausted all the possible fixes that I am aware of. At the minute I can't use it at all. I have the firmware .bin on the card but it doesn't reset to firmware. CUR and the control is completely erratic. Thanks.





**"John McKeown"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 03:24*

There is a reset button on the board, but you should only move the card in and out with the board off.  Just turning the board on is all you need to do. A few people have said that GRBL does like to be "sticky", so all I can tell you is to format a good card that is less than 32gb (2-4gb is great) with FAT32, put just the firmware file from batch 3 on there, safely eject it from your computer, put it into a board that is off, and try powering the board up - either with a USB cable or with a good 24v psu powering the board.  



Not sure if you have a separate 24v PSU and have isolated the board from the laser frame, but these are both important things. 


---
**John McKeown** *November 27, 2018 13:07*

Hi Ray, thanks but I think we went through this before without success. I tried several good quality cards and none of them seem to work in the way they're supposed to. I've just ordered a 24v supply and will isolate the board from the frame. I don't know if this will fix the reset problem. In fact I don't even know if this is a problem. At the minute I have GRBL firmware but the file doesn't change from .bin to .CUR as the documentation says. I don't know if this is an issue or if I can be sure my problems lie elsewhere. I guess when I fit a 24v supply and change the endstops it might be clearer.




---
*Imported from [Google+](https://plus.google.com/108648609732667106559/posts/RXX9MZochoE) &mdash; content and formatting may not be reliable*
