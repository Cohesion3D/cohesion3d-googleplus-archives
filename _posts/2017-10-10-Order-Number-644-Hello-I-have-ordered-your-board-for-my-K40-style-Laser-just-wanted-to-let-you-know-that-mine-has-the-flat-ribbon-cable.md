---
layout: post
title: "Order Number: 644 Hello, I have ordered your board for my K40 style Laser & just wanted to let you know that mine has the flat ribbon cable"
date: October 10, 2017 06:42
category: "K40 and other Lasers"
author: "Warren Eames"
---


Order Number:	644

Hello, 

I have ordered your board for my K40 style Laser & just wanted to let you know that mine has the flat ribbon cable. Also is Laserweb the only software that can be used or do you think it is the best to use with your board? I have 2 other Lasers one uses LaserCut the other uses RDWorks. It would be nice to not have to learn another bit of software. I presume neither of those can be used with this board? Also do you know when my order will be shipped?

Kind Regards

Warren Eames





**"Warren Eames"**

---
---
**Jason Dorie** *October 10, 2017 08:04*

There is a piece of software that’s unreleased, but should be soon, that will talk to the C3D and the Ruida controller.  Check out [facebook.com - Security Check Required](http://Facebook.com/LightBurnLaser) for details.  I’m expecting to release in about a month.


---
**Ray Kholodovsky (Cohesion3D)** *October 10, 2017 13:28*

Thank you for your patience over the holiday weekend, I will be answering emails today. 


---
*Imported from [Google+](https://plus.google.com/114364914232984549844/posts/cjmmLrjLr2W) &mdash; content and formatting may not be reliable*
