---
layout: post
title: "Looking at upgrading newer K40 with m2nano board to the Cohesion3d"
date: March 20, 2018 14:53
category: "K40 and other Lasers"
author: "Brad Januchowski"
---
Looking at upgrading newer K40 with m2nano board to the Cohesion3d. It has digital power display and test button,etc.. There there any pointers or issue when upgrading. Is everything going function as it is?





**"Brad Januchowski"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 14:56*

The wiring and swap <b>should</b> be the same. I would caution you to be prepared to install an mA meter and possibly replace the panel with a potentiometer if you desire finer control of power when grayscale engraving. The digital panel seems to make life a little harder here


---
**Brad Januchowski** *March 20, 2018 15:04*

Thanks. Any recommendation on those parts ( brand/type,etc..)Is the LCD a necessity?


---
**Don Kleinschnitz Jr.** *March 20, 2018 15:06*

Some more info on panel ....



[donsthings.blogspot.com - Understanding the K40 Digital Control Panel???](http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)


---
**Don Kleinschnitz Jr.** *March 20, 2018 15:09*

**+Brad Januchowski** 



[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)



[http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)


---
**Brad Januchowski** *March 20, 2018 17:42*

Thanks


---
*Imported from [Google+](https://plus.google.com/111956962431778575847/posts/Jwax6mgTakb) &mdash; content and formatting may not be reliable*
