---
layout: post
title: "First print on my \"BigOne\" without tuning things"
date: March 18, 2018 20:31
category: "Show and Tell"
author: "Marc Pentenrieder"
---
First print on my "BigOne" without tuning things. Thanks to **+Ray Kholodovsky**  for his nice Hardware and help. The print is not complete because the filament was out ;-(

![images/bd3379cb1f7e30af5b931ec52b928c59.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bd3379cb1f7e30af5b931ec52b928c59.jpeg)



**"Marc Pentenrieder"**

---
---
**Marc Miller** *March 18, 2018 20:34*

Wow, that's an impressive looking build.  Cool!


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2018 20:35*

Good one!


---
**Marc Pentenrieder** *March 18, 2018 21:03*

This is a 10cm^3 xyzcalibration cube


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/jYjT17rPzAf) &mdash; content and formatting may not be reliable*
