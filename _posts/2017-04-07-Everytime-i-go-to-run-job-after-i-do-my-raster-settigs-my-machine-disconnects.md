---
layout: post
title: "Everytime i go to run job after i do my raster settigs, my machine disconnects...."
date: April 07, 2017 19:33
category: "General Discussion"
author: "michael chartier"
---
Everytime i go to run job after i do my raster settigs, my machine disconnects.... any reason as to maybe why? 



In my settings i have generic smoothie (did not change anything in there... but in this same question am i supposed to?)



In my cam for laser raster i have min0 max 30 diameter .1, passes 1 cut rate 300mm/m trim pixels, join pixels and burn white selected





![images/49340b7cc7fee7d3c311d529a4ba5437.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/49340b7cc7fee7d3c311d529a4ba5437.jpeg)



**"michael chartier"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 19:42*

Is this you or the other guy?  Because you said you installed the Smoothie drivers on Win 10 and this looks like Win 10... So let's start with that. 


---
**michael chartier** *April 07, 2017 20:09*

This is me ill uninstall them


---
**michael chartier** *April 07, 2017 20:12*

I uninstalled them, plugged back in and connected, still doing same thing


---
**michael chartier** *April 07, 2017 20:12*

Could it be i over adjusted my stepper module?


---
**michael chartier** *April 07, 2017 20:14*

Someone else has mentioned to change to a shielded usb cord, im still using the same usb cord that came with my k40..... may be a lil overthinking, never heard of usb cord being an issue


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 20:14*

Restart computer after uninstall. 


---
**michael chartier** *April 07, 2017 20:18*

Quick question while im waiting, is there a max on mm/min i can run wirh this software?


---
**Ashley M. Kirchner [Norym]** *April 07, 2017 20:20*

MAX would be just before the head goes slamming through the side wall of the machine. :)


---
**michael chartier** *April 07, 2017 20:23*

Haha i like that answer, so running it at say, 10,000 mm/min as long as im not about to throw my laser head theough the machine ill be fine eh?


---
**Ashley M. Kirchner [Norym]** *April 07, 2017 20:24*

Each job is different, and while you can certainly get to a average speed that works for most, there's a chance you'll be creating a set of defaults for various types of raster jobs. For example, when I'm rastering something small, I lower the speed so the thing doesn't vibrate itself off of the table flying back and forth. But, on a larger area, I go ahead and speed it up.


---
**Ashley M. Kirchner [Norym]** *April 07, 2017 20:27*

Consider that the default raster speed on the Chinese controller is 400mm/s, which translated to 24,000mm/min. However, also remember that the Chinese controller only did ON/OFF on the laser, no PWM control, so it never had to process any additional codes to manipulate the laser, it can just fly by turning it on and off. With the Smoothieware boards (such as the C3D), there's a lot more information, so you don't necessarily want to hit that top speed every time. Play with it. I generally start at 10,000 and slowly increment that up.


---
**michael chartier** *April 07, 2017 20:28*

Yeah im just trying to get to where i was when dithering even tho i know its a different process, for anodized aluminum it ran at around 2-300mm/second .... since this runs mm/min im converting and trying at 166mm/second by using 10000mm/min .... seeing what it does if i can get past this shutting off on me



But yeah after uninstalling, restarting, and plugging back in, it still shuts off on me...


---
**Ashley M. Kirchner [Norym]** *April 07, 2017 20:31*

You don't have to work with mm/min. If you're more comfortable with mm/s, you can change that under Settings -> Application -> Feed Units


---
**michael chartier** *April 07, 2017 20:32*

After every step im learning while going through this im noticing how much easier this software can be easy to use.... just need to get to the part where i can actually run a test 😂😂


---
**michael chartier** *April 07, 2017 20:35*

Thank u for that...... i do feel a little dumber as i ask these questions when i shouldve noticed that going through all the tabs..... but i didnt.... <b>sigh</b> ill just chalk it up to me still learning.... and i have a long way to go... but.... now back to it shutting off on me, i may have gone a full turn with the pwm on the stepper module, should i back that off? Maybe its trying to put too much power through?


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 20:37*

A high quality USB cable would be high on the list as well.  

Look, essentially it comes down to noise or software issues.  



In order for us to help you we need A LOT more information every step of the way, please read this: [plus.google.com - How to ask for help from the support community [IMPORTANT]: -Please speak in...](https://plus.google.com/u/0/+RayKholodovsky/posts/U2r2sgrqb54)

and know that picture of screen from phone is unpleasant to look at so please use Print Screen on your computer to take a screenshot of the entire window.  



And we need to start with some very simple stuff like 

I powered on the laser.  I plugged in USB.  I waited a moment.  I opened LaserWeb.  I hit connect.  I tried to jog around.  Let's start there.  

Something went wrong!   I did <i>__</i>,  I expected <i>__</i> to happen, but instead <i>__</i> happened. 



That's my quick rant on effective tech support.  Rant concluded :)


---
**michael chartier** *April 07, 2017 20:37*

Still getting same error code overlapping result


---
**Ashley M. Kirchner [Norym]** *April 07, 2017 20:39*

(I suspect the next "rant" will come with an invoice attached.)


---
**Claudio Prezzi** *April 07, 2017 23:27*

That error basically sais that there is a problem with the serial connection. Stabilitiy issue, timeing issue, driver problem, what ever. 

You should always use a good shielded USB cable that is as short as possible. And try first with low feeds like 20-40mm/s. Increase when that is working stable.


---
**Claudio Prezzi** *April 07, 2017 23:33*

**+Ray Kholodovsky** In such cases, you could try to deactivate the second USB serial port in smoothies config.txt, and eventually also the USB MSD. That helped on one of my boards.


---
**Claudio Prezzi** *April 07, 2017 23:34*

See [smoothieware.org - usb [Smoothieware]](http://smoothieware.org/usb) for details.


---
**michael chartier** *April 07, 2017 23:54*

Thank you


---
**crispin soFat!** *June 10, 2017 22:19*

**+michael chartier** 

many USB cables have "issues" and the ones included free(!) often suck.


---
*Imported from [Google+](https://plus.google.com/116057139333311855526/posts/HN5exL6tkN4) &mdash; content and formatting may not be reliable*
