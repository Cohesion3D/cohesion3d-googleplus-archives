---
layout: post
title: "Hi. As I'm getting ready to install a C3D Mini on a K40, I'm thinking more and more that ethernet might be nice option"
date: March 08, 2017 04:58
category: "K40 and other Lasers"
author: "Dave Durant"
---
Hi.



As I'm getting ready to install a C3D Mini on a K40, I'm thinking more and more that ethernet might be nice option. 



Any cons to doing this? 



I see 10 or so LAN8720 boards at Amazon, all about the same price. Anything I should look/look-out for here or are these things pretty much all the same?





**"Dave Durant"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 05:03*

Hi Dave.

I just checked amz, the top bunch of results all look about right to me.  For reference they are about $5-10 on aliexpress.  Amazon is good in case you get a defective one. 



You can't directly communicate from LW to the board over ethernet.  But you can make your job in LW, save the gCode file, and upload it to the C3D via the smoothie web interface.  That's pretty much what you'd be getting by setting up ethernet: the ability to access the smoothie web interface, upload a file (it will save to the memory card in the board) and hit run job. 

**+Kelly S** I believe was doing this.  


---
**Dave Durant** *March 08, 2017 05:20*

Cool. Thanks. 



Mostly looking at amazon because they already know me.. I'll pay a few more $ to not have to enter my info into yet another site.. 


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 05:44*

First I'm hearing of that, awesome!


---
**Kelly S** *March 08, 2017 13:53*

That is how i currently have my workflow.  And really happy i came and seen this post, looking forward to great things.  If i get easier steps, i get the new round of beers.


---
*Imported from [Google+](https://plus.google.com/+DaveDurant/posts/hNBkpdMAKbv) &mdash; content and formatting may not be reliable*
