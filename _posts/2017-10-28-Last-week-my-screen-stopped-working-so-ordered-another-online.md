---
layout: post
title: "Last week my screen stopped working so ordered another online"
date: October 28, 2017 21:40
category: "General Discussion"
author: "tyler hallberg"
---
Last week my screen stopped working so ordered another online. Came in the other day and this is what pops on the screen now. You guys think it's a fault screen or a bad board? 

![images/78b0f5d5783f3a01840f19fec4561a34.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/78b0f5d5783f3a01840f19fec4561a34.jpeg)



**"tyler hallberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2017 21:42*

I get good quality screens, and it just happens at times, primarily due to the noise and power insufficiency of the stock k40 psu. Have you added a separate psu yet? 



Try plugging in a USB cable with k40 off to boot the board that way. 



The first goal is to see whether this screen will ever work... 


---
**tyler hallberg** *October 28, 2017 21:44*

Haha right as I posted this I read one post down and his screen turns on with USB. I tried and it works. I have never have this problem in the 3 screens I have had. I have been on external PSU for a long while now so not sure why its doing it with this one.


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2017 21:46*

Ah. Then it's just a matter of noise and stuff. I'll see if I can reproduce between k40 psu, external psu, and USB, inside the k40 and outside. 



So far, the noise inside the cabinet (aka close proximity or electrical connections to noisy k40 lpsu) is the only thing I have. 


---
**Alex Raguini** *October 28, 2017 23:30*

I had this problem.  Reload the files on your card after reformatting the card.  That solved my problem.


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2017 23:32*

Interesting.  Please do advise if that changes anything, Tyler. 


---
**Alex Raguini** *October 30, 2017 14:17*

I've just noticed something.  I'm getting this same behavior from time-to-time.  I found if I reseat the GLCD adapter, the problem goes away for a while.  I would give that a try as well.




---
**Bill Keeter** *October 31, 2017 05:49*

Yep, this happens to me if I move the display around when inserting a SD card into the C3D. I think the wires with C3D gets messed up. I just turn the K40 off. Push gently on the connection to the PCB and to the LCD. Turn power back on and i'm good.


---
**PowerVeloCity Channel** *December 11, 2017 01:30*

Just installed it in a k40 and getting this exact garbled screen. This happens only if I power it up from the PSU with the USB cable unplugged. If the USB is plugged in, it starts normally.


---
**Beto Mercado** *January 10, 2018 17:56*

**+Ray Kholodovsky** I am having this problem as well.  I tried using USB only, but to no avail.  I have a new mini board and new GLCD.  



After some tinkering, we noticed that it works consistently when we touch the solder points on the GLCD Adapter when turning it on.  After it boots, the finger may be removed and the LCD will continue working.  We get the same effect when touching the 6th pin from the right under the LCD and above the STOP button.  We soldered a capacitor from the 6th pin to the 1st pin on the right.  This seemed to slightly fix it, but it is not consistent.  It works about 1 in 5 times.  



I couldn't find a diagram for the GLCD, is that something that is available?

Any other ideas?  




---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 17:59*

Can you show me a picture of the capacitor you soldered - where? 


---
**Jake B** *February 08, 2018 21:55*

Hi there.  Just got my hardware today.  I also see this issue when powering up from the LPSU.  I get the boot-up tone but then the garbled screen.  I can detect a slight change in the tone during initial power up, maybe the power isn't stable from the LPSU for a few seconds?



If I power up from USB and then turn on the LPSU, I can remove the USB cable and things continue to work.



Currently, I'm using the "intermediate GLCD placement" as shown in the diagram until I can design a new panel, so the GLCD cables are going straight across the "noisy GLCD".



I'd like to get this to power up without a USB host eventually, but currently its not my first priority.


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 21:59*

That's the working theory Jake.  

I would encourage adding a separate 24v6a PSU to power the board in the near future. 


---
**Jake B** *February 08, 2018 23:10*

Thanks Ray.  I was also considering clamp-on ferrites for the GLCD cable, but I don't have any snap on ones for flat cables.



Probably not specifically C3D question, but is there a recommended 24v power supply most people use for this purpose?  I'll need it anyway for my eventual z-stage.



Thanks for this great product.  Once upon a time I was a contributor to the Sailfish firmware for Makerbot-style machines.  This is my first experience with Smoothie.  Might need upgrade my Makerbot 2X with a Remix someday.


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 23:18*

If it continues after installing the new PSU maybe you can just try wrapping the ribbon cables in aluminum foil - I don't know if that would help, but good to find out.  I can only think of it being an insufficient power or a cabinet noise thing. 



So far we have been recommending an "LED Style" power supply.  However, some people are uncomfortable with the AC wiring. 

I am currently vetting some samples of 24v5a DC Power Bricks and intend to make a power upgrade kit that will involve only  a few wires and no AC wiring.   It will probably be some weeks before I actually get inventory of that in though. 



Glad you like it. Feel free to offer to contribute, we're working on supporting 32-bit Marlin on the C3D boards right now :)




---
**Jake B** *February 13, 2018 11:30*

**+Ray Kholodovsky** thanks Ray, boot-up is fine with the dedicated 24v supply.  I pulled the 24v pin from the connector and use the screw terminals to power the C3D Mini. 



I have been working to install an interlock for the lid, so I have been frequently test firing the laser using the button on the digital power control included with the K40.  A few times now, a test fire has caused the C3D Mini to reset.  I'm not sure why.  Granted nothing is nicely mounted yet and the display is just draped across the top of the machine.  I can't get it to reoccur on demand.  Something I'm keeping an eye on.


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/XqGypTbV2DD) &mdash; content and formatting may not be reliable*
