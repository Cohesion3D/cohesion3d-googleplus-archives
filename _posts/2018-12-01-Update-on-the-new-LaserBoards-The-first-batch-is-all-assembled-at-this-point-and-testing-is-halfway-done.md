---
layout: post
title: "Update on the new LaserBoards: The first batch is all assembled at this point, and testing is halfway done"
date: December 01, 2018 01:40
category: "General Discussion"
author: "Cohesion3D"
---
Update on the new LaserBoards:

The first batch is all assembled at this point, and testing is halfway done. The remaining testing is going to take a few more days. Then they have to ship to us, and we'll ship them out to all of you that pre-ordered. We launched 1 week ago exactly, and said that the pre-orders would ship in 2 weeks. 

This would put us at next Friday, which is best case possible, but shipping is a mess this time of year and some (hopefully small) delays might occur getting them to us.. 

We thank everyone for their patience, and promise to keep you all updated as we know more over the course of next week. 

Thanks for supporting Cohesion3D!





**"Cohesion3D"**

---


---
*Imported from [Google+](https://plus.google.com/+Cohesion3d/posts/XNTsXvuVVGH) &mdash; content and formatting may not be reliable*
