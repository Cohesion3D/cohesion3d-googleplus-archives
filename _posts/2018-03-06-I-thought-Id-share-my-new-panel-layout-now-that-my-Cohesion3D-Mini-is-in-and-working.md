---
layout: post
title: "I thought I'd share my new panel layout now that my Cohesion3D Mini is in and working"
date: March 06, 2018 01:47
category: "K40 and other Lasers"
author: "Seon Rozenblum"
---
I thought I'd share my new panel layout now that my Cohesion3D Mini is in and working. Unfortunately due to my PowerLED board, any layout was too wide or too high for the existing panel cut, so I had to take an angle grinder to my K40 case and then laid the cut acrylic over the top to hide the "non-perfect" cuts...



The PowerLED board is mounted under the black acrylic and then had clear acrylic over the top with holes cut out for buttons, LED and the display.



AND - The last screw I put on, bottom left, I screwed too tight and cracked the acrylic!!!! 



Anyway, I am super happy with the result.... even though I know I am going to pull it all apart after I re-cut a new black acrylic face to get rid of that corner ;)



![images/7a4c720b03f7ddefb4aca4fd7df5dd15.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7a4c720b03f7ddefb4aca4fd7df5dd15.jpeg)



**"Seon Rozenblum"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 17:35*

I'm enjoying seeing your progress, keep it up!


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/h3ADzdpRmRs) &mdash; content and formatting may not be reliable*
