---
layout: post
title: "Hi I installed my new Cohesion 3D Mini in my K40 laser last night"
date: March 22, 2018 18:03
category: "C3D Mini Support"
author: "Tim Phillips"
---
Hi

I installed my new Cohesion 3D Mini in my K40 laser last night. All LEDs are lighting up correctly but, the laser head wont home correctly. It seems like it is trying to home but, the laser head wont stop. It just keeps going and  grinding when it gets to the edge of the laser bed. 

  Also I am confused as to where it is supposed to home. Is it left front or left rear? I have been told both. I have Lightburn software. I have included pics of how I installed. Do the potentiometers look oriented right.? The flat spot is facing x-y connectors.

Thanks



![images/284fee309bc9afb009cfdbfc42c7740b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/284fee309bc9afb009cfdbfc42c7740b.jpeg)
![images/528254b33e4c71ce968fc3f818a7d893.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/528254b33e4c71ce968fc3f818a7d893.jpeg)
![images/61992b80692bf792da31b68b8c7d4d0d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/61992b80692bf792da31b68b8c7d4d0d.jpeg)

**"Tim Phillips"**

---
---
**Ashley M. Kirchner [Norym]** *March 22, 2018 18:36*

It's supposed to home left-rear, since that is where the end stops are. Drivers are oriented correctly as well. So you need to check your end stops.


---
**Adam Scott** *March 23, 2018 03:12*

I had this issue (with the stock board) when the little metal piece that is meant to home into the end stop was bent slightly backwards.


---
**Tim Phillips** *March 23, 2018 18:12*

Installed my stock control board last night. No issues with end stops at all. Laser head homed first try and stopped. Don't know what problem might be.


---
**Tim Phillips** *March 23, 2018 19:08*

**+Ashley M. Kirchner [Norym]** End stops are apparently ok. I installed my old board and the laser homed and stopped.


---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2018 15:24*

When you home, the head needs to move to the rear left of the machine. Is it doing this? 


---
**Tim Phillips** *March 27, 2018 16:01*

**+Ray Kholodovsky** Yes it is with the stock board.


---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2018 16:03*

But is it with the C3D board? If it moved a different direction you need to flip that motor cable (with power off). I suspect you have the motors plugged in the wrong way. 


---
**Tim Phillips** *March 27, 2018 16:24*

**+Ray Kholodovsky** I had the C3D board installed. Flipped cables and it tried to home rear left the laser head just will not stop. It just keeps going and grinding the belts. Ashley told me to check the end stops so I put the old board in to see if they were working and they are.


---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2018 16:26*

Got it. 



Ok, I need a clear picture of the white cable plugged into the board. 



And you can test the endstops being read by sending a M119 command from the software and seeing the response. Should be 0 - open and 1 - pressed. 


---
**Tim Phillips** *March 27, 2018 16:46*

**+Ray Kholodovsky** Will have to do that tonight when I get home. Will post with results as soon as I can. Thanks Ray!


---
**Tim Phillips** *March 28, 2018 13:07*

**+Tim Phillips** Here is the photo of the white cable plugged into the board.

![images/3eab1c1f35ca03503be722e6d8c12f4e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3eab1c1f35ca03503be722e6d8c12f4e.jpeg)


---
**Tim Phillips** *March 28, 2018 13:08*

**+Tim Phillips** Here is a screenshot of the result when I sent M119 command from software.

![images/306c97e68f870bfd821edc01f29860c9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/306c97e68f870bfd821edc01f29860c9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2018 13:33*

Send M119 also with each switch pressed by hand, each one at a time. 


---
**Tim Phillips** *March 28, 2018 13:39*

**+Ray Kholodovsky** Will do.


---
**Tim Phillips** *March 29, 2018 14:41*

**+Tim Phillips** Here are results when I pressed each switch by hand and sent M119 command.

![images/294a45ecb2d7179afed83fa03d580923.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/294a45ecb2d7179afed83fa03d580923.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2018 14:43*

Your switches are being read properly. 



So in this case, it's possible that since the head actually touches the switches twice, you think it's crashing, but instead its just making a slow groaning movement.  People have been mistaken with this before.  Let it run for a bit. 


---
**Tim Phillips** *March 29, 2018 14:55*

**+Ray Kholodovsky** Ok


---
**Tim Phillips** *March 30, 2018 12:23*

**+Tim Phillips** Well I let it run for about 15-20 seconds and it never stopped. just kept trying to run out of the laser cabinet. It never hit both end stops. just hits rear one and keeps grinding not a slow groan a loud grind.


---
**Tim Phillips** *April 02, 2018 18:14*

**+Tim Phillips** Any other ideas about what might be wrong?


---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2018 02:08*

Try formatting the memory card and placing new firmware and config files.  The dropbox link with them is located at the bottom of the instruction manual. 



You can also try that with another memory card. 




---
**Tim Phillips** *April 03, 2018 14:12*

**+Ray Kholodovsky** I think ill try and place those files on a new card. I need to get an adapter for my laptop might as well try a new card. Thanks.


---
**Tim Phillips** *April 04, 2018 12:23*

Well I formatted a new card last night . The laser is homing correctly now it is just cutting my graphic upside down in the laser now. Just cant seem to get this right.


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2018 13:23*

Make sure you've got origin in LB set as the BOTTOM LEFT. 


---
**Tim Phillips** *April 04, 2018 13:59*

**+Ray Kholodovsky** Oh. OK so it homes rear left but origin is bottom left. Is that how it works?


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2018 14:47*

Yes.  


---
**Tim Phillips** *April 04, 2018 14:54*

OK. Maybe get this laser running soon and stop having to post all these questions. Thanks for your help hope it runs now!!


---
*Imported from [Google+](https://plus.google.com/108559807399850911893/posts/64GnMnXXfmc) &mdash; content and formatting may not be reliable*
