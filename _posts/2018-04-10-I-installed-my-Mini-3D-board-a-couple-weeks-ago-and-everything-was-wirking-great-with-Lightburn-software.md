---
layout: post
title: "I installed my Mini 3D board a couple weeks ago and everything was wirking great with Lightburn software"
date: April 10, 2018 15:29
category: "C3D Mini Support"
author: "Craig Bybee"
---
I installed my Mini 3D board a couple weeks ago and everything was wirking great with Lightburn software. Two days ago the sd card was corrupted and I cound not get any x or y movement of the laser. I put the nomsd firmware and k40 config file on a new sd card and reset the mini. Still no movement and windows keeps telling me i need to format the sd card everytime I boot up the board. 



I formatted the card Fat32 etc. Still asking for a format. All lights on board are correct. 



![images/01be235b5921d271cdd10750a14e24be.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/01be235b5921d271cdd10750a14e24be.jpeg)
![images/c6fc4cc7db42ab5441206fe31a6ccdc6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c6fc4cc7db42ab5441206fe31a6ccdc6.jpeg)

**"Craig Bybee"**

---
---
**Jim Fong** *April 10, 2018 15:50*

If the nomsd firmware actually flashed, windows would not detect any usb drive.  If you get the format message, standard smoothie is being used or some other error. 



Try flashing the original smoothie firmware and use the original config.txt.  Once you get the laser working with LightBurn then I can help with any problems flashing and setting up the nomsg version. 






---
**Craig Bybee** *April 10, 2018 16:01*

**+Jim Fong** Will do that right now


---
**Craig Bybee** *April 10, 2018 16:29*

I have formatted the sd card in windows 10, placed the files on it and attempted to flash the mini board.. The card does not show up in windows 10 as a formatted card wile in the mini. It works fine just inserted into a card reader on the computer. So can't get it to work on the board. New Card? is there a limit to the size of sd card, mine is 32g. Does it require a certain volume label?


---
**Jim Fong** *April 10, 2018 17:38*

**+Craig Bybee** I use a 16gb.  Not sure if there is a limit.  


---
**Craig Bybee** *April 10, 2018 18:19*

Anybody have an idea why the 3d mini won't recognize my SD card using Windows 10?


---
**Ray Kholodovsky (Cohesion3D)** *April 10, 2018 18:26*

I need to know what the lights on the board do. 


---
**Craig Bybee** *April 10, 2018 18:29*

**+Ray Kholodovsky** The lights show the mini boots up, red power, green light by the reset button and 4 green lights, solid, middle 2 blinking.


---
**Craig Bybee** *April 10, 2018 18:55*

The board will not reccognize the sd card, thus no config file


---
**Craig Bybee** *April 10, 2018 19:10*

Windows 10 says the file structure in not recognized and requests a format while in the board


---
**Ray Kholodovsky (Cohesion3D)** *April 10, 2018 19:38*

Try getting a new memory card, format FAT32, and put the files on it from your computer and then put it in the board. 


---
*Imported from [Google+](https://plus.google.com/101927015571558418192/posts/PfLa7rTndZ5) &mdash; content and formatting may not be reliable*
