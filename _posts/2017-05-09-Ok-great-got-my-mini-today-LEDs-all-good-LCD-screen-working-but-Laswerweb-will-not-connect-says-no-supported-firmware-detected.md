---
layout: post
title: "Ok great got my mini today, LEDs all good LCD screen working, but Laswerweb will not connect says no supported firmware detected"
date: May 09, 2017 00:23
category: "C3D Mini Support"
author: "Maker Fixes"
---
Ok great got my mini today, LEDs all good LCD screen working, but Laswerweb will not connect says no supported firmware detected. Where do I local this missing firmware, its windows 10





**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 00:25*

Is the MicroSD Card in the board? What are the LED's doing?  


---
**Maker Fixes** *May 09, 2017 00:35*

wow where you sitting there waiting for me pop in today?

 lol The card is in the board , the red LED is solid Green 1 and 4 are solid on and 2-3 are flashing 


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 00:44*

Ok, so what's going on is not actually firmware.  The firmware is on the board already.  - LEDs are blinking, this is good. Windows 10 does not need any drivers for the board. 



Do you see a drive show up in Computer? 

What USB cable are you using? Is it the one that came with the K40?  Try another known working high quality one. 

What can you find in Device Manager or in Devices and Printers?  Should be some entries for Smoothieboard, and there should be a COM Port with a #.  Make sure that's the same one that's select in LaserWeb. 


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/UcahgPhaFxE) &mdash; content and formatting may not be reliable*
