---
layout: post
title: "We'll be at the MRRF \"3D Printing Festival\" in Goshen, Indiana this weekend"
date: March 22, 2018 11:21
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We'll be at the MRRF "3D Printing Festival" in Goshen, Indiana this weekend. Stop by if you're in the area or traveled a long way to be there. :)  





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *March 22, 2018 12:18*

Have a great time Ray. Take lots of pictures please!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/BALnEmpWaQT) &mdash; content and formatting may not be reliable*
