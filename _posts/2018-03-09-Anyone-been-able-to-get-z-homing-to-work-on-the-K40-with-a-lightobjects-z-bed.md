---
layout: post
title: "Anyone been able to get z homing to work on the K40 with a lightobjects z bed?"
date: March 09, 2018 17:25
category: "K40 and other Lasers"
author: "Mike Hill"
---
Anyone been able to get z homing to work on the K40 with a lightobjects z bed?





**"Mike Hill"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2018 17:27*

Check with Brian TechBravo who I can't seem to tag 


---
**Mike Hill** *March 09, 2018 17:30*

I've got everything to work but the homing. I'd like to get it to hit the min endstop and back off to the focus distance at the z bed. Then I should be able to set material thickness and lower fairly precisely. But not until I get it to home...


---
**Joe Alexander** *March 10, 2018 03:10*

in the config file under homing order did you enable the z part? that should implement the home to z speed, pullback, etc I'd imagine.


---
**Mike Hill** *March 17, 2018 19:14*

Tried but never homed. Could change other settings and speed up, slow down but not enable homing. Will try again time permitting.


---
*Imported from [Google+](https://plus.google.com/+MikeHillDesign/posts/YTL8w8DFNVT) &mdash; content and formatting may not be reliable*
