---
layout: post
title: "Hi Ray I recently saw a post with you and a kossel mini..It looked like it was a work in progress, so idle curiosity leads me to ask are you constructing a kossel mini with one of your boards and how's it going?"
date: March 03, 2018 11:48
category: "3D Printers"
author: "Bryan Hepworth"
---
Hi Ray



I recently saw a post with you and a kossel mini..It looked like it was a work in progress, so idle curiosity leads me to ask are you constructing a kossel mini with one of your boards and how's it going?



Cheers

Bryan





**"Bryan Hepworth"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 14:25*

Yes, it was a diy scratch build of the Ultibots Mini Kossel VSlot design and it worked perfectly from the first job. It ran my Mini board. 

It needs an extruder and I'm currently using it to test some new stuff. 


---
**Bryan Hepworth** *March 03, 2018 16:09*

Hi Ray



Thanks so much for the reply, much appreciated. I think I'll be buying myself a mini board! I've had a Kossel Mini here for ages that hasn't really worked properly that I'm toying with the idea of updating with a different effector, extruder and feed mechanism. I think this news has just pushed me over the edge into making a decision.



Bryan


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 16:12*

Trinamic drivers man. Gold. 


---
**Family First** *October 29, 2018 20:12*

RAY, I just got my delta calibrated, but I am ata loss for where to plug in the extruder fan and print cooler 


---
*Imported from [Google+](https://plus.google.com/114125155844297036799/posts/GZwCNpXNp4C) &mdash; content and formatting may not be reliable*
