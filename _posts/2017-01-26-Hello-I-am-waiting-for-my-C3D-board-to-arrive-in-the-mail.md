---
layout: post
title: "Hello! I am waiting for my C3D board to arrive in the mail"
date: January 26, 2017 12:16
category: "General Discussion"
author: "Andreas Benestad"
---
Hello! I am waiting for my C3D board to arrive in the mail. In the meantime I have a question I haven't been able to figure out on my own:

I want to be able to put a pneumatic relee on my air assist hose to cut off the air whenever the laser is not firing.

Are there any available connections/outputs on the controller board that I could utilize for this purpose? (Anything between 12 and 240V)



Thanks!





**"Andreas Benestad"**

---
---
**Don Kleinschnitz Jr.** *January 26, 2017 14:11*

You could use one of the FET driver transistors and control it via M-codes with the "switch" command in the configuration file. The FET would  drive a 12-24VDC valve or is its a different voltage drive a relay that drives the valve.


---
**Bill Keeter** *January 26, 2017 15:38*

I wonder if you could have it turn on when M3 is called at the start of a job and them stopped at the laser off command, M5.


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 20:56*

Yep,  but if it's an inductive load you'll also need to put a diode across.

Use MOSFET2 since 1 will be taken with L wire for PWM control.  Remap laserfire or add another block switch in config and map the appropriate pin # and start and end gcode commands to it. 


---
**Andreas Benestad** *January 26, 2017 21:07*

Thanks guys. Being a complete noob, I have basically no idea how to do what you reccommended, but I will try and see if I can get some help. ;-)


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 21:12*

Well, you don't have the board yet and haven't used it. I'm just putting this here for later or for anyone else with the same question. The answer for you is yes- one of several ways we can do this. And you'll need a nice rectifier diode. 


---
**Don Kleinschnitz Jr.** *January 26, 2017 23:03*

**+Ray Kholodovsky** dont these FETs have internal snubber diodes?


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 23:06*

Yes.  Would I still rather have an external diode for a high inductive load?  Absolutely.


---
**Don Kleinschnitz Jr.** *January 26, 2017 23:13*

**+Ray Kholodovsky** "Chicken" :). By the way what kills FET's is the inductive kick and the shorter the leads the better. Internal diodes have short leads. :)


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 23:15*

Would the second external one hurt?


---
**Don Kleinschnitz Jr.** *January 26, 2017 23:19*

**+Ray Kholodovsky** just the time and annoyance to add it..

No it would not.

Oh and hooking it up backward lol


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/V9s6vLCGZm6) &mdash; content and formatting may not be reliable*
