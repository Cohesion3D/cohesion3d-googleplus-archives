---
layout: post
title: "Hi, new to the cohesion mini and using it per recommendation for my 3D printer and it left me with a couple questions since I normally use RAMPS 1.4/1.5"
date: April 17, 2018 00:28
category: "3D Printers"
author: "Bajicoy"
---
Hi, new to the cohesion mini and using it per recommendation for my 3D printer and it left me with a couple questions since I normally use RAMPS 1.4/1.5. There doesn't appear to be a wiring guide on the cohesion website like for RAMPS 1.4 and I got pretty confused navigating google+



What are FET's I find the FET Bed self explanatory but I have never heard of a FET-IN, is that power specifically for the print bed? and I am assuming FET2-EXT is for the extruder's heat cartridge but then where do I wire my 12V part cooling fans?



In terms of the stepper drivers, do I need to mount jumpers for A4988 or DRV8825's? If not, won't I destroy any TMC2130's I mount?





**"Bajicoy"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 01:49*

There's a thorough pinout diagram: [cohesion3d.freshdesk.com - Cohesion3D Mini Pinout Diagram : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)



Yes, separate power in for the 2 fets.

"Jumpers" are traces on the bottom of the board pre-connected already.  


---
**Bajicoy** *April 17, 2018 15:01*

Thanks for the response! Sorry to be a bother, the pin out didn't explain how the 10A blue connector was supposed to handle 16A for a bed and extruder unless I got the current limit wrong.  It also didn't mention where I should connect my fans. No controlled 12V outputs.



As for the jumpers,  how do I disconnect them if I ever need to for TMC2130's?



Sorry I have so many questions! Just in that exciting position I have my printer built but just need to wire things together without setting the house on fire haha.


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 15:07*

The blue connectors are specified to be rated for 16 amps max.  That's why there's a dedicated one for the 2 fets how we get a 12a bed + 3.33 amp hotend heater, for example. 



The fans are to be connected to FET3 and FET4 at the top right corner.  There is an option for VM - so it would be 12/24v/ whatever input voltage you are providing to the main power in.



You could cut jumpers on the bottom of the board or in the case of 2130's you would probably set up the headers as shown here in which case the "board's default state" doesn't matter: [hackaday.com - 3D Printering: Trinamic TMC2130 Stepper Motor Drivers](https://hackaday.com/2016/09/30/3d-printering-trinamic-tmc2130-stepper-motor-drivers-shifting-the-gears/)



Keep in mind that 2130's are not supported in Smoothie and would require the 32 bit Marlin currently in development.  


---
**Bajicoy** *April 17, 2018 15:23*

Thank you so much!!! It all makes sense now, I am very grateful for your explanation :D And yikes  about the 2130's  not being supported, does that mean their 256 micro stepping will not be used or smoothieware will flat out refuse to use them? I'm kind of hopping that only means that things like the skipped step sensing and end stop functionality won't be present


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 15:25*

It means you won't get the smart features like config and stallguard.  Just get 2100's (dumb version of 2130) or 2208's - people including myself use these all the time.  We have these available where you can add Trinamic drivers to the Mini when you buy. 


---
**Bajicoy** *April 17, 2018 16:14*

Interesting never knew that about the trinamic drivers, I'll look into some 2208's, my motors run with high current at 2A and 1.7A so hopefully they handle that fine


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 16:15*

Wouldn't count on it. That's a lot of current and a lot of heat to pump out of a stepstick. 


---
**Bajicoy** *April 17, 2018 17:08*

gotcha, glad to know, and yeah.. haha, SCARA printer, figured the more torque the better to not skip steps and they were the cheapest option at the time,  that's actually part of the reason why I wanted to figure out where I could mount some fans haha..


---
**Bajicoy** *April 17, 2018 17:10*

You wouldn't happen to know a Nicholas Seward by any chance? Creator of the RepRap Helios project, he recommended the board to me, really cool machine!


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 17:13*

Yes, I know Nick.  He uses the Mini with the 2208's for his Scara bot.  That design might have mounts designed and we also have a printable case for the Mini with a Pi and a 40mm fan. 


---
**Bajicoy** *April 17, 2018 22:44*

Awesome! By any chance do you have some links to what you're referring to?


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2018 23:11*

[drive.google.com - Mini WIth Pi3 Combo Enclosure - Google Drive](https://drive.google.com/open?id=0B1rU7sHY9d8qczM4dnhMTFZuZDg)


---
**Bajicoy** *April 18, 2018 14:28*

Thanks!


---
*Imported from [Google+](https://plus.google.com/108416859820371904210/posts/4Woko5gti14) &mdash; content and formatting may not be reliable*
