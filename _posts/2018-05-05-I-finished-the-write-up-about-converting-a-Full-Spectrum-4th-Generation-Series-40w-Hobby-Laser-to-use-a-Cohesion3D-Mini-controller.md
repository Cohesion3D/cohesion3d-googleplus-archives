---
layout: post
title: "I finished the write-up about converting a Full Spectrum 4th Generation Series 40w Hobby Laser to use a Cohesion3D Mini controller"
date: May 05, 2018 04:50
category: "C3D Mini Support"
author: "Pete Prodoehl"
---
I finished the write-up about converting a Full Spectrum 4th Generation Series 40w Hobby Laser to use a Cohesion3D Mini controller. I hope this is useful to anyone else looking to do the conversion.



I still have a few small issues, like the homing not working for the Y axis, but it's to the point where I can run jobs and cut and etch things, so that's some major progress! :) 





**"Pete Prodoehl"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 16:45*

Cool, nice write up.  



Homing - can you read the proper switch state using M119 command in console? 



To be clear the Mini doesn’t want 5v from the 4 pin blue wire thing. It makes its own. 


---
**Pete Prodoehl** *May 06, 2018 20:43*

**+Ray Kholodovsky** I'll check on the swiches with M119 this week. (I figured the Mini was getting 5v from USB so didn't need the blue wire from the power supply.)


---
**Ray Kholodovsky (Cohesion3D)** *May 06, 2018 20:44*

True, but it regulates its own 5v from the 24v as well. 


---
**Pete Prodoehl** *May 06, 2018 20:47*

Ahh, gotcha! That makes more sense.


---
**Jeff Ghielmetti** *September 15, 2018 22:39*

**+Pete Prodoehl** - Agreed.  outstanding write-up.  Followed the pics and instructions and had everything basically functional in an hour or so.  Now, I am testing and it seems that they Y axes homing isn't quite there yet.  You wouldn't happen to have the fix for that? 


---
**Pete Prodoehl** *September 16, 2018 15:59*

Hopefully the config file I sent you works.




---
**Jeff Ghielmetti** *September 16, 2018 21:36*

**+Pete Prodoehl** The good news is that the two config files have the same settings, the steppers move smoothly to "home" at the back left corner of the laser.  Also, the home switches are working and stop the movement and adjust as designed.  I am comfortable that the wiring, motors, switches, and communication are working as 

 designed. 



 The coordinates  at "home" are (0,200).  I would like to edit the config files so that the coordinates at "home" are (0,0) and match up with the design software coordinates.  ... thoughts? 


---
**Pete Prodoehl** *September 17, 2018 15:03*

**+Jeff Ghielmetti** That is probably a question for **+Ray Kholodovsky** or the LightBurn guys... My "home" is 0, 400 (top,left) and my 0,0 is bottom, left. I don't know a way to reverse things.






---
**Ray Kholodovsky (Cohesion3D)** *September 17, 2018 20:27*

It has to be that way.  GCode spec. 


---
**Jeff Ghielmetti** *September 17, 2018 21:39*

**+Ray Kholodovsky** Thx Ray.  Now, I know that the board is doing what it is supposed to do.  



I started running test jobs. I can move the gantry with LightBurn and it goes in the correct direction and can find home easily enough.  However, about 1 out of 5 times the gantry decides to go to the back/right corner instead of the back/left corner (home). 



The board is electronically isolated from ground, the connections seem to be nice and tight and I have connected everything with a good USB cable.  Are these the right things to check for random behavior?  What am I missing? 






---
**Jeff Ghielmetti** *September 18, 2018 05:53*

**+Jeff Ghielmetti** Figured it out.  The problem was a loose X axis stepper motor connector.  I replaced the connector, and things started working consistently.  So, if the board is installed, connected, and the motors are behaving erratically, check the stepper wiring and connection to the controller board.  



I am up and running.  The controller works great and my Full Spectrum Laser is back up and running.   



My thanks to Pete for the outstanding write-up and help, and Ray for the great product and support. 


---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2018 13:04*

I will add “loose wire” to my debugging checklist. (The list of crap that can go wrong that I vaguely maintain inside my brain.) :) 


---
*Imported from [Google+](https://plus.google.com/+PeteProdoehl/posts/ANK6mEPbBZa) &mdash; content and formatting may not be reliable*
