---
layout: post
title: "Update #1 on the boards. All 100 ReMix have been visually inspected at this point"
date: December 01, 2016 06:14
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Update #1 on the boards.  All 100 ReMix have been visually inspected at this point.  We are currently working through testing each one manually.  First few have been good.  We flash the firmware from the MicroSD Card and then do a full functional test to see that all the motors spin, endstops trigger, thermistors read, and mosfets can run heaters. As well as the GLCD and all other fun stuff. 



The MicroSD Cards and A4988 Motor Drivers (for those who added them) have been shipped via DHL and we expect to have them soon. 

Should have more updates after the weekend. 



And as if that wasn't enough, just placed the first order of 100 Mini's with the assembler.  

We said 4-6 week lead time and we launched for sale on Thanksgiving, just about 1 week ago.

Here's a rough idea of how this is going to go:

1 week already passed.

Assembler said 2-3 weeks lead time. Now,  they are first going to assemble 2 production samples for me, perform a functional test on their end, and then ship those boards to me.  I will put one in my K40 and the other is going to **+Joe Spanier** to test in his K40, he has the ribbon cable type and I have the all JST-style connectors type. That will take a little longer while we wait for DHL to deliver the boards to me, I test and send one to Joe, he tests, etc...

Once we are happy with all functionality, I give the go ahead and the rest of the batch gets assembled.

Some time later we get the batch, package up the bundles, and start shipping.

In short, you'll get it, it'll work, and you'll get it as soon as possible. 





![images/ace5932e62792632329d3592c9950c0f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ace5932e62792632329d3592c9950c0f.jpeg)
![images/94e8a46110e2f0727616dc0ab9605ed5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/94e8a46110e2f0727616dc0ab9605ed5.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Richard Vowles** *December 01, 2016 09:41*

Excellent. I also have a ribbon cable.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/jVpUDTYzvD5) &mdash; content and formatting may not be reliable*
