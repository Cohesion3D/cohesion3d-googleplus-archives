---
layout: post
title: "Is there a list of different laser cutting softwares that would work on a k40 with a Cohesion3D board installed?"
date: February 09, 2017 10:23
category: "Other Softwares"
author: "Andreas Benestad"
---
Is there a list of different laser cutting softwares that would work on a k40 with a Cohesion3D board installed? 

(Trying to understand Laserweb3, but have not succeeded so far...)





**"Andreas Benestad"**

---
---
**Ariel Yahni (UniKpty)** *February 09, 2017 12:28*

**+Andreas Benestad** whats troubles you in LaserWeb?


---
**Andreas Benestad** *February 09, 2017 12:36*

**+Ariel Yahni** well...It definitely boils down to my lack of knowledge and experience...

But I cannot get my laser to work. I think I have my C3D board correctly inserted, but after I was advised by Ray to move L to mosfet -2.5, I cannot even test fire the laser. I seem to be able to move the laser head around, but nothing more. 



Also, I seem to struggle to understand the work flow of Laserweb... I have read everything I could find but haven't found any good tutorials on how to use this software..? 


---
**Ariel Yahni (UniKpty)** *February 09, 2017 12:45*

**+Andreas Benestad** I suggest you take a brake, breathe and start over with the connection. Then go step by step to get your laser to fire. For Laserweb there is a new documentation site getting some love at [http://cncpro.co/](http://cncpro.co/) pelase if you havent go and read / see some videos on some of the workflows already added to that site. If you still feel lost, please tell me ill try to help


---
**Andreas Benestad** *February 09, 2017 13:03*

**+Ariel Yahni** cool. I just don't know what else I can do now. The board is installed according to instructions. And it does not work. So obviously something is not right, but I have no clue how to find the problem. 


---
**Ariel Yahni (UniKpty)** *February 09, 2017 13:10*

I will assume this is a K40 and it was previously working with the stock board correct?


---
**Andreas Benestad** *February 09, 2017 13:23*

**+Ariel Yahni** yes. Here is the story of my laser conversion thus far... [plus.google.com - Trying to install my C3d board. There were basically 4 wires I had to remove ...](https://plus.google.com/112581340387138156202/posts/eSNJCmN1aQZ)


---
**Ariel Yahni (UniKpty)** *February 09, 2017 13:59*

That's a very long thread :). Can you do a written diagram of what is connected to what? Nothing fancy just to have an overview of what's currently in there


---
**Stephane Buisson** *February 09, 2017 14:02*

just a lack of time, and I am late fitting my C3D mini, but I can't see any reason Visicut not working (work on original Smoothieboard x4 at the moment). I will update with C3D mini when I am done.




{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[youtube.com - Workflow Visicut - YouTube](https://www.youtube.com/watch?v=lbTTPkDEhOg) 


---
**Andreas Benestad** *February 09, 2017 14:05*

Does this help?

Stepper motor wires at the bottom (red,blue,yellow,white), stoppers on far right (green, light pink, dark pink), 3 red power wires, and the single red wire is L going to mosfet -2.5 as instructed by Ray.

![images/a2c5b310abefc60db2d66facd5f57bd9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a2c5b310abefc60db2d66facd5f57bd9.jpeg)


---
**Ariel Yahni (UniKpty)** *February 09, 2017 14:20*

A clear picture of your psu


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 14:59*

Did you change the laser pin in config from 2.4! to 2.5

How are you trying to test fire the laser? 


---
**Andreas Benestad** *February 09, 2017 16:19*

**+Ray Kholodovsky** I have tried to fire it through Laserweb with no response. But no, I haven't changed anything in config!! Wasn't aware of that, but I guess it makes a lot of sense if that is the issue here! Thanks, will try that. (My laser is at work, so I will not be able to check the effect of this until tomorrow)


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 18:08*

"I have tried to fire it through Laserweb with no response" - but more specifically, how? The test fire button? A bunch of people can't get that to work. Try running a cut command from the terminal text box at the bottom:

G1 X10 S0.5 F600


---
**Andreas Benestad** *February 09, 2017 19:14*

**+Ray Kholodovsky** I used the test fire button in the software. This button worked normally before moving L to mosfet -2.5.

I will try the cut command when I get to work (where my laser is) tomorrow. Thanks again.


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 19:19*

Ok. Try both then. Maybe you just need to change the pin in config. 


---
**Andreas Benestad** *February 09, 2017 19:29*

**+Ray Kholodovsky**​ the physical test button on the k40 machine still works. But no laser power on anything from laserweb. 


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 19:30*

Clarify?


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 19:31*

Oh. Different thread. Right. No wonder I got confused. Disregard...


---
**Andreas Benestad** *February 09, 2017 19:33*

I mean, when I push the test button on the actual machine (next to my lcd display on the k40) the laser tube fires. 

But when I click the test button in the laserweb software, nothing happens. It did test fire before moving L, but stopped working when I moved that wire. I have not been able to cut/engrave anything from Laserweb.


---
**Ariel Yahni (UniKpty)** *February 09, 2017 19:51*

The button on LW will not work if the pin is not correct and the proper setup is done in LW


---
**Tony Sobczak** *February 09, 2017 22:38*

Following 


---
**Andreas Benestad** *February 10, 2017 07:06*

So I changed the laser pin in config to 2.5. Still no response.

I also tried the cut command. No cutting happening... 


---
**Andreas Benestad** *February 10, 2017 07:37*

**+Ariel Yahni** so what is the "proper setup"? How am I supposed to know where to adjust settings in LW? Whenever I try to run a job the laser head just crashes into one of the sides. Probably just simple settings but I cannot seem to find out anything about this software...


---
**Andreas Benestad** *February 10, 2017 07:37*

And I still have no power from my laser tube..


---
**Andreas Benestad** *February 10, 2017 09:29*

Is Laserweb the only option for Cohesion3D? While struggling to get LW up and running I would like to have a look at other options also, preferably more plug and play...


---
**Ariel Yahni (UniKpty)** *February 10, 2017 11:27*

I assume you tried Ray suggestion from above? Can you place an image of how the smoothie config file in the laser section looks like?


---
**Andreas Benestad** *February 10, 2017 11:47*

**+Ariel Yahni** yes. Tried Ray's suggestions. Here is my config file. 

![images/de6f90093781368215db9ffc58647dce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/de6f90093781368215db9ffc58647dce.jpeg)


---
**Ariel Yahni (UniKpty)** *February 10, 2017 11:59*

The config looks fine to me. Can you post pictures of  LW settings tab


---
**Andreas Benestad** *February 10, 2017 12:02*

![images/ba7ac089c957a66b5fc0d32c992f4f0d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ba7ac089c957a66b5fc0d32c992f4f0d.jpeg)


---
**Andreas Benestad** *February 10, 2017 12:02*

![images/fe47a9075f9cb09b4afaef952459343f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fe47a9075f9cb09b4afaef952459343f.jpeg)


---
**Andreas Benestad** *February 10, 2017 12:02*

![images/801efb5b0039e0f63b9b7537df7ca713.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/801efb5b0039e0f63b9b7537df7ca713.jpeg)


---
**Andreas Benestad** *February 10, 2017 12:03*

Sorry for bad pictures..used my phone to take pictures of the screen.


---
**Ariel Yahni (UniKpty)** *February 10, 2017 12:04*

software side i cannot find any issues, your config seems to be good. 



Have you tried **+Stephane Buisson** suggestion above?



Mind that it does not look like a config issue


---
**Andreas Benestad** *February 10, 2017 12:10*

From what I understand I need to connect my board with an ethernet cable to use visicut. How can I get an ethernet expansion for my C3D..? **+Ray Kholodovsky**​

I would prefer not to have settle for Visicut...


---
**Ariel Yahni (UniKpty)** *February 10, 2017 12:13*

I get you. So we have:

Laser was working

Manual fire works

New board works moving steppers

Connection is good based on moving L to 2.5

LW config looks good to me


---
**Andreas Benestad** *February 10, 2017 12:17*

Well, more like this.

Laser was working with Moshi.

Installed C3D.

Manual test fire worked, no other firing.

Moved L to 2.5 = nothing works. Apart from the steppers which work as they should.

LW is probably not fully configured yet as the head crashes into the sides. 


---
**Ariel Yahni (UniKpty)** *February 10, 2017 12:40*

Under what's circumstances does the head crash? Is it when you are homing or because what you are sending could be bigger than the area? Maybe your 0,0 coordinates are off also. None of these are LW config issues


---
**Andreas Benestad** *February 10, 2017 12:43*

I haven't been able to find a pattern yet. Sometimes when homing, other times when trying to run a job (despite not having any power from laser).


---
**Ariel Yahni (UniKpty)** *February 10, 2017 12:45*

check your endstops cable and connection if they are mechanical, its very popular issue, not laserweb related


---
**Andreas Benestad** *February 10, 2017 12:55*

We have checked. They are fine. The problem is with the positioning. Homing seems to be consistent. The problem is when we start a job the head moved to seemingly random positions to cut, sometimes trying to get outside the work area. When we toggle in the jog area everything seems fine. But we have no chance of correctly positioning the image before running the gcode. Seems like a guess work...


---
**Ariel Yahni (UniKpty)** *February 10, 2017 13:12*

hmmm it does not make sense at least to me. If you are homing and have set 0,0 to that location and the path or graphics are within the bed size, theres no reason for it to go out of bounds, unless steps per mm are way off. Have you done a test for that? draw a line of about 100mm, load it to LW and mark the location the head is before running the job, after it finishes measure the distance traveled as it needs to be the same. It its not you need to fix that


---
**Ariel Yahni (UniKpty)** *February 10, 2017 13:37*

Well the same can be done by just jogging and above you said it was correct so


---
**Andy Shilling** *February 10, 2017 19:58*

I've just read through this post and one thing that hasn't been asked is have you set your image position to be at the top or bottom? If it's set to the top and you try to run a job it will crash even though your endstops show they are working in the M119 command. Worked this out myself :)


---
**Andreas Benestad** *February 10, 2017 20:33*

**+Andy Shilling** I have tried both. There might be something about it, though. 👍 

There seems to be very little connection between the "art board"/working area in LW and the actual place where the cutting will take place... When I move the image in Laserweb to a certain position, the laser head will start the job somewhere completely random. Just doesn't seem logical. There is something iffy about the origin or 0.0 or something that I don't understand... (probably the last option..)


---
**Andy Shilling** *February 10, 2017 21:01*

**+Andreas Benestad** can you post a picture of your config with the endstop details please.




---
**Andreas Benestad** *February 10, 2017 21:04*

I am sorry but I don't have it here right now..

But I am pretty sure the end stops are correct. When homing they perform their intended task.  


---
**Kris Sturgess** *February 10, 2017 21:48*

Did you do a power down and reset of your Cohesion 3D after you made the 2.5 change to the configuration? 


---
**Carl Fisher** *February 12, 2017 12:50*

Maybe I missed it, but looking at your gcode config, I don't see the M3/M5 commands to enable/disable the laser.



For test fire, can you try this



M3 G1 X10 S0.5 F600 M5




---
**Carl Fisher** *February 13, 2017 11:41*

**+Peter van der Walt** gotcha. Wasn't aware of that :)


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/euUbB67dXwF) &mdash; content and formatting may not be reliable*
