---
layout: post
title: "I am running a C3D with GRBL-lpc 4 axis firmware"
date: October 19, 2018 09:12
category: "C3D Mini Support"
author: "Richard Wills"
---
I am running a C3D with GRBL-lpc 4 axis firmware. I am trying to install a external stepper for the rotary.  I have hooked it up according to docs but can't get it to work.  it works with an A4988 driver in A axis slot, but when I hook up an external driver it doesn't move. Is there a special firmware needed for an external driver?





**"Richard Wills"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2018 18:45*

If you are wired the way our guide shows, then yes you will need different firmware build for grbl.  


---
**Richard Wills** *October 19, 2018 19:35*

**+Ray Kholodovsky** would you have a link where I can download? I am using Jim Fongs 4axis build grbl lpc right now


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2018 19:40*

That wasn't lost on me :) 

I'll have to dig it up later. 


---
**Richard Wills** *October 19, 2018 19:42*

Thanks Ray, no rush. 


---
**Ray Kholodovsky (Cohesion3D)** *November 06, 2018 20:20*

Here are our compiles for 4 axis.  The first one has open drain for Z and A set up.



[https://www.dropbox.com/s/4zgce8yuekduzpl/firmware_grbl-lpc_c3dmini_4axis_odZ_odA.bin?dl=0](https://www.dropbox.com/s/4zgce8yuekduzpl/firmware_grbl-lpc_c3dmini_4axis_odZ_odA.bin?dl=0)







[https://www.dropbox.com/s/vlxct86ldz6qi40/firmware_grbl-lpc_c3dmini_4axis_odA.bin?dl=0](https://www.dropbox.com/s/vlxct86ldz6qi40/firmware_grbl-lpc_c3dmini_4axis_odA.bin?dl=0)



[https://www.dropbox.com/s/eoagcfrgm03w83s/firmware_grbl-lpc_c3dmini_4axis_odX.bin?dl=0](https://www.dropbox.com/s/eoagcfrgm03w83s/firmware_grbl-lpc_c3dmini_4axis_odX.bin?dl=0)



[https://www.dropbox.com/s/qg6ewcdvclhf7he/firmware_grbl-lpc_c3dmini_4axis_odXYZA.bin?dl=0](https://www.dropbox.com/s/qg6ewcdvclhf7he/firmware_grbl-lpc_c3dmini_4axis_odXYZA.bin?dl=0)



[https://www.dropbox.com/s/88d5xn681m3sijj/firmware_grbl-lpc_c3dmini_4axis_odY.bin?dl=0](https://www.dropbox.com/s/88d5xn681m3sijj/firmware_grbl-lpc_c3dmini_4axis_odY.bin?dl=0)



[https://www.dropbox.com/s/zryx0lc74yjktpi/firmware_grbl-lpc_c3dmini_4axis_odZ.bin?dl=0](https://www.dropbox.com/s/zryx0lc74yjktpi/firmware_grbl-lpc_c3dmini_4axis_odZ.bin?dl=0)




---
**Richard Wills** *November 06, 2018 20:49*

Thanks Ray 


---
*Imported from [Google+](https://plus.google.com/115971902636333791915/posts/cwBiqBQdrtq) &mdash; content and formatting may not be reliable*
