---
layout: post
title: "When I start cutting with the C3D Mini, and the laser is on for more than 3 seconds, the entire board seems to reboot"
date: January 27, 2017 18:00
category: "General Discussion"
author: "Chuck Vanderwist"
---
When I start cutting with the C3D Mini, and the laser is on for more than 3 seconds, the entire board seems to reboot. Motion stops, laser turns off, and I have to reconnect LaserWEB to the C3D Mini, every single time.

I've even tried running the motors off of a separate PSU and just using the 5v "Laser fire" and PWM options on the Laser's PSU.

Any advice?

I just installed the C3DMini in my chinese k40 laser. It was not a 20min drop-in, but we got all functions working- mostly.... except for this.

Voltage spike? Ground is lifting on the Laser PSU? What the heck?





**"Chuck Vanderwist"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 18:02*

Need pictures of your setup please. The k40 electronics, the board and all wiring going to it. 




---
**Chuck Vanderwist** *January 27, 2017 18:24*

![images/620efb93ab3c4a9bda17bc518c9888d9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/620efb93ab3c4a9bda17bc518c9888d9.jpeg)


---
**Chuck Vanderwist** *January 27, 2017 18:25*

![images/ab374cadfa12f3bc5dafb66b2c538cfe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ab374cadfa12f3bc5dafb66b2c538cfe.jpeg)


---
**Chuck Vanderwist** *January 27, 2017 18:26*

I have a PDF diagram of how I've wired everything too...

[drive.google.com - cvande06-asdf-270117-1223-204.pdf - Google Drive](https://drive.google.com/file/d/0B5B5OLrL7xmBWDBuUld3NnRHVkE/view?usp=sharing)


---
**Chuck Vanderwist** *January 27, 2017 18:27*

In the diagram, I'm showing 2 different attempts, both produced the same symptoms. I tried using the K40 PWM connector, as advised, but then we suspected that pin might not be able to handle the current, hence our attempt to use the FET instead.



The first attempt with the FET worked for several cuts, but the next day when we powered up, it was back to reboot-upon-laser-fire-for-more-than-5s


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 18:36*

Please do not turn the laser back on or do anything until I have responded.  I'm not sure why you did all this other wiring but you're definitely putting voltages into places that they should not go. 


---
**Chuck Vanderwist** *January 27, 2017 18:41*

OK, holding for advice. Thank you


---
**Chuck Vanderwist** *January 27, 2017 21:16*

I was thinking of trying this out, based on what I've absorbed from other forum postings: [drive.google.com - L2.5only_with_POT.pdf - Google Drive](https://drive.google.com/open?id=0B5B5OLrL7xmBQTZzcElPa2FLSms) ... I haven't touched anything yet... worth implementing?


---
**Chuck Vanderwist** *January 27, 2017 21:17*

laser_module_pin                              2.5 


---
**Chuck Vanderwist** *January 27, 2017 22:56*

Update -  I got impatient and tried the L2.5only_with_POT.pdf wiring above. (-Bed 2.5 to L on PSU, no PWM pin connections. Changed laser_module_pin to 2.5 and verified that config is saved when board is rebooted)... and yes I checked the pot's resistance to make sure it was wired correctly. If I run a job with the pot all the way down, we can get motion through the whole thing without rebooting the board. If I run it at about 3mA or less (barely firing) I can USUALLY get through a full job with the laser on, but it still stops & reboots sometimes - especially at higher power or where the laser is running constantly (IE while cutting).

Could this be a noise problem? I'm not sure where else to look when troubleshooting.

Thanks again for your time and advice in solving this.


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 23:04*

Chuck you are running voltages into places they should not go. The fact that the board works at all right now... I will reply this evening when I sit down and handle "thorough" support. 


---
**Ray Kholodovsky (Cohesion3D)** *January 28, 2017 01:48*

Hi Chuck, 

Ok so your original wiring was an issue.  You had 5v and Sig backwards on the "K40 PWM Connector" so if you had 2.4 set as the PWM pin there would have been a short circuit every time the laser was pulsed.  BAD.



This latest diagram looks good though.  I'm presuming all the wiring has been put back to normal, and the pot has been hooked up. And to be absolutely clear,  you are only running those 3 wires, yes?  24v and Gnd to the Main Power In terminal, and L to 2.5 bed.  Any doubts please attach another pic of the current wiring state. 



Can you confirm for me:

-What exactly happens during this "stops and reboots" event.  You mentioned "motion stops, laser turns off, and I have to reconnect LaserWeb".  Can you monitor the green LEDs on the MIni and let me know what happens?  If you can catch a video of this in regard to what's going on on the board (LEDs) that would help a lot. 



-What is hooked up to the USB?  Are you driving with a computer, a Pi, or something else?  And if it's a computer - is it a laptop, what is the make and model #, and what OS? 



Behind the scenes thoughts:  I'm thinking some sort of spike or perhaps an undercurrent condition on the power supply - they are just barely powerful enough to run the tube and the 2 motors.  This is why I am asking about the computer and what the LEDs on the board are doing, precisely. 


---
**Chuck Vanderwist** *February 03, 2017 00:01*

**+Ray Kholodovsky**:



1. "..only running those 3 wires?" 

Yes: 24v, Gnd, L, connected as you have described above



2. "What exactly happens during 'stops and reboots'...?" This is where it gets weird! See the long story below



3. What is hooked to USB? (Pi 2 with LaserWeb on Raspbian Jessie). Initially with a 2.2a USB power supply and later with a 2a



RE:Weird - I believe I have discovered, completely by accident, that the cause of the "stops and reboots" behavior may be (scattered) OPTICAL radiation reaching the board. Is this possible?

In short, my K40 electronics were not shielded from the cutting area (I bought it second-hand, didn't know this metal divider existed, I <i>always</i> operate with all covers closed)

In order to catch a video, wanted to leave the electronics lid up without risking my hand, so I put in a makeshift divider (thick black terrycloth towel). Once I did this, everything worked perfectly. I could cut raster at full power for the full 300x200 area with no glitches.

I thought that my only other variable (upgrading the Pi's power supply) is what fixed it. 

Removed divider, closed lid... problem comes back.

Tested my theory with and without divider, with and without electronics lid open... I'm convinced that exposure to the actual laser light has something to do with my symptoms. 



I'm engineering a metal divider now and doing a laserweb deep-dive as soon as it's installed, so I'll let you know if anything else goes weird on me. :)



I learned a lot from my initial forays into over-engineering the wiring. I also learned that a vital part of my cutter enclosure was missing, and corrected a potential safety issue.



I am pleased with this product and excited to take full advantage of the beautiful quality it has enabled so far.

Thank you for your patience and assistance. I'll definitely recommend your products for all of our motion control needs as we get this mini-makerspace off the ground.

Best Regards,

Chuck








---
*Imported from [Google+](https://plus.google.com/100962229977611108619/posts/DZkK2gxmrNU) &mdash; content and formatting may not be reliable*
