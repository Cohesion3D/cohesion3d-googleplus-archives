---
layout: post
title: "Just installed the C3D mini - and I am having some troubles"
date: November 10, 2017 17:30
category: "C3D Mini Support"
author: "Arjen Jonkhart"
---
Just installed the C3D mini - and I am having some troubles. When I connected it to the computer via USB cable it gives me "Unknown USB Device (Device Descriptor Request Failed)". My OS is Win 10, standard $400 Chinese K40 ribbon version from eBay, installed while properly grounded. Things I tried:



- Three different USB cables

- Different USB ports (2 and 3)

- Power on, or off before connection

- Smoothie USB driver,  v1.0 and v1.1

- With SD card, without

- Deleting in Device Manager, rediscover

- Reboot computer



Sequence of green lights:

- single flash of 3v3, L1, L2, L3, L4

- 3v3 stays lid, LED (to the right) turns on

-  L1 turns on and L2, L3, L4 flicker

- 3v3, L1, and LED stay lid, Windows 10 USB error



Now, every 1 in 10 times it will recognize the board as "Smoothie board, USB com3", but then after about 45 seconds, it will go back to Unknown.



Please assist. Thanks!



Arjen





**"Arjen Jonkhart"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 18:40*

Windows 10 specifically does not need the Smoothie drivers installed, and installing them can cause problems.  Try uninstalling, rebooting the computer, and see if that fixes the issue. 


---
**Arjen Jonkhart** *November 10, 2017 18:54*

I should have mentioned that I tried it without, both versions, and without again. And yes, deleted from systems folder and rebooted every time.


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 19:08*

Hmmm... 

At the 1 minute mark here is shown what normal "braining" of the board should look like.  
{% include youtubePlayer.html id="KsNRvsvl8vc" %}
[youtube.com - Cohesion3D Mini First Boot](https://youtu.be/KsNRvsvl8vc?t=1m)



L1 and L4 solid, L2 and L3 blinking.  When you plug in USB, L2 and L3 will blink at a slower rate while the board agrees with the computer, then resume the faster blink. 



L4 should be solid at all times after the power up initial flash.  Please try removing and reinserting your MicroSD Card to make sure it is all the way in. 




---
**Arjen Jonkhart** *November 10, 2017 20:10*

Please see the light's boot sequence:



Without SD card: [https://tinyurl.com/y87nbaby](https://tinyurl.com/y87nbaby)



With SD card: [https://tinyurl.com/ycxn2g5d](https://tinyurl.com/ycxn2g5d)



View from Device Manager: [https://tinyurl.com/ydcwbjsk](https://tinyurl.com/ydcwbjsk)

[icloud.com - iCloud - Loading …](https://tinyurl.com/y87nbaby)


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 20:21*

The With SD Card video, is that what the LEDs do every time? 


---
**Arjen Jonkhart** *November 10, 2017 20:22*

Correct


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 20:23*

And do L2 and L3 eventually start blinking again as I described? 


---
**Arjen Jonkhart** *November 10, 2017 20:35*

They do not - at least not within 10 minutes. Which makes sense I guess since Windows has given up on communication with it


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 20:42*

Ok, thank you for your patience in troubleshooting.



Do you have another computer you can try connecting to?  I would be curious if the LEDs behave differently on another PC. 



Then, I would like to format this MicroSD Card FAT32 and put the fresh firmware and config files (from my dropbox link at the bottom of the k40 instructions) onto it again to see if that changes anything.  



Third thing would be to try a different MicroSD card. 


---
**Arjen Jonkhart** *November 10, 2017 20:59*

I think that the SD card was bad. It didn't even want to slow format. 



But, I reformatted the SD card from my camera, downloaded your files, and that seemed to have fixed the problem.



The board is recognized, and it sticks this time - and L1&L4 are solid while L2&L3 are flickering.



So - I think that I'll try cutting something now :)


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 21:04*

Good to hear it is working.  1% of the time weird things like this happen.  Please keep me posted. 


---
*Imported from [Google+](https://plus.google.com/109364066707746072621/posts/SyHruNTUT4u) &mdash; content and formatting may not be reliable*
