---
layout: post
title: "Is it possible to have motors/hoten/heated bed run off 24V and the fan mosfets run off 5V or 12V on a C3D Mini?"
date: May 22, 2018 08:46
category: "C3D Mini Support"
author: "Johan Jakobsson"
---
Is it possible to have motors/hoten/heated bed run off 24V and the fan mosfets run off 5V or 12V on a C3D Mini?





**"Johan Jakobsson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 22, 2018 14:40*

5v - yes - the 2 fan connectors have “motor input voltage” and 5v as available options. If you want 12v then you need to get a separate buck converter for 24v —> 12v, this will power the + of the fan and the - of the fan will go to the onboard fan fets


---
**Johan Jakobsson** *May 22, 2018 14:53*

I was expecting i'd need to get either a downconverter or a separate 12V powersupply so that's ok.

On what pin do i supply the power to the fan mosfets. On the VIN pin next to P2.4/P2.6?

Edit: Now i understood your reply correctly.

The fan mosfets either get their power from the Main Power In or 5V from onboard? And i switch between the 2 using a jumper?




---
**Ray Kholodovsky (Cohesion3D)** *May 22, 2018 15:43*

No. Please look at the pinout diagram (follow Cohesion3D.com/start ) 



There is a 3 pin fan header and you install the fan connector (2 pins) either on one side or the other side. 



You do not supply any power nor do you use a jumper. [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Johan Jakobsson** *May 22, 2018 22:42*

**+Ray Kholodovsky** Right, thanks. If I understand correctly you get either VIN voltage or 5V from the mosfet depending on which pins you connect the fan to as per the diagram. 

I'm used to smoothieboard where you have a separate power input (VIN) for the mosfets and you can switch between that and the main VIN using jumpers so I (wrongly) assumed it was the same on the C3D.


---
**Ray Kholodovsky (Cohesion3D)** *May 23, 2018 01:45*

Sounds right.  The fet switches the center pin, that's your - and you plug in the + depending on which voltage you want. 



On the Mini only bed and hotend heater have separate power input. 


---
*Imported from [Google+](https://plus.google.com/+johanjacobsson/posts/iLyHtWGC5kf) &mdash; content and formatting may not be reliable*
