---
layout: post
title: "What is the benefit of using the Cohesion3D board?"
date: April 01, 2017 22:03
category: "C3D Mini Support"
author: "bbowley2"
---
What is the benefit of using the Cohesion3D board?





**"bbowley2"**

---
---
**Eric Lien** *April 01, 2017 23:16*

It depends what you want it compared to. Compared to ramps... It is 32bit and runs smoothieware which is easy to configure. Compared to the original Smoothieboard... the price and feature sets are standouts on cohesion3d and Ray's support and response time is top notch. For integration in the k40 laser... It is a drop in controller replacement for all k40 configurations and allows the use of laserweb which is a killer cam and control solution for lasers.



I guess if you can be more specific in the question, I can be more specific in my answer.


---
**Ray Kholodovsky (Cohesion3D)** *April 02, 2017 23:56*

Assuming we're talking about the laser upgrade since that's been the topic of our previous conversations:

1. Gets you away from the stock software so you can use LaserWeb. Much more powerful and open source. 

2. Full laser power control. Let's you run jobs with multiple power levels and gets you full high quality grayscale engravings. 


---
**bbowley2** *April 03, 2017 18:05*

Thanks.  I get it.




---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/cjL1cqVmfew) &mdash; content and formatting may not be reliable*
