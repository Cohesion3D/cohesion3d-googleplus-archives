---
layout: post
title: "I just picked up a new \"upgraded\" red K40 and a Cohesion3D board"
date: April 06, 2017 20:01
category: "K40 and other Lasers"
author: "Steven Kalmar"
---
I just picked up a new "upgraded" red K40 and a Cohesion3D board.  I'm looking at setting the stepper driver current and was wondering what these are usually set to?  From the M2 board, it looks like it's set to 0.6A and 0.4A on the X and Y.  I saw no markings on the steppers themselves.  I'll be running a dedicated power supply, so overall current doesn't matter.  I'm building a motorized table, so I'll need more power anyway.



This K40 came with the digital board.  Is there anything wrong with keeping the digital board or is a good pot better to use with this controller?





**"Steven Kalmar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 20:06*

I don't know the exact currents, I go by the position of the flat of the pot on the A4988.  It comes with the flat facing out, this works for a bunch of people, if you go to higher speeds eventually that won't be enough and you can turn it 45 or 90 degrees clockwise to increase the current. 



Digital board seems to work, we've had an increase in the number of users with that. You're not replacing it (ie you are keeping it wired and in play) so set aside the white wire that comes with the kit as that won't be used.  


---
**Steven Kalmar** *April 06, 2017 23:43*

The vref voltage was 0.411V and 0.438V, which comes out to 0.256A and 0.273A.  The highest I can turn it up to is 0.83V, which is 0.518A.  This is assuming 0.2 ohm for the sense resistors.  Do you know what resistor they use?  I guess I can verify it by measuring the current on one of the windings.  


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 23:50*

Just pulled out a few drivers, the resistors on them read R10 so that would be 0.1 ohm.   Might want to verify against the ones you have though. 


---
**Steven Kalmar** *April 07, 2017 00:06*

My Fluke initially reads 0.2, but after a few seconds it bounces between 0.2 and 0.1 on a couple of the drivers.  I think I'll have to just measure the current directly.  


---
*Imported from [Google+](https://plus.google.com/106762627155275597313/posts/4bkb2ipdLNs) &mdash; content and formatting may not be reliable*
