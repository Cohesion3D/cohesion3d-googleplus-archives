---
layout: post
title: "Hi, I am completely new to the K40 laser scene"
date: August 22, 2018 16:23
category: "C3D Mini Support"
author: "Mohammed Shafiq"
---
Hi, 

I am completely new to the K40 laser scene. I purchased a k40 laser two months ago and have been using it with Coral Draw v 12 and Coral Laser. 

I have read on my many forums that the way to go with these lasers is to upgrade it with the Cohesion 3D mini board. Forgive me for my ignorance but I have very limited knowledge and experience. 

I do wish to improve the performance of the laser and produce better quality engraving and cuts. 

What i do not understand is what this improved board does? How it runs with the current motors? Do I have to change the current motors or simply swap the cables from the Nano M2 board and it starts working. What about the current panel I have on the K40, which has the big red stop button and laser tube temperature panel and other switches to control the LED lights in the box and the test firing switch of the laser. Will all these still perform as normal or will they become redundant once the cohesion 3D board is swapped? 

My main purpose of using coreldraw is to produce cake toppers, simple text files with very little graphics. Will i be able to do this using the software that you supply at an additional cost? Is that software all that I need to draw and print from the laser or will i still need the corellaser software? 

I just need someone to explain what these different elements do and what they are for. If the cohesion 3D makes the laser perform better than I will buy the card and the Lightburn software license too. 

Many thanks in advance for your patience.





**"Mohammed Shafiq"**

---
---
**Tech Bravo (Tech BravoTN)** *August 22, 2018 16:42*

the cohesion3d mini upgrade bundle is a drop in replacement for the factory m2 nano controller found commonly in k40 laser engravers. it is plug and play and can be installed in around 15 minutes with no modifications to your laser. it offers pwm laser power control and allows the use of lightburn software. this pwm laser power control means that the power of the laser can be adjusted on the fly by the software and controller for true 3d engraving capability. that alone makes the c3d worth it!




---
*Imported from [Google+](https://plus.google.com/111014047014412062885/posts/Gnc9Fr9yU9A) &mdash; content and formatting may not be reliable*
