---
layout: post
title: "i have Cohesion3D Mini and a blue PWM laser, the laser has 3 output: +12, GND, and PWM, has begun to test, has connected PWM laser to pin 2.4 PWM Output, the laser works always!!"
date: April 02, 2018 14:23
category: "C3D Mini Support"
author: "Alex"
---
i have Cohesion3D Mini and a blue PWM laser, the laser has 3 output: +12, GND, and PWM, has begun to test, has connected PWM laser to pin 2.4 PWM Output, the laser works always!!

i'm take multimetr and check output 2.4, always ~5V

the question - on pin 2.4 always ~5V it's a normally?



![images/88d33e4b0f99584527ed379f6ae70bae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/88d33e4b0f99584527ed379f6ae70bae.jpeg)



**"Alex"**

---
---
**Alex Hayden** *April 02, 2018 17:24*

Where did you get that driver?


---
**Alex** *April 02, 2018 17:33*

**+Alex Hayden** this driver installed on EleksMaker laser


---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2018 02:00*

The 2.4 pin has a pull up resistor to 5v.   This inverts the logic.  So you have to set it to 2.4! in the config file. 



This will probably still cause the laser to turn on for a few seconds when the board boots. 



So you can connect the +12 and Gnd to one of the larger mosfets like 2.5 or 2.7 and configure it with the M3 / M5 switch - like a safety - enable laser at start of job, disable at end of job.  But we can talk about this later if you have the problem I described.  



2.4! is the answer to your question. 


---
*Imported from [Google+](https://plus.google.com/106697873351656319086/posts/XTQYtenNPZS) &mdash; content and formatting may not be reliable*
