---
layout: post
title: "Testing complete. Now to gut it and install the cohesiond3d mini"
date: June 27, 2017 11:50
category: "K40 and other Lasers"
author: "Dushyant Ahuja"
---
Testing complete. Now to gut it and install the cohesiond3d mini. Wish me luck. 



![images/9ddcd3f39bb8202a48744eeb755d0198.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9ddcd3f39bb8202a48744eeb755d0198.jpeg)



**"Dushyant Ahuja"**

---
---
**Ashley M. Kirchner [Norym]** *June 27, 2017 17:41*

No need to gut the machine, just swap boards, done.


---
**Dushyant Ahuja** *June 27, 2017 18:58*

**+Ashley M. Kirchner** of course. That's what I meant. Already have the mini from Ray


---
**Dushyant Ahuja** *June 29, 2017 00:57*

I'm calling this a success. Thanks **+Ray Kholodovsky** - you made this transformation extremely easy. ![images/7dcfd65c5b343ee4c574a21d8c7277e8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7dcfd65c5b343ee4c574a21d8c7277e8.jpeg)


---
**Ashley M. Kirchner [Norym]** *June 29, 2017 01:19*

That is definitely a great result, congrats. Now you can spend some time dialing it in more. As each machine is slightly different, settings for each must be dialed in by each person. But this gives you a great starting point to work from.


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/1MkgCNesiuu) &mdash; content and formatting may not be reliable*
