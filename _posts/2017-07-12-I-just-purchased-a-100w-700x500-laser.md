---
layout: post
title: "I just purchased a 100w 700x500 laser"
date: July 12, 2017 20:11
category: "K40 and other Lasers"
author: "John Hunter"
---
I just purchased a 100w 700x500 laser.  I also have purchased a cohesion3d, as i loved it on the k40.  I'm a bit lost in wiring it.  I am currently going through the "wiring a z table and rotary to a k40" document as this look like it might be a similar set up.  Thanks again Ray for your support!





![images/835e3d1ca7ace5abfc677e34a83db34d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/835e3d1ca7ace5abfc677e34a83db34d.jpeg)
![images/9dcd57f82d2e1b888e656712f23f561d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9dcd57f82d2e1b888e656712f23f561d.jpeg)
![images/9e0a123e25a90a179ee9404e36a5be58.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9e0a123e25a90a179ee9404e36a5be58.jpeg)
![images/94adac3564aa5eae43358050ff78a63a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/94adac3564aa5eae43358050ff78a63a.jpeg)

**"John Hunter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2017 20:13*

I need a super clear shot of the black box in pic 3 so I can read the wiring on it. 

Same for the white box in pic 4 couldn't hurt. 


---
**John Hunter** *July 12, 2017 20:26*

![images/496bba545f1fd6ecdd5e04a5a603d3c6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/496bba545f1fd6ecdd5e04a5a603d3c6.jpeg)


---
**John Hunter** *July 12, 2017 20:27*

![images/14e873f465d2f996bb5222ea538223bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/14e873f465d2f996bb5222ea538223bd.jpeg)


---
**Sebastian Szafran** *July 12, 2017 20:32*

The white box is the controller and with current (factory) wiring it is compatible with 'RD Works' software. Check this channel for lots of educational videos: [youtube.com - SarbarMultimedia](https://www.youtube.com/user/SarbarMultimedia)


---
**John Hunter** *July 12, 2017 20:54*

That sounds like a better choice of software than the k40.   My main deal though, is i've already been using laserweb and i quite like it, so i'd like the option of running it on this machine too.




---
**John Hunter** *July 12, 2017 20:56*

It looks like my controllers are similar to the above mentioned document, but the pinout is in a bit different area.


---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2017 21:38*

Cool.  Glad I asked for the white box as well. 



The black box: external stepper driver matches up with our diagram. 



As per it, the + need to get combined and go to the 5v aux header on Mini, and the - will be going to the external stepper adapter relevant pins.  Interesting that enable isn't wired.  If you keep it that way I guess your motors are always going to be energized/ locked. 



You're also going to need to add !o to the end of each step/dir/en pin definition in config.txt for the external drivers wired that way. 



Let's start there, and just power the Mini over USB, and see what we can make work. 


---
**John Hunter** *July 13, 2017 00:45*

Does this look right?   I assume the 3 original wires i removed from the  plug should just be taped up. Is that right?



![images/bbdcfd464440e9b5a7487d0a46658e07.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bbdcfd464440e9b5a7487d0a46658e07.jpeg)


---
**Sebastian Szafran** *July 13, 2017 04:44*

Consider an ammeter add-on to see and control the current flowing through the laser tube. Cut the negative (black) wire coming from the laser PSU to the laser tube and connect two ends of this wire, you have just cut to the ammeter.



Remember to disconnect the machine from any power supply and give it some time to fully discharge. You don't want to get hit by 25000 volts, which might be dangerous to your health and life.



Cutting the hole with Dremel takes just few moments and you are all done.

![images/c862ced8165351322d6c2ada2dd9e297.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c862ced8165351322d6c2ada2dd9e297.jpeg)


---
**John Hunter** *July 13, 2017 10:43*

I was wondering how to add one,  thank you.  Will a 30ma ammeter meter, like the one below, be what i need for my laser?

   

[amazon.com - Analog Panel Ammeter Gauge DC 0 - 30mA Measuring Range 44C2 - - Amazon.com](https://www.amazon.com/Analog-Panel-Ammeter-Gauge-Measuring/dp/B00NQ8CW4A/ref=sr_1_1?ie=UTF8&qid=1499941711&sr=8-1&keywords=ammeter+30ma)


---
**Sebastian Szafran** *July 13, 2017 11:08*

Yes, this one should be fine. 


---
**Steve Clark** *July 13, 2017 17:08*

OK I will ask the question for the few of us that do not understand the advantage or reason for adding  the ammeter here. What information does this give you when comparing it to the ammeter already installed on the K40? Or is this only being suggested for the OP's new machine?


---
**Sebastian Szafran** *July 13, 2017 21:49*

Well, it allows you to see (and this way control) the current level to extend laser tube lifetime by avoiding values beyond tube's manufacturer recommendation (and maximum).


---
**John Hunter** *July 13, 2017 22:42*

i added "!o" to the config file as stated above.I hooked up the board to the USB on my laptop and the board came alive.  It seems to be connecting just fine, but the display, displays garbage.   The top 2 lights solid green.  The next two are flashing green, the bottom solid green and the light by the sd card is solid green.  I issued an M119 command (screen shot will be posted.)  Gcode file sends commands, but the machine doesn't do anything.  The job shows complete.

![images/0d5f282ae95e106b59ce7fee08c132de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d5f282ae95e106b59ce7fee08c132de.jpeg)


---
**John Hunter** *July 13, 2017 22:44*

I did mess with the contrast pot and it didn't help  (more pics and screenshots to follow)



![images/08520e206fc66f610eb19a4416b41acb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/08520e206fc66f610eb19a4416b41acb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 13, 2017 22:45*

Yeah... the screen will do that if the USB port can't provide enough power. Try plugging in the USB a few more times, if the issue persists then we'll hook up 24v to the board. 


---
**John Hunter** *July 13, 2017 22:46*

![images/44d8b72658522c89865057b3dd6a6fb8.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/44d8b72658522c89865057b3dd6a6fb8.png)


---
**John Hunter** *July 13, 2017 22:47*

![images/db7714a8fb026e160746dd73441a4b46.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db7714a8fb026e160746dd73441a4b46.png)


---
**John Hunter** *July 13, 2017 22:47*

![images/62b9c10ff035b8f8e4d2db6e858a90bf.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/62b9c10ff035b8f8e4d2db6e858a90bf.png)


---
**John Hunter** *July 14, 2017 20:34*

Okay, i used a shorter usb cable and now the screen issue is fixed.   I can scroll down now and go to jog and nothing happens with the x and or y axis




---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2017 21:14*

Maybe enable is flipped or isn't working.... 



Try removing the ! from the enable pin for alpha and beta in config.txt


---
**John Hunter** *July 14, 2017 22:07*

I tried that. No change




---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2017 22:09*

Can I see the wiring of the board as it is now? 


---
**John Hunter** *July 15, 2017 12:38*

these two are current pictures of the board and how they are wired compared to the axis boxes.  Do i need to reland the three wires i removed when i wired in the cohesion?  Right now they are taped (individually)

![images/4c6371ef03087be43cd71536568f56eb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4c6371ef03087be43cd71536568f56eb.jpeg)


---
**John Hunter** *July 15, 2017 12:39*





![images/6dbbcea421a1cb190f14b934bd4331d0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6dbbcea421a1cb190f14b934bd4331d0.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 15, 2017 14:20*

The red wires... the one that's feeding it from the white box, let's remove that and run a wire to the 5v on the Mini like in the diagram. 


---
**John Hunter** *July 17, 2017 12:55*

Yes!  That did it.  Thank you!  Both motors are moving now.  I even simulated a cut with the laser off and it worked just fine.  Now, on to the next part.  How do i wire the tube in for control from laserweb.




---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 13:02*

See CN5 on the white box? Trace those wires please, I'm thinking some of them are going to go to the laser psu. Need to know what goes to where. 


---
**John Hunter** *July 17, 2017 16:55*

cn5

![images/b362f057217004de821b80bc1f133a0d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b362f057217004de821b80bc1f133a0d.jpeg)


---
**John Hunter** *July 17, 2017 16:56*

Laser PSU

![images/d5f2d746a89bc496dc57ebd7c13259c7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d5f2d746a89bc496dc57ebd7c13259c7.jpeg)


---
**John Hunter** *July 17, 2017 16:56*

main psu

![images/28f7cb3ba863472952e370599dafa355.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/28f7cb3ba863472952e370599dafa355.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 17:01*

Can I get a clearer view of the laser psu so I can see the pinout legend? Should be above the yellow and green leds. 


---
**John Hunter** *July 17, 2017 17:50*

Here ya go

![images/411962bee25692c04c63f5a40757f83c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/411962bee25692c04c63f5a40757f83c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 19:06*

It looks as I suspected:

L on white box goes to L on the PSU

"LPWM1" goes to IN on the PSU.  



The names are recognizable from the k40, however the k40 has the potentiometer going to IN. 



We can do several combinations here, tying IN to 5v and pulsing L, but this might yield poorer raster performance, we can go 2 wire control with L on and send the pulsing to IN like we did on the first C3D laser upgrades, could potentially add a pot back in... 



I will ponder this for a bit.  **+Don Kleinschnitz** any thoughts?   Upgrading a 100w with a ruida here, need to decide which way to set up the laser fire and pwm to the LPSU.  Typical L and IN, but no pot. 


---
**John Hunter** *July 17, 2017 19:08*

I would love to add potentiometer control, so if that helps things, that would be great


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2017 19:18*

I was hoping it could be avoided.  Good to know you're open to that option.  Let's see if we can get good enough contrast without it first.  But Don does have some suggestions for what pot to get on his blog so maybe worth confirming that and getting the part on order soon enough.  



I'm thinking we keep the wiring of the LPSU similar to how we've been doing the K40 at present. 



We need a common ground for this, so the blue wire will go to the Gnd of the Main power in screw terminal of the Mini. 



Then let's pull L which is the brown wire and connect that to the 2.5- screw terminal on the Mini, 4th from the left of the bottom row.  



Then send the G1 X10 S0.4 F600 command from LW, see if we can get fire....  Experiment with the range of S values up to 1. 







Side note... Can you trace where the red wire goes to?  I think it's at the regular PSU but I don't know what the connection actually is. 


---
**Don Kleinschnitz Jr.** *July 18, 2017 00:53*

**+Ray Kholodovsky** 

Add a pot to IN and PWM "L".


---
**John Hunter** *July 18, 2017 11:16*

**+Don Kleinschnitz** Do you have a recomendation for which pot?




---
**Don Kleinschnitz Jr.** *July 18, 2017 13:14*

**+John Hunter** 



#K40Pot

#K40CurrentRegulationPot



[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)



This blog has lots on K40:



[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
**John Hunter** *July 18, 2017 13:50*

Thank you Don






---
**John Hunter** *July 18, 2017 14:13*

So i did the wiring as you explained.  I assumed i was taking the wires off on the connector that plugged into the white main board box and not the laser psu side.   So after i got done, i ran the command and it fired.  Once.   Since then, issuing the command doesn't work any more.  I've restarted everything, and the only thing i get when i issue that command again is the head will move maybe 10mm on the x.  I can still send commands to move the axis and that all works just fine.   I undid the laser wiring and hooked it back up per original, and it pulses just fine using the pulse button.   Redid my wiring again as you suggested and still no pulse.  It will move a little on the x axis, and then nothing.  If i move the laser head using the jogging, and then reissue the command,  it will once again move along the x axis, and then nothing upon reissue.          



The red wire takes quite a trip, but the best i can tell, is it goes over to the "laser switch" and then back to the main psu


---
**John Hunter** *July 18, 2017 14:27*

okay, this just in.  I let the machine sit off for a few minutes, then tried everything again.   When i tried the command.the laser fired for a couple of seconds and the head moved on the x axis at the same time.  Then when issuing the command again. nothing.

  




---
**Don Kleinschnitz Jr.** *July 18, 2017 14:29*

**+John Hunter** it is very hard to visualize what you did from the description can you provide a wiring diagram, picture and/or photo?



<i>So i did the wiring as you explained</i>; <b>Are you referring to the pot addition.</b>



<i>I assumed i was taking the wires off on the connector that plugged into the white main board box and not the laser psu side.</i>: <b>all the pot wiring will be done from the pot to the LPS??</b>  




---
**Don Kleinschnitz Jr.** *July 18, 2017 14:38*

**+John Hunter** do you think you are/were connected like this?

#K40LightObjectsController



Just realized this is not your old controller!

![images/80915593cb02d2cfecaf29ca86cc2b6b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/80915593cb02d2cfecaf29ca86cc2b6b.jpeg)


---
**John Hunter** *July 18, 2017 14:39*

Okay, it's getting even weirder.   It will fire, only if my chiller is not turned on.  The chiller is plugged into the same outlet as the laser, but not the machine itself.   Am i drawing too many amps?   The second the chiller comes on , the red led marked "laser" on the laser psu goes out.


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2017 14:50*

Move IN to 5v now see if it becomes more reliable. 



And right, it's supposed to move while firing. And then you can change the X value so you can move again to a new target. 


---
**John Hunter** *July 18, 2017 14:54*

Sorry, i was wiring as **+Ray Kholodovsky** was telling me.   I'll have to wait to wire in the pot when it comes in thrusday.   For the record, i reformated the SD card and reinstalled the config and firmware, and i am still getting everything as exactly as described above.




---
**Don Kleinschnitz Jr.** *July 18, 2017 14:55*

**+John Hunter** something is wired "really wrong". The Red LED on the LPS being out says that there is no AC power or the DC voltage is gone. 

Can you draw what you did on the wiring diagram above?




---
**Don Kleinschnitz Jr.** *July 18, 2017 15:49*

**+Don Kleinschnitz** 

Looking back over the thread here is a summary of the laser and controller connections



legend = [pin#, signal name,wire color, function guess]



CN5 controlled the laser 



5) L,AN1,NC

4) WP1, Red (water protect to water/safety circuits)

3) LPWM, Blk (low going PWM signal)

2) LON1,Brn (laser enable low going)

1) GND-Blue (ground)



<b>*LPS</b>



From left to right

H ; NC, high going enable [fires laser]

L ; Brn, low going enable [fires laser]

P ; Blue, protect [connected to safety/water circuits, ground enables laser?]

G ; Blk ground

IN; Blk [analog signal that controls power]

5V; [5VDC source not clear if there is a wire on this. 



As you can see the wire colors do not match so the wiring is more complex than just a 1to1 connection between CN5 and the LPS.



If we can map the wires that were connected to CN5 then we can understand how it was connected and therefore verify the appropriate new connections.



If you can trace the wires from the LPS to their endpoint and draw a wiring diagram that would speed things up.



<s>------</s>

My guess (same as **+Ray Kholodovsky**) for a C3D its would be.

<b>LPS</b>



From left to right

H; NC, 

L ; Brn, <b>connect to C3D PWM</b>

P ; Blue, <b>Protect need to trace this circuit likely connects to water sensor and interlocks</b>

G ; Blk ground, <b>one side of pot</b>

IN; Blk, <b>center pole on pot</b>

5V ;, <b>other side of pot</b>



If you want to fire with the new setup before you have a Pot then tie IN to 5V. 








---
**John Hunter** *July 18, 2017 16:45*

The only thing i have change on the main board related to the psu and lpsu show up in <b>bold</b> type below 



CN5 controlled the laser 



5) L,AN1,NC

4) WP1, Red (water protect to water/safety circuits)

3) LPW, Blk (low going PWM signal)





<b>2) LON1,Brn (laser enable low going) *now wired to mini  FET1bed- (4th from the left)</b>



<b>1) GND-Blue (ground) now wired to 24-ground 2.5mm screw on the mini</b>


---
**John Hunter** *July 18, 2017 16:55*

Reading through all this now makes me think that the reason the red "laser" led goes off is due to the water protection circuit.




---
**Don Kleinschnitz Jr.** *July 18, 2017 17:17*

**+John Hunter** could be, but the RED led is usually tied to the DC voltage in the LPS and the WP does not shut that down.



What did you do with the wires that were connected to the old controller and are now not connected to the C3D?



Are you hesitant to trace the wires from the LPS backward so we can avoid guessing :). We know how to connect a LPS like this to C3D we just are not clear on the wiring in this kind of machine. 


---
**John Hunter** *July 18, 2017 18:42*

This is where the wires off the c3d went. (picture below)  I will see if i can trace all the wires down .




---
**John Hunter** *July 18, 2017 18:56*

*LPS



From left to right

H ; no wire

L ; Brn, originally cn5 L=ON1 now FETbed- on mini

P ;Blue and Black wire (2 wires)



 Blue,originally cn5 gnd now 24v - on mini 



Black Goes to G on LPSU (not moved)



G ; Blk ground goes to P on LPSU (not moved)

IN; Blk goes to CN5  L-ON1 (not moved)

5V; No wire here


---
**John Hunter** *July 18, 2017 18:57*





![images/c26beba8fcf678e03b8e6b5aab2c9fc5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c26beba8fcf678e03b8e6b5aab2c9fc5.jpeg)


---
**John Hunter** *July 18, 2017 19:05*

The other wires on CN5 is a red one connected to WP1 and a black one connected at LPW1, which takes off, goes behind the tube and then back down again on the opposite side and ends at a water sensor of some type on the internal inlet hose See picture below


---
**John Hunter** *July 18, 2017 19:12*

![images/a8b6520de4eccbac097dad798a1338de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a8b6520de4eccbac097dad798a1338de.jpeg)


---
**John Hunter** *July 18, 2017 19:58*

**+Ray Kholodovsky** moving IN to 5v did the trick.   It's now firing just fine.  Now its time to just extend my wires, and mount the Mini some place safer.   I'll add an ammeter and when my pot. arrives, I'll wire it in, and i think i'm good to go.   I may try to do a little write up on it so you all can have it for the next conversion.


---
**John Hunter** *July 18, 2017 19:59*

Thank you both for your help! **+Ray Kholodovsky** and **+Don Kleinschnitz**


---
**Ray Kholodovsky (Cohesion3D)** *July 18, 2017 20:19*

Let me quickly jump in here: 



John, this was indeed the plan, typing IN to 5v is the same as having a pot on the k40 that is cranked up to the maximum. 



Now, please try the G1 line with different S values to see if you can get different burn levels. There's a sample file for this in the documentation --> troubleshooting.  



I'd like to see if we can get good enough resolution on rasters with just this setup. 



**+Don Kleinschnitz** the FET on 2.5 is connect to L, this is our default set up for the k40, it's a hardware pwm pin yes, and this is what we pulse for the variable power output. 


---
**Don Kleinschnitz Jr.** *July 18, 2017 20:22*

**+John Hunter** wow that was a big shift...everything is working?


---
**Sebastian Szafran** *July 18, 2017 20:43*

Great to hear it is working. Good job, gentlemen 👍



**+John Hunter**​ **+Ray Kholodovsky**​ Could you make notes on the final wiring once everything is tested and working fine and possibly document on Laserweb wiki?


---
**Don Kleinschnitz Jr.** *July 18, 2017 20:51*

**+Sebastian Szafran** BTW for future info. The red led on the LPS is not the DC its the laser fire state....my mistake.


---
**John Hunter** *July 20, 2017 15:24*

**+Sebastian Szafran** Once i get everything ironed out, i will do a write up or blog page or something




---
**John Hunter** *July 20, 2017 15:47*

**+Ray Kholodovsky** I'm getting different burn levels but the troubleshooting code is not suited to a 100w laser! It's all way too heavy.   I'll post a picture below.  I do have a config related issue.  I drew up a 30mm square.  When cutting with laser web the box cuts as a rectangle 42mm in the x by 20mm in the y.  Not sure how to fix this.  

![images/2730f9b901851ca0960dca6c68a3130c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2730f9b901851ca0960dca6c68a3130c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 20, 2017 16:46*

Sounds like the steps per mm values for X and Y are different for this machine.  That's reasonable.  See if someone on the big fb groups knows what they are.  



Re: burn levels, the trouble shooting code is exactly what's expected.  It shows us that your burns are not deep enough.   



Resolve steps per mm first (these values are at top of config as alpha and beta steps per mm, respectively) and try the tuning code again. 


---
**John Hunter** *July 20, 2017 16:59*

I will ask around.   Part of the issue with test was that my bed was way off for height.  I haven't done a ramp test yet.  Here's the front and back of another test.  

![images/83b4bf683c7d44cd7b0c1e11e71d46f9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/83b4bf683c7d44cd7b0c1e11e71d46f9.jpeg)


---
**John Hunter** *July 20, 2017 17:00*

![images/63a59b84fdc65cebeda584167d04f625.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/63a59b84fdc65cebeda584167d04f625.jpeg)


---
**John Hunter** *July 21, 2017 01:00*

Okay, i got it figured out by just doing some measuring and ratio work.  Steps for the x need to be 111.2294.  Steps for the y turn out to be 236.3625.   That's a little bit rough.  In 100mm one side of the square was 99.63 and the other side was 99.97.   So just a bit of tweaking left to do.  I guess the cut should be the center of the beam, so a little short of 100 would be better than exactly 100.




---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2017 16:37*

I use 4 thou/ 0.1mm for the beam diameter, this seems to be accurate at least on k40. 

By default the cut is center of the beam yes, so half that value is added to the measurement. 



It does seem like you are getting variable power in that first pic. If not, and changing the pwm period does not help, then we can discuss alternative wiring options to try. 


---
**John Hunter** *July 21, 2017 20:20*

Yes, the power ramps up perfectly.   I now have an ammeter installed on my laser and on each line in the test cut increases by a few milliamps.  I'll post up a youtube video and post the link.  Next question, would it be okay to hook the board up to a 5volt supply from the original board?  It would be nice to be able to use the jogging feature with out having a computer plugged in to it.  




---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2017 20:30*

No. We should give the board 24v and let it regulate its own 5v. Find a 24v line and connect it to the top pin of main power in screw terminal. 


---
**Michael Howland** *April 28, 2018 02:39*

Hey guys, I have followed this thread to the best of my ability. I have the laser connecting to the pc, moving under its own power both from PC and LCD, homing properly, but the laser is not firing. I am really sure that it is because of the water protect circuit. I am at a loss on how the WP circuit was handled. Could you guys elaborate on this? I could start a new thread but my machine is nearly identical on how it is setup so I figured asking this here would keep the info together.  Same RD mainboard, etc. 


---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2018 02:43*

New thread would be good. Pics of your exact machine and wiring please. 


---
**Michael Howland** *April 28, 2018 02:48*

okay




---
*Imported from [Google+](https://plus.google.com/105510960839020211759/posts/f8tjg9TWi7y) &mdash; content and formatting may not be reliable*
