---
layout: post
title: "I have a question about PWM,. I have seen the instructions and it seems that \"400\" in \"laser_module_pwm_period\" does better then \"200\" for me but it still seems it goes so quickly from light to dark and watching the ma meter"
date: August 14, 2017 12:09
category: "C3D Mini Support"
author: "Tammy Mink"
---
I have a question about PWM,. I have seen the instructions and it seems that "400" in "laser_module_pwm_period" does better then "200" for me but it still seems it goes so quickly from light to dark and watching the ma meter it goes max the last 3ma of a 10 grayscale range test the first which is at what 10% of the seems to read at maybe 4ma so it start high. Test grayscales, I can get maybe 2 good shades of light, but once it goes even medium dark, it goes way dark, I am guessing because it caps out I know it's not exact but I think further tweeks  could maybe get me closer. What does "laser_module_pwm_period" exactly do so I can try and determine if I need to go down or up from "400"? Basically right now when running the PWM test code from the website the amperage starts high and ends high so it's capping out fast at the top end.  I know I can make further tweaks in LaserWeb by setting the range and it has helped. I do have it kind of going, and it's neat and I know there are limitations but I think I can do a little bit better based on what others have done.





**"Tammy Mink"**

---
---
**Jorge Robles** *August 14, 2017 12:42*

I think different tubes will behave differently.. as long as are chinese, you know. Fine tuning need the boring trial error method I guess


---
**Don Kleinschnitz Jr.** *August 14, 2017 12:58*

<b>What does "laser_module_pwm_period" exactly do so I can try and determine if I need to go down or up from "400"?</b>



To oversimplify it changes the frequency (how often) that the power level can be changed. During each "pwm period" the laser is told to achieve a certain power level by how long the PWM signal is asserted. 



So the PWM period can effect engraving by setting the power control response to slow or two fast for the conditions under which the image is being created. 



Example: If the PWM period is long relative to the rate that the laser marks, the power that a pixel marks with is out of sync with the rate at which it marks. If the PWM period is to short it may not be seen by the LPS because it takes longer than the PWM signal to respond.



This is a complex subject but some things that can theoretically impact engraving are oversimplified below: 

...The type of material being engraved. Each material has level of energy at which it will burn.

...The moisture content in a material will change its burn point.

...The speed of engraving. The faster you go the higher the pixel rate and the lower the power per pixel. Slow down and you get more exposure/pixel and the controller and laser can respond better.

...The combination setting of your pot and software control. The pot and the software PWM control work together to create the actual power from the laser. Its approx =  (software %DF) *(LPS %DF).

...The resolution of the image. At to high a resolution the laser cannot respond. At to low you may get excessive exposure and scan overlap.

...Focal point, an out of focus beam has a larger exposure area than an in-focus beam meaning the same laser power is spread over a larger area. Out of focus conditions also cause scan overlap.



The challenge is that these factors interact to provide a given grey shade response from a given material. Speed, power, and resolution work together to create a certain degree of "blackness" for a certain material.


---
**Tammy Mink** *August 14, 2017 15:01*

Well that is quite a long explanation :), I guess I asked for it hehe. Let me check if I am correct ideally what I want anyways, if I set my power on the machine to 10ma and then via the software my power is 0.1 (AKA 10%) then the ampmeter should be 1ma, and 0.5 (AKA 50%) should be 5ma and 1.0 (AKA 100%) should be 10ma. 




---
**Don Kleinschnitz Jr.** *August 14, 2017 15:29*

**+Tammy Mink** yes that is correct. Keep in mind the change between light and dark can be affected by more than just power as stated above.





BTW that was the short version:)

[donsthings.blogspot.com - Engraving and PWM Control](http://donsthings.blogspot.com/2016/12/engraving-and-pwm-control.html)


---
**Tammy Mink** *August 14, 2017 18:34*

That was helpful, I realize when I was testing it at too slow of speed, at least when I was doing my grayscale burn tests. While of course I can see that those settings can change I am going to start my testing over at the speed of 300mm/s and go back to 200 as the PWM period to see what happens. :)


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/QUhGAVMMzxd) &mdash; content and formatting may not be reliable*
