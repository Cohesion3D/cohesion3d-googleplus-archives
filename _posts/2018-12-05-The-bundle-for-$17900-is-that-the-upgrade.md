---
layout: post
title: "The bundle for $179.00 is that the upgrade"
date: December 05, 2018 21:44
category: "K40 and other Lasers"
author: "Ashra Campbell-Clarke"
---
The bundle for $179.00 is that the upgrade.  





**"Ashra Campbell-Clarke"**

---
---
**Cohesion3D** *December 05, 2018 22:30*

I'm afraid I don't understand. 


---
**Griffin Paquette** *December 06, 2018 02:56*

The $199 board is the new Laserboard. If you are upgrading a K40, that’s what you want to get. It’s a great upgrade for the machine. Keep the required power supply as part of the purchase as well. You need both. 


---
*Imported from [Google+](https://plus.google.com/114192144580399794033/posts/Cp3ruxzpQ58) &mdash; content and formatting may not be reliable*
