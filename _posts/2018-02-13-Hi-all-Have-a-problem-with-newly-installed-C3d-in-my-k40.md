---
layout: post
title: "Hi all. Have a problem with newly installed C3d in my k40"
date: February 13, 2018 18:26
category: "K40 and other Lasers"
author: "chris b"
---
Hi all. Have a problem with newly installed C3d in my k40. Will post photos of original wiring and of how I have installed my new board.

The issue, motion along the x axis is fine, moves freely and predictably using jog, 

 but the y axis refuses to move using jog or asking it to cut a simple shape.

 I saw another person post with same issue, recommendation was move to A driver and use a different config file,  worked for that person but not me.

Will re install original board after work tomorrow and see what happens. 

 All advice appreciated, hope its as simple as incorrect wiring.



![images/09562b01906b468efd1db0a3c56cd789.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/09562b01906b468efd1db0a3c56cd789.jpeg)
![images/3f75d45339758ed89a23b216cbe0cfe8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f75d45339758ed89a23b216cbe0cfe8.jpeg)
![images/d512e96a56260e12168b13c7d8f30a1a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d512e96a56260e12168b13c7d8f30a1a.jpeg)

**"chris b"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 18:29*

Check that the Y motor plug is connected properly, it looks offset in your first pic. 


---
**Kelly Burns** *February 13, 2018 18:43*

What firmware are you running and where did configuration come from?  


---
**Kelly Burns** *February 13, 2018 18:45*

**+Ray Kholodovsky** I just went up and looked at mine.   Chris’ is definitely offset.  


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 18:46*

**+Kelly Burns** from me, on the memory card provided, as with every Mini Laser Upgrade bundle :)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 18:46*

If you zoom in you can actually see the exposed pin on the right


---
**Kelly Burns** *February 13, 2018 18:53*

**+Ray Kholodovsky** mine is still in original packaging.  ;)


---
**chris b** *February 13, 2018 18:58*

Thank you ,and yes you are correct, I did unplug to check it several times and this last time replaced it incorrectly, result was same with correct sheeting, apologizes for  using that photo. 

The firmware and config are the ones that came with it, I purchased the upgrade pack from cohesion so I didn't check its version but will. 

The alternate config I tried came from a post on here. 

I am fairly sure its a wiring issue as opposed to firmware/config. 

Will swap back to original board to be sure and post results after rechecking connections. 

Very much appreciate all the advice. 




---
**Kelly Burns** *February 13, 2018 19:20*

Swapping boards would be one of the last things I would do.  First remove power and Swap the x and y drivers.  Power up and test.  



Is the stepper “holding” when there is power applied?  



Next verify that the stepper doesn’t have a plug on it.  If it does, make sure that plug is secure.  


---
**Andy Shilling** *February 13, 2018 22:05*

Have you plugged it into the X stepper driver and true it. Make sure you manually centre the laser head first and then jog X 10mm either way. If it moves if day it's the Y stepper driver if not it's the motor. Or possibly the pot is turned down to much and it's not getting the amps it needs.


---
**Griffin Paquette** *February 14, 2018 03:38*

Your LCD adapter is backwards


---
**chris b** *February 15, 2018 12:37*

Sorry for lack of response ,back has gone again so laid up and medicated, will catch up and act on all the help then post result as soon as able. 




---
**chris b** *February 18, 2018 21:34*

OK, this evening I was finally able to get back to fault finding my c3d  install. 



Step one was to check every connection and ensure properly seated. 

I then remade the micro sd using the alternate config ( with Y switched to A) provided by Ray Kholodovsky, found it in a post by somebody with what sounded like the same issue,

Reset the board and Success!!  X and Y functioned as expected, have it a couple of simple shaped and again all good. 



Lastly I reinstalled the display adapter in the correct orientation, thank you Griffin Paquette.



Tomorrow I hope to get to grips with the power settings in lightburn and restart some projects. 

Thanks to all for all the pointers 




---
**Kelly Burns** *February 18, 2018 22:49*

Good to hear


---
*Imported from [Google+](https://plus.google.com/116957066831422624000/posts/VNE7uY2VPDF) &mdash; content and formatting may not be reliable*
