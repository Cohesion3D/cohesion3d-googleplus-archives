---
layout: post
title: "Has anybody used the trinamic 2130 driver with C3D in there laser?"
date: August 11, 2018 19:36
category: "General Discussion"
author: "Richard Wills"
---
Has anybody used the trinamic 2130 driver with C3D in there laser? I have a 4030 chinese laser. i am running lightburn with grbl lp firmware 4 axis build.





**"Richard Wills"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2018 20:47*

The TMC2130 "smart" features are not supported, so you could use the 2130 in dumb mode, or get the TMC2100 which is dumb to begin with.  See instructions here:



[plus.google.com - +Ray Kholodovsky - I've recently bought a couple of TMC2100s to use on my ReM...](https://plus.google.com/+DushyantAhuja/posts/KG2RsTWnGbk)


---
**Richard Wills** *August 12, 2018 22:12*

Will just switching to this driver quiet down the stepper from 4988?


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2018 20:24*

Presumably. 


---
*Imported from [Google+](https://plus.google.com/115971902636333791915/posts/4Rse6bpNYfV) &mdash; content and formatting may not be reliable*
