---
layout: post
title: "Hi everyone, I just got my k40 setup today and it has a few mods cohesion3d board, I added an analog amp meter, it has a new table, and I am running Lightburn with full license ."
date: April 11, 2018 23:02
category: "C3D Mini Support"
author: "Sam Jones (Thumpszilla)"
---
Hi everyone,

I just got my k40 setup today and it has a few mods cohesion3d board, I added an analog amp meter, it has a new table, and I am running Lightburn with full license . I went through and setup the mirror alignment this morning and that is good. I am hitting the same spot from all 4 corners and my workpiece is 50.8mm from the bottom of the housing fo mirror 3. I have check my ground connections and even tried it without the analog amp meter. However when I go to engrave ( anodized Aluminum) no matter if I have it in greyscale, dither, or any other setting I am not getting enough power to even scratch it. I have the settings on the machine hard set to 15ma and power in lightburn set to 100%. When I hit the test fire button I get good power and the amp meter reads dead on 15ma. I have attached a screenshot of the image settings in lightburn. Please help!!!

![images/b8feee314257dd3d94d4c433d175b5e5.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8feee314257dd3d94d4c433d175b5e5.png)



**"Sam Jones (Thumpszilla)"**

---
---
**Jim Fong** *April 12, 2018 01:10*

Only time I was able to make a mark on anodized aluminum is at slow feed rate.  100mm/sec is too fast.  Try 10mm/sec or slower. 


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2018 02:50*

The board with default config caps laser power to 80%.  You can disable this by finding the min laser power line on the board's memory card config.txt file and set to 1.0 then save, eject, and reset the board.  That'll get you actual max power.  Otherwise board changes nothing :)


---
**Don Kleinschnitz Jr.** *April 12, 2018 15:43*

Will the machine cut ok???


---
**Sam Jones (Thumpszilla)** *April 12, 2018 16:54*

The machine cuts fine but in image mode power is very reduced. The best I have been able to get is about 6 amps with lightburn limit set to 100% and machine limit set to 80%. I have a friend that uses a k40 to engrave anodized all the time and can do it in a single pass at 150mm/sec. He is running a lightburn x7 dsp




---
**Sam Jones (Thumpszilla)** *April 13, 2018 01:15*

ok I was told by another k40 user that I needed to wire the pwm to the cohesion board. I know where the plug is on the cohesion board but not which wires got to it and in which order. I attached a pic of the power supply I have.

![images/410a1f49547bd589a6d41395971d9b5d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/410a1f49547bd589a6d41395971d9b5d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 01:32*

No, you only need to follow exactly what is shown in the install instructions for the board. 


---
**Sam Jones (Thumpszilla)** *April 13, 2018 03:41*

**+Ray Kholodovsky** Then why with laser at 80% and lightburn set at 100% and I get 9amp max? I only have this issue when lightburn is engraving an image. Power works fine in cut mode and test fire. Mirrors are aligned and clean table is adjusted correctly as well. I need some help or I will have to replace this board with something else. Something isn't right either the board or lightburn I have tried settings and images used by a friend with the same exact setup. Only difference is mine has the digital controls his is analog and has a pot. His turns out great and mine turn out like it has barely discolored the surface.


---
**LightBurn Software** *May 02, 2018 05:44*

When engraving with dithering, the power bounces between 0 and max based on whether the current pixel is filled or not. With an analog meter, you're going to see the average of those, the value of which will depend on how dark or light the overall image is. Rather than starting with an image, which is about the hardest thing you can do on aluminum, try marking or scanning simple shapes.  If those work, an image should too - there's nothing magic or different with the code for an image, other than the beam being turned on and off very rapidly.


---
*Imported from [Google+](https://plus.google.com/112494554699999339483/posts/Vd8nH25K6mH) &mdash; content and formatting may not be reliable*
