---
layout: post
title: "Greetings all, I wrote previously as Robert Hopkins and am continuing to have problems with my board"
date: October 03, 2018 19:29
category: "C3D Mini Support"
author: "Kestrels Fury"
---
Greetings all,



I wrote previously as Robert Hopkins and am continuing to have problems with my board.  I have mounted the board with spacers and am using a 24v 5a external power supply.  After getting all of this setup, I am only getting a bright red power led, and very faint green on the 3v3 led.  Is this board defective, or could it be something else?  I checked the polarity before powering up the system so no magic blue smoke from that, I don't see any bulging caps, 



![images/1fc7195dba41733b6ad21b3291c2a57f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1fc7195dba41733b6ad21b3291c2a57f.jpeg)
![images/f075c6a3012dcec4bbbcf0122a89fa26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f075c6a3012dcec4bbbcf0122a89fa26.jpeg)
![images/103ba1c1ff69cf46a955a9be633949b6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/103ba1c1ff69cf46a955a9be633949b6.jpeg)
![images/8187eb4a13ccf98e1c6a5125cf3442c6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8187eb4a13ccf98e1c6a5125cf3442c6.jpeg)

**"Kestrels Fury"**

---
---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 19:30*

[https://www.lasergods.com/cohesion3d-led-indicator-status/](https://www.lasergods.com/cohesion3d-led-indicator-status/)

[lasergods.com - Cohesion3D LED Indicator Status](https://www.lasergods.com/cohesion3d-led-indicator-status/)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 19:32*

Power down, Remove all connections except power, And remove the sd card. Then power back up and check led status.


---
**Kestrels Fury** *October 03, 2018 21:14*

no difference or change


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 05:19*

what external power supply did you use? did you sever the +24v line coming from the laser power supply?




---
**Kestrels Fury** *October 04, 2018 16:58*

This is the power supply and the +24v lead from the laser power supply is cut as shown the picture #3

![images/23de3d9bb6e19dc4b59575b3c68716dd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/23de3d9bb6e19dc4b59575b3c68716dd.jpeg)


---
**Kestrels Fury** *October 04, 2018 16:59*

I was also able to confirm that is was the right wire to cut as I matched it up from the previous board.


---
**Kestrels Fury** *October 05, 2018 16:44*

Are there other troubleshooting steps I should take?  Is it a bad board?  Are there gremlins lurking in the setup?  Where should I turn from here?


---
**Kestrels Fury** *October 10, 2018 19:21*

Greetings,

I am still waiting for an answer to my request for a replacement.  I have done all you have suggested ie. proper spacers, precautions, and an external power supply.  The board is not working at all now leading me to believe, after adding the power supply, that I have a bad board.  Now my laser has a cut +24 power lead from the onboard power supply.  I await your reply.  

Addendum to this post:

I just received a call from the owner and am VERY happy the resolution.  Thanks again, Ray




---
*Imported from [Google+](https://plus.google.com/104761945487452832708/posts/6gxVR2L2zJU) &mdash; content and formatting may not be reliable*
