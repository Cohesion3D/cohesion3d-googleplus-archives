---
layout: post
title: "Before placing an order I wanted to make sure my K40 was compatible"
date: February 06, 2017 08:54
category: "K40 and other Lasers"
author: "Austin Baker"
---
Before placing an order I wanted to make sure my K40 was compatible. Looks like my power supply doesn't have a cooling fan -- will that be an issue? Thanks

![images/45688045f6c2f05eed9b329bfcb42616.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/45688045f6c2f05eed9b329bfcb42616.jpeg)



**"Austin Baker"**

---
---
**Don Kleinschnitz Jr.** *February 06, 2017 11:42*

Does that electronics bay have a fan in it? Is this a new or older machine?


---
**Stephane Buisson** *February 06, 2017 12:51*

just the previous version, I am pretty sure it will work directly with C3D Mini.


---
**Austin Baker** *February 06, 2017 15:35*

**+Don Kleinschnitz** no fan. Just got it a few weeks ago 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 16:04*

Wiring looks just like my machine. <thumbs up>

Is this the new red and white machine? 


---
**Don Kleinschnitz Jr.** *February 06, 2017 17:11*

So in the new unit they took out the LPS and the cabinet fan? :(.

I would at least put a muffin fan or two on the right sidewall to move air in that section of the enclosure. One related consideration is at the same time, minimize air pulled from the cutting chamber.


---
**Steve Anken** *February 06, 2017 17:48*

Looks just like one I converted and it took a while to get that silicon off the connectors, longer than installing and running it.


---
**Austin Baker** *February 06, 2017 19:44*

**+Ray Kholodovsky** Yes -- red and white. Purchased on Amazon ~3 weeks ago. Controller is board version "M2".


---
**Austin Baker** *February 06, 2017 19:46*

**+Don Kleinschnitz** Yeah I am interested in closing off the side cabinet better. Dual effect of keeping it clean, and improving air-flow (smaller volume) on the table side.


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 19:47*

Yepp.  New case colors and possible tweaks to PSU.  Connectors and M2Nano board look same as mine from last spring.


---
**Stephane Buisson** *February 07, 2017 11:33*

Silicon or hot glue ? look like factory don't like our upgrade. (they never reply to my mails)


---
**Don Kleinschnitz Jr.** *February 07, 2017 13:04*

Is this the one with the new digital control panel?


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 15:44*

Picture of the top with the lids closed please.. ^


---
*Imported from [Google+](https://plus.google.com/100996155784545993018/posts/Y2EA94mkuhJ) &mdash; content and formatting may not be reliable*
