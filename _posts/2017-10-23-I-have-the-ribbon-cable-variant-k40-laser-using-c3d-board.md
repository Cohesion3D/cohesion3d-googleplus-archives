---
layout: post
title: "I have the ribbon cable variant k40 laser, using c3d board"
date: October 23, 2017 02:18
category: "K40 and other Lasers"
author: "Kevin Lease"
---
I have the ribbon cable variant k40 laser, using c3d board.  I have an issue with one of the end stops for homing, I have two optical end stops.  I am going to try to fix the optical end stop but if that fails as a backup I will try to switch to mechanical end stops as suggested on k40 board.  I had some questions, if I were to switch to mechanical end stops, would you ditch the ribbon cable or can I keep it plugged in for the x stepper power supply and then run separate wiring for my mechanical end stops?  I think the power supply for the X axis stepper comes from the ribbon cable currently, it then has a connection to the stepper through the little board that has the Y axis optical endstop; if I had to ditch the ribbon cable the stepper motor can be unplugged, then I would run connections from the stepper motor to the 1B1A2A2B above the X A4988 driver board, and run my x and y end stop wiring to the bottom of the board x and y end stops as done for the z table end stop.? If the ribbon cable can be left in place that would be one less thing to have to change.





**"Kevin Lease"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 23, 2017 03:06*

I would leave the ribbon cable in place, disconnect the opto/ board on the "far" end, and wire in a mechanical switch.   That's the simplest way.  The ribbon cable connects to the X MIN and Y MIN lines so you'd want to plug your mechanical into that respective port (the white connectors at the bottom edge) and use M119 to determine if it is reading correctly.  Possibly may need to invert the logic. 



Z Min and Z Max ports are still available for your Z table. 


---
**Kevin Lease** *October 23, 2017 11:39*

**+Ray Kholodovsky** ok thank you for your help


---
**Frank Broadway** *August 09, 2018 18:38*

**+Kevin Lease** I think I have the same problem with converting my K40. What was your solution to this problem?


---
**Kevin Lease** *August 09, 2018 22:14*

**+Frank Broadway** 

[donsthings.blogspot.com - K40 Optical Endstops](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html?m=1)



See dons webpage

I bought a couple of TCSt1030 and 100 ohm resistors and desoldered original parts off x and y endstops and used these 



Worked great


---
**Frank Broadway** *August 12, 2018 20:38*

**+Kevin Lease** Thank you for the reply!


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/heEEvrTLkvB) &mdash; content and formatting may not be reliable*
