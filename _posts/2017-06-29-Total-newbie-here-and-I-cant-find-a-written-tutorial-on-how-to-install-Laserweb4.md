---
layout: post
title: "Total newbie here and I cant find a written tutorial on how to install Laserweb4 ?"
date: June 29, 2017 05:36
category: "K40 and other Lasers"
author: "William Kearns"
---
Total newbie here and I cant find a written tutorial on how to install Laserweb4 ? 





**"William Kearns"**

---
---
**laurence champagne** *July 03, 2017 00:51*

I feel your pain, it seems like everything is greek to me. Even all the super simple stuff that everyone else just overlooks because the expect people to know how to do it.



Go to this link: [github.com - LaserWeb4-Binaries](https://github.com/LaserWeb/LaserWeb4-Binaries/releases)



under downloads you will see two links, if you have a 64bit system click on the one that ends in 64.exe, if you have a 32 bit system click on the one that ends in 86.exe. Then just folow the promps like you would for installing any other program on your pc. 


---
**William Kearns** *July 03, 2017 04:39*

**+laurence champagne** thank you I got laserweb4 now!!!! I am very experienced with illustrator is there a file format I can export from it to use in vectors format


---
**laurence champagne** *July 03, 2017 18:13*

I solely use Illustrator also. All the tutorials I've seen recommend saving your files as .svg and then importing into laserweb.  My problem is that when I import, the size of the designs are 25% smaller. I started saving the files as .dxf and was having some success. I just started playing around with this yesterday and won't have a chance until tomorrow to mess around again.



I really only do engravings with my laser and as of now I wish I would have done some more research before upgrading to the cohesion board and laserweb. 



The stock nano board and corellaser worked quickly and perfect for what I needed. the only reason I upgraded was because I couldn't figure out how to adjust the steps per mm on that board.






---
**crispin soFat!** *July 07, 2017 07:16*

**+laurence champagne** Change the resolution to match your files under

Settings->File Settings

SVG PX PER INCH 96PXPI



Your SVG files might not be 96pxpi.


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:28*

This is an old thread but, FYI, Illustrator exports all SVG files at 72 DPI. Set LaserWeb accordingly.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/bmRFCHMEgsw) &mdash; content and formatting may not be reliable*
