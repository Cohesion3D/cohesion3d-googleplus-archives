---
layout: post
title: "Greetings all. Just installed C3D and so far so good"
date: July 04, 2018 00:58
category: "C3D Mini Support"
author: "Ken Begin"
---
Greetings all.  Just installed C3D and so far so good.  Just 1 question so far.  Laser is a K40, 1 year old, had to replace the power pot.  Comp. system is Windows 7 ultimate.  Nano board was working fine but I wanted the flexibility of C3D.  My question is that it seems that I still have to control laser power by the pot.  When I try to adjust it through Lightburn I get maybe 20 % difference in the overall performance.  I'm a little puzzled?





**"Ken Begin"**

---
---
**Anthony Bolgar** *July 04, 2018 01:06*

The way it works is:



You set your POT to say, for example, 10ma. The default config file for the C3D board then limits it to 80% of 10mA, giving you 8mA for Lightburn to control. You can edit the config file to allow for 100% of of what you set the POT at. The 80% was set in the config to protect people from overdriving there tubes if they did not have a current limiting POT installed in their machine. I hope this makes sense.


---
**Ken Begin** *July 04, 2018 17:38*

**+Anthony Bolgar**  Thanks Anthony, makes perfect sense.  I see that I will be doing some more experimenting. 


---
*Imported from [Google+](https://plus.google.com/100247205394366347256/posts/NTSU7ZH2Evm) &mdash; content and formatting may not be reliable*
