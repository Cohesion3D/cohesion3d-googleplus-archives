---
layout: post
title: "Our new LaserBoards arrived yesterday, all pre-orders have now shipped, and we are in stock and ready to go!"
date: December 07, 2018 21:16
category: "General Discussion"
author: "Cohesion3D"
---
Our new LaserBoards arrived yesterday, all pre-orders have now shipped, and we are in stock and ready to go!



LaserBoard is our new laser-focused 😀 controller with 4 quiet and powerful Trinamic stepper drivers for running your laser head, Z Table, and Rotary attachment with minimal fuss. 

It has a variety of ways to control your laser and peripherals, a direct connect DC Jack, and isolation/ protection measures to ensure stable performance.



Our low introductory price won't last long, get yours today!



[https://cohesion3d.com/cohesion3d-laserboard/](https://cohesion3d.com/cohesion3d-laserboard/) 

![images/3a3687300968ece4fac88fd1a039acfa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3a3687300968ece4fac88fd1a039acfa.jpeg)



**"Cohesion3D"**

---


---
*Imported from [Google+](https://plus.google.com/+Cohesion3d/posts/52ecJPbhZM8) &mdash; content and formatting may not be reliable*
