---
layout: post
title: "Stream ends before finish. Hi I am using Cohesion 3D mini with Smoothie and Lightburn My Problem: Lightburn does not stream until finish of longer job"
date: December 18, 2018 10:52
category: "C3D Mini Support"
author: "Rey Man"
---
Stream ends before finish.



Hi I am using Cohesion 3D mini with Smoothie and Lightburn



My Problem: Lightburn does not stream until finish of longer job. Short jobs are doing well. But bigger Jobs just end after 20 up to 120 min wihout reaching the end of the cutting job.



The console says:

stream compleated in xx:xx min

homing



But the job is not completed at all. In the preview tool everything is fine, it shows the job until end. But the real stream ends befor finishing the job.



- I use the propriate USB-Cable from C3D-Shop

- I have grounded the cutter at the screw on the backside

- I use the appropriate power supply for the C3D-board



Please help me. Thanks









**"Rey Man"**

---
---
**Rey Man** *December 18, 2018 13:08*

![images/f951de77b2c8ef5bcc6ea0a140b01774.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f951de77b2c8ef5bcc6ea0a140b01774.jpeg)


---
**Rey Man** *December 18, 2018 13:12*

Basecally everything works fine but only with short jobs. I was redirected from the Lightburn Group to here wit this issue.

![images/4bba52f0666ffae024c7ad9b8703ff15.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4bba52f0666ffae024c7ad9b8703ff15.jpeg)


---
**Rey Man** *December 18, 2018 19:57*

The C3D is well isolated from the chassis. Even with board completely out of the chassis it does not finisch long term jobs, longer than about 20 min. I have no idea how to proceed. Has anybody any suggestion?


---
**Joe Alexander** *December 18, 2018 22:44*

if you have the GLCD screen I would recommend trying to run the stream after copying the gcode to the sd card in the C3D board, eliminating the potential of the usb connection being the issue. BTW what are you doing that takes 120min, large raster engravings? If so maybe GRBL-LPC would work better as it can handle faster speeds.


---
**Rey Man** *December 19, 2018 15:28*

Thank you for you answer. That is a good idea. I have not used GLCD before and now tried it. Right at start the laser bumped with a horrible noise in the top axis. 



Maybe I have to do some adjustments in order to print from sd. That s another issue but do you know a source explaning to set the values for cutspace etc. on the GLCD without Lightburn? I have been thinking that all (cut-)information is in the config and the gcode.



I keep trying to realize long term cutts/engrave with the sd  as recommended by you. Maybe also with other computers.



I scan and cut little tags. One takes 5 Minutes (40x60cm, 60W Laser, at 6.000 mm/min engraving speed. To make 25 tags it takes about 2h.) But I never reached the end. Sometimes 95% sometimes only 5%. When I increased the speed over 6.000 mm/min I was not satisfied with accuracy in some areas.



I tried GRBL some weeks ago with different sd card and GRBL firmware. I was able to connect the cutter with Lightbrun GRBL. But I was not able to print. It just happend nothing when I hit the start button.



Producing the above mentioned tags even faster may be possible. But right now I am little desperate because I can not even produce 10 units securely in one stream. 



I have learned that the first run in the morning works best and the cutter resp. the board makes far longer job than with the following jobs. 



Isn t it possible that the C3D board has thermical issues? 










---
**Joe Alexander** *December 19, 2018 16:28*

it really shouldn't have any issues with overheating unless your running a massive amount of current through the stepper driver chips. 40cm x 60cm per tag? must have a huge bed to accommodate 25 of them at the same time. I would size it down to maybe 10 per run, potentially eliminating errors from running so long. Plus a person should be monitoring the laser at all times so it shouldn't be an issue with production delay, you know?

  As for setting your size: with smoothie you can edit the config.txt file to change your bed size parameters. for details check out [smoothieware.org - start [Smoothieware]](http://www.smoothieware.org) its pretty easy to do.

  If for some reason that all fails you could try Laserweb4: its also free and open source and works really well with C3D and smoothie-based boards. Its what I use personally, only downside is that you cant do much graphic creation or editing within it(I do all mine in inkscape).


---
**Rey Man** *December 19, 2018 17:45*

Thank you .... the size of my cutter bed is 40x60 cm. One tag is 5,7 cm diametre. I can not even engrave and print 10 of them securely in one run until it behaves like mentioned.



Above I mentioned that I have no idea how to set the size on the GLCD-Sreen. My config for smoothie is properly edited for 40x60 cm cutter. Everything works fine in the short run with one to five tags. But the system does not finish longer jobs securely.  



In the moment it is no option for me to turn to laser web. I have already worked with it. I have bought Lightburn license and C3D upgrade kit because I it should work fine with each other. But Lightburn says that the mentioned behavior above is a matter of the C3D board or the connection and not the software. 



But of course I will also try if it works better with laser web. In the moment I am just little sad this is obviously no knowen issue with any path to solve my problem: Everything works but only a few unknowen time per stream-turn.




---
**Rey Man** *December 21, 2018 21:40*

I tried it with another computer today. But same result.


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 04:38*

I am curious about the board isolation. Best I can see, this is screwed directly to the metal frame of the laser with metal screws? 


---
**Rey Man** *December 23, 2018 22:23*

yes, but not directly - with tiny plastics washers inbetween. I also tested it free hanging in front of the cutter as well as with printed mounting. I can say that there is defenitely no cunductive connection between chassis and controller board. But symptoms stay the same.

![images/148945510aeea252d5f4f7bb3b04b493.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/148945510aeea252d5f4f7bb3b04b493.jpeg)


---
**Rey Man** *December 23, 2018 22:25*

![images/79337305e0174e2b489816b0544057d7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/79337305e0174e2b489816b0544057d7.jpeg)


---
*Imported from [Google+](https://plus.google.com/117035180069976838179/posts/jdXCSdsyFfr) &mdash; content and formatting may not be reliable*
