---
layout: post
title: "Just upgraded my k40 with the new laserboard and notice as soon as lightburn detects the laser I get a high pitched noise, sounds like its coming from the laser tube but cant really tell"
date: December 12, 2018 18:30
category: "C3D Mini Support"
author: "Matthew Staten"
---
Just upgraded my k40 with the new laserboard and notice as soon as lightburn detects the laser I get a high pitched noise, sounds like its coming from the laser tube but cant really tell.  Does not stop until remove power from the laserboard. Any ideas?





**"Matthew Staten"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2018 18:40*

"LightBurn detects the laser" - try using other words to explain this please? 



I know the docs for LaserBoard aren't up yet so can you show me pics of how you've connected everything, and perhaps a video of the thing you're talking about? 



It's easier to attach pics/ video in the C3D FB Group, so would be happy to move the chat over there if that suits you. 


---
**Matthew Staten** *December 12, 2018 19:03*


{% include youtubePlayer.html id="o7agjV5loE4" %}
[youtube.com - Laser board setup](https://youtu.be/o7agjV5loE4)



Not sure if sound is from laser or motors, but it does it as soon as usb is plugged in and lightburn is open and connected to laser. Doesn't stop till power is pulled. I have plan to get some nylon to isolate the board asap.



Laser homes correctly at left rear but coordinates suggest that left front of machine is 0,0 and moving y axis with up arrow moves toward front of machine (coordinates counts down as going toward front of machine which is backwards from the displayed grid in lightburn.) Is that normal?


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2018 19:23*

Got it. That’s the hissing of the motors while engaged but not moving. 



The Trinamic drivers have 2 modes, a completely silent mode called stealthchop and a more powerful mode called spreadcycle. We have defaulted to use the more powerful mode but that’s why this sound exists. 

We can play with this configuration a little later but it’s the spread blocks at the bottom of the config file on the memory card 



And you don’t need standoffs, LaserBoard is isolated. 



I’ve just thrown up a thread in the fb group showing the rudimentary install and wiring, if you’d like to double check yourself against that. 


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2018 19:40*

But yeah the wiring in your vid looks good :) 


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2018 23:09*

Dare I ask?  How are the things going?


---
**Matthew Staten** *December 14, 2018 16:29*

Figured out when setting up machine in lightburn I must select the bottom left corner as the origin. Once I did that everything is working as it should. Now I just need some time to play and learn what new feed rates and power levels to use. 


---
*Imported from [Google+](https://plus.google.com/113481888999057772467/posts/4rJ5qMXaEfe) &mdash; content and formatting may not be reliable*
