---
layout: post
title: "Today's project: converting a Monoprice i3 to run on a Cohesion3D Mini"
date: December 31, 2016 02:37
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Today's project: converting a Monoprice i3 to run on a Cohesion3D Mini. And it is now printing a piranha. 



![images/af9f4a1f44a88a4b499b41d3d426a1d8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/af9f4a1f44a88a4b499b41d3d426a1d8.jpeg)
![images/3b95d84f04b9ee2450367d1b21f9485f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b95d84f04b9ee2450367d1b21f9485f.jpeg)
![images/070fe12b1cb59681880b13c1794f6224.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/070fe12b1cb59681880b13c1794f6224.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jonathan Davis (Leo Lion)** *December 31, 2016 02:41*

I'm assuming it's one of those Chinese 3D Printers.


---
**Erik Cederberg** *December 31, 2016 03:41*

**+Jonathan Davis**​, it is a Monoprice branded Wanhao i3


---
**Alex Hodge** *January 02, 2017 03:57*

Actually a great printer. I've got one and used it to print my MPCNC that I'm building. That printer is basically unbeatable for the money and there is a very strong community behind the Wanhao Duplicator i3...Which carries over to the rebrands like this one.


---
**Jonathan Davis (Leo Lion)** *January 02, 2017 03:59*

**+Alex Hodge** ok and where would one find such a system.


---
**Alex Hodge** *January 02, 2017 04:02*

[Monoprice.com](http://Monoprice.com) Or you can buy an actual Wanhao. A quick google search should do you fine. Like I said, big community behind these printers, you'll find more than enough info.


---
**Jonathan Davis (Leo Lion)** *January 02, 2017 04:03*

**+Alex Hodge** Good to know as I own a cheep benbox engraving system that has given me all kinds of trouble from a company in china Called Gearbest.


---
**Alex Hodge** *January 02, 2017 04:09*

**+Jonathan Davis** I haven't heard of that one, but I can enthusiastically recommend this printer. Works very well out of the box after typical post shipping adjustments. With an all metal hotend upgrade and upgraded extruder gear from uncle chuck,  great printer. My main thing is that I'd love to have auto bed leveling and with the stock Melzi based board, that is very difficult. So I'm excited to see this upgrade post!


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 04:11*

The Cohesion3D boards have a servo header and extra endstop ports, we are going to play with hooking up a BLTouch.


---
**Jonathan Davis (Leo Lion)** *January 02, 2017 04:15*

**+Alex Hodge** I bet that would be nice, though i do like the diy route via a kit but then again i'd buy it off of amazon. though on my machine I have two things yet to be done:

1) Make a install guide for the  #Cohesion3D  Mini controller board.

2) Get my endstops installed.


---
**Alex Hodge** *January 02, 2017 17:23*

**+Ray Kholodovsky** You're reading my mind. I've got my bltouch on the desk next to my Monoprice i3. How are you planning on mounting it? I removed my stock part cooling fan and printed up a little temporary bracket to use as a proof of concept. I've also got a RAMPS sitting next to it as my original plan was to just use RAMPS but maybe I'll have to order another Mini. So I'll have one in my printer, one in my K40 and who knows, maybe I'll end up with one in my MPCNC as well.


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 17:34*

**+Alex Hodge** I'm waiting for my BLT to arrive in the next few days. I would have to say, just using the simplest plate mount possible. Using existing screw holes from the fan seems like the way to go for me.  There's a whole host of upgrades on thingiverse/ and listed Facebook group for the wanhao i3.  Have you looked at any of those?  


---
**Alex Hodge** *January 02, 2017 20:35*

**+Ray Kholodovsky** I've done the cobra style part cooling fan, the z brace with leveling feet, the bed thumbscrews and the alternative x axis bearing mounts to eliminate belt rub (I think this might have been factory eliminated in later versions than mine).

I also fitted a glass bed plate, a microswiss all metal hotend and upgraded the extruder gear. It was a good printer out of the box...but I can't resist mods/upgrades. I've got three things left on my list for it. adjustable tension for y axis belt, auto bed leveling of some kind and an enclosure for better ABS printing.


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 20:36*

And upgrade the board. Remember I got this machine on Friday and I only played with it that one day. I'm still brand new to this one. 


---
**Alex Hodge** *January 02, 2017 20:41*

**+Ray Kholodovsky** You'll be happy with it. Especially if you like to upgrade/customize stuff (which I bet you do lol). And, yes the board upgrade will come with the auto bed level job. One way or another. Honestly, if I could get my hands on one of these Mini boards quickly, I probably wouldn't bother with the RAMPS...But it's sitting right here. I'll probably end up doing the RAMPS and then switching to a Mini assuming I like the Mini in my K40. When the Mini finally shows up... :)


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:07*

Well this is the week that will determine all those things :) you're welcome to order a 2nd one, and it will ship with the original order, and I'll refund you the shipping charge from the 2nd order since I'm consolidating shipping. 

Things like building and wiring are a huge time suck for me - I pretty much blinked and all of Friday had passed.  Frankly I would have to gut the entire head to an E3D hotend before I felt comfortable with any machine for production purposes. I would also agree that the bed needs glass and other work done. 

I already have a steel i3 that I've upgraded, as well as 5 machines of my own design, and more that I want to build. So I'd rather leave the "advanced tinkering" to folks such as yourself and **+Wayne Moore**  while I focus on making upgrade packages to support more of you. 


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:09*

My machines: ![images/6fecd994398e1bcd8ff6f2efd08661c8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6fecd994398e1bcd8ff6f2efd08661c8.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:09*

![images/2483637f1f4746765dacf296ccafbaf5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2483637f1f4746765dacf296ccafbaf5.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:10*

![images/6d14421478e6b140f42ba5e274fa80cb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d14421478e6b140f42ba5e274fa80cb.jpeg)


---
**Alex Hodge** *January 02, 2017 21:14*

**+Ray Kholodovsky** You know, I tried out an E3D V6 and Titan extruder combo on this printer and I just couldn't come up with a good mount to fit it all and not lose a dramatic amount of build height. I'll end up using the V6/Titan with my MPCNC at some point probably. Though I think I recently saw a mount design on thingiverse that supported the E3D parts AND the bltouch. Might end up giving it a shot at some point. But really, the microswiss hotend has been killer. So I'm not sure I'll bother. Unless I end up finding a decent price on a pancake stepper to use with the Titan/V6. LOL! I can't resist!


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:16*

I lost a lot of height just installing a v6 on the other i3! :) I haven't bothered to shuffle around a mount. I figure the other printers you see are the ones to use for bigger stuff. 


---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2017 21:21*

I'm waiting for boro glass to arrive for this one and I'm looking at some form of triple extrusion for this centered around a chimera. ![images/2ef86b917c09975cc627a71e5b9938c1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ef86b917c09975cc627a71e5b9938c1.jpeg)


---
**Alex Hodge** *January 03, 2017 03:27*

Looks like some fun projects Ray. I'm excited to try out the Mini when it arrives!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/hFwNFyNtsGK) &mdash; content and formatting may not be reliable*
