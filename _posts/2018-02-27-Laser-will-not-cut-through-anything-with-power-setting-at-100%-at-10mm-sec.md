---
layout: post
title: "Laser will not cut through anything with power setting at 100% at 10mm/sec"
date: February 27, 2018 00:49
category: "C3D Mini Support"
author: "Trey Roudebush"
---
Laser will not cut through anything with power setting at 100% at 10mm/sec. 

Laser has been focused.

Noticed power variables of:

Power Scale ->  set at zero default

S Value -> set at zero defalut.



What are these values for and what should be the value for each?



Thanks,





**"Trey Roudebush"**

---
---
**Anthony Bolgar** *February 27, 2018 00:58*

What laser, and what software are you using?


---
**Trey Roudebush** *February 27, 2018 01:01*

K40 40w laser tube with lightburn.  When I test fire the laser from the control panel it cuts through.


---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2018 02:48*

Do you have an mA meter? 


---
**Trey Roudebush** *February 27, 2018 04:05*

Not currently installed


---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2018 04:06*

In the smoothie config.txt file there is a max power line that caps the power to 80% - you can find that (the value is 0.8 and change it to 1.0 ) - did we already discuss this? 


---
**Trey Roudebush** *February 27, 2018 04:08*

No, we didn't talk about this.  I will try this and then update you


---
**Trey Roudebush** *February 28, 2018 00:42*

What is the power scale setting and s value on the c3d board and what should they set to?


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 01:10*

Those are things that the software sets up/ varies in the job. So I'm not sure I understand the question. 


---
**Trey Roudebush** *February 28, 2018 01:12*

They are at 0 in my at default.  Still having cutting issues so thought they might be set wrong.


---
**Trey Roudebush** *February 28, 2018 01:49*

On my LCD my value for s always stays the same even if the job is running but the value it can be within changes.  I think this may be the problem![images/f0c3c4a5fdc9b50ab105bd95afdef7fa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f0c3c4a5fdc9b50ab105bd95afdef7fa.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 01:51*

Just send a line G1 X10 S1 F600 and see what you get 


---
**Trey Roudebush** *February 28, 2018 01:57*

When I sent it the s value changed to 100, but then went back to 0 on the next job and didn't change the cut at all


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 02:07*

I don't think you understand. The S tells it what level to fire at for that line. It's not making a change. We've just proved the board is configured properly. 



Now you need to set the power levels you want in the software. 


---
**Trey Roudebush** *February 28, 2018 02:10*

I have them set in the software when I ran a job with 100 power the s value still stays at 0


---
**LightBurn Software** *February 28, 2018 02:42*

Do you, by any chance, have the "Fire Laser" button enabled?


---
**Trey Roudebush** *February 28, 2018 02:43*

No, how do I enable it?


---
**LightBurn Software** *February 28, 2018 02:44*

Power scale set at zero?  No - set at 100.  It's a percentage value. If you set Power Scale on a shape to 0, you'll get no output.


---
**LightBurn Software** *February 28, 2018 02:45*

You don't want to enable it - it's dangerous as hell for a CO2 laser, but the level setting for it could also override your cut, which is why I asked.


---
**Trey Roudebush** *February 28, 2018 02:45*

Everything is set to max


---
**LightBurn Software** *February 28, 2018 02:46*

Can you post the .LBRN project file and your smoothie config file here?


---
**Trey Roudebush** *February 28, 2018 02:52*

[dropbox.com - config.txt](https://www.dropbox.com/s/ff4odyz8dw2o0al/config.txt?dl=0)




---
**Trey Roudebush** *February 28, 2018 02:53*

[dropbox.com - SampleCut.lbrn](https://www.dropbox.com/s/an4bjgc6a95m08b/SampleCut.lbrn?dl=0)


---
**LightBurn Software** *February 28, 2018 03:13*

This file is set to 1mm/sec, 100% power. It appears to be placed lower than the minimum point of the bed, which will possibly trip your limits or slam the head, but otherwise I see no issue there.  I moved it into an appropriate position and it cuts fine for me, so I have to assume it's a config issue.


---
**Trey Roudebush** *February 28, 2018 03:14*

It will cut through with these settings, but barely.


---
**LightBurn Software** *February 28, 2018 03:15*

Do you have the knob on the K40 turned down?


---
**Trey Roudebush** *February 28, 2018 03:16*

I have and LCD with buttons and they are turned to 99.99


---
**LightBurn Software** *February 28, 2018 03:19*

Is that milliamps or percent?  If it's milliamps you would've fried the tube.


---
**Trey Roudebush** *February 28, 2018 03:20*

It is a percentage![images/65f89222ede6d26c11bfcfc95987cca1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/65f89222ede6d26c11bfcfc95987cca1.jpeg)


---
**LightBurn Software** *February 28, 2018 03:30*

If you can get a meter, connect it between the firing end (low voltage end) of the tube and PSU. That will tell you if you're getting power to the tube. You could also try putting a scope or meter between the Smoothie and the PSU at the L-pin to see if there's a signal there. I don't have a GLCD and have no idea how the display there correlates to anything, and have never seen an issue like this before.  If your test-fire button works, that would seem to indicate not a tube or PSU issue.



When you typed the command Ray suggested above (G1 X10 S1 F600) did it cut anything?


---
**Trey Roudebush** *February 28, 2018 03:31*

The test fire buttons works great and cuts within seconds.  I have tried the command and it did nothing


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 03:39*

My reading was the G1 command did indeed show an S1 value on the GLCD, suggesting the board was configured properly. 


---
**LightBurn Software** *February 28, 2018 03:41*

Loose / unconnected L-pin ?


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 03:43*

**+Trey Roudebush** can I get a pic of the latest board wiring? 


---
**Trey Roudebush** *February 28, 2018 03:46*

![images/363cc941a5c567183b1fcb567e7b9a4b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/363cc941a5c567183b1fcb567e7b9a4b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 03:59*

The GLCD ribbons are in the way, I'd like to see the top edge with the power connectors please. 


---
**Trey Roudebush** *February 28, 2018 04:02*

![images/887610925c2db54312272a65dad72f3b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/887610925c2db54312272a65dad72f3b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 04:06*

So let me get this clear.  When you ran the G1 line, the head should hav moved.  Did the laser fire or did it not? 


---
**Trey Roudebush** *February 28, 2018 04:07*

It did move and fire, but did not cut through


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 04:08*

But it did fire. And the GLCD said S 1? 






---
**Trey Roudebush** *February 28, 2018 04:08*

Yes


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 04:13*

I maintain the board is operating properly.  Unless you can meter the the tube (and I'd really recommend installing an mA meter anyways) and show that it's a lower value in this G1 exercise when compared to the test fire button... 


---
**Trey Roudebush** *February 28, 2018 04:33*

I put in a amp meter and the test fire and laser job ran the same at 25 millaamps.  The problem is that is at 100 power and 1mm/sec and I am just starting to cut through.  Before this upgrade I was able to cut through at 30mm/sec with 40 power.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 04:35*

Maybe your tube isn't performing as well. 


---
**Trey Roudebush** *February 28, 2018 04:37*

How long do the tubes usually last.  I have had this for almost a year and I my used it for the first 3 months.  For the past 9 months is has been collecting dust.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 04:38*

I'd recommend posting to the main k40 group where Don or Hakan can better advise you on such matters. 


---
**Trey Roudebush** *February 28, 2018 18:53*

Now I am having a problem where my y axis will not home.  I have tested the switch to confirm it works and it does and the motor will move when I job it manually.  It was working fine one second and then it just stopped working.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 18:57*

Send M119 with and without the switch pressed to check if the board is reading it. 


---
**Trey Roudebush** *February 28, 2018 18:59*

With the button presses the y min value is 0 and without it pressed it is still 0


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 19:00*

Check your wiring? 


---
**Trey Roudebush** *February 28, 2018 19:05*

I checked the wiring with a multimeter with continuity and the wires are not broken the switch is working and the wires are in contact with the pins on the board because they are wrapped around them the ensure they are touching.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 19:08*

Disconnect the Y switch connector from the board and send M119 again. 


---
**Trey Roudebush** *February 28, 2018 19:09*

Still y min 0


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2018 19:35*

Alright, you can move it over to another port and just change the pin numbers in config.txt. For example one over to "Z Min" and switch the pins between beta min and gamma min. 


---
**Trey Roudebush** *February 28, 2018 19:45*

As  I was going to change the pin to the gamma pin I noticed there was a # in front of the beta min endstop.  I have no idea how it got there but that fixed the problem once I removed it.


---
*Imported from [Google+](https://plus.google.com/107573574842290750272/posts/97NoophBfUa) &mdash; content and formatting may not be reliable*
