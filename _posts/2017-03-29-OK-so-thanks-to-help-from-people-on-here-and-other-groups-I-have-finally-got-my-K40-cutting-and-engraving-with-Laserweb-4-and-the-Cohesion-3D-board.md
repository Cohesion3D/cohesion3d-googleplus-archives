---
layout: post
title: "OK, so thanks to help from people on here and other groups, I have finally got my K40 cutting and engraving with Laserweb 4 and the Cohesion 3D board"
date: March 29, 2017 15:43
category: "General Discussion"
author: "John McKeown"
---
OK, so thanks to help from people on here and other groups, I have finally got my K40 cutting and engraving with Laserweb 4 and the Cohesion 3D board. Very impressed so far although it's been bit of a stumble in the dark for a few weeks. I can see now how useful Laserweb will be once I've figured out the settings for different jobs etc. Thank you Ray for the board and help in setting it up and thanks to all those who gave me advice!





**"John McKeown"**

---
---
**Ariel Yahni (UniKpty)** *March 29, 2017 16:31*

**+John McKeown**​ please explain what was the most difficult part for future user reference 


---
**John McKeown** *March 29, 2017 16:45*

For me the most difficult part was finding files that would import into Laserweb. I downloaded Inkscape for the purpose but it didn't work for me. I spent quite some time trying to resolve that, unsuccessfully. Chuck Comito advised me to try Illustrator to export my SVGs and that worked perfectly. Since then it has just been a matter of looking back through previous and new posts and experimenting with which settings work best in Laserweb for certain jobs on my machine. 


---
**Alex Krause** *March 29, 2017 17:24*

Don't forget to share some awesome projects :)


---
**Don Kleinschnitz Jr.** *March 30, 2017 01:31*

**+John McKeown**  very strange.... I just started using inkscape for use with my converted K40 and I found it fit with LW perfectly...


---
**Ariel Yahni (UniKpty)** *March 30, 2017 01:35*

**+Don Kleinschnitz**​ I can testify under outh  that there are some issues


---
**Claudio Prezzi** *March 30, 2017 07:51*

One issue with Inkscape I know is, that if you just open a file from another software and then save it with Inkscape, you could get problems. Instead, you should create a new file with Inkscape, importe the other file and save it as SVG. That solved most of the problems for me. 


---
**John McKeown** *March 30, 2017 09:32*

I was making new and very simple files directly in Inkscape, just to test .. 




---
**Don Kleinschnitz Jr.** *March 30, 2017 20:23*

Can you post the file that did not work?


---
**John McKeown** *March 30, 2017 22:08*

I no longer have those Inkscape files, that was a couple of weeks ago. None of them worked though. They imported into Laserweb but never at the right position and when I tried to run them it sent my gantry grinding into the frame. That was the ongoing issue that I couldn't resolve. Illustrator works perfectly.




---
**John McKeown** *March 31, 2017 02:37*

I've tried to replicate the issue in this file

[drive.google.com - drawing.svg - Google Drive](https://drive.google.com/open?id=0B3ziyHsR2enNc2tVUXlyX1YzaDg)


---
**Don Kleinschnitz Jr.** *March 31, 2017 11:45*

**+John McKeown** is the problem you were having in LW is that it placed the objects below the pallet, that is what it did on mine.



There are also multiple errors in the system dialog:

Skip tag : unsupported: {"element":{},"name":"sodipodi:namedview","parent":{"element":{},"name":"svg","parent":null,"layer":null,"attrs":{"viewBox":[0,0,297,210],"xmlns:dc":"[http://purl.org/dc/elements/1.1/](http://purl.org/dc/elements/1.1/)",



We should probably move this to the LW forum. 


---
**John McKeown** *March 31, 2017 15:20*

Yes the problem was the placement of the file. I know there are other errors in the dialogue too, but Idk what they mean. I did put this on LW forum a while back.


---
**Don Kleinschnitz Jr.** *March 31, 2017 16:36*

**+John McKeown** I put it on the LW forum and tried to link you but there are two John McKeown's and it would not find you :(?


---
**Ariel Yahni (UniKpty)** *March 31, 2017 16:55*

**+Don Kleinschnitz**​ It's easier if you follow him


---
**John McKeown** *March 31, 2017 18:19*

Found it, thanks Don. I must have an impersonator there too :D


---
**Todd Fleming** *April 01, 2017 01:14*

Ignore the unsupported messages; they just show it ignoring Inkscape-specific headers. That file reproduces bugs 1 and 2 (not 3) in [https://github.com/LaserWeb/LaserWeb4/issues/236](https://github.com/LaserWeb/LaserWeb4/issues/236)


---
*Imported from [Google+](https://plus.google.com/108648609732667106559/posts/LV4n3hf7nRy) &mdash; content and formatting may not be reliable*
