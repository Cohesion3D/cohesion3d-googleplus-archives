---
layout: post
title: "I have a Zen Toolwork that I am trying to use as a cnc"
date: September 23, 2018 02:12
category: "C3D Remix Support"
author: "Joseph Haniszewski"
---
I have a Zen Toolwork that I am trying to use as a cnc.  The picture shows the wire setup.  

 

Any help would be appreciated.  



I have the following equipment



Comhesion 3d-remix board got in 2016 (does it need upgrade software)

HY-DIV268n-5a 

Power supply  

Reprapdiscount Full Graphic smart controller.

Stepping motor  - 1.8  1.5A



Using Mach3 







![images/69a784021104740d716a1d6e77b7b445.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69a784021104740d716a1d6e77b7b445.jpeg)



**"Joseph Haniszewski"**

---
---
**Jim Fong** *September 23, 2018 10:16*

Mach3 is a windows gcode controller software. It will not interface or communicate with the cohesion3d board running smoothieware. It is not compatible with it.  



See this list of smoothieware compatible software 



[smoothieware.org - software [Smoothieware]](http://smoothieware.org/software)






---
*Imported from [Google+](https://plus.google.com/109188818076216888793/posts/9tuHB2rTo73) &mdash; content and formatting may not be reliable*
