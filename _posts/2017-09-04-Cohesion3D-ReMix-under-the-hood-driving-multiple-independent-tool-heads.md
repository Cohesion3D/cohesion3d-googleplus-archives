---
layout: post
title: "Cohesion3D ReMix under the hood driving multiple independent tool heads"
date: September 04, 2017 22:13
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Cohesion3D ReMix under the hood driving multiple independent tool heads. I'm also glad this video is finally being shared, I've wanted to talk about it for quite some time. 



<b>Originally shared by René Jurack</b>





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Brad Hill** *September 04, 2017 23:17*

Any reason you chose a full purge tower over a quick purge into a waste area / wiper blade?


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2017 23:31*

Tagging **+René Jurack** to answer question.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 00:12*

I have been recommending that René do a purge bucket for a while now. ![images/af336f89a2f313b17172319e2a79949a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/af336f89a2f313b17172319e2a79949a.jpeg)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/45ZooAErc7W) &mdash; content and formatting may not be reliable*
