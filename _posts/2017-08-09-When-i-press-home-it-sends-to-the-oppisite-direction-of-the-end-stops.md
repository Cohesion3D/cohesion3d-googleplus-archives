---
layout: post
title: "When i press home it sends to the oppisite direction of the end stops?"
date: August 09, 2017 02:03
category: "K40 and other Lasers"
author: "William Kearns"
---
When i press home it sends to the oppisite direction of the end stops? Running windows ten lw4 and cohesion3d board.





**"William Kearns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 02:13*

If you send G28.2 and the head moves away from the endstops you need to power down and flip the motor cable plug orientation 180 degrees for whichever axis does that. 


---
**William Kearns** *August 09, 2017 02:19*

it now has stopped responding




---
**William Kearns** *August 09, 2017 02:32*

ok just needed to reboot. It moves pretty slow an inch or so in the correct directions. When i click on the jog button pointing right the x axis moves left and its reversed on the y axis as well but the G28.2  code sends it the correct direction




---
**William Kearns** *August 09, 2017 02:51*

ok get this message saying not supported by firmware when I try and set home?

![images/e1c9178602e60f612aed57455091f4ea.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e1c9178602e60f612aed57455091f4ea.jpeg)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/GAkhYUmrG46) &mdash; content and formatting may not be reliable*
