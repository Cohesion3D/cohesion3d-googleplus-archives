---
layout: post
title: "I went to test for my laser and it seems that everytime I that fire the laser or run a job the LCD buzzer goes off and the cohesion 3d board disconnects"
date: February 22, 2018 17:26
category: "C3D Mini Support"
author: "Trey Roudebush"
---
I went to test for my laser and it seems that everytime I that fire the laser or run a job the LCD buzzer goes off and the cohesion 3d board disconnects.  I have tried running it through the computer and LCD does not make a difference.  I don't know if the LCD buzzer should be going off, but I know that it shouldn't be disconnecting.  In addition if you had the test fire button to long or to many times the machine will go into it's boot sequence with the buzzer on and will stay there so the buzzer will just be going off with werid text on the LCD or the laser firmware info.  I have my wiring configuration below.  I am using new motors and no endstops so I do not need the ribbon cable from the stock k40.

![images/67f2018e1ecc5cd952ac3060b87e8a1b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/67f2018e1ecc5cd952ac3060b87e8a1b.jpeg)



**"Trey Roudebush"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 17:28*

You might have a Ground loop or issue with the laser power supply. 



The latter is well known, as the stock psu is inadequate and under powered, and many people get a separate 24v 6a power supply to power the board. 


---
**Trey Roudebush** *February 22, 2018 17:33*

How would I hook up the board to the k40 power supply and the new one?


---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 20:28*

Start here: [https://plus.google.com/u/0/117720577851752736927/posts/emzYWQAAXuW](https://plus.google.com/u/0/117720577851752736927/posts/emzYWQAAXuW)


---
**Trey Roudebush** *February 22, 2018 20:38*

Does the power supply running the board have to be 24v?  The board says 24v max.


---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 20:39*

The stock LPSU puts out 24v... so we'd want to replicate that.

The Mini can take 12-24v.  24v is better for motor performance and speed.


---
**Trey Roudebush** *February 22, 2018 20:51*

I disconnected the 24v pin from the k40 power supply and have the k40 running from a dedicated 12v 30a power supply.  I am still having all the same issues as stated in the first post?


---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 20:52*

Then you have a ground loop and you need to investigate ALL your wiring. 


---
**Trey Roudebush** *February 22, 2018 21:02*

I went through all of my wiring and was able to get it working.  Thank you


---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 21:03*

What was the issue?


---
**Trey Roudebush** *February 22, 2018 21:04*

I think it was the grounding because I made my main voltage going to the k40 psu ground to the case and then it worked.


---
**Trey Roudebush** *February 24, 2018 16:53*

I am still having it disconnect when I fire the laser.  The buzzer no longer goes off on the lcd, but it still disconnects


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 16:53*

Are you using the USB cable that came with the k40?


---
**Trey Roudebush** *February 24, 2018 16:54*

No, I am not


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 16:56*

What is the computer and operating system? 


---
**Trey Roudebush** *February 24, 2018 16:57*

I built my computer and I am running Windows 10


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 16:58*

Then you can tell me what the specs are - namely motherboard - and whether you are plugged into a USB 2 or 3 port. 


---
**Trey Roudebush** *February 24, 2018 17:01*

The motherboard port is a 3 I do not have any 2 and it is a ASRock ab350itx.  This also isn't a stock k40 I changed the mechanics to have a larger cut area and at one point had all the wires unplugged


---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2018 03:16*

One thing that may help with disconnects is removing the +5v feed from USB to the board. 



There is a jumper trace to be cut on the bottom of the board but there are traces close by that are easy to screw up so Brian has done this by clipping the 5v wire within a USB cable. 


---
**Trey Roudebush** *February 26, 2018 02:37*

I was able to get everything all up and working.  It was still another grounding issue.  I had to go and ground every circuit board to earth ground and that worked.  The only issue I am having now is I home the laser cutter to go to 0,0 the minimum, but in lightburn and on the lcd they both say 0,200?


---
*Imported from [Google+](https://plus.google.com/107573574842290750272/posts/BLyRRCg2N1s) &mdash; content and formatting may not be reliable*
