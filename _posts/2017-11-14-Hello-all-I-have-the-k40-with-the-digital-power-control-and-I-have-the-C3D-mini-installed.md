---
layout: post
title: "Hello all, I have the k40 with the digital power control and I have the C3D mini installed"
date: November 14, 2017 23:09
category: "C3D Mini Support"
author: "Alex Raguini"
---
Hello all,



I have the k40 with the digital power control and I have the C3D mini installed. I want to remove it and replace it with a dumb variable resister.  I've seen the wiring diagram here somewhere but can't seem to find it.  Could someone please point me in the right direction?



I think the digital control is the root of some of my "current" problems (pun intended).





**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 23:11*

Before you go changing more hardware... have you checked our pwm tuning guide? 



You haven't stated the actual issue, is it to do with a grayscale lack of shades? 



Otherwise, check **+Don Kleinschnitz** blog. 


---
**Alex Raguini** *November 14, 2017 23:19*

it is more basic than that.  I am having power issues with the laser in general.  I'm just working with what I have to get projects done.  My laser will not draw more than 5mw no matter what the digital power setting is.  I am trying to identify where the issue is.  I'm not really fond of that POS digital control anyway.  I do have an ammeter installed and am troubleshooting that as well.






---
**Alex Raguini** *November 14, 2017 23:28*

I know I may be confusing but I do some things in parallel but I do keep changes to a single change, test, confirm, and verify.  Then I change something else.  While I make changes, I continue working on projects as part of my testing.




---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 23:32*

Nothing wrong there... 

So when I'm working on my 3d printers, some of the time when trying to figure something out I will change a bunch of things at once to see if the problem got solved.  Then I can track down the culprit.   It works within my head.  But it can make helping someone else debug more difficult, and if I'm at the point where I am being helped by someone I will purposefully slow down to one thing at a time, report result, wait for feedback "ping pong". 



I'll do my best to keep up with you and if needed I will ask you to break down symptoms, attempted resolutions, and results for me. My brain likes small, bite sized details presented in a linear fashion :)


---
**Alex Raguini** *November 15, 2017 01:02*

Ray, Thank you for understanding.  I'm just a methodical person.  I found that more often than not, making too many changes at once often clouds the issue for me.



First, to get the image results I want, I feel I need control over the hardware settings, current, power, etc., for consistent, repeatable results.  I was not having the control I needed.  



When I first got my machine, it easily pegged the ammeter over 21ma.  Suddenly, it wouldn't go over 10ma and now it won't go over 5ma.  This begs the question of what is broken.  I assumed I overworked my power supply and tube as the power supply was screaming - before c3d - and I was getting inconsistent results.  Now, that could be differences in the material, fluctuations in power, dirty lens, dirty mirrors, or misalignment.



I cleaned and aligned everything as near perfect as a k40 will allow.  I got the same results.  I swapped the power supply.  The noise went away but I still had inconsistent results.  Today, I aced my laser tube with one from cloudray - it is really nice and supper easy to install.  Still, my power meter was telling me only 5ma draw.



Well, it obviously wasn't dirty, misaligned mirrors and lens.  It wasn't the power supply or the tube.  So, I began testing the PWM system and decided to junk the digital board since it is on my list anyway.  Found the diagram to replace it with a pot.  Still, no draw over 5ma even driving the PWM with 4.99V.



Last thing I replaced was the ammeter.  I had one to replace the analogue one that combines voltage and milliamps.  Put that in place and BINGO!  The current now reads over 20ma at 4v in the PWM.



Problem solved.  All these components were going into a replacement panel to hold the GLCD anyway.  All I was really waiting on was switches to control pump, lights, etc. Those should be here soon.  Unless, I can use 12V 6A switches to control power to the pump, lights, and air.



Yes, I know - I should have gone to the ammeter first. 



Apologies for the very long post.



Now, I have a stable, controllable machine I can continue testing image settings.


---
**Alex Raguini** *November 15, 2017 01:08*

There wouldn't happen to be a way to put voltage and current on the GLCD would there?  Wishful thinking.


---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2017 02:35*

The power level % of the firmware is displayed on the GLCD bottom line. The board doesn't know anything else. 


---
**Alex Raguini** *November 15, 2017 02:40*

Thanks


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/Jz94Nkru47w) &mdash; content and formatting may not be reliable*
