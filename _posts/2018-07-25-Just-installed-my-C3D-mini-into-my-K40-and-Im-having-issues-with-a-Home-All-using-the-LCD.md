---
layout: post
title: "Just installed my C3D mini into my K40 and I'm having issues with a Home-All using the LCD"
date: July 25, 2018 00:50
category: "C3D Mini Support"
author: "Keith Conger"
---
Just installed my C3D mini into my K40 and I'm having issues with a Home-All using the LCD.   It moves to the left rear and hits both end-stop switches but it never stops.  I've verified both register using the M119 command.  I've also tried reformatting the sd card and reinstalling the firmware and config.  Anyone have any ideas what I'm doing wrong?





**"Keith Conger"**

---
---
**Tech Bravo (Tech BravoTN)** *July 25, 2018 00:57*

[smoothieware.org - endstops [Smoothieware]](http://smoothieware.org/endstops#testing)


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 00:57*

It is trying to touch off a second time.  Give it a moment. 


---
**Keith Conger** *July 25, 2018 01:02*

**+Ray Kholodovsky** Ok, I just let it run.  When the first axis hits, it stops homing the other.  Eventually the motor stops and I get an alarm message on the LCD.


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 01:04*

A video of this would be helpful, if you could. 

Also pics of your wiring please, and the exact error on the LCD.  Are you just homing over LCD or are you connected to Lightburn?


---
**Tech Bravo (Tech BravoTN)** *July 25, 2018 01:05*

Homing aside, does it jog in the proper directions?


---
**Keith Conger** *July 25, 2018 01:05*

Using the LCD or gcode command.  I'll get pics and video now.


---
**Keith Conger** *July 25, 2018 01:08*

Wiring![images/255aff70f24f48527594e2f22d0905b9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/255aff70f24f48527594e2f22d0905b9.jpeg)


---
**Keith Conger** *July 25, 2018 01:09*

Error after motor cuts off![images/9411d876c2306a9c64846c3946a80910.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9411d876c2306a9c64846c3946a80910.jpeg)


---
**Keith Conger** *July 25, 2018 01:10*

Video

[photos.google.com - New video by Keith Conger](https://photos.app.goo.gl/aBoKyp3R2MGB8vBq7)


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 01:15*

I see.  Can you move the head to the other end, run a homing cycle again, and manually press the switches?  If it does not respond... kill power, no need to let it grind again. Let me know what happens. 


---
**Keith Conger** *July 25, 2018 01:21*

Jogging works


---
**Kelly Burns** *July 25, 2018 01:25*

Verify that each end stop registers one axis at a time. Make sure you don’thave them reversed.  It’s been a while since I ran Smoothie, but it doesn’t seem like it’s homing in the right order.


---
**Keith Conger** *July 25, 2018 01:29*

Here are some videos of me testing the end stops.  The behavior doesn't seem right

[photos.google.com - 3 new photos by Keith Conger](https://photos.app.goo.gl/HDRZazqYUNg4TiFh8)


---
**Tech Bravo (Tech BravoTN)** *July 25, 2018 01:29*

Easy way to verify stepper wiring and orientation (turn auto home off in lightburn) but maybe wait for rays input (i agree homing order (swapped x and y) maybe): [https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Kelly Burns** *July 25, 2018 01:49*

Homing switches are reversed.  You clicked the x switch and it stoped homing Y.   I say this also because you also said it jogged correctly.  


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 01:51*

That could certainly do it (although it's a fixed endstop connector that I have never seen deviation from), or he  plugged the X and Y motors in place of each other.  Verify jogging X moves the head left to right and Y moves it forward/ back. 


---
**Kelly Burns** *July 25, 2018 01:55*

I assumed when he said it jogged correctly, he verified direction and axis ;)  


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 02:01*

Every time I assume something, I get burned.  

Also, frame of reference? 


---
**Kelly Burns** *July 25, 2018 02:02*

Keith, what you are seeing in the longer video is the Seek, Feed, Bounce Off functions that are part of homing sequence. It Seeks the home witches, then seeks again at a lower rate to be more precise and then it bounces off a defined distance.  Either motors or switches are reversed.  Either way, you are not far off.



I’m curious, did you have I working with stock controller before installing the C3D?


---
**Keith Conger** *July 25, 2018 02:04*

Sorry guys, looks like that was it my motors were swapped.  


---
**Kelly Burns** *July 25, 2018 02:06*

**+Ray Kholodovsky** not sure what you mean by Frame of reference?.  IMO, it wasn’t a dangerous assumption since he was directly asked the jogging question ;)


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 02:07*

Great!

I think we need to start providing a cattle yoke style part to put on the 2 motor connectors to keep them in the proper sequence when plugging between board.


---
**Kelly Burns** *July 25, 2018 02:07*

Good news!


---
*Imported from [Google+](https://plus.google.com/+KeithConger/posts/VbgmsiDj76K) &mdash; content and formatting may not be reliable*
