---
layout: post
title: "Hi i have a new Scotle SL-460 50W (From ChinaCNCZone.com), and i want to upgrade my machine to use Cohesion3D and Lightburn Software, can i easy swap cable on this board?"
date: June 30, 2018 16:12
category: "K40 and other Lasers"
author: "Felipe Jos\u00e9 de Oliveira"
---
Hi i have a new Scotle SL-460 50W (From ChinaCNCZone.com), and i want to upgrade my machine to use Cohesion3D and Lightburn Software, can i easy swap cable on this board? (Mine dont use flat cable at all...)

Thanks for all

![images/e25e022055051679ed0f985385293bbd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e25e022055051679ed0f985385293bbd.jpeg)



**"Felipe Jos\u00e9 de Oliveira"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 16:14*

I think so.  Check the instructions at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) - there is the wiring without the flat cable and then you will have to cut the big power plug and put 24v, Gnd, and L to screw terminals on the C3D board. 


---
**Felipe José de Oliveira** *June 30, 2018 16:26*

Cool xD, and the laser fire is the LO from mine right?


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2018 16:28*

Yes, that is it.  I call it "L" a lot. ^^



What power supply is in your laser?  These are usually shit and cause problems with the board. I am getting high quality 24v 4a power bricks to power the C3D board (and thus the motors) with, expect to have them available in 2 weeks. 


---
**Felipe José de Oliveira** *July 02, 2018 14:11*

Cool, and i can add a relay module to control my air compressor on/off when the laser is on?


---
*Imported from [Google+](https://plus.google.com/110870325678440086664/posts/MZHyk5r9sh8) &mdash; content and formatting may not be reliable*
