---
layout: post
title: "Hey Guys, I just got my Cohesion3d board Now i bought a cheap k40 laser I went over the unit the unit itself the gantry was completely outta wack so i had to take the thing apart square the gantry which i did i then removed"
date: August 30, 2018 00:23
category: "C3D Mini Support"
author: "Tony Mondello"
---
Hey Guys,

  I just got my Cohesion3d board Now i bought a cheap k40 laser 

I went over the unit the unit itself the gantry was completely outta wack so i had to take the thing apart square the gantry which i did i then removed the useless clamp board and replaced with with my own bed I then ripped the Lousy horrible nano controller out of it and Replaced it with this beautiful control board and gli.. Well, To my amazement The controllers the motors asound beautiful compared to that lousy control board as well as the horrible software i received with it and hat stupid dongle which was worhtless. I am using Lightburn which took this unit to a whole new level the unit actually does what it is supposed to do and Engravings on plexi are beautiful.. The whole way the control board actually controls the laser and the Stepper motors sound so much better It is a compeltely different machine now..   I have one question. I have the original control board which is digital still hooked up to the unit i set it to 100.00% power I then adjust the power from the Lightburn software I am new to this and i want to make sure i am doing this right without bruning my house down lol. Any tips would be appreciated...





**"Tony Mondello"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 30, 2018 01:22*

I dislike the digital panel, it puts out less resolution for what I am about to describe. 



You do not want to set 100% as you will burn the tube out. You want to set the maximum power that you will need for a job, typically around 10-15mA max. Then LB and the board will control power % within that range. 


---
**Ray Kholodovsky (Cohesion3D)** *August 30, 2018 01:24*

I cannot make any guarantees your k40 will not catch fire all on its own. 



I would recommend reading our PSA in the install guide about insulating the board mounting from the laser chassis as we have been finding that the lpsu can put out a surge/ power spike through the machine chassis and fry the board. ![images/4c3e525835f18bbe4ca313583a04cdc4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4c3e525835f18bbe4ca313583a04cdc4.jpeg)


---
**Tony Mondello** *August 30, 2018 12:54*

**+Ray Kholodovsky** I mounted the board of course with lengthy standoffs so it's completely insulated from the frame I am quite new to this as I just got this thing and I'm not used to the terminologies being used the one thing that I don't understand is the original digital panel that's on that machine I don't know how to remove it and hook those wires to the board so I can get rid of that original panel I did a beautiful cut last night and the laser burn software the laser was firing at 16% and I mean it cut out really nice it did a beautiful job but now you got me a little frightened because I have that original panel at 100% And I got the laser burn software controlling the power that is where I'm stumped if you can give me any help I would greatly appreciate it thank you for your time




---
**Ray Kholodovsky (Cohesion3D)** *August 30, 2018 13:15*

Take a deep breath, it’s difficult for me to read a block of text without any punctuation. 

Are the standoffs nylon? That is what I am saying. Metal standoff’s will still conduct between the frame and the mounting holes on the board which are connected to the board ground.  That’s what I’m saying needs to be avoided. 


---
**Tony Mondello** *August 30, 2018 13:18*

**+Ray Kholodovsky** I am sorry my friend I am doing this voice to text because they are nylon standoffs I read in the instructions about that so I went out and bought one in nylon standoffs with one inch nylon screws so the board is completely insulated.


---
**Tony Mondello** *August 30, 2018 22:32*

As you can see the board is completely mounted on nylon right down to the screws

![images/a7b4c020f017d59ee430155330f118a6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a7b4c020f017d59ee430155330f118a6.jpeg)


---
**Tony Mondello** *August 30, 2018 22:34*

Mounted the fgsc to the top as well 

![images/c76725f4236779870512e603d66cb84f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c76725f4236779870512e603d66cb84f.jpeg)


---
**Tony Mondello** *August 30, 2018 22:36*

The question I have is this the original control panel don't do away with it and if so where would I hook these wires into the control board or lpsu?

![images/bb5ecd7d4e56000d46a419fc5b11beb1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bb5ecd7d4e56000d46a419fc5b11beb1.jpeg)


---
*Imported from [Google+](https://plus.google.com/104298619252152309699/posts/H6WseqSkurU) &mdash; content and formatting may not be reliable*
