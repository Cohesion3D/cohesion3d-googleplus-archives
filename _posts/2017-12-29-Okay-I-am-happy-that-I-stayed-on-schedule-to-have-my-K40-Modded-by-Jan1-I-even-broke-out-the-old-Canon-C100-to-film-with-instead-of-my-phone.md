---
layout: post
title: "Okay, I am happy that I stayed on schedule to have my K40 Modded by Jan.1 I even broke out the old Canon C100 to film with instead of my phone!"
date: December 29, 2017 01:34
category: "Show and Tell"
author: "Tech Bravo (Tech BravoTN)"
---
Okay, I am happy that I stayed on schedule to have my K40 Modded by Jan.1 I even broke out the old Canon C100 to film with instead of my phone! And my air pump is 950 GPH, not GPM LOL. I caught that after the fact. I am excited to push out some work and see what this thing will do! Thanks to Don, Frank, Ray, Cohesion3D, Lightburn Software, and everyone else who helped and followed me through this project! Until next time. Peace Out! 
{% include youtubePlayer.html id="6i-DkGoAL7g" %}
[https://youtu.be/6i-DkGoAL7g](https://youtu.be/6i-DkGoAL7g)





**"Tech Bravo (Tech BravoTN)"**

---


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/7pxModU4Duv) &mdash; content and formatting may not be reliable*
