---
layout: post
title: "Installed the C3D LaserBoard. It was easy"
date: February 02, 2019 07:14
category: "General Discussion"
author: "John Blair"
---
Installed the C3D LaserBoard. It was easy. The only snag (literally) was the cord for the end stops needed to be tugged a bit to actually reach the spot on the C3D board. Now with LightBurn and C3D I can directly control it from my Mac rather than the strange combination of a Windows PC with K40Whisper, Drop Box and Team viewer I was using and hour ago. The one question I have is all of the other boards have pin out diagrams (Mini/Remix) but no diagram for the Full Sized board. Any ETA or am I missing a link somewhere (along with the screw that is loose)?





**"John Blair"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2019 15:19*

At this stage, we still prefer to “spoon feed” the exact information that people require for their builds - hence no pinout diagram yet.



We’d be happy to answer any further questions at our [forum.cohesion3d.com](http://forum.cohesion3d.com) [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](http://forum.cohesion3d.com)


---
**John Blair** *February 03, 2019 08:44*

What is the timeline for getting a real meal instead of spoonfuls at a time?


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2019 06:15*

I have noted the request, and will work on it, but there are a number of other things that require my attention first. 



There are a number of application specific guides in our forum: 



[forum.cohesion3d.com - Latest Documentation topics - Cohesion3D Community](https://forum.cohesion3d.com/c/documentation)



[https://forum.cohesion3d.com/c/faq](https://forum.cohesion3d.com/c/faq)



If you wish to do something not covered in these, please post in the forums and we will gladly guide you.  




---
*Imported from [Google+](https://plus.google.com/100055041331503549813/posts/6Kvu7PAngZT) &mdash; content and formatting may not be reliable*
