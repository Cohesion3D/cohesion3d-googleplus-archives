---
layout: post
title: "I seem to have developed a problem getting the laser to fire and I'm hoping its me being simple"
date: June 28, 2017 11:44
category: "C3D Mini Support"
author: "Jeff Lamb"
---
I seem to have developed a problem getting the laser to fire and I'm hoping its me being simple.



I've had it all working on the C3D mini without problems, but I have just done some upgrades to help the reliability.  This includes an external 24v power supply.  I've connected it to the K40 power connector on the C3D  - disconnected the two 24v wires from the k40 psu but left the fire (L) and ground (G) connected .  Now laserweb won't fire the laser either during a job or via the test function.  The physical test button still works.  Am I missing something?  Do I need to use different connection points when using an external 24v psu?  Thanks.





**"Jeff Lamb"**

---
---
**Jeff Lamb** *June 28, 2017 11:48*

Just a side thought - does the K40 psu and 24v psu need to share a common low voltage side ground?




---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 13:30*

Yes they do. 



I would actually wire to the power screw terminals and not worry about the connector, but either way would be fine as long as the pins get matched up. 


---
**Don Kleinschnitz Jr.** *June 28, 2017 13:37*

**+Jeff Lamb** the "L" signal needs an associated ground that connects to the LPS. Above you said "....but left the fire (L) and ground (G) connected" .... if so that should work.



1. Check with a meter that the 24VDC supplys grnd, the grnd on the controller and the grnd on the LPS are connected.

2. Is the only thing connected on the LPS right hand DC connector now the "L" and ground?

3. Post picture of your supply showing the connectors.


---
**Jeff Lamb** *June 28, 2017 13:59*

It sounds like the problem is to do with the ground then so I'll re-wire when I'm back home (at my day job now).  I presume I connect the 24v ground and K40 ground together? Thanks all.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 14:01*

Yeah, you need to connect the 2 grounds to each other. Directly, not through the board please. 


---
**Jeff Lamb** *June 28, 2017 21:12*

Thanks very much guys. I got my wires crossed and had L and 5v still connected.  Connected k40psu ground to 24v ground and problem solved and I'm working again.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 21:14*

Glad you're up and running. Thank you very much for your support! 


---
**Don Kleinschnitz Jr.** *June 29, 2017 01:34*

**+Jeff Lamb** VICTORY again!


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2017 01:36*

I think we should make a wiring diagram for this. The goal is to get people to upgrade psu anyways. And it would help illustrate where the wires go to the board for the people with the 6 pin connector too. 


---
*Imported from [Google+](https://plus.google.com/100451757440368369818/posts/Jvx4M2uJ9sR) &mdash; content and formatting may not be reliable*
