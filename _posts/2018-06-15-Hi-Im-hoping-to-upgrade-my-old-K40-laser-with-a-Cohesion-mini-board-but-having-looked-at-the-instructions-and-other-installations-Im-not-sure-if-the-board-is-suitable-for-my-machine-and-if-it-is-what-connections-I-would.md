---
layout: post
title: "Hi, Im hoping to upgrade my old K40 laser with a Cohesion mini board but having looked at the instructions and other installations Im not sure if the board is suitable for my machine and if it is what connections I would"
date: June 15, 2018 19:57
category: "K40 and other Lasers"
author: "Tim Smith"
---
Hi, I’m hoping to upgrade my old K40 laser with a Cohesion mini board but having looked at the instructions and other installations I’m not sure if the board is suitable for my machine and if it is what connections I would have to make, Ive posted a picture of the current setup to see if anyone has done one like this and any  help would be much appreciated. Tim

![images/0d5718b19e54d74f6d09cdc85256a652.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d5718b19e54d74f6d09cdc85256a652.jpeg)



**"Tim Smith"**

---
---
**Joe Alexander** *June 15, 2018 21:29*

wow that's an old one, still has the big green ballast resistor. I would recommend buying a new laser psu for ~$70 on ebay(MYJG40W).


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2018 22:00*

This is a much older machine.   Even though the ribbon and motor cable should plug right over, I would expect a lot of tuning and tinkering to be required, and while we do try to be helpful, we can't hold your hand through that process.  Whether you're up for it, is up to you.



Joe's suggestion to replace the Laser PSU is a good one. But that's an involved process. 



I would suggest either not doing it, be prepared to fail and attack it with a "this is going to take a lot of tinkering and learning" mindset, or buying a new laser.  But the tinkering and learning still applies even if you buy a new laser. 


---
**Tim Smith** *June 15, 2018 23:13*

Thanks Joe and Ray for good honest advice, for now I’m not sure the work involved is worth the risk, but I might come back when time and need change. Tim  


---
*Imported from [Google+](https://plus.google.com/115593562492889961698/posts/Jq3oZ1QhnkF) &mdash; content and formatting may not be reliable*
