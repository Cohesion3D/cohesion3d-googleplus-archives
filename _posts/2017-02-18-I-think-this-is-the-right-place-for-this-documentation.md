---
layout: post
title: "I think this is the right place for this \"documentation\""
date: February 18, 2017 15:13
category: "C3D Remix Support"
author: "Marc Pentenrieder"
---
I think this is the right place for this "documentation".



<b>Originally shared by Marc Pentenrieder</b>



This is the right orientation of the LAN Module in the Remix Board😀

![images/6630c60a8efd9ba86d3e936ff5878703.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6630c60a8efd9ba86d3e936ff5878703.jpeg)



**"Marc Pentenrieder"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 15:15*

It is a good picture. I should put it on the product site. 


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/8mapkyrhfyU) &mdash; content and formatting may not be reliable*
