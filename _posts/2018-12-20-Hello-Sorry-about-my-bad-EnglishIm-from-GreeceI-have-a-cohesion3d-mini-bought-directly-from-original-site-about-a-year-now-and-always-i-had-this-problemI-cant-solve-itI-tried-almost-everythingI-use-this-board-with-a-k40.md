---
layout: post
title: "Hello, Sorry about my bad English.Im from Greece.I have a cohesion3d mini bought directly from original site about a year now and always i had this problem.I cant solve it.I tried almost everything.I use this board with a k40"
date: December 20, 2018 20:17
category: "C3D Mini Support"
author: "Mixalhs Mar"
---
Hello,

Sorry about my bad English.Im from Greece.I have a cohesion3d mini bought directly from original site about a year now and always i had this problem.I cant solve it.I tried almost everything.I use this board with a k40 laser with lightburn software.And i have a chiller for water cooling.When it turns on and off the laser stuck and writes busy(this happens also with the lights in the room).I have to replug usb to work again from the start.I have 3phase-line incoming power and the k40 is alone in one phase-line.I have no power drop in the line.I tried to put filters in chiller and k40 laser.But nothing.Im really stuck.Could you please help me?Any ideas?Thanks in advance.





**"Mixalhs Mar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 04:37*

You are having communications drop, it can be due to a variety of reasons. There is advise in this group about isolating the board, separate 24v psu, good quality usb cable, using the latest batch 3 firmware that does not mount the sd card. Many things to try. 


---
**Mixalhs Mar** *December 28, 2018 05:41*

I tried all of these but there was no fix.Better usb(also tried only data cable),Separate lines for 24v psu ,batch 3 firmware ,filters in all lines and many other things.But no solution.Thanks anyway.


---
*Imported from [Google+](https://plus.google.com/106802105401492847836/posts/FrhivLpVS9U) &mdash; content and formatting may not be reliable*
