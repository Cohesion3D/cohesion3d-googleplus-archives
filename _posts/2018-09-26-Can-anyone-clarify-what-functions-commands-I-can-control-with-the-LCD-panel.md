---
layout: post
title: "Can anyone clarify what functions/commands I can control with the LCD panel?"
date: September 26, 2018 12:56
category: "General Discussion"
author: "chris hopkins"
---
Can anyone clarify what functions/commands I can control with the LCD panel?  I just ordered a c3d mini, but not sure if I need the LCD and adapter.  I jave no use for it to tell me coordinates or job info, but if it can be used to start/stop or frame a job, then I'm in.  Any help on what it can do would be appreciated.  Thx





**"chris hopkins"**

---
---
**Joe Alexander** *September 26, 2018 14:36*

jog, test fire, run file from sd, etc.


---
**chris hopkins** *September 26, 2018 14:47*

Ok thanks, Joe!  Sounds like definitely worth a few extra bucks.  


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2018 15:17*

Yep, that, in addition to machine coordinates and status. 



If you want to add it, order separately the glcd and adapter, let me know, and I’ll refund the shipping afterwards. 


---
**chris hopkins** *September 26, 2018 15:37*

Will do right now.  Thanks!  What state are u in Ray?  Just wondering for ship time.  I have pretty much de commissioned my k40 for now.  The glitches w the stock moshi board have gotten worse for some reason lately, and I can't afford to ruin products every day.  Our bigger ruida 60w is working double time.


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2018 15:53*

NJ.  I have a backlog of orders, so things are going out right at the 3 business days mark. 



And you know you can use LightBurn with both your Ruida DSP (instead of RDWorks) and your C3D, yeah?  



You  just need the $80 DSP version of LB. 


---
**chris hopkins** *September 26, 2018 16:00*

Ok, no problem. Just send as soon as u can.



Yep, already use lightburn!  



I have a sperate power supply for the board, a good USB cable, and nylon mounts btw.  I am ready!


---
**Ray Kholodovsky (Cohesion3D)** *September 27, 2018 18:53*

It's shipped and I just issued a refund for $4.58 second shipping charge. Keep an eye on your card for that **+chris hopkins** 


---
**chris hopkins** *September 27, 2018 19:22*

Thanks Ray!!  I'm looking forward to recieving it and getting it up and running.  Planning to add another k40 or 2 to my arsenal soon, im sure u will be hearing from again!



Have a great weekend & thanks again.


---
*Imported from [Google+](https://plus.google.com/117993478615673588498/posts/7Y5SChfiWT7) &mdash; content and formatting may not be reliable*
