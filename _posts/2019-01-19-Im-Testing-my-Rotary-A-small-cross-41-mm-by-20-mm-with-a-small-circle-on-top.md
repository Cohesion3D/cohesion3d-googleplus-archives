---
layout: post
title: "I'm Testing my Rotary A small cross 41 mm by 20 mm with a small circle on top"
date: January 19, 2019 15:05
category: "FirmWare and Config."
author: "Phillip Bell"
---
I'm Testing my Rotary

A small cross 41 mm by 20 mm with a small circle on top. The x axis line of 41 mm has a wiggle on the top/left end,  the circle is vastly elongated and the 20 mm Y line goes almost around the 50.8 mm diameter oak dowel. Speed seems excessive and I have it set at 95. 

Any idea what I should do to fix this?



![images/d21614397e2a0515c0937f18e0da8e1d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d21614397e2a0515c0937f18e0da8e1d.jpeg)
![images/4b694471a051535085f08bd123fe641a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b694471a051535085f08bd123fe641a.jpeg)
![images/4c6a885ba0b2eecffb8612cac1e28c2b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4c6a885ba0b2eecffb8612cac1e28c2b.jpeg)
![images/9583750563fa5bc2ef1c6b15a0d6def7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9583750563fa5bc2ef1c6b15a0d6def7.jpeg)
![images/2071af2a8165b1884f7d018d0d94fc16.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2071af2a8165b1884f7d018d0d94fc16.jpeg)
![images/26273ea8ff4945725565824e85771a86.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26273ea8ff4945725565824e85771a86.jpeg)
![images/a86239f261d7ae2d057113cc98ccaf3d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a86239f261d7ae2d057113cc98ccaf3d.jpeg)

**"Phillip Bell"**

---


---
*Imported from [Google+](https://plus.google.com/110671692842123504744/posts/TZnPXheXuBo) &mdash; content and formatting may not be reliable*
