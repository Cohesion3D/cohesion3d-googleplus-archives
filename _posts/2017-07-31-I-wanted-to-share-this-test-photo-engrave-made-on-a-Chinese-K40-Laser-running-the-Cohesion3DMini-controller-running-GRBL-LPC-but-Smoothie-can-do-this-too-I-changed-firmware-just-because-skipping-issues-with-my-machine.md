---
layout: post
title: "I wanted to share this test photo engrave made on a Chinese K40 Laser running the Cohesion3DMini controller (running GRBL-LPC, but Smoothie can do this too, I changed firmware just because skipping issues with my machine)"
date: July 31, 2017 05:27
category: "Show and Tell"
author: "John Milleker Jr."
---
I wanted to share this test photo engrave made on a Chinese K40 Laser running the Cohesion3DMini controller (running GRBL-LPC, but Smoothie can do this too, I changed firmware just because skipping issues with my machine). 3mm Baltic Birch plywood, two passes. Lightobjects head with air assist. LaserWeb4 on a Raspberry Pi 3.



I think I'm darned near 'there'. This is as dark as I can get birch without really tanking my speed. This is two passes at 150mm/s.



![images/baf44317a7f3b0a1d20f3d83276b943f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/baf44317a7f3b0a1d20f3d83276b943f.jpeg)
![images/6284772cc7fdb6bbedeb61b85515bb3c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6284772cc7fdb6bbedeb61b85515bb3c.jpeg)

**"John Milleker Jr."**

---
---
**Marc Miller** *July 31, 2017 05:29*

That looks super, John.


---
**Jorge Robles** *July 31, 2017 06:15*

Amazing


---
**Don Kleinschnitz Jr.** *July 31, 2017 13:05*

#K40Engraving

That may be the best balanced engraving I have seen from a K40. What was power setting pls.


---
**John Milleker Jr.** *July 31, 2017 18:43*

Thanks guys!



**+Don Kleinschnitz** - I have my K40 set so that 15ma is 100%. The power setting for the engraving was 7% at a speed of about 150mm/s, two passes. The second pass is a separate pass with tonality adjustments.




---
**Marc Miller** *July 31, 2017 20:24*

**+John Milleker Jr.** For the second pass, is it run right after the first, or are you looking at the first pass results and making adjustments based on how that looks?  What sort of adjustments for second pass?


---
**John Milleker Jr.** *July 31, 2017 20:27*

**+Marc Miller** - I'm running it directly after the first pass, a separate file all together that is extremely high contrast so that primarily the shadows are re-burning.




---
**Marc Miller** *July 31, 2017 20:31*

Ok, that makes sense.  Thank you for the info!


---
**Don Kleinschnitz Jr.** *August 01, 2017 14:09*

**+John Milleker Jr.** I am certain that the community would value a "how to" video or document on your process :)!


---
**John Milleker Jr.** *August 01, 2017 15:11*

Thanks **+Don Kleinschnitz** - I thought about it. I'm trying to get out of the clutches of PhotoGrav at the moment. I have a friend that will convert the occasional file for me but all it's doing is adjusting levels, sharpening and applying a Floyd-steinberg dither. Not too hard, sure - PhotoGrav is worth the trouble, but at the moment I don't have the funds to spend on it.



I'm close, using Photoshop and Gimp. Trying to make it all Gimp workflow. Either way you need Gimp for the Floyd-Steinberg.






---
**Jorge Robles** *August 01, 2017 15:29*

Laserweb has Floyd Steinberg filter embedded on raster operations :)


---
**John Milleker Jr.** *August 01, 2017 15:34*

**+Jorge Robles** - That's good to know! I'm trying to stay away from letting Laserweb do any processing though at the moment. Any time I send it a graphic 5x7" or higher it crashes on the gcode generation. My 'fix' until the developers fix whatever that's causing it is splitting my graphic into smaller sizes. When I do that I worry about how the edges of my burns may look if they were not processed as one continuous image.




---
**Jorge Robles** *August 01, 2017 15:37*

I'm one of them :) open an issue on github attaching a problematic file and will take a look. Obviously, LW is not photoshop and is not optimized for hires graphics but will be good to know what the problem is


---
**John Milleker Jr.** *August 01, 2017 15:42*

**+Jorge Robles** - Nice to meet you! It doesn't seem to be graphic related,, it seems to be a programming limitation somewhere. Lines of gcode maybe. I'm feeding it 300dpi graphics and expecting 0.07mm diameter. Crashes the web interface on a RPi, Win8 or Win10 on a beefy new i7. There's another user I met on the LaserWeb Google+ group that had the same issue. I'll get with him and see if we can open up a GitHub request. Thanks!




---
**Jorge Robles** *August 01, 2017 15:43*

Thank you! 


---
**Tim Anderson** *August 04, 2017 09:16*

**+John Milleker Jr.** When you say this was at 7% what do you mean? My Laserweb has a from and too power, was this 0% - 7%?


---
**John Milleker Jr.** *August 04, 2017 15:36*

**+Tim Anderson** - Yes sir, 0% minimum, 7% maximum. I keep wanting to test using 7% as minimum and maximum as I see many others, but it's just been my normal setting.




---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/bMMAFxpVX78) &mdash; content and formatting may not be reliable*
