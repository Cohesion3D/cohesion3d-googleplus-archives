---
layout: post
title: "Well, I've got my z table built and mostly working"
date: October 20, 2018 15:37
category: "FirmWare and Config."
author: "Brent Dowell"
---
Well, I've got my z table built and mostly working.  I do not have a z limit switch and the version of grbl I'm using is not configured for a z endstop.



The only problem I'm having is that after startup  I'm not able to to move the table down.  Grbl takes the startup position as 0 and wont let me move the z table into negative territory.



Is there a specific grbl command I can use to allow the z to go negative or to set the machine 0 for the z axis?



Thanks in advance for any help.



(Obligatory picture of Z-table)

![images/973f441cf0b208a477eceeb0e784504a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/973f441cf0b208a477eceeb0e784504a.jpeg)



**"Brent Dowell"**

---
---
**Brent Dowell** *October 20, 2018 15:48*

Never fails.  Ask a question in public and I'll find an answer shortly after.  $20=0 will disable the soft limits, and lets me go negative.  Not sure if that's the absolute right answer, but it seems to work.


---
**Jim Fong** *October 20, 2018 19:48*

[dropbox.com - firmware grbl-lpc c3dmini 4axis with XYZ homing.bin](https://www.dropbox.com/s/zuhn3varf6zde99/firmware%20grbl-lpc%20c3dmini%204axis%20with%20XYZ%20homing.bin?dl=0)


---
**Jim Fong** *October 20, 2018 19:49*

Yep turn off soft limits or install z switch and use this version of Grbl. 


---
**Brent Dowell** *October 20, 2018 20:51*

Nice.  Thanks Jim.  Adding the homing switch seems like a good idea.




---
**Joe Alexander** *October 21, 2018 06:31*

nice build on the z table! did you document it? looking to add one myself and been tossing around a few of the ideas found online.


---
**Brent Dowell** *October 21, 2018 18:24*

Thanks Joe, It's pretty basic, copied design from others I saw and modified a bit.



[thingiverse.com - K40 Z Bed by kingbubbatruck](https://www.thingiverse.com/thing:3158394)




---
**Brent Dowell** *October 31, 2018 18:05*

I got it working pretty well with smoothie last night.  Set the switch on the bottom to be z-max and had to reverse the motor plug so it would go down when I gave it the $HZ command.



But now I'm able to home it and then tell it to go to 50.8mm which is the focus point for this lense.  



On to getting grbl to work.  I do have it working with grbl, except for the whole limit switch/homing bit.  I think I just need to play around with the grbl $ settings a bit.  The bed starts to move at startup, but then I get an alarm 8 right away.  I saw some other posts on that so I know there's some info out there.


---
*Imported from [Google+](https://plus.google.com/117727970671016840575/posts/LEsCoaKw7pz) &mdash; content and formatting may not be reliable*
