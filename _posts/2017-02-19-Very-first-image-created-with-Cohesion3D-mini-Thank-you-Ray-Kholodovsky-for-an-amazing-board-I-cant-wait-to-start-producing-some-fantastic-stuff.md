---
layout: post
title: "Very first image created with Cohesion3D mini, Thank you Ray Kholodovsky for an amazing board I can't wait to start producing some fantastic stuff"
date: February 19, 2017 21:02
category: "Show and Tell"
author: "Andy Shilling"
---
Very first image created with Cohesion3D mini, Thank you **+Ray Kholodovsky** for an amazing board I can't wait to start producing some fantastic stuff. Also big thanks to **+Don Kleinschnitz** for his input for every user on here with his simple to understand builders guide.

![images/e9715b44549b502b174ea88babee2ced.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e9715b44549b502b174ea88babee2ced.jpeg)



**"Andy Shilling"**

---
---
**Kostas Filosofou** *February 19, 2017 21:29*

**+Andy Shilling**​ wow amazing! How did you do that? Can you share your settings? I am struggling for over a week or so to make a raster image with no luck..


---
**Andy Shilling** *February 19, 2017 21:34*

All settings were standard as yet, mA set around 10 via the pot. The image was just greyscaled in Photoshop and loaded in.


---
**Kostas Filosofou** *February 19, 2017 22:04*

**+Andy Shilling** I'll try to make a photo grayscale in Photoshop also .. I was trying only grayscale images from the web but I only managed to burn the wood not to engrave...


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:25*

**+Andy Shilling** what was the speed and the PWM period please.


---
**Andy Shilling** *February 19, 2017 22:29*

**+Don Kleinschnitz**​ I'll have to let you know tomorrow as workshop is locked up now but I think pwm was 20 and I hadn't changed the does from standard install.


---
**Don Kleinschnitz Jr.** *February 19, 2017 22:32*

**+Andy Shilling** 200us will be much better. I just ran into some data that suggests 300-400 might even be the best.


---
**Andy Shilling** *February 19, 2017 22:35*

Sorry that should be speed from standard install. 


---
**Anthony Bolgar** *February 20, 2017 01:36*

You have just scratched the surface. You should see how detailed photo engraving can be if you take a look at some of the work **+Alex Krause** has created (Do a search through the K40 and LaserWeb communities)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/ByK9y5QL6Sq) &mdash; content and formatting may not be reliable*
