---
layout: post
title: "I just went to Pennsylvania and back"
date: December 22, 2016 06:21
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I just went to Pennsylvania and back. Wayne, a member of the K40 g+ community, graciously allowed me into his shop and we verified that the ribbon cable on the production mini boards works great. 

With that developmental milestone taken care of, the assembler has been notified and will begin producing the mini batch now. 

Stay tuned for the next updates. 



Cool dude - thanks Wayne! 

![images/480f02aae0d9fcd63337c905ac9c51c4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/480f02aae0d9fcd63337c905ac9c51c4.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Richard Vowles** *December 22, 2016 08:36*

That's dedication that is.


---
**Stephane Buisson** *December 22, 2016 08:59*

"With that developmental milestone taken care of, the assembler has been notified and will begin producing the mini batch now. "



Good news ;-))


---
**Andy Shilling** *December 23, 2016 20:27*

Time to start the full upgrade then.  New parts all sat here waiting. Well done **+Ray Kholodovsky**​


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 20:28*

Still a few weeks before shipping bud, but we're ready to hit the ground running as soon as the boards are here.

Happy holidays!


---
**Andy Shilling** *December 23, 2016 20:56*

You too pal, enjoy the break.


---
**Bryan Hepworth** *December 26, 2016 14:35*

I'm good to go with all my parts and that rare commodity - time!




---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/UE3949g17vU) &mdash; content and formatting may not be reliable*
