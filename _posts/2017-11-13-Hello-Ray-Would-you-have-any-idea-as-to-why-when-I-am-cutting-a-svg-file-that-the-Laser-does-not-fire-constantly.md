---
layout: post
title: "Hello Ray, Would you have any idea as to why, when I am cutting a .svg file that the Laser does not fire constantly?"
date: November 13, 2017 06:49
category: "C3D Mini Support"
author: "Warren Eames"
---
Hello Ray,

Would you have any idea as to why, when I am cutting a .svg file that the Laser does not fire constantly? It does cut but it is firing intermittently.  If this is in this forum elsewhere please direct me.

Kind Regards





**"Warren Eames"**

---
---
**Kostas Filosofou** *November 13, 2017 07:22*

Where did you generate the .SVG file? Is all the path joined?


---
**Warren Eames** *November 13, 2017 08:34*

I did it in Corel 12. In the program it is closed as I cut the exact same file on another Laser without any problems.


---
**Warren Eames** *November 13, 2017 08:43*

Also I can not get it to cut from the inside out. It cuts from the outside first.


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 17:33*

Is it a constant power cut? 

What is your pot set at? 



You made the gcode in Corel? 

Cut order and other things will be entirely dependent on how that gCode was generated. 



LaserWeb, and LightBurn when it comes out soon, allow you to import your design and define exactly how and in what order to do things. 


---
**Warren Eames** *November 14, 2017 10:25*

Hello Ray,

I am sure the file problem is in Corel 12, as when I use a .svg file from Corel x3 it works OK. 

I use Cohesion #D & Laserweb. 

I am still having problems trying to cut from the inside out.

I have tried: Laser Cut, LaserCut Inside, Laser Cut Outside. 

I would have tried more only I keep getting the "Code 45" error on the comm. port & as yet I have not been able to fix it.


---
*Imported from [Google+](https://plus.google.com/114364914232984549844/posts/g4zs9NdcLQn) &mdash; content and formatting may not be reliable*
