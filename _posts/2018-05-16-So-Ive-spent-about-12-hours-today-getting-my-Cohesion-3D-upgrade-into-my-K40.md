---
layout: post
title: "So I've spent about 12 hours today getting my Cohesion 3D upgrade into my K40"
date: May 16, 2018 01:38
category: "C3D Mini Support"
author: "Bryan Wilson"
---
So I've spent about 12 hours today getting my Cohesion 3D upgrade into my K40. It homes fine and runs fine until the laser fires. Once it fires LightBurn disconnects and everything stops. This also happens when I test fire the laser. So after reading all of the posts here I installed a second 24Vdc Power Supply. I ran 24V and Gnd into the screw terminals on the C3D board and disconnected the 24V from the stock PSU. Still exact same same result.



Laser test fires OK but it always disconnects me from the PC. I tried 2 different USB cables and USB 2.0 and 3.0 ports on the PC.



So I know my connections are good to the C3D because if I leave the power off the laser it will air print the svg just fine.





**"Bryan Wilson"**

---
---
**Bryan Wilson** *May 16, 2018 11:13*

These wires are not permanent If i leave the extra PSU I'll trim them better.

![images/75a2812f0eda34ed974a807a666c47cf.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/75a2812f0eda34ed974a807a666c47cf.png)


---
**Bryan Wilson** *May 16, 2018 11:14*

![images/b1eab68b5d6756fb8caa69b78fc61fd1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b1eab68b5d6756fb8caa69b78fc61fd1.png)


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2018 13:21*

Is your k40 chassis grounded? 


---
**Bryan Wilson** *May 16, 2018 14:09*

It is grounded to the 3 prong ground which is plugged into the wall. Do I need to do a different ground? I checked it with the ground on the board and it reads continuity to the existing chassis ground.


---
**Bryan Wilson** *May 16, 2018 14:20*

However for some reason it works this morning. Did not change anything. 


---
*Imported from [Google+](https://plus.google.com/107164419107206640356/posts/Tehm7yQWLAu) &mdash; content and formatting may not be reliable*
