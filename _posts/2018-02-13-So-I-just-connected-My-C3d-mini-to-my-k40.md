---
layout: post
title: "So I just connected My C3d mini to my k40"
date: February 13, 2018 21:54
category: "C3D Mini Support"
author: "R D H"
---
So I just connected My C3d mini to my k40. When I first got the machine it had the red moshi board. The x and y axis both moved when I turned on the power. My machine doesn't do that with the mini. What am I missing. I hooked it up as described and all the leds light up.


**Video content missing for image https://lh3.googleusercontent.com/-xf8pxbfG71Y/WoNeeJzSByI/AAAAAAAAKxQ/0AIO15aXMo4gvizZo8H3hKxK02Ans372ACJoC/s0/20180213_144917.mp4**
![images/e52076c7610aaaeadb5596d6d8e9e20f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e52076c7610aaaeadb5596d6d8e9e20f.jpeg)



**"R D H"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 21:56*

The Mini won't home the machine when you turn it on, by default.

When you connect to Lightburn you can configure it to home itself then. 


---
**R D H** *February 13, 2018 21:57*

ok thank you, So it works more like my 3d printer. Used to Duet first time smoothie user. 




---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 21:58*

Indeed.  I can direct you to make a on_boot.gcode file on the memory card with G28.2 as the contents and then it will home on power up, but I consider unintended motion a safety concern and decided not to supply it that way by default. 


---
**R D H** *February 13, 2018 22:06*

Not a problem that is a good thing because movements like that are bound to bite someone. I just haven't had chance to connect to my laptop yet. I bought it used with known failure. I basically have a shell right know and have upgrades coming for it. getting the board was step one. have a 50w power supply and tube on the way and a new POT coming as well. Hope the POT is just bad as I get intermittent power to the tube but it doesn't matter what position on the I have the POT on it gives me enough power to only light up the tube for a very short time. I have cleaned the POT wipers and can get a enough laser time to burn paper but thats it and if the POT works I have a second laser set up just waiting to be built.


---
*Imported from [Google+](https://plus.google.com/105743491280620332486/posts/BZA8nEWMfzS) &mdash; content and formatting may not be reliable*
