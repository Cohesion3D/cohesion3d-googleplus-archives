---
layout: post
title: "I am in the process of setting up motorized z axis and rotary on cohesion3d for k40, when I look at the config file it mentions alpha beta and gamma settings which seem to correspond to x, y, and z axes but no settings for"
date: June 25, 2017 21:33
category: "FirmWare and Config."
author: "Kevin Lease"
---
I am in the process of setting up motorized z axis and rotary on cohesion3d for k40, when I look at the config file it mentions alpha beta and gamma settings which seem to correspond to x, y, and z axes but no settings for 4th (rotary) axis there, can anyone advise on this?

Thank you









**"Kevin Lease"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 25, 2017 23:56*

Can you search this group for rotary/ find **+Jim Fong** posts. He has a YouTube video with lots of stuff in the description. We have to compile some info better. 


---
**Jim Fong** *June 26, 2017 00:13*


{% include youtubePlayer.html id="UoGlDKjPGSE" %}
[https://youtu.be/UoGlDKjPGSE](https://youtu.be/UoGlDKjPGSE)



This is only for Mini running. Smoothie. Links in the description. 


---
**Kevin Lease** *June 26, 2017 03:25*

Ok, thank you I will take a look


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/Cfe7SMppRsr) &mdash; content and formatting may not be reliable*
