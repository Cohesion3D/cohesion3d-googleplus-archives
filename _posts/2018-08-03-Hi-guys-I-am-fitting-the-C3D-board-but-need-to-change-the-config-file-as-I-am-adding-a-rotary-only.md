---
layout: post
title: "Hi guys I am fitting the C3D board, but need to change the config file as I am adding a rotary only"
date: August 03, 2018 19:13
category: "General Discussion"
author: "John Camfield"
---
Hi guys    I am fitting the C3D board, but need to change the config file as I am adding a rotary only. tried to format the mini SD card. but says write protected. how can a format the mini SD card?







**"John Camfield"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 03, 2018 19:30*

Slide the write protect switch on your reader to the not-locked position?


---
**Tech Bravo (Tech BravoTN)** *August 03, 2018 19:31*

Are you using a microSD to SD adapter? If so there is a little switch on the side. They sometimes snag when being inserted and get set to lock. 


---
**John Camfield** *August 03, 2018 19:40*

Yes, checked the little switch on the side was set to unlock, but still says write protected?



Sent from Mail for Windows 10


---
**John Camfield** *August 03, 2018 19:41*

Tried that, but still will not let me format it.





Sent from Mail for Windows 10


---
**Tech Bravo (Tech BravoTN)** *August 03, 2018 20:07*

1.go to Start menu – Run – diskmgmt.msc, Enter

2.Navigate to the context menu of the partition that needs formatting

3.Click “Delete volume” and confirm your choice

[4.In](http://4.In) the context menu click “New volume”

5.Choose primary partition

6.Leave default parameters




---
**Tech Bravo (Tech BravoTN)** *August 03, 2018 20:08*

also, in an earlier post you said 3 files. there are only 2. the config.txt and the firmware.bin. i am updating the firmware file on the step by step page i linked for you. it will be about 20 minutes. you should go get fresh files in about 30 minutes :)




---
**John Camfield** *August 03, 2018 20:10*

Ok will wait for awhile





Sent from Mail for Windows 10


---
**Tech Bravo (Tech BravoTN)** *August 03, 2018 20:12*

the new firmware is uploaded. here is the page: [https://www.lasergods.com/knowledgebase/faq/configuring-4-axis-firmware-config-file-on-the-cohesion3d-mini/](https://www.lasergods.com/knowledgebase/faq/configuring-4-axis-firmware-config-file-on-the-cohesion3d-mini/)


---
**John Camfield** *August 04, 2018 10:00*

Sent from Mail for Windows 10

Hi downloaded the files, is this correct please.


---
*Imported from [Google+](https://plus.google.com/103234827681964755838/posts/iM3QL6xtmw7) &mdash; content and formatting may not be reliable*
