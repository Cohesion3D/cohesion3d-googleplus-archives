---
layout: post
title: "So I am trying to commission a delta printer on the cohesoin 3d mini"
date: October 28, 2018 23:29
category: "C3D Mini Support"
author: "Family First"
---
So I am trying to commission a delta printer on the cohesoin 3d mini.  The endstops are very problematic. 

 Here is the code, it worked the first time and then after that it did not. 



Anyhow the min endstop is at the top of the printer.  I have the unit wired from the signal to ground so it is normally closed.  I put the limits in because the instructions said it might help, when I do a homing on the unit the printer acts as though it has not tripped the switch.  However when I do an M119 the xyz all sho as 0, when i turn the unit off after it plows through the endstop the M119 shows 1s all the way across.  And so, the doggone machine is registering that the endstop is switched.  ONCE, on this configuration the unit worked on all three axis, each subsequent homing results in the unit ignoring the signal.  So yes I uncommented endstop_debounce_count                       100 it did not work.  



to review M119 is true to the condition of the endstop, this configuration worked once (I guess anything can) and now it ignores the signal.  





 alpha_min_endstop                            270               #



alpha_max_endstop                            1.24           # add ! to invert 

alpha_homing_direction                       home_to_max      # Home up

alpha_max                                    nc                #



beta_min_endstop                             270              #

beta_max_endstop                             1.26           #

beta_homing_direction                        home_to_max      #

beta_max                                     nc                #



gamma_min_endstop                            270               #

gamma_max_endstop                            1.28v           #

gamma_homing_direction                       home_to_max      #

gamma_max                                    nc             #







**"Family First"**

---
---
**Anthony Bolgar** *October 28, 2018 23:44*

You might have better luck asking this in the smoothieware group, as there are more printer guys in that group. Or the Google+ 3D printing group.




---
**Douglas Pearless** *October 29, 2018 09:31*

**+Anthony Bolgar** 

I think you are missing the pull-up definition on the pin, plus the turning on of the end stop module.



The pull-up allows the pin to go high when the switch activates ie goes open circuit, rather than gradually float to 1.



alpha_max_endstop                            1.24^!       # add ! to invert 



The ^ pulls the pin high and the ! Inverts it so it is 1 when the switch is closed on 0 when it is open.  You also need to enable the endstop module:



endstops_enable true



Check out [smoothieware.org - endstops [Smoothieware]](http://smoothieware.org/endstops)



Cheers

Douglas


---
**Joe Alexander** *October 29, 2018 14:23*

correct me if im wrong but its set for alpha home to max, on pin 1.24 but no distance set in the alpha max? wouldn't that be where your 270 figure goes rather than the min endstop spot? i would also experiment with inverting the pins and trying a pull up like previously mentioned by Douglas.


---
**Douglas Pearless** *October 29, 2018 22:13*

I think this is what you need, it will home to the MAX end stops and load alpha=270, beta=270 and gamma=270 as the homed position:



endstops_enable                              true             #enable the end stops module in the kernel



alpha_min_endstop                            nc               # pin not connected

alpha_max_endstop                            1.24^!           # pin is connected ^ pullup ! inverted for NC switch to GND

alpha_homing_direction                       home_to_max      # Home to max endstop

alpha_max                                    270              # load this value as the homed position



beta_min_endstop                             nc               # pin not connected

beta_max_endstop                             1.26^!           # pin is connected ^ pullup ! inverted for NC switch to GND

beta_homing_direction                        home_to_max      # Home to max endstop

beta_max                                     270              # load this value as the homed position



gamma_min_endstop                            nc               # pin not connected

gamma_max_endstop                            1.28^!           # pin is connected ^ pullup ! inverted for NC switch to GND

gamma_homing_direction                       home_to_max      # Home to max endstop

gamma_max                                    270              # load this value as the homed position



Cheers

Douglas


---
**Family First** *October 30, 2018 11:38*

**+Douglas Pearless** 

The thing is that the code is spot on actually the suggestions are decent but i only have alpha throigh gamma min endstops. The logic is inverted to have the travel first to min and not seek out max. The diatance is still 270 relative to the heatbed. The real issue was two fold:



1. I did not aolder the connections of thr wndstops thus thE intermittence of operation. 

2.  I has endtops and motors switched.



When i soldered the endstops and then switched the sensor pins and used the listed code, which was designed to accomodate the limited number of pins i had available, it is working. Now on to bed levelong and connecting fans. 


---
**Douglas Pearless** *October 30, 2018 20:38*

I am glad it is working for you :-)


---
**Family First** *November 15, 2018 02:25*

Doug it is working but I get it is like writing in a mirror.  We will see


---
**Douglas Pearless** *November 15, 2018 04:21*

**+Family First** What do you mean "writing in a micro" does this mean the axis etc and motion are mirrored ?


---
*Imported from [Google+](https://plus.google.com/105733681846829955664/posts/GgBBYhUH2x8) &mdash; content and formatting may not be reliable*
