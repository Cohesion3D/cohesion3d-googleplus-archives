---
layout: post
title: "Pre-Ordered my C3D board yesterday. I've been having issues with my current setup so figured I'd try this out"
date: January 11, 2017 04:20
category: "General Discussion"
author: "Kris Sturgess"
---
Pre-Ordered my C3D board yesterday. I've been having issues with my 

current setup so figured I'd try this out.



Anyone know where I can get the white connectors for the wire harness that goes from the PSU (24V,G,5V,L) to the M2 Nano? I've misplaced my original cable. I'm figure I'm probably going to need it.



Looking forward to getting this board in.



Kris





**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 04:27*

Show us a pic please, depending on your power supply you may not need it. 


---
**Kris Sturgess** *January 11, 2017 04:34*

Crappy pic. 



[drive.google.com - 20161028_195216.jpg - Google Drive](https://drive.google.com/open?id=0B2TmtKs6ubOYZTQ0QnlyRjZHcEk)


---
**Don Kleinschnitz Jr.** *January 11, 2017 16:33*

**+Kris Sturgess** go here and look at the Power tab. Zoom into the Laser Power Supply at the top. Connector and pins are there. The pins do not come with the connector.



Connector:

[digikey.com - 640445-4](http://www.digikey.com/products/en?keywords=A19952-ND)



Pins

[https://www.digikey.com/product-detail/en/te-connectivity-amp-connectors/640252-2/A19991CT-ND/290818](https://www.digikey.com/product-detail/en/te-connectivity-amp-connectors/640252-2/A19991CT-ND/290818)




---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 16:44*

Careful. What you need is JST VH 4 pin. 


---
**Don Kleinschnitz Jr.** *January 11, 2017 16:55*

**+Ray Kholodovsky** are you saying this is wrong, its what I am using for DC power???


---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 17:00*

No.  There are 2 types of connectors circulating on the wire harnesses.   Yours is one of them.  The other one is the JST VH - the difference is that it has a hook instead of the notch for securing.   Since the PSU and Control Board have the VH type, that's what he should ideally get.  Both the Chinese and my boards are "made to work" with the notch in the housing of your type squeezing into the wall of the VH male connector on the PCB. 


---
**Don Kleinschnitz Jr.** *January 11, 2017 17:55*

**+Ray Kholodovsky** my connector and supply has a hook? I have not seen any other type on white connector supplies?


---
**Kris Sturgess** *January 11, 2017 22:34*

Thanks. I'll see what I can find. Still searching for my original cable too.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/Eu9Q2a7acNk) &mdash; content and formatting may not be reliable*
