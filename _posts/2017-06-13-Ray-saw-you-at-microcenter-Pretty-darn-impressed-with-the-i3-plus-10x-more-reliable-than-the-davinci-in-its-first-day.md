---
layout: post
title: "Ray! saw you at microcenter - Pretty darn impressed with the i3 plus, 10x more reliable than the davinci in it's first day"
date: June 13, 2017 17:35
category: "Thank you and other tips"
author: "Jon C"
---
Ray! saw you at microcenter - Pretty darn impressed with the i3 plus, 10x more reliable than the davinci in it's first day.



Looking forward to what you've got coming for it



Ps: That stock buildtak is no joke. Lost the first 3 layers having it smashed down like using glass :P





**"Jon C"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 20:11*

Nice meeting you Jon. 

I'm liking the touchscreen of the Di3 Plus. 

The upgrade is going to be a bit more extensive than originally expected (aka on the non-plus i3). There's that custom hotend ribbon connector, and touchscreen isn't supported. 


---
**Jon C** *June 13, 2017 20:12*

for sure, that screen is pretty darn great - wonder if there's a way to run it through a Pi and octoprint - kinda just use it as a 'web frontend'


---
**Jon C** *June 13, 2017 20:19*

Here's something someone was working on: Touch UI for octoprint [plugins.octoprint.org - TouchUI](http://plugins.octoprint.org/plugins/touchui/)


---
*Imported from [Google+](https://plus.google.com/107532222526200227830/posts/GsKndBQojJW) &mdash; content and formatting may not be reliable*
