---
layout: post
title: "Have the k40 with C3D Mini with seperate 24v PSU installed per instruction on the site;"
date: January 12, 2019 16:50
category: "C3D Mini Support"
author: "Ray Rivera"
---
Have the k40 with C3D Mini with seperate 24v PSU installed per instruction on the site; while engraving using light burn (v0.8.07) the laser is firing during overscanning and fast moves and ruining materials. The preview in lightburn looks good though. Any recommendations?



![images/687a87a73cd8161945c920a252a013da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/687a87a73cd8161945c920a252a013da.jpeg)
![images/229a44933603ca1d75fdb12506a091e4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/229a44933603ca1d75fdb12506a091e4.png)

**"Ray Rivera"**

---
---
**Ray Rivera** *January 12, 2019 17:19*

i uninstalled v0.8.07 and reinstalled an older version v0.6.06 and everything engraves fine. I need to brush all the acrylic dust off but it is working again.


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 18:55*

If you could, please send the .LBRN and saved gCode file from each version .06 and .07, zipped up and clearly indicating which is from what, to developer at lightburnsoftware dot com with this info so we can all have a look.  


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 19:22*

Also, which firmware are you running (have you changed from the stock Smoothie firmware)? And have you made any modifications to the config.txt (Smoothie) or $$ settings (GRBL-LPC)? 


---
**Ray Rivera** *January 12, 2019 22:38*

I am running the stock firmware and only made changes as indicated for lightobjects ztable. Will reinstall .08 to save the file for you.


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/P9Ah4XqbnV8) &mdash; content and formatting may not be reliable*
