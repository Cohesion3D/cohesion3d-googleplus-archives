---
layout: post
title: "Sorry, not a very good photo, but doing image engraving I am getting these \"jogs\" in the cut"
date: December 21, 2018 19:31
category: "K40 and other Lasers"
author: "Tammy Novak"
---
Sorry, not a very good photo, but doing image engraving I am getting these "jogs" in the cut.  I have the new Laserboard, running on K40.  I figured I just have settings wrong on Lightburn.

![images/5e9cc2f299c04319589d49a9ae9f251a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5e9cc2f299c04319589d49a9ae9f251a.jpeg)



**"Tammy Novak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2018 19:40*

Like I said, stay with the stock smoothie firmware, decrease speed for engraving to 80mm/s with grayscale or use newsprint dither in LightBurn and you can increase the speed a little higher than that.  



For faster raster engraving you'll want to use GRBL-LPC firmware.  The build currently on our site is for the C3D Mini board. For LaserBoard we have a build almost ready that still needs a few minor tweaks, and I've just been really busy to get that taken care of.  Please use Smoothie as described for now and I'll let you know when the GRBL is ready.


---
**Tammy Novak** *December 21, 2018 20:37*

**+Ray Kholodovsky**  thank you.  I didn't research much before attempting that image, and I see now I was running way too fast & too much power.  I appreciate all the time and effort you give helping us out and always striving to make everything better.  Merry Christmas (or Happy Holidays, not knowing what you celebrate this time of year!)


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2018 20:54*

Same to you.  Mostly robot and laser holidays for me :) 


---
**Reini Grauer** *December 26, 2018 03:21*

I found the acceleration settings on mine needed some adjustment to keep from losing steps when moving rapidly.  




---
**Ray Kholodovsky (Cohesion3D)** *December 28, 2018 02:47*

GRBL-LPC for LaserBoard is now up on Jim's site: 



[embeddedtronicsblog.wordpress.com - grbl-lpc C3D LaserBoard Firmware Builds](https://embeddedtronicsblog.wordpress.com/2018/12/27/grbl-lpc-c3d-laserboard-firmware-builds/)



Use the spreadcycle version unless told otherwise.


---
**Ray Kholodovsky (Cohesion3D)** *December 28, 2018 02:53*

Download the file, rename it to firmware.bin



Turn your board off and unplug the USB Cable. Pull the SD Card, put the files on the SD Card (backup then delete the other files), then put it back into the board. Power up.  Wait for a few seconds while the LEDs count up a bit.  Then plug in the USB Cable.  You'll currently need to install the usbser.inf driver. [dropbox.com - GRBL-LPC Driver](https://www.dropbox.com/sh/fykgkqvrdssv0mi/AAB4OkMKN6YnPbFZtYRrZXkUa?dl=0)



That's a manual install through the Device Manager, and Win8 or higher might cause it to fail in which case you'll need to do this: 



[https://windowsreport.com/12109-install-unsigned-drivers-windows-8/#.XCWQFFVKiHs](https://windowsreport.com/12109-install-unsigned-drivers-windows-8/#.XCWQFFVKiHs)


---
*Imported from [Google+](https://plus.google.com/105496110568876018217/posts/Ci3FpeR5MmJ) &mdash; content and formatting may not be reliable*
