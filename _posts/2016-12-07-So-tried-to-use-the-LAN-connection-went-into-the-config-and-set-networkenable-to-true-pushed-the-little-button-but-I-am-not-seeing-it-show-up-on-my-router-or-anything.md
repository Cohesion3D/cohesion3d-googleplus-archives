---
layout: post
title: "So tried to use the LAN connection, went into the config and set network.enable to true, pushed the little button but I am not seeing it show up on my router or anything"
date: December 07, 2016 21:21
category: "General Discussion"
author: "Kelly S"
---
So tried to use the LAN connection, went into the config and set network.enable to true, pushed the little button but I am not seeing it show up on my router or anything.  Little light is flashing on it too.  Am I missing a step?  This is with the C3D mini





**"Kelly S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 21:26*

Let's debug.  Got a pic of how it's installed?  I think you know the correct orientation, but let's cross that out. 

Are both yellow and green LEDs on the module on?  I believe the green one blinks in regard to network activity, yellow should be a more steady pulsing. 

I'm assuming you saved the config and if you open it up again then you'll see the enable option is indeed true.  




---
**Kelly S** *December 07, 2016 21:31*

Yes I saved the config, and then moved it over keep the old one in a separate folder in case it got corrupt somehow.  If I connect via USB and open it indeed the change is there.  I will try to get a picture in just a little bit, taking a break from the office for a tad bit to re-food myself.  


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/Wip2L6cNcT3) &mdash; content and formatting may not be reliable*
