---
layout: post
title: "here are screen shots from my computer....."
date: April 07, 2017 22:31
category: "General Discussion"
author: "Mike Chartier"
---
here are screen shots from my computer..... wanted to give you a clearer picture of what is happening in more detail like requested.... 



when i first installed the board, i installed the stepper modules for both X and Y, found out i only have one jst-xh connector, i still left both modules in anyway (wasnt sure if i should take one out for X axis being that my ribbon cable is my x axis) the only one i adjusted was the Y axis module and i went way over a 45 degree turn. 



now when ill start from where i uninstalled drivers to continue;



1. Uninstalled Drivers that i originally installed having windows 10

2. Restarted machine.

3. Plugged in usb, it went through the driver install automatically. 

4. Opened software

5. Started over with uploading photo to test. 

6. Rastered my photo with following settings:

7. i went to run my job and it would start but than give me "machine disconnected plus that error in the picture.....



Laser power: Min-0 Max-20

Laser diameter - 0.1

Passes - 1

Trim pixels, join pixels and burn white is checked off. 

i did not change any other setting other than greyscale set to "desaturation"



In my settings, i did not do anything with the gcode tab, i left that alone... i tried adding in the homing code but it made my machine go nuts and rammed everything into the wall..... G28.2 is what i saw in a tutorial for that.... anyway...... i did have it going for a while, even  did a test run on anodized aluminum to find out my axis wire was backwards.  



![images/dbdbe65418875e083e700b89cda89374.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dbdbe65418875e083e700b89cda89374.jpeg)
![images/da9015dd6505253a08a4ef7154b045c6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/da9015dd6505253a08a4ef7154b045c6.jpeg)

**"Mike Chartier"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 03:56*

Thanks Mike. 



There are a few quick things that I'd like to address: 

You said you did a G28.2 and things got rammed into the wall. When you do this the head should move left and to the rear towards the switches in those positions. If the head moves a different way that axis cable needs to be flipped. Do that with power off. Sounds like you already figured this out? 



Can you connect to the board and jog around using the LaserWeb arrows? 



Can you advise if you're using a different USB cable now? 


---
*Imported from [Google+](https://plus.google.com/111530720512046600076/posts/MhZaKt5fkku) &mdash; content and formatting may not be reliable*
