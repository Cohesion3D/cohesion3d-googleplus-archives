---
layout: post
title: "Hi! Just received this about 2 minutes ago"
date: July 08, 2017 18:01
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Hi!  Just received this about 2 minutes ago.  Notice what looks like a capacitor broken off in the package.  The package is still sealed.  Does this mean i have to return it and wait longer?



Is this something I can fix with a heat gun since it's SMT?



If I attempt to fix and that fails, can i still return it for an exchange as you can see it's still int the package and sealed?



![images/d012345c0473d6ace7b6de0dade4cfc4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d012345c0473d6ace7b6de0dade4cfc4.jpeg)
![images/0eb038da471caea0e9a3baa826be165d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0eb038da471caea0e9a3baa826be165d.jpeg)

**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 18:14*

Sorry to hear that happened.   We've had one other case of that cap getting knocked off in transit.  



Good news is that the board works fine without that capacitor.  There's plenty enough capacitance on the same voltage rail to level things out. 



A heat gun would not work here, you'd end up melting the connectors next to it.  



As far as options go, I can suggest some combination of these things:

-I send you a replacement board, I need the original one back.

-You use the board you have as it will still work without that component on it.  I recommend starting here and keeping me posted on your progress. 



The main thing I cannot do is accept back a board you attempted to fix, as I said that would probably end up heavily damaging it. 


---
**Todd Mitchell** *July 08, 2017 18:19*

Thank you.  I will not attempt to fix but take your advice on using the board.  If I have troubles (even subtle weird stuff), can I return the board for an exchange even though I've opened it?




---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 18:27*

Yes, please go ahead and open it and do the upgrade!  



Just, if you have issues/ questions let us address them first as I'm pretty confident it won't be because of the cap.  But we definitely take care of our customers.  



Thank you!




---
**Todd Mitchell** *July 08, 2017 20:41*

Ok - Up and running with two current issues:



1) When I press the Laser Test button, nothing happens but the manual button on the machine still works.



The LEDs - L1 - L4, 3v3, and VMOT are indicating correctly as described in the K40 upgrade guide.



and firmware shows as : "Firmware smoothie edge-7d257e9 detected"



2) using Laserweb4, I get the error shown in the image message when I try to home to 0 on either axis.



Note: this is not a k40 but had the m2nano. This is the ten-high laser cutter you can find on amazon (similar to k40, just a bit larger with additional features)



All Plugs and instructions matched as described.



The laser PSU is HY-T50



![images/2ec3fe9930d183bfacfd79b3f83cfaad.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ec3fe9930d183bfacfd79b3f83cfaad.png)


---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 20:45*

Cool!



1.  Correct, 2 settings must be configured in LW: the test power and duration, before this will work.  



2. The error itself, please post that screenshot to the fellow LaserWeb community for dev review. 



To check homing, please send a command G28.2 from the terminal in the bottom right.   And that is the command you've put in for the homing command field in Settings right? 


---
**Todd Mitchell** *July 08, 2017 21:34*

Thanks!



OK, fixed



1. The laser is now test firing.  If I have a control panel on the laser that has a % setting, should I set it to 100% so the software can fully control the power level or should I use it as a cap?



2. The machine now performs home when I use G28.2 X and G28.2 Y.  The Y axis has a tricky home switch that I think has an issue.  Is there a way to determine the input state of the button?  (much like mach3 has the diagnostics panel)  Or should I pull out the multi-meter?


---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 21:37*

1.  Not full power.   10-13mA on the pot is a good place to start.  The config.txt will cap to 80% you can change to 100% when you're ready. (it's the laser max pwm 0.8 --> 1 value)  



2.  The command is M119 to check endstops states.  It is possible that the latest versions of LaserWeb do not show the response string, so you will need Pronterface to read it.   Check a recent thread here by Tammy, we went really into depth about endstops there.  


---
**Todd Mitchell** *July 08, 2017 22:09*

I used proteface to see the input state state.  X shows a change in Pin state but Y does not.  I'm fairly sure it's faulty from the manufacturer since I had the same issue before your cohesion board was installed.  I'll need to test it manually.



Question: I notice that the direction of G28.2 Y is always in the directino of Y+ however G28.2 X is always in the correct directino of X-.



Is this an issue on the board?  I tried inverting in config (removing the '!') but that didn't work.



Also, is there a recommendation on the pot setting for the stepper motors?  one of the motors sounds a bit janky.


---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2017 22:14*

Well, the k40 endstops are at the left (X min) and rear (Y max) positions, so that's how the configuration specifies it to work.   You can get an endstop and wire it to the board, and put it at the front and designate it as Y min if you so choose. 



The actual value you wanted in config was beta home_to_max   - can change it to min, but I just explained why.  And it recognizes that rear as coordinate 0, 200.  0,0 is origin is the front left.  



Pots... Tricky one... Default is the flat facing out.   As you get to higher speed stuff you'll need to turn 45 - 90 degrees clockwise to increase current so the motors won't stutter.  But then the stock power supply isn't powerful enough anymore and it'll brown out, so you need to get an external psu.  I guess turning them 45 degrees is a happy medium for the time being. 


---
**Todd Mitchell** *July 09, 2017 01:20*

Ok - figured out the Y end-stop deal.  The laser cutter expects 1 wire per GND per endstop but the Cohesion expects X and Y endstops share the ground.  It's homing correctly.



Also had to change the endstops config to use beta_min_endstop instead of max_endstop


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 01:24*

That's not technically correct, so if you run into issues later we can revisit this. But if it works for you, great. 


---
**Todd Mitchell** *July 09, 2017 01:28*

Are you saying that the cohesion has separate grounds per endstop?  I realized the issue by reading this document: [s3.amazonaws.com](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570)



Is this out of date?


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 01:28*

Sorry, I was referring to the 



Also had to change the endstops config to use beta_min_endstop instead of max_endstop



bit.  


---
**Todd Mitchell** *July 09, 2017 01:40*

Ah ok.  Well, as I understood it the min/max part of that setting maps to a Pin.  



Either case, it works - just made a first cut and I'm stoked!



This cohesion board took my workflow from:



Sketchup ->svg -> inkscape -> LaserDRW -> loads of konami codes to adjust the lines and convert a ton of segments to a proper circle -> $@%^ of frustration -? load up multiple tasks by generating multiple files and queing -> more @#$!% when I make a mistake -> finished part



to 



Sketchup -> dxf -> laserweb -> finished part.



Now tempted to write some plugins for laserweb but first need to checkout alternatives


---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2017 01:44*

Re: the endstop.  Not quite as you described.  



The min vs max is where the endstop is.  On the stock k40 the Y is at max. 



The pin # following it is what pin. 



On the C3D Mini, the physical port is Y MIN, but the Y MAX endstop of the k40 is being hooked up to it.  Hence the stock config provided is exactly how it <b>technically</b> should be.  You home to rear left, and the board should report back to LW that it is at 0, 200.   



But if you just use the set zero button in LW none of this matters anyways.  





And I'm glad you're up and running!


---
**Todd Mitchell** *July 09, 2017 04:01*

Hi Again, just did a more complex cut.  The cut came out opposite from the screen (backwards text even).  My laser cutter, when facing it directly homes at upper left corner.



Are there any settings that you know of that can affect this?





Note: i have beta_dir_pin as 0.11^ rather than the default value you specified.  


---
**Todd Mitchell** *July 09, 2017 04:12*

Cancel this.  Just had to flip back the direction pin on the Y axis


---
**Claudio Prezzi** *July 09, 2017 08:41*

The Error message on single axis homing was a bug in LW that has been fixed already. Please update LW4.


---
**Todd Mitchell** *July 12, 2017 01:54*

Thanks Claudio




---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/Qf5RCbBPiyR) &mdash; content and formatting may not be reliable*
