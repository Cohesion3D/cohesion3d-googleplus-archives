---
layout: post
title: "So...definitely going to add a safety switch on the lid!"
date: October 15, 2018 09:51
category: "K40 and other Lasers"
author: "Kieron Nolan"
---
So...definitely going to add a safety switch on the lid! Got zapped when doing the mirror alignment. 10% really hurt! 

![images/a5aa170056779fdb9fc984d456530c8a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a5aa170056779fdb9fc984d456530c8a.jpeg)



**"Kieron Nolan"**

---
---
**Anthony Bolgar** *October 15, 2018 10:08*

Yup, those beams can be a bitch.


---
**Joe Alexander** *October 15, 2018 11:09*

I think most of us have done that  once or twice...and they burn! just be lucky it wasn't the focused beam :P


---
*Imported from [Google+](https://plus.google.com/113795912126148646501/posts/QWbSk4AgAsX) &mdash; content and formatting may not be reliable*
