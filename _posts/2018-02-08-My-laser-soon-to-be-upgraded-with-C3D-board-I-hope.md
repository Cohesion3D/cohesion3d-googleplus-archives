---
layout: post
title: "My laser - soon to be upgraded with C3D board (I hope)"
date: February 08, 2018 02:59
category: "K40 and other Lasers"
author: "Bill Knopp"
---
My laser - soon to be upgraded with C3D board (I hope).  I got my board a few days ago, but have not had time to work with installing it.  As you may be able to tell, my laser is in a closet, and needs to be pulled out in order to get at the side with the electronics (under the big exhaust vent pipe).  I have been working on bettering the exhaust fan system and seem to have it worked out now.  I use the fan that came with the machine on the inside, and designed a window box that uses 2 X 140mm Noctua industrial fans (look like PC fans but run at 24V and move a lot more air).  The only problem that I have now is that I need to leave the door of the laser open about 2" to let air into the machine to exhaust the smoke.  Anyway, you can see my new panel inside the laser - cut from some left over 1/8" hard board.  Hopefully the install of the new C3D board goes smoothly this weekend.



![images/678eadbc3fd85ef644055619e05c4cce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/678eadbc3fd85ef644055619e05c4cce.jpeg)
![images/337dceef216a744b09e11c9f36f1d827.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/337dceef216a744b09e11c9f36f1d827.jpeg)
![images/685d180775a2d4cb7451fc34362df536.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/685d180775a2d4cb7451fc34362df536.jpeg)

**"Bill Knopp"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 03:00*

Make sure to check the power supply voltage. 24v good. More - not so good. 


---
**Bill Knopp** *February 08, 2018 03:20*

**+Ray Kholodovsky** Thanks!  I will be sure to check before connecting anything.  I did not even think of this, as it is running an M2Nano, so I just assumed...Well we all know where that goes.




---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 03:22*

I have seen one of these with the M2Nano and 36v psu. That's a bit high for the C3D. 


---
**Griffin Paquette** *February 08, 2018 23:49*

^ Caps are rated for 35V. I like to give it some buffer from there so I would say don’t realistically exceed ~24 and you are perfectly fine. 


---
**Bill Knopp** *February 09, 2018 03:33*

Hi, starting to get ready to install my board on the weekend.  Here are pictures of the power supply and m2Nano.  From looking at the getting started guide, I think:

1) The ribbon cable just transfers over.

2) The small 4 pin white plug is the Y motor connector, and it just moves over to the Y connector

3) The power connector is a bit different - the current plug has 6 spots in it, and 4 are used - OK I get this 24V, 5V, GND, and Laser.  But, it will obviously not connect directly to the C3D board.  Can I connect it to screw terminals - 24V and GND to the 24V screw terminals, L to FET 2 (-) {I think, can check in the docs}, but where do I run the 5V line to?  It would be nice to have a screw terminal, as then I can just connect all of these wires to screws, and don't have to worry about a plug for just the 5V, but I don't see one on the board.  Any suggestions? Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2018 03:37*

Yes, hook up 24v, Gnd, and L to screw terminals. Diagrams are floating around. Do not connect 5v anywhere. Insulate the wire. 


---
**Bill Knopp** *February 09, 2018 03:56*

**+Ray Kholodovsky** Wow, I even forgot to add the photo, and I got an answer.  Cool!  Here is the photo anyway...  Also the P/S says 24V but I will check first anyway.

![images/cef096190f1cf2dcd93b6c5ab287708c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cef096190f1cf2dcd93b6c5ab287708c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2018 03:59*

The picture actually isn't helpful, however 6 pin power connector is not uncommon and we know what to do. 

Search the group, diagrams exist.  L goes to 4th screw terminal from the left on the bottom row.


---
**Bill Knopp** *February 10, 2018 18:27*

**+Ray Kholodovsky** I can not thank you enough!  It is like I got a whole new machine!  Install went exactly as planned, and worked first try.  I had to change the Y-Max value in the config before I could get it to home and print from the homed position correctly, but that is to be expected with a larger bed.  Other than that it is fantastic!  Thank you again!



LightBurn software is also fantastic.  Took a bit of fiddling to get used to it, but I find it so much better than the software I had to use with the Nano.


---
**Ray Kholodovsky (Cohesion3D)** *February 10, 2018 19:14*

Great! Sounds about right. 


---
**Bill Knopp** *February 21, 2018 20:14*

Well, I can truly say I have done more cutting since I got the C3D board than I did in the first 3 years of owning this laser combined.  Only issue is that it stutters when engraving.  I have seen several posts about this, but have not found a solution.  I have tried copying the file to the SD card and running from there, and it seems a bit better but still I see the head speeding up and slowing down as it engraves.  I am using LIghtBurn and will continue to search for solutions.  I have tired slowing down to 100mm/s and it got a bit better, but still noticeable.  I have only tried grey scale, and even with this issue have seen some nice results, it just seems strange.


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 20:23*

Go into config.txt on the board's memory card and change both the 2 values near the top from 4000 to 18000 or 30000







There is also a newsprint mode in Lightburn you can try. 



Failing that, grbl-lpc firmware. 


---
**Bill Knopp** *February 22, 2018 03:16*

**+Ray Kholodovsky** So looks like I may have to try the grbl-lpc firmware.  To do this can I install it on a different SD card, and if I want to go back just install the card that came with the C3D board and reboot?  For cutting the smoothie firmware seems to work quite well, and I have all of the end stops and sizing set up perfectly so I would like to make sure that changing back will be simple if I can not get GRBL dialed in.


---
**Ray Kholodovsky (Cohesion3D)** *February 22, 2018 03:18*

No, you'd have to replace the firmware.bin file for each. It changes to firmware.cur (capitalized) after it flashes. 

The files you need for Smoothie are at the bottom of the install guide and the firmware for grbl is in an article on our support/ docs site 


---
**Bill Knopp** *February 23, 2018 03:08*

**+Ray Kholodovsky** Tried installing grbl firmware.  The LED's do the counting up, but the board just sits there  beeping with no display.  I have been able to go back to smoothie successfully, but grbl just beeps, and the LED's look like the photo

![images/f8baf6e864b89357884a658fd076d3c1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8baf6e864b89357884a658fd076d3c1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 03:10*

GRBL doesn't support a screen so you may want to disconnect that. 

Have you tried connecting with Lightburn after flashing the grbl...?


---
**Bill Knopp** *February 23, 2018 03:26*

**+Ray Kholodovsky** Just tried, Lightburn connects, and will move the head.  Just has a continuous beep from the board.  It will move the head when I try a raster, but does not fire the laser.  This could be a wiring thing?




---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 03:29*

Did you redefine the Device in Lightburn to be Cohesion3D (GRBL)   ?


---
**Bill Knopp** *February 23, 2018 03:30*

**+Bill Knopp** looking at the console in lightburn, it sends a $H command, and gets an alarm 9 response, so I am thinking config issue.


---
**Bill Knopp** *February 23, 2018 03:37*

**+Ray Kholodovsky** Yes, and using the firmware for the Cohesion3dmini from [https://github.com/cprezzi/grbl-LPC/releases](https://github.com/cprezzi/grbl-LPC/releases)


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 03:37*

Did you get the grbl-lpc from my support site? 




---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 03:40*

No. 



[cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)



The firmware file from here.  That's it. Onto the card and reset the board. 


---
**Bill Knopp** *February 23, 2018 03:49*

**+Ray Kholodovsky** Same thing, the beep that it makes as startup never stops.  Lightburn will home, move the head to commands, and move the laser to raster, but does not fire the laser.


---
**Bill Knopp** *February 23, 2018 03:53*

**+Bill Knopp** OK, removing the display has stopped the beeping, don't know why I did not try that when you first said.  Sorry for that.  Now just to get the laser to fire.




---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 03:53*

As I said, grbl doesn't support the screen so if you want the buzzer to stop you need to unplug the GLCD Adapter from the board. 



Type $$ into LB terminal.  What are the values for $30 and $32 ?


---
**Bill Knopp** *February 23, 2018 04:05*

**+Ray Kholodovsky** $30=1000   $32=1


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 04:06*

Those are all good.  Did laser fire work when you were on Smoothie? 


---
**Bill Knopp** *February 23, 2018 04:11*

**+Ray Kholodovsky** Yes smoothie worked great except for I had to raster engrave very slow or I would get stuttering in the laser movement.  I will switch back to smoothie for now, as I need to get up for work in about 6 hours and need some sleep.  Thank you so much for your help on this!  I can not believe the support I have received, and all of the time you put in!  I have a bunch of cutting to do this weekend so probably best just to go back to smoothie.




---
**Bill Knopp** *February 23, 2018 04:26*

**+Bill Knopp** No laser with smoothie either now.  Picture of LEDs below...

![images/836733d05d44b9c78968abb083070287.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/836733d05d44b9c78968abb083070287.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 04:27*

Are you sure your yellow wire is going to the 4th screw terminal from the left?


---
**Bill Knopp** *February 23, 2018 04:31*

**+Ray Kholodovsky** It was working all last week fantastic.  I will check wiring to see if it came loose when I was changing the SD card, but that is a project for tomorrow.  Thanks again!




---
**Bill Knopp** *February 24, 2018 03:05*

**+Bill Knopp** Thanks again for all the help yesterday.  I got it firing again...It was my bad!  I hit a wire that had a bad solder joint on one of the switches from the manufacturer.  A little hunting with a meter and O-scope, and I tracked it down.  Re-Soldered it and all is good.  Tried GBRL-LPC and it is faster with the engraving, but I really like the screen functionality, so for the most part I will stick with smoothie, and just try to get my settings worked out so that I can raster at 75mm/sec.  It may be slow, but I like the firmware better so that is just the way it will have to be.  Thanks again,

Bill


---
*Imported from [Google+](https://plus.google.com/109571243909788664200/posts/LHegjH1gr8a) &mdash; content and formatting may not be reliable*
