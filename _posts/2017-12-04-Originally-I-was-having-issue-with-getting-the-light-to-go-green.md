---
layout: post
title: "Originally I was having issue with getting the light to go green"
date: December 04, 2017 03:02
category: "C3D Mini Support"
author: "JT T"
---
Originally I was having issue with getting the light to go green. After Flashing the Firmware I went back, and it did now work with all the lights. 



However my Y axis now doesn't move, I suspected a bad A9488. I then took two DRV8825's from a 3D printer and used those and still no results. X axis moves great, Y axis doesn't move. 



I tried flip flopping chips from Y axis to X axis cables No change. Even the Known good chips both A9488 and DRV8825 did not move the Y location servo.  I suspected the servo motor was having an issue since I ruled out Chips, current, and everything else. 



After hooking up the original board to test for a burnt up servo motor the motor spins right across the screen just like it did before. 



So Servo Motor is good, chips were tested good, and firmware was checked. The results is anything plugged into the Y access doesn't work properly. 





![images/6e6eee1cdde5b23c9ceb15c888e11f26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e6eee1cdde5b23c9ceb15c888e11f26.jpeg)
![images/9ee83a6f87efd44c3c85e634c8c39ac4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9ee83a6f87efd44c3c85e634c8c39ac4.jpeg)
![images/c0cbeadf53711854a1a626715db08b18.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c0cbeadf53711854a1a626715db08b18.jpeg)
![images/8b22387b3b223a8f535e625405f18b46.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8b22387b3b223a8f535e625405f18b46.jpeg)

**"JT T"**

---
---
**Griffin Paquette** *December 04, 2017 04:38*

By swapping the drivers you eliminated that being an issue.



You can remap the pins for the Y axis in the config file to use the Z port instead. 



**+Ray Kholodovsky** is the one that can give you a definite solution though. 


---
**Anthony Bolgar** *December 04, 2017 04:42*

Did you try swapping the actual cable for X and Y, not the chips?




---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2017 21:14*

Hey there.  



Sorry to hear about this issue.  I test everything personally before shipping so I am very confused as to what happened between "I shipped your board and everything was working" to your having the issue with the Y not working.  If you can tell me more details about the earlier issues you had, I hope this can help with debugging and understanding the problem. 



Anyways, as a stopgap, you can move the Y driver and motor cable over to a different socket.  



In this case I will have you try the A socket.  



Here is a new config file to put on the memory card (then safely dismount and reset the board). 

[dropbox.com - C3D Mini Laser Batch 2 - Provided Config with Y remapped to A](https://www.dropbox.com/sh/9b6z1rpg1nbmnkn/AAAAmp2KY9oDjijR6r3X0ndWa?dl=0)



I can definitely replace the board if you need me to, but I would like to get you up and running this way first.  






---
**JT T** *December 05, 2017 03:22*

Thank you I will upload this tomorrow. If this works then no issues.



 As to what occurred I am not sure at all. I printed out a test print using a photo, running it through Laserweb4. Ran all the way through with some odd stepper motor skips, but completed all the same.  



I then adjusted some of the contrast of the photo and ran it again. I got to a certain point and it stopped moving up or down, just tried cutting the same line over and over. After a little bit it shut down with a loud beeping and simply gave me a red light. I couldn't figure out how to turn it on again, and I found the Micro-SD card was not even showing up. I figured a corrupt drive and grabbed one out of a stack flashed it and it came up. Then I realized the Y was totally not working at all. 



That is what brought me here.   

![images/d40a5e8ec11080d6f5c87b0adec41a8b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d40a5e8ec11080d6f5c87b0adec41a8b.jpeg)


---
**JT T** *December 05, 2017 03:29*

**+JT T**  Here was the first run, I ran it once and the stepper skipped so I ran it again, I ran it a few different times trying to get the location right. The point being it completed just fine.



If this works properly using the A,  I was planning on getting rid of the current steppers anyways and using the adapter you sell to run a microstepper and larger Nema17 motors.   

![images/da97930622919b3c0f5116ff7b960e1e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/da97930622919b3c0f5116ff7b960e1e.jpeg)


---
*Imported from [Google+](https://plus.google.com/110303323938619777357/posts/asSdpw89pHZ) &mdash; content and formatting may not be reliable*
