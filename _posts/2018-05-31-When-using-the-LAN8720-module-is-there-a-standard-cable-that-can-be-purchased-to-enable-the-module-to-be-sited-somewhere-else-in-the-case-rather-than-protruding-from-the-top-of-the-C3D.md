---
layout: post
title: "When using the LAN8720 module, is there a standard cable that can be purchased to enable the module to be sited somewhere else in the case rather than protruding from the top of the C3D?"
date: May 31, 2018 13:16
category: "C3D Mini Support"
author: "Scruff Meister"
---
When using the LAN8720 module, is there a standard cable that can be purchased to enable the module to be sited somewhere else in the case rather than protruding from the top of the C3D? I've been searching my local hardware store for a 7way x 2row male-female "jumper cable" with no luck. Does anyone have a part number for such a cable please?





**"Scruff Meister"**

---
---
**Tech Bravo (Tech BravoTN)** *May 31, 2018 13:36*

keep in mind that the LAN communication is EXTREMELY slow and ethernet connection is not supported in LightBurn. You would need to save the Gcode file, access the web interface and upload there. if you wanted to use this communication to overcome distance/length restrictions of USB then there are other options that allow you to maintain the speed and workflow of a USB connection.


---
**Scruff Meister** *May 31, 2018 20:15*

Thanks **+Tech Bravo** - I had not realised LightBurn cannot directly control C3D ethernet. Could you elaborate on the other options please? My issue is laser cutter is on one side of my garage, computer on other and the distance is significant but also I don't want a cable trailing across the room...


---
**Tech Bravo (Tech BravoTN)** *June 01, 2018 22:56*

**+Scruff Meister** sorry for the delay. my solutions to require a wire to be run but you can make it 100 feet plus if needed with regular cat5 or cat6


---
**Tech Bravo (Tech BravoTN)** *June 01, 2018 23:21*

[amazon.com - Amazon.com: HDE USB over Cat5/5e/6 Extension Cable RJ45 Adapter Set: Computers & Accessories](https://www.amazon.com/HDE-over-Extension-Cable-Adapter/dp/B004XYEXX4)


---
**Tech Bravo (Tech BravoTN)** *June 01, 2018 23:22*

these are not meant to be connected to a router or other network appliance. they simply allow you to make a really long usb cable out of an ethernet cable.


---
*Imported from [Google+](https://plus.google.com/116126524987588221083/posts/McQmPFurSqm) &mdash; content and formatting may not be reliable*
