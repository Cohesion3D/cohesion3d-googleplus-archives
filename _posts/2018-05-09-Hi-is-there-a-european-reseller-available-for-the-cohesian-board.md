---
layout: post
title: "Hi, is there a european reseller available for the cohesian board?"
date: May 09, 2018 21:46
category: "General Discussion"
author: "Sebastian Klos"
---
Hi, 

is there a european reseller available for the cohesian board? 





**"Sebastian Klos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2018 00:07*

Not at the moment, no.  Just the main website and we ship from the USA. 


---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2018 01:13*

We do ship worldwide. 


---
*Imported from [Google+](https://plus.google.com/109764463043098234376/posts/PhQAZVmVCYT) &mdash; content and formatting may not be reliable*
