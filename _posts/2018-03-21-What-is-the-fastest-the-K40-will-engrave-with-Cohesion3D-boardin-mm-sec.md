---
layout: post
title: "What is the fastest the K40 will engrave with Cohesion3D board..in mm/sec"
date: March 21, 2018 02:06
category: "K40 and other Lasers"
author: "Brad Januchowski"
---
What is the fastest the K40 will engrave with Cohesion3D board..in mm/sec





**"Brad Januchowski"**

---
---
**Ashley M. Kirchner [Norym]** *March 21, 2018 02:23*

It's not about how fast you can do it. It's about what kind of quality do you want or expect. This is something that you will have to test yourself, on your laser. For example, not all wood types engrave the same, and if I were to keep the machine as a constant speed across different materials, I will get a very different result on all of them. Acrylic engraves different yet again, then there's metal. All are different.


---
*Imported from [Google+](https://plus.google.com/114103379689506589665/posts/AMYtFkrre7f) &mdash; content and formatting may not be reliable*
