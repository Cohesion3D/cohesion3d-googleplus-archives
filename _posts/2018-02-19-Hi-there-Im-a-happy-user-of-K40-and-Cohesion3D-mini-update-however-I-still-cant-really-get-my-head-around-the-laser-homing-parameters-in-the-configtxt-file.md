---
layout: post
title: "Hi there, I'm a happy user of K40 and Cohesion3D mini update, however I still can't really get my head around the laser homing parameters in the config.txt file"
date: February 19, 2018 16:10
category: "K40 and other Lasers"
author: "Maurizio Martinucci"
---
Hi there, I'm a happy user of K40 and Cohesion3D mini update, however I still can't really get my head around the laser homing parameters in the config.txt file.  I have a lot more bed space than the one I seem to be allowed to use with the combination C3D and LightBurn. My home coords are set now at left rear position but there's a lot more space on the Y axis (in fact a couple of mm also on the X)  to set it further down. However the homing goes always a little up than what I wish, so possibly the 0,0 can be reset in the config file, or am I wrong? 

![images/4f31769c30cb8498fee3582a97eec57c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f31769c30cb8498fee3582a97eec57c.jpeg)



**"Maurizio Martinucci"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 16:15*

In a k40 the homing switches are at the rear left. 



The origin (what you select in LB) is always the front left. This is 0,0. 



In config.txt (board's memory card) the beta max is the y max value, on the k40 this is usually 200mm. So you need to measure your total usable space and adjust this value in that file. Then save, safely eject the drive in windows, and reset the board. 


---
**Maurizio Martinucci** *February 19, 2018 17:04*

Yes,  that worked! Thanks a bunch!


---
*Imported from [Google+](https://plus.google.com/117027581754233433770/posts/a8JGGFSJgKU) &mdash; content and formatting may not be reliable*
