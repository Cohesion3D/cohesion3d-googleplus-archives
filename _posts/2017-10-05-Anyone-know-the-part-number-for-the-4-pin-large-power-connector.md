---
layout: post
title: "Anyone know the part number for the 4 pin large power connector?"
date: October 05, 2017 19:26
category: "K40 and other Lasers"
author: "ThantiK"
---
Anyone know the part number for the 4 pin large power connector? Just got my c3d mini but had begun a conversion on my k40 a while back and lost this connector.   Or better yet, is there alternative connection points I could utilize? I know where there is an alternative for 24/gnd, but 5v/L connection? 





**"ThantiK"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 19:29*

VH3.96, but you only need to hook up 24v and Gnd to the Main Power In terminal at top left, and L to the P2.5- terminal, 4th from left on the bottom strip. 


---
**Ariel Yahni (UniKpty)** *October 05, 2017 19:46*

**+ThantiK**​ Long time overdue convertion


---
**ThantiK** *October 05, 2017 22:59*

**+Ray Kholodovsky** Thanks!  Ordering some of that connector now, just so I can clean this back up.


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2017 23:25*

Again, 3 wires to 3 screw terminals.  I actually prefer that as far as cleanliness. 


---
**Anthony Bolgar** *October 06, 2017 21:37*

I am with Ray on using the screw terminals.


---
**ThantiK** *October 06, 2017 22:14*

I may have fucked myself anyways.  Plugged the damn drivers in backwards because I've got a lot going on in my life.  Now I'm even more stressed and pissed because I probably just killed this board.  


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 22:19*

Symptoms? Grab a pic of the board currently, remove all drivers and connectors, and just plug in USB. Any green lights? Which and What do they do? 


---
**ThantiK** *October 06, 2017 22:30*

**+Ray Kholodovsky** I'll let you know in a day or so - Plugged it into pi, the L-series lights all it up + 3v3.  All except 3v3 and L1 go out a second after, then a little later, L1 goes out too -- I gotta flash a new raspbian to one of these pis, get it connected to my network, and see what I can get out of it.  It's such a pain to mount/unmount this board because the way the K40 mounts that stupid little panel.



I am <i>HOPING</i> that if I mucked anything up, I only mucked up RST/SLEEP/MS3/MS2 and can recompile firmware to use the other 2 sockets.



Worst case, I'll buy another board from you at full price - I've already been given the go-ahead.


---
**Anthony Bolgar** *October 06, 2017 23:02*

Don't beat yourself up over this, I have messed up plenty of times myself.


---
**ThantiK** *October 07, 2017 01:14*

**+Anthony Bolgar**, I build 3D printers for a living.  I'm beating myself up over it.  Of all the people in the world to fuck up, I should have been the last. :/


---
**ThantiK** *October 09, 2017 20:45*

So I flashed the original smoothieware back to the board; It does mount the SD card appropriately again.



When plugged into the computer, I get a com port, mounted SD card, 3v3, L4, L1 and LED over by reset button are all solid.  L2 and L3 are blinking rapidly.  **+Ray Kholodovsky**, tell me this is a good sign! :P


---
**Ray Kholodovsky (Cohesion3D)** *October 09, 2017 20:47*

Green lights means you didn't fry it. Stay on Smoothie to test motion and all the stuff until your installation is complete. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/fX3ce2evMcu) &mdash; content and formatting may not be reliable*
