---
layout: post
title: "I seem to be having a problem getting laserweb3 running as a systemd service on my pi3"
date: December 31, 2016 00:27
category: "General Discussion"
author: "Bryan Hepworth"
---
I seem to be having a problem getting laserweb3 running as a systemd service on my pi3. I've installed as per pi instructions and it runs as an ordinary pi user. I've copied the file over as root to systemd and restarted the service but it fails. I had noticed other peoples posts from a few weeks back, but all the names would appear correct to me.



I was wondering if there is something daft I'm missing?



Cheers

Bryan



pi@raspberrypi:~/LaserWeb3 $ vi laserweb3.service

[Unit]

Description=Laserweb3 node.js service



[Service]

ExecStart=/usr/local/bin/node /home/pi/LaserWeb3/server.js

Restart=always

RestartSec=10                       # Restart service after 10 seconds if node service crashes

StandardOutput=syslog               # Output to syslog

StandardError=syslog                # Output to syslog

SyslogIdentifier=laserweb3

WorkingDirectory=/home/pi/LaserWeb3

User=pi

#Group=<alternate group>

Environment=NODE_ENV=production PORT=8000



[Install]

WantedBy=multi-user.target



~

~

~

~

~

"laserweb3.service" 18 lines, 512 characters







**"Bryan Hepworth"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 31, 2016 00:33*

Hi Bryan, I'd like to clarify - are you able to get LaserWeb running and can access it over the browser?  It sounds like you are trying to perform a "more advanced task" and this is what I'd like to confirm. 


---
**Bryan Hepworth** *December 31, 2016 01:29*

Hi Ray



Running as the pi user this works: -



cd /home/pi/LaserWeb3

node server.js   



I can see it in chrome.



Copying the laserweb3.service across sudo root and running this doesn't: -



cp /home/pi/LaserWeb3/laserweb3.service /etc/systemd/system/laserweb3.service

sudo systemctl enable laserweb3.service

sudo systemctl start laserweb3



I saw other people were saying the filename was wrong but mine says server.js in the LaserWeb3 Directory and the laserweb3.service file.



I was hoping a fresh pair of eyes could see if I was doing something daft.




---
**Ray Kholodovsky (Cohesion3D)** *December 31, 2016 01:51*

I think we should move this to the LaserWeb group and or ask the devs for help. **+Peter van der Walt** can you take a look?  I've not dealt with running LW as a service on Pi, so they may be able to assist quicker. 


---
**Bryan Hepworth** *December 31, 2016 02:30*

Hi Ray Thanks for that


---
**Bryan Hepworth** *December 31, 2016 12:35*

OK after a quick heads up and some sleep, the answer is from the bash shell: -



which node



To which the reply was: -



/usr/bin/node



Alter laserweb3.service to reflect this: -



[Unit]

Description=Laserweb3 node.js service



[Service]

#ExecStart=/usr/local/bin/node /home/pi/LaserWeb3/server.js

ExecStart=/usr/bin/node /home/pi/LaserWeb3/server.js

Restart=always

RestartSec=10                       # Restart service after 10 seconds if node service crashes

StandardOutput=syslog               # Output to syslog

StandardError=syslog                # Output to syslog

SyslogIdentifier=laserweb3

WorkingDirectory=/home/pi/LaserWeb3

User=pi

#Group=<alternate group>

Environment=NODE_ENV=production PORT=8000



[Install]

WantedBy=multi-user.target


---
**Bryan Hepworth** *December 31, 2016 13:03*

**+Peter van der Walt** Thanks Peter that was all the information I needed. Which node /use/bin/node alter service file to suit


---
*Imported from [Google+](https://plus.google.com/114125155844297036799/posts/cphivgXRA52) &mdash; content and formatting may not be reliable*
