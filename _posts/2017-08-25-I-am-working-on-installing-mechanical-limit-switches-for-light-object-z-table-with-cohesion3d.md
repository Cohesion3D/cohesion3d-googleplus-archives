---
layout: post
title: "I am working on installing mechanical limit switches for light object z table with cohesion3d"
date: August 25, 2017 11:58
category: "FirmWare and Config."
author: "Kevin Lease"
---
I am working on installing mechanical limit switches for light object z table with cohesion3d.  I have read the page [http://smoothieware.org/endstops](http://smoothieware.org/endstops) that suggests how to set them up.  Without the gamma limit switches enabled in config, I can jog the z table up and down fine.  When I enable the gamma limit switches in config (gamma_limit_enable                          true ) then I can't get any z table movement . 

I have the switches wired at common and normally closed so when the switch is not depressed the switch is closed, and when switch is pressed the switch is open.

I have the minimum limit switch wiring attached to Zmin P1.28 signal and adjacent ground pin and  maximum limit switch wiring attached to Zmax P1.29 signal and adjacent ground.

I have the endstop settings for the gamma pins left as default:

gamma_min_endstop                            1.28^   

gamma_max_endstop                            1.29^ 



Is there something set wrong in config for this setup?

Thanks





 





**"Kevin Lease"**

---
---
**Kevin Lease** *August 25, 2017 12:41*

not sure what happened, but started working now without making changes...


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/F8Zhi1ndKaa) &mdash; content and formatting may not be reliable*
