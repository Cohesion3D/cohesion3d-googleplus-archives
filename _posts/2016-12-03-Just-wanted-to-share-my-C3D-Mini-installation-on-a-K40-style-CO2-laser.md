---
layout: post
title: "Just wanted to share my C3D Mini installation on a K40 style CO2 laser"
date: December 03, 2016 03:29
category: "General Discussion"
author: "Carl Fisher"
---
Just wanted to share my C3D Mini installation on a K40 style CO2 laser. Might be a good reference if you have the same machine.



[https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0](https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0)







**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:57*

Thanks Carl. I have been sending all the new buyers a link to a Dropbox with this PDF and the config file. :) 



**+Anthony Bolgar** should be pulling some info from here for our more thorough reference documents going forward. 


---
**Anthony Bolgar** *December 03, 2016 05:19*

Yup,hopefully this weekend I can get it all finished up.


---
**Richard Vowles** *December 03, 2016 06:12*

The confusing thing about the PDF is the requirement to purchase the additional's - I have purchased the bundle and so it ostensibly includes this ("The bundle includes the Mini Board itself, 2 A4988 drivers, a PWM Cable for the laser, and a MicroSD Card with the laser configuration preloaded, allowing you to be up and running right out of the box. "). I had to go back and check, so you may wish to update the PDF? 



Otherwise, thanks for the detail!



The power on my existing board as 3 pins, not 4, so I presume its the kind that "doesn't fit & click".



My board has an "OUT" for the laser control (I think) which is a 3 wire JST. My power supply only has 2x24V, 1x5V and 2xGND. Not sure where that leaves me with the POT section?


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 06:15*

**+Richard Vowles** 

2 points:

Carl got his boards earlier before I was doing the current bundle configuration. You are indeed getting what you quoted. 

You're saying that your PSU has white JST connectors not screw terminals, and the pwm is a 3 pin jst, right?  Perfect! That's what the PWM cable actually is. You have the other kind of laser same as me. You should be fine on these notes. 

**+Anthony Bolgar** please take note of these points for your guide. 


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 06:16*

And to clarify, the pwm cable is 3 pin jst xh on both ends so you won't have to cut it. Those with a screw terminal like Carl have to cut one end and expose the bare lead. 


---
**Richard Vowles** *December 03, 2016 06:19*

Thanks - I just spent some time with my meter doing checks to find out what the plethora of buttons and dials connected to. One I can't figure out is the button that simply says HX40B. I mean, what?



Lighting, Laser Test, Current Regulator - but Laser Switch? I'm sure it will all work out :-)


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 06:22*

Not sure Richard. Attach some pics please. 


---
**Steve Anken** *December 03, 2016 06:44*

I expect to be running tomorrow after a bit of a glitch. Way easier than my first smoothie conversion but everything was new then and a bit much to take in.  Did not look at the PDF yet but I did use the config because I did not have a SD and did forgot I had to do that.



Thank you Ray for being so helpful. 


---
**Richard Vowles** *December 03, 2016 07:37*

**+Ray Kholodovsky** [https://goo.gl/photos/3qYFJTxi31KvY47r6](https://goo.gl/photos/3qYFJTxi31KvY47r6) - i'm sure it will be fine. I'll just follow the instructions and see how it goes :-)


---
**Jérémie Tarot** *December 03, 2016 07:57*

Is it on my side or file is damaged ? 


---
**Richard Vowles** *December 03, 2016 08:12*

Works for me **+Jérémie Tarot** 


---
**Jérémie Tarot** *December 03, 2016 08:33*

Arf, my bad, sent the link to downloader 

Sorry for the noise :/


---
**John Sturgess** *December 08, 2016 18:05*

I've got the moshi v4.6 in mine, does that make much difference with the C3D mini installation? 


---
**Ray Kholodovsky (Cohesion3D)** *December 08, 2016 18:09*

**+John Sturgess** It looks like the ribbon cable and motor connections are the same, however I am seeing what looks like a 6 pin power cable so that may need to be "revised" slightly on your end, and the mounting holes are probably different so you'll need to drill new ones. So based on what I've seen it'll be a little extra wiring work for you.


---
**Carl Fisher** *December 08, 2016 18:17*

I'd be curious to see some pictures of the different setup. 




---
**Ray Kholodovsky (Cohesion3D)** *December 08, 2016 18:18*

Likewise, pics help. Maybe a new thread in this group and make sure to write "Moshi 4.6 Machine" 

I was able to find a pic of the board on eBay so that's what I answered based on. 


---
**John Sturgess** *December 08, 2016 18:24*

Once the new board is here I'll do a how to unless someone beats me to it. 


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/8tqGxJwUhzP) &mdash; content and formatting may not be reliable*
