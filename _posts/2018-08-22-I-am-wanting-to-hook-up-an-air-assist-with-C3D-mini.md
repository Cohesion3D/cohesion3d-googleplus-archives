---
layout: post
title: "I am wanting to hook up an air assist with C3D mini"
date: August 22, 2018 16:29
category: "C3D Mini Support"
author: "Richard Wills"
---
I am wanting to hook up an air assist with C3D mini. What is the output pin for M7 air assist in light burn. I am running Grbl lpc with a 4 axis build. I have a 24v relay also.





**"Richard Wills"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 21:07*

It should be 2.7. This is good as it is the large right mosfet at the bottom. 

You should use a flyback diode if you are hooking up a relay to the mosfet. 



Please consult the pinout diagram as you will also have to provide power to “FET in” terminals. 


---
**Richard Wills** *August 27, 2018 21:07*

**+Ray Kholodovsky** I have hooked up the relay with a flyback diode. when I power the machine it triggers relay. But there is no response from console in lightburn for M7 or M9. 

I have it hooked to 2.7 FET. as soon as machine is turned on it has a constant 24v without any switching. any help would be appreciated


---
*Imported from [Google+](https://plus.google.com/115971902636333791915/posts/PrD6h6fzey7) &mdash; content and formatting may not be reliable*
