---
layout: post
title: "A public service announcement: Everyone , we want to help you, really, we do"
date: June 24, 2017 18:22
category: "General Discussion"
author: "Ashley M. Kirchner [Norym]"
---
<b>A public service announcement:</b>



<b>Everyone</b>, we want to help you, really, we do. However in order to provide you with the best possible level of support, you need to also provide us with some basic information. So, here are a few things that are important:



<b>If you have just installed your Cohesion3D board and are having issues</b>

Take a picture, or several pictures, of the connections, all the wires, where they're plugged in, and post them in your initial post. Take a picture of the stepper drivers so it's easy to see their orientation. Having the pictures will help identify problems with the wiring, if any. If you want to post the pictures as soon as you've installed the board just so we can verify your connections, by all means, do. Not every machine is built the same, not every machine have the same set of wires or control board. We can't be guessing what yours looks like, well, we could, we just don't want to.



<b>When you run into problems</b>

Check out the online documentations at [http://cohesion3d.com/start](http://cohesion3d.com/start) - there are several instructions, images, diagrams, and pictures submitted by others who may have the same setup as your machine. Try to do some debugging first: check the cables, make sure they're all plugged in right, and in the right spot (you did post pictures, right?). It could very well be something as simple as not having pushed one of the connectors in all the way.



<b>When you post about whatever issues you may be having</b>

Please follow instructions given as we try to debug things with you. It's hard trying to figure out what's going on if you don't follow directions. Things like screenshots are important! And please try not to jump ahead when we're trying to help debug problems. If someone asks for the results of step A, don't jump on ahead and come back with step C. We try to be methodical when debugging problems. All we ask if that you follow along. Also, please be descriptive. "I plugged it in and now I get nothing." That means absolutely nothing. What have you done, what have you tried, what software are you using, on a Windows, OS X, or Unix (Raspberry Pi).



<b>Grammar & punctuation!</b>

This may seem like we're nit-picking but please try to write proper sentences that one can understand. The nature of social platforms such as Facebook and Google Plus makes this a very diverse group and the last thing we need is to try and decipher what you are trying to convey to us. Write proper sentences, use punctuation where required/needed/necessary, make it so that there is little to no chance of anyone misinterpreting your post or what it is you are having an issue with, or wanting help with. <i>(and remember to post pictures!)</i>





**"Ashley M. Kirchner [Norym]"**

---
---
**Alex Hayden** *June 25, 2017 01:51*

I recommend you pin to the top.


---
**Ashley M. Kirchner [Norym]** *June 25, 2017 02:25*

Yup, it is.


---
**Tennessee Tony** *June 26, 2017 03:05*

Thanks. 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/DAwQYWRrZnD) &mdash; content and formatting may not be reliable*
