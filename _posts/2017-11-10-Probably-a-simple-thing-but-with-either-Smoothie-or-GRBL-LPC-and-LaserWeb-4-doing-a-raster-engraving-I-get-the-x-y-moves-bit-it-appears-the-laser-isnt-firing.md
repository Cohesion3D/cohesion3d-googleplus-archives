---
layout: post
title: "Probably a simple thing but with either Smoothie or GRBL-LPC and LaserWeb 4 doing a raster engraving, I get the x/y moves bit it appears the laser isnt firing"
date: November 10, 2017 23:03
category: "C3D Mini Support"
author: "David Fruehwald"
---
Probably a simple thing but with either Smoothie or GRBL-LPC and LaserWeb 4 doing a raster engraving, I get the x/y moves bit it appears the laser isn’t firing.  Did I miss something in the configs?  I set the tool on to M4 and tool off to M5 but no joy.







**"David Fruehwald"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 23:05*

Can you make a video so we can understand the issue better? 


---
**David Fruehwald** *November 10, 2017 23:08*

It moves in x and y as though it is engraving but the laser is not firing.  I switched back the the M2 Nano and K40 Whisperer and it is engraving the same SVG file just fine.


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 23:30*

The SVG is unlikely the issue - it could be configuration... 

Specifically, we want M4 in the start gCode and M5 in the end gCode for grbl. I don't think I need anything in there for Smoothie.  You don't want these to be in the tool on/ off fields at the bottom of that settings panel. Those should be blank. 



Can you test-fire from LW?


---
**David Fruehwald** *November 10, 2017 23:42*

I’ll give that a try after I swap the C3D back in.  I have GRBL-LPC on it right now and all the $ stuff set according to the K40 recommendations.


---
**David Fruehwald** *November 11, 2017 00:31*

OK, C3D back in with GRBL-LPC installed.  LaserWeb 4.0.991started and set for generic GRBL machine.  Carriage homes as expected and moves as expected, however laser test fire does not work.  I’m suspecting a pin mapping issue.  Using the compiled version from here [github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases) which has the laser PWM mapped to pin 2.5




---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 00:32*

Yeah that's all good. Not sure if test fire works on grbl. Can you verify the settings are as I described regarding where the M4 and M5 are? 


---
**David Fruehwald** *November 11, 2017 00:37*

Gcode start has M4 S0 and Gcode stop has M5, I also emptied out tool on/tool off as suggested.


---
**David Fruehwald** *November 11, 2017 00:48*

Think I found the problem.  I still had the power set on the panel as though I was using the K40 Whisperer (8%) I bumped it to 50% and it it engraving now.   Only disovered this because I forgot to rest the power after my last cut with the old board before I removed it.  I forgot that GRBL-LPC is managing the power and not just using on/off like the M2 Nano and Whisperer.  DOH!


---
**David Fruehwald** *November 11, 2017 00:50*

Bonus is I’m now finally getting my grayscale that was blowing up K40 Whisperer.  Thanks for helping out.  Hopefully I can move onto better questions. ;-)


---
*Imported from [Google+](https://plus.google.com/110578764378886132074/posts/XeU4PyQGyKx) &mdash; content and formatting may not be reliable*
