---
layout: post
title: "Sorry if this has been asked a billion times already..."
date: March 12, 2017 04:45
category: "General Discussion"
author: "Bob Buechler"
---
Sorry if this has been asked a billion times already... does anyone really like their choice of GLCD with their C3D? If so, why and which did you buy? 



I bought the adapter for my mini, and some of the GLCDs look pretty inexpensive so I thought I'd look into them. 



I'm also kinda wondering where people get the most value out of having one on their K40.



Thanks in advance.





**"Bob Buechler"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 04:51*

The adapter is really for that one GLCD only, the 12864. It makes for a good heads up display with coordinates and laser firing information, as well as a way to remotely start a job if you put it on the microsd card.  I'll leave it at that and defer to my customers for their actual experiences. 


---
**Bob Buechler** *March 12, 2017 05:04*

Ahh! Well, that certainly narrows it down nicely, doesn't it? :) Thanks, **+Ray Kholodovsky** 


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 05:08*

You can have any model, as long as it's this one. ![images/4d57d73964369fdcb89e1cbd4aec4d50.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4d57d73964369fdcb89e1cbd4aec4d50.png)


---
**Bob Buechler** *March 12, 2017 05:10*

Uhh... Hmm.. Where do I buy these things? :)

 

[reprapdiscount.com - Search - RepRapDiscount Online Shop](http://www.reprapdiscount.com/search?controller=search&orderby=position&orderway=desc&search_query=12864&submit_search=Search)


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 05:12*

Amazon ^^^ 

We've seen a few defective ones. Qc is terrible on these. Amz returns helps. 


---
**Bob Buechler** *March 12, 2017 05:12*

ahh... Amazon eh? ... how quaint. :P

/sarcasm


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 05:14*

I attached a screenshot above of what to look for. If you want to wait for the slow boat they're 10 bucks on Aliexpress. 


---
**Bob Buechler** *March 12, 2017 05:16*

Slim pickins with only two real options: Anycubic or OSOYOO.... I thought I saw someone just recently talking about quality problems with their OSOYOO. Anyone have any opinions either way? 


---
**Bob Buechler** *March 12, 2017 05:17*

I don't know why G+ refuses to notify me until way after someone replies. Sorry for the extra posts.


---
**Bob Buechler** *March 12, 2017 05:18*

Eh. I'll take a swing at Amazon here in a few days and see if I get lucky.


---
**Steven Whitecoff** *March 13, 2017 15:55*

Amazon, ReprapGuru and Reprap(something, one word0

Got mine via Prime next day on two day shipping. $15


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/i315qk85rTa) &mdash; content and formatting may not be reliable*
