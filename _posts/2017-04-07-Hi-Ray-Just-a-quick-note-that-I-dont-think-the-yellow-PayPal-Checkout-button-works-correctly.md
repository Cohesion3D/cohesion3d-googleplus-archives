---
layout: post
title: "Hi Ray, Just a quick note that I don't think the yellow 'PayPal Checkout' button works correctly"
date: April 07, 2017 12:16
category: "General Discussion"
author: "Phillip Meyer"
---
Hi Ray, Just a quick note that I don't think the yellow 'PayPal Checkout' button works correctly. I used this to try and place an order. It took me directly to PayPal who took payment from my PayPal account and then sent me back to your single page checkout where I started to register but your site wouldn't accept the order and dropped me back into the basket page. Oddly enough PayPal then refunded me saying that they thought it was a fraudulent transaction. I'm trying to create another order right now but going through your basket, hopefully that will work. (It looks like you use a hosted version of bigcommerce, it might be worth speaking with them about it)





**"Phillip Meyer"**

---
---
**Phillip Meyer** *April 07, 2017 12:19*

Ugh, I can't order from you, I keep getting a warning that something went wrong after I pay, although the payment is going through to you and then was credited by PayPal. I'm going to try one last time using a credit card, rather than PayPal.

![images/10304e7e9b35cb84763d8960a08d8344.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/10304e7e9b35cb84763d8960a08d8344.png)


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 12:19*

Hi Phillip, 

Can you email info@cohesion3d.com so that we can discuss further?

Thanks,

Ray


---
**Phillip Meyer** *April 07, 2017 12:24*

Hurray! I managed to order, payment worked with credit card. Hope you don't mind me posting here, I'm not complaining, I'm just trying to be helpful (I also have an e-commence business) as I would want to know if people were finding it hard to order or pay.

Looking forward to receiving my new Cohesion3D card!

![images/b8285b26d99784faa51b225553a28c7c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8285b26d99784faa51b225553a28c7c.png)


---
**Phillip Meyer** *April 08, 2017 19:04*

Hi Ray, all is good, I managed to place the order and it's marked as shipped. Very excited and looking forward to receiving delivery (I'll drop you a line anyway) 


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/dTistXPgkfh) &mdash; content and formatting may not be reliable*
