---
layout: post
title: "Hey Folks, Loving my K40 Cohesion3D upgrade so far"
date: January 18, 2018 19:02
category: "K40 and other Lasers"
author: "Joe Althaus"
---
Hey Folks, Loving my K40 Cohesion3D upgrade so far. Soo much easier/better than the stock junk. The kit that I ordered was the K40 upgrade kit with the GLCD. Everything seems to work as expected using Lightburn and LaserWeb. However, my LCD is just displaying garbage. I've double checked the wiring and before I dig much deeper I wanted to see if anybody knew what was going on. Thanks!

![images/c621216333236d153ca37b9721e90162.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c621216333236d153ca37b9721e90162.jpeg)



**"Joe Althaus"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 19:07*

Try powering from the k40 first (USB cable disconnected) and also try the other way (USB cable first with k40 power off) 


---
**Joe Althaus** *January 18, 2018 19:10*

Hi Ray, I just tried all permutations of connections and powering with same result. (Thanks for responding)


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 19:14*

Please see if flipping the cables helps (although presently you do have them the way I prefer) 



Failing that, we can try a different MicroSD card (format FAT32 and the files you need are in a Dropbox at the bottom of the install guide) 


---
**Joe Althaus** *January 18, 2018 19:16*

ok - i'll give it a go and report. Thanks!


---
**Joe Althaus** *January 18, 2018 20:58*

Ok, so swapping the cables didn't help - the LCD didn't even turn on at that point. 



However, the solution was in updating the files on the mircoSD card. At first, I simply transferred the files to the SD that ships with the Cohesion3D package and it worked right away. Excitedly, I unplugged my computer and started prepping for some cuts. My computer said disk unmounted improperly. Thinking that this may have been what caused the issue (although I never saw the LCD working before now) I transferred the files again thinking that I had inadvertently corrupted the files by not ejecting the disk properly. Unfortunately, this trick did not work a second time. So, ultimately, I have switched to a different microSD card and so far so good. But I have also been making sure to unmount and eject the disk properly too. Thanks for your help! As a by-product of this exercise now I have the most updated firmware and configuration! I may have had it before, but now the LCD tells me so ;) 

![images/f51638a386d767900ba1461c5dda4570.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f51638a386d767900ba1461c5dda4570.jpeg)


---
**Mike R** *January 18, 2018 21:13*

I get the same messed up screen sometimes, normally if I connect usb cable before powering on the k40.


---
**Wild Bill** *January 19, 2018 18:36*

Strange - I have been having the same problem with messed up LCD screen. On mine I found that if I plug the USB cable in first the screen works - then I power up the machine. If I power the machine up without the USB cable 9 out of 10 times I get the garbage screen and 1 out of 10 times the screen is OK. Not a problem for me because now I always plug the USB cable in first and it always works.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2018 18:38*

For these cases, I believe the reason it is (and varies) is that some computers/ USB ports cannot provide enough current at "jump start" and that is the result. 


---
**Mike R** *January 20, 2018 07:56*

Yes I think your right with the power for the USB. It's not a issue for me I just start the machine first.


---
**Wild Bill** *January 20, 2018 15:10*

It's strange that I am backwards to every one else. I do have a separate 24V power supply with the 5V from the laser supply still hooked up. Was playing last night, Turned on the power w/o USB and got the messed up screen. Turned the power off and as soon as the LCD display went out turned the power back on and the screen came on OK. But as long as we know how to make it work its not a big problem.


---
*Imported from [Google+](https://plus.google.com/113408307179068231626/posts/2E6zL7RGcHx) &mdash; content and formatting may not be reliable*
