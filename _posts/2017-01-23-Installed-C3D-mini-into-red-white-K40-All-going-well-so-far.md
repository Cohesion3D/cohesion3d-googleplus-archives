---
layout: post
title: "Installed C3D mini into red/white K40. All going well so far"
date: January 23, 2017 00:40
category: "General Discussion"
author: "Glenn Nanney"
---
Installed C3D mini into red/white K40.  All going well so far.  Including pics of my before and after connections for others, of course YMMV.  Note that the stepper motors needed  to be wired up with the white wire towards the middle on both connections, exactly the same as on the M2 Nano board.  This puts the configuration as bottom left is 0,0 in cartesian coordinates when using LaserWeb3/4.  Using Y endstop as MAX rather than min to define the Y axis.



Ray, thanks for a great board.







![images/844ccb12f90a5ae932f4bbcde0dc701b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/844ccb12f90a5ae932f4bbcde0dc701b.jpeg)
![images/17a6068e6a80e8bff4b77683602835a6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/17a6068e6a80e8bff4b77683602835a6.jpeg)
![images/82d7e38334cbbd90c294206787efe4aa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82d7e38334cbbd90c294206787efe4aa.jpeg)
![images/066c96c675252639be6013f095430ae6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/066c96c675252639be6013f095430ae6.jpeg)
![images/3eb09aefb8921f102ad7f4a4f32e72b1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3eb09aefb8921f102ad7f4a4f32e72b1.jpeg)
![images/6e7b28e113e6905fe8768ff0dccbe9bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e7b28e113e6905fe8768ff0dccbe9bd.jpeg)

**"Glenn Nanney"**

---
---
**Bill Keeter** *January 23, 2017 00:55*

Nice pics. I have the exact same setup.


---
**Andy Shilling** *January 27, 2017 17:49*

**+Glenn Nanney** I am doing a complete rewire as the original psu I have is from the dark ages. could you tell me if the k- k+ terminals are to trigger the laser and P+ and G are to arm it please. If possible could you post a photo of the whole wiring you have done as I really am hitting a brick wall because my original board has no markings on it to help.


---
*Imported from [Google+](https://plus.google.com/+GlennNanney/posts/3zqiNdKEwUz) &mdash; content and formatting may not be reliable*
