---
layout: post
title: "I would prefer to use mm/s as opposed to mm/m in LaserWeb, and I see the option to change it in LaserWeb, but if I change it would it mess up the settings in the Cohesion3D Mini config file and confuse LaserWeb or would it"
date: August 14, 2017 18:31
category: "C3D Mini Support"
author: "Tammy Mink"
---
I would prefer to use mm/s as opposed to mm/m in LaserWeb, and I see the option to change it in LaserWeb, but if I change it would it mess up the settings in the Cohesion3D Mini config file and confuse LaserWeb or would it not matter at all?





**"Tammy Mink"**

---
---
**Ariel Yahni (UniKpty)** *August 14, 2017 18:41*

It would not matter.


---
**Tammy Mink** *August 14, 2017 18:42*

Excellent! Thanks. 




---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/Z1NUQfNVnPd) &mdash; content and formatting may not be reliable*
