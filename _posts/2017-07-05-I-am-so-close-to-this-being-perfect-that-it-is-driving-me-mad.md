---
layout: post
title: "I am so close to this being perfect that it is driving me mad..."
date: July 05, 2017 21:19
category: "C3D Mini Support"
author: "Tim Anderson"
---
I am so close to this being perfect that it is driving me mad...



Please look at these pictures, one is the original sent to the laser and the other is the output.



I think it just needs running faster to bring the shades into it around the clouds and stuff but then if I do that wont I lose the black?



![images/9a21c639f3eb7ab4be68ef35ba65d2c6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9a21c639f3eb7ab4be68ef35ba65d2c6.jpeg)
![images/f1b6da3a411b8c8de4ea42998add9ad2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f1b6da3a411b8c8de4ea42998add9ad2.jpeg)

**"Tim Anderson"**

---
---
**Dushyant Ahuja** *July 05, 2017 23:45*

I think it might help if you increase the contrast in the original image. Right now the shades are too close together 


---
**Ashley M. Kirchner [Norym]** *July 06, 2017 01:08*

Also, have you tried multiple passes?


---
**Claudio Prezzi** *July 06, 2017 06:25*

You experience a general problem with laser engraving: light grays are hard to get, because there is a certain minimal power/duration needed to get a reaction on the wood.

You could try to do multiple passes, like **+Ashley M. Kirchner** writes and/or you can play with laser_module_pwm_period in the config file. In general, longer period (lower frequency) creates better grayscale. Try 200-600us with a gray bar.


---
**Tim Anderson** *July 06, 2017 06:33*

Ok cool thanks for the ideas ☺️ mine is set to 400us as it didn't work correctly at 200us. 



I think I will retry my raster chart, get that right and that should then follow through I guess... 



Hadn't thought of two passes, do you mean send the whole image twice or split it into shades? 



In the images above it is like white clouds then dark grey, there should be a slightly darker bit of cloud but it's just not there. 


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 13:35*

Yeah you can try a larger period value. There's no hard max or anything. Like Claudio said... he uses different values for different job types. 


---
*Imported from [Google+](https://plus.google.com/109492805829132327540/posts/BdfArS5csoM) &mdash; content and formatting may not be reliable*
