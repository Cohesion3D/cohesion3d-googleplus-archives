---
layout: post
title: "Now that I have my Cohesion3D mini I have noticed a problem when I am engraving grayscale images"
date: August 15, 2017 12:12
category: "C3D Mini Support"
author: "Tammy Mink"
---
Now that I have my Cohesion3D mini I have noticed a problem when I am engraving grayscale images. It's like my images are shifting or skipping on the X axis part way through. As far as I can tell it's not physically the air assist catching etc and on the stock board I was running faster and via the USB without kipping issues. The images I am engraving are at 300mm/s and on the stock I was running 500mm/s no issues, though that was black and white images. To eliminate the USB connection, I put the job on the SD card and that helped but I am still getting it some. Suggestions?





**"Tammy Mink"**

---
---
**Tammy Mink** *August 15, 2017 12:22*

I will also add while this only happened with one file so far, an image at 3 inches tall seemed to freeze the CohesionMini3D board up completely to the point where even the GLCD stops responding and everything has to be reset. This was a grayscale image at 300mm/s about 3 by 4 inches even running from the minisd card.


---
**Don Kleinschnitz Jr.** *August 15, 2017 12:36*

Pictures of the output would help.

Don't know if these are what your are experiencing: 

...There is a known throughput issue with LW-smoothieware.

...There is also a reoccurring and random skipping phenomena (left-right shift) that we have not solved.

Your stock machine only dithers. Smoothieware renders in either dither or raster depending on what your source is (rastering takes a lot more horsepower). I have not kept up with LW so I do not know if it offers dithering native. 


---
**Tammy Mink** *August 15, 2017 12:59*

I'll take some pictures when I get home, I will say it started to happen when I upped my speed from 150 mm/s to 300 mm/s and in the cases it was a grayscale bmp picture with raster engrave chosen. Seems the one that was freezing the machine totally up it up, stopped freezing up after I disabled "burn white". I was trying to run 3 passes at low power for better grayscale but maybe I'll have to slow down the engrave if 300mm/s is too fast for it. 


---
**Ashley M. Kirchner [Norym]** *August 15, 2017 15:19*

Slow down. As you said yourself, the stock board only did 2 things: move the stepper left and right, and turn the laser on and off. With a PWM signal from the C3D, there is a lot more data being pushed through, something had to give. Those machines aren't built for speed. Slow down. 


---
**Tammy Mink** *August 15, 2017 18:23*

Hopefully at slower speeds I'll be able to get those shades of gray. This one came out pretty good with the laser at lower power and like 4 passes even if I engraved it pretty small. I was pleased with the shades I could see. I suppose I can also cut down on the shades of gray in the photo. Less for it to calculate.

![images/653ff1ab666d4db8f0305e69c0cdc0c6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/653ff1ab666d4db8f0305e69c0cdc0c6.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 20:21*

Known data points (being posted from slow wifi connection far from home, hopefully others can fill in). 

I've heard of tiny dot size causing smoothie to freeze. You need to use at least 0.1mm in LW. 

Are you describing layer shifting? What are you stepper driver pots set at? By default the flat faces outward. If you've hooked up an external psu, you can turn them a full 90 clockwise. 

If still on laser psu, 45 degrees is about as far as I'd risk going before the lpsu can't provide enough current and starts browning out. And then you have to slow down your speeds if you have skipping. 


---
**Tammy Mink** *August 15, 2017 21:38*

I got again the board freezing up when engraving from the SD card. I was only going 200mm/s this time and the picture was about 150 mm across and 100 tall. I was doing grayscale but only 16 colors in the imagine and in LaserWeb. In LaserWeb it is set to the default 0.2 diameter. Should this be 0.1? The machine seems locked up and on the motherboard  I see 2 solid green lights and a red solid. It was also fairly early in the job. 


---
**Tammy Mink** *August 15, 2017 21:40*

My steppers are whatever they were when I got them.. I'm not sure where they are. I know where the chips are but not the stepper pots


---
**Tammy Mink** *August 15, 2017 21:43*

Here is a picture of the motherboard (most the wires are off so I could get the picture)

![images/dde9c47a3bd8c9724b89d3f92015c20b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dde9c47a3bd8c9724b89d3f92015c20b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 21:48*

0.2 is indeed default and great, I was just saying don't go below 0.1.



Current adjustment (pic):![images/042d1ce1d1eb672be98c3df494afe765.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/042d1ce1d1eb672be98c3df494afe765.jpeg)


---
**Tammy Mink** *August 15, 2017 22:01*

Both times it froze up totally it was running from the SD card. I'm trying to figure out if it's the SD card or the job itself. I was running from the SD card since it goes smoother then the USB for these grayscale jobs


---
**Ray Kholodovsky (Cohesion3D)** *August 15, 2017 22:04*

If you can get a different sd card (anywhere from 1 up to 16gb is fine), format fat32, and put just the config.txt file from my Dropbox on there (and then you can add your job files) this could rule that out. 


---
**Tammy Mink** *August 15, 2017 22:15*

All these old electrics I have an no extra microsd cards..lol..Well guess I'll have to order one. 


---
**Tammy Mink** *August 22, 2017 12:21*

I have not quite gotten back to this yet (got a new PC, troubleshooting other issues). I have put in a new minisd card. I have slowed my feed rate to no greater then 150mm/s and that seems to help, but I have been keeping my images no more then 3 inches. I have run a small raster job off the SD card and no crash, but I'm not sure what will happen if I make it more complex. Anyways I am considering trying to slow down my travel speed since it happens most between lines(much less often now that I slowed down). Where can I slow my travel speeds? 


---
**Tammy Mink** *August 22, 2017 22:29*

**+Ray Kholodovsky**. Even with a new minisd card, the GLCD and Cohesion3D mini is freezing up again from the minisd card. I was able to do one simple raster but it's back again. It was a simple b&w dither Raster job at 150mm/s it was just about 4 inches across... seems like it doesn't freeze up if I keep the items less then 3 inches or so. (this is on the working win 7 pc)


---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2017 22:30*

Is this with the USB Cable connected? 


---
**Tammy Mink** *August 22, 2017 23:38*

I this time the USB was connected, maybe one of the other times it wasn't but I'm not certain. 


---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2017 23:41*

My long-time request to you is to identify this problem job, save the LaserWeb workspace and the GCode output of it.  Run the gcode off the microsd card via the GLCD menu with the USB Cable <b>disconnected</b>, so the board is only being powered from the Laser.  Observe and report back.  If it freezes with the usb cable out of the loop, please send over those files so we can try to reproduce. 


---
**Tammy Mink** *August 23, 2017 03:23*

It's been numerous files that issues happened on but I know I have the one that just happened still on the minisd card...I have ran it without the USB connection and I think it still froze but with me trying all sorts of things and my attempt to get it working on my new machine.. I'm not 100% certain... so I need to retest. I also never got the shifting really  resolved (I haven't yet adjusted the little screws I admit because I was afraid I would turn them too far and kill the board) other then it only happens on occasion if I keep it at or below 150mm/s but seems the USB can't really keep up at that speed intermittently so thus why I was trying the SD card. I've been trying to keep my jobs smallish and slowish to avoid  the issues. I know the machine itself can handle the speed as I ran from stock a job at 400mm/s just today (yes I know simplier board). I might try putting to the laser grbl on it again but last time I couldn't get LaserWeb to control it right but I think my issue there was settings but it was a bit confusing there.


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2017 03:24*

Have you installed a separate power supply yet? 


---
**Tammy Mink** *August 23, 2017 03:49*

No, I have the power supply (24v, 5a) I bought but zero idea how to make it work and set it up, I don't know how I get power to the power supply since it only came with the box or etc. Maybe what I should do there is obvious but I've not done that much rewiring etc. 


---
**Tammy Mink** *August 23, 2017 03:52*

Also if I rewire the power won't that kill my ability to switch back to the stock at will? Or can it be wired in such a way that aids the Cohesion3D board without losing my ability to flip back if needed? 


---
**Tammy Mink** *August 23, 2017 12:06*



Besides the 12V 5 A switch box type that I can't seem to figure how I would even plug it into an outlet. I also have from a failed attempt at a connecting an inline blower, a 12V 6A power supply more meant for laptops that I have stripped out the red and white wires to be exposed but I know that is not ideal for this project, but maybe it can be used? At least on that one it has a wall plug, but it's not obvious what to do with it. How do I get power to the box type of power supply? Is there an adapter that I need? Once I get that part what do I do?  Can I somehow feed the power into the Cohesion3D mini without disrupting the connections for the stock board? 



[ebay.com - Details about  AC 110V 220V to DC 5V/12V/24V Switching Power Supply Driver for LED Strip/CCTV](http://www.ebay.com/itm/AC-110V-220V-to-DC-5V-12V-24V-Switching-Power-Supply-Driver-for-LED-Strip-CCTV/272073668891?ssPageName=STRK:MEBIDX:IT&var=570884247959&_trksid=p2060353.m2749.l2649)




---
**Tammy Mink** *August 23, 2017 12:54*

I suppose one other option I have seen on this forum is looking for a "data only" USB cable or make one with some instructions online. I figure stripping down an existing USB wire is less permanent to the machine the rerouting the power (ok besides sacrificing a USB cord).


---
**Tammy Mink** *August 24, 2017 15:49*

Since I am still clueless on how to install a secondary power supply so I think my only other choices are to buy another USB cable and strip out the powering wire (making it data only) and/or try going to grbl again. 


---
**Ray Kholodovsky (Cohesion3D)** *August 24, 2017 15:57*

You would need a 24v psu for best performance. 



Search ebay for "24v 6a power supply"




{% include youtubePlayer.html id="vajgCQMir5o" %}
[youtube.com - Wiring up a Power Supply (PSU)](https://www.youtube.com/watch?v=vajgCQMir5o)



[http://img.dxcdn.com/productimages/sku_388661_7.jpg](http://img.dxcdn.com/productimages/sku_388661_7.jpg)



Wiring involves, Gnd, L, and N from an AC cable, either directly by cutting the non-wall end off an IEC cable and connecting the 3 wires like in the video,  or thru an "IEC Socket" which is more work... 



Then you run 2 wires to main power in screw terminal on the cohesion3d mini board and disconnect the +24v wire from the laser power supply - Mini, but keep L and Ground connected to the Mini. 


---
**Tammy Mink** *August 24, 2017 16:18*

Thank you for the information. I have a 24v 5A power box, the IEC Socket does seems like a more stable and safe option somehow splicing a power cable but his might be farther then my wiring skills can take me; as I might want to err on the side of not destroying my machine….I have to weigh the risk/reward as I didn't think this drop in replacement would require rewiring my power supply connections when I decided to order. I also worry that doing this will make me unable to use the stock board, and if nothing else the stock board has been stable as opposed to the Cohesion3D which has been anything but (though I have made a few cool things from it and LaserWeb is good software). I guess I can try the very other few options I have (data only USB cable & grbl etc) and see how that goes and then I can decide if it's worth the risk or just to give up. Maybe if I go grbl successfully this time between not using the GLCD (thus less power draw) and the different firmware (as the grbl doesn’t access the mini-sd so hopefully less interference) it will help. 




---
**Ray Kholodovsky (Cohesion3D)** *August 24, 2017 18:56*

You don't need to do this psu change now.  



I need the results of the test I told you about before, problematic file run from SD Card with USB cable disconnected, in order to be able to assist you further.  Otherwise we're all just chasing our tails here to no end. 


---
**Tammy Mink** *August 24, 2017 19:28*

Yes,  I admit I totally haven't done the further tests we talked about, I've been running off the stock board and LaserDrw on Windows 10 the last 2 days. I’m just building my tolerance back up to take another stab at it. LOL. I wonder if the grbl might connect to Windows 10/my new pc easier, probably only one way to find out. Yes I know I'm bouncing around on the problems. 


---
**Tammy Mink** *August 25, 2017 01:35*

I managed to get grbl working after tinkering with the settings and so far so good suprisingly. So far no disconnects (on the new Win 10 machine too) and no shifts and seems smoother in rastering. Of course just ran a few tests so still a bit early but I've gotten grbl going at least now.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/79tUdbqHyks) &mdash; content and formatting may not be reliable*
