---
layout: post
title: "Ok, I did not know what group to put this in but thought i would start here as Cohesion3D Remix is the brains of my 3D Printer"
date: June 13, 2017 11:23
category: "3D Printers"
author: "Robert Hubeek"
---
Ok, I did not know what group to put this in but thought i would start here as Cohesion3D Remix is the brains of my 3D Printer. 





I want to run a ATX power supply. but not sure of what WATT's to get



I have a 38CM x 38CM Silicone Heaterbed

DRV8825 Drivers x6

Nema 17 x 6

E3D Chimera Head

BLTouch

Full GRaphic Smart LCD

Cohesion3D Remix



I read that due to the size of the heaterbed i should go direct power for this, is this correct do i just need the solid state relay to wire this up with the Cohesion3D? 



Next question is, I do have a spare Corsair RM1000x 1000W power supply. i know it is over kill for aa 3D printer but could it run both the Electronics and the 3D printer?  

![images/b564bf5bb8968285d1148c0be22cc561.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b564bf5bb8968285d1148c0be22cc561.png)



**"Robert Hubeek"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 02:58*

Hey Robert.  Have you tried the main 3D Printing group? 



Happy to try answering your questions though.  



You said ATX PSU, so I'm assuming 12v.  For a 38x38cm heatbed, well it has a certain power rating which means a certain # of amps at a certain voltage (12v, 24v, or AC).  I would say 12v is 30+ amps.  Don't do that.  Do AC with SSR.



So without a heatbed factored in, 400 - 600W for the basics there should be enough, and anything higher is fine. 



Frankly, I'd go 24v, and AC + SSR for the heatbed. 


---
**Robert Hubeek** *June 14, 2017 03:23*

The reason i asked here was due to the Cohesion bored, from what i researched people stated the board was the main issue. e.g can it support the AMPS and SSR and so on. 



Now i guess next question is, How do i control a Heaterbed,SSR and Temp of the bed via the 3Dremix? i understand how to wire up the SSR and the powerpoint plug but not much more from that. 






---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 03:25*

Thermistor as usual, mosfet on the C3D controls the ssr. 



The Amps is way high for any board. 


---
**Robert Hubeek** *June 15, 2017 07:55*

Going 24v route. 



Anything i need to change? or does the cohesion take care of all the voltages for everything? 



Plan is 24V power supply and SSR + AC for the heater bed as recommended but the heater element on the printer hot end will have to be changed? or does the C3D only supply certain voltage to that  . 








---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 14:14*

Main (motors and logic), bed, and bottom row of mosfets each have their own power input. So you can mix and match. 

If you have a 12v E3D and want to run 24v everywhere there is a setting in config to set a duty cycle for the heater. I think 2 PSU's might be safer. Use 24v for the motors and 12v to the bottom fets. Consult the pinout diagram on the docs site thoroughly :)


---
*Imported from [Google+](https://plus.google.com/+RobertHubeek/posts/E472G4bf3CG) &mdash; content and formatting may not be reliable*
