---
layout: post
title: "Ray, I saw the extensive instructions for adding a rotary when using the C3D Mini, but is there a similarly detailed set of instructions for adding one when using the ReMix board?"
date: March 18, 2018 01:02
category: "C3D Remix Support"
author: "Mike Hall"
---
Ray, I saw the extensive instructions for adding a rotary when using the C3D Mini, but is there a similarly detailed set of instructions for adding one when using the ReMix board? I bought additional drivers at the same time of the board. I plan to get another PSU soon, but wanted to go ahead and get things wired up. I would appreciate any guidance or direction to where I can find the info I need. Thanks! 





**"Mike Hall"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2018 01:06*

Same exact thing, really. Just the 5v header is on the upper right area of the board. Consult pinout diagram. 


---
**Mike Hall** *March 18, 2018 20:10*

**+Ray Kholodovsky** Ok, I'm using the A4988 stepper driver I bought from you, so it's a bit different, but I'll keep at it until it makes sense to me. You should know by know I'm a bit ignorant in this arena. ;-) 


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2018 20:11*

Then just leave the config alone, mostly.  (Don't put !o at the end if you're using Smoothie)


---
**Mike Hall** *March 18, 2018 20:13*

Will do, thanks.


---
*Imported from [Google+](https://plus.google.com/106337213390028821874/posts/X2s7Yq9Tj4X) &mdash; content and formatting may not be reliable*
