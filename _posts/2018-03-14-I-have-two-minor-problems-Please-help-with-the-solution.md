---
layout: post
title: "I have two minor problems. Please help with the solution"
date: March 14, 2018 15:00
category: "K40 and other Lasers"
author: "Vita Slovakia"
---
I have two minor problems. Please help with the solution.



1,If I first switch on the main switch on to K40 and then plug in the USB cable to PC, I will not find the display. If I do this in reverse, everything is fine. 



2, Instead of the A4988 Stepper Motor Driver I used the Drv8825 Stepper Motor Driver. All images after burning are half-sized. Can I set a different number of steps in the config file? Or is HW intervention necessary? So far I have only changed it through the display. For the time being, I've modified it in config:



alpha_steps_per_mm                           315,15          # Steps per mm for alpha stepper

beta_steps_per_mm                            315,15          # Steps per mm for beta stepper

gamma_steps_per_mm  



Everything else works as well, and thanks for such an excellent control board.





**"Vita Slovakia"**

---
---
**Jim Fong** *March 14, 2018 15:59*

The drv8825 will default to 32microsteps on the c3d board.  You will need to double the steps_per_mm in the config.txt.  315.15 would be correct. 



I turn on the computer first before the laser.  The display usually initialize correctly that way. 


---
*Imported from [Google+](https://plus.google.com/105422396409020564447/posts/Lu4v1igSkGm) &mdash; content and formatting may not be reliable*
