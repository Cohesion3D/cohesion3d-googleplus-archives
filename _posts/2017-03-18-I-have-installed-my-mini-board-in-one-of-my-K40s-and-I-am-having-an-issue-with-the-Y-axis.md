---
layout: post
title: "I have installed my mini board in one of my K40's and I am having an issue with the Y axis"
date: March 18, 2017 02:41
category: "K40 and other Lasers"
author: "Anthony Bolgar"
---
I have installed my mini board in one of my K40's and I am having an issue with the Y axis. Ity does not move properly it vibrates badly and moves forwards and backwards a few millimeters, not in any proper direction. ANy ideas of what is wrong and how to fix it?





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 02:43*

Show a video please, possibly swap over with X to rule out that the driver module isn't bad? 


---
**Anthony Bolgar** *March 18, 2017 02:46*

OK, will swap the driver module, if it is bad no biggie, I have 20 or 30 of them kicking around. If that isn't the problem, I'll post a video. Thanks Ray.


---
**Claudio Prezzi** *March 18, 2017 09:29*

It could also be the motor wiring, like one broken or half broken wire.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/DQ5adq8J1HU) &mdash; content and formatting may not be reliable*
