---
layout: post
title: "Ray Kholodovsky to find out the info your readers look for, you should really offer a sort out by category"
date: February 05, 2017 13:03
category: "General Discussion"
author: "Stephane Buisson"
---
**+Ray Kholodovsky** to find out the info your readers look for, you should really offer a sort out by category.



to create categories for this community, head into "edit community" it's located at the bottom of it. 



cnc, laser, C3D mini, C3D Remix, etc...



you can also create (up to 10) links. to your site ...





**"Stephane Buisson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 00:21*

I made you a mod. Get to work :)


---
**Kris Sturgess** *February 06, 2017 01:10*

When in doubt... delegate.


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 01:11*

Stephane knows what he's doing. And it's only fair I return the favor to him since he made me a mod of the k40 group. 


---
**Kris Sturgess** *February 06, 2017 01:16*

That sounds fair. I can't post on the K40 Group any more.:-)


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 01:27*

Why not?


---
**Kris Sturgess** *February 06, 2017 01:29*

I corrected a moderator on a rude comment made directly about me. Shortly after I could no longer post or re-join the group.


---
**Stephane Buisson** *February 06, 2017 07:30*

**+Kris Sturgess** you are the first to report you can't post. it could happen you are hold by G+ in the spam box, I need to wake up (EU time) to find out, and release it. in +2 years of K40 community I kick out only 2 asian spammers and block only few posts without ban.

So please try again


---
**Stephane Buisson** *February 06, 2017 07:37*

**+Ray Kholodovsky** I have limited time but will try my best. (categories done)


---
**Kris Sturgess** *February 06, 2017 15:06*

Hi **+Stephane Buisson**​ it gives me a "Can't join community. Try again later."


---
**Stephane Buisson** *February 06, 2017 19:20*

**+Kris Sturgess** no need to join, the K40 is open to all public. you should be able to make a post straight away.


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 19:21*

**+Stephane Buisson** nice work on the categories!


---
**Stephane Buisson** *February 06, 2017 19:24*

**+Ray Kholodovsky** and links into "about community" . ;-))


---
**Kris Sturgess** *February 08, 2017 05:25*

**+Stephane Buisson** you need to join the group to be active on it. Every Google+ group has a join/member button. I also do not get the add comment box you normally see to post....Essentially locked out. 


---
**Stephane Buisson** *February 08, 2017 07:24*

**+Kris Sturgess** I can see a Kris Sturgess as member in K40 community, but with no icon on the profile. if it's you, did you use another gmail address ??? if so, look that way ... (I have no other option to help within G+)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/RTVPFbEFZpo) &mdash; content and formatting may not be reliable*
