---
layout: post
title: "K40 eBay Chinese CO2 Laser Upgrade to potentiometer"
date: March 31, 2018 06:07
category: "General Discussion"
author: "Allen Russell"
---
K40 eBay Chinese CO2 Laser Upgrade to potentiometer.



I would like to add potentiometer and get rid of the digital so I can have better control or can I have both?



Need help if anyone knows how to do this. Thanks





**"Allen Russell"**

---
---
**Anthony Bolgar** *March 31, 2018 07:52*

You would have one or the other. It is very simple to swap the digital display for a Pot and mA meter. I think **+Don Kleinschnitz** has documented the procedure.


---
**Don Kleinschnitz Jr.** *March 31, 2018 12:27*

You can add just a meter and leave the rest of the controls.

or

You can replace the entire panel which includes meter, pot, laser switch, laser test button and move the power switch.



These should help. 



[http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)



[http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)

[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)


---
**Allen Russell** *March 31, 2018 16:07*

Thanks guys, This is what I have Now

![images/ee6d2ef8aeff03de4eefdb1d9cb88b25.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ee6d2ef8aeff03de4eefdb1d9cb88b25.jpeg)


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/fQfd77VvrSc) &mdash; content and formatting may not be reliable*
