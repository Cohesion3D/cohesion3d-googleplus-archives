---
layout: post
title: "So when they do away with google+ where is this page going to move to?"
date: November 15, 2018 01:31
category: "General Discussion"
author: "Tim Shepherd"
---
So when they do away with google+ where is this page going to move to?





**"Tim Shepherd"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2018 01:53*

I have no idea.  There should be a way to save the contents for historical and reference purposes. We have a decent amount of activity on the fb group, might just move fully over there. 


---
**Tim Shepherd** *November 15, 2018 17:23*

What a pain! i hope you guys find a new location to keep posting this helpful information


---
*Imported from [Google+](https://plus.google.com/104043869405329045983/posts/PBkw6dZGNpv) &mdash; content and formatting may not be reliable*
