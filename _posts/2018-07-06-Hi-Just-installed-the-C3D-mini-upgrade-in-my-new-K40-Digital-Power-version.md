---
layout: post
title: "Hi! Just installed the C3D mini upgrade in my new K40 (Digital Power version)"
date: July 06, 2018 15:44
category: "C3D Mini Support"
author: "Robert Luken"
---
Hi!

Just installed the C3D mini upgrade in my new K40 (Digital Power version). I also installed a separate 24V supply to power the C3D mini, directly to the main terminals. I removed the 24V wire from the LPS and now have only L and G connected from the LPS to the mini K40 Connector. Everything seems to be working but I haven't tried to engrave yet. My question is, the documentation refers to alternately connecting L from the LPS to the FET 2.5 terminal. Which connection is preferred and why?





**"Robert Luken"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2018 18:48*

Leave as you have described. The last bit of the install docs are for people that have a different style plug. 


---
*Imported from [Google+](https://plus.google.com/107953279340834579617/posts/Epy2826kJZD) &mdash; content and formatting may not be reliable*
