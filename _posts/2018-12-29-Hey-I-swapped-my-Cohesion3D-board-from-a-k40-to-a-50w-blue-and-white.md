---
layout: post
title: "Hey, I swapped my Cohesion3D board from a k40 to a 50w blue and white"
date: December 29, 2018 17:09
category: "K40 and other Lasers"
author: "Aaron Cusack"
---
Hey, I swapped my Cohesion3D board from a k40 to a 50w blue and white. I have everything set wired up correctly and it home's to the correct position. 



1. I set the work space area to the same size as my new honeycomb bed around 480×350 in the setting menu. But when I start a job it tries to go up and grinds the gantry. It's like the o.o start position is up a few cm's off the gantry.



2. How can I set the workspace to the honeycomb part in the middle?





**"Aaron Cusack"**

---
---
**Aaron Cusack** *December 29, 2018 17:12*

![images/864bbb78b7381d26f6cd55c93896845f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/864bbb78b7381d26f6cd55c93896845f.jpeg)


---
**ThantiK** *December 29, 2018 18:57*

So, remember that it homes to the 0, 350 position.  0,0 is actually located in the bottom left.  If you have your home position set incorrectly to top left, then it thinks it's at 0,0 when it's at 0,350 and tries to go up instead of down.



Make sure you set (in lightburn if that's what you're using) the 0,0 to bottom left.


---
**Aaron Cusack** *December 29, 2018 20:36*

**+ThantiK** Yea I use lightburn and have set on 0,0 (bottom left) 

Just hits off the gantry when I cut something near the top. It's like the top limit is off by a few cm.



Also is there a way to make a boundary say like 5cm all round the work space so I only cut in the honeycomb area?


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2018 20:52*

You need to adjust the new work area dimensions in the C3D config file as well. 

There’s an article on lasergods


---
*Imported from [Google+](https://plus.google.com/104714035697130129832/posts/1Nwqfzz6EYg) &mdash; content and formatting may not be reliable*
