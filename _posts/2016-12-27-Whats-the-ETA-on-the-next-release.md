---
layout: post
title: "What's the ETA on the next release"
date: December 27, 2016 20:38
category: "General Discussion"
author: "Kelly Burns"
---
What's the ETA on the next release.  If I pre-order today, when do think they would ship?  I believe I have an everything needed for the Smoothieboard setup, but a drop-in replacement sounds easier ;)





**"Kelly Burns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 20:44*

The production run is almost complete, and new orders should be shipping in just a few weeks.  We are doing everything in our power to ship as soon as possible. 


---
**Kelly Burns** *December 27, 2016 20:46*

Thanks Ray.  I like what you are doing.  You were supposed to contact me though.  I would have been a good Guinea Pig ;)


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/e8rjRqLwRwz) &mdash; content and formatting may not be reliable*
