---
layout: post
title: "Air assist for grbl-lpc, c3d and LightBurn 8.02 I've been running smoothiware on the C3D and have air assist working with LightBurn 8.02 (M105/M106) using FET 2.6 to turn on a 5V relay"
date: August 28, 2018 00:34
category: "FirmWare and Config."
author: "Robert Luken"
---
Air assist for grbl-lpc, c3d and LightBurn 8.02

I've been running smoothiware on the C3D and have air assist working with LightBurn 8.02 (M105/M106) using FET 2.6 to turn on a 5V relay.

Now I'm trying to get air assist working with grbl-lpc. Using the 3 axis firmware referenced in the LightBurn setup docs. LightBurn let's me select either M7 or M8 for Air Assist. The grbl-lpc info says M7/M8/M9 are mapped to 2.7 on the C3D. The gcode generated does use the selected M7 or M8 code. When I manually issue a M7 code I get ERROR:20; however M8 returns ok. Neither turns on FET 2.7.

Any help would be appreciated.





**"Robert Luken"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 28, 2018 02:39*

M8 doesn’t work. M7 is what you need. 

**+Jim Fong** mentioned that perhaps a version of grbl-lpc does not have these M7 series of commands built in. I’m not too sure. Jim, any thoughts? 


---
**Jim Fong** *August 28, 2018 03:17*

If you are using a early version of grbl-lpc, m7 wasn’t turned on.  Try a newer version. m8/m9 is broken and doesn’t work for any version.   It turns on but can’t be turned off. 


---
**Claudio Prezzi** *August 28, 2018 06:51*

On my latest release M7/M9 should be working. M8 has a problem, like Jim wrote. M7 is on pin 2.7 of the C3d boards.


---
**Claudio Prezzi** *August 28, 2018 07:16*

**+Robert Luken** Do you have the C3d Mini or the Remix? I think M7/M8 was never tested on the Mini.


---
**Ray Kholodovsky (Cohesion3D)** *August 28, 2018 13:26*

Just about everyone has the Mini :) 

Thanks Claudio. 


---
**Robert Luken** *August 28, 2018 14:49*

**+Claudio Prezzi** I changed to a different firmware(firmware grbl-lpc C3Dmini 4 axis with XY homing.bin). It now works with M7 on the 2.7 pin. Where is your latest release?


---
**Ray Kholodovsky (Cohesion3D)** *August 28, 2018 17:08*

You can just use that, 4 axis should not slow down anything even if you never use rotary. 


---
**Claudio Prezzi** *August 28, 2018 17:16*

**+Robert Luken** I would stay with Ray's version, if it works. I think this is very close to my last release. 

I just have additional versions for other board types @ [https://github.com/cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC).


---
**Robert Luken** *August 28, 2018 17:37*

**+Claudio Prezzi** Ok, thanks. I did notice that if I set the max power for a cut to anything above 0, I get ~ 3ma. Is there a min power setting I need to change?


---
**Robert Luken** *August 28, 2018 22:32*

**+Ray Kholodovsky** Thanks, I do plan on setting up a rotary sometime so that will be fine. I've noticed that with the current setup I can't set any power below 3ma. This didn't happen with smoothie. Also grbl-lpc notes mentions using pin 2.5 for Laser PWM. Is that the same as connecting to the Laser pin of the 4Pin header (I'm currently hooked up that way, (Laser + ground).


---
**Ray Kholodovsky (Cohesion3D)** *August 28, 2018 22:34*

I don’t know, and yes. 



What are you doing to test the 3mA case/ how can I replicate it?


---
**Jim Fong** *August 28, 2018 23:14*

**+Robert Luken**  it’s probably due to the laser PWM frequency.  The default value is different from smoothieware.   Grbl is set at 5000hz.  Some laser power supplies work better with higher frequency.  


---
**Robert Luken** *August 28, 2018 23:30*

**+Jim Fong** Yes $33 is set to 5000, should I change it? What are the pros and cons?


---
**Jim Fong** *August 28, 2018 23:35*

**+Robert Luken**  you can lower it and see what happens. I forget what smoothieware default was set at.  I’m thinking it was around 2000hz.   On my k40, higher Works better for greyscale.  


---
**Robert Luken** *August 28, 2018 23:45*

**+Jim Fong** I'll play with it, what's the upper limit. I just did an engrave on a tile. Tried 200/50, scan, Atkinson, but had to do 2 passes, came out great. Have to try it again with transfer paper and higher power.


---
**Jim Fong** *August 28, 2018 23:57*

**+Robert Luken**  all laser power supplies are different so can’t really say what the upper limit is.     



I did some testing a couple years ago but I would have to look at my notes to see what the results were again. 


---
**Claudio Prezzi** *August 29, 2018 06:56*

**+Ray Kholodovsky** I think the default "laser_module_pwm_period" of smoothieware is 20 microseconds (=50kHz) which is much too fast for the cheap K40 power supplys. Most people use something around 200us (=5000Hz).

In my experience, lower frequencies get a bit better grayscale and higher frequencies a bit more cutting power. 


---
**Claudio Prezzi** *August 29, 2018 07:12*

**+Robert Luken** Do you still have the pot connected? What I usualy do to get the best grayscale results is limit the max power with the pot to the darkest color I want and use the full range of 0-100% in LaserWeb. This way the PWM range of the conroller can use the whole dynamic range available.

But 1.5 - 3 mA is anyways the minimum to get the tube lazing.


---
**Jim Fong** *August 29, 2018 11:18*

I checked my notes.  I had changed my pwm frequency to 2000hz for better greyscale. 



I also checked the default for Cohesion3d smoothieware config is 200microseconds which is 5000hz, same with grbl-lpc. 



Goes to show, that memory is getting faulty and why I write everything down.  


---
**Ray Kholodovsky (Cohesion3D)** *August 29, 2018 11:30*

**+Claudio Prezzi** correct, stock smoothie config has period of 20 microseconds. K40/ other Chinese lpsu does not like this. 



We ship our smoothie config file with a period of 200 by default and if people still have issues then I recommend them to try a period of 400. 


---
**Robert Luken** *August 29, 2018 12:56*

**+Claudio Prezzi** My K40 has a digital panel and I have it set for 31% which gives 16ma max laser current on the meter I added. And I use 0-100% in Lightburn. I'll try your suggestion. I'm also replacing the panel with a custom one including a pot. Question, what drives the laser light on the panel when the laser fires? Is it just an LED in series with the digital Pot and the LPS?


---
**Robert Luken** *August 29, 2018 13:09*

**+Ray Kholodovsky** I now have the 2.6 and 2.7 pins connected to the ground return of my 5V relay, high side of the relay to +5V. so that air assist works with both Smoothie and grbl. Thanks for the help


---
**Claudio Prezzi** *August 29, 2018 13:43*

**+Robert Luken** I don't know how the LED on the digital panel is driven. I guess it's just a resistor and LED from the L signal.


---
**Ray Kholodovsky (Cohesion3D)** *August 29, 2018 16:58*

The K40 digital panels suck for this and other reasons concerning grayscale contrast.  I always recommend installing a 10k pot and mA meter so that you can get maximum grayscale contrast.  


---
**Richard Wills** *August 29, 2018 18:43*

**+Robert Luken** are you putting voltage to the FET in, I can not get mine to work with grbl lpc, I am using a 24v relay. The relay is powered when machine is turned on but can not shut it off. Tried 2.6 and 2.7


---
**Robert Luken** *August 29, 2018 19:55*

**+Richard Wills** I'm using a 5V relay module to operate my 24V solenoid. The high side of the relay is connected to my 5V supply and the (2.7) "FET2 ext - " pin is connected to the low side of the relay. The relay then apples 24V to my air solenoid to apply full air pressure to the laser head. There is a bypass with a needle valve to apply just enough air pressure to keep the lens clear. Currently the air pump runs when the machine is on but I'm adding a switch to enable/disable it. For Smoothie I have the (2.6) MOSFET3 low pin connected to the relay low side.


---
*Imported from [Google+](https://plus.google.com/107953279340834579617/posts/axhpC6cXERf) &mdash; content and formatting may not be reliable*
