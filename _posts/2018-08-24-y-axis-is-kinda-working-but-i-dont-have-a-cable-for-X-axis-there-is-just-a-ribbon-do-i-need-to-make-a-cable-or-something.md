---
layout: post
title: "y axis is kinda working but i dont have a cable for X axis there is just a ribbon do i need to make a cable or something"
date: August 24, 2018 03:46
category: "General Discussion"
author: "James Muirhead"
---
y axis is kinda working but i dont have a cable for X axis there is just a ribbon do i need to make a cable or something 





**"James Muirhead"**

---
---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 04:02*

If you have a ribbon cable it carries the endstops and the x axis motor connections.


---
**Ray Kholodovsky (Cohesion3D)** *August 24, 2018 15:47*

Have you read the instructions at Cohesion3D.com/start ? [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**James Muirhead** *August 24, 2018 23:14*

I followed and installed everything correctly 


---
*Imported from [Google+](https://plus.google.com/108641697491643088534/posts/MqmqnQTipiD) &mdash; content and formatting may not be reliable*
