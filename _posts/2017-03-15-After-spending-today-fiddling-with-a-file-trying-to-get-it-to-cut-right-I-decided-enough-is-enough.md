---
layout: post
title: "After spending today fiddling with a file, trying to get it to cut right, I decided enough is enough"
date: March 15, 2017 07:37
category: "General Discussion"
author: "Ashley M. Kirchner [Norym]"
---
After spending today fiddling with a file, trying to get it to cut right, I decided enough is enough. I've had the Mini for the past three months and just didn't get a chance to put it in. Today was the day. Yanked out the M2 Nano, put in the C3D, including a rogue wire for "L" (per Ray's instructions.) Dealt with an oddly behaving stepper driver, and voila. Was able to do a test cut, no issues.



Now it's time to figure out the ins and outs of LaserWeb3 (or 4) ... and see where this next adventure takes me.





**"Ashley M. Kirchner [Norym]"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 07:38*

Awesome!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/dTWZuZtK865) &mdash; content and formatting may not be reliable*
