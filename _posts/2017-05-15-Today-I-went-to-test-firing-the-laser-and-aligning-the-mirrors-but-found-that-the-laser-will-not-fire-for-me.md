---
layout: post
title: "Today I went to test firing the laser and aligning the mirrors but found that the laser will not fire for me"
date: May 15, 2017 22:47
category: "K40 and other Lasers"
author: "David Cantrell"
---
Today I went to test firing the laser and aligning the mirrors but found that the laser will not fire for me. It makes a hissing sound when I press the test fire button but thats it. What would cause this? 
{% include youtubePlayer.html id="T2-ogkJsXaE" %}
[https://www.youtube.com/watch?v=T2-ogkJsXaE](https://www.youtube.com/watch?v=T2-ogkJsXaE)





**"David Cantrell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 22:49*

Did you put the board in, if so when, and did it work after you put it in? 


---
**David Cantrell** *May 15, 2017 23:07*

Those are the photos of my setup. Yes the board is installed but this is the first time I went to really test the install.


---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 23:12*

As long as you didn't unplug the potentiometer wires, laser firing has nothing to do with the board.  It's really a good idea to just verify a simple test fire with the stock electronics before changing over... I think you need to verify all the tube connections and power supply performance first.  The K40 group may be of more help. 


---
**David Cantrell** *May 15, 2017 23:36*

![images/a249e0b37a81f1619a920e648d17fd2d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a249e0b37a81f1619a920e648d17fd2d.jpeg)


---
**David Cantrell** *May 15, 2017 23:37*

![images/d2ebde415d50106b82811d40137b66ff.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d2ebde415d50106b82811d40137b66ff.jpeg)


---
**Eric Lien** *May 16, 2017 00:01*

Is it just me or is that cooling line completely kinked? 


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2017 00:02*

k40 group:  [K40 LASER CUTTING ENGRAVING MACHINE](https://plus.google.com/u/0/communities/118113483589382049502)


---
**Don Kleinschnitz Jr.** *May 16, 2017 11:34*

**+David Cantrell** It sounds to me like you have an arc somewhere? Is the tube lighting up at all?


---
**David Cantrell** *May 16, 2017 12:08*

**+Don Kleinschnitz** No, I don't see the tube making any light. All it does is make the hissing sound. I have looked and all of the connections seem ok. If there is an arc, I don't really know were to look.


---
**David Cantrell** *May 16, 2017 12:11*

**+Eric Lien** I guess it does look a little kinked but I have seen bubbles moving in the tubes, when the pump is running. So I think I am good for water flow.


---
**Don Kleinschnitz Jr.** *May 16, 2017 12:37*

**+David Cantrell** turn out the lights at night 



Open the cabinet where the LPS is and push the test button and look for any arcs around the LPS or its wiring.

Then open the laser tube cabinet looking into the laser compartment off axis (just in case) to see if there is any arching which will occur near the anode when you push the test button.



How old and used is your tube machine and LPS?



It sounds like the LPS or tube, or both, is dead uggh! :).


---
**David Cantrell** *May 16, 2017 18:16*

**+Don Kleinschnitz** Thanks for the tips. I will try those tonight. The laser is about 1 1/2 years old. I only used it a little (less than 1 hour total)  because I Chinese software really sucks so I put the whole thing aside until I could find a way to run real code on the thing. 



Some questions. If the tube is shot would it still make the noise? 



It looks like eBay has 40w tubes for $100 and 50w for $150. If I need to replace the tube, do you know what I would need to upgrade it to the 50watt. (I know it is longer) Does the k40 PSU support the larger tube or would I need to swap that out? If I put another 40watt in, does it drop right in or is there anything else I would have to do?


---
**Don Kleinschnitz Jr.** *May 16, 2017 19:23*

**+David Cantrell** 



If the tube is shot the supply can make noise. 



40 watt should drop in if you have a good supply. Check the length. Also search the forum to see what new tubes folks have bought.



50w needs a 50w supply to run properly and cabinet extension.



When my tube goes I'm converting to 50w.


---
**David Cantrell** *May 17, 2017 23:13*

![images/ed158f636820d346c9fb0f6df9700871.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed158f636820d346c9fb0f6df9700871.jpeg)


---
**David Cantrell** *May 17, 2017 23:25*

**+Don Kleinschnitz** Thanks for working with me on this one. I think I am going down to the issue. Last night I tested the laser in the dark and saw a very small amount of light in the anode. The rubber boot (with red wire) and some of the 'U' shape beneath it was lighting up. I turned the laser off and went to make sure the "rubber boot connection" was seated and got quite a shock up my thumb which surprised me as I had not touched anything but the rubber. I must have entered a EMF, backed by a capacitor, which my thumb grounded to the frame.



The biggest question is, How do I make sure the thing is discharged before I go messing with the connections any more? Thanks to my thumb, it may already be discharged, but I rather not get another surprise. 



Also, does any of this suggest where I should look next?


---
**Don Kleinschnitz Jr.** *May 18, 2017 02:27*

**+David Cantrell** Please be careful. Leave the machine off and unplugged for 15-30 min. Get a wire with one end connected to the chassis and the other with its end bared, taped to the end of a 1ft at least, wooden dowel. Hold the dowel at the opposite end from the bare wire. With the other hand behind your back touch the bare end all around the anode side of the tube and try to make a connection.



Are you saying that you can see an arc at the anode end. If so is it inside the tube or in the connection?


---
**David Cantrell** *May 18, 2017 02:58*

**+Don Kleinschnitz** Yes, the anode end. It does look like the faint light is coming from inside the rubber boot area and the U shape underneath it.


---
**Don Kleinschnitz Jr.** *May 18, 2017 09:32*

**+David Cantrell** you may want to try and repair that connection. This post has multiple links for tube replacement with anode connection methods. 

Be gentle .....

You can pull off the silicone sleeve, pull off the wire,  re-strip the wire.  Then re-wrap the wire tightly. Finally put silicone in the tube and slide it back on. Wait 24hrs.



[donsthings.blogspot.com - K40 Laser Tube Specifications, Maintenance, Failure & Replacement](http://donsthings.blogspot.com/2017/05/k40-laser-tube-specifications.html)


---
**David Cantrell** *May 18, 2017 18:29*

**+Don Kleinschnitz** Thanks for the link to your blog. A lot of excellent info. I took off the silicone from the pins on both ends of the tube and found that neither end was soldered. I cleaned the pins and wires, rewrapped the wires around the pins, made sure nothing was near the unprotected pins, and test fired the laser. I found that there was a crackling sound from from the anode pin and there was some sparking on the tip of the pin. It sounds like there is some arcing going on, I just don't know why.



Do I just need to solder the wires to the pins for a better connection, or is there something else that is going on? Could a damaged/bad tube cause this?


---
**Don Kleinschnitz Jr.** *May 19, 2017 11:48*

**+David Cantrell** there are various views on soldering the connection. I don't think that the solder will stick to the pin but it will fill the gaps and create a filled connection. Wrapping the wire and filling the tube with silicon is sufficient. The site linked to the unique way to hold the wire using teflon tape but I have not tried that.

If you do solder it be careful because the heat can crack the tube. Also make sure that the solder flows with smooth ball-like surface. Points induce arcs in high voltage.

Unfortunately you are stuck as we all do wondering if the LPS is bad or the tube is bad or both.  

We have no good way to test a tube and without a HV probe it is hard to safely test the LPS.



One dangerous "Warning---Warning"  thing to do it test the supply by allowing the anode to arc to ground. 

What level do you consider your electronics/electric skills to be?


---
**David Cantrell** *May 20, 2017 03:10*

**+Don Kleinschnitz** Thanks. I will try to get some solder to stick to the pin. I will at least stick to the wire so that may be good enough.



My electric skills are relatively good and I have a couple of guys at my local makerspace that are wizard level.  What do you mean by "allowing the anode to arc to ground"? Do I short the anode pin to the ground pin? What all do I need to do the test?


---
**Don Kleinschnitz Jr.** *May 20, 2017 21:16*

**+David Cantrell** I meant let the anode wire arc to ground. 

.....

The best way to test is use a high voltage probe and dvm to measure the supplies output.

.....

Otherwise you can see if the supply can create an arc. This is dangerous so I don't recommend it. Also you could damage the supply if you leave it on to long.



However without a meter I don't know any other way.

Tape a ground wire to the tube, with end bared, about 1 inch from the anode (assume the anode wire is uncovered and wound around the post). This will be a spark gap. 



The idea is to press laser test and see if the lps creates an arc from the anode to the ground wire.

If not you can move the ground lead closer, after discharging the supply like I explained in an earlier post. 

A good supply should arc easily at .5-1". 

Pulse the test, do not hold it on. Stay away from the gap while arcing. 

It may be easiest to take the laser compartment cover off. 


---
**David Cantrell** *May 22, 2017 01:51*

**+Don Kleinschnitz** Sadly, The problem must be with the tube. I did as you said and got a nice healthy arc. Coolest thing I have done all week. Is replacing the tube all that's let? 
{% include youtubePlayer.html id="UpwJU3Uvsjc" %}
[youtube.com - Making lightning with my laser cutter](https://www.youtube.com/watch?v=UpwJU3Uvsjc)


---
**Don Kleinschnitz Jr.** *May 22, 2017 03:38*

**+David Cantrell** Holy shit that supply is working overtime! 



.... Anyone that thinks these supplies aren't lethal should watch your video!


---
*Imported from [Google+](https://plus.google.com/107911974344505492651/posts/DFo5xkkheA8) &mdash; content and formatting may not be reliable*
