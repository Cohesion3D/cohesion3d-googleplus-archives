---
layout: post
title: "Probably silly questions, but if I were to copy the files from the micro sd card to a larger storage micro sd card would I have more memory for my laser to engrave/cut larger files?"
date: July 01, 2017 02:01
category: "K40 and other Lasers"
author: "Aaron Russell"
---
Probably silly questions, but if I were to copy the files from the micro sd card to a larger storage micro sd card would I have more memory for my laser to engrave/cut larger files?



Any advice for creating a template for lining up operations in the software relative to the machine's area?  Like to make sure I have the item being engraved/cut centered or in the right spot on the laser bed?



Thank you! 





**"Aaron Russell"**

---
---
**Ashley M. Kirchner [Norym]** *July 01, 2017 02:25*

The SD card is only used for the firmware and config file, as well as storing files that you want to run directly from the GLCD.



As for a template, create a physical jig that you put on the bed of the laser so you can align your material starting in the same corner each time. Look at the attached picture. I 3D printed those red pieces that I can use to align the material. I purposely made it so the material sits roughly -3,-3mm from where the laser head homes (0,0). This way I know exactly where the material is, and within LW I always know where things are.

![images/5bc96452fc8d6ac3b462bc95267ce701.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5bc96452fc8d6ac3b462bc95267ce701.jpeg)


---
**Aaron Russell** *July 01, 2017 02:28*

Thank you!


---
**Ray Kholodovsky (Cohesion3D)** *July 01, 2017 02:31*

The first question is like RAM vs Hard Drive space I think.  You need enough card space to fit your files.  Usually the 2gb would hold quite a few files.  It would not contribute to the thinking portion of the board. 


---
*Imported from [Google+](https://plus.google.com/111661714770079378479/posts/RCLMQKxbSXp) &mdash; content and formatting may not be reliable*
