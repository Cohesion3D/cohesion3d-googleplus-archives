---
layout: post
title: "Need some pics for the new C3D Mini documentation"
date: February 23, 2017 00:30
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Need some pics for the new C3D Mini documentation.  

I've got the machine without the ribbon cable, so I need

A) pics from someone who has the ribbon cable type machine.  I need a pic of the cables that go to the board (ribbon, y motor, power) prominently displayed so that I can label them, and a pic of them hooked up to the Mini. 

B) Someone with the red board (and I guess 2nd PSU) in their machine... This was a shitstorm of manual hookup (but not bad in the sense that you can just cut the wires from the 6 pin and route them to our screw terminals) and hopefully everything else stayed the same.  



Help much appreciated, want to make this much more clearly defined for the next wave of upgraders.





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**John Sturgess** *February 23, 2017 08:09*

I'm starting work on properly installing my C3D Mini this weekend, i've got the moshiboard and 2nd PSU so i'll take some pictures


---
**Arion McCartney** *March 03, 2017 06:02*

**+Ray Kholodovsky**​ in preparation for my c3d mini upgrade to my k40, is there a single location for instructions on the upgrade? I am reading bits and pieces of info in this group, but I just want to make sure I install the board correctly along with correct firmware. Thank you!


---
**Arion McCartney** *March 03, 2017 21:10*

Nevermind, found the documentation link at the top of this group page.


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 21:11*

I am going to make new docs but can't give a firm timeline at this moment.  Take everything you read with a grain of salt, for example we no longer recommend replacing the pot.


---
**Arion McCartney** *March 03, 2017 21:13*

OK. Thanks for that info. I have a k40 with ribbon cable if you still need pics of that


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/3GVjhBAKgyT) &mdash; content and formatting may not be reliable*
