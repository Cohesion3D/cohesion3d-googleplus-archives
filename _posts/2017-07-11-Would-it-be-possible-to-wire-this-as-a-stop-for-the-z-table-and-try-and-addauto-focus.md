---
layout: post
title: "Would it be possible to wire this as a stop for the z table and try and addauto focus?"
date: July 11, 2017 04:12
category: "C3D Mini Support"
author: "William Kearns"
---
Would it be possible to wire this as a stop for the z table and try and addauto focus?

![images/f705e71302f735795b32e4d9493da56a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f705e71302f735795b32e4d9493da56a.jpeg)



**"William Kearns"**

---
---
**Don Kleinschnitz Jr.** *July 11, 2017 22:07*

Looked at this once and decided that its likely not accurate enough. I would use a "BL touch sensor though (below).



You would need a companion processor to handle the control algorithm unless you added smoothieware code. You could interface that controller as a switch closure to the C3D i/o.



[amazon.com - Amazon.com: BLTouch : Auto Bed Leveling Sensor / To be a Premium 3D Printer: Industrial & Scientific](https://www.amazon.com/BLTouch-Leveling-Sensor-Premium-Printer/dp/B01FFV2TOS)








---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/AaCG9w9tuvR) &mdash; content and formatting may not be reliable*
