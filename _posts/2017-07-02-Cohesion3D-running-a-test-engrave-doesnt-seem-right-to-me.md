---
layout: post
title: "Cohesion3D running a test engrave, doesn't seem right to me"
date: July 02, 2017 20:16
category: "General Discussion"
author: "RastaC74"
---
Cohesion3D running a test engrave, doesn't seem right to me. Is it supposed to be this jerky?





**"RastaC74"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 20:26*

Are you running from LW over the USB Cable?  If so, save the file to your computer, then copy it to the MicroSD Card, and use the GLCD Menus to play the file. 


---
**Claudio Prezzi** *July 03, 2017 08:11*

Or just reduce the feed until it's not jerky anymore (around 2000mm/min for 0.1mm laser diameter). 


---
*Imported from [Google+](https://plus.google.com/106765184888555688169/posts/6EXuWmJbf84) &mdash; content and formatting may not be reliable*
