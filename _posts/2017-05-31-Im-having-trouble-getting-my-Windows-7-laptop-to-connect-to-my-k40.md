---
layout: post
title: "I'm having trouble getting my Windows 7 laptop to connect to my k40"
date: May 31, 2017 04:25
category: "FirmWare and Config."
author: "Aaron Russell"
---
I'm having trouble getting my Windows 7 laptop to connect to my k40.  I've installed the board into the machine and my laptop recognizes the smoothieboard under devices and printers.  When I open laserweb 4, it says that the machine is disconnected or machine is not connected when I try to run a job.  I'm assuming the drivers did not install properly, but I can't figure out how to fix the issue.





**"Aaron Russell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 31, 2017 04:32*

To be absolutely clear, you installed the Smoothie drivers yes? 



If so, then read on: 



1. Have you verified correct wiring/ machine functionality? For example if you have the GLCD installed you could jog around and whatnot via that. 

2. Are you on the latest version of LW? 

3. In devices and printers, when you go to smoothieboard --> hardware can you see the COM port # it was assigned?  Is this the same as the selected one in LW?

4. You could get Pronterface and try connecting from that. 

5. Ultimately if it's not working, then you should uninstall the 1.1 Smoothie driver exe you used and do a manual install. **+Ashley M. Kirchner** has described this in this group before. 


---
**Ashley M. Kirchner [Norym]** *May 31, 2017 04:44*

One other step: Did you actually tell LW to connect to the board? I know it may seem stupid but it's easily overlooked. Every time you open LW, you have to click on the Comms tab and connect it to the correct port and speed for it to actually communicate with the board.


---
**Aaron Russell** *May 31, 2017 04:44*

Thank you for the quick response!  Yes, I downloaded them and ran the install.  



I'll check/try these tomorrow. 



Thank you so much! 


---
**Aaron Russell** *May 31, 2017 04:45*

Thank you for the quick response!  Yes, I downloaded them and ran the install.  



I'll check/try these tomorrow. 



Thank you so much! 


---
**Aaron Russell** *May 31, 2017 04:48*

**+Ashley M. Kirchner** 

Yes...but I got a windows message saying drivers were not installed properly from device...is there a separate set of drivers on the micro sd card that are not installing properly?


---
**Ashley M. Kirchner [Norym]** *May 31, 2017 05:13*

Ok, it sounds like you installed the v1.1 drivers, which I also could not get to work. So you'll have to install the v1.0 drivers. It's a manual process though. Follow this link and read the section on installing the drivers manually: [forum.smoothieware.org - Windows Drivers - Smoothie Project](http://forum.smoothieware.org/windows-drivers)


---
**Aaron Russell** *June 01, 2017 01:19*

So when I go to that link, I click on the 1.0 driver zip file, but it says that I've clicked on a link that doesn't exist yet.  Any ideas?




---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 01:23*

I keep a backup :).  Here you go: [dropbox.com - Smoothie Drivers](https://www.dropbox.com/sh/3dacbghod6e2gg0/AAB60iBQL8wp8ARQdaFUop0ba?dl=0)


---
**Ashley M. Kirchner [Norym]** *June 01, 2017 01:31*

Sorry, I didn't check that link. The same page on the main site (but with missing pictures) does have a valid link: [smoothieware.org - windows-drivers [Smoothieware]](http://www.smoothieware.org/windows-drivers)


---
*Imported from [Google+](https://plus.google.com/111661714770079378479/posts/Udzo5fPnaGg) &mdash; content and formatting may not be reliable*
