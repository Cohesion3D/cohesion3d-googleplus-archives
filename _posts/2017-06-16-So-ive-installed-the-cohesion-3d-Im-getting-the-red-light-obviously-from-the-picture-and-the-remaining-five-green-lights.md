---
layout: post
title: "So i've installed the cohesion 3d. I'm getting the red light (obviously from the picture) and the remaining five green lights"
date: June 16, 2017 18:31
category: "C3D Mini Support"
author: "John Hunter"
---
So i've installed the cohesion 3d.  I'm getting the red light (obviously from the picture) and the remaining five green lights.  2 and 3 are flashing as per guide.   It appears to be communicating with my computer as it shows up in my hardware, and i am able to see the flash drive on it.   It says it is connecting in laserweb for.  But when i try to manually jog the controls or home it, it does nothing.  Under "live jogging" disabled is marked.  Does this need to be enabled?  I tried clicking on it and it wont change.  

 

![images/ef47631d5ee08783e12350eab47e8abb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef47631d5ee08783e12350eab47e8abb.jpeg)



**"John Hunter"**

---
---
**John Hunter** *June 16, 2017 18:43*

I just found that i need to hit "home all" to enable.   When i hit "home all" it does nothing.




---
**Joe Alexander** *June 16, 2017 18:44*

you can only enable live jogging after you have performed a homing cycle, what it does is allow you to use alt-click in the workspace to jog to that location(might be ctrl, either way). Homing will only work if it is set under settings to G28.2.


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 18:59*

[cohesion3d.freshdesk.com - Troubleshooting/ FAQ : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq)



The first section there please. 



A screenshot of the LaserWeb Window please.  



What is the operating system of the computer? 


---
**John Hunter** *June 16, 2017 19:08*

Under my machine settings G28.2 seems to be there.   Or does it need to be put somewhere else?  

![images/7c1710db31c00472254838ff7dfb4d34.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7c1710db31c00472254838ff7dfb4d34.png)


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 19:13*

Settings -> Gcode, enter it in the box labeled GCODE HOMING


---
**John Hunter** *June 16, 2017 19:23*

I have gotten live jogging to show up now.

But when i hit the "home all" nothing moves.  When i hit "alt" plus an arrow key, the command shows up in the list , but nothing happens.







I'm using windows 10



Laserweb shows connected



Yes a new drive shows up with the config file visible and all



I have tried multiple USB cables, all of good quality



Smoothie board shows up in Devices and Printers





![images/3d1913e35c89273a35e3ed947b723eb6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d1913e35c89273a35e3ed947b723eb6.png)


---
**John Hunter** *June 16, 2017 19:27*

Ashley, Thank you, i have entered that too  and still no response from the k40


---
**John Hunter** *June 16, 2017 19:39*

I entered the example gcode and ran it and the program acts like it is running it, but it completes in 0 seconds and the laser itself does nothing.  Here's a screenshot of that.  

![images/2cc87944f0144045cb1878641ca66ac5.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2cc87944f0144045cb1878641ca66ac5.png)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 20:01*

Can you try sending a M119 gcode please and see if you get a response string in the terminal window area (lower right)?  Screenshot after sending. 


---
**John Hunter** *June 16, 2017 20:27*

I apologize for my lack of knowledge on laserweb, but i think this is what you are asking

![images/a25b0572cdfb9996f53177d2460deff7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a25b0572cdfb9996f53177d2460deff7.png)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 20:29*

M has to be capital, please try again. M119


---
**John Hunter** *June 16, 2017 21:11*

Okay, i did that and still no response.  I  did a little "M119" research on this board, so i took the liberties to also try "M114" with no response.  I also downloaded and tried pronterface,  I connected, then tried jogging and homing.   No response.   I then hit the x/y (center symbol thing) in the center of the jog control and got this "SENDING:G0 X100.0 Y100.0 F3000"

![images/4e24ea9d3e6a9148978afb7efc716475.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e24ea9d3e6a9148978afb7efc716475.png)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 21:12*

Aha, you found my script.  But I can see the result of sending the M119 both in LW and in Pronterface? Screenshots of that please.


---
**John Hunter** *June 16, 2017 21:28*

Yup, here they are



![images/890796e457b2a7246388c0be8f3d76e8.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/890796e457b2a7246388c0be8f3d76e8.png)


---
**John Hunter** *June 16, 2017 21:28*

![images/38069f43b53eea8fdbcca616d7125fa7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/38069f43b53eea8fdbcca616d7125fa7.png)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 21:37*

Ok, so there seems to be some sort of communications concern here.  M119 should be returning a line with the endstop pin states, and it is not.  Is there another computer you can try this stuff on? 



Are you referencing this thread per chance? :)

[plus.google.com - Hello everyone, I got my Cohesion board a few days ago, and I can't get it to...](https://plus.google.com/106338687452907889031/posts/VikuythRg2h)



Verifying the MicroSD Card will be the next thing we try.  



Also, can you located the HALT LED on the board?  It is the lone green one between SD Socket and the button in the top right area. Is it solid or blinking? 


---
**John Hunter** *June 16, 2017 21:40*

Just tried another computer and got

the same results




---
**John Hunter** *June 16, 2017 21:59*

So yes. Tried on another widows 10 machine ending in same results.  

Yes that is the thread i found.

MicroSD card is reading (see screen shot below)

That green light is solid

I'm also posting a picture of the board with the laser switch turned off (as the red light was blinding the camera) so you can see how it is connected 



![images/4fa3eeadcd7da6e8c8f5515ec252f013.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4fa3eeadcd7da6e8c8f5515ec252f013.png)


---
**John Hunter** *June 16, 2017 22:00*

![images/8e6baaba53b7bbb14650b97280f9bd26.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8e6baaba53b7bbb14650b97280f9bd26.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 22:01*

And L2 and L3 are blinking, right? 


---
**John Hunter** *June 16, 2017 22:21*

Yes




---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2017 22:36*

Ok, let's rule out any quirks related to the memory card.  

Please put the card in your computer via a reader, format it FAT32, and put the firmware.bin and config.txt files on it again.  These files can be found in a dropbox link at the bottom of the install guide. 



Then when you plug in USB, you should see green leds L1 - L4 count up in binary before resuming their normal behavior.  Try the M119 stuff in Pronterface with just USB. 


---
**John Hunter** *June 19, 2017 15:15*

Just got back to the office this morning.  YES!  Formating the card fixed that problem.  I can jog and what have you.   Thank you so much for your assistance!




---
*Imported from [Google+](https://plus.google.com/105510960839020211759/posts/6vEC8tWaE5g) &mdash; content and formatting may not be reliable*
