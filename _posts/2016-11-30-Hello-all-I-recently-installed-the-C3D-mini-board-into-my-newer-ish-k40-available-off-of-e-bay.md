---
layout: post
title: "Hello all I recently installed the C3D mini board into my newer-ish k40 available off of e-bay"
date: November 30, 2016 20:55
category: "General Discussion"
author: "Kelly S"
---
Hello all I recently installed the C3D mini board into my newer-ish k40 available off of e-bay.  Most of it went smoothly, if no one wants to read the link I will post below, note that the laser will fire if the gantry is moved while the board is off, but machine powered on.  Not sure why, so be careful.  Otherwise the install went great and **+Ray Kholodovsky** was very helpful along the way, great man he is.  I have some pictures in my little install guide and will post some here as well as I finished my new face plate to hold the glcd and a amp meter (however I got the wrong one, new one will be in tomorrow) so never mind it is not the right one, haha... If anyone wants the file for the control panel will upload.    here is a link to my install guide, though it is a very rough draft I will likely go in and fix some things and add more details.  [https://docs.google.com/document/d/1f4u79SzF7th84rFIfUXIpY8u4YEpwoGeH7z-K8yjxQc/edit?usp=sharing](https://docs.google.com/document/d/1f4u79SzF7th84rFIfUXIpY8u4YEpwoGeH7z-K8yjxQc/edit?usp=sharing)

![images/fae7fa8ff7df5575448d5c6e753cd4b3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fae7fa8ff7df5575448d5c6e753cd4b3.jpeg)



**"Kelly S"**

---
---
**Kelly S** *November 30, 2016 20:57*

![images/5352d81fd15f6330b057ba485670fc99.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5352d81fd15f6330b057ba485670fc99.jpeg)


---
**Carl Fisher** *December 03, 2016 02:55*

Can you elaborate on the laser firing when you move the gantry? Do you have a pic of your power supply and wiring setup? It sounds like you're getting feedback from the stepper into the pin that triggers the laser which doesn't make sense to me.






---
**Carl Fisher** *December 03, 2016 02:58*

Ok, I looked at your install. First thing I noticed is that you've bypassed your laser enable switch. Are you sure you want to do that? You essentially have no way to hardware disable the laser now.


---
**Kelly S** *December 03, 2016 02:58*

**+Carl Fisher** When it fires, the PSU is on, but the red switch that enable the board is off, it will not do it if the board is on.  So say you turn the main switch to on, and not the toggle and move the stepper it will fire the laser.   Everything is confirmed wired correctly, even odder is it only fires the laser if the gantry is moved left and right, back and forth wont trigger it.   And in my little install thing I wrote up of my install is a picture of it, it is on the correct pin. 


---
**Kelly S** *December 03, 2016 03:04*

Power button disables it just fine.  (edit) and I followed the install provided to me when I got the board, it if it wired wrong, I wouldn't know.  


---
**Carl Fisher** *December 03, 2016 03:22*

Right, but my concern is that you've put a jumper between the two left most pins on the main connector which hard wires the laser to enabled. I'm not 100% familiar with the version of control board that came with your machine originally, but mine had a red push button that connected those 2 terminals to enable/disable the laser. I'm curious if you added that jumper or if it was like that from the factory.


---
**Carl Fisher** *December 03, 2016 03:24*

Ok, I just looked at your log again. It looks like that jumper is like that from the factory. That's an interesting twist on the design. 



When you move a stepper, it generates voltage that feeds back into your board. That's normal however causing it to fire is not.



**+Ray Kholodovsky** is there any way that feedback from his stepper could be triggering the fire pin?


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:29*

**+Carl Fisher** it's a bit different with his setup. What I saw was that his laser enable switch (the switch other than main power) actually controls power to the 4 pin power connector. So (with USB not connected) his board is off. Pins are by default low or in other undefined states. So when he manually moves a stepper it generated energy and the laser fired. Pwm would be at 5v by default (thus max power). 



Which leads up to the other interesting point - that his laser is firing with the laser enable switch in the DISABLED position. 


---
**Kelly S** *December 03, 2016 03:29*

**+Carl Fisher** From my understanding, that is where I would put in a door switch?  My machine never shipped with one, so I am pretty sure that was the solution. 


---
**Carl Fisher** *December 03, 2016 03:33*

That's definitely strange. It WOULD make sense that those can be used as a door switch as many people use the same circuit for various safety switches. Manual enable, door interlock, loss of water, etc... Simply opening that circuit will disable the laser from firing. Assuming there is not something VERY different in two power supplies that look identical, which is definitely a possibility.



Take a look at the pinout on my power supply in the instructions I just posted. From left to right, it should be 1/2 are the enable circuit, 3 is test fire 4 is ground, 5 is laser trigger (PWM) and 6 is +5v


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/43bDL9nVozD) &mdash; content and formatting may not be reliable*
