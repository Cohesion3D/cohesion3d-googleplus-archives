---
layout: post
title: "Hi everyone, im just install de c3d and do the jobs but inverted and some times jump steps and lost alignment on jobs..please help me"
date: June 22, 2018 16:39
category: "C3D Mini Support"
author: "MAXIMILIANO HUMBERTO ORTIZ TORRES"
---
Hi everyone, im just install de c3d and do the jobs but inverted and some times jump steps and lost alignment on jobs..please help me





**"MAXIMILIANO HUMBERTO ORTIZ TORRES"**

---
---
**Marko Marksome** *June 25, 2018 16:44*

I have the same problem right now. It can be that your speed is  to fast try to engrave with 80mm/sec and 254 dpi. I believe you shouldnt go above that if you are using the smoothie firmware. Somebody has suggested that the x axis belt should be tightened (which i did) but without any results.

The missallignment still exists.

The third option is to change the firmware from smoothie to grbl which is more stable for engraving...



I hope i could have helped you. 




---
**Tech Bravo (Tech BravoTN)** *June 26, 2018 17:35*

are you using lightburn? and, if so, the origin needs to be set to front left and NOT rear left. rear left is 'home" which is actually 0,200. origin is 0,0. if this is incorrect it will mirror your work and cause the head to crash


---
**MAXIMILIANO HUMBERTO ORTIZ TORRES** *June 28, 2018 18:52*

**+Marko Marksome** with the original board i can engrave to 500 mms without any issues, so that make me think that the hardware is not the problem...just the software or i don`t know !!. thanks to techbravo las night i could solve the inverted problem, now is just the misalignment !!!






---
**Marko Marksome** *June 28, 2018 20:05*

Yes i was thinking the same, but it seems that it cant handle 500 mm/sec.

I only can go up to 60mm sec  that my max.

Maybe its software maxbe the card i dont know. I gotta test it with only the glcd without lightburn.


---
**MAXIMILIANO HUMBERTO ORTIZ TORRES** *June 28, 2018 20:29*

**+Marko Marksome** 60 mms for me it is not enough fast...im very impacient 😂, i do not have glcd 😢. Other think is that maybe im going to disconnect the digital % screen and control directly from the board to the power source 🤔


---
**Marko Marksome** *June 28, 2018 21:57*

You can try or you can just copy the grbl firmware to your sd card and try it that way. Everyone is saying that grbl firmware is way faster then the smoothie firmware. Its not complicated to do it aswell.




---
**Tech Bravo (Tech BravoTN)** *June 28, 2018 22:13*

Yes grbl processes raster images much faster. You will lose the glcd if its installed.


---
*Imported from [Google+](https://plus.google.com/115264403817808939843/posts/5BKBFY5eNq1) &mdash; content and formatting may not be reliable*
