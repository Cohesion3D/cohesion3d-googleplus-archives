---
layout: post
title: "Ray, first off, thanks for a great product!"
date: May 12, 2017 02:30
category: "K40 and other Lasers"
author: "Nick Winnard"
---
Ray, first off, thanks for a great product! I ordered a C3d Mini to free up my smoothieboard x5 for an upcoming diode laser build. got it installed in the k40 this last week. Connected to laserweb4 no issue. Pretty much picked up right where I left off. 



I do have a question about the stepper motor current (Forgive me if this has been discussed. I could not find any posts) 



On my x5 I had dialed the settings on my stepper motors to where they would sing very nicely when engraving and cutting. my understanding was the config file set the current directly. The same settings for the c3d don't seem to work as well. I seem to get quite a bit more stuttering than I did before. 



I know that the C3d uses a driver board that has adjustable current control for the motors (current limit?) my question moves along the lines of trying to better understand the relationship between the config file setting, and the stepper driver boards. 



Does the pot on this board set a threshold? e.g. if theoretically, I was to set the Pot to Max threshold (Which I understand to be 2A for this )   Could the stepper driver current be controlled accurately via the smoothie config as before? or is there a better/different way I need to think about this on the c3d



Thanks in advance for your time! 





**"Nick Winnard"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 12, 2017 02:33*

Hey Nick. The current is controlled entirely from the pot on the driver boards.  The config line does nothing. It's covered in the new FAQ/ troubleshooting guide over on the docs site. Happy to answer any follow up questions. 


---
**Nick Winnard** *May 12, 2017 03:15*

Thanks Ray, That was the bit of info I needed! One follow up question, do you know offhand what the Current sense resistor values are on the board so I can calculate the current accurately? The heat shields unfortunately obscure that info at this point.   




---
**Ray Kholodovsky (Cohesion3D)** *May 12, 2017 03:17*

Quite possibly R100 so 0.1 ohm. 


---
*Imported from [Google+](https://plus.google.com/112795894257485102297/posts/fiffPWGd5n6) &mdash; content and formatting may not be reliable*
