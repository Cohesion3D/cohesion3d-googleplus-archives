---
layout: post
title: "Hello all, I completed the upgrade to my K40 last night"
date: June 28, 2017 13:33
category: "C3D Mini Support"
author: "RastaC74"
---
Hello all, I completed the upgrade to my K40 last night. I have checked all the connections, updated firmware and adjusted the potential screw on the driver adapter various positions between 45 and 90 degrees. The X-axis will not move. Here is a video of what it does when I jog it through laserweb. 





**"RastaC74"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 13:34*

Can we see a clear pic of the wiring to the board please? 


---
**RastaC74** *June 28, 2017 13:39*

![images/168da5c4102732e5250e463e02652ae0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/168da5c4102732e5250e463e02652ae0.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 13:44*

Double check your x motor connection. I think it might be not lined up on the header properly. I can't see from this angle of the picture. 


---
**RastaC74** *June 28, 2017 13:44*

![images/31212057c5767b3dcbe1b21cdd8d7348.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/31212057c5767b3dcbe1b21cdd8d7348.jpeg)


---
**RastaC74** *June 28, 2017 13:47*

I'll have to uninstall the mounting plate to get a better angle. Won't be able to do that till later, I have to be at work in an hour.


---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 13:49*

Just need to make sure that the header lines up with the plug for the X motor connection on the board - wire. 

We started preloading and testing all the boards and motor drivers here so I really don't think it's a board issue. The grinding is commonly either trying to move too fast, or a bad wiring connection. 


---
**E Caswell** *June 28, 2017 13:50*

Are you trying to move it on the GLCD or on LW4?

I think the x axis plug needs to be reversed, it sounds like its hitting the stops and jumping teeth on the drive belt.

Looking at the picture it looks like they are in different ways. Just my thoughts


---
**RastaC74** *June 28, 2017 13:54*

Definitely not hitting the stops, I manually moved the laser head to the center as I wanted to make sure homing was working. I was trying to move it through LW


---
**RastaC74** *June 28, 2017 16:36*

So I reversed the x connector and the x axis now moves, slow but it moves and also moves in reverse of the direction selected in LW. I may have to put the original board back in, have jobs waiting. The response between LW4 and C3D is so slow.


---
**E Caswell** *June 28, 2017 20:10*

**+RastaC74** Stick with it, it will be worth it in the end. I know how you feel when work stacks up. Been there done that.

I had problems with mine and had to change back to stock equipment to clear the backlog of work.

You will need some machine downtime if only to get it working how you want it too.

You will also need some "learning" time to get a grips with the differences between Laserdraw and the laserweb configuration.

Believe me tho t is worth the pain to get it working, there is an enormous amount of help on here but is frustrating when it doesn't go as quick as you expect it too.



Just to clear up the problem.



1. Do you say that now that you have switched the cable around the axis now moves opposite to what the direction states on LW4? 



I think you may have a cable fault and if you turn the cable back to what it was you may find it works this time as I believe you my have disturbed a cable when changing it around. 



2. If you are operating from LW buttons, have you changed the "Jog" seeds?



You can change "jog" speeds on LW 

See Pic



![images/c3edee53314a0f8c5e15f57847b8c2c9.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c3edee53314a0f8c5e15f57847b8c2c9.png)


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 20:15*

Been corrected. Made the wrong assumption. Ok, so if your gantry is moving slower than you're expecting it to, play with the speed as PopsE suggested.



<s>The response from C3D isn't slow. Ray has asked for a better picture of the connections and you said you will later. There isn't much (if anything) that we can do without being able to verify that your connections are correct, so we simply wait on you.</s>


---
**E Caswell** *June 28, 2017 20:18*

+RastaC74

You will find that Jogging manually will be slower and when you start to run jobs on the K40 you will find the speeds and efficiency much better once you get the hang of it.

Its much more efficient than Ldraw. with LW4 you can cut, raster and laser fill in one operation and one press of the "run job" Thats my view



You can also change machine setting to MM/s rather than MM/Min. see pic.

![images/f99e26c1acde43cf428bacb5c80650d4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f99e26c1acde43cf428bacb5c80650d4.png)


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 20:20*

As for the opposite direction, the best thing you can do is try a homing sequence and see what it does. If it isn't moving to where the end stops are, then you likely have a connector reversed.


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 20:24*

One other thing to try as far as communications between computer and laser, is a different (better) USB cable. The one that came with my machine was of a somewhat inferior quality.


---
**RastaC74** *June 28, 2017 23:26*

Ashley, I am using a different USB cable. I parked the blue one when the laser was starting to stall during jobs. I tried to change the feed speed in LW4, the y axis is fine and the x-axis is the problem. I had to put the old board back in to get it running again. I will try again this weekend. Hopefully it works out. Another question, is the GLCD supposed to stay on full time even when the laser and CPU are off? 


---
**Ashley M. Kirchner [Norym]** *June 28, 2017 23:41*

When you get back to it this weekend, let us know and perhaps we can figure out what's going on. As for the GLCD, it's the C3D board that stays powered up because it gets power from the USB cable (as well). 


---
**RastaC74** *July 01, 2017 16:57*

Reinstalled the C3D board, do I have to do anything special to get the limit switches to activate, gantry defeats the switches.


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 19:00*

Send a G28.2, it should move back left, touch the switches, back off, and then re touch the switches and stop.  Because of the slow motion, during the 2nd touch off, people sometimes think the switches did not read.  


---
*Imported from [Google+](https://plus.google.com/106765184888555688169/posts/EZTBfAEKSMR) &mdash; content and formatting may not be reliable*
