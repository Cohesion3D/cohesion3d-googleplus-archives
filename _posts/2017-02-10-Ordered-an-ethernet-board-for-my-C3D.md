---
layout: post
title: "Ordered an ethernet board for my C3D"
date: February 10, 2017 21:58
category: "General Discussion"
author: "Kris Sturgess"
---
Ordered an ethernet board for my C3D. Just arrived amazingly fast from China.



What current features does it allow? Or is it more for future development?



Running LW3/4 on a K40.



Also still have not been able to get LW3 running on my RPI3. .. grrrr ;-)





**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 10, 2017 22:02*

You can access the smoothie web interface from it, upload a gCode file to the sd card that way, and select a job and hit run. 

LaserWeb support for Smoothie Ethernet was never finished. 

In order to use it, you need to plug it in, and there's a network section at bottom of config that needs to be enabled (set to true). Then you can go to the IP address of the board and see the interface. 


---
**Kris Sturgess** *February 10, 2017 22:06*

Kinda what I figured. Might see if I can get a wireless adapter to work with it. I did get the .gcode on SD to work via LCD.

Just keeping my options open.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/bQGDwtkFR1d) &mdash; content and formatting may not be reliable*
