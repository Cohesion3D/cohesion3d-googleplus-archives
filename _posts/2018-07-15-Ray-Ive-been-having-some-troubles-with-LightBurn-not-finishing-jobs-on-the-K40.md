---
layout: post
title: "Ray, I've been having some troubles with LightBurn not finishing jobs on the K40"
date: July 15, 2018 23:18
category: "C3D Mini Support"
author: "David Ewen_P"
---
Ray,

I've been having some troubles with LightBurn not finishing jobs on the K40.

After discussions with Oz, he suggested that I use GRBL-LPC or NoMSD versions on the Mini 3d board.



Can you direct me to those programs? I'm not finding them on your website.



Thanks,

David





**"David Ewen_P"**

---
---
**Tech Bravo (Tech BravoTN)** *July 15, 2018 23:20*

here is the grbl info:

[cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)


---
**Tech Bravo (Tech BravoTN)** *July 15, 2018 23:21*

here is the link for batch 3 smoothie firmware and config:

[dropbox.com - C3D Mini Laser Batch 3 - Provided Config](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AADLn0lRlfl7f3y-IrX5fVWIa/C3D%20Mini%20Laser%20Batch%203%20-%20Provided%20Config?dl=0)


---
**David Ewen_P** *July 15, 2018 23:23*

Tech Bravo,

Thanks for the quick response.



Is this the NoMSD version of the standard Mini?



Also, will both GRBL and this batch 3 still work with LightBurn?



David


---
**David Ewen_P** *July 15, 2018 23:24*

Sorry, but one more question.



Should I do a backup, or just copy these files to the SD card?



> On Jul 15, 2018, at 4:23 PM, David Ewen_P <*<b>***</b>@<b>**</b>> wrote:

> 

> Tech Bravo,

> Thanks for the quick response.

> 

> Is this the NoMSD version of the standard Mini?

> 

> Also, will both GRBL and this batch 3 still work with LightBurn?

> 

> David

>


---
**Tech Bravo (Tech BravoTN)** *July 15, 2018 23:25*

yes batch 3 firmware removes the windows mass storage, otherwise it is the same and yes grbl will work with lightburn (set up a new device and choose cohesion3d grbl).




---
**Tech Bravo (Tech BravoTN)** *July 15, 2018 23:36*

No need to backup. Just format the card and write one or the other.


---
**David Ewen_P** *July 16, 2018 01:39*

Ok thanks.



Just FYI, I’m also running Mac OS. Hope that doesn’t change anything.



David


---
**David Ewen_P** *July 16, 2018 02:12*

I formated a new SD card and copied the 2 files to it.



config.txt

firmware.bin



When I put it in the c3d board and start up, my display is blank and the k40 does nothing.



Any ideas?



David


---
**David Ewen_P** *July 17, 2018 20:45*

**+Tech Bravo**

When I install grbl files, my k40 doesn't repsond. I have the display board that came with the C3D mini, it is also blank. 


---
**Tech Bravo (Tech BravoTN)** *July 17, 2018 23:19*

Grbl doesnt support the glcd. Its not a problem its just not functional. With the microSD in your PC format FAT, copy the grbl file only to the card. When you insert the card in the c3d and power it on it will alter the card and write a .cur file i believe.



[cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)


---
*Imported from [Google+](https://plus.google.com/109639692587843616301/posts/Ahzduugjd34) &mdash; content and formatting may not be reliable*
