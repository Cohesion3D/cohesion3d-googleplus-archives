---
layout: post
title: "I plan on upgrading my k40 to a bigger work area using linear rails and all"
date: March 03, 2018 19:35
category: "K40 and other Lasers"
author: "Dar Thw"
---
I plan on upgrading my k40 to a bigger work area using linear rails and all. Was planning on going with the C3D Mini. Do I need an external power supply to power the drivers if I plan on going with 2 Y motors? If I do, does it have to be 24v? I have a 12v here Im not using. 





**"Dar Thw"**

---
---
**Ashley M. Kirchner [Norym]** *March 03, 2018 19:54*

It is recommended that you use an external PSU to run the stepper motors. The stock PSU doesn't have enough amps to do that. Also, the Cohesion3D board runs off of 24V.


---
**Dar Thw** *March 04, 2018 01:54*

**+Ashley M. Kirchner Norym** Is there a guide for wiring it with an external psu?


---
**Ashley M. Kirchner [Norym]** *March 04, 2018 02:13*

I wrote an extensive one covering external drivers and a PSU for a z-table and rotary. The setup would be the same. Look at where the 24V lines are, that would be the external PSU. It's in the documentations on the Cohesion3D web site:

[cohesion3d.freshdesk.com - Wiring a Z Table and Rotary with External Stepper Drivers : Cohesion3D](https://cohesion3d.freshdesk.com/solution/articles/5000739904-wiring-a-z-table-and-rotary-with-external-stepper-drivers)


---
**Dar Thw** *March 04, 2018 14:13*

**+Ashley M. Kirchner [Norym]** If Im still wanting to use the a4988 drivers on the board how would I wire it? Not plug in the blue wires with big white connector? Then run power from the 24v psu to the 24v connectors on the board? I dont see that working though, maybe I have to keep 2 of the blue wires?


---
**Dar Thw** *March 04, 2018 14:25*

**+Dar Thw** Ok Im a loser that should have read more, I found [plus.google.com - I want permit the K40 power supply to only be tasked to power the K40 laser a...](https://plus.google.com/u/0/117720577851752736927/posts/emzYWQAAXuW)  explains it all perfectly, thanks.


---
*Imported from [Google+](https://plus.google.com/107923441357901969822/posts/LJb37YQY6ii) &mdash; content and formatting may not be reliable*
