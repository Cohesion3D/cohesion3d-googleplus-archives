---
layout: post
title: "Just wanted to share what the production glcd adapter will look like installed in the c3d board"
date: December 13, 2016 15:11
category: "General Discussion"
author: "Kelly S"
---
Just wanted to share what the production glcd adapter will look like installed in the c3d board.  I had access to test the only pre production version.  It feels right, and definitely finishes the look.  

![images/69e3d5ce31e8a622b2b311765aa03482.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69e3d5ce31e8a622b2b311765aa03482.jpeg)



**"Kelly S"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 15:27*

You're right, I am thoroughly enjoying looking at it. 


---
**Griffin Paquette** *December 13, 2016 18:47*

First one I've seen with the adapter. Might have to pick one up;-)


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/JFPUk322wUh) &mdash; content and formatting may not be reliable*
