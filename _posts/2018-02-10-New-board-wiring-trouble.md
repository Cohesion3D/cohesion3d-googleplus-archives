---
layout: post
title: "New board, wiring trouble"
date: February 10, 2018 20:57
category: "General Discussion"
author: "F D"
---
New board, wiring trouble



![images/2b4ee7bee1cb73989435e55ace0bcf7e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2b4ee7bee1cb73989435e55ace0bcf7e.jpeg)
![images/e1ca54482e49e134866edf18d8e6af17.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e1ca54482e49e134866edf18d8e6af17.jpeg)

**"F D"**

---
---
**Andy Shilling** *February 11, 2018 10:24*

What trouble are you having?


---
**F D** *February 11, 2018 13:06*

Wiring looks different, want to make sure I hook it up correctly 


---
**Andy Shilling** *February 11, 2018 13:07*

But your not showing any wiring so we have nothing to compare to unless you mean your other post with the moshi board?


---
**F D** *February 11, 2018 13:41*

Yes, first picture is stock board.


---
**Andy Shilling** *February 11, 2018 13:49*

Ok the ribbon speaks for it's self, power wise if you look at the front of the power in on the moshiboard you will see the ground,+24v +5v markings. you can loose the 5v for the c3d and if i remember right the ground and +24v need swapping locations. these two in the image need swapping over for the c3d board.





X carriage is run by the ribbon and Y will be feed off of the stepper driver marked.



If the third wire in the image is L ( I can't remember what mine was) you will then wire that to P2.5 on the c3d for pwm firing.



Better images will help confirm wires so please check with the community first before trying anything.

![images/fc013bbb6be223cb34c529a4f5974155.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fc013bbb6be223cb34c529a4f5974155.jpeg)


---
*Imported from [Google+](https://plus.google.com/116399413604240668728/posts/GDmZ8PPHNTV) &mdash; content and formatting may not be reliable*
