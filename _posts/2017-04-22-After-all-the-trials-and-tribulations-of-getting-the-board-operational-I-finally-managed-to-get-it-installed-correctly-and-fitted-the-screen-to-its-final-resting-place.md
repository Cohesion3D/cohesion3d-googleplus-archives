---
layout: post
title: "After all the trials and tribulations of getting the board operational I finally managed to get it installed correctly and fitted the screen to its final resting place"
date: April 22, 2017 19:17
category: "Show and Tell"
author: "E Caswell"
---
After all the trials and tribulations of getting the board operational I finally managed to get it installed correctly and fitted the screen to its final resting place. Although not finished until I get some acrylic to replace that nasty piece of aluminium.. 

Thought I would share pics.  

Only thing outstanding is to get the machine to run from the card correctly, that's on the back burner for now.  

Board mounted on left hand side of electronics bay  and standard LCD cables nicely reach. (The relays you can see are 24v coil controlled for 240v switching that will be converted to SS relays once I get the time.- board already purchased.)

Separate 24v 10a (overkill I know) supply feeding the board without that horrible plug from K40.All and runs nice and smooth.

Mounted the LCD in the position that the EM and machine control switch was in that recessed area. (the EM switch does catch the back mount now so installed a "bonnet" type prop to hold it up if I need to work on the electrics,

L wire independently wired to the 2.5 pin to the L output on original  LPSU.

I just need to set up the PWM and get the ratios right for a decent raster of  a Jpeg.





![images/ff43a42b22acf3f0fe4be664d9c72b72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ff43a42b22acf3f0fe4be664d9c72b72.jpeg)
![images/73825afa63001907c4f838f09133b4c3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/73825afa63001907c4f838f09133b4c3.jpeg)
![images/014f0eff1d13b391426546ae2d0d23b1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/014f0eff1d13b391426546ae2d0d23b1.jpeg)

**"E Caswell"**

---
---
**Griffin Paquette** *April 22, 2017 20:27*

Lookin good!!


---
**E Caswell** *May 17, 2017 19:57*

Finally got rid of that nasty piece of Ally.. :-)

![images/6a28e745e6757d7f66b6f245fffb030e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6a28e745e6757d7f66b6f245fffb030e.jpeg)


---
**John Milleker Jr.** *June 27, 2017 19:55*

Very nice!




---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/15Bu53RBHbY) &mdash; content and formatting may not be reliable*
