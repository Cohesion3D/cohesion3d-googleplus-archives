---
layout: post
title: "Ray, Can you explain or give me some hints how I would deal with using the Cohesion board with a larger format machine with differing X and Y gearing to the steppers and different travels"
date: April 12, 2018 18:28
category: "FirmWare and Config."
author: "Steven Whitecoff"
---
Ray, 

Can you explain or give me some hints how I would deal with using the Cohesion board with a larger format machine with differing X and Y gearing to the steppers and different travels.

 I dont see any thing that is obvious in the config.txt file beyond

 "steps per mm", if thats it, how in the world do I come up with that? 





**"Steven Whitecoff"**

---
---
**Chuck Comito** *April 12, 2018 21:25*

**+Steven Whitecoff**​, it is the steps per mm setting. If you Google search steps per mm calculator you'll find a ton of great ones. Just enter your parameters and away you go. 


---
**Steven Whitecoff** *April 13, 2018 02:09*

Sorry for the double post, could not find it after first post attempt....likely browser did not update when I reloaded page


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/6xt2YNfHmFY) &mdash; content and formatting may not be reliable*
