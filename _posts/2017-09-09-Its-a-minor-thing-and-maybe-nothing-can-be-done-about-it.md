---
layout: post
title: "It's a minor thing and maybe nothing can be done about it"
date: September 09, 2017 16:20
category: "C3D Mini Support"
author: "Tammy Mink"
---
It's a minor thing and maybe nothing can be done about it. I'm using the grbl firmwared Cohesive3D mini and LaserWeb. The issue is that at the edges of anything I'm Lasering (more noticeable for engravings of course)  it's always a little darker like it's lingering at the edges or something, the speed seems to not effect it much. Is there any settings that can help this in the firmware or LaserWeb? I know it might be just how it is but I figured wouldn't hurt to ask.





**"Tammy Mink"**

---
---
**Jorge Robles** *September 09, 2017 17:28*

Is a known issue. No speed compensation in lw4 yet.


---
**Tammy Mink** *September 09, 2017 17:52*

Thanks for the info :)


---
**Jason Dorie** *September 09, 2017 19:25*

Ping me on FB - I'm looking for testers.  I have a very small group for the moment.  I think you'd be a good fit if you're into it.


---
**Jorge Robles** *September 09, 2017 19:31*

**+Jason Dorie**​ want to contribute LW?


---
**Jason Dorie** *September 09, 2017 19:34*

**+Jorge Robles** - want to contribute to LightBurn?  ;)

I'm not a JS coder, and I feel like our ideas of design are disparate enough that I'm happier going my own way for the moment, but my counter-offer is legitimate.  It's a ton of work and so far it's just me.


---
**Jorge Robles** *September 09, 2017 19:47*

Sorry, didn't know about your project, no conflict intended. Anyway is always refreshing a different point of view. Please come to github an talk about your vision.


---
**Jason Dorie** *September 09, 2017 20:08*

I didn't take offense, or mean to offend - just being a little cheeky.  :)  I have used LW and appreciate the effort put into it, I just have some different goals in mind.  That said, there's a strong chance I'm going to open up my codebase, which would mean greater ability to share components.


---
**Todd Fleming** *September 09, 2017 22:13*

If you're rastering, try the Over Scan option to compensate.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/3TLas9KA8cc) &mdash; content and formatting may not be reliable*
