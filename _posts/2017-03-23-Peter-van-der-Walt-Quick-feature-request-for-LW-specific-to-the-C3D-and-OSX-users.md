---
layout: post
title: "Peter van der Walt : Quick feature request for LW specific to the C3D and OSX users"
date: March 23, 2017 22:35
category: "Laser Web"
author: "Bob Buechler"
---
**+Peter van der Walt**: Quick feature request for LW specific to the C3D and OSX users. When LW launches and/or when the user clicks Connect in the Comms section, can you add a check to see if the C3D SD Card is currently mounted, and if it is, throw an error? 



I ask because, as an OSX user, I'm running into situations where LW isn't even able to connect to the C3D when the SD card is mounted, because of how much traffic it creates over the USB cable. The moment I unmount the card, LW connects without a problem. 



If it helps, when the card mounts for me in OSX, it presents as "NO NAME", so I'd imagine a quick scan of 'diskutil list' output for disks mounted off of the C3D with that identifier would be enough to validate it. 





**"Bob Buechler"**

---
---
**Bob Buechler** *March 23, 2017 22:58*

Weird. Nope. This is a stock install of El Capitan (my Macbook Pro is just one minor revision too old to use Sierra). Nothing fancy about it. Sorry for the n00b question, but where do I put that .bin file?


---
**Alex Hodge** *March 24, 2017 02:08*

Good luck buddy. One time I made some suggestions for improvements to laserweb and was told basically that I was the only person in the world who wanted it to be more user friendly. Then I was banned from the laserweb google+ community and blocked from following Peter. It was weird. He doesn't take criticism well. No matter how constructive.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/eRRqocrRzKY) &mdash; content and formatting may not be reliable*
