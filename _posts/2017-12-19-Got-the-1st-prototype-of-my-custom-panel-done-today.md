---
layout: post
title: "Got the 1st prototype of my custom panel done today!"
date: December 19, 2017 06:04
category: "Show and Tell"
author: "Tech Bravo (Tech BravoTN)"
---
Got the 1st prototype of my custom panel done today!

![images/f58d73dc32a2e529166197a9f82f6cca.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f58d73dc32a2e529166197a9f82f6cca.jpeg)



**"Tech Bravo (Tech BravoTN)"**

---
---
**James G.** *December 19, 2017 11:28*

Looks great!


---
**David Allen Frantz** *December 19, 2017 13:02*

Looks good man. Pretty familiar if I may say so. 😉


---
**Tech Bravo (Tech BravoTN)** *December 19, 2017 13:44*

**+David Allen Frantz** yeah it looks similar lol. I really just started laying stuff out and it ended up that way. Wasnt on purpose haha. Im making another one. Maybe i mirror it? :). The layout just feels right as you are aware. It was suggested by someone on fb to move my pot, which is now just a current limiter, to the inside to reduce clutter and accidental changes. Hmmmmm




---
**David Allen Frantz** *December 19, 2017 13:49*

I don’t mind you being influenced by my design. It’s all about efficiency. Don’t worry about mirroring it brother, whatever works for you.  It’s your machine and this whole open source world is about doing whatever you can think of! Not saying you “stole” my design or anything. Lol. 


---
**Tech Bravo (Tech BravoTN)** *December 19, 2017 13:52*

Oh yeah. I didnt take it that way :) rev 2 will move pot to the inside and add a switch for exhaust i think. Still thinking everything over


---
**Tech Bravo (Tech BravoTN)** *December 20, 2017 18:41*

here is the .dxf file of my panel if anyone wants to use it.

[techbravo.net - Downloads - Bravo Technologies](https://techbravo.net/hosting/download/category/3/K40-Chinese-Lasers.html)


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 01:20*

**+David Allen Frantz** I looked at your panel again. I like that the laser power and test button are located under the meter and pot. hmmmmm....




---
**David Allen Frantz** *December 21, 2017 03:33*

Lol go for it man!!


---
**Joel Brondos** *December 30, 2017 00:29*

1) What is toggle switch for at the bottom left? Air assist? Z-Table?



2) Are BOTH of those meters active and measuring current? Where did you get (what looks like) that digital meter (top left)?



3) I see that the pot on the GLED changes the % above "Laser," but what does it actually do?



4) I bought some control face plates on Amazon: [amazon.com - Uxcell a15050500ux0783 Potentiometer Variable Transformer Round, 0-100 Control Dial Face Plate, 5 Piece: Amazon.com: Industrial & Scientific](http://a.co/agGe0vK)


---
**Tech Bravo (Tech BravoTN)** *December 30, 2017 00:51*

**+Joel Brondos** the toggle switch is for the air assist. the digital meter top left is the water temp coming out of the tube, the analog meter is the current meter for the tube. the potentiometer under the analog meter is the current limiter (was the laser power but power is now dictated through the cohesion3d controller. the knob on the gled can navigate through all the menus and jog the x,y,z, axis, make settings changes, etc...

you can see more at [techbravo.net - K40 Laser Info - Bravo Technologies](http://www.techbravo.net/k40) (watch the REVEAL video in the youtube playlist)

thanks for your interest! let me know if i can help :)


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/V68jbScp75m) &mdash; content and formatting may not be reliable*
