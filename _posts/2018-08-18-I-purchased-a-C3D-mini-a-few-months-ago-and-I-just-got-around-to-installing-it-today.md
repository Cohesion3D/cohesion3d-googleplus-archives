---
layout: post
title: "I purchased a C3D mini a few months ago, and I just got around to installing it today"
date: August 18, 2018 20:15
category: "C3D Mini Support"
author: "Tim White"
---
I purchased a C3D mini a few months ago, and I just got around to installing it today.  It ran for a bit, and then stopped working.  I investigated, and I'm now getting just the single red led light for vmot when power is applied; none of the other leds are lighting up or blinking as they were when I installed it this morning.  I've verified that the stepper motor wiring and all the other connections are sound and correct; I've also verified that the separate power supply I'm using is putting out a solid 24v.  Any ideas what might be wrong? 





**"Tim White"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 20, 2018 15:18*

Hi Tim, 

Could you take some pictures of the board and how it's wired up, and email us (info at cohesion3d dot com) to discuss the issue?

Thanks!


---
*Imported from [Google+](https://plus.google.com/107810046539384552772/posts/j6AkpsMCJVV) &mdash; content and formatting may not be reliable*
