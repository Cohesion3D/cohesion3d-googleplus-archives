---
layout: post
title: "My board arrived yesterday, won't be putting mine in just yet as my workshop PC software is on XP so won't run LW"
date: March 10, 2017 18:47
category: "C3D Mini Support"
author: "E Caswell"
---
My board arrived yesterday, won't be putting mine in just yet as my workshop PC software is on XP so won't run LW. :-(  Looks like an up-grade is on the cards.

My main PC in the house is on windows 10 and a few years old now so I may end up buying a new one for the house and use that in the workshop.  More expense.



I have had a look through Stevens thread so that should help as mine is the digital K40 version the same as his. I will read through it again once the house goes a little quieter.



I see the PWM wire is not used on this version



Quick question. 



How is the laser power controlled if that isn't connected to the board?  Just struggling to get my head around that one. :-O











**"E Caswell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 18:49*

PWM goes to L.  Pot/ Digital Display sets the current ceiling,  L is a percentage of that.  Works better this way. 


---
**E Caswell** *March 10, 2017 18:54*

is that done through the card configuration then? I don't need to connect anything to the digital layout.. does that mean I can take it out and use the space for the LCd display?


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 18:56*

You need a pot in play of some sort.  So you can''t unplug the digital display.  Where you put  it physically, your call. 



Leave the card in play, you don't need to modify anything unless I say so, it's all preconfigured for you. 


---
**Steven Whitecoff** *March 10, 2017 18:59*

No the display is untouched. Think of the buttons that increment the power setting as a digital equivilent of the pot in early machines. As Ray says, the orginal power buttons set the power at "S1" which is the gcode for full power. So if you set your code to S.5 an the meter reads 50%, you get 50% of 50% = 25%




---
**Steven Whitecoff** *March 10, 2017 19:00*

If you want to cut at full power then panel set to 100% and gcode is S1




---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 19:00*

However putting the panel at 100% may dampen the affect of the board PWM so that is not recommended.  10mA or less, depending on the material and application.  


---
**Steven Whitecoff** *March 10, 2017 19:04*

And FWIW, the laser wont fire below 10% approx. as it takes a certain power to lase at all. So you cant dial it down below that. Cant see why one would need less as you can engrave paper where you can barely see the line which light brown.


---
**E Caswell** *March 10, 2017 19:14*

Thanks for the fee-back. **+Steven Whitecoff** yep, fully understand the digital pots rather than a wiper version. **+Ray Kholodovsky**  Had a look at the pin diagram for the mini and "think" I understand it a little better now. 

Pity about the digital display. got me thinking now. Where to put the LCD..

Edit..

Is the output on a 4 to 20 Ma basis?


---
**E Caswell** *March 11, 2017 10:26*

**+Ray Kholodovsky** looking in to the LCD, can I download the g code to a sd card from my win 10 pc and run it on the laser? 


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 16:55*

We don't support the sd card slot of the LCD. However you can upload to the microsd card in the board itself.  It shows up as a flash drive over USB. 

Im not sure what your 4-20mA question is asking. 


---
**E Caswell** *March 11, 2017 18:22*

**+Ray Kholodovsky** thanks for that info. As for 4 to 20 it's a control circuit and was wondering if the card was using that philosophy. 4 being 0% and 20 being 100% see link below [en.m.wikipedia.org - Current loop - Wikipedia](https://en.m.wikipedia.org/wiki/Current_loop)


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 18:24*

Oh I wouldn't count on the Chinese abiding by any standard practices. We just pwm the L wire. The old way was with a level shifter - when we replaced the pot which is no longer recommended. 


---
**Steven Whitecoff** *March 12, 2017 02:03*

My understanding is while the "box" controls the power level, its a constant power. With PWM, you modulate the beam on and off rather quickly and the width of the pulse can be varied to vary the "effective" power level. So with PWM, you can in fact get less power for cutting/engraving than the minimum continuous power to excite the laser.


---
**E Caswell** *March 13, 2017 21:42*

**+Ray Kholodovsky** Set everything up so I can change everything over once I have my new PC. 



However I Connected the board up and downloaded a file from LW3 (LW4 not working at time) to the card, worked well but I think the homing and positioning needs setting up 

To get that done right am I right in thinking  I will need to connect with computer to see that's set right and working right. 



Otherwise it looks good so far. 


---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2017 21:44*

LaserWeb settings: [cohesion3d.freshdesk.com - LaserWeb Configuration : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726545-laserweb-configuration)



And have you seen my various notes in the group about not running the pwm cable and keeping the pot in play? 


---
**E Caswell** *March 13, 2017 22:07*

**+Ray Kholodovsky** Yes thanks Ray, discarded the white wire link and kept the digital pot in play. (Mine is the digital set up) Once I get set up I'm sure there will be a few niggles here and there that I am sure will be a resolution on here or the c3d website. Any problems I will be in touch. I will go on the laser web 4 G+ for software set up issues. 

Edit... I have put in the G28.2 code in but I have a feeling the head wasn't homed before I powered the board on..


---
**E Caswell** *March 15, 2017 21:32*

**+Ray Kholodovsky** I now have the PCin the workshop, the only problem I am having is network connection is poor.. yet another problem to sort.  :-( 



Installed the C3D today and now connected with LW3 with windows 10.

Board connected without PWM connection.



All homing and jog movements are working spot on, however the software laser test button is not working. Any pointers please?



Did  a test run raster and the job kept stopping and had the "halt" fault that keeps coming up mid run. Checked all settings as per the setup instructions and looks good. Any ideas?


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 21:37*

Yeah, software fire button doesn't work a bunch of the time.  Can run a G1 X10 S0.5 F600 gCode line manually if you want to test firing.  Try putting the gCode file on the memory card, run from the LCD, see if that fixes it? 


---
**E Caswell** *March 15, 2017 21:41*

**+Ray Kholodovsky** are you saying that I need good network connection to use it direct from PC Ray? 



I will test it again tomorrow, if it doesn't work I will use download to card and run from the screen. 


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 21:44*

No, I'm not saying anything about network.  You're running the job over USB right?  I'm trying to determine if noise or bad cable is causing your issue.  Hence, put gCode on memory card and run from LCD without usb. 


---
**E Caswell** *March 15, 2017 21:50*

OK, thanks will test run again direct from PC and if not then will do card run. Thanks.


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/hLnMRiNruXX) &mdash; content and formatting may not be reliable*
