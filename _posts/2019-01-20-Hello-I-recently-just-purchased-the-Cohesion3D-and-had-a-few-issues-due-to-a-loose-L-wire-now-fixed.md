---
layout: post
title: "Hello, I recently just purchased the Cohesion3D and had a few issues (due to a loose L-wire, now fixed)"
date: January 20, 2019 19:58
category: "C3D Remix Support"
author: "giuseppe Ilmago"
---
Hello,



I recently just purchased the Cohesion3D and had a few issues (due to a loose L-wire, now fixed).  After I fixed the L-wire issue I'm still having this problem of the print stopping almost as soon as I start the job.  Any info on how I can fix this?



thank you,

Giuseppe


**Video content missing for image https://lh3.googleusercontent.com/-q8F3d8lWZrA/XETS1edhtpI/AAAAAAAAKLE/SRP4t4TgfxQbb3Of-3SCF34vC9jiQr1jwCJoC/s0/VID_20190115_223123.mp4**
![images/3c4fb0db9b924050f2fecf5b0a0dd7bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c4fb0db9b924050f2fecf5b0a0dd7bd.jpeg)



**"giuseppe Ilmago"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:06*

So the issue is that the job stops/ the head stops moving? That's different than "the head keeps moving but the laser isn't firing". 



Please show pics of your full LightBurn window, what are the specs of your computer, and are you using the USB Cable that came with the laser or a better high quality one?


---
**giuseppe Ilmago** *January 21, 2019 19:14*

Again, the issue BEFORE was the laser isn't firing, but I mentioned on this post that I fixed that issue which was the loose L-wire.



The issue NOW is the head/job stops moving. which was also happening before, the difference now is that the laser is firing now. One part is fixed, but the video shows what is happening now.



I will attach the lightburn window, my computer is brand new gaming ith Windows 10 and I am using a high quality USB cable



Thank you

![images/712cffc4a86057286bb45e05e3941d16.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/712cffc4a86057286bb45e05e3941d16.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2019 04:41*

I'm not sure of what is causing the issue and will need more info, could you shoot a video of it happening so I can see your wiring, the LightBurn Window, etc.?


---
**giuseppe Ilmago** *January 25, 2019 18:52*

Yes, here is the video of the issue and connections

NOTE: the L-wire is now jumped with a twist tie, but had to cut and reconnect to fix the laser not firing.  The other wire for pot is jumped as well from the Gerbil, but it is connected as it should be. they were just cut and reconnected with the ties  

![images/792849b708ce2307151440c90f0fbe55](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/792849b708ce2307151440c90f0fbe55)


---
**giuseppe Ilmago** *January 25, 2019 18:52*

![images/18a0a1e4b81eb732d1ffed16a073825d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/18a0a1e4b81eb732d1ffed16a073825d.jpeg)


---
**giuseppe Ilmago** *January 25, 2019 18:52*

![images/2d8a4da570c3cb47d12424ceddec7d38.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d8a4da570c3cb47d12424ceddec7d38.jpeg)


---
**giuseppe Ilmago** *January 25, 2019 18:52*

If you need additional screens please let me know


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2019 19:34*

What does it say in the LightBurn Console? A pic of that please when the stop occurs. 



Also, can you try connecting the C3D power brick to a different outlet than what the K40 is on?  The only other person that was getting stops with LaserBoard, I asked him to run an extension cord to a different outlet and hook the C3D brick up to that, and all his issues went away. 


---
**giuseppe Ilmago** *February 03, 2019 00:01*

Hello,



So switching cables definitely helped! but since doing the extension cord and changing outlets, this is happening sometimes.  I have changed the USB cord a few times and it seems to make this issue better, but not sure if you maybe know the cause? I've never experienced this in my time using laser so just very strange.



  Anyways thanks for all the help!



Giuseppe

![images/de57a3cab34a664a5a31295d40c5e9a1](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/de57a3cab34a664a5a31295d40c5e9a1)


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2019 00:00*

I apologize about the lack of response.  Google + is shutting down soon and we have been moving everything over to [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](http://forum.cohesion3d.com) - if you could make a new post there with your latest situation we can continue to assist you.



Again, really sorry that I lost track of this and didn't respond for a week. 


---
*Imported from [Google+](https://plus.google.com/118036785421071885491/posts/f7aTCejAfMd) &mdash; content and formatting may not be reliable*
