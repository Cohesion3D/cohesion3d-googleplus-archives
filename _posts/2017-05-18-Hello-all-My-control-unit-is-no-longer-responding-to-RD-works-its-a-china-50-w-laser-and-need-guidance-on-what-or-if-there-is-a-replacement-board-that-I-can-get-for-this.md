---
layout: post
title: "Hello all, My control unit is no longer responding to RD works (it's a china 50 w laser) and need guidance on what or if there is a replacement board that I can get for this?"
date: May 18, 2017 17:39
category: "K40 and other Lasers"
author: "Thomas West"
---
Hello all, My control unit is no longer responding to RD works (it's a china 50 w laser) and need guidance on what or if there is a replacement board that I can get for this? also since i'm in the market why not upgrade? so... take a look and let me know what you think. thanks!



![images/d0440d9304953ea5ae3698b90bac2a5d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d0440d9304953ea5ae3698b90bac2a5d.jpeg)
![images/8da0ea038e2c89e105a94842769a25a2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8da0ea038e2c89e105a94842769a25a2.jpeg)

**"Thomas West"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 17:49*

Hi Thomas.  Great speaking with you earlier. 

Yep, this is exactly what I envisioned - it's the 50W which has the white control box and 2 external stepper motor drivers. 



As discussed, you will need a motion controller to generate the signals to those drivers and get commands from the LaserWeb software on your computer. 



Either Cohesion3D board will do the trick. (Mini or ReMix). 



I would get the Laser Upgrade bundle (recommend adding the GLCD Adapter and GLCD screen so that you have a heads up display ) + some external stepper adapters to interface to the existing XY external drivers and your possible Z bed and rotary in the future.



[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



[http://cohesion3d.com/external-stepper-driver-adapter-for-pololu-style-socket/](http://cohesion3d.com/external-stepper-driver-adapter-for-pololu-style-socket/)


---
**Thomas West** *May 18, 2017 19:01*

thanks for the quick response, I'll take a look and let you know if I have any questions!


---
*Imported from [Google+](https://plus.google.com/113017472460485580353/posts/gYmDLZuyLut) &mdash; content and formatting may not be reliable*
