---
layout: post
title: "I'm really struggling to get an acceptable cut of 1/16\" laser ply with my 40W laser - edges and top surface are badly burned to point component is unusable"
date: June 18, 2018 14:59
category: "General Discussion"
author: "Scruff Meister"
---
I'm really struggling to get an acceptable cut of 1/16" laser ply with my 40W laser - edges and top surface are badly burned to point component is unusable. 



To be clear, I don't have an air-assist so part of this question really is "would it be worth me building one? Is it a real game-change for this sort of thing?"



More Info: using my C3D controller, running it at full power (20mA) with feed rate of 10mm/sec I am requiring 2 passes to JUST get through the material BUT the edges are so burned that the component isn't usable. I've tried faster feeds/lower power/more passes but nothing has really improved the cut to the standard I was hoping. I've done ramp tests and cleaned optics, plus 1/16" balsa cuts like a dream at 65mm/sec at 45% power so am happy all is focused I think.



A secondary question, is if anyone can suggest better settings for 1/16" laser ply cut with 40W (although I realise woods and laser machines vary!)



Thank you!









**"Scruff Meister"**

---
---
**Tech Bravo (Tech BravoTN)** *June 18, 2018 15:06*

20mA is pushing it. You can seriously shorten tube life that way. many seasoned k40 owners concur that 16mA is about as far as you can go. yes, air assist is a game changer. without air assist and/or smoke assist your lens will not last long and your power will decrease rapidly until your lens fails. Visit [k40laser.se - Air assist - K40 Laser](https://k40laser.se/air-assist/)




---
**Scruff Meister** *June 18, 2018 16:36*

Thanks **+Tech Bravo** for the advice on laser power, I'll turn it down at the pot!



I'm definitely adding smoke and air assist features. Could I just clarify that the air assist stops edges from charring (as well as having a good effect on lens life)?


---
**Joe Alexander** *June 18, 2018 17:24*

air assist helps tremendously. Not only does it move the smoke and debris from the cut allowing it to cut faster but it "blows out" any of the flare ups that occur.


---
**Anthony Bolgar** *June 19, 2018 01:03*

I would check the mirror alignment, sound like yours is way off. You should be able to cut 1/16 ply like butter at much less current.




---
**LA12 Sports Images LA12 Sports Images** *June 19, 2018 08:58*

Initially I thought I had to run at full power to cut through 3mm Laserply but  discovered if I set my focus point in the middle of the surface being cut it improves things dramatically, so for 3mm ply on a 50.8 mm focal length K40 I setup for a cut at focal length minus 1.5mm. This lets me cut at 35% power and 10mm/s, parts just fall out with very little darkening to the edges unlike the black mess full power leaves.


---
*Imported from [Google+](https://plus.google.com/116126524987588221083/posts/VKkw8fNLX4f) &mdash; content and formatting may not be reliable*
