---
layout: post
title: "Air assist - I'm looking at options at the moment (not an air assist head)"
date: October 17, 2018 03:58
category: "K40 and other Lasers"
author: "Kieron Nolan"
---
Air assist - I'm looking at options at the moment (not an air assist head).



Option one is to use an aquarium air pump, coiled hose and outlet near the head.



Option 2 is to install PC fans inside the front of the machine to blow air across the work area towards the rear outlet.



With either I can use mains voltage or 24V. I'll have the original power supply unit plus the additional one to power the Cohesion board.



I'm interested to know people's thoughts on either solution and the best way to power them.



Cheers!





**"Kieron Nolan"**

---
---
**Tech Bravo (Tech BravoTN)** *October 17, 2018 04:05*

Fans are smoke assist which is a good idea. I use a $3 section of copper brake line for air assist in conjunction with a "commercial" aquarium pump and it works very well. 


---
**Anthony Bolgar** *October 17, 2018 12:25*

Ya, having both is a good idea. The "air assist" will help with your ability to cut at a higher speed or a lower power setting, and the "smoke assist" will help keep smoke stains off engravings. They both serve a purpose, so installing both is a good idea.


---
*Imported from [Google+](https://plus.google.com/113795912126148646501/posts/YGvuoz4tQto) &mdash; content and formatting may not be reliable*
