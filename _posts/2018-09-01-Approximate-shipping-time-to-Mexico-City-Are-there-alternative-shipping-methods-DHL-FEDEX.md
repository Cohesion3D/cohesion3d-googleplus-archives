---
layout: post
title: "Approximate shipping time to Mexico City? Are there alternative shipping methods (DHL, FEDEX)?"
date: September 01, 2018 16:43
category: "C3D Mini Support"
author: "drjcpp"
---
Approximate shipping time to Mexico City? Are there alternative shipping methods (DHL, FEDEX)?





**"drjcpp"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2018 23:16*

We ship via USPS.  The transit time for first class is usually 2-3 weeks but can be longer, and for priority it is stated as 6-10 business days.  

Neither is guaranteed transit time. 


---
*Imported from [Google+](https://plus.google.com/101782563262158927493/posts/hhCaefwRbax) &mdash; content and formatting may not be reliable*
