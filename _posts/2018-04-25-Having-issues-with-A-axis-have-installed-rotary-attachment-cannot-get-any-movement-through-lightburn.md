---
layout: post
title: "Having issues with A axis. have installed rotary attachment cannot get any movement through lightburn"
date: April 25, 2018 12:02
category: "C3D Mini Support"
author: "Josh Smith"
---
Having issues with A axis. have installed rotary attachment cannot get any movement through lightburn. have upgraded the firmware and config from a working version. A motor is locked just won't turn if I plug it into Z axis or Y Axis works just nothing in A axis. 





**"Josh Smith"**

---
---
**E Caswell** *April 25, 2018 12:26*

Just a bit more info here.. been working with Josh on A axis issues off line.. 

His set up was originally nema 17 with external stepper driver.. 

To prove the nema 17 was working and not a wiring issues  the stepper motor only has been connected directly to the board and 4988 driver on the Y axis. That proved and is working correctly. So the stepper is working fine.

He has my config file and the firmware I use, he has flashed that and it is loading as proved by the file info on the Glcd. 

I have asked josh to open up the config file to make sure it's been uploaded and is the correct one..waiting for that answer.

The stepper also works off the Glcd  by plugging directly in to the board Z axis and y axis.

 I believe it's  Firmware issue and not a stepper or board issue. 










---
**Jim Fong** *April 25, 2018 15:22*

Which firmware version? I did the initial gcode beta rotary testing for LightBurn. I tested both grbl-lpc and smoothie with the A axis stepper driver.   It’s been a few weeks since I used the rotary so things may have changed with LightBurn. 



Type version in the LightBurn console and post it.  I’ll test that version out. 


---
**Josh Smith** *April 25, 2018 15:27*

I Am using April 18, 2017, edge-a1108ea9 as popsE Cas gave me with his config file.




---
**Jim Fong** *April 25, 2018 15:30*

**+Josh Smith** ok I’ll try to test that version as soon as I can and let you know. 



I just looked and that doesn’t seem to be one of the multiaxis firmware versions I posted for rotary.   

Can you post the actual LightBurn screen shot for version 



Thanks. 


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2018 15:31*

Jim - from fb they're saying that sending a G0 A move doesn't move A. 



Try using the firmware and config file from the z table/ rotary article. Back up the ones you've got now. 


---
**Josh Smith** *April 25, 2018 15:44*

What is latest 4 axis version the one precompiled on the help pages on your site ray August 18 2017?  4 axis? this is the current config that PopsE Cas gave me linked off my drop box.

[dropbox.com - Config.txt](https://www.dropbox.com/s/0d3uqq6hp0t6u7e/Config.txt?dl=0)


---
**Jim Fong** *April 25, 2018 15:45*

**+Ray Kholodovsky** oh it is one that I compiled but older version that I didn’t test with LightBurn, only with laserweb...



I’ll test tonight after work.   Darn I pulled out the stepper motor rotary cable when I swapped boards last week.  Need to make a extended cable with proper connector.....


---
**E Caswell** *April 25, 2018 16:31*

**+Jim Fong**. **+Ray Kholodovsky** I'm using that version of firmware currently with LightBurn Jim. It's what I used to test the rotary when Oz sent it  me in Beta..   it works fine for me..  

what I'm not sure about is that the config file Josh is using, I sent him mine with my configuration but not sure it's been added to his boot directory or loaded 

Just to add, it's one I have been using daily since the launch of LB rotary. 




---
**Jim Fong** *April 25, 2018 16:57*

**+PopsE Cas** I’ll still check with my setup anyway to see if there are any other issues. 



Can you post the LightBurn file for your other grbl issue.  I’ll test that too.


---
**Josh Smith** *April 25, 2018 17:13*

So is there an updated version of firmware I should try? Should I try with laseeweb?


---
**Jim Fong** *April 25, 2018 17:52*

**+Josh Smith** best stick with the one posted on Cohesion3d website.  That’s one most used by others.  




---
**E Caswell** *April 25, 2018 18:42*

**+Jim Fong**, **+Josh Smith** I downloaded that one a few weeks back and it wouldn't run my A axis. So went back to the one that did. That's the one you built Jim over a year ago and it's worked very well for mine.. if there is a newer one then I can try that. 


---
**Jim Fong** *April 25, 2018 19:03*

**+PopsE Cas**  if it works then don’t change. The only other version one might consider is the nomsd, no sdcard version I post couple weeks ago.  Use that only if you are having USB dropout problems. 



I tested that with rotary on my setup. 


---
**E Caswell** *April 25, 2018 19:33*

**+Jim Fong** Exactly, if it works don't fix it..  Yep, I do have drop outs at times so might be worth trying that at some point. will look back at the posts and see if I can find it and test it out when I get chance.

But I can't understand why that firmware is not working with Josh's machine.  

I did offer a video chat or phone call to go through it with him but I think the little babes is teething so not getting much peace. :-O 


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2018 19:40*

Frankly it might be easiest to make sure the A socket is working on the board by changing the step dir en pins in the config file between Z and A for example. 



But the firmware and config XYZA no endstops from the article <b>should</b> work....


---
**Josh Smith** *April 25, 2018 19:42*

Tell me what you want me to do as mine is defenly not working 


---
**E Caswell** *April 25, 2018 21:17*

**+Ray Kholodovsky** 

Delta-step pins for Gamma step pins if I remember rightly Ray?




---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2018 21:20*

Yep. 


---
**Josh Smith** *April 25, 2018 21:47*

In config txt ok will try that first before I try upgrading to jim’s new firmware 


---
**E Caswell** *April 25, 2018 21:48*

**+Ray Kholodovsky** lets wait to see what Jim comes back with first.

**+Josh Smith** I will have a look at changing a config file over for you to see if Z works. Unless you want to have a go at it? 

Basically changing the config file so that the gamma_pin 2.2 and the following direction steps are changed with Delta_pin 2.3 and following direction steps in Delta are changed. 


---
**Josh Smith** *April 25, 2018 21:59*

like so

![images/71f0126ef1995c23974ed41d80d322f1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/71f0126ef1995c23974ed41d80d322f1.png)


---
**Josh Smith** *April 25, 2018 22:10*

just one with that config do I keep it plugged in a or z axis




---
**Josh Smith** *April 25, 2018 23:06*

Works with it plugged in z get movement on the a axis. Will swap over to aA Axis tommorow and see if I get movement on z command 


---
**Jim Fong** *April 26, 2018 02:34*

Just did a simple rotary test using the 4 axis firmware on Cohesion3d website dated Aug 12 2017.   Vector cut on rolled up paper.  Rotary on A axis 



LightBurn 0.7.00 






---
**Jim Fong** *April 26, 2018 02:41*

![images/69955e3ddfb455e9a97bc3338206006d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69955e3ddfb455e9a97bc3338206006d.jpeg)


---
**Jim Fong** *April 26, 2018 02:49*

The new rotary jog keys on the latest version of LightBurn works for A axis.  No need to manually enter gcode move commands to rotate. 


---
**E Caswell** *April 26, 2018 06:42*

**+Jim Fong** jog keys are a big plus from me as well jim..


---
**E Caswell** *April 26, 2018 07:34*

**+Josh Smith**is that a axis moving with it plugged in to z axis  socket using LB  a axis jog buttons Josh? 


---
**Josh Smith** *April 26, 2018 08:08*

**+PopsE Cas** yes that is correct using the jog keys in LB i am yet to burn anything will give it ago later as well as swapping ports and see if a pins are not working.




---
**E Caswell** *April 26, 2018 16:46*

**+Josh Smith** have you got the external drive in place now then.?  The reason I ask is it depends on what load you rotary is liable to put on the board and not overload it. 

That said a Nema 17 on a rotary should be ok but it depends on the design of rotary if it's  roller or chuck! .




---
**Josh Smith** *April 26, 2018 20:45*

No movement on A axis port swapped it back  z axis port moves no laser activity firing  video shows it rotation the laser firing is me hitting firing.test button

![images/867befc2d3c540f398fe2bb8ba5630f4](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/867befc2d3c540f398fe2bb8ba5630f4)


---
**Josh Smith** *April 26, 2018 20:46*

**+PopsE Cas** **+PopsE Cas** no removed exsternal drivers 


---
**Josh Smith** *April 27, 2018 10:45*

**+Ray Kholodovsky** looks like my A port don't work on the board? any ideas?




---
**E Caswell** *April 27, 2018 10:49*

**+Josh Smith** just for confirmation, have you used the same driver in A as you did on Z? So we know that is running with the same configuration and same parameters. 


---
**Josh Smith** *April 27, 2018 10:51*

**+PopsE Cas** yes I did just swap ports.


---
**E Caswell** *April 27, 2018 10:57*

**+Josh Smith** I know it might sound silly, but I guess you changed the config file over as well? It also might be worth trying the firmware that is on the C3D Z table upgrade page. That way it's using the firmware that is identified by Ray as being the correct one. Just trying to cover all options. 


---
**Josh Smith** *April 27, 2018 11:02*

**+PopsE Cas** will use the update tonight when i get in from this pesky day job lol




---
*Imported from [Google+](https://plus.google.com/104342512578563937192/posts/FKbDTa2yz49) &mdash; content and formatting may not be reliable*
