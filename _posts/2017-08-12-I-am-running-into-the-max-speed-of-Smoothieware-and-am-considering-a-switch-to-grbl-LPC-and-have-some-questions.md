---
layout: post
title: "I am running into the max speed of Smoothieware and am considering a switch to grbl-LPC and have some questions"
date: August 12, 2017 14:32
category: "General Discussion"
author: "Cris Hawkins"
---
I am running into the max speed of Smoothieware and am considering a switch to grbl-LPC and have some questions.



1. Does grbl-LPC support the GLCD? (I don't believe the 8 bit grbl does)



2. Can grbl-LPC run off the SD card or is LaserWeb required?



3. Is there a group online that is dedicated to grbl-LPC?





**"Cris Hawkins"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 14:37*

1. Nope, no GLCD support in grbl-lpc. 

2. Grbl does not show the sd card nor support . I explain it as: if you have the glcd and want untethered, use Smoothie and run jobs off the card. Grbl is "raw power specifically for streaming jobs over USB from LW". 

3. No dedicated group for grbl-lpc. It was ported with the help of the LaserWeb devs, so this group is as good a place as any. 



What max speed issues are you running into? **+René Jurack** runs his dice 3d printer at 1 meter /second off the Mini. 


---
**Cris Hawkins** *August 12, 2017 14:49*

Thanks Ray! (Although the answers aren't the ones I was hoping for)



In regard to my max speed issue, as mentioned in a previous post, I am using external stepper drivers and have numerous choices for microstepping resolution. I found that increasing the resolution resulted in quieter, smoother movement. But as I increased the resolution I sacrificed top end speed. This isn't a problem at this point, but I am eyeing this setup for a rudimentary Pick-and-Place machine I'm building, where top end speed can make a difference....


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 14:57*

Ah, external drivers.  I should have known.  Jim bench tested the Mini and found consistency with Smoothie's claims of 99khz step rate for a single axis and a nice 90khz for multiple axes.  I do believe I heard something about higher Microstepping being a concern, and Rene is using 1/16ms Trinamic Drivers that internally create 1/256 microsteps. But the Mini only has to create 1/16 ms so he can achieve those speeds.



Yeah, Pnp is fun stuff. 

Sounds like go slower or decrease Microsteps is the best I can suggest. :|


---
**Cris Hawkins** *August 12, 2017 15:08*

My testing confirms the ~100 kHz max step rate. It is definitely a trade-off with speed vs microsteps. But as I think about it now, the laser cutter doesn't need high speed since it mostly operates at a G1 feedrate. However, the PnP is ALWAYS moving as fast as it can. therefore higher resolution on the PnP is not really used. I am amazed at how fast I can get these small motors to move with my setup (I've gotten it up to 3 m/s).


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 15:11*

I think it's pretty good for something that isn't positioned nor priced as an industrial grade controller, and that it is running industrial grade drivers and motors. 



"Sure we could do faster but it would cost 10x as much" :)



Appreciate the feedback as always. 


---
**Jim Fong** *August 12, 2017 15:37*

Yes stock smoothieware firmware does about 90khz.  I was told by Art that there was some compile settings to increase it but never tested it out. 



I run into the same issues with high microstepping drives or high servo encoder ppr.  One reason why I use a external pulse adapter board on my gantry CNC.  There are some that do several MHZ pulsing. 



I have only tested grbl-lpc out to 80khz. 



Grbl-lpc doesn't require Laserweb.  Any grbl compatible gcode streamer should work. I use grblpanel when testing on my workbench  however I find Laserweb4 streams gcode much much faster.  



Only Laserweb4 was able to send gcode fast enough on my corexy machine at 900+ mm/sec speeds. Grblpanel would stall when send serial blocks at that speed. Most machines don't go that fast anyway. 




{% include youtubePlayer.html id="O3NwQxUiyS4" %}
[https://youtu.be/O3NwQxUiyS4](https://youtu.be/O3NwQxUiyS4)





3m/sec speeds is crazy fast. I'm not sure if I ever seen any diy machine reach those speeds.  What is your mode of transport? 


---
**René Jurack** *August 12, 2017 18:50*

Discussing all the speeds... Is the hardware even capable of catching all the inertia? 3m/s with 100g head-weight... I bet there is overshooting. 🤣


---
**Cris Hawkins** *August 12, 2017 18:56*

OOPS ! My mistake. I got my units confused (my memory isn't as reliable as it used to be). Not 3 m/s, more like 40 m/min - which is .666 m/s. Thank you for making me look again....



Yes, 3 m/s would be CRAZY fast !


---
**Jim Fong** *August 12, 2017 20:32*

**+Cris Hawkins** darn I was hoping to see a crazy fast machine!!!!


---
**Jim Fong** *August 13, 2017 03:24*

I did a grbl-lpc pulse test. With the minimum 3us step pulse width, 210khz is the maximum step pulse rate. 



Grbl won't let me set the pulse width less than 3us.  



My Geckodrives have a minimum 4.5us pulse width. My Parker step drives can do 1us.  Very few stepper drives can go over 200khz due to the relative slow optocoupler.  Drives that can accept much higher step pulse rates use a pretty expensive high speed optocoupler such as HCPL2531 which can do 2.5mhz. 



Pretty good pulse output. 

[https://imgur.com/a/lP4oG](https://imgur.com/a/lP4oG)


---
**Cris Hawkins** *August 13, 2017 05:40*

**+Jim Fong** that's good to know, thanks for checking.



I checked the specs of my drivers for the heck of it.



200 nanosecond minimum pulse width

2 mHz maximum pulse rate


---
**Jim Fong** *August 13, 2017 10:52*

**+Cris Hawkins** I double checked the specs on my Parker drives and they are similar.  200ns and 2mhz. 

254 microstepping capable.  I have no gcode board that can go that fast.   I'm curious to see how a smooth a step motor can run at 2mhz. My Tek freq generator can go that high.....I should make a short video. 


---
**Cris Hawkins** *August 13, 2017 14:35*

**+Jim Fong** keep in mind that once you get past the point the driver is a current source and becomes a voltage source (due to motor inductance and waveform frequency), it doesn't matter to the motor if it's microstepped or not. At that point, motor inductance is the prime determinant of the waveform shape (and it is not close to sinusoidal). At some RPM, there becomes no difference between full stepping and microstepping.



Microsteps are most effective at the lower RPMs where the driver is still determining the current that the motor sees.



The real reason for the need for high frequency is that you can have a high number of microsteps for very slow speed smoothness, but you still want to be able to spin the motor fast. The drives that I have (Compumotor ZETA4) rectify and filter line voltage (120 V) so can deliver about 170 V. This makes it possible to get pretty high RPM.


---
**Jim Fong** *August 13, 2017 16:10*

**+Cris Hawkins** sorta knew that since Geckodrives morphs into a full step drive around 600rpm. I think that varies depending on motor inductance. This is where microstepping is no longer needed for smoothness.  You explained it better than I could. 



One of my drives is a Parker E-AC which plugs into line voltage.  I assume it rectifies to 170volts.  It's for my rotary 4th axis. I rarely use it so it only gets plugged in when needed.  Nice feature actually. 














---
*Imported from [Google+](https://plus.google.com/112522955867663026366/posts/78qvnSNohu7) &mdash; content and formatting may not be reliable*
