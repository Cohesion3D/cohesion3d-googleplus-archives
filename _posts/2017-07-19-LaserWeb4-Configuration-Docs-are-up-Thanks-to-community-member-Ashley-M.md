---
layout: post
title: "LaserWeb4 Configuration Docs are up: Thanks to community member Ashley M"
date: July 19, 2017 20:01
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
LaserWeb4 Configuration Docs are up:



[https://cohesion3d.freshdesk.com/solution/articles/5000743202-laserweb4-configuration](https://cohesion3d.freshdesk.com/solution/articles/5000743202-laserweb4-configuration)



Thanks to community member **+Ashley M. Kirchner** for providing the screenshots!





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ariel Yahni (UniKpty)** *July 19, 2017 20:16*

Should point that 72 pxpi is default for Illustrator


---
**Ashley M. Kirchner [Norym]** *July 19, 2017 20:25*

Yeah sorry, I only use Illustrator. It's worth getting the different resolutions for other applications and list them.


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2017 20:36*

What's another common value? 96?


---
**Ashley M. Kirchner [Norym]** *July 19, 2017 20:39*

Your guess is as good as mine. I'll defer to other folks who use apps such as Inkscape and others.


---
**Todd Fleming** *July 19, 2017 20:40*

It ignores pxpi when reading Inkscape files :)


---
**Ariel Yahni (UniKpty)** *July 19, 2017 20:40*

**+Ashley M. Kirchner**​ no sweat. Inkscape v0.91=90dpi and v0.92=96dpi


---
**Ariel Yahni (UniKpty)** *July 19, 2017 20:41*

**+Todd Fleming**​ is correct. This was added lately. 


---
**Todd Fleming** *July 19, 2017 20:46*

I was forced to add auto detection. Inkscape .92 produces wildly different pxpi values based on various settings, making it it hard to document. Fortunately all versions over the past few years embed the value in the header.


---
**Ashley M. Kirchner [Norym]** *July 19, 2017 20:55*

With Illustrator SVG files, forcing a different dpi in LW4 can, in some cases, be an advantage. Essentially the graphic gets scaled when pulled into LW4. So knowing that Illustrator saves SVG files at 72 dpi, if I set LW4's setting to 144 dpi, all imported <b>vector</b> files will get scaled down 50%. :)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/TJmRf9gT4Fz) &mdash; content and formatting may not be reliable*
