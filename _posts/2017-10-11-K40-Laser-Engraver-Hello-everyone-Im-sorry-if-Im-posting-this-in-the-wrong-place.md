---
layout: post
title: "K40 Laser Engraver Hello everyone! I'm sorry if I'm posting this in the wrong place"
date: October 11, 2017 08:29
category: "K40 and other Lasers"
author: "\u0421\u0442\u0435\u0444\u0430\u043d \u0421\u0438\u043d\u043e\u0431\u0430\u0434"
---
    K40 Laser Engraver

Hello everyone! I'm sorry if I'm posting this in the wrong place.

I have a huge problem with the engraving quality that I'm getting with my K40 laser.

As you can see in the pictures some letters are crooked and wavy.

I have tried to fix this in many ways...

I have cleaned, screwed, tightened, lubricated everything, without any improvement.



I would be thankful if I can get someone's opinion on I how I may try to solve this problem.



Thanks! 



![images/f30e4c5c28135d4ae846370047672a09.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f30e4c5c28135d4ae846370047672a09.jpeg)
![images/583588efc2107ceb8c188b7e740a35c4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/583588efc2107ceb8c188b7e740a35c4.jpeg)

**"\u0421\u0442\u0435\u0444\u0430\u043d \u0421\u0438\u043d\u043e\u0431\u0430\u0434"**

---
---
**Claudio Prezzi** *October 11, 2017 10:13*

I would guess that the wheels on the x carrage are not turnung rund or the track is dirty.


---
**Claudio Prezzi** *October 11, 2017 10:17*

It could also be an interference of the stepper steps. It this is the case, the "wavelength" of the wobble should change if you change the x travel speed.


---
**Claudio Prezzi** *October 11, 2017 10:18*

I've also read from someone, that he had interference from vibrations of the blower or air assist pump.


---
**Стефан Синобад** *October 11, 2017 11:24*

**+Claudio Prezzi** thanks for your reply.

The wheels are turning and the track is clean.

took of the blower and put the head without the blower, still no change.

Can you explain your second post to me? Maybe a picture of what you think is wrong?

Thanks for your help.




---
**Claudio Prezzi** *October 12, 2017 10:50*

The stepper motors cause vibration during moves. If that virbration gets in resonance with the mechanics, it can cause accumulation. By changing the speed, you could tune the frequency and probably get out of resonance.


---
**Стефан Синобад** *October 12, 2017 11:48*

Hello **+Claudio Prezzi** I tried adjusting the speed... i went from 250.. 200...150... 100... the engraving is still wavy :(


---
**Douglas Pearless** *October 14, 2017 01:05*

Perhaps try engraving a hollow rectangle so effectively on the X or Y are working and any given time and see it it is related to one or other axis movement.


---
**Claudio Prezzi** *October 15, 2017 09:18*

If speed doesn't change the waves, it probably isn't related to vibration.

To see if your axes move staight, you can engrave a simple rectangle (from vector file svg or dxf!).


---
*Imported from [Google+](https://plus.google.com/+СтефанСинобад/posts/3da1AiHoXJh) &mdash; content and formatting may not be reliable*
