---
layout: post
title: "Hi guys. I have a k40 with digital display"
date: February 28, 2017 09:23
category: "FirmWare and Config."
author: "Andreas Benestad"
---
Hi guys.

I have a k40 with digital display. Did the switch to C3d a few weeks back, and have been practising cutting and engraving. But I was just today made aware that I am still not engraving greyscale..haha. 

So... It seems to be a problem with my psu or pwm config or something... My knowledge and experience is so limited, so I would like to ask how I can troubleshoot this..? Does my psu even work with C3d??



Here is a picture of my psu (ignore the wires in front, it was for something else).



Thank you so much!

![images/93cec91c2baa2fc5eac824acde133ca7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/93cec91c2baa2fc5eac824acde133ca7.jpeg)



**"Andreas Benestad"**

---
---
**Don Kleinschnitz Jr.** *February 28, 2017 10:34*

That PSU works fine.

How do you know you are not doing grey shades?

Pictures of some work?


---
**Kostas Filosofou** *February 28, 2017 11:02*

**+Andreas Benestad** try to change in config laser_module_minimum_power to 0.1 and laser_module_pwm_period to 200 ... that works for me..


---
**Andreas Benestad** *February 28, 2017 11:11*

**+Konstantinos Filosofou** Thanks, I'll try that. The current values are 0.0 (minimum power) and 20 (pwm_period).


---
**Kostas Filosofou** *February 28, 2017 11:22*

**+Andreas Benestad** yes correct ... i use to have exactly the same problem .. no matter what the setting in LW i can't really engrave with different power levels .. only the laser beam goes on and off .. after i change that values works fine with grayscale engraving 


---
**Andreas Benestad** *February 28, 2017 11:26*

**+Konstantinos Filosofou** Cool. Can I ask you what settings you have in the Laser Power Constraints??

Also, I have been struggling to control the speed of engraving... is there anywhere else to change that than the Raster: Proportional Feedrate?


---
**Andreas Benestad** *February 28, 2017 11:35*

**+Konstantinos Filosofou** Hm... still not working. Just burning a solid rectangle, no shades... :-/


---
**Kostas Filosofou** *February 28, 2017 12:03*

**+Andreas Benestad**​ have you reset the board after the changes?


---
**Andreas Benestad** *February 28, 2017 12:06*

**+Konstantinos Filosofou** Yes, Ejected the drive with the config file. Reset the board. Switched it off.


---
**Kostas Filosofou** *February 28, 2017 12:14*

**+Andreas Benestad**​ what kind of image did you use for engraving in LW  .bmp ? And your settings? Beam diameter?


---
**Andreas Benestad** *February 28, 2017 12:21*

**+Konstantinos Filosofou** I have tried both jpg and bmp. Does it only work with bmp?

Settings: Raster proportional rate: 300 light, 400 dark.

Laser power constraints: 0 min, 30 max (have tried different things here)

Laser beam diameter in the LW settings: 0.15


---
**Kostas Filosofou** *February 28, 2017 12:43*

**+Andreas Benestad**​ I always export in .bmp I don't know really if makes any difference.. your settings is similar with mine.. I just set the minimum percentage in 5% as you can see in the photos the left one is before I make the changes in config and the right one after the changes

![images/dec7b20af60569641f08a35becaa0b01.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dec7b20af60569641f08a35becaa0b01.jpeg)


---
**Andreas Benestad** *February 28, 2017 12:50*

**+Konstantinos Filosofou** What power do you have on your pot (is that what it's called? the actual power on the laser machine)?


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 12:50*

You may also need to set the pwm period to 400 


---
**Kostas Filosofou** *February 28, 2017 12:53*

**+Andreas Benestad**​​ yes right.. I have 2volt 15mah


---
**Andreas Benestad** *February 28, 2017 12:53*

**+Ray Kholodovsky** So from the original 20 to 400 in config?


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 12:55*

Yes, we have seen that high needed sometimes. 

20 --> 200 --> 400

Try each of those values in config. 

Also do you remember my paragraph about run a G1 X10 S0.4 F600 line with the different S values and see if you can get different burn powers? 


---
**Andreas Benestad** *February 28, 2017 12:59*

**+Ray Kholodovsky**​ still not solving it. This was supposed to be a nice little picture of me. 😉 

Let me try those commands. 

![images/3b7b8605d2e27cf20a6541f486b27a66.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b7b8605d2e27cf20a6541f486b27a66.jpeg)


---
**Andreas Benestad** *February 28, 2017 13:08*

**+Ray Kholodovsky**​ Tried different S values. No visible difference.. 

![images/9e455a0627cdd06911f97dc41603cb81.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9e455a0627cdd06911f97dc41603cb81.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:13*

Agreed. Ok back to hardware stuff. Remind me how you are wired. What is the value on the digital pot?


---
**Andreas Benestad** *February 28, 2017 13:15*

This is the digital display. Is that you meant by digital pot?

![images/907b18e42a5f4008d60ff51c4aa96642.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/907b18e42a5f4008d60ff51c4aa96642.jpeg)


---
**Andreas Benestad** *February 28, 2017 13:16*

Can you see enough from this pic?

![images/b55e1621200ab37cb412b7b37a7de19d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b55e1621200ab37cb412b7b37a7de19d.jpeg)


---
**Joe Alexander** *February 28, 2017 13:17*

Try pulling a greyscale test template like Konstantinos shows in his pic as your "dummy image". all the pics I tried had the same error until I pulled a good formatted one. If there isn's enough contrast between you and the background it becomes a black box


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:18*

Yes, what is the current indication on the digital display showing? 

Also, please show me the laser section of the config.txt


---
**Andreas Benestad** *February 28, 2017 13:20*

**+Ray Kholodovsky**the current indication on the digital display is on 30 (is that % or what..? no clue).




---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:21*

Show me a picture of it with the machine on 


---
**Andreas Benestad** *February 28, 2017 13:22*

**+Ray Kholodovsky** Laser module config

![images/9238fdc62845374d4d5d91754d2eb8dc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9238fdc62845374d4d5d91754d2eb8dc.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:23*

Config looks good as well. 


---
**Andreas Benestad** *February 28, 2017 13:25*

**+Ray Kholodovsky**​ the actual machine.

![images/b851a0a629057a24f1e35652498cd4e4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b851a0a629057a24f1e35652498cd4e4.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:28*

Picture of panel with machine on please. 


---
**Andreas Benestad** *February 28, 2017 13:29*

Sorry what do you mean by panel?


---
**Andreas Benestad** *February 28, 2017 13:29*

**+Ray Kholodovsky**​ this?

![images/60cb6b60a914169ac79fe4000c20cca5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/60cb6b60a914169ac79fe4000c20cca5.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:30*

Nope. Same pic as before, with machine on. I want to see what the digital display shows. 


---
**Don Kleinschnitz Jr.** *February 28, 2017 13:31*

Is the pot turned up to high? That will override the PWM control?

I guess there is no pot, the power setting then.


---
**Andreas Benestad** *February 28, 2017 13:34*

**+Ray Kholodovsky**​ the machine is actually on in the pic. The display shows 30.0


---
**Andreas Benestad** *February 28, 2017 13:35*

![images/fb4ed684de2b190e912557e7c9ea4b88.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fb4ed684de2b190e912557e7c9ea4b88.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:35*

Oh. I was expecting brighter numbering. Start with that set to 10. 


---
**Andreas Benestad** *February 28, 2017 13:36*

I'm never going to be able to cut anything if that is set to 10..?


---
**Kostas Filosofou** *February 28, 2017 13:38*

**+Andreas Benestad**​ try this one ..Export it as .bmp and play with the settings in LW [thingiverse.com - Laser cutting materiale template by Noloxs](https://www.thingiverse.com/thing:728579)


---
**Andreas Benestad** *February 28, 2017 13:39*

**+Ray Kholodovsky** with it set to 10, there is no  engraving. The minimum is about 13-14. But it still just engraves a solid rectangle.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:41*

Ok. Like you said I don't know if it's a current, a wattage or a percentage being shown on the screen. I assumed mA for which 10 is a good value. 

If you set the digital display at 15, 30, etc different values, and run the same G1 line (S1) can you get different power cuts? 


---
**Andreas Benestad** *February 28, 2017 13:46*

Yes. 👍 


---
**Andreas Benestad** *February 28, 2017 13:51*

**+Ray Kholodovsky**​ here is a test of that test file. As you can see, no difference in shade... I ran this at 17.0 on the digital display.

![images/6eef852b9cf2e430420d7e580e28022e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6eef852b9cf2e430420d7e580e28022e.jpeg)


---
**Anthony Bolgar** *February 28, 2017 13:51*

**+Ray Kholodovsky**, the digital display is showing % of available power. So at 30% it would be approximately 8-10mA


---
**Don Kleinschnitz Jr.** *February 28, 2017 13:53*

Pretty sure its not mA that value is a relative value like we do when a voltmeter is put across the pot. 


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 13:57*

30% is a good value then. 


---
**Anthony Bolgar** *February 28, 2017 14:00*

I have a K40 with an analog mA meter and one with the digital display. I will put a mA meter in series with the tube to ground HV circuit and see how many mA different digital settings gives. That way we will know once and for all,instead of just speculating. :)


---
**Andreas Benestad** *February 28, 2017 14:01*

That would be awesome **+Anthony Bolgar**​.

I am pretty sure I have not been able to cut 3mm mdf with the digital display reading 30... But again thst might be because of something else.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 14:03*

Well unless our resident geniuses come up with something (let's let this thread simmer for a bit) I think we might need to go with replacing the digital display (pwm wire). But that hasn't worked well for the other folks with a digital display. So, don't do anything yet, let's see if any other thoughts chime in. 


---
**Anthony Bolgar** *February 28, 2017 14:05*

At 10mA you should be able to cut 3mm mdf if you slow the speed down, as long as your mirrors and lens are properly aligned, and the coolant water temp is between 15 and 20 degrees celcius. 


---
**Andy Shilling** *February 28, 2017 14:24*

I've got a digital volt meter attached to my pot  but I'm not sure it's the help you want as mine shows 1.2 at around 8mA. 


---
**Andy Shilling** *February 28, 2017 14:39*

**+Andreas Benestad**​ how many wires have you got going into this plug? 



You have disconnected the L from the power plug into the C3D.

![images/6b606b3b29ad51aa9ac9ee889c16a746.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6b606b3b29ad51aa9ac9ee889c16a746.png)


---
**Andy Shilling** *February 28, 2017 14:41*

Here?

![images/68605ca3899d2b3599b1bbc09f498fb6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/68605ca3899d2b3599b1bbc09f498fb6.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 14:41*

He's got L going to the 2.5 terminal. That's good. 


---
**Andy Shilling** *February 28, 2017 14:45*

**+Ray Kholodovsky**​ yes but is there a possibility that if he still has the L connected at the power in on the board as well it might have some confusion. I've only 3 wires going to that plug but even using the 5v it would only make 4. Looks to me as though there are possibly 6.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 14:47*

I see a typical 4 wire plug. The mini doesn't have anything connected at the 5v pin so that's fine. If 24v and Gnd was in the wrong spot we would know (fireworks). 


---
**Andy Shilling** *February 28, 2017 14:50*

No sorry clearly 4 reds and 2 green at the PSU end. I might be barking up the wrong tree but that's still 3 more wires than me from that plug.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 14:54*

Andy you're right. I see 2 green wires that I believe are ground and 5v, coupled with the red wires. Not sure what they are powering. 


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 14:55*

And in the original post here I see a 6 pin connector. D'oh!


---
**Andy Shilling** *February 28, 2017 14:59*

**+Ray Kholodovsky**​ those reds just like to much like the original wires so I was thinking maybe he had left  that on the L and also run a new wire to P2.5. I know you change the config to this pin but what stops the other pin from still firing? The +24 G and L don't seem to have a labelled pin number in the diagram I have.


---
**Don Kleinschnitz Jr.** *February 28, 2017 15:09*

I trouble shot this with someone else on here.

These digital panels have NO connection to the return of the laser tube so they cannot be reading tube mA directly. The only thing that connects to these control boards is the LPS pot connections (and switches). Therefore the controller on that board simply provides a analog value to IN that simulates the pot using the increments switches. I suspect there is a digital pot or the like on that board. They may try and calibrate the display to the actual current but if so it would be very inaccurate.

Get a mA meter :)


---
**Andy Shilling** *February 28, 2017 15:24*

If I remember correctly I think it was Andreas that had a moshi board like mine and I helped him wire it to start with.  I did recommend then to get the 10k pot you recommended Don. Also he had to swap the G and+24 wires round in the plug to suit the C3D.


---
**Allen Russell** *February 28, 2017 18:53*

My supply also has 6 wires, just thought this might help

![images/15f651584990535ad3c1208e13568be3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/15f651584990535ad3c1208e13568be3.jpeg)


---
**Allen Russell** *February 28, 2017 18:56*

Sorry better picture

![images/a47e77c81181975468287ea06f7265b0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a47e77c81181975468287ea06f7265b0.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 18:59*

Your supply is normal, he was referring to the white connector on the other end, and or the number of wires going into the 4 pin block on the right. 


---
**Tony Sobczak** *March 01, 2017 01:02*

Following 


---
**Andreas Benestad** *March 01, 2017 07:28*

I am not entirely sure I understand what your question is, but here are some better pictures of the wires.

As you can see, I have just left the cut off end of L like that, in case I had to go back and reverse the move to 2.5 mosfet.

![images/c5e1549080f4d95721e103cc21a76701.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c5e1549080f4d95721e103cc21a76701.jpeg)


---
**Andreas Benestad** *March 01, 2017 07:29*

Here is the other end going into the PSU. When tracing the green wires it seems like they go to the red dot laser switch and the light switch on the front panel.

![images/ec52878aa374f605ff2bc5da7a0828f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec52878aa374f605ff2bc5da7a0828f4.jpeg)


---
**Andy Shilling** *March 01, 2017 07:32*

That's perfect, it rules out what I was saying. Back to the drawing board 😉


---
**Andreas Benestad** *March 01, 2017 13:12*

**+Andy Shilling** my drawing board is pretty empty... :-)

What could we try next..? I suspect it still is something with the wiring, is that still an possibility **+Ray Kholodovsky**?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 13:17*

Yeah so everything seems in order with the wiring. Maybe backup the config file, format the card, and put the 2.5 config.txt from here on to rule out corruption: 

[dropbox.com - C3D Mini Laser v2.2 Release](https://www.dropbox.com/sh/42l2ps3l53xq79t/AAAP9pLoaG9BDDwyrR3d85Sla?dl=0)



Barring that I guess we have to swap out the digital display with the pwm wire like in Carl's guide. 


---
**Andy Shilling** *March 01, 2017 13:29*

**+Ray Kholodovsky**​ as the pot is just a variable resistor is it possible to by pass the digital display with a couple of standard resistors just to test the different output levels? (Just thinking outside the box)


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 13:30*

Also an option, however it seems his machine is not taking a pwm signal to L, so I would like to send the pwm signal to IN and see what happens.


---
**Andreas Benestad** *March 01, 2017 13:44*

I was advised by another user to disconnect the 5V wire. I have done so, but without any effect. Where could I find IN **+Ray Kholodovsky**?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 13:47*

I have no idea what you are talking about, please clarify and provide pictures if necessary. 

What I need you to do is to unplug the 3 pin jst that goes from the digital display to the psu.  Get the 'pwm cable' that was included with the C3D. run it from the K40 PWM port on the C3D to the PSU.   Change the config to the 'provided config' without the 2.5 mention in the dropbox I linked you. 


---
**Andy Shilling** *March 01, 2017 13:52*

He's disconnected the +5 from the main power plug on the C3D. Like you said yesterday the board doesn't need it so makes no difference.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 13:54*

Gotcha.  Oh and one more thing, when you use the config I said, you need to change the switch.laserfire from 2.6 to 2.5



So the desired outcome is this:

laser pwm pin 2.4!

switch.laserfire 2.5



and once that works try the G1 lines and fiddle with the pwm period as before.


---
**Andreas Benestad** *March 01, 2017 13:56*

The power "connector" originally had 4 wires: L, G, 5V and 24V. L has been moved to the mosfet. The 5V wire is not necessary (I was told). I have unplugged it, but it made no difference.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 13:56*

Yes, Andy just explained above.  It makes no difference. 


---
**Andreas Benestad** *March 01, 2017 14:09*

I think I did everything as you suggested. The laser will not fire..


---
**Andreas Benestad** *March 01, 2017 14:11*

the L is still in mosfet 2.5.

Shouldn't the laser_module_pin be changed from 2.4! to 2.5? (Just guessing here, I have no clue...)


---
**Andy Shilling** *March 01, 2017 14:15*

Can you put another photo up of your wiring now you've changed it.


---
**Andy Shilling** *March 01, 2017 14:18*

Just or of interest did your laser test and  laser arming buttons work ok?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:23*

You'll need to send an M3 

then the G1 line

and an M5 when you are done to disarm.

No, the laser module pin should be 2.4! and the switch.laserfire pin should be set to 2.5


---
**Andreas Benestad** *March 01, 2017 14:25*

This is my wiring at the moment. PWM from the C3D board to the PSU. 

L to mosfet 2.5.

![images/515aa5f7a4ade02277ac283f7009e67e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/515aa5f7a4ade02277ac283f7009e67e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:26*

Yes. I stand by what I said. 


---
**Andreas Benestad** *March 01, 2017 14:27*

**+Ray Kholodovsky**​ the laser would not fire. So I tried changing laser module pin to 2.5 and then it does fire. Still keep it at 2.4!  ?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:28*

How are you trying to fire the laser? 


---
**Andreas Benestad** *March 01, 2017 14:30*

With this setup (2.5 laser module pin and PWM straight from C3d to psu) I believe I have gotten the first result so far with variable power on the same image. 

![images/0a739131faa14ca0f1dc2e8972de55be.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0a739131faa14ca0f1dc2e8972de55be.jpeg)


---
**Andreas Benestad** *March 01, 2017 14:32*

I am firing the laser by running a jobb through Laserweb. 


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:32*

Hahaha awesome. So the pwm isn't actually doing anything then. It's just pulling up to 5v. Hey there's an idea!  **+Don Kleinschnitz** you talked about replacing the pot, how about bypassing it? If Andreas is right here, he's essentially got 5v going to IN and now Pwm to L works :)


---
**Andy Shilling** *March 01, 2017 14:38*

**+Ray Kholodovsky**​ should the explanation mark be removed from the 2.4!  or did I only have to remove that because my laser would fire constantly.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 14:39*

It should stay.


---
**Andreas Benestad** *March 01, 2017 14:41*

Doing some greyscale testing. Not so sure anymore...


---
**Andreas Benestad** *March 01, 2017 14:44*

Yeah, not much greyscale after all... 😕

![images/0721b94716c8de31d68679dc8b37f4cd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0721b94716c8de31d68679dc8b37f4cd.jpeg)


---
**Andy Shilling** *March 01, 2017 14:44*

^^^^^^^^^^?????.


---
**Don Kleinschnitz Jr.** *March 01, 2017 14:51*

Something is really out of wack here:

...............

" With this setup (2.5 laser module pin and PWM straight from C3d to psu)"

.. This is how its supposed to be wired??? Was it connected some other way.

..............

 "So the pwm isn't actually doing anything then. It's just pulling up to 5v."

.. L is pulled up? If it was there would be no image. The fact that there is an image means the PWM is doing something. PWM is the only place there is any digital information to the K40?

..............

"...should the explanation mark be removed from the 2.4! or did I only have to remove that because my laser would fire constantly."

.. If an OD output is being used it should not be inverted. If it is inverted (!) it will fire all the time.

..............

"Hey there's an idea! +Don Kleinschnitz you talked about replacing the pot, how about bypassing it? If Andreas is right here, he's essentially got 5v going to IN and now Pwm to L works :)"

.. You can pull up the "IN" but that will remove all local power control and leave you with no contrast control as the tube wears. 

..................

Something does not make sense to me. 



a. With the pot in and the PWM on pin "L" it did not print an image? 



b. Now with the pot out and "IN" pulled up with PWM on L it images? 



(b) is the same as (a) but with full power.



...If this were true simply turning the pot full on in (a) would have given the same result as (b)?


---
**Andreas Benestad** *March 01, 2017 14:52*

argh! 

Here is a picture of my config right now, in case anyone can make sense out of this mess...



![images/ceb1a9d13aa4c7e43044cb0a8392dc37.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ceb1a9d13aa4c7e43044cb0a8392dc37.jpeg)


---
**Andy Shilling** *March 01, 2017 14:52*

Looks like you need to fit a pot and ditch the digital display, that way you can be wired up like the rest of us. If you go back to the original post when we first spoke about your wiring I posted a link to the type you would need. Also get yourself an ammeter. It'll save you all this hassle.


---
**Andy Shilling** *March 01, 2017 14:56*

**+Don Kleinschnitz**​ there's no way the jst in the back of the digital display could be in backwards is there? Could a +5 be enough to illuminate it and change the display but it's not actually doing anything?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 15:05*

**+Andreas Benestad** you need what I said, with 2.4! and 2.5  

If your laser does not fire, you need to debug that.  I repeat, M3 then G1 line.  

**+Don Kleinschnitz** disregard previous, it was conjecture and now disproven. 


---
**Don Kleinschnitz Jr.** *March 01, 2017 15:06*

**+Andreas Benestad** OK, you should decide is you really want to keep that digital display. 



I say NO because .....

You need an milliamp meter anyway to tell what your tube is doing

The up/down functions are no different that the pot that we all have and its not more beneficial just more complex. 



If you want to keep it we can make it work. I think we just out of sync somehow. 



I will read the thread again and see if I can be helpful.


---
**Don Kleinschnitz Jr.** *March 01, 2017 15:14*

**+Ray Kholodovsky** are you saying that the OD output should be inverted (2.4!). If so, something is still wacky. Are you wired to an OD output or internal to the board. 

If its an OD is should be (2.4)?

Is there a wiring diagram of how this is supposed to be.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 15:20*

This is how I and around 20 of the original beta testers ran their machines. The only difference is that since he cut the L wire it is going to 2.5 instead of 2.6 on the C3D laser power connector. 

The PWM is 2.4! because it is a MOSFET that is then being pulled up to 5v, hence the invert is needed. L in this case is just an arming switch. 



The first batch folks reported subpar performance this way so we moved to pulsing L, but there are still people for whom this is the working configuration.  ![images/7d4ac137d86e378880ae218f5af3fe6a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7d4ac137d86e378880ae218f5af3fe6a.jpeg)


---
**Don Kleinschnitz Jr.** *March 01, 2017 15:21*

**+Andreas Benestad** **+Ray Kholodovsky**   you have 2.5 configured to the PWM pin AND switch configuration in your configuration file? Don't think that is correct?



**+Ray Kholodovsky** Is both 2.4 and 2.5 supposed to be wired and if so to where?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 15:23*

2.4 is the JST cable you see going to the right side of the C3D port and the pot IN area of the PSU. 



L wire needs to go somewhere. He has it at the MOSFET 2.5. Easy. 



Yes, Andreas needs to not change the config I told him which has 2.4! as the laser pwm pin and he is using 2.5 as the switchfire pin for ^^ reason. 


---
**Don Kleinschnitz Jr.** *March 01, 2017 15:27*

**+Ray Kholodovsky** 



"The PWM is 2.4! because it is a MOSFET that is then being pulled up to 5v, hence the invert is needed. L in this case is just an arming switch."

............

Then its NOT an open drain! The pull-up may draw current from the L pin and may/will cause problems. 



AGAIN, the L pin is not a logic signal it is the cathode of a LED. It needs to be gnd or OPEN. Any current in pin L will power the supply at some level. 

............

If L is an arming switch where is the PWM going to and coming from?


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 15:31*

No, 2.4 it is not an open drain. This is how we level shifted to 5v to replace the pot. 



Again we are sending the pwm signal to IN instead of the pot wiper. It is coming from the 2.4 MOSFET output. We've discussed this approach before when you told me how much of a hack it was, Don. 

--

Now L is hooked up straight to a MOSFET. There is no pull up here. When 2.5 is activated, L is connected to ground. As you said. 


---
**Andreas Benestad** *March 03, 2017 09:19*

Success! Finally some greyscale happening. Now I just need to tweak the settings I guess.



Time for a shoutout: massive thanks to **+Bjarke Book**​ (my fellow Norwegian superhero) who went out of his way to help me find a solution. Also thanks to this whole community (special thanks to **+Ray Kholodovsky**​ and **+Peter van der Walt**​ for your kind patience). I am starting to really like the opensource community! 😊

![images/da853add7b63f818a4e2b838dfeb061c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/da853add7b63f818a4e2b838dfeb061c.jpeg)


---
**Andreas Benestad** *March 03, 2017 09:24*

Here is a picture of how the board is wired now. Main difference is that we completely skipped the original power input. L is routed to Mosfet 2.5, 5V is just remove, 24V and GND is routed to their designated inputs in the corner of the board (bottom right corner when looking at pic). This way I can keep PWM going from the digital frontpanel straight to the PSU. 



In config:

Laser module pin 2.5

PWM period  200

-

laserfire.output pin 2.6



(Hoping this might help someone else at some point.👍)



![images/f49d0c575d2b68606d190800b9cccbda.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f49d0c575d2b68606d190800b9cccbda.jpeg)


---
**Andy Shilling** *March 03, 2017 10:12*

Great news buddy.


---
**Don Kleinschnitz Jr.** *March 03, 2017 15:08*

**+Andreas Benestad** it would be great if when you have time, draw a wiring diagram. Hard to tell the detail from a photo.


---
**Allen Russell** *March 05, 2017 19:14*

So is this the setup I want to follow


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 19:16*

Post pics of your setup in your thread here and I will advise you. 


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 19:19*

You don't need to cut wires unless I say so though. The new batch makes it unnecessary to move the L wire. It stays part of the connector. Which is why it's better to just post pics of your machine and I'll let you know. 


---
**Steven Whitecoff** *March 07, 2017 03:38*

Ray,

So much for plug and play....waded thru this thread and it appears having a digital panel is going to keep my Mini sitting in its box. Hell I can't even decipher the pinout for the Y axis from the Mini pinout diagram, not that I can identify which lead does what from the stepper without removing the entire stage to get at it. Always a big disappointment to find instead of using something, one is going to spend entirely too much time learning something not connected with the goal at all.  

Where do we start? New thread or here? 


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 03:42*

Hi Steven, 

A new thread would be great, thanks. 

By my count, we have at least 4 people with the digital panels. In all cases, the current wiring solution is preferred and I have not heard complaints from others once we settled on the current wiring scheme. 



Post some pics of your machine wiring setup and I'll walk you through what to do. 


---
**Anthony Bolgar** *March 07, 2017 04:18*

I have a K40 with t he digital panel as well as one with Pot and mA meter. I was just going to trash the digital panel and set it up like the analog K40. But I am open to setting it up with the digital panel as well. I would love to see some pics of how the digital is set up.


---
**Anthony Bolgar** *March 07, 2017 08:23*

**+Ray Kholodovsky**, you may want to edit the config file in your drop box to change PWM period from 20 to 200 


---
**Don Kleinschnitz Jr.** *March 07, 2017 14:43*

**+Anthony Bolgar** If you use the digital panel you still need as milliamp meter.




---
**Anthony Bolgar** *March 07, 2017 15:01*

I meant putting in a pot and mA meter to replace the digital crap.


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/cMBa5rau5ja) &mdash; content and formatting may not be reliable*
