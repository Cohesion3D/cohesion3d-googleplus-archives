---
layout: post
title: "Hi everyone. I installed an air assist and exhaust fan control via 2.7 fet with solid state relay"
date: November 20, 2018 07:45
category: "C3D Mini Support"
author: "Ali Sureyya Torun"
---
Hi everyone.



I installed an air assist and exhaust fan control via 2.7 fet with solid state relay. Everything works perfect.



Just curious if there is any way to add delay via config file or Lightburn setting just before M107 command. By that way the exhaust fan could work a little bit longer for excess fume extraction.





**"Ali Sureyya Torun"**

---
---
**Anthony Bolgar** *November 20, 2018 10:13*

You can buy a relay that has a delay built in to it. That is the only way I know how to do that.


---
*Imported from [Google+](https://plus.google.com/107651097210973310424/posts/HFG5pUVk9vu) &mdash; content and formatting may not be reliable*
