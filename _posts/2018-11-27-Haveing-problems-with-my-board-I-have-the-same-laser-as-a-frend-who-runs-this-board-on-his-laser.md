---
layout: post
title: "Haveing problems with my board I have the same laser as a frend who runs this board on his laser ."
date: November 27, 2018 02:26
category: "C3D Mini Support"
author: "Richard Groenendyk"
---
Haveing problems with my board 

I have the same laser as a frend who runs this board on his laser . 

When i try to run mine with his seating i have this problem .

If i put my stock board i can run it as fast as it will go without a problem .

I have to run my laser slow to be able to run this board so i dont get this problem and need to no how to fix it 



Thiis what i should be abel to run 

$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=1.000

$36=100.000

$100=158.000

$101=158.000

$102=157.575

$103=157.575

$110=30000.000

$111=30000.000

$112=24000.000

$113=24000.000

$120=2400.000

$121=2400.000

$122=2500.000

$123=2500.000

$130=450.000

$131=300.000

$132=50.000

$133=5000.000

$140=0.600

$141=0.600

$142=0.600

$143=0.600



This is what i am runing

$$

$0=3

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=1.000

$36=100.000

$100=158.000

$101=158.000

$102=157.575

$103=157.575

$110=30000.000

$111=30000.000

$112=24000.000

$113=24000.000

$120=1800.000

$121=1800.000

$122=2500.000

$123=2500.000

$130=450.000

$131=300.000

$132=50.000

$133=5000.000

$140=0.600

$141=0.600

$142=0.600

$143=0.600

ok


**Video content missing for image https://lh3.googleusercontent.com/-I3h9BPTbuLg/W_yrb9ZurII/AAAAAAAAADg/vlMsrH_4d6I2SlJbfHiaY6G_YLfd_O0hgCJoC/s0/received_366835164062035.mp4**
![images/970fab303b44e8954cc37b1f65789dea.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/970fab303b44e8954cc37b1f65789dea.jpeg)



**"Richard Groenendyk"**

---
---
**Richard Groenendyk** *November 27, 2018 02:28*

If you can't tell the steeper is stopping and starting back up and ramming my laser head into the sides of my laser


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 02:39*

The board ships with Smoothie firmware.  If you want to raster fast you should put the GRBL-LPC firmware on the board. 



[cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)



I take it you have done this? 


---
**Richard Groenendyk** *November 27, 2018 18:23*

yes I have




---
**Richard Groenendyk** *November 27, 2018 21:46*

It still dose it with my lower seatings just not as much 




---
**Richard Groenendyk** *November 27, 2018 21:48*

![images/12829ec8356ebe25461c55a98e7fc36b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/12829ec8356ebe25461c55a98e7fc36b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 22:07*

For starters you should install the separate power supply to power the board - the 24v that these machines put out isn't enough and is a major source of problems. 

[cohesion3d.freshdesk.com - K40 Additional 24v Power Supply Upgrade Guide : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000783636-k40-additional-24v-power-supply-upgrade-guide)


---
**Richard Groenendyk** *November 28, 2018 02:03*

I do not have a k40 and the power supply i have is good and the only thing my power supply runs is my board 


---
**Ray Kholodovsky (Cohesion3D)** *November 28, 2018 02:18*

Ok, well in that case you can try configuring GRBL, changing your acceleration settings, etc. until it is working to your liking.  



and you can actually use the Machine Settings Dialog in the latest version of LightBurn to handle all that!


---
**Richard Groenendyk** *November 28, 2018 04:00*

This is a video  of my friends unit same thing as mine.  I can not run over 250. He was the one that gave me the  configuration for mine  and he is runing what i should be abel to. He is runing at 500mms.  I have  dropped mine down quite a bit from his already and i find my stock board faster. This one seems slow.  I can run 400mms on stock and not have this problem  and after import and shipping and cost of board i am over 300 for hafe the quality of board  i had 



Sorry if i seem rude but how would you like me to chage it becase wright now if i drop it down more i  would be better off runing my stock board.   And if  i have a customer print i go back to stock so i no i will not have a problem 

![images/4dd2384cd7f0e974e6536d01a0e21481](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4dd2384cd7f0e974e6536d01a0e21481)


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2018 01:46*

What I'm still trying to understand is are the motors jamming and causing a buzzing sound?  Or what? 



If it's that, and you're confident you have a sufficient enough 24v power supply, you might want to increase the current on the stepper drivers by (with all power off, and preferably with a non-conductive screwdriver) turning the trim potentiometers on the drivers clockwise 45-90 degrees at most, so that the flat ends up facing to the right.  This will give the motors more current. 



Again, I'm guessing here because I don't fully understand the issue from your description, but if it's a motion/ max speed related issue then some combination of turning up the current on the drivers and playing with your acceleration settings in the config would be the way to solve it. 


---
**Richard Groenendyk** *December 04, 2018 04:44*

I figured it out  but it would just stop no buzzing  just stop for a second  and start back up and i did all that before i came on hear thanks for help i just needed to walk away for a bit and did a few things and works fine now 


---
*Imported from [Google+](https://plus.google.com/100358955632658580927/posts/PggdEbpQnRm) &mdash; content and formatting may not be reliable*
