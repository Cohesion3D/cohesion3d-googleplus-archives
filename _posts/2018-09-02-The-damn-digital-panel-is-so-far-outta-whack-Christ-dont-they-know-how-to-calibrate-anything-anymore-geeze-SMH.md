---
layout: post
title: "The damn digital panel is so far outta whack Christ don't they know how to calibrate anything anymore geeze SMH"
date: September 02, 2018 20:54
category: "K40 and other Lasers"
author: "Tony Mondello"
---
The damn digital panel is so far outta whack  Christ don't they know how to calibrate anything anymore geeze SMH





**"Tony Mondello"**

---
---
**Tech Bravo (Tech BravoTN)** *September 02, 2018 21:04*

Who said the max output was 18mA to begin with? The purpose of the meter and pot is to set the hard limit and remove the digital % which, as you said, is useless. The power supply will provide way more that 18mA so you are giving it 80% of the full output of the supply. Once you do the analog thing there will be no more worries lol. Oh also, the c3d mini comes defaulted to only output 80% of whatever hard limit is set with the controls for the power supply.


---
*Imported from [Google+](https://plus.google.com/104298619252152309699/posts/4cAy1srdenR) &mdash; content and formatting may not be reliable*
