---
layout: post
title: "If I manually move the laser head with the laser powered down and the USB lead unplugged it seems to be powering up the Cohesion3D board somehow"
date: April 21, 2017 16:21
category: "K40 and other Lasers"
author: "Phillip Meyer"
---
If I manually move the laser head with the laser powered down and the USB lead unplugged it seems to be powering up the Cohesion3D board somehow. I only discovered this because the screen starts to beep. I'm guessing that the motors that usually move the laser head are acting instead as generators and passing power back to the Cohesion3D board but that doesn't sound to healthy.

P.S. I'm only moving the head manually to be able to replace parts on it.





**"Phillip Meyer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 21, 2017 16:24*

That's exactly it, you'll find the same behavior on just about any other stepper or motor driver board out there.  Which is why all those manufacturers say not to move the motors/ head by hand.


---
**Phillip Meyer** *April 21, 2017 16:56*

Thanks, makes sense!


---
**Joe Alexander** *April 21, 2017 17:00*

yea steppers generate a back current when moved by hand. I unplug mine from my controller when I do beam calibration so I can move by hand without blowing something.(maybe that's why my first stepper driver and 2.4 MOSFET stopped working...)


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/SoEP7cNiBg2) &mdash; content and formatting may not be reliable*
