---
layout: post
title: "Cant control laser power with LightBurn. I just got my old K40 converted to the cohesion board and LightBurn software"
date: August 12, 2018 02:09
category: "K40 and other Lasers"
author: "Trevor Hinze"
---
Cant control laser power with LightBurn.



I just got my old K40 converted to the cohesion board and LightBurn software.  The members here were very helpful, and instrumental in helping me figure out the correct connections.  Now I am hoping someone will know how to fix my next problem. My laser is old enough that it has an old potentiomer on top that is used to adjust the output of the laser tube.  I am not able to vary the laser output power using software (LightBurn) No matter what power setting I assign to a layer in the software it only cuts at the rate that is set by the know on the control panel of my laser. I was curious how power was going to be affected since none of the wires that I had to hook up had anything to do with the pot but I was optimistic that the software was somehow using switching of the laser on pin to approximate a given power level but that does not seem to be the case.  I know that the newer lasers have a digital control panel on the front that is hooked to the cohesion with a thin ribbon cable but I do not have that option.  I really really want to vary the power output by layer.  Any ideas are greatly appreciated!!!





**"Trevor Hinze"**

---
---
**LightBurn Software** *August 12, 2018 02:23*

My K40 also has the potentiometer, and the C3D varies the output power just fine, via PWM of the L connection on the power supply.


---
**Trevor Hinze** *August 12, 2018 03:02*

**+LightBurn Software** Thanks for the reply, I will need to do some more playing, at present the lines all just cut at the amperage determined by the pot on the front.  I tried everything from 5pct to 100pct and I still got the same power output as monitored by the analog Ma gauge on the front panel..  I will keep after it though as variable output is the main driving force behind the conversion....BTW the software is awesome, I am only on day one of my trial but I will be purchasing it well before my 30 is up!


---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2018 13:07*

Please follow the PWM tuning guide located at the docs site —> troubleshooting/ FAQ article. 


---
**Trevor Hinze** *August 12, 2018 14:09*

**+Ray Kholodovsky** thanks so much!  I changed the file as directed in the guide and now I have variable control!  I used the suggested interval of 400 for a pwm timing number and it worked!  These are 5,20,40,60,80, and 100 pct power cuts.  Thanks so much. The board and the software are both amazing!

![images/2118d0776e491291a89c1e528a777aa5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2118d0776e491291a89c1e528a777aa5.jpeg)


---
**Mario Gayoso** *August 21, 2018 05:02*

**+Ray Kholodovsky** mine doesn't cut thru plywood or I need the make 8 passes for 4mm basswood at 20 speed/100 power. Any suggestions?


---
**Trevor Hinze** *August 21, 2018 05:17*

Slow down, 20mm /sec seems pretty fast for cutting wood, others will chime in with their formula kit at 10ma current I would prob try 4 or 5mm/sec at the most. 


---
**Mario Gayoso** *August 21, 2018 15:33*

**+Trevor Hinze** That sounds like great advice, at what power in percentage. I dont have an Amperage indicator




---
*Imported from [Google+](https://plus.google.com/106981345031249926577/posts/DjBWMpYwoAZ) &mdash; content and formatting may not be reliable*
