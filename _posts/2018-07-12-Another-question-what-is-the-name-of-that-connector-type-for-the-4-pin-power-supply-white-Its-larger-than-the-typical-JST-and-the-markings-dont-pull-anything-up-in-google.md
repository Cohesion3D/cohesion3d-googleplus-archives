---
layout: post
title: "Another question, what is the name of that connector type for the 4 pin power supply (white) It's larger than the typical JST and the markings don't pull anything up in google :/"
date: July 12, 2018 21:20
category: "C3D Mini Support"
author: "julian contain"
---
Another question, what is the name of that connector type for the 4 pin power supply (white)  It's larger than the typical JST and the markings don't pull anything up in google :/





**"julian contain"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2018 22:07*

VH3.96



However you can connect 24v, Gnd, and L to the screw terminals on the board as shown in the last pic of the install guide. 



Did you get my email response yesterday?  I want to ship your order but want to confirm with you first in the event you want to add anything else to it. 


---
**julian contain** *July 12, 2018 22:35*

Thanks.  Go ahead and ship that bad boy.  I'm heading out of town for a month soon anyway and want to get back up and running asap.


---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2018 22:50*

Do you need....power supply, additional drivers, anything like that? 


---
**julian contain** *July 13, 2018 08:48*

Nah, I think I should be good for now


---
*Imported from [Google+](https://plus.google.com/116932289299679953939/posts/byYJAu87vNR) &mdash; content and formatting may not be reliable*
