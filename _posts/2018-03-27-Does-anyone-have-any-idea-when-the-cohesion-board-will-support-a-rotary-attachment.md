---
layout: post
title: "Does anyone have any idea when the cohesion board will support a rotary attachment?"
date: March 27, 2018 20:10
category: "General Discussion"
author: "Angela Newingham"
---
Does anyone have any idea when the cohesion board will support a rotary attachment?





**"Angela Newingham"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 27, 2018 20:14*

Already? 



[https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**Anthony Bolgar** *March 27, 2018 20:42*

Lightburn 0.6.05 now supports rotary use. Will be released very soon, is in beta right now.


---
**Jim Fong** *March 27, 2018 22:02*

It has supported rotary for about a year now using Laserweb4.  



I did some rotary beta testing using LightBurn on Sunday.  Works nice.


---
**Angela Newingham** *March 28, 2018 11:33*

Thanks everyone.  I was informed it did not support rotary yet.


---
**Angela Newingham** *March 28, 2018 11:38*

**+Anthony Bolgar** thanks for posted link,  do you happen to have a link for the extra 24v 5-10 power supply that will work with all this. i plan on ordering everything together soon


---
**Anthony Bolgar** *March 28, 2018 13:37*

**+Ray Kholodovsky** do you have a recommended 24V PSU for the upgrade?




---
**Ray Kholodovsky (Cohesion3D)** *March 30, 2018 19:43*

Just a 24v 6a "LED STYLE" one off ebay will be fine until I come up with something better. 


---
**Angela Newingham** *April 07, 2018 22:25*

does anyone have a link for a rotary that will work with this set up.. i dont want to waste money  on the mini upgrade unless i can get a rotary also . 

thanks everyone 


---
*Imported from [Google+](https://plus.google.com/101357182958712120601/posts/EVgdbKsjRFo) &mdash; content and formatting may not be reliable*
