---
layout: post
title: "Installed Cohesion3D in my K40, install Lightburn 7.04 on my computer running Win7 pro with the original USB cable that came with the k40, but part way through the engraving an image it stopped, so I contacted Lightburn"
date: September 18, 2018 00:34
category: "C3D Mini Support"
author: "adstaggs"
---
Installed Cohesion3D in my K40, install Lightburn 7.04 on my computer running Win7 pro with the original USB cable that came with the k40, but part way through the engraving an image  it stopped, so I contacted Lightburn support and they recommended  to me to upgrade to 8.03 and change the USB cable which I did but still stop, even tried another image but does the same. I went back to stock board and everything works fine.





**"adstaggs"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 18, 2018 00:39*

Please provide pictures of the board, wiring, and your installation.


---
**adstaggs** *September 18, 2018 12:37*

![images/f92ca40e93707dcd1b75e5cc61d0c391.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f92ca40e93707dcd1b75e5cc61d0c391.jpeg)


---
**adstaggs** *September 18, 2018 12:37*

![images/76d0225a9d27355e143bd9f67ce653de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/76d0225a9d27355e143bd9f67ce653de.jpeg)


---
**Kelly Burns** *September 19, 2018 00:20*

Run the same design with the laser disabled.  If it finishes, I would suspect you have a ground or bleed over issue with the laser part of the power supply.  


---
**Ray Kholodovsky (Cohesion3D)** *September 19, 2018 00:33*

The main recommendations are to insulate the board from the frame of the laser (nylon standoffs, 3D Printed plate, or use a acrylic plate) and then to use a separate power supply to power the board such as the one on our webstore.  


---
*Imported from [Google+](https://plus.google.com/116460242075106049086/posts/LAqPdBScJ6K) &mdash; content and formatting may not be reliable*
