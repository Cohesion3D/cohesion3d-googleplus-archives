---
layout: post
title: "Hello my friends. My Chinese laser uses the nano m2 plate as pictured"
date: August 19, 2018 22:25
category: "C3D Mini Support"
author: "Rafael Santos"
---
Hello my friends. My Chinese laser uses the nano m2 plate as pictured. Is the upgrade plug and play? my machine is still under warranty. I noticed that the mini CD3 card is 24v and my card is 36v.

![images/4ad48b1c727b43a2fea87dbb1cb889f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4ad48b1c727b43a2fea87dbb1cb889f4.jpeg)



**"Rafael Santos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 20, 2018 15:05*

Ok, this is very interesting!  The board says M2Nano but it is very different than one I have seen before. 



You are correct that the C3D Mini can only take 24v in, but it also appears as if you have external drivers. 



Please consult this guide: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



You should see black boxes like those in your machine (follow the red/ blue/ white wires).  Please take pictures of them for me. 



It is certainly possible to rewire but will take more work and require some additional components. 


---
**Rafael Santos** *August 20, 2018 19:45*

**+Ray Kholodovsky** 

![images/43bc7ebbb3590603f37fbc0d92c29b89.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/43bc7ebbb3590603f37fbc0d92c29b89.jpeg)


---
**Rafael Santos** *August 20, 2018 19:46*

**+Rafael Santos** 

![images/b0206b04ceea1b7b46b120ee0cea6c48.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0206b04ceea1b7b46b120ee0cea6c48.jpeg)


---
**Rafael Santos** *August 20, 2018 19:46*

**+Ray Kholodovsky** 

![images/bffe87b9ed9ec575d7f11df5ef76afca.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bffe87b9ed9ec575d7f11df5ef76afca.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 15:59*

So it will be a different process and will require some more work for you. 



I think you will need the laser bundle and a set of external stepper adapters in addition to whatever else you may want (perhaps GLCD Adapter + Screen)



You should not plug the 36v into the Mini, and will need a different way to power the board - our "power supply upgrade kit" contains a 24v power brick that will do this nicely. 



Again, consult that and the other articles at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) to get an idea of what you need to do and figure out if you're comfortable with doing it and figuring it all out. 


---
*Imported from [Google+](https://plus.google.com/112495069141704136610/posts/D1ivaimGcoA) &mdash; content and formatting may not be reliable*
