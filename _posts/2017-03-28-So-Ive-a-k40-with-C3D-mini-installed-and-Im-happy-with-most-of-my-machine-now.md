---
layout: post
title: "So I've a k40 with C3D mini installed and I'm happy with most of my machine now"
date: March 28, 2017 22:50
category: "C3D Mini Support"
author: "oliver jackson"
---
So I've a k40 with C3D mini installed and I'm happy with most of my machine now. I'm working on reducing the noise as much as possible. Silent step sticks, motors and fans on anti vibration mounts. But the main source of noise is PWM, my machine whines like crazy now I'm not using the original digital control panel.



Any thoughts on inserting an LPF between the PWM out and PSU laser level in? Also is there an up to date schematic of the mini? What I find on GitHub doesn't match my board at all





**"oliver jackson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 22:56*

Hey Oliver, 

Just want to confirm that you're pulsing L and that the pot is still connected to the PSU, as per our latest instructions. 

Other than that, should be worth a shot. 

There's a dropbox link with the latest mini floating around somewhere, but yeah you're right the Github is a bit behind and I'll get that caught up. 


---
**Don Kleinschnitz Jr.** *March 29, 2017 00:09*

**+oliver jackson**​​ can you describe or post video of the whine?

How do you know it's the pwm? 

LPF?


---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2017 00:27*

Low pass filter. Don, I defer to you for advise as always. 


---
**oliver jackson** *March 29, 2017 02:12*

It's late here so I will double check the wiring in the morning but I'm pretty certain I've got the PWM out to the level in and the trigger is coming through the 4 terminal connector which has 24v on it using the same wiring as the m2 nano had. The whine is only when firing the laser, tested with no gantry movement. I plan to do some debugging tomorrow, hook up a pot instead and see what happens. But I was thinking it was the PWM only because it's the main part that is different to before and it's operating at a clock rate in range of human hearing.



I wanted to check out the schematic to see the cutoff frequency on the PWM out. It's basically a DAC and normally you would place a low pass filter with a cutoff related to the clock frequency on the output. But the stock clock frequency of smoothies PWM is higher. I think the config you send out has a clock of 50Hz off the top of my head, so there's a band of the spectrum above this where we have no useful signal. Anything there will just be noise and can be filtered out. Any reduction in noise should make a more stable system.










---
**Don Kleinschnitz Jr.** *March 29, 2017 03:29*

**+oliver jackson** I would not reccomend a filter in the PWM, also we have never found it to be necesary. 



The PWM from the smoothie (if that is what you mean) is not a DAC it is a digital pulse whose pulse width is proportional to the the DF or % of power expected, all digital, no analog. 



The PWM period should be set to 200-400us.

I do not see your point about the frequency spectrum of the PWM that is useless? The PWM is a square wave and needs as fast as possible rise and fall times.

I also do not understand what miight be  "unstable".



Do you have it connected directly to IN or on the L pin. It should not be on IN, rather on L?

Can you provide a circuit diagram of the PWM from the smoothie to the LPS, that can help my understanding?



For electroincs to make an audible sound something must be vibrating.

Two things come to mind that can be related to driving the LPS with the PWM control and maybe create a secondary sound:

1. the pwm in to the LPS is concatinated with the LPS internal PWM and switcher. For some reason the flyback could be making noise when driven due to an internal problem but is useually does not and should not.

2. arching can itself make a whine and it also can cause the overloaded flyback to whine. Both not good. 

...

What coolant are you using. Sounds like a strange question but we have found conductive coolant to create arching and poor laser and LPS behavour, even failure. The arching has been described as a screech or a whine.



Hope this helps.


---
**oliver jackson** *March 29, 2017 13:11*

sorry, I really shouldn't write posts when I am tired late at night.  Turns out smoothies default PWM frequency is the same with hotends - I just read it was faster somewhere else and didnt actually check.



After a PWM out it is common to have a reconstruction filter with a cutoff at nyquist to eliminate aliasing and any other sources of noise outside of our operating bandwidth.  Had smoothies default PWM clock actually been faster, then the nyquist frequency and so filter cutoff would have been higher.



I agree PWM is not a DAC, nor is it an analog voltage.  Its a digital representation of an analog voltage.  If I want 2.5V from a 5V microcontroller I sned out a 50% duty square, if I want 1.25V I set 25% duty etc.  But its not actually the desired 2.5V without averaging.



Coolant is distilled water with no additives.  Bought in January this year, was produced in December.  Stuck a multimeter in with probes either end of the tank, resistance was above 1.2M.  What other arcing checks can I do?


---
**oliver jackson** *March 29, 2017 13:22*

Ok so I've a PSU marked MYJG40W. I worked out the pins to be:

1. Laser negative 

2. Chassis ground

3. Ac in

4. Ac in

5. Ground

6. Protect switch

7. Low TTL fire

8. Ground 

9. Power control 0-5V

10. 5V

11. 24v

12. Ground 

13. 5V

14. Low TTL fire



So pins 5 and 6 go to a panel "laser enable" switch on the front panel. Nothing physically connected to pin 7. But I've checked continuity between pins 7 and 14, where laser fire is actually connected. C3D PWM out connected to pin 9. Will do some recordings in next few mins of with PWM and then with potentiometer connected instead

![images/53e0c86943965ddfb2d35e6cbc3d32f8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/53e0c86943965ddfb2d35e6cbc3d32f8.jpeg)


---
**Don Kleinschnitz Jr.** *March 29, 2017 13:57*

**+oliver jackson**

Check out this blog most of this information is available in one post or another: [donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



You are connecting the PWM from the smoothie to IN. IN expects an analog voltage. Its important to know where you are getting PWM on the C3D from, some sources are 3VDC some are open drain.



**+Ray Kholodovsky** what kind of output is C3D PWM as stated above. Is that level shifted or open drain?



I'm loosing track how to help though. You are trying to reduce audible noise and I am not sure what the LPS wiring has to do with that? If the machine is working I am reasonably certain that PWM is not the cause of audible noise? 


---
**oliver jackson** *March 29, 2017 14:22*

Provided the above as both you and ray wanted to double check I had things wired right. My supply doesn't match what is in his guide so I provided a pic and pinout to eliminate confusion.



I've looked at the C3D mini PWM out on a scope now. It looks analog. Last time I did this was on an arduino uno. That looks blocky without at the very least a 2 pole LPF reconstruction filter on the output. But then we're comparing 8 bit and 32 bit. Didn't consider that at all when I was thinking about this yesterday.



There is definitely a change in audible noise from the PSU between an analog laser level from a voltage divider and a PWM generated laser level. Does the machine work perfectly fine? Yeah. Im just being fussy. Between what you've said and my own investigations adding a filter will do nothing significant. 



I'm just accepting things as they are. I work in audio, so I'm pretty tuned in to audio frequencies. Other people may have barely noticed. Thanks for the help anyway


---
**Ray Kholodovsky (Cohesion3D)** *March 30, 2017 04:10*

"Just want to confirm that you're pulsing L and that the pot is still connected to the PSU, as per our latest instructions. "



Based on your above description it sounds like you have the pot disconnected, and the pwm cable connected to the C3D.  This is no longer recommended.  Please see our latest instructions.  

Was your board the first or the 2nd batch?  If first batch (v2.2 written on back of board) then need to run a wire from L to 2.5 - bed terminal and change pwm pin in config from 2.4! to 2.5






---
**oliver jackson** *March 30, 2017 11:56*

board is rev 2.3.  You mean the instructions pinned at the top of here, last modified 21st March?  Those instructions actually dont say leave the pot connected, nor do they discuss the included PWM cable.  I hooked it up because it was included in my delivery.  



Please be clear, how do you recommend to hook up?  Are you saying I should be hooking up PWM to fire and reconnect the original front panel control to the PSU (I had a digital panel).  When you said pulsing I read it as "logic on/off" not PWM


---
**oliver jackson** *March 30, 2017 12:00*

I have one of these

![images/7ca2254bc730ecc111e5643a2d12325f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7ca2254bc730ecc111e5643a2d12325f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 01, 2017 04:26*

Hey, that's a great catch!  I thought I had a specific notation in there along the lines of "we include this pwm cable in case your pot is bad or your laser is acting weird, but by default don't use it" but I can't find this text in the guide.

By default keep the pot/ your digital control panel connected.  Just wire the motors/ ribbon/ endstops/ power cable but not replace the pot.  So set that pwm cable that came included aside. 



Also here's the dropbox link for the board eagles.  I haven't had time to deal with Github to update that.  Different repos, etc... [https://www.dropbox.com/sh/z69pps4bkr04ken/AACz2QzB9jaMkkMLR_4QOTc0a?dl=0](https://www.dropbox.com/sh/z69pps4bkr04ken/AACz2QzB9jaMkkMLR_4QOTc0a?dl=0)


---
*Imported from [Google+](https://plus.google.com/117912755306057047954/posts/eUhojdLqKX2) &mdash; content and formatting may not be reliable*
