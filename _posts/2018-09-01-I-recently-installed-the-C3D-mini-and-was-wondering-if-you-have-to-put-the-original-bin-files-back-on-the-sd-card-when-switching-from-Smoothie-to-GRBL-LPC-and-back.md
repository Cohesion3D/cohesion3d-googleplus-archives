---
layout: post
title: "I recently installed the C3D mini and was wondering if you have to put the original bin files back on the sd card when switching from Smoothie to GRBL-LPC and back?"
date: September 01, 2018 02:32
category: "C3D Mini Support"
author: "James L Phillips"
---
I recently installed the C3D mini and was wondering if you have to put the original bin files back on the sd card when switching from Smoothie to GRBL-LPC and back? Windows 7 laptop.





**"James L Phillips"**

---
---
**Tech Bravo (Tech BravoTN)** *September 01, 2018 02:41*

You only need to write the grbl file to the sd card (after a fat format) and power up the laser. There is no config file for grbl. To switch back to smoo then the firmware and config files need to be present.


---
**James L Phillips** *September 01, 2018 02:49*

Thanks


---
**James L Phillips** *September 01, 2018 02:56*

Could I use another sd card with the file on there and just switch any time without a reformat? Does it have to be the .bin or will the .cur file work?


---
**Tech Bravo (Tech BravoTN)** *September 01, 2018 03:01*

I dont see why not. The bin file needs to be used i think. When you power up the controller it modifies the file and changes the extension to cur. I think this needs to be done each time. I suppose you could try the resulting cur file to see if it works.


---
**Ray Kholodovsky (Cohesion3D)** *September 01, 2018 10:53*

You have to use the bin file. This would make multiple cards less useful, I would think. 


---
**James L Phillips** *September 01, 2018 13:36*

Thanks Ray and Tech Bravo.


---
*Imported from [Google+](https://plus.google.com/100626830592271005238/posts/7XoGMmoQRyN) &mdash; content and formatting may not be reliable*
