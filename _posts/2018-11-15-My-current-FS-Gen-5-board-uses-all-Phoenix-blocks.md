---
layout: post
title: "My current FS Gen 5 board uses all Phoenix blocks"
date: November 15, 2018 23:17
category: "K40 and other Lasers"
author: "Em W"
---
My current FS Gen 5 board uses all Phoenix blocks. This question is in regard to the blue power block wires/connector. 



I purchased the JST connector pack from C3d so have the stepper motor connectors covered and anything that uses that size from the cohesion site. 



HOWEVER, the blue wire power block connector looks larger in the image I provided.



Please, if someone can tell me specifically what the connector type is called and size if needed, so I can purchase one in order to move my current power wires to plug into the C3d mini. And anyone have a good link to where to purchase one. Looks like I only need one of that size?



Maybe it is the same size that is used for the steppers and just can't tell from the image? \Llooks larger...



Thank you in advance!



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145702/original/r97QnKaaq_yir7vYIJYWYjNiM0TE-8xnTg.jpg?1489518421](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145702/original/r97QnKaaq_yir7vYIJYWYjNiM0TE-8xnTg.jpg?1489518421)





**"Em W"**

---
---
**Tech Bravo (Tech BravoTN)** *November 15, 2018 23:40*

Technically you dont need to terminate the wiring for the screw connectors. They are made for bare stranded wire. You could solder the ends of the wires if you want i suppose


---
**Joe Alexander** *November 16, 2018 02:22*

soldered ends can change shape as they heat from the current passing through them over time so it is not recommended. If anything you can use a ferrule crimped on the end, or just go with bare wire.


---
**Ray Kholodovsky (Cohesion3D)** *November 16, 2018 04:18*

The 4 pin block with blue wires shown in the C3D instructions is the k40’s power connector. I don’t know if you have one of these or not. You don’t need to make one. 



There are screw terminals for power and laser fire on the C3D Mini. 



When the time comes I will take a look at your laser psu and other wiring and advise you. 


---
**Ray Kholodovsky (Cohesion3D)** *November 16, 2018 04:18*

And are you taking lots of pictures like I asked? :) 


---
**Em W** *November 16, 2018 05:11*

**+Ray Kholodovsky** Yes and thank you for the reply.


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/M7FtvXaTXow) &mdash; content and formatting may not be reliable*
