---
layout: post
title: "Hello Everyone. Can someone direct or confirm that the pins for the 24v, ground, 5vLaser, and ground are the same on the Laser board as on the Mini?"
date: January 06, 2019 19:45
category: "K40 and other Lasers"
author: "Just Josh rfpom"
---
Hello Everyone. 

Can someone direct or confirm that the pins for the 24v, ground, 5vLaser, and ground are the same on the Laser board as on the Mini? I am retrofitting an ancient MS10105 V4.6 board with the new Cohesion3d LaserBoard. I know there is not much documentation at the time and it is marketed as a direct replacement for the M2 Nano so not much need for documentation right? :-) At any rate if someone could confirm which pins on the LaserBoard get the 24v, ground, 5VLaser and ground I would be very appreciative! I already have the steppers, endstops and homing setup so no problems there. Everything jogs and runs well. One last thing is it supposed to have a hissing sound coming from the steppers when idle and locked? Kind of sounds like an air leak. Goes away if I trigger an endstop. Just curious if that is normal. Sound goes away when jogging and x axis is much louder than y axis. My guess is noise from unshielded wires running the steppers but I am curious if anyone else has this.



Thanks ahead of time for the help. 





**"Just Josh rfpom"**

---
---
**Just Josh rfpom** *January 06, 2019 19:55*

Ahh never mind I didn't notice that the pins were called out on the back of the board. LOL


---
**Just Josh rfpom** *January 06, 2019 20:52*

Got it working, and it is sooo nice not having to deal with Moshisoft! Thanks for making such an awesome product!


---
**Just Josh rfpom** *January 13, 2019 03:05*

I made a quick how to video on how I wired up the MS10105 board. Maybe it will help someone out 
{% include youtubePlayer.html id="iB6wjvk6vRc" %}
[youtube.com - How to Replace the MS10105 Board with a Cohesion3D Laserboard - K40 Upgrade (Fixed Audio)](https://youtu.be/iB6wjvk6vRc)


---
*Imported from [Google+](https://plus.google.com/117855393701125080947/posts/VphvfUAmamY) &mdash; content and formatting may not be reliable*
