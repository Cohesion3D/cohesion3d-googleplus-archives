---
layout: post
title: "Who is running an aux power supply with their k40?"
date: April 08, 2017 01:02
category: "K40 and other Lasers"
author: "tyler hallberg"
---
Who is running an aux power supply with their k40? I am thinking it is not getting enough power to run everything and dont want to over work the factory PSU. Thinking the factory one for the laser and like a computer PSU to run the controller and the motors?





**"tyler hallberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 01:08*

Computer ATX PSU is 12v, Laser puts out 24v.   You want the 24v. So an LED style 24v PSU off ebay is the go to option. 


---
**tyler hallberg** *April 08, 2017 01:23*

something like this?

 

[ebay.com - Details about  AC 110-220V TO DC 24V 12V 5V Switch Power Supply Driver Adapter For LED Strip](http://www.ebay.com/itm/AC-110-220V-TO-DC-24V-12V-5V-Switch-Power-Supply-Driver-Adapter-For-LED-Strip/332167576696?_trksid=p2045573.c100505.m3226&_trkparms=aid%3D555014%26algo%3DPL.DEFAULT%26ao%3D1%26asc%3D43252%26meid%3Daaec96883fae451b8006d2cefad50ec8%26pid%3D100505%26rk%3D1%26rkt%3D1%26)


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 01:25*

Like that yeah, but you don't need 10 amps. I think 24v 5 amps would be enough.  Your call, overkill can't hurt. 


---
**Griffin Paquette** *April 08, 2017 10:06*

Go with the 10A. Overhead current can never really hurt in this type of situation. 


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/FWeRnDBprZt) &mdash; content and formatting may not be reliable*
