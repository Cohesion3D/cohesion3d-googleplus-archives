---
layout: post
title: "hello, i've just received my laserboard and I've installed it"
date: December 21, 2018 23:17
category: "General Discussion"
author: "Darryl Vogler"
---
hello, i've just received my laserboard and I've installed it.



I'm not sure if the laser i have is actually a k40 or some variant? It had an M2 nano board with a single connector for Y-axis, a ribbon cable, and the 4 wire power supply cable. 



here's my issues:

-i have smoothie installed (but i'd prefer gbrl)

-the firmware SD card has been formatted to FAT, I put firmware.bin and config.txt in it but it keeps changing to .CUR

-I have lightburn configured to use with smoothie

-My laser goes crazy and starts bumping against the Y gantry if I allow it to home on startup 

-the home button doesn't seem to do anything

-I had no control over the motors until i set endstops to "disable"

-X and Y controls are backwards (left arrow moves the gantry to the right, etc.)

-lightburn does not fire the laser



thank you very much for your time





![images/49e970f9061d1e9b53c2d45600cfd3ef.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/49e970f9061d1e9b53c2d45600cfd3ef.jpeg)
![images/3ef69912f82db25ecc35127308ed0b2a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3ef69912f82db25ecc35127308ed0b2a.jpeg)

**"Darryl Vogler"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 00:32*

Got a pic of the original board? 



The wiring is normal and covered in the instructions. 



The machine is not a k40, but a larger one? 



Let’s get you squared away with Smoothie first. I need a little bit more time to finalize the grbl build for this board. 


---
**Darryl Vogler** *December 22, 2018 00:39*

**+Ray Kholodovsky** 

![images/6e42437d662412573e41014d7cb02ae0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e42437d662412573e41014d7cb02ae0.jpeg)


---
**Darryl Vogler** *December 22, 2018 00:42*

**+Ray Kholodovsky** here is a better photo of the board by itself

![images/358c368879b164c6337e36f818ee7035.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/358c368879b164c6337e36f818ee7035.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 00:46*

When you connect in LightBurn the head should move to the rear left. Your homing switches are in the rear left, right? 


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 00:47*

You talk about putting files on the memory card...

It came with the files you need already on the card, and I don’t believe those files for LaserBoard are published anywhere yet. So what did you do? 


---
**Darryl Vogler** *December 22, 2018 00:50*

**+Ray Kholodovsky** uh oh...well, when lightburn asked me whether my device was GRBL or Smoothie, I read into the difference and found the link for the GRBL firmware ([https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini))  and then flashed the card on my PC with that .bin. when it didn't work, I re-flashed the SD card with the latest .bin from the smoothie website...have I screwed myself?

[cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)


---
**Darryl Vogler** *December 22, 2018 00:51*

**+Ray Kholodovsky** when I start lightburn the head does not move at all.


---
**Darryl Vogler** *December 22, 2018 00:55*

Yes the switches are in the rear left


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 00:57*

That grbl is for the C3D Mini. LaserBoard has other requirements. I’ll have to get you the smoothie files for LaserBoard in a bit. 

When it flashes the firmware file changes from bin to cur. That’s good. 

When homing the head should move to rear left. 

You can use M119 command in LightBurn console to test the status of the endstop switches being read. 


---
**Darryl Vogler** *December 22, 2018 01:13*

**+Ray Kholodovsky** It would be great if you could get me those. I thought I had backed up the original SD files, but I think I may have accidentally overwritten them during my "troubleshooting myself in the foot" process.



as of right now, the smoothie website .bin allows me to have Lightburn acknowledge my laser with "Ready", but unless I disable endstop in the config.txt file, any move commands are unresponsive. When I do disable it, it seems only the X axis jog commands are inverted, and Y jog operates normally. Home command is unresponsive. 


---
**Darryl Vogler** *December 22, 2018 01:14*

I should add that M119 just gives me "ok ok"


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 19:46*

Here are the stock smoothie files for laserboard.  



[dropbox.com - laserboard stock files smoothie batch 1](https://www.dropbox.com/sh/a7q9fzwc8x9xoje/AACXAvIufHBeUzVT3XSodbZRa?dl=0)


---
**Darryl Vogler** *December 22, 2018 20:13*

**+Ray Kholodovsky** thank you so much for your quick help. I'll install those right when I get home and update you. Cheers again 


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2018 20:18*

The rest will have to wait until after the weekend.


---
**Darryl Vogler** *December 22, 2018 21:24*

**+Ray Kholodovsky** OK, thanks. I'm still having issues with the laser not firing with the new firmware. Have a great weekend!


---
*Imported from [Google+](https://plus.google.com/104508192203924927953/posts/9K5qLYvzBDJ) &mdash; content and formatting may not be reliable*
