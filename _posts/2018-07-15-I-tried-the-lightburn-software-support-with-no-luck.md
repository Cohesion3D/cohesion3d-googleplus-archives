---
layout: post
title: "I tried the lightburn software support with no luck"
date: July 15, 2018 02:08
category: "C3D Mini Support"
author: "Lashley Guyton"
---
I tried the lightburn software support with no luck. Wondering if maybe it’s a board issue. Trying to get the k40 to mimic the computer as far as laser position. In the video I am pointing the laser to 0,0 but you can see where it goes. 



K40

C3D mini

Lightburn software



No other mods. 

![images/38254b38a22a33f3a3ff1847d07d283a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/38254b38a22a33f3a3ff1847d07d283a.jpeg)



**"Lashley Guyton"**

---
---
**LightBurn Software** *July 15, 2018 02:58*

Can you try issuing simple GCode commands in the console?

G0 X0 Y0 should go to zero.

G0 X300 Y200 should go to the diagonally opposite corner.

G0 X0 Y200 should go to the exact spot that it homes to.



If these don't work, it's a configuration issue of some kind.


---
**Lashley Guyton** *July 15, 2018 03:01*

No luck. I got a new card and downloaded the files again. Going to try it in the morning. 


---
**Lashley Guyton** *July 15, 2018 03:02*

0,0 takes it to the middle of the cutting area. 


---
**LightBurn Software** *July 15, 2018 03:04*

Try typing:



get wcs



in the console and hitting enter. What does that say?


---
**LightBurn Software** *July 15, 2018 03:05*

It's possible you've set some kind of workspace offset, which would cause all kinds of trouble.


---
**Lashley Guyton** *July 15, 2018 03:09*

**+LightBurn Software** typing that where?


---
**LightBurn Software** *July 15, 2018 03:13*

get wcs  (in the console)


---
**Lashley Guyton** *July 15, 2018 03:14*

**+LightBurn Software** ok pardon my ignorance. By console do you mean computer? 


---
**LightBurn Software** *July 15, 2018 03:17*

In LightBurn there's a window called the console that allows you to type commands direct to the board, instead of having LightBurn issue them for you behind the scenes. 



So, in the same place you would manually type G0 X0 Y0 which you said you had done, you type "get wcs" (without the quotes) and hit enter.



If you haven't used the console before, it's tabbed behind the cut list by default. If it's not visible, you can enable it in the Window menu.


---
**Lashley Guyton** *July 15, 2018 15:25*

I see now. I was using the move tab. 



When I type the fist set of commands X0,Y0 it goes here ![images/be7c825e435427b24e037a454a63916e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/be7c825e435427b24e037a454a63916e.jpeg)


---
**Lashley Guyton** *July 15, 2018 15:31*

And for get wcs. ![images/830af093068cf45aa24db18dc2031800.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/830af093068cf45aa24db18dc2031800.jpeg)


---
**LightBurn Software** *July 15, 2018 17:01*

Not “GO”, “G0” (G zero).  Though it looks like it still moved. The get wcs command has to be typed in lower case or it will be ignored.


---
**Lashley Guyton** *July 15, 2018 17:12*

![images/4f3e333a5789c96224deb5e7a8960ae4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f3e333a5789c96224deb5e7a8960ae4.jpeg)


---
**LightBurn Software** *July 15, 2018 17:15*

Pay attention to which things I’ve capitalized above, copy the commands exactly. You’ve lower cased the G0 command, not the get wcs command.


---
**Lashley Guyton** *July 15, 2018 17:22*

Ok got it. It went home when I typed it correctly. Top left. 


---
**Lashley Guyton** *July 15, 2018 17:23*

Get wcs came back with all 0s on every line. 


---
**LightBurn Software** *July 15, 2018 17:26*

If G0 X0 Y0 sent it to the rear left, you’ve set your config wrong, or it’s not homing properly.  Origin is front left.


---
**Lashley Guyton** *July 15, 2018 17:33*

Ok. What needs to be changed? 


---
**LightBurn Software** *July 15, 2018 17:36*

I would re-download the config file that comes with the C3D and copy it over whatever is on your SD card to make sure it’s correct. I would also check that it homes correctly when you connect it. If it doesn’t, you may have a motor plugged in backwards. C3D should take over here, as this doesn’t appear to be a software issue.


---
**Lashley Guyton** *July 15, 2018 17:40*

Ok. Downloaded the new files last night on a fresh card. Still doing it today. Maybe something is plugged in wrong. 


---
**Lashley Guyton** *July 15, 2018 17:41*

Thanks for you help. Sorry it was difficult. 


---
*Imported from [Google+](https://plus.google.com/114884907345259366479/posts/Mh4HW6A5WwS) &mdash; content and formatting may not be reliable*
