---
layout: post
title: "Hey guys, i installed the coh.3d mini into my k40 and when i start the machine the glcd whistles and quits"
date: November 08, 2017 15:58
category: "C3D Mini Support"
author: "Marko Marksome"
---
Hey guys, i installed the coh.3d mini into my k40 and when i start the machine the glcd whistles and quits. The machine doesnt home itself when i start it. When i click on jog on the screen the laser head moves but it never homes itself. Is this board not homing itself like the m2 nano board? Also the dial knob on the lcd when i turn it left it goes down the list when turning right it goes up. The same when joging. Is there a way to reverse it? I believe it should be the other way round no?





**"Marko Marksome"**

---
---
**Marko Marksome** *November 08, 2017 16:40*

Oh next problem is i cant install the smoothie drivers on my win 7 professional 32 bit. It says that my windows is not compatible with the version 1.1 smoothieware... can anybody help? Please


---
**Marko Marksome** *November 08, 2017 17:11*

Next problem, i have installed the laserweb4 on my computer and it worked once, but now after the restart i only get a white screen... i dont know if i should cry or laugh...


---
**Ray Kholodovsky (Cohesion3D)** *November 08, 2017 21:33*

Hello!



-The screen will beep for about 3 seconds while the board is booting up, then turn off. 

-The machine will not home by default, but this is easy to change if you would like.  Create a text file called on_boot.gcode and the contents of the file should be G28.2

-Encoder knob: change 3.25 to 3.26 and 3.26 to 3.25 in the config.txt panel section. Save, safely dismount the drive, and reset the board or cycle all power. 



Regarding the 2 software issues.



There is a way to manually install the smoothie drivers, see: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)



The LaserWeb issue sounds like a lack of computer resources, between this and the fact that you are running 32 bit Windows... can you tell us more details about the computer you are using? 


---
**Marko Marksome** *November 08, 2017 22:27*

Well, its a Dell Optiplex 360 i use it because of the Mach3 software i use for my cnc milling machine. Nothing fancy. I see laserweb4 is web based, does that mean i have to be connected to the internet? I dont have internet in my workshop.

Thanks for the solutions of the other two problems.


---
**Ray Kholodovsky (Cohesion3D)** *November 08, 2017 22:45*

You don't need an internet connection for LW.  



Please check this:

[plus.google.com - Hello, I cannot anymore make the software start.. I get a blank screen when ...](https://plus.google.com/100075568232518942576/posts/WQHxyQCF2Sf)



(from here: [https://plus.google.com/+ArielYahni/posts/dr7R34GUidY](https://plus.google.com/+ArielYahni/posts/dr7R34GUidY) )



The 2 cases of the white screen I have seen are either that, or the insufficient computer resources. 



To the best of my knowledge, most new computers sold in the last 5 years have at least 4gb of RAM and thus run 64 bit Windows.  As a general statement, many new software do not have a build for 32 bit Operating Systems.   For example, Lightburn has builds only for 64 bit Windows and for Mac at this time. 






---
**Marko Marksome** *November 09, 2017 00:26*

Yeah i realised that. I have been talking to Jason about that. He mentioned that he will have a 32 bit version of lightburn but it will be limited. Guess i have to work on two computers or buy a usb smoothstepper board for the mach 3.. Well one cant have everything it seems :)


---
**Bill Keeter** *November 09, 2017 03:43*

**+Marko Marksome**  since you have GLCD you could just build the gcode on the windows box and save locally. Then load to SD card and put it back in to the K40.  No smoothie drivers needed since you can start the job with the glcd. 


---
**Marko Marksome** *November 09, 2017 10:45*

**+Bill Keeter** true, in the worst case i will do that. Only problem is my laser is in the workshop and my strong pc is at home, so testing and making adjustments is a little difficult. Is there an other free app i can use for generating the gcode?


---
**Marko Marksome** *November 09, 2017 11:20*

**+Ray Kholodovsky**​ Hey Ray, i change the knob dirrection like you said. It works now like it should. I made the text file for homing but it doesnt home after booting. I copied it on the sd card into the folder where the config file sits. When i go to play/ from sd card /and press on the "on_[http://boot.gcode](http://boot.gcode)" it homes itself, but like i said it doesnt do it automaticly. Am i missing something?



Also i did the deleting of the folder and laserweb is working again. Thanks


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 15:12*

Is .gcode the extension of the file? It should be. Is it in the root of the sd card? There should not be any folders. 


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 15:37*

It's in my first comment here. Flip 3.25 and 3.26 in config.txt 


---
**Bill Keeter** *November 09, 2017 15:53*

**+Marko Marksome** Auto homing would be cool. Hadn't even thought about it until now. For me, I boot the machine. On the GLCD I scroll down to "Prepare" and then click on "Home All Axes". Then I start my job. Also with Laserweb I added a home command to my exit gcode. So normally it's already homed from the previous cut.


---
**Marko Marksome** *November 09, 2017 16:01*

+Ray Kholodovsky (Cohesion3D) well i just made an text file and named it on_[boot.gcode](http://boot.gcode) extension is .txt

I copied it onto the sd card where the firmware and the config file are located.

I didnt use a special g code program, just a regular text file


---
**Marko Marksome** *November 09, 2017 16:02*

**+Bill Keeter** its a cool feature and if its possible to do then why not :)


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:02*

Yeah, you need to get rid of the .txt at the end. 



I realized I worded that tricky, the intent of it was, hey you only need to use a text editor, but the extension needs to be .gcode


---
**Marko Marksome** *November 09, 2017 19:20*

**+Ray Kholodovsky** ok now i managed to do that. On windows 10 the default setting is adjusted that one can not see and rewrite the extensions.... For everybody who wants to do the same, you need to go to start/explorer and in the right corner you go under options and  view and there you can click off the mark on hide extensions or something like that. I managed to create a gcode file now. Thanks!


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/RK2zECXnvti) &mdash; content and formatting may not be reliable*
