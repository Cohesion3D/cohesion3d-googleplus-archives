---
layout: post
title: "Hi, Other than the power-input I provide to the mosfets, is there a max DC current that i can run through them?"
date: December 14, 2017 19:22
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Hi,



Other than the power-input I provide to the mosfets, is there a max DC current that i can run through them?





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2017 19:25*

Motor drivers are rated ~35v, voltage regulator and mosfets are 40V rated. 



In general I do not recommend putting more than 30v anywhere. 



If you need higher values, why? and then you will probably want a different solution. 


---
**Todd Mitchell** *December 14, 2017 19:33*

Just looking at options for what I can control digitally rather than a large switch box.



Pump for water cooled spindle

Couple of fans

Perhaps Low voltage lighting for the case


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2017 19:41*

In my experience if these things are not 12/24v then they will be AC powered in which case - SSR or Relay 




---
**Todd Mitchell** *December 14, 2017 19:56*

Agreed.  I'll use a relay for anything outside of that range.  I still need to know what sort of current I can handle with the mosfets tho?  Do you have any ratings?


---
**Todd Mitchell** *December 14, 2017 19:59*

I.e. I want to run this water cooler directly from the cohesion board without a relay. 





It's 19W @ 12v so looks like 1.6A



[amazon.com - Robot Check](https://www.amazon.com/gp/product/B01H1BE4VQ/)


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2017 20:04*

That's fine.



For ReMix I specified ~3 amps per bottom row mosfet making for a total of 12 amps in thru the pluggable input terminal at the bottom left. 



The FETs themselves are capable of 14 amps, each output terminal is 8 amps...  If you need more than 12 amps in total across the bottom row, let me know. 


---
**Todd Mitchell** *December 14, 2017 20:23*

**+Ray Kholodovsky** Curious - if the FETs are 14 and terminals are 8, what is the constraining factor keeping it at 3 amps?


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2017 20:28*

The maximum rating of the input terminal - 12 amps / 4 amps = ~ 3amps per if used "uniformly"



Of course you can bypass this by manually feeding the + voltage to each device (ie: do not hook anything up to the + on the output terminal, those just redistribute the same +VMOSFET you put into the input terminal). 



Now you need thick ground wires at the remaining 2 power input terminals (main and bed), are limited by the 8 amps each of the mosfet output terminals and the total draw on ground through everywhere. And heat.  There will be heat. 


---
**Todd Mitchell** *December 14, 2017 20:36*

Thanks for the explanation.  Sounds like it's easier and less risky to just use SSRs


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2017 20:39*

Eh, I think devices up to a few amps like the one you linked is fine.  We're doing 3.3 amp heaters for a 3d printer anyways. 


---
**Todd Mitchell** *December 14, 2017 20:51*

Yes, I agree.   I was speaking about the larger items.  I'm happy to drive it up to 3 amps.  that should get me fairly fare in avoiding a switch panel.  I'm using a wall-wart from the thrift store rated at 12vdc 6A so that's my max for now which should be fine for LED panel, water pump and alignment alignment lasers


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/VrCXnVYXJUV) &mdash; content and formatting may not be reliable*
