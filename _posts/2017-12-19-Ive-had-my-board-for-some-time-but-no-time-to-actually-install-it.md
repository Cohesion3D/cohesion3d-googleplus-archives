---
layout: post
title: "I've had my board for some time, but no time to actually install it"
date: December 19, 2017 08:32
category: "K40 and other Lasers"
author: "Richard Vowles"
---
I've had my board for some time, but no time to actually install it. I'm not asking for help as yet, chances are something fundamental is wrong with the laser knowing my luck!



Photos of my device are attached.



![images/17120517d7d90aa8cf6d41e7143e85d3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/17120517d7d90aa8cf6d41e7143e85d3.jpeg)
![images/dafac8f4343c0f9d9b5fa16672ec7fae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dafac8f4343c0f9d9b5fa16672ec7fae.jpeg)
![images/82d319d3f2ef2e51b2a7463d6f4e6185.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82d319d3f2ef2e51b2a7463d6f4e6185.jpeg)
![images/b9e8806dc78f6e174dba30341f8f582a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b9e8806dc78f6e174dba30341f8f582a.jpeg)
![images/b0d9244ced2a495e99c16136a3eb148d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0d9244ced2a495e99c16136a3eb148d.jpeg)

**"Richard Vowles"**

---
---
**Richard Vowles** *December 19, 2017 09:32*

So i can see that my 'out' has four pins not three. And my power has 3 pins, gnd, 5v and 24v. I presume i will need to ignore the 5v and put the other two by themselves in the mains power blue terminal.


---
**Don Kleinschnitz Jr.** *December 19, 2017 13:48*

**+Richard Vowles** unclear as to what you are asking or need help with :)?


---
**Richard Vowles** *December 19, 2017 17:15*

I'm currently just posting what I'm doing 😁 (or yells stop! That won't work!)


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:40*

This is older/ worse than what I have ever seen before. 



Just trace what is coming where from the PSU, figure out what is on the other end of the ribbon cable, and trace the other wires. 



Going to warn you now about that ancient PSU which is what I think the 3rd pic is of.  It might cause problems down the line. 



Ultimately we need to identify 2 motors, endstops, and power, and the L line for laser fire (this would go to said ancient PSU). 


---
**Richard Vowles** *December 19, 2017 23:41*

Yeah, I'm pretty lost I must admit.


---
**Richard Vowles** *December 19, 2017 23:43*

The ribbon cable goes to the end stops which appear to be optical and the x-motor. The problem i am having is what the 3 wires coming out of the existing board do.  I think I should unwrap everything - or should I just give up now? Would it be more sensible to replace the PSU (if that is possible?)


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:47*

That's usually the case with the ribbon, but then the motor next to it is going to a port labeled "X" while usually the individual motor is the Y. 



If this is the case, that settles those concerns. Just check the guide for the proper way to plug those in. 



[cohesion3d.com - Getting Started](http://cohesion3d.com/start)



Grab the latest firmware and config for your memory card as well while you are there (dropbox link at the end).



If I recall you have an original generation board (does it say v2.2 at the bottom?)


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:49*

Oh, I would mentally (aka financially) prepare to get a new LPSU but would not do it now... that would complicate matters further. 


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:51*

[plus.google.com - Just received a Cohesion3D Mini for my K40 Laser Cutter My old board and hook...](https://plus.google.com/116921257581951229508/posts/1ujtXtVQKnp)


---
**Richard Vowles** *December 19, 2017 23:51*

Yes, sorry, I put it in the wrong place (the X vs Y).


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:55*

Ok, we need to identify L.  Where do the white wires from "Out" on the main board go to? 


---
**Richard Vowles** *December 19, 2017 23:55*

I have a 2.2 - yes - so I'll do as you say. I still however don't know what to do about the missing L or where to put the 2.4 pin. 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 00:02*

Find the white cable coming from "OUT" at the bottom right of the main board - this is the only thing left that we have not identified, trace where that goes.  Process of deduction suggests that that would have something to do with firing the laser. 



You correctly said that you can take 24v and Gnd and plug them in to "Main Power In" screw terminal, meter it first to check polarity is correct.  You can ignore 5v, we don't need it. 


---
**Richard Vowles** *December 20, 2017 00:06*

Ok - they seem to go to the PSU, but I think this is just going to have to be a voyage of discovery :-)


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 00:06*

Rule of thumb: I need at least 3 pictures to be able to help :) 


---
**Richard Vowles** *December 20, 2017 01:02*

Ok - so I traced Out to 5V, GND2 and a yellow wire. 5V is by itself - connected to the PSU. GND is connected via black and green back to the PSU from the controller - and then the black links to the LPSU.



The yellow wire travels by itself to the LPSU. So I'm going to presume that this is my PWM? 



I traced the Laser Fire and Laser Test buttons back to a couple of black pins on the LPSU.  I'm unsure whether the L is an <b>out</b> or an <b>in</b> on the Cohesion3D - I am presuming an <b>out</b>? Using the buttons on the front, when pressing Fire, it completes the circuit for the two black pins. Not sure if or what I should connect to these?



The whole block is YELLOW-BLACK, BLACK-BLACK. I took a closer photo of these...







![images/0c4cc4acc40cb03bc0610239555bd4a1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0c4cc4acc40cb03bc0610239555bd4a1.jpeg)


---
**Richard Vowles** *December 20, 2017 01:10*

If that isn't clear, YELLOW-BLACK is ?PWM-GND2, then the next BLACK-BLACK forms a closed circuit when the Fire button is pushed.


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 01:11*

L is a wire that is part of the power chain between the stock control board and the LPSU. It can be tested - when you touch the presumed L wire to ground the laser should fire. 


---
**Richard Vowles** *December 20, 2017 01:24*

Ok, so I should fire the machine up and short the yellow and black wires (that came from Out) to see if it fires? Ok, i'll do that.


---
**Richard Vowles** *December 21, 2017 23:38*

Right, it has taken me a few days to get it in a shape where I can do anything. On checking the water pump I found it broken, so I had to purchase a new one, and then ended up having to 3d print an adapter for the piping. Once I got that going, we fired it up (my son and I) and managed to get it firing - but it seems pretty weak - no laser comes through to the main body, but the yellow shorted with 5v does nothing.



At this point I have reached the end of what I'm willing to do to get this one working, so I am going to abandon it for parts and order one of the newer ones. I'll try and use the Cohesion board in that and see how I go - thanks for all your help!


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2017 23:41*

The going advice is to get a blue and white one with a pot, no need for any of the fancier ones or a digital control panel. 


---
**Richard Vowles** *December 21, 2017 23:42*

Just looking in Aliexpress now :-)


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2017 23:44*

Where are you located? 


---
**Richard Vowles** *December 21, 2017 23:44*

New Zealand!


---
**Richard Vowles** *December 22, 2017 00:07*

Thanks for your help - really appreciated **+Ray Kholodovsky**!


---
*Imported from [Google+](https://plus.google.com/+RichardVowles/posts/Tmk4hnZSRib) &mdash; content and formatting may not be reliable*
