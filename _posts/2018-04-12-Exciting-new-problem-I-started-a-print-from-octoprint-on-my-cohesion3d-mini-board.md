---
layout: post
title: "Exciting new problem! I started a print from octoprint on my cohesion3d mini board"
date: April 12, 2018 02:47
category: "C3D Mini Support"
author: "Chapman Baetzel"
---
Exciting new problem!  I started a print from octoprint on my cohesion3d mini board.  It had done countless prints before now with no trouble.  Suddenly serial lost connection.  after a bit of digging into how to try to connect octoprint to printer over usb, I try a dmesg on the pi to find it putting out "unable to enumerate USB device" messages.  Seems like the USB chip(?) on cohesion3d mini might be kaput.  Any guidance?  





**"Chapman Baetzel"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2018 02:48*

There is no USB Controller on the board.  What are the LEDs on the board doing?  


---
**Remco Schoeman** *April 12, 2018 06:27*

Did you check the cable and/or the pi itself? Maybe the USB hub on the pi fried?


---
**Chapman Baetzel** *April 12, 2018 11:47*

tried another USB port on the pi.  same result.  failed to enumerate USB device.  I'll try another USB cable, then connecting to another device.  I haven't checked the lights on the board; what should I be looking for them to do? 


---
**Chapman Baetzel** *April 12, 2018 13:29*

My inital hypothesis was poorly stated.  Something on the c3d mini(not remix, thats another printer) enables it to communicate over USB.  Based on googling "unable to enumerate USB device" error message, a possible cause could be the failure of this thing which enables USB communication.  I called it a USB controller, but that is inaccurate.    To recap.  Cohesion 3d Mini board, not talking to rPi over USB. troubleshooting in progress.  


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2018 21:42*

I'd like to know what the green leds are doing, to see if there is any brain activity on the board. 



First assumption if there are green leds is that the microsd card corrupted, see if you can put files on a new one.



[dropbox.com - Smoothie Stock Config June 2017 w GLCD Enabled and 0.27o for C3D Mini](https://www.dropbox.com/sh/t2f37b4l182lig0/AACNhyTLnqMcfXiSuGuxiKOXa?dl=0)


---
**Chapman Baetzel** *April 12, 2018 23:52*

green LEDs are blinking on the controller.


---
**Chapman Baetzel** *April 13, 2018 00:09*

Its a power supply issue.  I connected to computer(not pi) with no power to controller aside from USB and it connected fine.  Connected power and homed axes.  Within a few minutes it stopped responding on serial.  I'm gonna have to check that power supply tomorrow.  Could be a loose wire, but i think the C3d mini is not the point of failure. 


---
**Chapman Baetzel** *April 26, 2018 23:20*

ok maybe not a power supply issue.  the new PSU is doing the same thing.      What happens is I start a print, which sends gcode G28 to home all axes then a M190 to wait for bed to reach temp.  after about 8 temp status messages it stops responding on serial.  PSU cycle fixes it, but only if i also unplug and replug USB.  So actually i have to cycle the c3d mini entirely.  Is it getting a brownout trying to heat up the bed?


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2018 00:28*

Do you have a screen hooked up? 



Alternatively, please look at the PLAY LED - it's alone by the button in the upper right corner.  Is it solid or blinking? 



Brownout is something a psu does when it can't provide enough current, not something a screen does.  



It is possible that it heats up too slowly and the board halts as a safety measure.  The screen or LED blinking would tell you that ^^


---
**Chapman Baetzel** *April 27, 2018 01:00*

no screen.  Connected to a pi running octoprint.  I could try the same thing with a laptop connected and see if that changes things.  pi's are notorious for not providing sufficient USB power. And if it is insufficient power from the PSU then it would try to draw from the pi(maybe? idk).  But its a 20A 24V PSU.  I bought this one to run my bigger printer with the 300mm bed.  Heating a 200mm square bed shouldn't give it so much trouble.  oh wait.  Its a 12V bed, with a pwm limit of 64 to be on 24V. I bet that's causing RF noise and messing up the USB serial.  Not sure if i have chokes on that USB cable, if it would even help. 


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2018 01:07*

No it wouldn't but it might cause a USB issue, I have also experienced the low amount of power the board gets from the pi. But what about the led status I asked about? 


---
**Chapman Baetzel** *April 27, 2018 01:29*

I'll check it when I'm next at my printer.  Sadly this issue seems to crop up when I'm working and trying to get parts done remotely, with my wife clearing the bed for me 150 miles away.  


---
*Imported from [Google+](https://plus.google.com/+ChapmanBaetzel/posts/eui6dmVkpRY) &mdash; content and formatting may not be reliable*
