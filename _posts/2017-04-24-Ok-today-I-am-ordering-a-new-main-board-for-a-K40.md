---
layout: post
title: "Ok today I am ordering a new main board for a K40"
date: April 24, 2017 17:02
category: "General Discussion"
author: "Maker Fixes"
---
Ok today I am ordering a new main board for a K40. What are the advantages of your board over a Rams board. I know your won't need rewiring. Are there other thinks the smoothie system is better for?





**"Maker Fixes"**

---
---
**Griffin Paquette** *April 24, 2017 17:11*

It's worlds better. 32 bit processing at 120mhz vs. 8bit at 16mhz. The board is more compact, reliable, and safer. RAMPS boards are usually quite unreliable.  I went through 4 in a year just from board failures.


---
**Maker Fixes** *April 24, 2017 18:09*

4 in a year that's better then the Moshe board I got lasted 2 hours lol


---
**Griffin Paquette** *April 24, 2017 20:30*

Ha the costs added up quite quickly. 5 months was the longest I got I believe and that's with a $60 Ramps setup


---
**Claudio Prezzi** *April 25, 2017 07:09*

The biggest advantage is, that you can use LaserWeb4 with it ;)


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/MLD7oxjiTpd) &mdash; content and formatting may not be reliable*
