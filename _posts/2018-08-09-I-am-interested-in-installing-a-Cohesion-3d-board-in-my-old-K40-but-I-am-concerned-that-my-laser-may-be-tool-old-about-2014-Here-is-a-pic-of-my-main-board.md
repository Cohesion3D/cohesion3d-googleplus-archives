---
layout: post
title: "I am interested in installing a Cohesion 3d board in my old K40 but I am concerned that my laser may be tool old (about 2014) Here is a pic of my main board"
date: August 09, 2018 17:15
category: "General Discussion"
author: "Trevor Hinze"
---
I am interested in installing a Cohesion 3d board in my old K40 but I am concerned that my laser may be tool old (about 2014)  Here is a pic of my main board.  My laser also has the large green power resistor on the side wall and some reading on here led me to believe that may present a problem.  Can anyone tell just by looking if I will have any luck replacing this board with the cohesion?  I dont want to order one if it also means that I need to buy a new laser at the same time.  Thanks in advance for any help!!!

![images/11b5bc837e5f6788fc3ff0951005595c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11b5bc837e5f6788fc3ff0951005595c.jpeg)



**"Trevor Hinze"**

---
---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 17:32*

that looks like an m2 board. the motor and endstop connectors should be the same. the power connector is different. That said, there are provisions on the cohesion3d to connect with screw terminals. this will require a little bit of extra work (cutting and stripping the wires for the power and laser signals and maybe mounting the board) but is not difficult. At the end of the day you should still be able to use the c3d mini.



here is an article on identifying boards: [https://www.lasergods.com/controller-dsp-identification/](https://www.lasergods.com/controller-dsp-identification/)


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 17:36*

this is from the installation documentation and is very similar to what you would need to do. here is the link to the full documentation: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)

![images/347a9150ac8248bd9da49379c0d1e813.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/347a9150ac8248bd9da49379c0d1e813.png)


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 17:37*

so, long story short we can help you identify the wires on your power connector and determine where they go :)


---
**Trevor Hinze** *August 09, 2018 18:01*

excellent, thanks for your quick reply.  Cutting, splicing, soldering are all zero problem for me, I just wasn't confident that I would be able to determine the necessary connections.  Thanks for the link as well and BTW your Videos are an inspiration as to what this little laser can be!**+Tech Bravo** 


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 18:10*

no problem. thanks for checking out my ramblings. when you decide to pull the trigger let me know and i will provide you will the necessary info on the wiring for the power connector and other info so you can get a head start on it. and if you want, feel free to try out the beta of my product configurator :) : [lasergods.com - Cohesion3D Product Configurator](https://www.lasergods.com/cohesion3d-product-configurator/)


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 19:03*

here is the 6 pin connector pinout for the lihuiyu m series: [lasergods.com - Lihuiyu 6 Pin Power Pinouts](https://www.lasergods.com/lihuiyu-6-pin-power-pinouts/)


---
**Trevor Hinze** *August 10, 2018 04:58*

**+Tech Bravo** excellent,I actually found a friend locally who had purchased one and then decided to go another route so I just bought it from him today complete with the screen.  I look forward to starting the conversion soon.  By using the links and photos you provided I feel as though I can get this to work.  Thanks again .


---
**Trevor Hinze** *August 11, 2018 20:28*

Thanks to the excellent pictures that tech bravo pointed me to I was jist able to get the cohesion board hooked up in my old k40 and get it to home with the menus, that seems to be a very good sign.  Any ideas why it thakes 3 to 5 power cycles to get it to boot up properly?  The other 80 pct of the time upon power up I get an audible alarm, then this screen

![images/a9e5131311d95f9a94ba537d72d5c13e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a9e5131311d95f9a94ba537d72d5c13e.jpeg)


---
*Imported from [Google+](https://plus.google.com/106981345031249926577/posts/gKfQmLk5PNH) &mdash; content and formatting may not be reliable*
