---
layout: post
title: "Cohesion crew, this is another configuration issue maybe a wiring, it is an issue my extruder motor will not move, and of course there is no logical reason why it will not it is a motor like all of the other ones Here is the"
date: October 31, 2018 03:11
category: "General Discussion"
author: "Family First"
---
Cohesion crew, this is another configuration issue maybe a wiring, it is an issue my extruder motor will not move, and of course there is no logical reason why it will not it is a motor like all of the other ones  Here is the code, what do you see?# Extruder module configuration

extruder.hotend.enable                          true             # Whether to activate the extruder module at all. All configuration is ignored if false

extruder.hotend.steps_per_mm                    140              # Steps per mm for extruder stepper

extruder.hotend.default_feed_rate               600              # Default rate ( mm/minute ) for moves where only the extruder moves

extruder.hotend.acceleration                    500              # Acceleration for the stepper motor, as of 0.6, arbitrary ratio

extruder.hotend.max_speed                       50               # mm/s



extruder.hotend.step_pin                        2.3              # Pin for extruder step signal

extruder.hotend.dir_pin                         0.22             # Pin for extruder dir signal

extruder.hotend.en_pin                          0.21             # Pin for extruder





**"Family First"**

---
---
**Family First** *October 31, 2018 03:42*

second update, I trided a different driver and nothing happened.  There is not resistance on the motor, nothing.  


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2018 03:43*

Grab the latest smoothie firmware build. We ship a cnc build on our memory card since we sell laser cutter boards.  


---
**Family First** *October 31, 2018 13:23*

**+Ray Kholodovsky**   I will do that, it would make sense that this would be the issue. 


---
**Family First** *October 31, 2018 14:02*

I did that, the I downloaded the raw files the firmware.bin converts to to firmware.cur just like it is supposed to do, and then I get the temp on the hot end up,  no problem and the extruder still does not move.  Home the delta, and once again no movement.  I wonder to what degree these line have anything to do with it: 

                                                              # see [https://github.com/grbl/grbl/blob/master/planner.c#L409](https://github.com/grbl/grbl/blob/master/planner.c#L409)

                                                              # and [github.com - grbl](https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.8)



I put these links in my browser and they lead to nowhere.  But when I click on the image they bring up information which will not consume.


---
**Family First** *October 31, 2018 14:04*

Looks like the GRBL is an arduino link smoothieware on Arduino, is that correct?  If that is the case then the links are not germane to the problem I am seeing.


---
**Family First** *October 31, 2018 14:10*

Quick test of the extruder on the Z shows that the motor is operational.  I will try another driver with the new configuration now. 


---
**Family First** *October 31, 2018 14:17*

Bad driver was masking a CNC configuration.  Changed the configuration first (be sure to download the raw file, not the other watever that might be) then look at the motor itself, the wiring to the motor, then the driver and finally your passport, I do not think it will work for citizens of Macedonia.  


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2018 15:45*

I think you need to try saying that again, I don’t know what “citizens of Macedonia” is supposed to mean. 


---
*Imported from [Google+](https://plus.google.com/105733681846829955664/posts/UqeWuHmMuwT) &mdash; content and formatting may not be reliable*
