---
layout: post
title: "Can anybody tell me why those lines apear while engraving??"
date: June 25, 2018 17:26
category: "C3D Mini Support"
author: "Marko Marksome"
---
Can anybody tell me why those lines apear while engraving??

It happens when i am working with 254 dpi in lightburn.

They are different then the rest of the engraving.

Like some sort of frequency shift.



![images/29a4a27398a49cc90cb63405af587b1d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/29a4a27398a49cc90cb63405af587b1d.jpeg)



**"Marko Marksome"**

---
---
**Don Kleinschnitz Jr.** *June 25, 2018 17:49*

The grips are curved so the beam is exposing at various depths. That may or may not be the problem?



Look at the surface with a loop and that may also help give a clue.



Need to rule out optical vs mechanical vs software by changing materials, position ect.



I would try flat material, in different locations at different speeds and powers.



Lets us know your speed, power and material.




---
**Greg Nutt** *June 25, 2018 17:50*

Sure.  It's a different elevation and angle.  Changes the focal length and intensity.  Also the refraction of the wood grain will probably differ slightly because of the curvature in those spots.  The changes in density in the wood grain itself could also create some variations.




---
**Marko Marksome** *June 25, 2018 18:11*

No, i can clearly rule that out about the curvature.

I am making grips since 1 year now and before cohesion 3dmini i used the standard nano board with the k40 whisperer software. Never ever had i this problem with it.

I have used the c3dmini board for few months now and usualy i have worked with 500 dpi.

Never had this problem aswell. Only with 254 dpi in lightburn this thing happens.



I cant use 500 dpi anymore because the engraving is out of allignment anytime i work with it. It distorts and skipps  if i use it.


---
**Don Kleinschnitz Jr.** *June 25, 2018 18:22*

I would still expose a sample that is flat so that we can better see what is going on ....

Its dunlikely that the C3D board itself is doing this its either something changed in your machine or an anomalously with Lightburn.



Is this a K40?


---
**Marko Marksome** *June 25, 2018 18:43*

Yep a k40.

On a flat surface it happens the same.

I believe it is a lightburn issue.

I will talk with the maker of lightburn. If we can sort this problem out i will post the solution here, so anybody with the same problem can afjust it accordingly.




---
**Don Kleinschnitz Jr.** *June 25, 2018 19:38*

**+Marko Marksome** 

Is this image dithered?




---
**Marko Marksome** *June 25, 2018 20:08*

Grayscale.

I talked allready with the developer of lightburn, and he suggested to change the dpi to 260 or 270. Ill test it tomorrow.


---
**Remco Schoeman** *June 26, 2018 05:16*

Maybe it's Moire interference caused by the difference in dpi of the source pattern and the engraving. If the banding artifact changes if you change the engraving dpi slightly it's definitely Moire interference.



[en.m.wikipedia.org - Moiré pattern - Wikipedia](https://en.m.wikipedia.org/wiki/Moir%C3%A9_pattern)


---
**Marko Marksome** *June 26, 2018 07:08*

That is very logical and possible.


---
**LightBurn Software** *June 27, 2018 01:12*

This was our thinking as well, hence the suggestion to try slightly different DPI settings.  We have an option for “pass through” processing. It currently enforces 1-bit (for dithering) but we could alter it to allow it for grayscale as well. It locks the scanning pattern to the source image pixels, regardless of scaling. Primarily used to avoid resampling artifacts (moire patterns) when using pre-dithered images, but could be used here as well. We use a good bicubic resampler, so with continuous tone images this is rarely an issue, but it’s still possible.


---
**Marko Marksome** *June 27, 2018 07:45*

If you want to test the pass through for grayscale i can test it out on the grips, since my image is affected. (If you cant replicate the moire)


---
**Marko Marksome** *June 27, 2018 18:36*

Yes, i have tested it today with 260 dpi and 270 dpi.

260 dpi is even worse, but 270 it seems that there is no moire at all. It seems clean now!


---
**LightBurn Software** *June 27, 2018 18:37*

**+Marko Marksome** - that’s good info, thank you. I’ll have a look at the resizing code and see if there is additional filtering we can do to reduce this.


---
**Marko Marksome** *June 27, 2018 19:22*

Thanks, thats great news. One more thing tho. In the next update of lightburn is it possible to get just a little filter, where the grayscale image can be adjusted? Just a simple lightness/contrast adjustment combo? Please....


---
**Ned Hill** *June 27, 2018 22:17*

Are the lines parallel or perpendicular  to the engraving direction?


---
**Marko Marksome** *June 27, 2018 22:36*

Parallel.


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/SVoDDsjRxmX) &mdash; content and formatting may not be reliable*
