---
layout: post
title: "I installed the Cohesion3D Mini on my K40 today"
date: January 23, 2018 19:29
category: "C3D Mini Support"
author: "Holly Hartner"
---
I installed the Cohesion3D Mini on my K40 today. I also installed Lightburn on my Mac and set up a file to test. When I plug the laser in via USB I cannot connect...no motion. I am also not seeing the blinking green lights on L2 and L3. What did I do wrong. Before and after photos attached for reference. 



![images/f32c566ead709b79c24cceeb2d4ebd0f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f32c566ead709b79c24cceeb2d4ebd0f.jpeg)
![images/069f6d51a7cb38d47199b5af5c380d1f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/069f6d51a7cb38d47199b5af5c380d1f.jpeg)
![images/2e985030dc4fa60ec3450cef9f9b58c3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2e985030dc4fa60ec3450cef9f9b58c3.jpeg)
![images/eec90d659aa72ffb1bf163c48670c34a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eec90d659aa72ffb1bf163c48670c34a.jpeg)

**"Holly Hartner"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 19:33*

What are the states of each LED? Are the green ones on? 


---
**Holly Hartner** *January 23, 2018 19:36*

**+Ray Kholodovsky** yes, it was hard to get a pic because the red light is much brighter. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 19:37*

So, all the green ones are on and solid? 


---
**Holly Hartner** *January 23, 2018 19:41*

**+Ray Kholodovsky** Correct


---
**LightBurn Software** *January 23, 2018 19:43*

One other user has an issue connecting on their Mac. I’ll be looking at the code tonight to see if there is anything missed.


---
**Holly Hartner** *January 23, 2018 19:46*

**+LightBurn Software** Thank you!!


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 19:48*

I don't think we're there yet - L2 and L3 should be blinking. 



Power off and disconnect USB Cable.



Pull and Reinsert the MicroSD Card - make sure it is seated properly. 



Power up just the K40.  See if the LEDs blink.



Failing that, please power down again, pop the memory card into a reader, and see if it shows up and you can see the config and firmware files on the card. 


---
**Holly Hartner** *January 23, 2018 21:00*

**+Ray Kholodovsky** I didn't have any luck with reinserting the MicroSD card. The only item I have that can read a MicroSD card is a Kindle. When I put it in the kindle it notes that 1.87GB of 1.87GB are available. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 21:03*

When you plug the USB cable into your computer does a drive show up? Can you view the contents that way? 


---
**Holly Hartner** *January 23, 2018 21:17*

**+Ray Kholodovsky** I tried it on my Mac and a PC, but no luck with that either. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 21:21*

Let's stick with PC here:

To be absolutely clear:

The card is in the board, you plug the usb cable in, and you don't see any new drives show in Computer?



What is the make of the PC and what Operating System?


---
**Holly Hartner** *January 23, 2018 21:35*

**+Ray Kholodovsky** That is correct, I do not see a drive show up on the PC. The PC is a Dell running Windows 10.


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 21:41*

First thing on our debugging list:

Are you using the USB Cable that came with the K40?  I think you are based on the 3rd picture.

It has been known to cause problems. 

Can you get another USB Cable, preferably a good one with Ferrite Beads?


---
**Holly Hartner** *January 23, 2018 21:52*

**+Ray Kholodovsky** I tried three other USB cables, but do not have one with ferrite beads. Tow of the cables I use regularly for a printer and a Silhouette machine. The blue one that came with the laser did work with the original board and Chinese software on the PC if that helps. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2018 22:07*

The next debugging step would be to try a different MicroSD Card:



Format it FAT32, and then put the files onto it and put the new card in the board then try again. 



The files are located here: [dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0) 



Are you able to do this?  (with respect to needing another MicroSD Card and reader for your computer)


---
**Holly Hartner** *January 23, 2018 22:15*

**+Ray Kholodovsky** I will have to buy both the reader and MicroSD card.


---
**Holly Hartner** *January 24, 2018 21:19*

 **+Ray Kholodovsky** Will exFAT work?


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 21:20*

Is FAT32 not an option? This is available in the windows format utility. 


---
**Holly Hartner** *January 24, 2018 21:27*

**+Ray Kholodovsky** I am slightly PC illiterate, but I will see if I can format it to FAT32 on the Dell. 


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 21:31*

This PC/ computer/ whatever it's called in file explorer then right click on the card drive, format. File System is FAT32 


---
**Holly Hartner** *January 24, 2018 21:40*

**+Ray Kholodovsky** It worked!! The laser reacts to Lightburn now and the 2 green lights are blinking. Thank you! Thank you!


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 21:43*

I'm glad to hear it. SD card corruption is one one of those problems that's been tough to pin down. Be sure to "safely dismount" the drive in windows before pulling the USB cable. 


---
**Holly Hartner** *January 24, 2018 21:52*

**+Ray Kholodovsky** I understand. Thank you!


---
**Holly Hartner** *January 24, 2018 22:13*

 **+Ray Kholodovsky** I just tried a test and it is only moving along the X-axis. I made sure the drivers were plugged in securely, but still no change. 


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 22:15*

Try the jog buttons, there's a move tab in LB.


---
**Holly Hartner** *January 24, 2018 22:23*

Are you referring to this tool in the attached image? I tried that all over the workspace in Lightburn, but it only moves left and right. 

![images/328169f884c27a09d845a2fca5929cec.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/328169f884c27a09d845a2fca5929cec.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 22:26*

On the right toolbar, Cuts, Move, Console are tabs.  Click Move. 



Then you'll get this: the up and down are Y. 



[raw.githubusercontent.com](https://raw.githubusercontent.com/LightBurnSoftware/Documentation/master/img/MoveToolBox.PNG)


---
**Holly Hartner** *January 24, 2018 22:41*

**+Ray Kholodovsky** Up and down do not work and when I tried left the head moved hard to the left with a bang. 


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2018 23:26*

As a quick fix you can do this:



Power off.  Move the motor cable from Y to A (the 4th driver).



Remove the config.txt file currently on the card and place this new one onto it. You can do this via USB cable (board shows up as a drive in computer) and then you have to dismount the drive in Windows and reset the board.  



[dropbox.com - C3D Mini Laser Batch 2 - Provided Config with Y remapped to A](https://www.dropbox.com/sh/9b6z1rpg1nbmnkn/AAAAmp2KY9oDjijR6r3X0ndWa?dl=0)


---
**Holly Hartner** *January 25, 2018 01:20*

**+Ray Kholodovsky** Success! I was able to do a quick test and then this photo...not perfect I know, but I'm happy it's working. 

![images/cd9b1915d19744016ab6cf512a9d1cd5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cd9b1915d19744016ab6cf512a9d1cd5.jpeg)


---
**chris b** *February 13, 2018 06:51*

Very great full for this info, installed my  c3d, identical wiring to this one, and have the exact same issue ,soon as home from work will try the fix and post update. 


---
*Imported from [Google+](https://plus.google.com/111821420838612937083/posts/JFRPyJbcyix) &mdash; content and formatting may not be reliable*
