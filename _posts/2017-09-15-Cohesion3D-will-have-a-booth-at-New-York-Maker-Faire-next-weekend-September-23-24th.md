---
layout: post
title: "Cohesion3D will have a booth at New York Maker Faire next weekend September 23 & 24th..."
date: September 15, 2017 00:24
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Cohesion3D will have a booth at New York Maker Faire next weekend September 23 & 24th... I'll be there, showing off some cool machines that the boards are controlling.  



We should be in Zone 2 with the 3D Printers. 



If you will be there, please stop by to say hello and check out the showcase. 



Also, I could use some help at the booth.  If you can help out, please comment and let me know.  I will cover your ticket.





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jim Fong** *September 15, 2017 00:42*

Good luck. I've only been to the one here in Michigan.  New York would be a crazy fun time.  


---
**Marc Miller** *September 15, 2017 05:33*

I'd be happy to meet you in person but will have to send you a virtual high five from California instead.  Hope it's a great show.


---
**Ray Kholodovsky (Cohesion3D)** *September 15, 2017 13:01*

I'll try to make the next Bay Area Maker Faire **+Marc Miller** if that's more within your travel capabilities. 


---
**Marc Miller** *September 15, 2017 16:40*

It would only be a six hour drive from LA!  :P 


---
**David Scull** *September 22, 2017 10:58*

Going to try to make it on Sunday and bring you some things 3d printed with your board.  Hope to see you then.


---
**Ray Kholodovsky (Cohesion3D)** *September 23, 2017 03:16*

**+David Scull** I hope to see you too.  I emailed you a ticket with my compliments. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/ZAtj9ruEMNT) &mdash; content and formatting may not be reliable*
