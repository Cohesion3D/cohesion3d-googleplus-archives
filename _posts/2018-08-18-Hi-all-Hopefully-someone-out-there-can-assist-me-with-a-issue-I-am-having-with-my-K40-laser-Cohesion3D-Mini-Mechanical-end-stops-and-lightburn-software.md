---
layout: post
title: "Hi all. Hopefully someone out there can assist me with a issue I am having with my K40 laser / Cohesion3D Mini / Mechanical end stops and lightburn software"
date: August 18, 2018 03:47
category: "K40 and other Lasers"
author: "Cammo T"
---
Hi all.  Hopefully someone out there can assist me with a issue I am having with my K40 laser / Cohesion3D Mini / Mechanical end stops and lightburn software.



I made the cardinal sin of upgrading my K40 laser right out of the box before even getting it up and running.  



The latest problem I have is when I press the home button within lightburn the gantry moves back to the top left hand side and then keeps banging into the side of the gantry and doesn't stop.  



So I purchased 2 × Mechanical Limit Switch but I can't seem to get them to work with the Cohesion3D board.  Is there a guide on how to wire/code them? 



I have them wired as common / normally closed.  I have plugged them into the end stops pins on the Cohesion3D but again after pressing the home button in lightburn the gantry heads back to the top left hand side position hits the mechanical limit switch and then keeps banging against it.



Any ideas?



Thanks in advance.







**"Cammo T"**

---
---
**Joe Alexander** *August 18, 2018 04:14*

please add some photos of the actual endstop positions as well as the connections to the board to help troubleshoot this for you. It mat be something as simple as editing the config.txt file on the sd card for the endstops by inverting the pin(documentation is available on [smoothieware.org](http://smoothieware.org))


---
**Don Kleinschnitz Jr.** *August 18, 2018 17:45*

It does sound like the enstop signal is inverted?


---
**Tech Bravo (Tech BravoTN)** *August 18, 2018 18:44*

try this: [https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Cammo T** *August 19, 2018 10:09*

![images/6af58e9a8c76478a39735200b4be9b1c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6af58e9a8c76478a39735200b4be9b1c.jpeg)


---
**Cammo T** *August 19, 2018 10:10*

![images/0a091ab0ba1dffb15f3dae1131cf51af.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0a091ab0ba1dffb15f3dae1131cf51af.jpeg)


---
**Cammo T** *August 19, 2018 10:13*

Thanks for the fast replies.  The video was also very informative.  I went back and checked all options in lightburn and they were as per the video.  I have uploaded photo's of the cohesion mini and end stop as requested.



Does everything look like where it should be?  


---
**Joe Alexander** *August 20, 2018 10:26*

Is the entire gantry banging or is it just the laser head itself? That will tell us which endstop is not functioning properly. Also I see only one mechanical endstop, I'm guessing that your also using the optical endstop for the Y axis? If so make sure the little arm that "breaks the beam" is aligned with the gap in the sensor. (I avoided that by replacing the ribbon cable itself with fresh stepper wires and a mechanical endstop.)


---
**Cammo T** *August 24, 2018 22:40*

Just the laser head head banging against it.  The reason for installing the mechanical end stops was due to the optical end stops not seem to be working.


---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 22:48*

if im not mistaken the x endstops are looking for x min. you have your endstop switch going to x max


---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 22:51*

![images/27924ab1515a7ad20548bbd30e349f2a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/27924ab1515a7ad20548bbd30e349f2a.jpeg)


---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 22:57*

and the Y endstop need to be connected to the y-min header next to the x-min i circled.




---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 23:05*

disregard about the y endstop. its in the ribbon and should be okay. just move the x endstop wires to the x-min header on the signal and gnd pins (be careful as there is a 5v pin there too and it is not to be used). Then run m119 in the console to test the switches.




---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 23:21*

here is more on m119: [http://smoothieware.org/endstops#testing](http://smoothieware.org/endstops#testing)


---
**Cammo T** *August 27, 2018 03:26*

Thanks again Tech Bravo for all your help.

Still no luck.  When trying to test I can't get the laser cutter to connect to Pronterface via usb.  Says it is offline even though I am connect and can move the laser head in Lightburn.

![images/ce675db98250f1c62eeaee360ede092c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ce675db98250f1c62eeaee360ede092c.jpeg)


---
**Tech Bravo (Tech BravoTN)** *August 27, 2018 03:38*

use the console within lightburn. you can execute commands there


---
**Tech Bravo (Tech BravoTN)** *August 27, 2018 03:41*

more on the lightburn console: [github.com - Documentation](https://github.com/LightBurnSoftware/Documentation/blob/master/MachineInstructions.md#g-code-console)


---
**Cammo T** *August 28, 2018 03:12*

Thanks again Tech Bravo.  The plot thickens.  It does not seem to be detecting the end stops.  Ran the test with and with out holding the switch in.  Still shows up as 0.  I have tested the mechanical stops via multimeter and they seem to be working fine.  So it must be something else. 

![images/e6a134c6e28bda3ca4a9a5c552a13edc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e6a134c6e28bda3ca4a9a5c552a13edc.jpeg)


---
**Cammo T** *September 02, 2018 00:46*

Any other ideas guys?  Do you need to unplug the optical sensors when changing to mechanical?  


---
**Don Kleinschnitz Jr.** *September 02, 2018 12:57*

**+Cammo T** I do not know your setup but if you have both connected to the C3D's input remove one? 

How are they connected?


---
*Imported from [Google+](https://plus.google.com/100980986272635435257/posts/gc68RjwnpqT) &mdash; content and formatting may not be reliable*
