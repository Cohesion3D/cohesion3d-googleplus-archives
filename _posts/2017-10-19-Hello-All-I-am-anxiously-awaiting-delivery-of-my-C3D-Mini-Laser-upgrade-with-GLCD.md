---
layout: post
title: "Hello All, I am anxiously awaiting delivery of my C3D Mini Laser upgrade with GLCD"
date: October 19, 2017 22:15
category: "C3D Mini Support"
author: "Alex Raguini"
---
Hello All,



I am anxiously awaiting delivery of my C3D Mini Laser upgrade with GLCD.  While I'm waiting, I'm designing a new control panel.



Does anyone have the mounting specs or diagram for the GLCD or a link here?  I've done a search but have not found what I'm looking for.



Thanks





**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 22:18*

It's this thing but red: [reprap.org - RepRapDiscount Full Graphic Smart Controller - RepRapWiki](http://reprap.org/wiki/RepRapDiscount_Full_Graphic_Smart_Controller)



There's a gajillion case designs for 3d printing on thingiverse, and a bunch more panel designs by previous upgraders, who have already designed panels, including one from **+Ashley M. Kirchner**, I just for the life of me can't seem to get all the files together into one place. 


---
**Alex Raguini** *October 19, 2017 22:34*

Understood.  Found the answer I was looking for.  Thank you for the reply.


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/3niqSX5i2F5) &mdash; content and formatting may not be reliable*
