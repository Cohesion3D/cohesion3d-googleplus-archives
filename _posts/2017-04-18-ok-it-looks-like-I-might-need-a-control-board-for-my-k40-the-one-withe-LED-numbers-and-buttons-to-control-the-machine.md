---
layout: post
title: "ok it looks like I might need a control board for my k40 (the one withe LED numbers and buttons to control the machine"
date: April 18, 2017 16:02
category: "C3D Mini Support"
author: "Maker Fixes"
---
ok it looks like I might need a control board  for my k40 (the one withe LED numbers and buttons to control the machine. Do you know of an after market one that works with the mini and don/t cost an arm and a leg ? I know about light objects DSP its pretty pricey since I have yet to create a single item for profit 







**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 16:04*

The Cohesion3D Mini is a control board... it lets you run the machine with the LaserWeb software...

What is it you are looking to do? 


---
**Maker Fixes** *April 18, 2017 16:27*

The board that seems to bad on my k40 is the one with all the buttons and led readout, if I understand yours will replace the board that has the USB plug on it


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 16:29*

Yes, the Mini replaces the board with the USB. 

You keep the digital panel with the number readout. 


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/GQ9HugayyYY) &mdash; content and formatting may not be reliable*
