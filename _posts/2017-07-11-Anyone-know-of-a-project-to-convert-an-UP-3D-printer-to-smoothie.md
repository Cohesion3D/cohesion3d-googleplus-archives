---
layout: post
title: "Anyone know of a project to convert an UP 3D printer to smoothie???"
date: July 11, 2017 22:09
category: "3D Printers"
author: "Don Kleinschnitz Jr."
---
Anyone know of a project to convert an UP 3D printer to smoothie???





**"Don Kleinschnitz Jr."**

---
---
**Brad Hill** *July 12, 2017 00:35*

There is a drop in cpu that does it.


---
**Don Kleinschnitz Jr.** *July 12, 2017 01:48*

**+Brad Hill** can you point me to that project?


---
**Brad Hill** *July 12, 2017 01:49*

Search for cetus smoothieware


---
**Don Kleinschnitz Jr.** *July 12, 2017 01:59*

**+Brad Hill** thanks for the hint. I was hoping someone had done a UP conversion with a C3D :). Has anyone reverse engineered the electronics with schematics or the like?




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/R6E22xM8MSc) &mdash; content and formatting may not be reliable*
