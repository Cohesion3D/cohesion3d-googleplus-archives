---
layout: post
title: "Ashley M. Kirchner Jim Fong Who can advise me in setting the steps_per_mm for my servo drives in config.txt"
date: April 30, 2017 20:34
category: "3D Printers"
author: "Marc Pentenrieder"
---
**+Ashley M. Kirchner**​ **+Jim Fong**​

Who can advise me in setting the steps_per_mm for my servo drives in config.txt.

What would you do to synchronize 2 servo motors on one axis, both get signals from z axis output on the remix board, but the dont behave the same. After 5 to 6 moves the difference is about 5mm.

Thanks for any advice. If you need more information or photos of the cabinet, just tell me.





**"Marc Pentenrieder"**

---
---
**Ashley M. Kirchner [Norym]** *April 30, 2017 20:41*

Add encoders on them?


---
**Marc Pentenrieder** *April 30, 2017 20:48*

The servo motors have encoders on them, but how do i use them? I didn't know of a config option or connector to use on the remix board or in [config.txt](http://config.txt).


---
**Jim Fong** *April 30, 2017 21:10*

If you are feeding  the same step signal to both servo drivers, they should stay in sync. 

Unless....

One servo motor has different encoder resolution than the other.

The PID tuning of one drive is very much different than the other.  



What make model are the motors, drivers, encoder resolution?   


---
**Marc Pentenrieder** *April 30, 2017 21:33*

The servo motors all have the same encoder resolution. It is called "line-saving, capacity increasing 5000ppr". 

The motors are Dorna AC Servo Motor 130DNMB1-0001CKAM

The drivers/controllers are Dorna EPS-B1

The encoder resolution is 5000ppr (internally multiplied by 4, so 20000ppr)

Because of my ball screw is a 3210 with 10mm pitch per revolution, i set steps_per_mm to 2000. Is this right, or is my calculation faulty?

![images/bdde0bea408e3b5f5213bfef14107c5c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bdde0bea408e3b5f5213bfef14107c5c.jpeg)


---
**Jim Fong** *April 30, 2017 21:43*

2000 step//mm is correct for that combination of pitch and encoder 



Keep in mind that almost every modern servo driver has step input multiplication available. You need to check the setting on both drives are the same for that config value. 


---
**Jim Fong** *May 01, 2017 00:56*

You should rule out any mechanical problems.  With a good dial indicator check that both ballscrews are exactly the same pitch.  

Check for backlash in the ballnuts. and are similar on both

Check the angular contact bearings on the mounts are not loose in the housing.  




---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/Yz1Nfbvnh6Z) &mdash; content and formatting may not be reliable*
