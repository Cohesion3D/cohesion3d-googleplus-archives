---
layout: post
title: "I am having issues with my C3D board connection to the computer"
date: November 11, 2018 08:54
category: "C3D Mini Support"
author: "John Harris"
---
I am having issues with my C3D board connection to the computer. Randomly it loses connection to the computer and resets the laser to the home position. This happens at random times and can occur in the middle of scanning, cutting or when the laser is idle. I have run diagnostics on the computer and shutdown all non-essential services but the issue still is present. I get the sound of USB device connection and then the laser moves to home when it happens. Any ideas???



![images/c5d5f94ab9654de84bd75232c3b5683c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c5d5f94ab9654de84bd75232c3b5683c.jpeg)
![images/2aa5a065cfeb3bb7605928ee37e6d68a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2aa5a065cfeb3bb7605928ee37e6d68a.jpeg)

**"John Harris"**

---
---
**Joe Alexander** *November 11, 2018 09:13*

what kind of usb cable are you using? the cheap blue ones have had connection issues before, ideally use one with ferrite on both ends


---
**John Harris** *November 12, 2018 03:30*

**+Joe Alexander** I am using a cable with ferrite on both ends. Should I get one with foil and braided shielding as well as ferrite?


---
**Joe Alexander** *November 12, 2018 03:39*

you could try that but if u have ferrite on both ends  already it should be ok. doesn't hurt to try a different wire in my opinion, easy troubleshooting option.


---
**John Harris** *November 18, 2018 08:10*

Finally resolved


---
*Imported from [Google+](https://plus.google.com/117587099300180272958/posts/fBuHYBQLCqR) &mdash; content and formatting may not be reliable*
