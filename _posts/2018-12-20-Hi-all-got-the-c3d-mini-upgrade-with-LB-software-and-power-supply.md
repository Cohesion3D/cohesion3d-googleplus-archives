---
layout: post
title: "Hi all, got the c3d mini upgrade with LB software and power supply"
date: December 20, 2018 20:20
category: "C3D Mini Support"
author: "Dave Zerby"
---
Hi all, got the c3d mini upgrade with LB software and power supply. Done all the downloads, followed all directions. The laser will test fire if I push the button, the problem is when I turn on the machine the gantry and the laser head does not move. Any help would be greatly appreciated as the machine worked fine before I upgraded. Thanks



![images/0b267b3ef0ec903f21f1c245e00b5d59.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0b267b3ef0ec903f21f1c245e00b5d59.png)
![images/f3d860fdd2cabedd11e7576c83d29530.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f3d860fdd2cabedd11e7576c83d29530.jpeg)

**"Dave Zerby"**

---
---
**Chris Leitch** *December 20, 2018 20:34*

Would be better to ask on the Facebook group alone. This group will be closing down with G+ soon enough. 


---
**Joe Alexander** *December 20, 2018 21:28*

the C3D board doesn't home on powerup like the original control board if thats what your referencing. does it home with the controls in lightburn?


---
**Dave Zerby** *December 20, 2018 21:45*

I just watched the lb tutorial first time user, followed each step and nothing happened.


---
**Dave Zerby** *December 20, 2018 22:03*

No it does not move in lightburn


---
**Joe Alexander** *December 20, 2018 22:39*

in your pic of the board I see that you have 3 stepper chips installed and a external driver adapter, yet I only see wires going to the adapter with the x axis coming in on the ribbon cable. Is the wires going to the adapter for the y axis? drawing a quick schematic of your wiring could help us troubleshoot it for you.


---
**Dave Zerby** *December 21, 2018 00:48*

I only have 1, 4 prong wire connector for the motors and the ribbon cable which looks like it splits and goes to both motors.

![images/5d7de110bf22d2c1c5e594d71f8af333.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5d7de110bf22d2c1c5e594d71f8af333.jpeg)


---
**Joe Alexander** *December 21, 2018 00:54*

yea that would be the wires for the x axis motor(the one on the left side of the gantry), then im guessing the 4 wires going into that adapter are for the y-axis? this motor would be located on the front right edge of the rails with a connecting rod that drives a belt on both sides of the X-Y frame. if so that motor doesnt need the adapter it needs to be plugged in to a stepper driver chip on the 4 pins next to each chip respectively(in your first pic they would be on the bottom edge of the board.)


---
**Chris Leitch** *December 22, 2018 09:35*

This issue was solved in the Facebook group. Wiring issues plus corrupted firmware I believe. Just thought I would leave an update to let anyone else having similar issues know. 


---
**Dave Zerby** *December 22, 2018 19:02*

resolved by a very, very, patient and helping person ( should be "sainted") that also makes some very nice pens and pencils.


---
*Imported from [Google+](https://plus.google.com/112460708396846121818/posts/FGBAgpv9VeL) &mdash; content and formatting may not be reliable*
