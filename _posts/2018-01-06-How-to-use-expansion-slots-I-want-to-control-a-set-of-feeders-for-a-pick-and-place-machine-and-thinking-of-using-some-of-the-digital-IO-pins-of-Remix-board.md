---
layout: post
title: "How to use expansion slots I want to control a set of feeders (for a pick and place machine) and thinking of using some of the digital IO pins of Remix board"
date: January 06, 2018 13:37
category: "General Discussion"
author: "Manusha Wijekoon"
---
How to use expansion slots



I want to control a set of feeders (for a pick and place machine) and thinking of using some of the digital IO pins of Remix board. Does this need changes to firmware? 





**"Manusha Wijekoon"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 07, 2018 00:29*

No, look at smoothieware switch documentation page. Just add a switch block to config.txt and reference the pin from the ReMix pinout diagram. 


---
**Manusha Wijekoon** *January 07, 2018 16:19*

ok great, I will take a look. Cheers.




---
*Imported from [Google+](https://plus.google.com/+ManushaWijekoon/posts/KVNZMk1tSGY) &mdash; content and formatting may not be reliable*
