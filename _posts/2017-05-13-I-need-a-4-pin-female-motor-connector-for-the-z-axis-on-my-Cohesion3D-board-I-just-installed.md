---
layout: post
title: "I need a 4 pin female motor connector for the z axis on my Cohesion3D board I just installed"
date: May 13, 2017 00:32
category: "General Discussion"
author: "Steve Clark"
---
I need a 4 pin female motor connector for the z axis on my Cohesion3D board I just installed. Does anyone know the size callout and where to get them?





**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 00:34*

DuPont or Molex KK. 2.54mm pitch. 


---
**Griffin Paquette** *May 13, 2017 00:35*

^


---
**Steve Clark** *May 13, 2017 00:41*

Thanks, I didn't want to get the wrong one.


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/KAKYXsMLH1Z) &mdash; content and formatting may not be reliable*
