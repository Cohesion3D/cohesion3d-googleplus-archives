---
layout: post
title: "That AZSMZ 1286 LCD I posted about earlier works great"
date: March 13, 2018 23:21
category: "General Discussion"
author: "Jake B"
---
That AZSMZ 1286 LCD I posted about earlier works great.  I just need to build a better wiring harness. 



I like the size and viewing angle better than the Budget RepRap display.  Just needed some minor configuration changes and an extra signal for A0.



![images/1cdad82cdba33c184580957d7b6bcb8e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1cdad82cdba33c184580957d7b6bcb8e.jpeg)



**"Jake B"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2018 23:24*

I know what A0 is, are you short a pin for all the stuff from just the glcd header? 

Did you have to modify the config file? 


---
**Jake B** *March 13, 2018 23:30*

It uses the full LCD expansion header (Not using the LCD adapter, because it added another level of mapping pins that I didn't care to figure out.). Only MISO is left unconnected.



using 0.28 for A0.  panel.lcd = viki2 as per the listing on aliexpress.



Here is my config:

panel.lcd                                   viki2     # as per listing

panel.spi_channel                           0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                            0.16              # spi chip select     ; GLCD EXP1 Pin 4

panel.encoder_a_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 5

panel.click_button_pin                      1.30!^            # click button        ; GLCD EXP1 Pin 2

panel.buzz_pin                              0.27o             # pin for buzzer      ; GLCD EXP1 Pin 1

panel.back_button_pin                       2.11!^            # back button         ; GLCD EXP2 Pin 8

panel.a0_pin                                0.28              # st7565 needs an a0

panel.encoder_resolution		    4




---
**Jake B** *March 13, 2018 23:41*

side note, I reversed the encoder lines because I'm civilized and like clockwise = forward and counterclockwise = backwards. :)


---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2018 23:42*

Also possible to do that by swapping enc A and B pins 


---
**Jake B** *March 13, 2018 23:48*

Yeah, but this way also works with the budget LCD and the adapter with a fixed ribbon cable.   



Another side note, this LCD has a place for an ESP-01 8266 module.  This appears to just expose RX/TX serial lines on one of the connectors.  Probably could jump these to the serial header, but I don't know what kind of software support you'd need on the smoothie side. (probably a config change at best). Also I'm not sure what firmware the ESP-01 needs, my guess is a telnet-to-serial type thing with an AT command set.  I haven't really researched it.


---
**Alex** *April 20, 2018 19:53*

**+Jake B** Hi Jake, do you have full pinout from controller to display, i'm try connect use your config.... and nothing ((((


---
**Alex** *April 20, 2018 19:58*

![images/7bae9040f1433216163ce0edd49263c5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7bae9040f1433216163ce0edd49263c5.jpeg)


---
**Alex** *April 20, 2018 20:44*

**+Jake B** Jake, i'm try connect but nothing, may be you can help to me

![images/159dfdd2bf8770991153511841ff5b4a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/159dfdd2bf8770991153511841ff5b4a.jpeg)


---
**Jake B** *April 20, 2018 21:41*

**+Алекс** note the config file changes in the post above.  Need to change to a viki2 type LCD and also map an panel.a0_pin, which is not needed by the budget glcd.


---
**Jake B** *April 20, 2018 21:47*

Also looks like you don't have SCK hooked up.


---
**Jake B** *April 20, 2018 21:53*

@Алекс I don't have MISO hooked up because I don't plan to use the SD card slot. (Also no enough pins for both chip selects). But definitely need SCK


---
**Alex** *April 20, 2018 22:05*

**+Jake B** Yeaaaa ... I have forgotten, i'm donkey )))) one head well, but two heads is better )))) Thanks Jake!!

![images/4782119bcb2e6398babfac13f8acb55e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4782119bcb2e6398babfac13f8acb55e.jpeg)


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/4ZrrAG2U6UA) &mdash; content and formatting may not be reliable*
