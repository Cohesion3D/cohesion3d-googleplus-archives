---
layout: post
title: "Hello everybody Ive updated my large k40 with 3D cohesion mini board and Lightburn, and the machine is running now way faster and smoother than xith the M2 nano board"
date: March 16, 2018 09:11
category: "General Discussion"
author: "Vincent Bernolin"
---
Hello everybody 



I’ve updated my large k40 with 3D cohesion mini board and Lightburn, and the machine is running now way faster and smoother than xith the M2 nano board.

However  I experiment the same issues with horizontal lines, as you can see on the pics. It seems to be speed related. The word on the pic was engraved at 50mm/sec speed. Only horizontal lines are affected. No problem on the curves, iI suspect a mechanical issue on too fast direction changes. Anybody in the same case? 



Best regards 



![images/e2257d05f839478195cae66b4c3edcef.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e2257d05f839478195cae66b4c3edcef.jpeg)
![images/cb731262bd3cb8209be11d43b6270894.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cb731262bd3cb8209be11d43b6270894.jpeg)

**"Vincent Bernolin"**

---
---
**Vincent Bernolin** *March 16, 2018 09:33*

Indeed I think I understand. In cutting mode it is advisable to not go faster than 10 mm/sec if the path has square angles. If all is radiused,  the cutting speed can be much rised. I used the cutting mode to engrave which is not the way it should on such light machines. 

Any other idea/comment welcome


---
**Jake B** *March 16, 2018 10:51*

3D printers have this same issue: 



[groups.google.com - Google Groups](https://groups.google.com/d/msg/makerbot/yxOZdPjS5aw/KIaOFxqIaBIJ)



I in in the camp where it is the g-code generator (the slicer in the case of a 3d printer, Lightburn here) to do the processing necessary to identify tight radius curves and slow down the tool feed rate.  Others seems to feel the issue can be tweaked with acceleration settings.  I disagree, because acceleration settings  only apply to a single line segment.  Curves are made up of many small segments, hence the issue.


---
**Vincent Bernolin** *March 16, 2018 11:21*

Yes, an optimisation of the feedrate and acc dec could be made, and it is the case on high speed machining centers, which controllers are able to manage these values. But low priced softwares as Lightburn are not supposed to do this. 

Indeed it is obviously related to the rigidity of the machine : as the rigidity is far better in the left-right direction,  X acis positionning is more precise and thus , vertical lines are better. 

I’ll try to adjust the acc-dec values on the Y axis, and would not be surprised to see an improvement 


---
**Claudio Prezzi** *March 16, 2018 12:12*

I belief this is related to mechanical problems. Check that your belts and pulleys are tight.


---
**Claudio Prezzi** *March 16, 2018 12:15*

On my K40 the pulley on the left side was not running round because the axle was bent.


---
**Jake B** *March 16, 2018 12:32*

**+Vincent Bernolin** agreed, that ideally the CAM (lightburn) package shouldn't have to worry about this.  However we're talking about underpowered motion controllers, running on commodity microcontrollers.  They don't have the speed/memory to look far enough ahead, detect curves, and dynamically adjust the speed.  I'm sure someday we'll have constant velocity motion control.  I'm just not aware of any controllers that do it yet (short of something like Mach3/4 running on a PC)


---
**Jake B** *March 16, 2018 12:47*

Side note, I don't see this as a deficiency of LightBurn or other CAM packages like Simplify3D for 3D printing.  They <b>shouldn't</b> have to worry about this. Even still, the end user of these hobbiest-quality machines are left with only one option, reducing the overall speed of the job when it contains tight curves.  It would be a nice thing if the CAM software could accommodate less capable motion planners.


---
**Claudio Prezzi** *March 16, 2018 13:08*

**+Jake B** The Cohesion board is not so limited as you descibe. It can handle vector stuff like that with more than 100mm/sec without any overswing. The firmware cares about speed increase/decrease in corners and even adjusts the laser power during acceleration/decelleration, to create a nearly equal burn. Smoothieware by default has a look ahead buffer of 32 moves. That's enough for vector cutting, at least if you don't generate 0.1mm line segemnts.


---
**Anthony Bolgar** *March 16, 2018 13:10*

I am with Claudio on this one, check belts, pulleys etc. I regularly vector engrave at 50mm/s without issues using a C3D mini and Lightburn on my K40 and 50 and 60 Watt lasers.




---
**Jake B** *March 16, 2018 13:14*

Fair enough.  Occam's razor also says its probably mechanical.  32 moves isn't really that much.  Mach 3 is defaults to 200 moves, for example.  But I guess you're right, for vector laser work you don't need to generate huge numbers of small segments.  I yield.


---
**Jim Fong** *March 16, 2018 14:01*

I get this too on my well used K40.  X-axis belt/wheels is a little worn in and could be tighter. May need to replace soon.



It is also the lightest axis moving. The fast change of direction will make the laser head vibrate. Make sure your white roller wheels are tight to the gantry to keep it from vibrating. 



Lowering the acceleration helps too but I mostly do raster scans so I like to keep it at 2500.  The lower acceleration helps to ease the laser head in and out of the corners. Instead I just run vector cutting at 20mm/sec or less. On my worn out machine, that seems to work pretty good. 






---
**Jim Fong** *March 16, 2018 15:26*

**+Jake B**  Mach3 actually has a poor trajectory planner but you have to keep it in perspective though.  It was written many years ago based on EMC code.  EMC is now known as LinuxCNC.   This was long before reprap, grbl, smoothieware etc.  



Im a long time Mach3 user. It still works great for low speed milling. I use it on 2 mills cutting aluminum, where speed is relatively slow.  The trajectory planner/constant velocity implementation has some major bugs when using it on a higher speed machine.  Known issue but will not ever be fixed.  The original author, Art Fenerty, sold the program when he retired.  New owners deemed it to difficult to fix (also too many pirate versions) and decided a ground up new rewrite was required.  They called it Mach4.  The last time a Mach3 update was released is over 5 years ago.  You can still buy a Mach3 license but you have to put up with its many bugs that come with it. Highly not recommended when much better gcode controller programs are now available.  LinuxCNC new trajectory planner is superior to Mach3 and is free.  Grbl, smoothieware, marlin etc trajectory planners are considered better than old Mach3.  UCCNC is Great and is what I use on my larger gantry router for high speed v-carving. 



Many consider Mach4 “not ready yet”.  A lot of the long time Mach3 users have switched to something else, many to UCCNC.  


---
**Claudio Prezzi** *March 16, 2018 15:39*

**+Jim Fong** You wrote something that remembered me of a problem I had. My mirror on the y axis was a bit loose and wobbled on speed changes ;)


---
**Claudio Prezzi** *March 16, 2018 16:02*

**+Jake B** You cannot compare Mach3/4 with a controller like the Cohesion board with Smoothieware! 



In case of a controller with firmware like Smoothieware, the controller only gets gcode commands. The gcode is then parsed in the firmware, then moves are planned (splitted into accelleration, constant and decelleration parts) and written into a queue. A "parallel" process executes the queue and generates the stepper pulses in "realtime".

 

Mach3/4 does all that on the PC and doesn't need an intelligent controller, but insted, Mach3/4 must calculate all stepper pulses on the PC and send them directly to the external stepper drivers (but Windows is not good at realtime tasks!).


---
**Jake B** *March 16, 2018 16:11*

+ClaudioPrezzi yes, I think we're saying the same thing.  My point is that if the developers of CAM packages were inclined, they could preprocess the accelerations and generate G-code specifically crafted to run on "dumb" controllers that have limited look-ahead ability.  At g-code generation time, you have all of the processing power you need to figure things out for the motion controllers that are less capable.


---
**Jim Fong** *March 16, 2018 16:13*

**+Claudio Prezzi** I haven’t  adjusted my mirrors in a very long time. It wouldn’t surprise me if it is loose on my machine too.  


---
**Vincent Bernolin** *March 16, 2018 22:13*

Thanks everybody for interesting comments. I didn’t check the looseness of the axis just now, but suspect it is quite in good condition. However, reducing the acc from 3000 to 1000 improved significantly the result at the same speed, please see pic attached

![images/75cff04bd441c9290a4c4ff2fcf4f860.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/75cff04bd441c9290a4c4ff2fcf4f860.jpeg)


---
**Vincent Bernolin** *March 16, 2018 22:15*

With reduced acc

![images/11879bd3cc2059e43f48f75d85b745a4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11879bd3cc2059e43f48f75d85b745a4.jpeg)


---
**Vincent Bernolin** *March 16, 2018 22:16*

But I was not able to reduce only one axis, the 2 seem to share the same parameter


---
**Vincent Bernolin** *March 16, 2018 22:19*

On large moves, I consider the gain on the feedrate speed to positively overbalance the loss on acc-dec, considering vectorial drawings. 

One other option , easy to implement on graphics, is to use radius corners, for example with the offset function


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2018 22:33*

If you talk to **+Joe Spanier** or **+Carl Fisher** the latest/ more recent smoothie firmware will allow individual acceleration per axis.  But it will require to rebuild the config file from their latest template because some syntax changed since the one I provide. 


---
**LightBurn Software** *March 16, 2018 22:53*

That’s physical bounce along the Y. The Y axis carries the most mass, so when cornering from a Y move to an X move the axis is vibrating and you’re seeing that in your output. I notice that your axis is offset (lifted) from the drive rails - make sure the connection between the  X rail and the Y bearings is solid and stiff, on both sides and you can’t easily flex the X rail with the machine powered. If the problem is worse in the middle of the X than on the sides, the material chosen for the X rail may not be stiff enough.


---
**LightBurn Software** *March 16, 2018 22:56*

Also, if I’m not mistaken, the latest Smoothieware has separate controls for X and Y acceleration (called alpha and beta acceleration in the config).  Lowering just your Y accel will help fix this without affecting your raster scanning speed.


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2018 23:08*

Yep, I covered that above :)  Needs a rebuilt config file though, they changed some syntax. 


---
**LightBurn Software** *March 16, 2018 23:09*

Missed that - apologies.  :)


---
**Jim Fong** *March 17, 2018 01:24*

50 mm/sec speed

Both x&y 3000mm/sec^2 accel![images/e1271b5f24377f641a9092dc1eadabe5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e1271b5f24377f641a9092dc1eadabe5.jpeg)


---
**Jim Fong** *March 17, 2018 01:28*

20 mm/sec speed

Both x&y 3000mm/sec^2 accel



I find lowering feed better looking than reducing y acceleration. ![images/4ede5bfc36aba00f28104ade6bdc322d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4ede5bfc36aba00f28104ade6bdc322d.jpeg)


---
**Jim Fong** *March 17, 2018 03:39*

50mm/sec speed

X 3000mm/sec^2

Y 500mm/sec^2



Using new firmware with individual axis acceleration. Less horizontal ripples.![images/1c16cd03afb9e2236ab1777be315d933.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1c16cd03afb9e2236ab1777be315d933.jpeg)


---
**Vincent Bernolin** *March 17, 2018 06:24*

**+LightBurn Software** 

Probably exactly what you describe on this light machine ! 


---
**Vincent Bernolin** *March 17, 2018 06:25*

**+Jim Fong** Thanks for these informations !


---
**Vincent Bernolin** *March 17, 2018 06:28*

**+Ray Kholodovsky**Nice update, the separate X an Y acc looks like a very good solution to improve the quality of the vectorial cuts without sacricifing the engraving speed


---
*Imported from [Google+](https://plus.google.com/109023396074615225526/posts/ct74GR26EH8) &mdash; content and formatting may not be reliable*
