---
layout: post
title: "Hi all, I have converted my K40 to a much larger (and better) machine over the last eight months"
date: September 01, 2018 13:24
category: "K40 and other Lasers"
author: "Frederik Deryckere"
---
Hi all,



I have converted my K40 to a much larger (and better) machine over the last eight months. It is called the LAYZOR.



All the details about what I ended up with are on my website:

[https://manmademayhem.com/layzor/](https://manmademayhem.com/layzor/)



That site also links to my blog on which I documented the whole conversion process for those who are interested.



A full set of plans for the conversion is now also available for sale.

The next upgrade is switching the stock Nano crap with a C3D board, which is currently on its way to me. Stoked!



Greetings,

Frederico





**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *September 01, 2018 14:33*

Very Nice Work...


---
**Kelly Burns** *September 02, 2018 14:10*

Man, I was very happy with my conversion.  Now I feel so inferior!


---
**Frederik Deryckere** *September 02, 2018 14:28*

I'll take that as a compliment ;-) Hey as long as it works... anyway, thanks!


---
**Kelly Burns** *September 02, 2018 18:37*

**+Frederik Deryckere** It was certainly meant that way.  Great Work.  


---
**Ciaran Whelan** *September 03, 2018 01:08*

Fantastic work, the end result certainly shows the good attention to detail. 



My query on the K40 upgrade like you did. Can the CO2 tube be used if I go down the path of buying the plans, but want to go 1000mm long on the X axis?



Most of what I will be doing is 980mm sheets of 3mm ply and balsa. If so, then this certainly looks like a winner. 


---
**Joe Alexander** *September 03, 2018 06:15*

the Co2 tube can go in any machine you build as long as there is room for the length of the tube. but with more area you can size the tube up to something more powerful(higher wattage tube=longer tube overall) if you have room for a bigger tube it might be ideal to up the wattage for faster jobs.


---
**Ciaran Whelan** *September 03, 2018 06:19*

**+Joe Alexander** thank you for clarity on this.  I am used to using a diode laser of 5.6w. So looking at an upgrade in the next month or two to buy a K40 and then mod to be similar to this build,  but slightly longer. I think a 40w CO2 laser should be quicker already to what I have.  


---
**Frederik Deryckere** *September 03, 2018 11:55*

Hey,

the laser tube will work on any size you want the work area to be. It just makes aligning the mirrors a bit more difficult because the distances are larger. 

That said, the plans are for 600x400 so while extending surely is possible, you'll need to work the specifics out on your own. But the plans will be a very good base to work off.



As far as wattage is concerned, Joe is right. If you're going to extend it anyway, looking into a more powerful tube is certainly worth considering. Up to 60W (50-55mm diameter tube) should be pretty straightforward. From 80W upwards, tubes tend to be 80mm in diameter and that would require even more heavy modding.



I suggest you download the CAD file if you can and check how things are built so you can make up your mind.



How big are the sheets you want tu cut in total? There is a feed-through slot so maybe you can rotate your work 90° and use tiling?


---
**Ciaran Whelan** *September 07, 2018 04:55*

**+Frederik Deryckere** I have been looking over the CAD file and BOM as well as thoroughly going over your build log on the blog again. My summary would be from your last post in that there is not much of the K40 left that was used. 



In saying that, I do not own a K40. Would this be silly to buy a new K40 to build the LAYZOR? My concluding thoughts are I could save some cash by simply buying the parts only that will be needed. Your build log is fairly concise, so I see nothing that will be too technical. Would you say this is probably a better decision if I don't already own a K40?


---
**Frederik Deryckere** *September 07, 2018 10:55*

If you are a resourceful man, I say go for it! There will be some details here and there that will need creative problemsolving, but nothing major as far as I can tell. I suggest buying a quality tube and PSU from the start. I don't think you'll end up saving money, but you'll surely benefit from having quality electronics in the long run! Oh yeah and install a C3D right away and buy a Lightburn license. You won't regret it.




---
**Ciaran Whelan** *September 07, 2018 11:25*

I have Lightburn, for now I have a MKS Gen L board to take care of motion.  I will get a C3D for sure. I only need the C3D mini dont I? 



As for resourceful.. yes 100%


---
**Ciaran Whelan** *September 07, 2018 11:26*

At recommendations on tube and PSU? I was thinking 40W is plenty for what I do. Generally my work will be cutting 3mm ply and different thicknesses of balsa sheet. 


---
**Frederik Deryckere** *September 07, 2018 14:11*

40W is the only tube that will fit inside the case without modification, unless you use an external chiller, then you could go up to 50W by using that space. Higher wattage tubes are generally longer than 1000mm and/or thicker than 55mm.



For C3D, they sell kits if i'm not mistaken with everything you need and some optional stuff. But you better ask the seller he'll help you out I'm sure.



Good luck!


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/SXEXgvD73xV) &mdash; content and formatting may not be reliable*
