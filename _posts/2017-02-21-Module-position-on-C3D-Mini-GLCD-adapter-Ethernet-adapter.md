---
layout: post
title: "Module position on C3D Mini GLCD adapter Ethernet adapter"
date: February 21, 2017 13:53
category: "C3D Mini Support"
author: "Stephane Buisson"
---
Module position on C3D Mini



GLCD adapter

Ethernet adapter



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570)



![images/222a0e1dca4347527e30ff39d217c196.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/222a0e1dca4347527e30ff39d217c196.jpeg)
![images/2b5385a8fcb26b2635924b338fe79c96.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2b5385a8fcb26b2635924b338fe79c96.jpeg)

**"Stephane Buisson"**

---
---
**Jonathan Davis (Leo Lion)** *February 21, 2017 13:54*

I would say a right angle connection would be more effective, in terms of saving space. 


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 13:57*

Yes, both are correctly positioned. 


---
**Stephane Buisson** *February 21, 2017 13:59*

thank you  **+Ray Kholodovsky** for this confirmation


---
**James Rivera** *February 21, 2017 18:30*

Good to know. I still haven't even opened the box.  :-(


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/Av6hDD1vyJa) &mdash; content and formatting may not be reliable*
