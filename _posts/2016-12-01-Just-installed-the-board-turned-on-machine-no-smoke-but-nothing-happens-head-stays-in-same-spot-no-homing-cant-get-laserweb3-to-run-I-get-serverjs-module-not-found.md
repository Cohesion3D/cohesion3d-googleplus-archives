---
layout: post
title: "Just installed the board - turned on machine - no smoke - but nothing happens - head stays in same spot - no homing - can't get laserweb3 to run, I get server.js module not found"
date: December 01, 2016 18:05
category: "General Discussion"
author: "greg greene"
---
Just installed the board - turned on machine - no smoke - but nothing happens - head stays in same spot - no homing - can't get laserweb3 to run, I get server.js module not found





**"greg greene"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 18:09*

Thanks for the additional info. First point: c3d board will not auto home on power up. A bunch of people expect it to, but we don't do that. 

Sounds like you need to get LaserWeb sorted first. Can you provide more details about your installation and what steps you are doing now? Screenshots help. 


---
**greg greene** *December 01, 2016 18:13*

Not good on the screenshot thing for some reason



I followed the directions on the install page



get comand prompt

cd \laserweb3

Laserweb3: node server.js

error cannot find server.js


---
**greg greene** *December 01, 2016 18:15*

Tried that url all I got back was preview not available - I mustbe missing some app?


---
**greg greene** *December 01, 2016 18:22*

OK, laserweb is open -thanks for that - clicked on the connect button - no joy


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 18:23*

Greg can we see a screenshot of LaserWeb? Can you confirm the options available at the top bar regarding com port, speed, etc? 



Also have we already dealt with getting your MicroSd card set up and into the board? 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 18:25*

Thanks for the help Peter. I'll take it from here. 


---
**greg greene** *December 01, 2016 18:31*

Sorry to be such a pia folks - haven't used unix like commands since I sold my PDP-11 back in the last century and I don't have the road map oh how it works in my mind yet


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 18:32*

No problem at all Greg. Feel free to ask any questions. 

So... Microsd card with config.txt file is in the board? Flashing green lights on board when you power up? 

Com ports and stuff on the top bar of LaserWeb? Screenshot of that? 


---
**greg greene** *December 01, 2016 18:36*

card yes

lights yes

says com port opened

also says not connected and machine control disabled


---
**greg greene** *December 01, 2016 18:55*

I got a screen print file - but I can't seem to get it to attach to a reply,

is here another driver I need for win 8 to connect?


---
**greg greene** *December 01, 2016 18:58*

email sent Ray with screenshot


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:00*

Yes, there may be a driver required for Win8.  

First, can you enter 1 2 3 4 for the machine pin, to get that out of the way.

Then can you try jogging around.

You can find the driver link at the top of this page, but since you are already in LW please try jogging around first.  [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)


---
**greg greene** *December 01, 2016 19:06*

no joy on the jog, machine pin entry boxes have disappeared


---
**Ashley M. Kirchner [Norym]** *December 01, 2016 19:07*

Poor Peter. I feel his pain. Occasionally we just need to step aside for a moment. But, we are all here to help one another, as Ray is doing.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:08*

Yes, the machine entry pin should disappear.  That's fine.  

Ok, please disconnect (press the red x next to green opened com 3), disconnect the board from USB, and install the drivers from the previous link.


---
**greg greene** *December 01, 2016 19:13*

Drivers installed fine, selected usb, com 3, connect - says it opened com3 - and



not connected


---
**greg greene** *December 01, 2016 19:16*

Is the baud rate of 115K correct for your board?


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:16*

Yes.


---
**greg greene** *December 01, 2016 19:18*

ok - well still no connection then, so until I get that resolved - can't expect much else to work :)

One thing though, when I start up laserweb it says it can't find the config file and asks me to go to settings to do the setup, I've dont that and saved it - the saved parameters do reappear correctly - so it is getting saved somewhere.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:20*

Greg, can you please go into Start Menu --> Devices and Printers.  You should see an entry labelled Smoothieboard, and if you double click and go to the hardware tab can you confirm that you see something similar and that the COM Port matches up? 

![images/1e2eaf13e4bfbcf107a24e78d8c31d32.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e2eaf13e4bfbcf107a24e78d8c31d32.png)


---
**greg greene** *December 01, 2016 19:34*

I saw the log file peter - Thanks for that, the com port is labelled smoothie - but it has the dreaded yellow exclamation mark on it - so it isn't working


---
**greg greene** *December 01, 2016 19:35*

win 8.0


---
**greg greene** *December 01, 2016 19:37*

Error on driver installation



a service information on inf is invalid


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:39*

Greg, I say this kindly: that type of limited information tells us nothing.  Please send a screenshot of everything that happens.  

If error on installing driver, unplug everything, restart the computer, and try again.


---
**greg greene** *December 01, 2016 19:40*

Ok got the driver working - com6 but that is not an option on Laserweb


---
**greg greene** *December 01, 2016 19:41*

understood Ray - I've been there !  Let me see what I can do


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:41*

press the little spinny icon next to the red x to update the list.


---
**greg greene** *December 01, 2016 20:37*

got it connected on com6 - no response to jog requests


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 20:42*

Greg can you look at the board and confirm for me that 2 green LEDs are indeed blinking?

Can you also see a red LED on the board?  Again pictures would help please: of the board and wiring going to the board and a screenshot of LW right now.


---
**greg greene** *December 01, 2016 20:44*

no blinking leds - no red led will email screenshot of LW


---
**greg greene** *December 01, 2016 20:46*

email sent


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 20:46*

Well, no red LED means your power is not on/ not going to the board. So that would certainly do it.  

If you all your green LEDs are solid then you've frozen the board and you need to either reset it using the small button in the top right (near the mounting hole).  

Again, I need pictures of the board and how you've wired the board to provide any further assistance.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 21:09*

Greg I got your board pics. Now you need to see my above post and answer my questions please. 


---
**Ashley M. Kirchner [Norym]** *December 02, 2016 06:07*

I get what you're saying and where you're coming from. I've been in the IT business for way longer than I'm willing to admit, and I've seen and heard it all from users. What I saw from you today was pure frustration, and I get it, it hits me too at times. So please don't take what I'm about to say as anything personal or directed at you. It's not. We've all been there.



Over time I learned to come down to a user's level (my old boss used to say, 'Please come down to my level of stupid.') It doesn't matter who I am in relation to the user, if a user can't get something to work, no matter how simple, I still need to come down to their level and understand that just because <b>I</b> know the problem or how to solve it, that does not automatically translate to them knowing it too nor having the ability to figure it out, even with my verbal instructions.



It's not an easy task, to help others. And we often don't get even the littlest of appreciation nor praise for what we do. But we do it anyway. Yes I get frustrated too, sometimes to the point where I just want to hang up the phone, or walk away from the person. But in the end, that accomplishes nothing. So we continue to try.



When it's time to walk away, I ask the user to let me think it over and find out what the problem if, even if I already know it's between the operator and the keyboard. But by doing that, I give myself time to cool off, and it gives the user the satisfaction that I will at least try to do something for them. If it's someone at my office, I simply offer the person to go to lunch, early if they need to. Let's tackle it when you get back. For my phone clients, after hanging up, usually I follow it up with an e-mail, detailing what needs to happen based on what I've discovered while talking to them. And I will let them try the steps outlined in my e-mail and when they get stuck, give me a call again.



I like to give folks options when I start getting frustrated with them. It allows me to step away from the issue, or said person, while still able to help them out.



It's not easy doing that. All we can do is try. You were trying, I saw that. Things just got a tad frustrating there and I'm glad you decided to step away for a bit and let others step in.



Chin up, no harm, no foul, no one died, and hopefully by now he's gotten it to work with Ray's help. Tomorrow's another day, the battle continues.


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2016 06:15*

We've gotten to the point that Greg can jog the machine (apparently the power issue was a loose pin in the power connector which has since been fixed). The issue right now seems to be that his endstops are not being read - as evidenced by the fact that the axes grind at the end when he does a homing. 



I do a full functional test on all the boards before shipping so I'm quite confident that it's not a board issue. That said, I'm trying to figure out the best way to help diagnose the issue.  



Usually I'd do an M119 and trigger and read the endstops, is there a way we can replicate this in LW?  Best I can tell the console can send gcode but doesn't show the serial coming back, does it? 



Any thoughts? 

Peter thanks for helping out Greg to the extent that you did and could. And Ashley thanks for the support along the way.  


---
**greg greene** *December 02, 2016 15:02*

My sincere thanks to all that chimed in, I really do appreciate the help and the patience.Like Ashley, I to have an IT background and fly RC models, and full scale too - built my first computer way back in 1968 so I'm not a stranger to the workings.  My problem in this particular case is not being familiar with either the board, the software or the installation process as to what to expect.  I missed the part about getting a red light when the power connection is correct - so I didn't look for it. The instruction link was a great help for sure and i am grateful for that.  Given the ever changing engineering the manufacturer does I don't think it is possible to ever come up with a set of instructions that cover every detail - because as soon as Ray or Scott do, the K40 will "evolve" into a different set of hardware components. 



So here's my progress to date



Got the board in and once I modified the power connector to fit the board - from 6 pin to 4 pin and knew what to look for - it came up fine - I hope.

Once I got past the somewhat cryptic error messages from the LaserWeb install - I haven't used Unix,Micronix,CP/M HDOS MSDOS or any other flavour of root command based OS since the last century - sold my PDP-11 in the eighties, and with the super link provided for the installer URL - Laser Web3 came up too. Except it wouldn't talk to the board - I remembered reading at some point that drivers may be required - so I asked Ray about that and yes they were.  Downloaded them - got the message that they installed correctly and carried on.  LaserWeb said it opened the comport correctly - and also that it was still not connected.  After two more driver deletions and re-installs Windows finally installed it as COM6 and we were off again.  Pressing the jog button got a smooth jog on both axis.  Pressing the home button got a terrible grinding noise and I am observing it only moves left to right now with the head skipping on the belt drive and the carriage trying to move in all directions at the same time.

Oh and during the whole procedure Microsoft decided it was an opportune time to inundate me with 22 updates to the Win 8 OS - Curse you Bill Gates !



So today - after a good night's sleep - and hopefully not all the distractions of yesterday - did I tell you I'm building a house and the Inspectors showed up in the middle of this?

I will attempt to remember where the spare stepper motor driver boards are that I purchased - in case one or both of them got fried yesterday, and that they will work with what ever brand of steppers the K40 Folks found in their parts bin to outfit my machine with.  



Although I try as best I can to think things through - most of this procedure is by necessity going to be trial and error because none of us have the complete picture.  It will be my errors causing everyone a lot trial on their patience I expect. I will see it through to the end, not so much because I'm a stubborn 70 year old curmudgeon - but because by doing so I hope to shed a little light on what others will find when they do it.



So again folks, thanks very much for your patience and help, I owe you all a beverage of your choosing - and possible a bottle of hair color and or restorer.


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2016 15:27*

Hi Greg, 

So regarding the grinding, is this happening always at the end of the axis or ever somewhere else? You wording is a bit unclear on this. 

Anyways I want to check your endstops. 



Disconnect in LaserWeb 

Download the Pronterface for windows. I will provide link shortly. 



You need to open it, connect via the same com port settings, and you can try jogging to make sure all is good. 



What I need you to do is type M119 into the console (bottom right) and verify what shows up in console (on the right). 



When a switch is not pressed, it should report 0. If you hold a switch down and send an M119 while it is pressed, you should see a 1 for that switch. We are interested in min x and max y. Please tinker with this a little bit and let us know what you find. 



Thanks for being patient as well. I take my commitment to get you up and running very seriously. 


---
**greg greene** *December 02, 2016 15:57*

Thanks Ray, It will be an hour or so till I can get to that - awaiting final inspection on the House - at long last!



With respect to the grinding, at first it was only when it reached the 'home' position, now it is anytime the head moves - and it will not move to 'home' anymore - it's like the steppers are fighting with each other over which way to go.  If you like, later I will try to get a video of that - but I'm worried the belt is probably chewed up at this point.



I appreciate your commitment to help, and all the help others have offered as well


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2016 16:02*

Ok. Do the endstops test like I said. 


---
**greg greene** *December 02, 2016 17:25*

Link?


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2016 17:38*

[http://koti.kapsi.fi/~kliment/printrun/Printrun-Win-Slic3r-03Feb2015.zip](http://koti.kapsi.fi/~kliment/printrun/Printrun-Win-Slic3r-03Feb2015.zip)


---
**greg greene** *December 02, 2016 17:50*

got it loaded - attempts for jogging just result in a huge racket and no movement


---
**greg greene** *December 03, 2016 01:07*

Connecting...

Printer is now online.

Disconnected.

Connecting...

Printer is now online.

>>> M119

SENDING:M119

min_x:0 min_y:1 min_z:1 max_x:1 max_y:0 max_z:1


---
**greg greene** *December 03, 2016 01:10*

x axis jogging works y axis just makes a lot of noise


---
**greg greene** *December 03, 2016 01:17*

switching the stepper driver boards does not change the above result, so they are probably ok


---
**greg greene** *December 03, 2016 01:23*

ok, the installation pdf shows the y axis connection reversed by color code than what my machine needs - I reversed it and now it jogs well in both axis next trying the home command in LW :)


---
**greg greene** *December 03, 2016 01:28*

Home command in LW is down left corner instead of up left corner, head attempts to travel past end stops



Looks like everything on my machine is opposite the ones you got Ray !!! :)


---
**greg greene** *December 03, 2016 01:30*

and finally - the test laser button doesn't fire the laser - but I'm not surprised at that !!!


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:49*

Hi Greg, 



If the motor is stalling IN PLACE then your stepper current needs to be adjusted. Please turn off power and use a screwdriver to turn the potentiometer on the A4988 clockwise approx 45 degrees, then try jogging again, and do another 45 if necessary. Power off before fiddling with the pot. 



Homing should be LEFT and UP. That's how it is on my machine and all other machines we have seen so far. 


---
**greg greene** *December 03, 2016 14:00*

Hi Ray, the motors are moving fine now, the problem was the orientation of the y axis (up and down) plug to the board.  The installation pdf showed it backwards to what my machine needed.  I could only go by wire color code as there are no orientation pins - and it appears the wiring harnesses are different in each production run anyway as far as what color wire goes to what pin and I suspect - the directional wires on the connector are reversed also.

So the only two things left to sort out are to reverse the direction of the y axis travel - as it currently wants to home down to the left - rather than up to the left, and the jogging in LW and pronterface confirm that.  Is that accomplished by reversing the direction wires on the connector - or setting a value in the config file?

I've confirmed via vom that the PWM wire is making to contact with what used to be the wiper contact pin on the PSU, but the laser doesn't fire when commanded via LW..Is there setting I'm missing here?



Thanks !

Greg


---
**greg greene** *December 03, 2016 20:11*

Just letting everyone know it is working great now thanks to Ray and the group ! :)


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/YTxDFXbD5hn) &mdash; content and formatting may not be reliable*
