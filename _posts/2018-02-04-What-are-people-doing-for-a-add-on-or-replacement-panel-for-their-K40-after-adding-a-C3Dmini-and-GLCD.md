---
layout: post
title: "What are people doing for a add-on or replacement panel for their K40 after adding a C3Dmini and GLCD?"
date: February 04, 2018 21:29
category: "K40 and other Lasers"
author: "John Plocher"
---
What are people doing for a add-on or replacement panel for their K40 after adding a C3Dmini and GLCD?  I've got a digital panel style, and have added a mAmeter and C3D, but the dangling dongles make things look and feel unfinished...





![images/429e5403997b6e8fcd90ef813ac86866.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/429e5403997b6e8fcd90ef813ac86866.jpeg)



**"John Plocher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2018 23:13*

New panel cut out of acrylic (check out tech bravo) or 3d printed glcd holder that sits on top (thingiverse) 


---
**James Rivera** *February 05, 2018 03:48*

LOL @ the “FDA” logo. 🤣


---
**John Plocher** *February 05, 2018 03:55*

Gotta love it - I can safely use this certified built-in-china machine to ... what?  cook my bacon?  Slice my pizza?  Ahhh, I know, that's how them hoodlums cut their stash!  ;-)


---
**Anthony Bolgar** *February 05, 2018 05:15*

FDA is responsible for regulating lasers in the USA


---
**Wild Bill** *February 05, 2018 23:53*

I just took a dremel and cut a hole for the meter to be mounted in the existing panel.

![images/0a38dac2a4f55894fc3c2ef85e25a795.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0a38dac2a4f55894fc3c2ef85e25a795.jpeg)


---
**John Plocher** *February 06, 2018 02:50*

I was hoping someone had AI/Corel/Lightburn files for a whole panel replacement that somehow fit both the GLCD and the K40 Digital LED/buttons and a meter in the existing space :-)



Second choice is milling out a hole in the cover for the GLCD and another in the panel (like you did) for the meter...


---
**John Warren** *February 06, 2018 14:12*

Search thingiverse for k40 panel.  There are numerous templates there.  I downloaded one and modified it to fit my needs.  



Then cut it out of 3mm acrylic.  One of these days I will get it installed.  I am holding off until i get a few more upgrades ready and doing them all at once


---
**John Warren** *February 06, 2018 14:13*

[https://www.thingiverse.com/thing:2722965](https://www.thingiverse.com/thing:2722965)


---
**John Plocher** *February 11, 2018 01:15*

**+John Warren** - thanks!  I found the baseline files as well, here’s what I ended up with!

![images/a5ff38d587d55a9b33c9154ab814ef25.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a5ff38d587d55a9b33c9154ab814ef25.jpeg)


---
**John Warren** *February 11, 2018 01:40*

very nice... now you have motivated me to finish mine and install it


---
**John Plocher** *February 11, 2018 04:01*

I used 1/4" thick black cast acrylic, and rubbed yellow latex paint into the etched letters, which easily wiped off the surface with a damp paper towel.  The digital panel's button tops are below the surface of the panel, which is good (for me) as I don't want to accidentally change the settings or trigger the laser test.  (The buttons were proud of the surface in a test-fit version I made using 1/8" stock, if that is what you want...)



Still to do:  rewire the aux outlets to connect them to the green switch (for compressor) and hardwire in the 24v stepper power supply for the Z-table.



Thanks for all the inspiration, y'all!



Edit:  Files on Thingiverse:

[https://www.thingiverse.com/thing:2789809](https://www.thingiverse.com/thing:2789809)




---
**Ray Rivera** *February 15, 2018 16:25*

Great setup; once my C3D arrives it will be the same. Did you have any issues wirh the wire swaps? There is hot snot all over the cables/plugs into the M2 Nano board.


---
**John Plocher** *February 15, 2018 18:41*

The hot snot (like the term!) all came off easily, and the C3D swap went well after I realized I'd swapped the X and Y connections.




---
*Imported from [Google+](https://plus.google.com/+JohnPlocher/posts/ScHxz9Z5GqK) &mdash; content and formatting may not be reliable*
