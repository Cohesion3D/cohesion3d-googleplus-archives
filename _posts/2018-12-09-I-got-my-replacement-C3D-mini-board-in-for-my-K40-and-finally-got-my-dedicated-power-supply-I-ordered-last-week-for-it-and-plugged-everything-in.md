---
layout: post
title: "I got my replacement C3D mini board in for my K40 and finally got my dedicated power supply I ordered last week for it and plugged everything in"
date: December 09, 2018 00:45
category: "C3D Mini Support"
author: "Reini Grauer"
---
I got my replacement C3D mini board in for my K40 and finally got my dedicated power supply I ordered last week for it and plugged everything in.  One thing seems like it's not working still: the Z axis only moves in one direction no matter what direction I select.  The Z table was tested and working fine when plugged into the X and Y axis and jogged so the hardware on that end works fine. I have narrowed this down to either a board or config issue, but I'm not sure what to do further.  I saw this post: [https://plus.google.com/109943375272933711160/posts/CwYLrztb5Ky](https://plus.google.com/109943375272933711160/posts/CwYLrztb5Ky)

And I'm having an identical issue.  X and Y dir pins show ~3.3v when moving one way, and ~0 when moving the other. Doing the same thing with the Z dir pin shows ~0.2v no matter what happens.  I used the external stepper driver breakout board to get the dir and ground references for my voltmeter.  

The only thing I changed on the config is the steps per mm on gamma, which I calculated based on my pulleys and screws on my Z table.  The config seemed to work fine at first when I was measuring the Z travel, until I went to move the table back down and it just kept moving up.



Is this axis probably dead?  I don't know yet if I'm ever going to get a rotary for this (the K40 doesn't have much room in the Z direction for anything really) and I'd really like to start engraving some christmas presents, do I need to change the pins for Z to the A pins?  What all is involved with that?





**"Reini Grauer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 01:48*

I think Steve has a handmade board by me from over 2 years ago, so things might be a bit different.  

Our factory tests all 4 axes in the board before shipping and I additionally tested your replacement board... so not sure if/ why an axis is bad. 



I"d be happy to take a look at pics of your wiring if you'd like to send some. 



In order to remap Z to A just change the step dir en pins in the config file from extruder 1 (that's the A axis) to gamma (that's Z) and back, and put the adapter in the A socket. 


---
**Reini Grauer** *December 16, 2018 23:13*

Got the Z table remapped to the A output.  Everything works great now.  Thanks for the help!




---
*Imported from [Google+](https://plus.google.com/107615990717524166780/posts/MchPyoayPcj) &mdash; content and formatting may not be reliable*
