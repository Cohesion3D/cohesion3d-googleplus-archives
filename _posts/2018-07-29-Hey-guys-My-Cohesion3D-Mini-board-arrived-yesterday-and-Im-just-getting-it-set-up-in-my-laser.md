---
layout: post
title: "Hey guys, My Cohesion3D Mini board arrived yesterday, and I'm just getting it set up in my laser"
date: July 29, 2018 00:48
category: "C3D Mini Support"
author: "Luke Prattley"
---
Hey guys,



My Cohesion3D Mini board arrived yesterday, and I'm just getting it set up in my laser.



I've got it all connected, and when connected to lightburn the laser homes correctly to the upper left hand corner...



However...



When i manually jog the laser position using lightburn, the Axes are inverted. the Up/Down arrows move the laser head left and right, and the left and right arrows move the head forwards and backwards!



Using Y Axis plug and ribbon cable configuration on the C3D board



Any ideas if there is any way to swap these in teh config or in lightburn? Otherwise its dismantle the laser and swap the wiring around between the two stepper motors...





**"Luke Prattley"**

---
---
**Tech Bravo (Tech BravoTN)** *July 29, 2018 01:00*

[lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Tech Bravo (Tech BravoTN)** *July 29, 2018 01:03*

Make sure about the origin and that the y axis isnt inverted at the board. If x is still inverted after that then yes i the config could correct i think. But, that should not be  necessary.


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 01:05*

Please don't post to multiple groups, it just results in burning more supporter's time.  **+Tech Bravo** we're knee deep on FB, I got it. 


---
**Tech Bravo (Tech BravoTN)** *July 29, 2018 01:05*

If i had to guess the origin is not set to front left (causing the x issue) and the y axis connector is flipped ( causing the y issue).


---
**Tech Bravo (Tech BravoTN)** *July 29, 2018 01:05*

Ok thanks


---
*Imported from [Google+](https://plus.google.com/104306411116306910187/posts/c9Jqmmeko1K) &mdash; content and formatting may not be reliable*
