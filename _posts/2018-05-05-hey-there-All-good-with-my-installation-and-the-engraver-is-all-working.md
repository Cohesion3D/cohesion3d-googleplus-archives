---
layout: post
title: "hey there. All good with my installation and the engraver is all working"
date: May 05, 2018 11:21
category: "K40 and other Lasers"
author: "Alaisdair McConachie"
---
hey there. All good with my installation and the engraver is all working. However, and you knew there was going to be something, the software just stops part way through the gcode. The first time it was just 5% off finishing so I lived with that, but the past three times it has not even got to 50% of the way through. Is this a memory thing? I have attached a couple of screenshots to see if that helps. As you can see there has been no movement since 15:39 minutes to 17:53, in fact there has been nothing since 9 minutes.



If anyone can shed some light I would be very much appreciated.



Many thanks

Al





![images/b100b61a7294142e862b2d957685ccdd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b100b61a7294142e862b2d957685ccdd.jpeg)
![images/1d1fc41be21502a5c9859cd7cb248303.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d1fc41be21502a5c9859cd7cb248303.jpeg)

**"Alaisdair McConachie"**

---
---
**Anthony Bolgar** *May 05, 2018 12:39*

I would post this on the LightBurn facebook support forum. Most likely a software issue.




---
**Alaisdair McConachie** *May 05, 2018 13:58*

ok, thanks. I shall give it a go. Thank you for your help though


---
**Ray Kholodovsky (Cohesion3D)** *May 05, 2018 14:22*

Not necessarily software issue. 

Are you using the stock USB cable from the K40? 

Is your K40 chassis grounded? 

Completely by chance I notice you have Simplify3D open. It likes to grab USB ports (you know how it auto connects) . Perhaps try closing it. 


---
**LightBurn Software** *May 05, 2018 20:16*

It won't help with the drop-out issue, but I do notice you're several versions behind (We're up to 0.7.01 now).


---
**Alaisdair McConachie** *May 13, 2018 16:36*

Guys, thanks for replying to the query and the advice. I have upgraded the software on the pc and made sure that all the grounding is in place and it is getting worse. Each time I run the code it gets less far than the previous time We are now down to 18% before it stops. Has anybody got any other ideas?




---
**LightBurn Software** *May 13, 2018 16:40*

**+Alaisdair McConachie** Try switching to GRBL-LPC. It’s much less sensitive. If your PC ever touches the mounted SD card while Smoothie is running, it can cause a dropout. It’s also possible to snip the power connection (or just tape over it) in the USB cable, as that prevents the possibility of a power loop which can add noise.


---
**Alaisdair McConachie** *May 16, 2018 19:04*

Thanks Lightburn, is there any documentation or guidance you can recommend for this. That would be really great. Thanks


---
**LightBurn Software** *May 16, 2018 19:07*

I suggest looking through the C3D documentation - they have help there for switching to GRBL, and their G+ page and FB group both have lots of topics on the power supply / isolation issues.


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2018 22:24*

Follow the link at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) for grbl-lpc instructions.


---
**Alaisdair McConachie** *July 14, 2018 15:18*

Hi All, and thanks Ray for giving me the links. Finally got round to changing over to GRBL and swapping over the lightburn software to Cohesion 3D GRBL. I plug in my USB and i get a constant alarm sound from the board. Have I missed something. It does not home. Your help as always is appreciated


---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2018 15:32*

Yes, that’s the trade off. Grbl-Lpc doesn’t support the screeen. 



Neither firmware will Home the board on power up. You need to get connected on Lightburn, then try jogging from there. 


---
**Alaisdair McConachie** *July 21, 2018 12:23*

Hi All again, maybe I am being a little stupid, but I don't seem to be getting anywhere. I have taken the screen off the board and the ALARM:9 still shows, when I start Lightburn and nothing happens. I have included a picture. I am hoping it is something simple

![images/dd643eab83b1a1696d9ed070db9d5037.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dd643eab83b1a1696d9ed070db9d5037.jpeg)


---
**LightBurn Software** *July 21, 2018 13:10*

ALARM:9 indicates "Homing fail. Could not find limit switch within search distances. Try increasing max travel, decreasing pull-off distance, or check wiring." 


---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2018 13:34*

Is this a K40? What is the maximum travel for each axis if you measure it? 



Still waiting to hear if you’re using the stock K40 USB cable or a better one. 


---
**LightBurn Software** *July 21, 2018 17:23*

Alarm 9 is a homing failure - which way does the head move when you turn it on? Could be a motor plugged in backwards and going the wrong way.


---
**Alaisdair McConachie** *July 28, 2018 17:14*

hey guys, sorry for the delay in responding, I am not always able to get the internet when I work away. I am still using the standard cable and the head does not move at all. It used to move when I had the smoothie version but it did not finish the job, since the upgrade to GBRL (i think that is how it is spelt) then is does not move at all, I have not changed the plugs for the motors, I have only removed the noisy scrfeen as that is not compatible.  Thanks guys for your support and I shall try harder to get a signal




---
**LightBurn Software** *July 28, 2018 17:18*

“I am not always able to get the internet..” Are you suggesting that a serial/USB connection to your C3D is causing internet connectivity issues for you? We get blamed for nearly everything, but this is new.


---
**Alaisdair McConachie** *July 29, 2018 11:10*

ha ha ha, no. The delay in my replies to you sometimes are a week, I work offshore or in a basement. The problem with the internet connection is mine not your software. I was asked if I am using the original lead, and yes I am. And I was asked which way the head moves when I connect to the laptop, it doesn't move at all. I shall try and take a video and post this.


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 14:12*

In short, start by using a high quality usb cable that is not the stock usb cable. 


---
**Alaisdair McConachie** *September 18, 2018 10:02*

Hey Ray, thank you for your continued support. I ma still having no joy. I have replaced the USB cable for the best one they had in the PC World near me and when I connect it through to my laptop I get no response. Thinking back this has been since I went to GRBL, maybe I have got the wrong version or something. I have taken a screenshot of what is shown on the lightburn screen, and have a picture of the files on the SD card, not that I think they will be of much use. Looking at the text file it seems to be more for 3d printers more than laser cutter, could it be that I have had the wrong one all this time? Is there anything I can send you that will make it easier to diagnose?

![images/849e069ec6fecce2c351e12ea82d23a3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/849e069ec6fecce2c351e12ea82d23a3.jpeg)


---
*Imported from [Google+](https://plus.google.com/117964355344506912526/posts/64RQVM1zfGW) &mdash; content and formatting may not be reliable*
