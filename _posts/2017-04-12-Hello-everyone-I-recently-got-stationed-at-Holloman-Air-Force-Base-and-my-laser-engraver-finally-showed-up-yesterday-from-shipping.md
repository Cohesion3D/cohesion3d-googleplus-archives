---
layout: post
title: "Hello everyone, I recently got stationed at Holloman Air Force Base and my laser engraver finally showed up yesterday from shipping"
date: April 12, 2017 01:09
category: "K40 and other Lasers"
author: "Collin Turner"
---
Hello everyone, I recently got stationed at Holloman Air Force Base and my laser engraver finally showed up yesterday from shipping.  The tube didn't get broken in my move which I am very happy about.  I have been looking to upgrade my controller for awhile, and decided I would do it when I got here.  I saw the great things that Ray and the community have been doing here and the Cohesion3D board looks nice, and thought about getting one myself.  The biggest problem is I'm not confident if my PSU and wiring is compatible with a Cohesion3D.  I was looking at getting the "Cohesion3D Mini Laser Upgrade Bundle" with the GLCD to replace this crappy chinese software and controller, and also add a PWM ability.  If someone could point me in the right direction and tell me what I'm doing right or wrong, I would really appreciate it.  

Thanks, 

Collin Turner

I included some pictures of the PSU and board.



![images/6bab70d538ffa53be896bc1f3fe5b591.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6bab70d538ffa53be896bc1f3fe5b591.jpeg)
![images/925c346e8add5e19df24a3b58d231d41.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/925c346e8add5e19df24a3b58d231d41.jpeg)

**"Collin Turner"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 01:16*

Yep, that's the ribbon cable variant, at least half the machines come like that. You're good. 


---
**Collin Turner** *April 12, 2017 01:18*

Thank you Ray for the quick reply, so the Cohesion board with GLCD screen will be a plug and play kind of deal?  Just reconnecting wires and whatnot?  Are there any links or tutorials with adding the PWM?


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 01:27*

Follow the link for Documentation at the top of the Cohesion3D.com website. Your wiring type isn't so much in the guide (I had the other machine wiring type but it's in the community here). [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3D.com)


---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 01:29*

This is the supplant for your machine: [plus.google.com - C3d Mini – Setup My laser machine has 2 Model numbers 1st 1 is KH40W from ven...](https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn)


---
**Collin Turner** *April 12, 2017 01:43*

Thank you a ton Ray, I'll get the board ordered tonight!


---
**Mike Chartier** *April 13, 2017 08:47*

With the ribbon cable, may have to take a wire off the power supply cord and re route it.... its what i had to do to get my laser to fire


---
**Mike Chartier** *April 13, 2017 08:55*

![images/3f943a000c08f7b30f229dc6d70889e7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f943a000c08f7b30f229dc6d70889e7.png)


---
**Mike Chartier** *April 13, 2017 08:55*

![images/319808420377cb0568fc0be4550b0178.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/319808420377cb0568fc0be4550b0178.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 16:39*

No no, that's not the case, we had to do that in rev 1 of the boards but not anymore not for a while now. 


---
**Mike Chartier** *April 13, 2017 21:45*

Ah ok


---
**Mike Chartier** *April 13, 2017 21:46*

I had to do that with mine to get my laser to fire though


---
*Imported from [Google+](https://plus.google.com/106338687452907889031/posts/dEs2eC1AhLD) &mdash; content and formatting may not be reliable*
