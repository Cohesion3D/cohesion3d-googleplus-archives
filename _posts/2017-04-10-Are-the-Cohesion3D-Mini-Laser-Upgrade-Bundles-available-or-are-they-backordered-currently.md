---
layout: post
title: "Are the 'Cohesion3D Mini Laser Upgrade Bundles' available or are they backordered currently?"
date: April 10, 2017 13:16
category: "General Discussion"
author: "Mike Hall"
---
Are the 'Cohesion3D Mini Laser Upgrade Bundles' available or are they backordered currently? I got my new K40 in last week, but I want to install a Cohesion3D board to avoid having to dig out a Windows based laptop. I'd rather start off with my MacBook and use LaserWeb.



Was hoping to buy today but the website lists them as available for pre-order. Thanks!





**"Mike Hall"**

---
---
**Joe Alexander** *April 10, 2017 14:11*

**+Ray Kholodovsky**


---
**Ray Kholodovsky (Cohesion3D)** *April 10, 2017 14:22*

We ran out of the Mini's. There's a new batch already being made, but it's a few weeks out. 


---
**Mike Hall** *April 13, 2017 13:45*

Nice! My C3D ReMix has shipped. Looking forward to getting it installed soon!


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 16:42*

Yep. I'll walk you through everything when you're ready to upgrade. 

May want to read up on the k40 Mini upgrade guide. 

We will have a few tweaks to do for the Remix. 


---
**Mike Hall** *April 15, 2017 18:04*

Got the ReMix... now I'm intimidated! 😉 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 18:19*

I prefer majestic :) 

We do have a few things to tweak, I'll walk you through the stuff when you're ready. 


---
**Mike Hall** *April 16, 2017 00:12*

**+Ray Kholodovsky** ready whenever you are!


---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 00:18*

**+Mike Hall** hangouts messaged you, or can do a call (phone number at the bottom of the website), or just tell me to start typing away :)


---
*Imported from [Google+](https://plus.google.com/106337213390028821874/posts/2DpLWJyRLec) &mdash; content and formatting may not be reliable*
