---
layout: post
title: "What apps are out there besides LaserWeb to run gcode with a Cohesion Mini board on a k40?"
date: June 02, 2017 01:58
category: "C3D Mini Support"
author: "bbowley2"
---
What apps are out there besides LaserWeb to run gcode with a Cohesion Mini board on a k40?





**"bbowley2"**

---
---
**Anthony Bolgar** *June 02, 2017 02:26*

There is Visicut at [http://hci.rwth-aachen.de/visicut](http://hci.rwth-aachen.de/visicut). 

[hci.rwth-aachen.de - The Media Computing Group :](http://hci.rwth-aachen.de/visicut)


---
**Joe Alexander** *June 02, 2017 04:31*

Well the C3D boards run off of either smoothieware or GRBL so I would imagine any program that can use those formats will work. I would check [smoothieware.org - start [Smoothieware]](http://smoothieware.org) first myself.


---
**E Caswell** *June 02, 2017 07:02*

**+bbowley2** take a look at this page [smoothieware.org - software [Smoothieware]](http://smoothieware.org/software)

Gives you a complete list of what will run on what machines. 


---
**Ax Smith-Laffin** *June 02, 2017 12:47*

Octoprint (primarily a 3D printing controller) if you're just looking to stream GCode and have control  now has a plugin that supports GBRL. So either form of GCode 3D Printer or GBRL will work.


---
**bbowley2** *June 02, 2017 15:12*

I have both Visicut and LaserWeb.  Also Inkscape.  I'm generating gcode in

Ventric Vcarve for my Piranha CNC but I'm having trouble running LaserWeb.

I undeerstand LaserWeb runs in Chrome.  How do you start LaserWeb in

Chrome?


---
**Joe Alexander** *June 02, 2017 15:56*

Laserweb3 was hosted in Chrome. As far as I can tell LaserWeb4 is more stand-alone.But LW3 is no longer being updated IIRC so I wouldn't use it. What issue are you having with LW4?


---
**bbowley2** *June 03, 2017 01:21*

When I bring a file into LW 4 I get an image on a white screen SVG file or

an image on a black screen bmp file.  There are no tools displayed.  The

only way to remove the image from the desktop is to close LW 4.


---
**Phil Housh** *June 03, 2017 15:35*

Same problem here.  Is there something that I'm missing


---
**Joe Alexander** *June 03, 2017 15:46*

LW doesnt support dragging and dropping the file into it, the drag part is once you load a file you pick the paths for the various processes you make. There is a ""Add Document" button near the top with a red flag saying click here to begin. Click that, select your file in the popup and load. You will see the various paths and groups appear right below the load button. those are the ones to drag down to make the engraving and cutting tasks. make sense?


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/iTksAVNdBFu) &mdash; content and formatting may not be reliable*
