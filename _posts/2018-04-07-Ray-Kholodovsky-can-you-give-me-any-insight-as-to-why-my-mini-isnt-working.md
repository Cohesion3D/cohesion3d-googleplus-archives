---
layout: post
title: "Ray Kholodovsky can you give me any insight as to why my mini isn't working?"
date: April 07, 2018 11:22
category: "C3D Mini Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky** can you give me any insight as to why my mini isn't working?



I have been playing around trying to get both K40s connected to the same extractor and now when I turn my mini powered one on I only get a single red light at VMOT. I have double checked my wiring and all seems ok, I have tried to reflash both smoothie and GRBL but neither of them are doing anything. I've even changed sdcards but that hasn't helped.



There doesn't seem to be anything smoked on the board so I am at a loss, any tips on whats next please buddy.





**"Andy Shilling"**

---
---
**Anthony Bolgar** *April 07, 2018 11:26*

Can you connect via usb if all you have connected to the board is the USB cable?


---
**Andy Shilling** *April 07, 2018 11:37*

**+Anthony Bolgar**  nothing at all connected to usb, I have cut the track for usb power though so that might be that reason.

I don't get a windows notification if I connect the usb when powered by 24v either.


---
**Don Kleinschnitz Jr.** *April 07, 2018 14:10*

**+Andy Shilling** the only thing you changed was the extractor setup? Did it work before that?


---
**Andy Shilling** *April 07, 2018 14:28*

**+Don Kleinschnitz** yes, I've not used it for a couple of weeks as I've been trying to get the k40 whisperer writing on this other k40 I have here. I'm thinking it has died for some reason but want to be sure before swapping it out for a new board.



I don't know why it's like this but I've never been 100% confident in this board and maybe it's time for a change.


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2018 15:08*

Say you power up, do you get any green lights even flash for a moment?  Or is it just the red LED? 


---
**Andy Shilling** *April 07, 2018 15:14*

**+Ray Kholodovsky** just a straight red light 🤪 even when trying to reflash it I get no other lights. I know when I flashed grbl it happened really quickly but I'm 100% sure nothing is happening this time.


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2018 15:17*

Means board no longer has any brain activity.  Sorry bud. 


---
**Andy Shilling** *April 07, 2018 15:19*

**+Ray Kholodovsky** a bit like me then. So I take it there are no options to recover it in any way shape or form.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/CSgS7GiSyCy) &mdash; content and formatting may not be reliable*
