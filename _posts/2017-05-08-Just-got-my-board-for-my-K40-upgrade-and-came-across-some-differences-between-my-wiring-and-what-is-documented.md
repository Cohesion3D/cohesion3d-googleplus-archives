---
layout: post
title: "Just got my board for my K40 upgrade and came across some differences between my wiring and what is documented"
date: May 08, 2017 19:05
category: "K40 and other Lasers"
author: "David Regis"
---
Just got my board for my K40 upgrade and came across some differences between my wiring and what is documented.  



My K40 is the type with on multi-color cable ending in a 4-pin connector and one gray flat cable (photo below -- these are the only two cables going to the XY stage).  The Cohesion board has a connector for the ribbon, so I'm assuming it plugs in there.  The other cable goes to the Y stepper connector at the top of the board.  Does this sound right?



Included was a 3-pin gray cable which would appear to fit the "K40 PWM" socket on the board, but it's not at all clear where the other end would connect to.



Lastly, the connector on the end of my power cable has the same number of pins and pitch, but it doesn't really mate with the connector on the board.  Photos of the power cable and power-supply below.



(I've just found another thread where the customer had a similar setup... [https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn](https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn) ... sorry if this is a repeat).





![images/a0296a5a34324b2a91c95aa7ac2b61db.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a0296a5a34324b2a91c95aa7ac2b61db.jpeg)
![images/87f866948916046a63c8e1c8a601fd18.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/87f866948916046a63c8e1c8a601fd18.jpeg)
![images/39b3ded7e0a4611c9f13f075af2d1962.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39b3ded7e0a4611c9f13f075af2d1962.jpeg)

**"David Regis"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 19:06*

Here you go: 

[plus.google.com - C3d Mini – Setup My laser machine has 2 Model numbers 1st 1 is KH40W from ven...](https://plus.google.com/117786858532335568822/posts/TfiJPuYAdAn)


---
**Ray Kholodovsky (Cohesion3D)** *May 08, 2017 19:11*

Oh, you even found the post I usually link to.  Missed that bit. 



Ribbon Cable - yep. 

Y Motor - yep

Big power connector pushes the wall out, as documented. Good. 



Do not use the included cable, it is for legacy applications.  Just those 3 things. 




---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 00:55*

I just updated the instructions to include this wiring variant, please let me know if you have any feedback on it. 


---
**David Regis** *May 11, 2017 00:44*

Ray,



The updated instructions are fine.  I've got my machine up and running, and finally with integrated support for my Z-table!  I haven't setup my A-axis yet, but will do so as time permits.   



As I understand it, connecting/disconnecting steppers with the power-on is a bad idea... do agree?  If so, I assume I'll be powering down whenever I want to install/remove my A-axis.



I manually walked through the Laserweb4 setup and think I've got everything.  There was mention of a config file on the SD card, but I couldn't figure out how to load it.



My next challenge will be retraining myself from the Lasersaur workflow to LaserWeb.  I've made enough progress to run a job with multiple layers requiring different power/feedrate settings... not quite as intuitive as Lasersaur, but it appears to be far more flexible.



So far, I'm really impressed with the hardware... great work!


---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 00:45*

Yes, in general "turn power off before fiddling with any wiring".



The config file on the card configures the Cohesion3D board and the Smoothie firmware running on it.  LaserWeb is separate.


---
*Imported from [Google+](https://plus.google.com/113393104930715759897/posts/feVPsCCjKym) &mdash; content and formatting may not be reliable*
