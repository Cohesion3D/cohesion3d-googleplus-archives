---
layout: post
title: "Hi all I'm looking for yet more help if anyone has the time or is able too"
date: September 14, 2018 13:52
category: "C3D Mini Support"
author: "jim walker"
---
Hi all I'm looking for yet more help if anyone has the time or is able too.

I have a c3d mini with a fault on the Y output, fully tested and its just dead.



its no big hassle as there is the 2 unused outputs "Z&A"



Ray said to just swap the Dir,step and en from the beta to the A, this is simple enough but I don't have the "A" output in the config text just the apha,beta and gamma.

I don't want to use the z output as I intend to add one in the near future.

can any one tell me the lines of code I need to add to enable the A output (I don't need pin values they are easy enough for me to sort )or if anyone has a config text already configured with A output that would be great.

thanks in advance

Jim





**"jim walker"**

---
---
**Tech Bravo (Tech BravoTN)** *September 14, 2018 13:55*

4 axis config file can be found here if it helps any: [lasergods.com - C3D 4 Axis Firmware & Config](https://www.lasergods.com/c3d-4-axis-firmware-config/)


---
**jim walker** *September 14, 2018 14:07*

**+Tech Bravo** Many thanks I'm sure it will be a great help




---
**Ray Kholodovsky (Cohesion3D)** *September 14, 2018 14:25*

Consult the pinout diagram or the 4 axis config file.  But keep the config file you had just manually change the pins. 


---
**jim walker** *September 14, 2018 14:57*

+Ray Kholodovsky With you thanks ray


---
**jim walker** *September 14, 2018 15:53*

It worked a treat thanks both for your help


---
*Imported from [Google+](https://plus.google.com/112725541647995467484/posts/RpyY12w7EzX) &mdash; content and formatting may not be reliable*
