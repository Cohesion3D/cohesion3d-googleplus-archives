---
layout: post
title: "First, I must thank both the Cohesion3D and LighBurn teams for the great combination"
date: February 14, 2018 01:30
category: "C3D Mini Support"
author: "joe abaya"
---
First, I must thank both the Cohesion3D and LighBurn teams for the  great combination.   Easy upgrade to the K40 that improved the usability and functionality.

Please review the center and right panels bottom edges in the picture.  Also the long rectangular cuts in the bottom of the right most panel were not lined up.  Does this look like an issue with binding X stage? or Speeds too high causing Cohesion3D mini and Lightburn to lose steps on my K40 laser?  I tested manual movement in the x stage, but it did not feel like it was binding along the y home.   Other prints cut  fine without this issues, but this is the first almost full area  sized print.  The cuts and outline were done last and i did hear some "crash" during the center section's top edge was approached.  I did not see it hit or drag, and i could not tell if the end stop was triggered.  Thank you all in advance for any advice on speeds and feeds to prevent this, or where to check for binding in the machine movement.    LightBurn on MacOS High Sierra in case that makes a difference.

 Here is the LightBurn file on Google drive for comparison. [https://drive.google.com/file/d/1G_0ujO9pDc0mXMZGFOsw5Yxds04J1TC8/view?usp=sharing](https://drive.google.com/file/d/1G_0ujO9pDc0mXMZGFOsw5Yxds04J1TC8/view?usp=sharing)

![images/e2b88eb6ded2094f2e4df4e573625bb0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e2b88eb6ded2094f2e4df4e573625bb0.jpeg)



**"joe abaya"**

---
---
**joe abaya** *February 14, 2018 17:35*

I ran a second test, but cleaned and lubricated the X linear rail first.  It still shows the issue but improved slightly.  I believe it to be a physical issue and not as software or electronic issue.   Therefore I will continue to troubleshoot it as such, and If I can find a root cause I will update even if it is out of scope for this community.  


---
**Ray Rivera** *March 14, 2018 21:11*

Joe looks like I am having a similar problem. Here is my rub

![images/3b2df35ec34ac1b9fe989c45b3451066.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b2df35ec34ac1b9fe989c45b3451066.jpeg)


---
*Imported from [Google+](https://plus.google.com/102436526546983870272/posts/2pTUbhdunUL) &mdash; content and formatting may not be reliable*
