---
layout: post
title: "Thinking of upgrading my wanhao with a cohesion"
date: May 01, 2018 04:21
category: "3D Printers"
author: "tyler hallberg"
---
Thinking of upgrading my wanhao with a cohesion. I've been printing on the printer for years with very little issue, only big one being bed leveling. Ray, does the cohesion support auto bed leveling input? This is the only reason for me upgrading the hardware on printer. Successfully running a cohesion board in my laser for the last year +





**"tyler hallberg"**

---
---
**Alex Hayden** *May 01, 2018 14:19*

You can run auto bed leveling with the original board. You just have to modify it a bit. Are you using ADVi3++ firmware?


---
**Sam Jones (Thumpszilla)** *May 02, 2018 14:22*

I'll sell you all of my cohesion stuff been used about 2 weeks. I decided to go with a light object dsp so I don't need it.

3d mini 

glcd

glcd header

z table header

4amp stepper driver

24v 5a power supply

analog amp meter



Email me at calvarycreations1@gmail.com


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/GBYejYrxphd) &mdash; content and formatting may not be reliable*
