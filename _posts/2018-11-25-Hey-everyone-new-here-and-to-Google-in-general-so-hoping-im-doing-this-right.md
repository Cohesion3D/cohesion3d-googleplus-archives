---
layout: post
title: "Hey everyone, new here and to Google+ in general so hoping im doing this right"
date: November 25, 2018 03:26
category: "K40 and other Lasers"
author: "Josh Mosselli"
---
Hey everyone, new here and to Google+ in general so hoping im doing this right.



The problem im having is with my C3D mini in my k40. I installed a lightobject Z axis table today and am having issues getting it to function right. It works as it should, but it moves very slow. What should i check to get it to move how its supposed to? I followed the wiring directions on C3D website to a T, so im fairly confident its not that. Please Help!





**"Josh Mosselli"**

---
---
**Josh Mosselli** *November 25, 2018 04:52*

Scratch this. somehow i managed to make it move faster by updating the firmware on the C3D mini and changing the config file. I also had to go under the "Feed Rate" menu on the LCD and crank it up to 6000 to match the X and Y axis. however everytime i turn the machine off, the Z axis resets to 300mm/min or whatever it is. Is there a way to make it save the settings that are put into it?


---
**Josh Mosselli** *November 25, 2018 04:54*

Scratch that last question as well. No i see it needs to be updated in the config file to be saved.


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 03:25*

So you're good now?


---
**Josh Mosselli** *November 30, 2018 03:14*

**+Ray Kholodovsky** yes sir. Thanks for checking and sorry for the late reply. 


---
*Imported from [Google+](https://plus.google.com/105476315635281718893/posts/RqfAz9j2bDZ) &mdash; content and formatting may not be reliable*
