---
layout: post
title: "Is it possible to modify the Mini's config via USB, should the need arise?"
date: March 14, 2017 22:38
category: "FirmWare and Config."
author: "Bob Buechler"
---
Is it possible to modify the Mini's config via USB, should the need arise? Or do you have to actually eject the SD card and find a micro card reader somewhere? 





**"Bob Buechler"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 22:40*

Yepp.  [smoothieware.org - sd-card [Smoothieware]](http://smoothieware.org/sd-card)



It shows up as a drive on your computer when you plug in over USB.


---
**Bob Buechler** *March 14, 2017 23:01*

Whoa. Glad I asked, as I'm an OSX user. Will turn that crap off immediately. :/




---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 23:03*

Not sure what I've sparked here. Are you talking about the auto mounting thing? We don't care about that. 


---
**Bob Buechler** *March 14, 2017 23:26*

Yeah. This: "It is not recommended you allow the sdcard to auto mount, and it is highly recommended that the sdcard be unmounted at all times except when files need to be copied. <b>*This is especially true on Macs*</b> which like to randomly read and write the sdcard, if this happens during a print it will cause pauses and other printing problems."


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 23:27*

Aha right. Well if you see any of those symptoms, then you'll know. I think it's a separate firmware build that does that tho. 


---
**Bob Buechler** *March 14, 2017 23:41*

huh okay. I'll disable it proactively just to be safe. I'm fine mounting it manually and avoiding the whole thing. :)




---
**Bob Buechler** *March 15, 2017 02:24*

BTW: This turned out to be very necessary for me as a mac user. My host machine was lagging severely every one or two seconds until I unmounted the sdcard.


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:25*

Noted thanks.


---
**Bob Buechler** *March 23, 2017 21:50*

Quick update on the sdcard mounting issue for OSX users. After spending more time with the C3D Mini, I strongly recommend emphasizing the need to unmount the SD card. The computer is almost unusable any time the card's filesystem is mounted caused by the huge lag spikes you experience trying to repeatedly access the card like a mounted disk over USB. Furthermore, apps like Laserweb may not even be able to establish a connection with the controller while the card is mounted due to all the excess USB traffic it causes. 


---
**Ray Kholodovsky (Cohesion3D)** *March 24, 2017 00:34*

**+Andy Shilling** please read this. 



Thanks Bob!


---
**Bob Buechler** *March 24, 2017 02:32*

Also: Later this afternoon, **+Peter van der Walt**​​ suggested on a different thread that I should try using a specific Smoothie firmware that, I believe, disables the mounting entirely. Link: [https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-disablemsd.bin](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-disablemsd.bin)


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/VFBkYQZSg4J) &mdash; content and formatting may not be reliable*
