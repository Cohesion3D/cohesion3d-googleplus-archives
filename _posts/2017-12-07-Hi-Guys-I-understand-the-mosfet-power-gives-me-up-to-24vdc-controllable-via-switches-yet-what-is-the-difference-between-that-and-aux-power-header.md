---
layout: post
title: "Hi Guys, I understand the mosfet power gives me up to 24vdc controllable via switches yet what is the difference between that and aux power header?"
date: December 07, 2017 13:41
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Hi Guys,



I understand the mosfet power gives me up to 24vdc controllable via switches yet what is the difference between that and aux power header?  If i add +24v to the aux power header, does that give me another +24v I can control  using heat bed out?

Also, What is heat bed in?





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2017 15:06*

There are 3 separate power inputs on Remix: Main (motors + logic), bed, and fets. 



Aux power I think refers to the header at top left is just a copy of Main Power In, it was meant for plugging in an accessory like a fan. Definitely do not try to insert power via here and the main power in screw terminal. 


---
**Todd Mitchell** *December 07, 2017 18:07*

**+Ray Kholodovsky** so for the bed, can i control that much like any other relay, thus giving me another switch?  (I'm not using this for a 3d printer)


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2017 18:09*

Yes, you can configure it however you want by defining additional switch modules in config. 


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/aR6UWKuvk4E) &mdash; content and formatting may not be reliable*
