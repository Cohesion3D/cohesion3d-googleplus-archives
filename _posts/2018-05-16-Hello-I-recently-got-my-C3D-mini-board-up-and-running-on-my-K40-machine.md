---
layout: post
title: "Hello, I recently got my C3D mini board up and running on my K40 machine"
date: May 16, 2018 17:17
category: "C3D Mini Support"
author: "Alex McKelvey"
---
Hello, I recently got my C3D mini board up and running on my K40 machine. At first, I was only able to control the X axis. I was not able to move the Y axis by hand and could not get lightburn to move the Y at all. I did some reading on here and found the configuration files that **+Ray Kholodovsky** uploaded to remap the Y axis to the A axis. With those files and the Y axis hooked up to A, the machine works well and I am able to control both axes. 



Why is this workaround necessary? Is there another way to get the controller to use the y axis working properly? I would like to add a rotary to my machine in the future.





**"Alex McKelvey"**

---
---
**Jim Fong** *May 16, 2018 19:54*

With electronics, nothing is going to be 100% failure free.  Manufacturing defects, faulty semiconductor chips, damage from static handling etc.  



I’ve done my own damage by putting drivers in backwards, reversing power leads or wrong voltage.  Mis-wiring. Not reading instructions correctly. Not using anti static wrist band. Last week I blew a backlight LCD screen when the end of the DC power adapter cord just happened to touch the back of the board.  Magic smoke appeared. Shit happens. 



Talk to Cohesion3d, they can walk you through to verify the Y is faulty. 



As far as the rotary, you can still use the Z axis stepper driver to run it.  


---
**chris b** *May 20, 2018 16:49*

Searching for this file my self, had same issue and worked for me as well. Managed to corrupt card(long story) now need the file again and have also lost my backup. 


---
**David Piercey** *June 26, 2018 18:48*

Are you still able to link to those config files?


---
*Imported from [Google+](https://plus.google.com/100095806046074605228/posts/XAeq9zgiEau) &mdash; content and formatting may not be reliable*
