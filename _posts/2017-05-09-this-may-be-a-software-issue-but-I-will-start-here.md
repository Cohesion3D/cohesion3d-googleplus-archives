---
layout: post
title: "this may be a software issue but I will start here"
date: May 09, 2017 08:58
category: "C3D Mini Support"
author: "Maker Fixes"
---
this may be a software issue but I will start here. Installed my mini and laserweb4 seem to be able to make it go except the burned images is rotated 90 degrees counter clockwise. any suggestions to what the issue could be?





**"Maker Fixes"**

---
---
**Griffin Paquette** *May 09, 2017 10:13*

Check that your axis are plugged in correctly. It sounds like you have one backwards and in the wrong port. 


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 12:58*

If you jog X- the head should move left. If you jog Y+ it should move to the rear of the machine. Are both of these things correct? 


---
**Maker Fixes** *May 09, 2017 22:02*

nope -  ok I undid the connector to the servos and did them one a ta time. I followed your diagram and it seams they put my wire colors opposite of yours on this machine that issue works now thanks!


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/2qNaaEoF1wu) &mdash; content and formatting may not be reliable*
