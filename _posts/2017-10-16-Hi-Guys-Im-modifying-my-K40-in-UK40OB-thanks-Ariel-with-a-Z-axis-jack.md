---
layout: post
title: "Hi Guys, I'm modifying my K40 in UK40OB (thanks Ariel!) with a Z axis (jack)"
date: October 16, 2017 16:23
category: "C3D Mini Support"
author: "Yann Capitaine"
---
Hi Guys,



I'm modifying my K40 in UK40OB (thanks Ariel!) with a Z axis (jack).



I had to install 2 stepper motors for Y axis.

My controller is the Cohesion3D Mini.



I quite finish all the installation and now I try to configure the config.txt.

But, it looks like Chinese for me.



Anyone could explain me how to modify it?

for Y:

Y motor 1 plugged in Y and Y motor 2 plugged in A

for Z:

Z motor plugged in Z



Thanks a lot



Yann





**"Yann Capitaine"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2017 17:06*

You should grab the config and firmware files from the Dropbox at the bottom k40 install instructions. 



You will probably only need to change your steps per mm and possibly the homing switch location (min or max). 



There is not a way to "slave an axis" in the config file. Perhaps you can use a TB6600 single external stepper driver to drive both your Y from the same single one? 

You will probably need a similar external driver for your Z axis. 

I'm not sure which drivers you are currently using for your build. 


---
**Yann Capitaine** *October 19, 2017 16:20*

My steppers are all polollu plugged in the cohesion3D mini.



For the Z axis, it's ok (thanks for your dropbox).



I found this:

[smoothieware.org - general-appendixes [Smoothieware]](http://smoothieware.org/general-appendixes)

scroll down till: "Doubling stepper motor drivers"



But it's a little bit like Chinese for me. Anyone can help me?




---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 16:22*

They want you to physically run wires to connect the step, dir, and en pins of 2 drivers.  This is not possible on the Mini.


---
*Imported from [Google+](https://plus.google.com/107351544578813346016/posts/dZzU95uLf5T) &mdash; content and formatting may not be reliable*
