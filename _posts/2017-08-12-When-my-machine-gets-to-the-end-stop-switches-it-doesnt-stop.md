---
layout: post
title: "When my machine gets to the end stop switches it doesn't stop?"
date: August 12, 2017 01:57
category: "K40 and other Lasers"
author: "William Kearns"
---
When my machine gets to the end stop switches it doesn't stop? Any idea as to why? Running cd3 with LW4





**"William Kearns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 01:59*

Yes, it backs off some and then hits them a second time. It's doing this slowly so people think that it's grinding at the end. 



If this is not the case please send a video and a pic of your board wiring. 


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/SF9x43xwz72) &mdash; content and formatting may not be reliable*
