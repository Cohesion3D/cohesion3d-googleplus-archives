---
layout: post
title: "I have the same basic question as Brent Dowell a few days ago"
date: October 26, 2018 02:00
category: "FirmWare and Config."
author: "Rob Dailey"
---
I have the same basic question as Brent Dowell a few days ago.  I made a powered Z table for my K40 and have enabled the config for Smoothieware based control.  



My issue is that I don't have endstops installed for the table and don't plan on doing this for quite awhile.  I can get my table to move up, but it never moves negative.  How do I fix this in the config.txt for the Cohesion3D Mini.



I don't want to home the table either.  I plan on setting focus using a jig and moving the table up or down using the GLCD.



In this same vein, I'm trying to set the steps per mm for the axis so that it is correct.  Based on calculations on RepRap Calculator ([https://www.prusaprinters.org/calculator/](https://www.prusaprinters.org/calculator/)), I should have the value set at 2560.  (1.8 pitch with 1/16 microstepping using a M8 threaded rod)  However, when I use that value, the motor doesn't move.  Any thoughts?





**"Rob Dailey"**

---
---
**Anthony Bolgar** *October 26, 2018 07:28*

Stepper current may not be high enough, stepper itself may not be strong enough. Try it at a lower micro step rate. You really do not need it to be so fine of a movement, when will you need to move the bed 1/2560th of a mm?




---
**Rob Dailey** *October 26, 2018 13:13*

**+Anthony Bolgar** I was following a z table build that someone else did and those were the settings they used.  If I lowered the value to 1280 that would effectively move the table .5mm for each revolution correct?


---
**Rob Dailey** *October 26, 2018 14:53*

I found in the smoothie documentation that you can set the gamma_min_endstop and gamma_max_endstop to nc if they aren't installed.  If I do this, would that solve my z travel issue of not being able to go down?



I wanted to verify first before trying it tonight when I get back home.


---
**Ray Kholodovsky (Cohesion3D)** *October 26, 2018 16:41*

That’s what I was going to suggest. Total shot in the dark though - let me know if it works. 


---
**Rob Dailey** *October 26, 2018 16:56*

I'll try it out tonight. If not, I might be putting on a limit switch sooner than later. 


---
**Rob Dailey** *October 26, 2018 23:26*

**+Ray Kholodovsky** No dice.  The machine will let me go up all day long.  I can never decrease the height.  Any other suggestions?


---
**Rob Dailey** *October 27, 2018 02:00*

**+Ray Kholodovsky** I figured out the issue.  I followed the instructions provided by you for adding a Z table with ONE EXCEPTION.  I didn't use the 5VDC header off of the board.  My initial thinking was "I need 5VDC for the driver, I'll off load that onto a 5VDC power supply I have for my aiming lasers."  5 volts is 5 volts.  Apparently not.  I made a jumper like the instructions said and all is good with the world.



I would suggest adding a small disclaimer to the instructions that states that the 5 volts needs to come from the Cohesion3D board.  That way other people like me don't screw it up.






---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2018 03:41*

Ah so something like: 

“If you don't follow the instructions it will not work as intended.”

?


---
**Rob Dailey** *October 27, 2018 05:35*

That should do the trick.  Thanks for all you do.  Looking forward to firing this thing up and cutting something this weekend.


---
*Imported from [Google+](https://plus.google.com/116013967162753886043/posts/YhFbC2Dx1wM) &mdash; content and formatting may not be reliable*
