---
layout: post
title: "I searched for an answer to this before posting, so i'm sorry if its out there and I missed it"
date: May 17, 2018 16:17
category: "C3D Mini Support"
author: "Chapman Baetzel"
---
I searched for an answer to this before posting, so i'm sorry if its out there and I missed it.

What is the recommended way to connect some LEDs to the cohesion3d mini board?  Simple 5V leds, i think they've got 200ohm current-limiting resistors inline, but i could change that.   I don't absolutely need them to switch on and off.  I just want to be able to see the print on my rPi camera without needing the overhead lights on in the room.  If the mini can't drive them easily, I'll sort out a way to power them from the pi.  But I think the mini would be better able to do it. 





**"Chapman Baetzel"**

---
---
**Jim Fong** *May 17, 2018 16:29*

The on board 5volt has limited current ability. I wouldn’t use it to drive a string of LED lights which probably requires several hundred milliamperes.  Use a separate external 5 volt power supply to drive your LEDs. A good phone charger may work. 






---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2018 00:20*

What Jim said. I recommend a cheapy lm2595 buck module from china for under a dollar. Buck. Haha. 


---
**Chapman Baetzel** *May 18, 2018 00:43*

I can just use the pi.  Its driving the camera anyway, I know it can power a few LED's to illuminate the camera area.


---
*Imported from [Google+](https://plus.google.com/+ChapmanBaetzel/posts/9WFW9U3cGFP) &mdash; content and formatting may not be reliable*
