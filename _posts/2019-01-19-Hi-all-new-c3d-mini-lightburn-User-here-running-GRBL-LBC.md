---
layout: post
title: "Hi all, new c3d mini/lightburn User here, running GRBL LBC"
date: January 19, 2019 21:07
category: "C3D Mini Support"
author: "Ryan G"
---
Hi all, new c3d mini/lightburn User here, running GRBL LBC. When I start an engraved the laser head moves at a crazy quick speed which is causing the destination it arrives at to not be accurate. Seems to home at a more sensible speed.. can somebody assist with the GRBL command I need to slow this down? Thanks! 

![images/fa1c11b592a7da58f8148ec0bfaf1d7b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fa1c11b592a7da58f8148ec0bfaf1d7b.jpeg)



**"Ryan G"**

---
---
**Jamie Richards** *January 19, 2019 23:12*

Are you using an additional power supply or the stock K40 LPSU?


---
**Jamie Richards** *January 19, 2019 23:24*

Can you show some pics of your board wiring?


---
**Ryan G** *January 20, 2019 00:41*

Hi, am running off of cohesion supplied psu. I have disconnected the 24v from the k40 psu (yellow cable on the right) rather than totally cut it until I’m sure I have everything working. Here’s a pic. Thanks!

![images/ecf6b77d0eb1633f61b69755eae321bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ecf6b77d0eb1633f61b69755eae321bd.jpeg)


---
**Ryan G** *January 20, 2019 08:33*

To add to this post, I appear to be having more issues than just this.



1) Sometimes lightburn doesnt see the laser.  I click something, nothing happens, switching to a different com port and back again seems to kick it into life.



2) When I go to engrave an image, its pot luck where the engraving starts.  Sometimes it travels to the bottom of the work area (too fast) and engraves from there (as per vid), sometimes it starts at the top of the work area (and quickly runs out of room.  Sometimes it picks somewhere random?!  Although i think this is it trying to get to the bottom but skipping teeth due to the speed.  Sometimes it tries to travel upwards from the top left, causing the motors to grind.  Either way, start points are seemingly random and there appears to be no pattern as to which is chosen.



3) Quality of engraving:  I simply can't get a decent picture to engrave, I'm yet to produce anything which rivals what I can make with the M2 Nano.  When I try greyscale, there is are no shades of grey, it appears to be basically a 1 bit image.  Jarvis kind of works, but does not give good quality, even compared to the M2 Nano.



If anyone could provide some assistance regarding this I would be super grateful, just can't get this working as it should be.


---
**Claudio Prezzi** *January 20, 2019 09:36*

Did you check [laserweb.yurl.ch - GRBL 1.1 - LaserWeb / CNCWeb](https://laserweb.yurl.ch/documentation/initial-configuration/31-grbl-1-1e) for acceleration and feed configuration of Grbl?


---
**Claudio Prezzi** *January 20, 2019 09:44*

In Grbl $110 sets the maximum feed for X ($111 for Y) which is used for G0 moves. The accelleration is set with $120 and $121.

You can see the actual settings by sending $$.


---
**Jamie Richards** *January 20, 2019 21:35*

You may wish to check the LightBurn and Cohesion3D forum on Facebook, it is very active.  How are your configuration settings?  Did you download the latest GRBL-LPC for the mini, are your bed size settings the same size for the grbl configuration as it is in the LightBurn settings?  Lots of variables.  Can you do a $$ in the console tab to show your settings and paste them here?  Plus a screenshot of your LightBurn settings. 




---
**Ryan G** *January 21, 2019 01:34*

Thank you for your input both.  Claudio, I have experimented with the commands you suggested, and although they do slow down the movement of the axis, they also slow down the speed of engraving.



I am running the latest version of the C3d mini Firmware as detailed in the lightburn documentation.



GRBL is as follows:



$0=10

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=1

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=1.000

$36=100.000

$100=160.000

$101=160.000

$102=160.000

$103=160.000

$110=24000.000

$111=24000.000

$112=24000.000

$113=24000.000

$120=2500.000

$121=2500.000

$122=2500.000

$123=2500.000

$130=310.000

$131=220.000

$132=50.000

$133=100.000

$140=0.600

$141=0.600

$142=0.000

$143=0.000



I have posted to Cohesion board on Facebook but no reply yet.



LB machine settings are attached also.



Many thanks

![images/56638da27db086db49b563703855a044.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/56638da27db086db49b563703855a044.png)


---
**Ryan G** *January 21, 2019 01:35*

and device settings:

![images/cfc1eef84c00aed18f4a46f8ce3833b2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cfc1eef84c00aed18f4a46f8ce3833b2.png)


---
**Jamie Richards** *January 21, 2019 03:29*

You somehow got the wrong grbl.   



Download appropriate Grbl-LPC for your setup from this site.

[embeddedtronicsblog.wordpress.com - grbl-lpc C3D Mini Firmware Builds](https://embeddedtronicsblog.wordpress.com/2018/12/26/grbl-lpc-firmware-builds/)



Don't forget to reconfigure the grbl settings for your bed size since it will reset.



This is my setup:

$0=5

$1=255

$2=0

$3=3

$4=0

$5=1

$6=0

$10=0

$11=0.010

$12=0.002

$13=0

$20=0

$21=0

$22=1

$23=1

$24=50.000

$25=6000.000

$26=250

$27=2.000

$30=1000

$31=0

$32=1

$33=5000.000

$34=0.000

$35=1.000

$36=100.000

$100=157.480

$101=157.480

$102=157.480

$103=8.889

$110=30000.000

$111=30000.000

$112=30000.000

$113=30000.000

$120=2000.000

$121=2000.000

$122=2000.000

$123=2000.000

$130=310.000

$131=220.000

$132=150.000

$133=1000.000

$140=0.900

$141=0.900

$142=0.900

$143=0.900


---
**LightBurn Software** *January 21, 2019 04:02*

LightBurn never changes your G0 rate (rapid speed), but you can - Try typing G0 F3000 in the console (or in a macro) and see if that behaves better for you.


---
**LightBurn Software** *January 21, 2019 04:11*

Realistically, if the machine loses steps when you move that fast, you shouldn't be using that speed for engraving either. You can also try lowering the acceleration, which will help you hit higher overall speeds, but less aggressively.  Try cutting your $120 & $121 values in half.


---
**Claudio Prezzi** *January 21, 2019 08:02*

**+Ryan G** You should also check the stepper current settings ($140 and $141). Some machines/motors need more than others.


---
**Ryan G** *January 21, 2019 23:44*

thank you all for your input.  I have loaded the firmware supplied and am in a much better place, i can now navigate round the whole work surface without grinding my motors so thats positive.



Will investigate a way to slow down the start of the job later, am using 'Go To Origin' in the meantime (which happens at a suitable speed) which works for now.



Next issue I am tackling is the quality.  I am trying to dial in PWM in GRBL but not getting any output irrespective of the power I give in the command.  S1 gives no fire and no movement on ammeter.  Have changed PWM config using $33 to 200/400/600 but none of them fire the laser? Have read the FAQ but it does not cover what to do if you get no fire at all?!


---
**LightBurn Software** *January 21, 2019 23:56*

S1 wouldn't give output on GRBL if you have the defaults. $30 is usually 1000, so you need S1000 (100%) or similar.


---
**Ryan G** *January 22, 2019 00:17*

Yes $30 is set to 1000.  I have run G1 X10 S1000 F600, laser head moves but still no output.  No issue with firing if running a normal engrave though?


---
**LightBurn Software** *January 22, 2019 00:59*

Have you enabled the laser?  (M4)


---
**LightBurn Software** *January 22, 2019 00:59*

then disable with M5


---
**Ryan G** *January 22, 2019 01:47*

Thank you! M4 now means laser is firing. I’m amazed this isn’t documented anywhere on the c3d site (that I can see anyway). Will continue to persevere with this.. thank you again for your help


---
**LightBurn Software** *January 22, 2019 01:52*

C3D makes the board, not the firmware. It is documented well on GRBL’s site.


---
**LightBurn Software** *January 22, 2019 01:53*

And if you had just drawn a simple box in LightBurn and clicked start, that would've worked too - it does all of that behind the scenes for you.


---
**Ryan G** *January 22, 2019 10:19*

How would drawing a square help me dial in pwm? C3d do not make smoothie either but this is documented on their site.



Does anybody have an email address for Cohesion please? Their web form is not working and giving a zoho smtp time out so I am unable to contact them.


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 15:37*

We ship the boards with the Smoothie firmware so typically people dial in their settings using Smoothie and the config (these settings sometimes being none at all, and other times being things like the speeds, size and PWM frequency for a particular laser), and then when they feel comfortable with good performance it is much simpler to simply port those values over to GRBL using the $$ commands. 



It's very important to use the grbl-lpc build that we provide - either on the C3D docs site or on Jim's embeddedtronics site, for the C3D Mini. 



Different lasers respond to different frequencies - PWM tuning is covered here: [cohesion3d.freshdesk.com - Troubleshooting/ FAQ : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq)



Again it's written for the Smoothie firmware since that's where everyone starts out with a C3D board. 



I do see that we had the contact form fail to send a message a few hours ago but not sure why - it appears to be working now.  Try again? 


---
**Ryan G** *January 22, 2019 18:38*

Ok I will load smoothie and try PWM config from there with pot set to 10ma.



When doing a greyscale engrave, what is a suitable level to set the pot?


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 18:40*

Yes, grab the stock files (firmware and config) from the dropbox link at the bottom of the C3D Mini install guide. 



The proper answer is "as low as possible so that you can get the maximum grayscale resolution.  But again, do PWM tuning as shown in that guide. 10mA is good. 


---
**Ryan G** *January 22, 2019 18:54*

Ok makes sense. And when engraving grayscale, if having pot as low as poss is the way, should my power be high to compensate?. A better way to ask this might be: ‘do I want 50% power on a 10ma pot’ or ‘100% power on a 5ma pot’ if 5ma is my desired ‘black’. Hope this makes sense!?


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 19:02*

The latter will get you better grays,  that is what we are trying to achieve. 



A number of customers have written that they set the pot once to a "safe" max value such as 15mA and leave that alone - that way the board can freely work in its full pwm range from 1-100% but your tube is safe.

I personally set the pot to what is the highest power needed for a particular job - that might be only a few mA to engrave  only or 12mA to cut through some plastic.  In this example, if I want to engrave and then cut, I would set to 12mA. 






---
**Ryan G** *January 22, 2019 19:06*

Ok thank you for the advise. I will do PWM tuning in smoothie and report back. Thanks


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2019 19:08*

The last bit I will leave you with for now is that these LPSU's vary in quality A LOT.  Some respond great to PWM in the full range (pot set high) and we've seen some, mostly really old ones, that won't respond at all.  This is what the tuning will tell us. 


---
*Imported from [Google+](https://plus.google.com/101539982197658337854/posts/VR6MnXXpQnk) &mdash; content and formatting may not be reliable*
