---
layout: post
title: "This is the second board I have bought form cohesion3d loved and had no problems with the first one should have kept that machine"
date: November 10, 2018 19:36
category: "C3D Mini Support"
author: "chad cory"
---
This is the second board I have bought form cohesion3d loved and had no problems with the first one should have kept that machine.



So I have a c3dmini  that has been giving me some problems. First it would not fire the laser and I reloaded firmware like 5 times tried all can't of things and it finally just started working. Second it will randomly restart. And now it only as one 1 red light that comes on.









**"chad cory"**

---
---
**chad cory** *November 14, 2018 00:17*

So no one can help 


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2018 02:30*

Try unplugging the memory card and all connectors except for power, then turn the laser on and tell me about LED activity. 


---
**chad cory** *November 16, 2018 00:55*

Just the vmot![images/b0ea2693af07f1cfdc0ad13dc1bf5929.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0ea2693af07f1cfdc0ad13dc1bf5929.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 18, 2018 02:12*

I’ve made a note to email you back on Monday with instructions to arrange a repair/ replacement board. 


---
**chad cory** *November 22, 2018 14:22*

**+Ray Kholodovsky** I have not got your email 


---
*Imported from [Google+](https://plus.google.com/+chadcory/posts/gBPkbSCHXcE) &mdash; content and formatting may not be reliable*
