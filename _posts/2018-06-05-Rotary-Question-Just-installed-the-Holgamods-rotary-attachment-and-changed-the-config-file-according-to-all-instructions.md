---
layout: post
title: "Rotary Question: Just installed the Holgamods rotary attachment and changed the config file according to all instructions"
date: June 05, 2018 15:50
category: "C3D Mini Support"
author: "Wayne Ryan"
---
Rotary Question:

Just installed the Holgamods rotary attachment and changed the config file according to all instructions. I'm getting power to the stepper but cannot for the life of me get it to turn. Question is: When changing config text the instructions are for adding Z or adding Z and A. Do I need to compile the the board for A axis like it says in the adding both? Confused beyond belief.





**"Wayne Ryan"**

---
---
**Wayne Ryan** *June 05, 2018 16:19*

Forgot to mention I also went to Tools and enabled Rotary on A Axis


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2018 16:35*

The instructions have files in them.  You need to put the new firmware file on the card (we have compiled the smoothie for rotary for you) AND you need to either modify your existing config file as shown or replace it with the config file.  Z is already and has to be in the config file, but if you are not using it then you want the config file called XYZA no endstops. 


---
**Wayne Ryan** *June 05, 2018 20:26*

**+Ray Kholodovsky** Thanks Ray I got it working


---
*Imported from [Google+](https://plus.google.com/101642450378746266949/posts/8mhyUKnpdFQ) &mdash; content and formatting may not be reliable*
