---
layout: post
title: "I was working with my K40 and all of a sudden I lost the connection"
date: November 26, 2017 23:48
category: "C3D Mini Support"
author: "bbowley2"
---
I was working with my K40 and all of a sudden I lost the connection.  I couldn't see the micro disk in explorer.  I tried to remove the sd card and it snapped in half.  The smallest sd card I could find is an 8GB card.  I copied the firmware onto the 8GB mini sd card but the C3D won't recognize the card.  In LaserWeb when I connect to the K40 it doesn't see the firmware.  There is only 1 green light on the C3D board.  Did I blow the board?





**"bbowley2"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2017 23:51*

Did you fully remove the old card from the slot? 



I need you to format the card FAT32 and put the config.txt file on it. Do this with power off. 



Then turn it on and tell me what LED's end up being on. 


---
**bbowley2** *November 26, 2017 23:53*

I got it all but the new card goes in both ways.


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2017 23:54*

I'm not sure what you mean. Please show pictures. 


---
**bbowley2** *November 26, 2017 23:56*

I can fire the laser manually.  When I send a job to the K40 thr head goes through the motion but the laser won't fire.


---
**bbowley2** *November 26, 2017 23:57*

I'' try try formatting.


---
**bbowley2** *November 27, 2017 00:50*

OK I formatted the sd card and loaded the firmware that came with the board.   The K40 ran but it wanted to start beyond the 200 mm vert size. Then I reformated the sd card and loaded the GRBL firmware.  I get the correct lights but the laser dead doesn't move.  

![images/03a8bae85f8207fbc3ef44329e423d52.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/03a8bae85f8207fbc3ef44329e423d52.jpeg)


---
**bbowley2** *November 27, 2017 00:51*

I'll start on this tomorrow morning.

We will figure this out.


---
**bbowley2** *November 27, 2017 15:30*

Got it working this morning.  Had to wiggle the sd card but I'm back in business.

Thanks for your help.  BTW do you repair/replace the C3D board's micro slot?


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2017 15:34*

Yep, that's what the LEDs sounded like. Green is good, but not all the LEDs on could be that it wasn't reading the card. 



Do you think your slot is damaged? 


---
**bbowley2** *December 04, 2017 19:43*

I know the slot is damaged. When the original card broke I had to open the slot a little to grasp the stuch piece.  

On a no ther note I'm looking at a 50W Chinese laser and I contacted the dealer and asked what the voltage is.  Here is his reply,

Message from 3rd party seller:

Dear Bryan,

Thank you very much for your message.



The voltage on the controller board and mainboard are same 24V power supply.



You could refer to your requirements and place the order. Product link is [amazon.com - Amazon.com: Iglobalbuy 50W CO2 Laser Engraving Machine Engraver Cutter Build-in Air Pump: Home Improvement](https://www.amazon.com/Iglobalbuy-Engraving-Machine-Engraver-Cutter/dp/B01CU5RMLW/ref=sr_1_1?m=A3TSCDICMCKXU7&s=merchant-items&ie=UTF8&qid=1512367398&sr=1-1&keywords=Iglobalbuy+50W+CO2+Laser+Engraving+Machine+Engraver+Cutter+Build-in+Air+Pump)



Warm welcome to purchase from us!



Any question you have, please feel free to contact me.



Thanks and regards,



Double 


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2017 18:40*

Per your question, I am not able to repair/ replace the MicroSD slot. 



Regarding the new machine: 24v is fine for the board.  I will recommend the same things as for the K40, which is to wire a separate 24v PSU since the included single one in many of these machines is underpowered and can brown out, causing issues. 


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/UqWdhebA7RQ) &mdash; content and formatting may not be reliable*
