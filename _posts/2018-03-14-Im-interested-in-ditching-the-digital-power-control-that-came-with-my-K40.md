---
layout: post
title: "I'm interested in ditching the digital power control that came with my K40"
date: March 14, 2018 11:37
category: "K40 and other Lasers"
author: "Jake B"
---
I'm interested in ditching the digital power control that came with my K40.  Its flakey and takes up an unnecessarily large piece of the panel.



With the C3D mini, I know that the power to the laser is controlled by pulsing L.  I also read that the P2.4 PWM output was tried for this, but that pulsing L was better, so the PWM output header is no longer used.



As I understand it, In the current "pulsing L" configuration, the power setting input to the power supply is essentially a current limit, setting the max power for when L is PWM=100%



I'm not super familiar with smoothie, but I was wondering if both could be done: PWM the L pin for controlling the power during the actual build, but also putting out a fixed value on P2.4 to set the max current.  I'm not sure if its possible to configure a user interface for such a setup.  I'd want 2 features in this regard:  the ability to set the absolute value for the PWM output (set the upper bound of the current limit) and a way to adjust the current power with the knob for when doing a "test fire".



Not sure such a thing is possible, and might just resort to a 10-turn pot.





**"Jake B"**

---
---
**Don Kleinschnitz Jr.** *March 14, 2018 12:00*

Not sure what you mean by: <i>I also read that the P2.4 PWM output was tried for this, but that pulsing L was better, so the PWM output header is no longer used.</i> 



I and others use P2.4 to <i>pulse</i> the L pin?



When using the Pot driving <i>IN</i> and Px.x driving <i>L</i>, the power setting is the product of the duty factor (DF) of both the "IN" and the "L" PWM. They collectively set the power.  Power =  IN(DF) * L(DF).



I interpret your above idea to consist of; adding an analog value to the IN of the LPS using P2.4? i.e replacing the pot.



P2.4 is a digital output and the IN pin needs an analog value. I do not know if the smoothie processor has a D/A?

You could use a PWM to analog converter connected between the P2.4 and the IN.



However, in all cases you would need firmware changes to get a user interface to the <i>IN</i> power control. 



In the configuration that I use;



You can set the range of PWM power control from the Px.xx (and therefore the program) in the smoothie configuration.



You can adjust the max power under test fire or L control with the POT....


---
**Jake B** *March 14, 2018 12:04*

When I said "P2.4" I was talking about the center pin of the K40 PWM header.  Is this the same pin as the laser fire pin on the power header? (Pinout diagram does not list the PIN number for laser fire)  If these are the same pin, I hadn't realized this, and I thought they were separate things that could be used independently.




---
**Jake B** *March 14, 2018 12:06*

Basically what I'm suggesting is a C3D mini controlled potentiometer hooked to IN, but as you said, UI changes would be needed so probably not so straightforward.  Probably just easier to put a pot in and be done with it.


---
**Jake B** *March 14, 2018 16:17*

One other question for folks.  The digital power control has a nice feature of an LED that indicates when the laser is firing (L is low).  It seems to be just an LED and a 1K resistor from 5v to the L (or K+ test fire) pin so when it goes low, the LED lights up. Has anyone added such an indicator separately from the digital power board?  If I'm replacing the power board with a POT, I'd like to add this LED separately. I don't see any issue with this, but I figured it didn't hurt to ask.


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2018 16:23*

What you describe is how we used to do the wiring, but found that removing the pot limited the maximum grayscale contrast possible. So it is best to get a pot (Don has some good recommendations) and an mA meter and leave the C3D config as is. 


---
**Don Kleinschnitz Jr.** *March 14, 2018 21:55*

**+Jake B** If I recall correctly, I am pretty sure that the Laser Instruction LED only activates with the Laser Test button. L does not go up to that board.



[http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html](http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)



Here is a post regarding using a LED/SW that may be useful.





[plus.google.com - Anyone used LED buttons for the laser power and test button? I can't figure o...](https://plus.google.com/113403625293456436523/posts/4ystTbEYGas)


---
**Jake B** *March 15, 2018 00:46*

**+Don Kleinschnitz** on my power supply the L (Laser Fire) and K (test fire) pins are connected internally in the power supply (confirmed with a continuity meter).  So if you had a LED connected in an active-low configuration, when L pin from the C3D board is commanding the laser to fire, then K pin would also go low and light the indicator.


---
**Jake B** *March 15, 2018 00:48*

Note: I had K and P confused. When I said P, I was referring to the test-fire input on the power supply.  I've updated the above posts to K which is what I meant.


---
**Don Kleinschnitz Jr.** *March 15, 2018 01:42*

**+Jake B** the test fire is usually isolated internally from the L with a diode so you don't ground what's driving L with the test fire.  Either yours is different or swap the dvm leads to see if there is a diode inline.

In any case whatever your end configuration, insure that the L line is pulled close to ground (no diode drops) if you want proper control.


---
**Jake B** *March 15, 2018 22:50*

**+Don Kleinschnitz** just checked and I get continuity between the test fire and laser fire pins on my power supply in either direction.


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/ErGEaRdxM15) &mdash; content and formatting may not be reliable*
