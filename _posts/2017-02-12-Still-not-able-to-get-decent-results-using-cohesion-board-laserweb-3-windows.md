---
layout: post
title: "Still not able to get decent results using cohesion board, laserweb 3, windows"
date: February 12, 2017 20:05
category: "FirmWare and Config."
author: "Chris Mann"
---
Still not able to get decent results using cohesion board, laserweb 3, windows. Pwm l to 2.5 the image that looks decent is from stock k4p board.



![images/2e6b69058c0845a156bf0a2d9ad344ec.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2e6b69058c0845a156bf0a2d9ad344ec.jpeg)
![images/b4b869edc2b0c643b5e61f556a92b77e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b4b869edc2b0c643b5e61f556a92b77e.jpeg)
![images/913f80174b5e84496b987ef1636c0be3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/913f80174b5e84496b987ef1636c0be3.jpeg)
![images/db8d5a9057807ca1252e56e17fec1904.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db8d5a9057807ca1252e56e17fec1904.jpeg)

**"Chris Mann"**

---
---
**Chris Mann** *February 12, 2017 20:11*

Tried beam size everything from .09 to .4 and either spacing or engraves entire area out. It also seems jumpy when moving on x 


---
**Chris Mann** *February 12, 2017 20:13*

Dont know if supposed to change anything in config besides the pwm pin which ive already done.


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 21:43*

**+Ray Kholodovsky** would be the best person to ask 


---
**Bryan Hepworth** *February 12, 2017 22:05*

I might have a slightly different board version to you Chris so I'm not commenting until I see Ray's response. I had the pre-release version sent me.


---
**Chris Mann** *February 12, 2017 22:31*

To L and changed the config to match


---
**Jonathan Davis (Leo Lion)** *February 12, 2017 22:43*

**+Bryan Hepworth** I received the same controller board as well. 


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 00:52*

Ok, so you're well aware of the changing beam diameter setting from 0.1 to 0.5...  No concerns regarding your other settings.  

I think we would like you to try jogging in each axis, say you tell it to move 10mm and then measure how much it actually moved. 

Please report back on that one.  

Other than that, right now the best thing to do is to get some other folks to contribute "known working" files, settings, and possibly gCodes.  

For example **+Bryan Hepworth** has a picture of a boat that he engraved using LaserWeb with this DPI, this beam diameter, and these power settings.  He provides those and shows a picture of the outcome.  You try the same image file and settings and see what the outcome is.  Then you try his gCode file and see what the result is.  



These 2 strategies of attack should help us narrow down the culprit. 



As far as all your boards, they are running a similar version but there's a tweak to the endstop pins so don't go sharing any full config files, but other than that, have at it. :)


---
**Chris Mann** *February 13, 2017 01:05*

I will get those measurements. Thanks Ray. And yes please if anyone has settings or gcode and examples of what looked like on your machine please share. 


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 01:09*

Thanks Chris, best of luck, and looking forward to hearing back about them. 


---
**Chris Mann** *February 13, 2017 01:59*

**+Ray Kholodovsky** i jog 10mm on x and y several times done test fire measured distance between dots and comes out perfect whether 5mm or 10mm  of distance i moved laser head


---
**Jonathan Davis (Leo Lion)** *February 13, 2017 02:07*

**+Chris Mann** Just as a suggestion and comparison but have you tried using Viscut?


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 02:15*

Ok, we've ruled out a bunch of hardware issues then.  



Let's do the 'compare to other folks' suggestion I outlined above.  



In fact, I'll ask people to contribute and we can start a repository of these things. 


---
**Chris Mann** *February 13, 2017 02:19*

Thanks again **+Ray Kholodovsky** and **+Jonathan Davis** no i have only tried laserweb with the cohesion board mainly because i read it was the best for engraving.


---
**Jonathan Davis (Leo Lion)** *February 13, 2017 02:25*

**+Chris Mann** Yeah, that is understandable.Though Ray's board is smoothieware based so your options are not limited.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 02:27*

And because I said to :)

I've never used Visicut so I would be of even less help if you did that.  Not saying you shouldn't though - could then teach me a few things about it I'm sure.


---
**Jonathan Davis (Leo Lion)** *February 13, 2017 02:28*

**+Ray Kholodovsky** indeed as it might be a possible option if laserweb3 does not work.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 02:28*

**+Chris Mann** this one's for you: [https://plus.google.com/u/0/+RayKholodovsky/posts/5Kb8zcC6uTP](https://plus.google.com/u/0/+RayKholodovsky/posts/5Kb8zcC6uTP)


---
*Imported from [Google+](https://plus.google.com/116957815076991050273/posts/Q3rm7wLGmjG) &mdash; content and formatting may not be reliable*
