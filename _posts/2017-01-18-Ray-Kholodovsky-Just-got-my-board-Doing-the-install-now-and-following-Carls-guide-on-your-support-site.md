---
layout: post
title: "Ray Kholodovsky Just got my board! Doing the install now, and following Carl's guide on your support site"
date: January 18, 2017 01:36
category: "General Discussion"
author: "Robin Sieders"
---
**+Ray Kholodovsky** Just got my board! Doing the install now, and following Carl's guide on your support site. It looks like I have the same model psu as Carl, with the screw terminal blocks, but I notice that it's highlighted on the support page that cutting the pwm cable isn't required. I have traced my pot wires to the psu, and there is no jst connector present. Should I go ahead and trim the cable as shown in the guide, or is the connection inside the psu?





**"Robin Sieders"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 01:37*

Please attach a picture of your PSU so that I can see. 


---
**Robin Sieders** *January 18, 2017 01:39*

Let me know if you want a better angle. Thanks!

![images/51dccf2caa2bf055d5178a5a37b9dd23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/51dccf2caa2bf055d5178a5a37b9dd23.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 01:49*

That note is for a different type of PSU that has the mating JST connector for the PWM cable.  Follow Carl's guide exactly, since you have the same PSU as him. 


---
**Robin Sieders** *January 18, 2017 01:53*

Perfect, thanks.


---
**Kostas Filosofou** *January 18, 2017 06:10*

I have also the same PSU and I have a question too.. if I want to keep my pot for example? I connect it all together?


---
**Carl Fisher** *January 19, 2017 17:46*

I'm not sure if you can keep your pot honestly. **+Ray Kholodovsky** can confirm though.




---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 17:50*

2 ways to wire the board. If you want to keep the pot then don't remove it from the psu, don't run the pwm cable, and you'll need to run a wire from L on the PSU to the bed MOSFET - pin. (Consult the pinout diagram) then change the laser pwm pin in config from 2.4! to 2.5


---
**Kostas Filosofou** *January 19, 2017 18:06*

ok thanks **+Ray Kholodovsky** i thing i got it ..  when you say L wire you mean the far right connection if i am right ?



[https://goo.gl/photos/Zzb1B5LDycRRuE4E7](https://goo.gl/photos/Zzb1B5LDycRRuE4E7)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 18:20*

That's the one!  

You can actually remove all the wires from that terminal, and run your own wires for 24v and gnd to the main power in terminal, and then L to the bed - (pin #4 on the bottom left that reads - P2.5/ Bed).

Entirely up to you. 

[cohesion3d.freshdesk.com - Cohesion3D Mini Pinout Diagram : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)


---
*Imported from [Google+](https://plus.google.com/+RobinSieders/posts/6nDKWnJB1WP) &mdash; content and formatting may not be reliable*
