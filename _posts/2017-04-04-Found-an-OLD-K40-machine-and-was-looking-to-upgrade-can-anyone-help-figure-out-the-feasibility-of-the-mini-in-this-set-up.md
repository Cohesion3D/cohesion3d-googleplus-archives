---
layout: post
title: "Found an OLD K40 machine and was looking to upgrade, can anyone help figure out the feasibility of the mini in this set up?"
date: April 04, 2017 09:21
category: "K40 and other Lasers"
author: "Baxter Smith"
---
Found an OLD K40 machine and was looking to upgrade, can anyone help figure out the feasibility of the mini in this set up?



![images/d077f728a773d31d68b5b3163f38f7c7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d077f728a773d31d68b5b3163f38f7c7.jpeg)
![images/155d7a817a7743179e0b477cfbb93312.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/155d7a817a7743179e0b477cfbb93312.jpeg)
![images/300247a6c174059e01a3c5acf94777ee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/300247a6c174059e01a3c5acf94777ee.jpeg)
![images/3f57d58078353b88c298b6abdc7fc697.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f57d58078353b88c298b6abdc7fc697.jpeg)
![images/3d3ce18b1a462ea2610e2923f4573f71.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d3ce18b1a462ea2610e2923f4573f71.jpeg)
![images/08c2933d549b517f411111bdacd8dd3d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/08c2933d549b517f411111bdacd8dd3d.jpeg)

**"Baxter Smith"**

---
---
**Joe Alexander** *April 04, 2017 09:28*

That's a REALLY old PSU, never seen it before. I would recommend a new Laser PSU to go along with the C3D Mini at the very least.**+Ray Kholodovsky** sounds like a perfect question for you :)


---
**Andy Shilling** *April 04, 2017 09:48*

Agreed take it all out, buy a new 40w psu and C3D mini. Have you plugged it in and tested the laser actually fires? 


---
**Baxter Smith** *April 04, 2017 10:19*

**+Andy Shilling** laser fires, controller works, the issue is working with moshisoft which is a horrendous software. 

**+Joe** **+Ray Kholodovsky** is it possible to use the existing psu? i already waited two weeks for the c3d mini which i now have and dont want to wait any more! is there a wiring diagram for the c3d in terms of how the power is plugged into the board?


---
**Andy Shilling** *April 04, 2017 10:25*

Yup lol i had it and a similar psu, there is no point having a C3D if you don't buy a new PSU. I would be very surprised if anybody would suggest using that psu you have there and doubt it would even be possible. The new upgrade PSU would take about an hour to rewire into one and give you so much more back.


---
**Baxter Smith** *April 04, 2017 10:38*

**+Andy Shilling** ok then, i guess i have one option here, where is the best place to get a psu? i found this one: [buildyourcnc.com - 40 Watt Laser Tube Power Supply](https://buildyourcnc.com/Item/Laser-Component-Power-Supply-CO2-40W)


---
**Andy Shilling** *April 04, 2017 10:42*

Where are you in the world? I got mine from aliexpress for $54 and had it in about 10 days to the uk.


---
**Baxter Smith** *April 04, 2017 10:50*

**+Andy Shilling** indonesia, but can source from thailand and singapore, can you share the link for aliexpress?




---
**Andy Shilling** *April 04, 2017 11:00*

New MYJG-40 220V/110V 40W CO2 Laser Power Supply PSU Equipment For DIY Engraver/ Engraving Cutting Laser Machine 3020 3040

 [http://s.aliexpress.com/EJbYNJbq](http://s.aliexpress.com/EJbYNJbq) 

(from AliExpress Android)


---
**Andy Shilling** *April 04, 2017 11:00*

[aliexpress.com - New MYJG-40 220V/110V 40W CO2 Laser Power Supply PSU Equipment For DIY Engraver/ Engraving Cutting Laser Machine 3020 3040](http://s.aliexpress.com/EJbYNJbq)


---
**Don Kleinschnitz Jr.** *April 04, 2017 11:48*

Please do not use that LPS.... it is well ....... DANGEROUS! That open frame is waiting to hit you with 20,000v. That silicon on the HV leads will do nothing but make you think its safe. $70 is not to much to pay to keep from getting fried. 



Is that really a 40W system I have only seen dual Flyback transformers on higher wattage lasers.  


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 17:59*

Well, of course I recommend heeding the warnings of the smart people in this community, and that PSU does seem unpleasant, to put it kindly. 

Anyways, we have to trace the wires going to the red board.  I'm looking for 2 motor cables, 2 endstops, 24v and Ground, possibly a 5v line (not needed but might exist), and an "L" (this would be between the red board and the psu and is what fires the laser).   


---
**Kirk Yarina** *April 04, 2017 23:48*

At what point does it make more sense to use this for spare parts and just buy a new K40?  Is this more rugged than the newer models?  How much life is left on the tube?


---
**Don Kleinschnitz Jr.** *April 05, 2017 00:50*

**+Kirk Yarina** 

Refurb:

..New tube: $200-300

..New LPS: $70

..New Steppers: $60

..New lens and mirrors: $40-60

Total: $370- 490



I bought mine for $450.


---
**Baxter Smith** *April 05, 2017 03:12*

**+Kirk Yarina** tube is still good, steppers all are good, lens and mirrors are good, the only thing now is the lps, so i will be getting one of those! i guess a new machine would be easier to get but the cost shipping and importing would also be quite high for me


---
**Baxter Smith** *April 05, 2017 03:22*

**+Ray Kholodovsky** i have the motor cables and end stops but they are wired differently than the board, and i have the 24v and two ground wires, still figuring out which one is the L but i imagine if i trace it to the right spot i should find it. if i do the wiring diagram seems to be L (space) Ground 24v, is this correct? does anything go in the space slot? 


---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2017 03:26*

All 3 have screw terminals. 

24v and Gnd at the top left,

L is bed - 2.5 which is 4th from left on the bottom strip. 


---
**Baxter Smith** *April 05, 2017 07:06*

**+Ray Kholodovsky** yes thank you! i have that setup now, just wondering about the end stops, are they next to the reset button? is there a correct order to put them in?


---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2017 16:32*

The end stops are the bottom row of white connectors. You should need the X and the Y one, putting the 2 wires from the switch between Sig and Gnd for each one. 


---
**Baxter Smith** *April 20, 2017 15:22*

**+Ray Kholodovsky** coming back with some more questions, the laser is not firing when running nor when testing with the laserweb interface, it does pulse fire when I go back to the machine interface and press "test" on top of the box. I feel I wired up everything correctly, do you have any suggestions?

Thank you for your continued help!

![images/68af57269838ce61df59635e2eacd713.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/68af57269838ce61df59635e2eacd713.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 15:25*

Probably a good idea to put something under the board so it's not sitting on metal. 

Run a line G1 X10 S0.4 F600

That should fire while moving. 


---
**Baxter Smith** *April 20, 2017 15:41*

**+Ray Kholodovsky** thanks for the reminder :) i ran the line G1 X10 S0.4 F600 and still no firing from the board, i put it in starting g code and also put it in the command line but still no firing, is there something else im missing?




---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 15:43*

Show me more pictures please 


---
**Baxter Smith** *April 20, 2017 16:07*

**+Ray Kholodovsky** 

![images/7c520b8ea180ca519bb81f050b0ebdeb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7c520b8ea180ca519bb81f050b0ebdeb.jpeg)


---
**Baxter Smith** *April 20, 2017 16:17*

**+Ray Kholodovsky** only been able to upload one photo, the rest get stuck. im going to do more trouble shooting, left this project 2 weeks ago from this issue and only now have the means to pick it up and back in action


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 16:19*

1 photo per post. Otherwise dump them all on imgur and put the album link here. 


---
**Baxter Smith** *April 20, 2017 23:29*

**+Ray Kholodovsky** 

![images/a64cb2130b792fd049bdd7beff0a2756.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a64cb2130b792fd049bdd7beff0a2756.jpeg)


---
**Baxter Smith** *April 20, 2017 23:35*

**+Ray Kholodovsky** what are you needing to see? I posted pictures of the old set up already, do these two new pictures help? Im going to resolder the ends of the wires, would that be enough to change this predicament?


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 23:48*

Please don't make changes like resoldering at this time, that would impede me trying to help you.  In regard to the 4 pin connector, are you sure the blue wire is the one to fire to the laser? If you connect that to ground, does the laser fire? 


---
**Baxter Smith** *April 22, 2017 04:16*

**+Ray Kholodovsky** switched these two and the board doesnt power on, im pretty sure the blue is 24v and the red is ground


---
**Ray Kholodovsky (Cohesion3D)** *April 22, 2017 05:29*

Could you please use a multimeter and verify? Connecting power backwards to the board is a great way to kill it. 

But yes if you were getting power before that would support the "before" way of wiring. And I had the wrong perspective. I meant, is the green wire in that plug the right one, if you touch that to ground will the laser fire? 


---
**Baxter Smith** *April 22, 2017 08:26*

multimeter tells me blue is power and red is ground, the green is the Laser and the yellow is the 5V, I know its hard to trouble shoot in this way but if there is something I can do and test that you think might work please let me know, the test fire button on the original laser interface still test fires the laser. if i run a file the motors move and if i hold down the test fire laser button as the gcode runs i can fake a test cut, hahaha, but this is obviously not ideal


---
**Ray Kholodovsky (Cohesion3D)** *April 22, 2017 15:38*

Yes, I am asking you to touch the green wire to ground and tell me if this fires the laser or not. 


---
**Baxter Smith** *April 24, 2017 02:24*

the laser doesnt fire with any combination of wire touching, the ground to the green blue or yellow


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2017 02:26*

Well, you need to start by figuring that part out, because one of those should be the "L" wire which is responsible for firing the laser.  Did it work before? 


---
**Baxter Smith** *April 24, 2017 04:44*

Yes laser worked before with the old board, i was able to use the moshisoftware which is terrible and why i decided to make the change. If I touch the right wires together the laser should fire, am I understanding this correctly? When I touch the ground and the blue wire there is a spark at the wires but nothing at the laser head. The laser head will fire if I press the test fire button


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2017 04:47*

**+Don Kleinschnitz**  **+Andy Shilling** do you guys know anything about this psu type? 

Baxter either can't identify the L line or can't get it to work properly. 

Yes, L to ground fires the laser on all the PSU's I have seen. I would not go randomly touching wires to ground as with 24v or 5v that would cause a short which is bad. Only L to ground. 


---
**Andy Shilling** *April 24, 2017 06:11*

**+Ray Kholodovsky**​ have you checked his config file to make sure it's using the right settings. I know I changed from a PSU like this to the newer type but I had to change to p2.5 and if memory serves I had to remove the ! to get mine to fire. 




---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2017 06:14*

2.5 is the default setting for the after the first batch boards. 


---
**Andy Shilling** *April 24, 2017 06:31*

So is this from the first batch then? I thought mine was. 


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2017 06:32*

V2.2 on bottom of the board is first batch, v2.3 is all future batches to present day. 


---
**Andy Shilling** *April 24, 2017 08:59*

**+Baxter Smith**​ I've been looking over your wiring and I'm trying to work out where the 5v yellow wire in the power in side of the C3D goes, it looks like it's connected to your lighting switch on the control board is this correct and if so what type of lighting are you running as i would presume this is for 5v power to the board not 5v out.



I'm trying to get my head around the wiring because there seems to be a lot there and not being able to see where everything goes to doesn't help.


---
**Baxter Smith** *April 24, 2017 11:24*

**+Andy Shilling** 

![images/9e21c3ff31170b908ca8ce8c665cf057.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9e21c3ff31170b908ca8ce8c665cf057.png)


---
**Baxter Smith** *April 24, 2017 11:25*

**+Andy Shilling** this is what I have for the wiring, would a larger diagram be helpful?


---
**Andy Shilling** *April 24, 2017 11:34*

I'm confused with that as you have the green going to ground and the 5v yellow going to 24v out am I reading that wrong? I'm on my phone looking at it.


---
**Andy Shilling** *April 24, 2017 11:46*

Where does the green wire go to? On the board or the small psu? 



Also on the main LP's you have what looks like 7 black/blue wires and a yellow in the same plug where does that yellow go to?


---
**Baxter Smith** *April 24, 2017 12:51*

**+Andy Shilling**

The green wire goes from the L slot on the 3D mini to the ground slot on the psu.

The yellow wire in this diagram only goes from the 5V on the 3D mini to the 24V on the PSU, there are other wires going from the buttons on the front of the machine to the light, one of which is yellow and is what you might be seeing



This was how the original set up had the wire arrangements


---
**Andy Shilling** *April 24, 2017 13:00*

**+Baxter Smith**​ ok but I'm hoping that the 24v hasn't blown something! **+Ray Kholodovsky**​ this parts for you.



Baxter is that yellow wire the one I see on the main board that is connected between the +5v and +24v or is that a different yellow if so where does that go.



![images/7bde947ffd7c3c19c9fa3fed5831f09e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7bde947ffd7c3c19c9fa3fed5831f09e.png)


---
**Baxter Smith** *April 24, 2017 13:34*

**+Andy Shilling** **+Ray Kholodovsky** The wiring is based off of what the original set up was. How to check if the 24v has blown?

Yes this is the yellow wire that connects +5v to +24v


---
**Andy Shilling** *April 24, 2017 13:41*

Ok lose the yellow from the C3D for now it doesn't need anything on the +5v anyway.  How do you know for sure it's 24v  on the lps end? 



Sorry for all the questions but i think I'm the only one here that had an old dog of a psu like yours in this community.


---
**Don Kleinschnitz Jr.** *April 24, 2017 13:45*

**+Baxter Smith** this pictures background is such that I can't see the txt, on my phone at least.


---
**Don Kleinschnitz Jr.** *April 24, 2017 13:47*

**+Ray Kholodovsky** if this is the open frame above I don't know anything about it. If this supply has to be used I suggest disconnecting it and testing it's inputs and outputs first. I will look closer once I am near a PC.


---
**Andy Shilling** *April 24, 2017 13:48*

**+Don Kleinschnitz**​ I was about to tag you in on this from another post, what's your view on the 24v in 5v line could that damage anything or are they protected?


---
**Don Kleinschnitz Jr.** *April 24, 2017 14:35*

**+Andy Shilling** not sure I understand your question? Are you asking if connecting 24v to the 5v will damage anything?


---
**Andy Shilling** *April 24, 2017 15:04*

**+Don Kleinschnitz**​ Yes from looking at the diagram Baxter put on ( the one you can't see properly) I've worked out he connected a +24 on the lps to the 5v of the C3D. It may be an input on the lps.


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2017 15:30*

The "+5v" pin of the k40 power connector on the Mini isn't connected to anything. 

Now, since the Mini doesn't need 5v, I recommend not dealing with that wire at all and just putting some electrical tape around it. 


---
**Baxter Smith** *April 24, 2017 23:34*

**+Ray Kholodovsky** great! eliminate one variable! 


---
**Baxter Smith** *April 24, 2017 23:45*

**+Andy Shilling** thank you for help and knowledge of this older psu set up! The 24v to power the motors/mini board is coming in on the blue wire from what I called the power board. If I plug in the 24v from the psu nothing happens. Whats the best way to test where the wires are connected to? Turn over the board and trace the headers?


---
**Andy Shilling** *April 25, 2017 05:17*

**+Baxter Smith**​​ Ok just to test this quickly as I believe the green should be the wire we need for the laser, can you pull it out of the power block that's it's in at the moment and fit it into p2.5  that's the blue screw terminal on the C3D third terminal in from the right as you look at the block of 6. This is where I fire my laser from so I'm guessing yours​ should be the same.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 05:20*

I would advise you to not plug things into the board until you figure out what's what and get the laser to fire.  Plugging things in incorrectly or backwards is a great way to burn the board. 


---
**Andy Shilling** *April 25, 2017 05:28*

**+Ray Kholodovsky**​ is right don't just plug and play. Ray I'm just advising to trying the L from p2.5 the green wire was already in L on the moshi board so in reality is got to be the missing connection as to why this isn't firing from the board surely.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 05:30*

Right Andy, the concern is that he can't get it to fire at all. Normally if you touch L to ground the laser fires, no board necessary. I'm trying to get that to happen, or figure out why it's not happening, before introducing the board to the mix. 


---
**Andy Shilling** *April 25, 2017 05:47*

Sorry my bad must have missed that part, it seems that he has that extra yellow +24 wire on there that I didn't have so I'm wondering if that comes into the mix somewhere then? 


---
**Andy Shilling** *April 25, 2017 06:17*

**+Baxter Smith**​ just out of curiosity are both HV leads connected to the laser tube post


---
**Baxter Smith** *April 25, 2017 07:35*

**+Andy Shilling** this is the HV leads out from the board, one goes to the laser tube directly and one goes through the resistor? to the other end of the tube. Is this what you are asking about? 

![images/6c70f8d2c0a5e22250feee4c674b21b3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c70f8d2c0a5e22250feee4c674b21b3.jpeg)


---
**Baxter Smith** *April 25, 2017 07:37*

**+Andy Shilling** **+Ray Kholodovsky** thank you both very much for your help! i really appreciate it and love trying to fix things like this, but i realize the change over of the PSU is a good investment and will be getting one in the next few days, with that said is this one that you have worked with before? [aliexpress.com - New MYJG-40 220V/110V 40W CO2 Laser Power Supply PSU Equipment For DIY Engraver/ Engraving Cutting Laser Machine 3020 3040](https://www.aliexpress.com/item/New-MYJG-40-220V-110V-40W-CO2-Laser-Power-Supply-PSU-Equipment-For-DIY-Engraver-Engraving/32664610332.html?shortkey=EJbYNJbq&addresstype=600)

and **+Andy Shilling** did you ever change over your PSU on your old machine? 


---
**Andy Shilling** *April 25, 2017 08:22*

**+Baxter Smith**​ yes that's exactly the one I use, it's very easy to wire in and will give you so much more from the C3D. I recommend finding the biggest hole you can and tossing that beast into it so nobody has to deal with it again 😉


---
**Baxter Smith** *April 25, 2017 08:40*

**+Andy Shilling** good idea! speaking of which, do you know any good diy builds for cnc or laser cutters? Im looking to source more open source digital fab tools, dont think i will be going with the cheap chinese version again


---
**Andy Shilling** *April 25, 2017 08:48*

Unfortunately I don't, I don't have time for all the things I HAVE to do so I stay away from researching the things I'd like to do.


---
**Baxter Smith** *April 25, 2017 13:42*

**+Andy Shilling** haha, well said. Last thing that I was wondering, do you have a gauge for how far away the laser lens should be from the material? Ive been guess checking and was wondering if I could make a stick to measure this, do you know the right height for this older style laser cutter?


---
**Andy Shilling** *April 25, 2017 13:54*

**+Baxter Smith**​​ I set mine to 50.2mm for engraving but if you're cutting you need to work from the centre of your material.



 Ie cutting 4mm acrylic you would need a distance of 52.2mm



I cut myself some acrylic rectangles and place them under the lens, I also etched the sizes into them and made up quite a few starting at 50.2 and going down in 1mm increments.



Obviously an adjustable bed helps as well 😁


---
**Baxter Smith** *April 25, 2017 14:01*

**+Andy Shilling** thanks Andy, that is super helpful, I have been bouncing around a few numbers so this helps skip more testing. Next step adjustable bed, need to have something to do tomorrow haha


---
**Andy Shilling** *April 25, 2017 14:19*

Z Adjustable Bed for K40 Chinese Laser found on #Thingiverse [thingiverse.com - Z Adjustable Bed for K40 Chinese Laser by brianvanh](https://www.thingiverse.com/thing:1906231)


---
**Andy Shilling** *April 25, 2017 14:20*

Baxter I'm having good results from this if you want a cheap solution for a bed.


---
**Don Kleinschnitz Jr.** *April 25, 2017 15:12*

**+Baxter Smith** na, send that lps to me for addition to the library of lps types?


---
**Baxter Smith** *April 27, 2017 02:47*

**+Andy Shilling** super cool, ill try to print these out and get it built in the next few weeks




---
**Baxter Smith** *April 27, 2017 02:47*

**+Don Kleinschnitz** sure, what do you need for the library?


---
**Don Kleinschnitz Jr.** *April 27, 2017 10:48*

**+Baxter Smith** I am collecting dead laser power supplies to be used  to research how they work, enable test and repair, and document the electronics. Yours is unique in that it is vintage, and has external components like ballast resistor and flyback. 



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Laser%20Power%20Supply)


---
**Baxter Smith** *April 28, 2017 11:49*

**+Don Kleinschnitz** sounds awesome! the machine is in indonesia, i can probably get it back to the states in a few weeks, where does it need to go?


---
**Don Kleinschnitz Jr.** *April 29, 2017 10:51*

**+Baxter Smith** I am in Sandy, Utah, 84092 USA perhaps its not worth the expense to ship it? No idea of the shipping cost?


---
**Baxter Smith** *May 29, 2017 23:45*

**+Andy Shilling** Hi Andy my PSU finally came in the mail and Ive tried wiring it up and havent gotten any power going to the board for some reason, do you have any documentation of how to wire it?




---
**Andy Shilling** *May 30, 2017 05:51*

**+Baxter Smith**​ could you show what you have so far, if any. There is a pinout diagram on the C3D website that could help.


---
**Baxter Smith** *May 30, 2017 08:20*

**+Andy Shilling** yes! found that and rewired and now i am cutting, the issues now turn to the laserweb interface, ive been scourging the google+ group for answers but cant find anything related, i import images and they come in way too big. ive tried every file format and it comes it too big and i cant scale it down, im using laserweb3 because laserewb 4 keeps crashing on me, is there another program you use for exporting to laser?


---
**Andy Shilling** *May 30, 2017 08:39*

Are you making sure your images are imported using the correct image size? ie bmp=300 pixel resolution I believe it is in LW. Have a look here I'm sure this explains it.



[https://github.com/LaserWeb/deprecated-LaserWeb3/wiki/Workflow:-Existing-Bitmap-Image](https://github.com/LaserWeb/deprecated-LaserWeb3/wiki/Workflow:-Existing-Bitmap-Image)


---
**Andy Shilling** *May 30, 2017 08:39*

[github.com - deprecated-LaserWeb3](https://github.com/LaserWeb/deprecated-LaserWeb3/wiki/Workflow:-Existing-Bitmap-Image)


---
**Andy Shilling** *May 30, 2017 08:43*

Also I had some issues with LW4 and windows 10 but there are some older versions that you can try. It's worth persisting with as LW4 is the best option by far.


---
**Baxter Smith** *May 30, 2017 09:38*

**+Andy Shilling** again, it seems i say something and it works, so im going to try it again, hahaa. I now have laserweb 4 up and running on ios, but now the files are cutting mirrored, is it just a simple switch of some wires on each of the axes?


---
**Andy Shilling** *May 30, 2017 09:41*

I would like to say yes but where does the laser home to? It should be top left. If it does home that way I would say it's a different issue.


---
**Baxter Smith** *May 30, 2017 11:49*

**+Andy Shilling** thats another issue, the end stops are not responding, is there an activation i am missing for these? the y end and x end and grounds are in the right spot but im not sure why the board is not picking up the signal when i press them to stop the laser head



i switched the wires around and now the machine is running in the proper orientation


---
**Andy Shilling** *May 30, 2017 12:29*

What pins are you using for end stops and do you have the original optical or have you swapped them out for the mechanical type?



Have you added the G28.2 code for your gone location?


---
**Baxter Smith** *May 30, 2017 12:45*

the end stops I have are original and look like this:



i added the g28.2 code for home but no homing sequence took place and triggering the end stops did not prevent the head from moving, wondering is there is another pin on the board to use?

![images/0b998a85b4191519aa42c255d0612176.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0b998a85b4191519aa42c255d0612176.jpeg)


---
**Andy Shilling** *May 30, 2017 12:57*

There are two sets of headers for end stops, your x axis would normally run off the ribbon cable though. Are you using the headers behind the main power connection on the C3D or the ones by the mosfet terminals? I have a different type to you but they should do the same job just wired differently.


---
**Baxter Smith** *May 30, 2017 13:42*

**+Andy Shilling** the pins i am using are the ones behind the main power connections, is it possible to use the ones next to the mosfet terminals?



![images/84ff3e36c78b1233ad1b3a1bd8bd82bf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/84ff3e36c78b1233ad1b3a1bd8bd82bf.jpeg)


---
**Andy Shilling** *May 30, 2017 13:51*

I'm not 100% on that best to ask **+Ray Kholodovsky**​. What is the extra blue wire in the picture? I think your endstops should use a common ground between them. Mine have the circuit board on with an led so I know if they are triggered so I use the headers by the mosfets.


---
**Ray Kholodovsky (Cohesion3D)** *May 30, 2017 14:02*

You don't need to do that for the endstops, the Endstops are also the bottom row of white headers, consult the pinout diagram for the X and Y ones. 

But do this: 

Head to the middle. 

Send M119 gCode. Move head to left and send again. And again for the back. See if the X and Y changes from 0 to 1 respectively. 


---
**Baxter Smith** *May 31, 2017 00:57*

**+Andy Shilling****+Ray Kholodovsky** thank you both for your help! yes I entered in M119 and when the stop is engaged for x and y 0 changes to 1, what would be the next step to include the home in this location? 



a last but not as critical situation is my end stops put the laser head home in the top left of the machine but the laserweb interface uses the bottom left as 0,0, should i just change the end stop on the y axis to be bottom instead of top?





![images/60b0f65cc7a9bc08e5aa1d57a652d396.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/60b0f65cc7a9bc08e5aa1d57a652d396.jpeg)


---
**Baxter Smith** *May 31, 2017 00:57*

![images/0333fd63bc743e3e7933186190afccb2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0333fd63bc743e3e7933186190afccb2.png)


---
**Ray Kholodovsky (Cohesion3D)** *May 31, 2017 00:58*

So when you run G28.2 the head should move back left towards the switches. If it does not you need to flip the appropriate motor wire until it does. 

Homing goes to 0,200 


---
**Baxter Smith** *May 31, 2017 00:59*

As a follow up, in laser web i hit home all and the laser went to the top left of the board by itself using the end stops, so yay! thank you **+Ray Kholodovsky**


---
**Baxter Smith** *May 31, 2017 01:14*

**+Ray Kholodovsky** yes G28.2 sets the zero there but the LW4 interface still shows 0,0 as bottom left, is there a way to change 0,0 to top left? i just posted in laserweb google group




---
**Baxter Smith** *May 31, 2017 02:49*

**+Ray Kholodovsky****+Andy Shilling** all set up! thank you both for your help, after two months finally got it working!


---
**Andy Shilling** *May 31, 2017 05:32*

**+Baxter Smith**​ well done sir, sorry for not inputting over the last four hours but I needs me beauty sleep 😉.  I'm glad it's all working for you now and look forward to seeing your next posts on what amazing things you designed and cut 👍


---
*Imported from [Google+](https://plus.google.com/107385189673563567882/posts/WCeLnrmfbFc) &mdash; content and formatting may not be reliable*
