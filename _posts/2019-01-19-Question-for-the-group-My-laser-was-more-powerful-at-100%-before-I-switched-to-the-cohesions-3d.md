---
layout: post
title: "Question for the group. My laser was more powerful at 100% before I switched to the cohesions 3d"
date: January 19, 2019 23:55
category: "C3D Mini Support"
author: "Brandon Brooks"
---


Question for the group. My laser was more powerful at 100% before I switched to the cohesions 3d. I have heard that there is a potentiometer, but I only see one on the stepper driver, not on the board itself. This website says to set the potentiometer to 10ma and then test different power levels. 1. Where is the potentiometer? 2. How do I connect to it with my multimeter to ensure I'm not over charging my laser? 3. Are there additional settings in the config file that would adjust the max output?





**"Brandon Brooks"**

---
---
**Brandon Brooks** *January 19, 2019 23:56*

[cohesion3d.freshdesk.com - Troubleshooting/ FAQ : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq)




---
**Joe Alexander** *January 20, 2019 01:05*

the referenced potentiometer is the one that comes on the front panel on stock K40's and similar lasers. It isn't a trimpot on the board itself. connects to control the 0-5v to the laser power supply that regulates the laser intensity. make sense?

  also in the config.txt file there is a line for laser max power that sometimes come set to .08(80%), this can be changed to 1 if needed.


---
**Brandon Brooks** *January 20, 2019 01:31*

Yes that makes perfect sense. I thought I was losing my mind. I'll take a look at the config file to see what that is set to.


---
**Brandon Brooks** *January 22, 2019 19:55*

**+Joe Alexander** That did the trick. My config file was set to .80 as the max. It seemse to be working now as well as it did before.


---
**Joe Alexander** *January 22, 2019 21:18*

right on, glad I could help :)


---
*Imported from [Google+](https://plus.google.com/103265107426522879893/posts/aCUGVfufTmr) &mdash; content and formatting may not be reliable*
