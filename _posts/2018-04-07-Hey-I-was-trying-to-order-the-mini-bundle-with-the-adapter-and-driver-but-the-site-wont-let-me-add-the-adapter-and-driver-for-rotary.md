---
layout: post
title: "Hey. I was trying to order the mini bundle with the adapter and driver, but the site wont let me add the adapter and driver for rotary"
date: April 07, 2018 02:58
category: "General Discussion"
author: "Angela Newingham"
---
Hey. I was trying to order the mini bundle with the adapter and driver, but the site wont let me add the adapter and driver for rotary.  Any ideas? Also the lightburn software, is there a free version or do i have to purchase for $30. Thanks 





**"Angela Newingham"**

---
---
**Chuck Comito** *April 07, 2018 03:11*

Can't speak for your first 2 questions but lightburn is $30 and there isn't a free version. There is a 30 day trial period though. 


---
**Angela Newingham** *April 07, 2018 04:28*

**+Chuck Comito** ok


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2018 15:07*

Are you getting the dropdown menus to select the driver and adapter? 

Maybe try a different browser. 


---
**Angela Newingham** *April 07, 2018 15:14*

**+Ray Kholodovsky** no ray when i select the arrow for drop down it doesn't do anything 


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2018 15:18*

Try a different browser. 


---
**Angela Newingham** *April 07, 2018 21:48*

that did ot, thanks 


---
**Angela Newingham** *April 07, 2018 21:48*

do you take paypal 


---
*Imported from [Google+](https://plus.google.com/101357182958712120601/posts/fCcSGqDzAsZ) &mdash; content and formatting may not be reliable*
