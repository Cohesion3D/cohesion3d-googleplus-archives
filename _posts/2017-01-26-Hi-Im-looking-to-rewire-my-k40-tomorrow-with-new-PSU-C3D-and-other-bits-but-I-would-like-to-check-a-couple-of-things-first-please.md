---
layout: post
title: "Hi I'm looking to rewire my k40 tomorrow with new PSU,, C3D and other bits but I would like to check a couple of things first please"
date: January 26, 2017 10:42
category: "General Discussion"
author: "Andy Shilling"
---
Hi I'm looking to rewire my k40 tomorrow with new PSU,, C3D and other bits but I would like to check a couple of things first please.



1 I have a water pressure switch what is the best way to connect it (is there an option to go straight to the C3D and modify the config file)



2 because I damaged my ribbon I take it I can run the steppers straight from the headers by the stepper drivers.



3 is the stop button a push to make so I can fit a new button run off the header behind the switch.



Thanks for any help given.





**"Andy Shilling"**

---
---
**Don Kleinschnitz Jr.** *January 26, 2017 14:20*

1. Wire it in series with the "Laser Switch". While your at it "Please" install interlocks on the covers (front & back) and its advisable to install a temp monitor for the water temp.

2. Yes, but its advisable as a minimum to use stranded (not solid) wire and minimize the cable movement. If it was me I would get the same cable material (the white with traces) that is the same pitch (you can only find it wider) and slice it to fit. This is because that white cable construction is made to flex. Standard wire "work hardens" and will eventually break. Finding intermittent's in a gantry harness is a bitch!

3. Not sure what you mean, **+Ray Kholodovsky** can tell you. 


---
**Andy Shilling** *January 26, 2017 14:47*

Thanks **+Don Kleinschnitz**​, I've got an external temp gauge so covered there, when you say wider do you mean with more tracks and do you know the exact pitch needed as I can find FFC with the same numbers on  ie awm 20624 80c 60v vw1, but I'm not sure if that means it's exactly the same pitch etc


---
**Don Kleinschnitz Jr.** *January 26, 2017 16:25*

**+Andy Shilling** the ribbon connector is:

CONN FFC VERT 12POS 1.25MM 



I have not been able to find a cable of that pitch AND 12 positions AND long enough. 



I have found some at 15 conductor but to short. I thought we could cut off the extra traces but I have not tried it.



Wallmart??

[https://www.walmart.com/ip/20Pcs-1.25mm-Pitch-15-Pin-AWM-20624-80C-60V-VW-1-Flexible-Flat-Cable-300mm/46934333?wmlspartner=wlpa&selectedSellerId=571&adid=22222222227034296999&wl0=&wl1=g&wl2=c&wl3=55753083338&wl4=aud-273067696542:pla-87044919938&wl5=9029746&wl6=&wl7=&wl8=&wl9=pla&wl10=111838760&wl11=online&wl12=46934333&wl13=&veh=sem](https://www.walmart.com/ip/20Pcs-1.25mm-Pitch-15-Pin-AWM-20624-80C-60V-VW-1-Flexible-Flat-Cable-300mm/46934333?wmlspartner=wlpa&selectedSellerId=571&adid=22222222227034296999&wl0&wl1=g&wl2=c&wl3=55753083338&wl4=aud-273067696542:pla-87044919938&wl5=9029746&wl6&wl7&wl8&wl9=pla&wl10=111838760&wl11=online&wl12=46934333&wl13&veh=sem)



[donsthings.blogspot.com - K40-S Middleman Board Interconnect](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)




---
**Andy Shilling** *January 26, 2017 16:31*

I've just spoken to a contractor here in the UK and basically I've got no chance of buying the exact replacement so it's either the middle man board to extend our come up with some other way to make the repair.


---
**Don Kleinschnitz Jr.** *January 26, 2017 17:01*

**+Andy Shilling** have you tried repairing the cable? Can you post a picture of the damage?


---
**Don Kleinschnitz Jr.** *January 26, 2017 17:15*

Solder a couple of these together ?

[http://www.ebay.com/itm/12-PIN-RIBBON-FLAT-FLEX-CABLE-250mm-Lenght-by-1-25mm-Pitch-Connector-/282197408171?&_trksid=p2056016.m2516.l5255](http://www.ebay.com/itm/12-PIN-RIBBON-FLAT-FLEX-CABLE-250mm-Lenght-by-1-25mm-Pitch-Connector-/282197408171?&_trksid=p2056016.m2516.l5255)


---
**Andy Shilling** *January 26, 2017 17:22*

I have made the repair by soldering tracks 1,3,5,7,9,11 one side and even numbers on the opposite side to make sure I don't short it. Followed by a good covering of hot melt glue but I would still be happier replacing it if I can.


---
**Don Kleinschnitz Jr.** *January 26, 2017 17:47*

**+Andy Shilling** I would feel the same. That's why its on my list to get a source.


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 20:58*

3.  KILL button and Kill button header.  Connect the 2 header pins = activated.  That is indeed a "Push to Make" or Normally Open type of switch. 


---
**Andy Shilling** *January 26, 2017 21:00*

Cheers **+Ray Kholodovsky**​ thought it best to check lol


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/inLW5bHFak8) &mdash; content and formatting may not be reliable*
