---
layout: post
title: "Hi! I use a K40 with the C3D Mini Laser"
date: November 18, 2018 09:52
category: "K40 and other Lasers"
author: "Joel Wedberg"
---
Hi! I use a K40 with the C3D Mini Laser. Most of it works fine but I have an issue with the X-axis.

The axis doesnt move smooth as it do on several YT-videos. It go for full travelspeed, then retards while lasering and then go full again. The results is a very uneven motion on the X-axis that will probably brake som mechanics sooner or later.



Does anyone have a idea how to fix it?





**"Joel Wedberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 03:20*

I would need more details, not entirely sure what you are asking. 


---
*Imported from [Google+](https://plus.google.com/109885619104860254968/posts/gb4KdmJrLHr) &mdash; content and formatting may not be reliable*
