---
layout: post
title: "I got a cohesion3d board last week"
date: April 29, 2018 21:22
category: "C3D Mini Support"
author: "james blake"
---
I got a cohesion3d board last week.  I just installed it in my system.  When I plug it into the usb cable it connects to the computer and all 5 lights light up and the 2-3 light blinks but when I turn on machine the first five light stay green and 2-3 blink but the vmot lights red.  I have tried to reset and have removed all cables except the laser and 24v cables and no change.  I have not down loaded or installed the software yet.





**"james blake"**

---
---
**james blake** *April 29, 2018 21:24*

I just tried without the usb cable connected and the same result.


---
**Ray Kholodovsky (Cohesion3D)** *April 29, 2018 21:33*

Sounds normal. What's the problem you are trying to solve? 


---
**james blake** *April 29, 2018 21:52*

I did not know if the red vmot light was normal or a problem,  I have seen that you needed the first 4 light and 2-3 blinking but did not see anything about a red vmot.  I thought it was an error light.


---
**Ray Kholodovsky (Cohesion3D)** *April 29, 2018 21:58*

A lot of people think that it seems. It just means the board is getting 24v power from the laser. Carry on! 


---
**james blake** *April 29, 2018 22:20*

Ok thanks, it is just when see red light that had always meant error or trouble.  Now just have to figure out the x-y motor connection and end stops and I might have it working.  At lest now I know every thing is starting up normal.


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/fejG5KPG1kM) &mdash; content and formatting may not be reliable*
