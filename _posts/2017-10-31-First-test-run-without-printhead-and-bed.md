---
layout: post
title: "First test run without printhead and bed"
date: October 31, 2017 21:45
category: "Show and Tell"
author: "Marc Pentenrieder"
---
First test run without printhead and bed. It does what it should do ;-)

Any suggestions how to record without sound or delete sound from recorded videos on android ?





**"Marc Pentenrieder"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 04:48*

Cool. How is the printbed going to work? (What surface, heating? - how?) 


---
**Ashley M. Kirchner [Norym]** *November 01, 2017 05:28*

<though bubble> bonfire right in the middle of the bed, then spread out the hot coal for even heating </though bubble>


---
**Marc Pentenrieder** *November 01, 2017 06:54*

**+Ray Kholodovsky** The printbed will use 2x 380V 2.5KW heating mats, each 60cm by 120 cm. As the print surface I thought I would buy a carbon plate 130cm by 130 cm 1 to 2 mm thick, perhaps with a 2cm thick aluminium plate under it to spread the heat evenly.


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 14:04*

Would the carbon plate be flat enough? 


---
**Marc Pentenrieder** *November 01, 2017 15:04*

**+Ray Kholodovsky** I have good results with my Creality CR-10 and carbon plate 30x30cm. I will also try to use a bltouch to do bed leveling.


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 15:06*

How do you get 380 volts? 


---
**Marc Pentenrieder** *November 01, 2017 15:09*

**+Ray Kholodovsky** By using 3 phases each 220 Volts


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 15:09*

3 phase. Got it. 


---
**Marc Pentenrieder** *November 01, 2017 15:14*

It is AC not DC ;-)


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 15:15*

I know. Please don't die while handling it :|


---
**Marc Pentenrieder** *November 01, 2017 15:16*

**+Ray Kholodovsky** The heated bed will be driven by an chinese 100A SSR, which will be driven by the cohesion3d remix board


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/7TtLSe4pXwe) &mdash; content and formatting may not be reliable*
