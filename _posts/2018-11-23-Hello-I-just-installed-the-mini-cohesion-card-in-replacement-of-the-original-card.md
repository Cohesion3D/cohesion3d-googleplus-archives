---
layout: post
title: "Hello .I just installed the mini cohesion card in replacement of the original card"
date: November 23, 2018 17:13
category: "K40 and other Lasers"
author: "Philippe Thonnard"
---
Hello .I just installed the mini cohesion card in replacement of the original card. I'm having trouble.

I work with windows 10 and lightburn. The model is a k40.

The X axis is working properly, but the x min end stop do not work.

Y and Z axis motors are not powered.

the laser test works.

Thank you for helping me troubleshoot this machine.

Regards. Philippe

ps : excusez mon anglais approximatif !!!



![images/2452f2d12daafc20141b5cd538ded74c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2452f2d12daafc20141b5cd538ded74c.jpeg)
![images/2be70b0adb77cd2e5f75ef99348b213b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2be70b0adb77cd2e5f75ef99348b213b.jpeg)

**"Philippe Thonnard"**

---
---
**Jim Fong** *November 23, 2018 17:29*

Looks like you have the 4pin stepper driver connector plugged into the k40 endstop port.  Disconnect and re read the installation manual. 


---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2018 18:14*

Yep 


---
**Philippe Thonnard** *November 23, 2018 18:14*

**+Jim Fong** 

Thanks Jim. actually bad connection !!!.

I corrected . X and Y work but not the Z. And still not the endstops. Philippe

![images/eb2130c87957923567c8bcf51778cdd7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eb2130c87957923567c8bcf51778cdd7.jpeg)


---
**Jarrid Kerns** *November 25, 2018 02:56*

**+Ray Kholodovsky** Your board was mentioned in the failed laser tube hack a day post. You may see an increase in customers.


---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2018 02:59*

That may have explained yesterday.  But I launched LaserBoard yesterday... so I thought that was it. 


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 03:20*

**+Philippe Thonnard** not following the instructions can certainly cause problems. 

I would need more information about what actually is and is not happening in order to be able to help you. 

[plus.google.com - A public service announcement: Everyone, we want to help you, really, we do....](https://plus.google.com/u/0/+AshleyMKirchner/posts/DAwQYWRrZnD)


---
**Philippe Thonnard** *November 27, 2018 14:17*

**+Ray Kholodovsky** 

 Hi Ray. Thank you for interest in my problem:

X and Y engines work well and in the right direction. Ends stops X and Y too. the laser works too.

I installed lightburn: no problems.

What does not work: the Z engine does not turn and does not seem powered. enstop min Z is detected (0/1)

Another problem: I find that the power supply makes a lot of noise.

thank you for your help .

Philippe

![images/369c73ea69072a5c9398dd03890f34d0.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/369c73ea69072a5c9398dd03890f34d0.png)


---
**Ray Kholodovsky (Cohesion3D)** *December 05, 2018 22:40*

This is what we recommend doing: 



[cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)






---
**Dave Zerby** *December 21, 2018 00:36*

Hey Guys, could you post a pic of where you moved your x-y plug to please? I need to do the same I think as my laser head does not move with lightburn.




---
*Imported from [Google+](https://plus.google.com/113074176061576929679/posts/JLCPCYSYASg) &mdash; content and formatting may not be reliable*
