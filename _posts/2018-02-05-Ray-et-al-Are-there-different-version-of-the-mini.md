---
layout: post
title: "Ray et al- Are there different version of the mini?"
date: February 05, 2018 19:15
category: "C3D Mini Support"
author: "Dennis P"
---
Ray et al-



Are there different version of the mini? 



This pic shows 6 endstops, but the description of the board says 3. 



[http://cohesion3d.com/product_images/uploaded_images/talos-tria-with-c3d-mini-5.png](http://cohesion3d.com/product_images/uploaded_images/talos-tria-with-c3d-mini-5.png)



If we are limited to 4, can we wire the MIN stops for homing and wire the rest to one of the MAX's in series to act like a stop limit switch?  



I am building a Eustathios currently using a leftover RAMPs 1.4 and am looking to upgrade once I get it printing. 



Thanks



Dennis







**"Dennis P"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2018 19:19*

There is only one version of the Mini, it is the main product photo.  

There are 4 "protected" endstop inputs and the remaining 2 are exposed as simple GPIO pins.  You can plug switches into them also. 



The picture you reference is a pre-production prototype that Eric was testing for me.





[cdn8.bigcommerce.com](http://cdn8.bigcommerce.com/s-kq4bjok/images/stencil/1280x1280/products/115/389/DSC_2618__75879.1493310813.JPG?c=2&imbypass=on&imbypass=on)


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2018 19:51*

Mini Pinout Diagram: [cohesion3d.freshdesk.com - Cohesion3D Mini Pinout Diagram : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)


---
**Dennis P** *February 05, 2018 20:35*

Thanks Ray! 




---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/GTudJmwzSYG) &mdash; content and formatting may not be reliable*
