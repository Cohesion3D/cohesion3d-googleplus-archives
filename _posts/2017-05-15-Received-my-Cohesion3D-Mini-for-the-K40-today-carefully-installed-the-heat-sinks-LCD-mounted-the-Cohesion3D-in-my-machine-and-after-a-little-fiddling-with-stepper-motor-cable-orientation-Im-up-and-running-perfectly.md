---
layout: post
title: "Received my Cohesion3D Mini for the K40 today, carefully installed the heat sinks, LCD, mounted the Cohesion3D in my machine and after a little fiddling with stepper motor cable orientation I'm up and running perfectly"
date: May 15, 2017 21:01
category: "General Discussion"
author: "John Milleker Jr."
---
Received my Cohesion3D Mini for the K40 today, carefully installed the heat sinks, LCD, mounted the Cohesion3D in my machine and after a little fiddling with stepper motor cable orientation I'm up and running perfectly. 



LaserWeb was easy to install with the settings in the guide. Amazingly easy and I'm glad to now have a device that takes standard GCode and an app that uses my already CNC and 3D design programs.



Is everyone mounting their LCD's on the outside of the machine? To me seems like it's not going to get much use, and while I love having it in case of issues, it might find a home inside the cutter.



Thanks!





**"John Milleker Jr."**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 21:03*

Yeah, there's a bunch of laser cut panel designs out there and also a 3d printable attachment for the LCD as well (search this community and thingiverse) 


---
**Jim Fong** *May 15, 2017 21:19*

I use it quite a bit. For large/long raster engravings, I always copy the gcode to the sdcard.   I use the  LCD knob to select and "play" the gcode.  It runs much smoother without stuttering.  Also less chance of windows doing something silly to mess up the job.  Some of my engravings take a hour or more.  Usually use the time waiting for the engraving to be done working on the next one in laserdraw or use it to control one of the cnc machines 



Oh yea congrats on the upgrade!!!


---
**Anthony Bolgar** *May 16, 2017 00:27*

I'm with Jim, I run big jobs off the SD card as well.


---
**John Milleker Jr.** *May 16, 2017 09:49*

Thanks Gents, makes sense. Haven't thought of the big jobs yet. The 3D Printers run a front end that basically does the same thing. Basically runs an interface that stores the gcode locally. Hmmm, looks like LaserWeb has a Raspberry Pi version. Might be the next upgrade.


---
**Steve Clark** *May 16, 2017 16:56*

Oh boy, you guys have opened up a lot on questions for me.



First the SD card… Are they all standard at that size? I’ve only seen two sizes and this slot appears to be for the larger also is 32g memory is enough? it seems like it would be.



 I never paid much attention to this stuff so I’m on a learning curve here. In fact, it took me a couple of reads to figure out what you guys were even talking about.



Second, once it's installed how do you direct a program to be stored and read from there? 




---
**Steve Clark** *May 16, 2017 17:04*

Mine is mounted but you can access the SD slot.

The LCD is mounted on standoffs to clear the pins and make it fluse to the cover.

![images/acbef5ddf8a0f350e42b0b7cc8de083a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/acbef5ddf8a0f350e42b0b7cc8de083a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2017 17:06*

You don't use the SD slot in the GLCD, you use the MicroSD card that's in the board.  You can literally take it out and put files on it in your computer or transfer over USB (the board shows up as a drive in your computer). 

Is 32gb enough? The included one is 2gb. 

You can run a job on the MicroSD Card by using the menus on the GLCD. 


---
**Steve Clark** *May 16, 2017 17:09*

Panel mount of the LCD pic of the display side.

![images/6b090e2f63d09c43b06a4f6631dc4688.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6b090e2f63d09c43b06a4f6631dc4688.jpeg)


---
**Steve Clark** *May 16, 2017 17:21*

**+Ray Kholodovsky** Thanks Ray... that clears that up for me!


---
**John Milleker Jr.** *May 16, 2017 17:42*

Nice lookin' mount job on that LCD, **+Steve Clark**. 



I've bought a 32GB card for mine, format it as FAT32 from what I've read around the community. Still not sure if I really need that much space, maybe I'll swap it out with an 8 or 16GB in another device.




---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/WQUQV2xksQk) &mdash; content and formatting may not be reliable*
