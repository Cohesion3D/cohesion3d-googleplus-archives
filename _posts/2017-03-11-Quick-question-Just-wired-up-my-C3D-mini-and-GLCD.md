---
layout: post
title: "Quick question. Just wired up my C3D mini and GLCD"
date: March 11, 2017 20:31
category: "General Discussion"
author: "Arash Sepehri"
---
Quick question.  Just wired up my C3D mini and GLCD.  Pretty certain that I have everything wired correctly and correct direction.  During my first test power-up, the LCD screen lights up, but remains blank.  Should it be displaying anything, or do I have to mess with some code on the card?  Thanks





**"Arash Sepehri"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 20:33*

GLCD works out of the box. 

Possible issues include bad contrast setting (pot on lower left), inverted connectors on the GLCD (see my pics in a moment), or DOA GLCD (get Amazon to replace it). 


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 20:34*

Verify against my pics: [dropbox.com - C3D Mini Laser v2.3 Photos](https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0)


---
**Arash Sepehri** *March 11, 2017 20:43*

Oh, I have your pics bookmarked :)



Given your choices, sounds DOA, dang it.  Wires are correct (I'll triple check again), and I've tweaked with contrast.  Contrast fades in and out a grid of boxes, but no text still seen.



Should I verify files on SD card before I send it back, or just send it back?



Ray, thanks for that super fast response!




---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 20:45*

I don't know.  We have had DOAs before for sure.  If you want to grab another MicroSD Card, format it, and put the config file on it - that would be good to check.  I don't necessarily want people fiddling with the stock card and files "just because".  


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 20:46*

Stock Sd files: [dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)


---
**Arash Sepehri** *March 11, 2017 21:05*

Second SD card produced same results.  Will return to amazon for replacement.  I'll report back once replacement arrives.  Thanks again.  




---
**Arash Sepehri** *March 11, 2017 22:28*

Off topic, but for sake of not cluttering board.  Everything seems to be working great (minus GLCD), with one exception.  The laser is not cutting all the way through material, regardless of the power setting (on pot or lw3), or the number of passes.  It is as if the laser power has been capped, resulting in only cutting so far down.



Is this a pwm setting in config that I need to adjust, or a laserweb setting I have incorrect?


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 22:30*

Yes, there is a max power of 0.8 enabled in the config.  You can set this to 1.0 or comment it out with a # in front. 



Running tube at full power has been shown to decrease lifespan. 


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 22:32*

Oh and in regard to both of these things.  You are aware of save, eject, and reset/ power cycle the board to get the config changes to kick in? 


---
**Arash Sepehri** *March 11, 2017 22:38*

Thanks.  Most definitely on the board reset question.  I saw that line, but I do not believe it is even cutting at 0.8.  As 80% has been more than adequate on the chinese board (usually had Pot at 50% or less for usage).  For what its worth, the mA gauge is reading appropriately.  And the test fire (on K40) button fires laser at correct strength.  


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 22:44*

I think you need to test PWM.  Set pot at "known value" confirmed by test fire and gauge.  I say 10mA. 



Run G1 lines such as G1 X10 S0.5 F600 (change the X values and S values)  and check that the proportions match up.  



You may need to set PWM period from 200 to 400 in config if your pwm isn't working.   But almost always, when PWM doesn't work then it's just firing at full power.  So I'm not sure what's going on, provide more info,  but step 1 is to ensure that a full power G1 cut lines up (either 80 or 100%) depending on the max power config line. 


---
**Arash Sepehri** *March 12, 2017 00:47*

I feel like the PWM is working, and no noticeable difference when changing 200 to 400.  The mA gauge steps up accordingly per the GCode.



I'm having a hard time even cutting through cardboard, the cut never makes it all the way through.  Even if the mA gauge is reading 18-20mA, or 10mA.  Also multiple passes makes no difference.  Can't cut 1/8 acrylic either, which used to cut like butter!



Still need to test changing power from 0.8 to 1.0, but I'm taking a break to go grab me and the wife some dinner.  Any other suggestions in the mean time?  Need some pictures, or any more info?



Just to make sure, here is the process I do when changing settings.

-open config file

-save changes

-eject from computer OS

-unplug USB (not sure if needed, but just in case)

-press the reset button (small metallic button next to kill knob)

-wait for LEDs to return to normal, and power completely off and back on (also I think not needed, but wanted to be sure!)

-Plug everything back up and test


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 01:09*

This is concerning. If the gauge is showing 18mA and you can't cut through cardboard I'd stop using right away and investigate tube, electrical connections, and mirror alignment. 

17mA is already extremely high in regard to tube life. 

**+Don Kleinschnitz** may have more thoughts. 


---
**Arash Sepehri** *March 12, 2017 04:30*

Just to be safe, I did inspect tube, and all electrical connections.  As far as my eye can tell, everything does look to be intact.



My primitive understanding of how this machine works, makes me think it is something with the board, config file, or my laserweb settings. Reason being, when I press the mechanical test fire switch, the machine fires full power of the pot limiter just fine.  And it was working just fine prior to board swap.



Finally had a chance to switch 0.8 to 1.0 and that does seem to have significantly made a difference.  Too late now to conduct a full test, but I'll report back.



Thanks for being patient as I debug!




---
**Joe Alexander** *March 12, 2017 11:34*

I would definitely spend the time to recheck beam path and alignment, making sure the beam isn't being "clipped" by mirror edges or air assist nozzle(if relevant). Also make sure your focus lens is bump up, this is one of the most common errors and has these symptoms as a result.


---
**Arash Sepehri** *March 12, 2017 13:34*

Thanks Joe.  That was my plan today, to completely realign everything.  It appears my last post in saying changing 0.8 to 1.0 was a bit premature.  As I'm still having some difficulties.




---
**Arash Sepehri** *March 16, 2017 02:05*

So did a complete re-align and cleaned all mirrors and lens.  Seems to work much better now!  Caught cardboard on fire when I thought i was set to low!



Quick question.  This may be super simple thing that I oversaw and can't figure out.  Why does LaserWeb3 mirror everything?




---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 02:06*

The answer, is that you are thinking one way and it works a different way. Please describe the issue symptoms. 


---
**Arash Sepehri** *March 16, 2017 02:14*

That is likely the case Ray! :)



Picture says more than words.  That is supposed to say Cardboard (ignore the irony of it being on ply).  Images appear correctly on laserweb.





![images/77478b32c8e34e6d382185f249ecbbee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/77478b32c8e34e6d382185f249ecbbee.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 02:15*

Run a G28.2 gcode. Does it home to the back left? 


---
**Arash Sepehri** *March 16, 2017 02:17*

Will do as soon as this job is done, as mirrored or not, it will still answers my greyscale question! 


---
**Arash Sepehri** *March 16, 2017 02:19*

Oh, and another update, the new GLCD came in today.  Works great.  The current job was run off the SD card, much better than moving laptop in and out of garage!




---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 02:20*

Cool. On that note, if you can get Ethernet in the shop and can get a LAN8720 module you can upload over the smoothie web interface. 


---
**Arash Sepehri** *March 16, 2017 02:25*

Stop feeding the monster!  I've already seen some headings about an ESP8266 mod that I've refrained from reading!



Considering my office shares a wall with the office, ethernet may definitely be in the future.  Then I'll have to get camera setup to monitor remotely.  It's an ever hungry monster!  The more you feed it, the more it wants!


---
**Arash Sepehri** *March 16, 2017 02:49*

G28.2 takes it to the bottom left.  To make sure we are on the same page, bottom is the side nearest me if I am standing in front of the machine.  


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 02:52*

Yeah, you need to power down and flip the Y motor cable 180 degrees. 


---
**Arash Sepehri** *March 16, 2017 02:57*

Another unrelated question, why does it do this to the top right of the square.  I noticed it also doing this before I had everything aligned, but I just assumed I had messed up the AI file (completely separate file than this one that I downloaded.





![images/cc7985d9c184c146b52cceda6bf1f741.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc7985d9c184c146b52cceda6bf1f741.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 03:03*

That last one is a question for the K40 group, and you should post the files, settings, etc. It's not necessarily a hardware question. If you said that all the corners were weird I might think your acceleration or speed was a bit high. But that one corner? It's either the file or how the Gcode is being generated. 


---
**Arash Sepehri** *March 16, 2017 03:10*

Will do!  I flipped the Y cable.  But it is bedtime for me, so I'll report back when I have an update.  Heading out for vacation soon, so it may be a week+ until I get to play again.  Ray, thanks again for all the help, you have gone above and beyond the expected support!  This group of people here is amazing!


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 03:12*

Enjoy the vacation. I'm going to Midwest RepRap Festival next weekend. That'll be a nice 10 hour drive. 


---
*Imported from [Google+](https://plus.google.com/116731587725016526079/posts/TNMUkP1pM5U) &mdash; content and formatting may not be reliable*
