---
layout: post
title: "I upgraded with the mini 3d board a couple of weeks ago, and today it started cutting images mirrored, and also the program and the board do not match were i put my image"
date: December 09, 2018 05:42
category: "C3D Mini Support"
author: "Paul Rodenborn"
---
I upgraded with the mini 3d board a couple of weeks ago, and today it started cutting images mirrored, and also the program and the board do not match were i put my image. But it will go to the correct corner when i home it.

I have the same results from 2 different computers, so think setting  on board or sd card were changed some how.

Need help trying to fix.





**"Paul Rodenborn"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2018 05:49*

I will place a relatively large bet (like I am literally calling my bookkeeper as I type this) that you have selected the wrong origin in LightBurn. For the K40, it should be the front left. Yes I know the limit switches are in the rear left. 

Homing should put your head to the rear left where the switches are. But origin is different. 


---
**Paul Rodenborn** *December 09, 2018 06:05*

it was working perfect, then all of a sudden it started cutting images mirrored...will check the origin settings.


---
*Imported from [Google+](https://plus.google.com/101523489494247053011/posts/cLdkJppeFCZ) &mdash; content and formatting may not be reliable*
