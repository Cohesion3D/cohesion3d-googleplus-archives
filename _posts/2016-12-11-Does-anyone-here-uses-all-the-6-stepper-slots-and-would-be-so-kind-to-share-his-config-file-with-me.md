---
layout: post
title: "Does anyone here uses all the 6 stepper slots and would be so kind to share his config-file with me?"
date: December 11, 2016 16:19
category: "General Discussion"
author: "Ren\u00e9 Jurack"
---
Does anyone here uses all the 6 stepper slots and would be so kind to share his config-file with me? I tinkered two days now, and am only able to get 2 Extruders up and running. But not 3.





**"Ren\u00e9 Jurack"**

---
---
**Eric Lien** *December 11, 2016 16:21*

I think **+Ray Kholodovsky**​ is out of a robotics competition this weekend. I'm sure he'll chime in once he gets back.


---
**René Jurack** *December 11, 2016 16:22*

thanks for the info, **+Eric Lien** :)


---
**Griffin Paquette** *December 11, 2016 20:59*

**+Eric Lien** beat me to it :-)


---
**Dushyant Ahuja** *December 11, 2016 22:58*

I think the firmware needs to be compiled with a specific parameter for 3+ extruders. That might be the issue. I remember reading something in the Smoothie documentation. But will have to dig some more to confirm. 


---
**René Jurack** *December 11, 2016 23:00*

Thank you, **+Dushyant Ahuja** 


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 00:12*

Yepp, busy weekend. I'm starting to pick back up on the support inquiries now. Helping generate any config files will take a little longer **+René Jurack** 



**+Dushyant Ahuja** no. One does not need to compile the firmware specially. One does need to disable the LEDs and add another module to the config for extruder3. But there's a bunch of other things similar where your statement would be true :)


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/YCJ1a4oNeFa) &mdash; content and formatting may not be reliable*
