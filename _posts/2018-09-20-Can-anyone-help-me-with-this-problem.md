---
layout: post
title: "Can anyone help me with this problem?"
date: September 20, 2018 09:54
category: "K40 and other Lasers"
author: "Cammo T"
---
Can anyone help me with this problem?


{% include youtubePlayer.html id="7pEWydbKD1M" %}
[https://www.youtube.com/watch?v=7pEWydbKD1M](https://www.youtube.com/watch?v=7pEWydbKD1M)





**"Cammo T"**

---
---
**Tech Bravo (Tech BravoTN)** *September 20, 2018 09:58*

Snap pics of the board showing all connections and also yhe power supply and as much of the endstops as you can please.


---
**Cammo T** *September 20, 2018 10:03*

I'm trying to use mechanical end stops as the optical one does not seem to work.  Mechanical end stops are connected to the Cohesion3D smoothie board and the software I am running is Lightburn. 

![images/eb5427bafccc9c133f56aa43ce73978d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/eb5427bafccc9c133f56aa43ce73978d.jpeg)


---
**Cammo T** *September 20, 2018 10:05*

![images/3d1fb84e25b840bb72193adac23e720f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d1fb84e25b840bb72193adac23e720f.jpeg)


---
**Cammo T** *September 20, 2018 10:06*

![images/b92c55da0a8c54af3a1ec5ec17938ac6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b92c55da0a8c54af3a1ec5ec17938ac6.jpeg)


---
**Tech Bravo (Tech BravoTN)** *September 20, 2018 10:07*

can you do an actual screenshot (printscreen) of your config file please. its not readable :)




---
**Joe Alexander** *September 20, 2018 10:32*

try saving a copy of your config file for backup first, then configuring your endstops as such(note I have installed min and max endstops):comments deleted to keep it clean



endstops_enable                                true            

corexy_homing                                   false            

alpha_min_endstop                            1.24           

alpha_max_endstop                            1.25            

alpha_homing_direction                   home_to_min      

alpha_min                                              0                

alpha_max                                           320              

beta_min_endstop                             1.26            

beta_max_endstop                            1.27           

beta_homing_direction                        

beta_min                                               0                

beta_max                                            230            

gamma_min_endstop                         NC            

#gamma_max_endstop                      NC            

gamma_homing_direction               home_to_min     

gamma_min                                          0               

gamma_max                                       200             



alpha_max_travel                                500              

beta_max_travel                                  500              

gamma_max_travel                             500             



# Endstops home at their fast feedrate first, then once the endstop is found they home again at their slow feedrate for accuracy

alpha_fast_homing_rate_mm_s              50            

alpha_slow_homing_rate_mm_s            25        

beta_fast_homing_rate_mm_s                50        

beta_slow_homing_rate_mm_s              25        

gamma_fast_homing_rate_mm_s          4         

gamma_slow_homing_rate_mm_s         2          



alpha_homing_retract_mm                     10              

beta_homing_retract_mm                       10               

gamma_homing_retract_mm                   1              





# Optional enable limit switches, actions will stop if any enabled limit switch is triggered (all are set for delta)

alpha_limit_enable                           true           

beta_limit_enable                            true            

#gamma_limit_enable                    false         



# Optional order in which axis will home, default is they all home at the same time,

# If this is set it will force each axis to home one at a time in the specified order

homing_order                                 XYZ              

move_to_origin_after_home         false            

endstop_debounce_count               100             

endstop_debounce_ms                     1               

#home_z_first                                    true            



Note how the hashtags are removed on relevant setting.

AGAIN backup your config first! Hope this helps :P


---
**Kelly Burns** *September 20, 2018 11:51*

What end stops are you using? Just switches or the pre made rip rap ones?


---
**Ray Kholodovsky (Cohesion3D)** *September 20, 2018 14:47*

Power down. Unplug the ribbon cable. Use M119 command in LB console to test if the switches are being read. 


---
**Joe Alexander** *September 20, 2018 19:08*

im using just plain microswitches, not even on a circuit board. heres a link for an example:

[https://www.amazon.com/Cylewet-V-156-1C25-Momentary-Arduino-CYT1046/dp/B06WRN7FQB/ref=sr_1_10?ie=UTF8&qid=1537470464&sr=8-10&keywords=microswitch](https://www.amazon.com/Cylewet-V-156-1C25-Momentary-Arduino-CYT1046/dp/B06WRN7FQB/ref=sr_1_10?ie=UTF8&qid=1537470464&sr=8-10&keywords=microswitch) 

[amazon.com - Robot Check](https://smile.amazon.com/Cylewet-V-156-1C25-Momentary-Arduino-CYT1046/dp/B06WRN7FQB/ref=sr_1_10?ie=UTF8&qid=1537470464&sr=8-10&keywords=microswitch)


---
**Cammo T** *September 22, 2018 05:03*

**+Tech Bravo** 

![images/cb0e94d89252f8e4f1e55aa1dc2e1a68.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cb0e94d89252f8e4f1e55aa1dc2e1a68.jpeg)


---
**Cammo T** *September 22, 2018 05:14*

**+Joe Alexander** Hi Joe.  Thanks for the reply.  I tried the config you posted but still no luck.  Does the exact same thing.




---
**Cammo T** *September 22, 2018 05:20*

**+Kelly Burns** Hi Kelly.  I am using these ones.

[k40laser.se - End stop upgrade - mechanical - K40 Laser](https://k40laser.se/k40-parts/gantry-diy-kits/end-stop-upgrade-mechanical/)


---
**Cammo T** *September 22, 2018 05:21*

**+Ray Kholodovsky** Hi Ray.  Here are the results.  I also lost X - axis movement when doing this as well.

![images/bd5c95cd4dcac780e98a9119c0b8fa97.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bd5c95cd4dcac780e98a9119c0b8fa97.png)


---
**Ray Kholodovsky (Cohesion3D)** *September 24, 2018 14:35*

What do you mean lost x axis movement? 


---
**Cammo T** *September 25, 2018 09:43*

**+Ray Kholodovsky** Hi Ray.  When in Lightburn software and I press left or right arrow to move the head it does not respond.  It works and moves when I press up and down though.


---
*Imported from [Google+](https://plus.google.com/100980986272635435257/posts/6wi2tGmCHpV) &mdash; content and formatting may not be reliable*
