---
layout: post
title: "Hello, Subject: Homing and Y axis issues on K40 and Y axis issues on Gen 5"
date: December 26, 2018 23:37
category: "K40 and other Lasers"
author: "Em W"
---
Hello, Subject: Homing and Y axis issues on K40 and Y axis issues on Gen 5.



First an overview: I want/wanted to put the C3D mini in a gen 5. In my education path these past months I purchased a K40 to trace the wiring, learn, and have something to use in the meantime. 



The K40 is the one with the M2 Nano with the mA and Potentiometer on the simple panel model.



My gen 5 does not have a mA or Potentiometer. From the K40 I now understand the wiring and how they work.



Anyway, the other day I hooked up the gen 5 to the C3d to test the stepper motors. I had an issue and thought is was a Lightburn software issue so forumed Lightburn. Here is the correspondence:



<b>***********</b>

Hi, hope you can provide some input.



My only goal - to see if Lightburn would talk to the C3D Mini board with connected X and Y axis going to a FS Gen 5 CO2 laser with 0.9 degree 42mm series , Nema 17 type LDO-42st33-1334MB stepper motors.



FYI, the setup to help with the Q.



Before any test I can move both my X and Y axis by hand and they freely move to wherever I want them (importany notation for later)



X and Y axis plugged onto the C3d Mini (board) = check

24V power connected to board = check

Config file in board = check

Turn on board and check lights = check, all working correctly per C3d site

USB cable purchased from C3d going from board to computer = check



Install Smoothie drivers, computer sees serial port = check

Open Lightroom = check

Create laser profile for board = check – basically used the K40 or default when opening the program and selecting the Smoothie option



Find the arrows to jog the stepper motors = check



Move the X axis arrow = check, yes Lightburn is talking to the board and the stepper. I do think it may be moving in the wrong direction but I think I can fix that later. At least is seems to be moving as it should otherwise



Move the Y axis arrow = issue; Lightburn is talking to the Y axis in that it tries to move, but it won’t go anywhere. It is locked in the position it is at, and will not move even though pressing the Y axis arrow in the software it is trying to move it.



The origin was set to upper right. I changed it to lower left. Same result and software is Not set to auto home.



Granted I don’t have any limit switches or anything hooked up, other than what is outlined above on the board, but if I am only moving the steppers in small increments, and that wouldn’t seem like it should factor/be issue in this test?



While in Lightburn, when I try to move the X and/or Y axis by hand they will not budge = locked in place. (in my Gen 5 software there was a setting to lock the axis/s or unlock the axis/s (steppers). I looked in Lightburn for something to unlock the steppers/axis/s but didn’t see anything?



So I exited the program, and as soon as I did, I could move the axis by hand freely. So I know it is either a Lightburn thing or a config card thing when in the software (or both if they are in communication on this) – but I have no idea which?



The board was still powered on and I could move the axis/s so I know it is not a board powered on issue.



QUESTION: So do you have any insight about:



A) Why the axis/s lock when I go into Lightburn and how do I unlock the axis/s – steppers.

B) Why is the Y axis acting like it is locked in place when I try to move it in Lightburn when the X axis does move OK.



Is this a software setting, or do I have to go into the config file. And if the config file, any suggestion which part of the file need to address. Any help on this would be greatly appreciated. Thanks!



<b>***</b> Here was the reply (thank you for the reply)

Lightburn Owner

These are all very much hardware configuration questions. When the software connects to the board the board will lock the motors, so that’s expected. The configuration and wiring questions would be more appropriate for the Cohesion3d forum.



<b>********</b>



So I abandoned the task and put my gen 5 controller back to be sure I didn't mess up the Y axis and was not ready to tackle a forum post here. 

With gen 5 board back in all is well with the steppers.



FAST forward to yesterday:



Having a K40 now, I decided to just put the C3d mini in there so it doesn't go to waste. Yes, it was very simple, about 20 minutes as Ray  notes in his instructions.



Everything was fine! EXCEPT same issue as when I put it in the gen 5 - well several:

In K40:

A) It wanted to "home" in the lower left? I have origin set in Lightburn for lower left but could not find where to set "home". So I don't know if that is because of the software, the config card or the stepper wiring. I just moved the stepper wires from M2 to C3d per instruction.



B) While the X axis moves fine and in the correct direction, ie: left arrow moves it left, etc.



The Y axis moved in the wrong direction. The upper arrow moves it downward.



C) It will lock and make horrible sounds just like it did on the gen 5.



SO, I put back the M2 to be sure I didn't mess up the Y axis on the K40. 



So now the C3d mini is NOT in either laser, so no image to provide right now. When I get inspired to go through this again, I will just do a video:-)



QUESTION and the POINT: But in the meantime: PLEASE can someone tell me "GENERICALLY SPEAKING" what would cause the homing in the lower left, the Y axis locking in place and not moving, and/or the Y axis moving in the wrong direction on K40.



Today I have been using the K40 with LaserDRW to do projects and everything is working fine. But as you all know, the software is really lacking.



Sorry for my long post and thank you ahead of time:-)



PS: For suggestion box unless it is there already and I have not found it - allow user to unlock the steppers in software like I can in gen 5, LaserDRW and/or K40 whisperer:-)









**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 26, 2018 23:46*

The issue you had on the Full Spectrum, from your quoted correspondence on the LightBurn forum, is that you tried to run it without limit switches, and possible other configurations need to be set.  If I recall we did not get as far as doing Limit Switches. 



If you didn't modify the config file, meaning you still had the stock config file we provide for the K40 laser cutter, then you probably had the Y stepper motor connected backwards.  The homing switches are in the back left, and this is where the head should move to during homing. 



"So I abandoned the task and put my gen 5 controller back to be sure I didn't mess up the Y axis and was not ready to tackle a forum post here. " --> Have we offended or put you off somehow?  Information overload?   Genuinely asking. 


---
**Em W** *December 27, 2018 02:23*

**+Ray Kholodovsky** Hi Ray, thank you for the reply. To answer your question:



I know I tend to over ask and over analyze. It was when I actually had the C3d in hand and started to convert. The first Q I had was met with a reply that you didn't have time to hold hands, paraphrasing. I know you are busy and I had/have a lot to learn and hence, hesitate to ask/bother. 



That is one reason why after that I have spent a lot more time watching vids, reading forums and purchased a K40 to map the wiring, so I only have to ask if feeling desperate. 



I do feel much more knowledgeable now about the wiring and mechanics of it all. The config file not so much as I have not studied that yet.



Anyway, my goal/task now that I have the K40 is to put the C3d mini in that and table the gen 5 for now. The k40 is working as it should with the Laserdrw software, and I have already put the C3d board in it once, with the same Y issue and homing as talked about above.



I will put the C3d mini back in the K40 in a little while, and video what I do so that way you can actually see what is going on and not have to read my novels trying to explain:-) 



I do think I was overwhelmed at first, but now better understand it all. Once I get the homing and Y axis squared away on the K40 with the C3d, I feel it will be successful. I still want to eventually do the gen 5, but thinking about a different board for that with all this new found knowledge. Want to get the K40 done first though. So, lucky you, I will be back:-)


---
**Em W** *December 28, 2018 01:42*

**+Ray Kholodovsky** HOORAY! I put the C3d board back in the K40 today and swapped the Y axis wires around and now everything is working. Thanks for the help...


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2019 21:30*

Glad to hear it is working.  



I do want to see the conversion also happen for your FSL, I think we've cracked a good chunk of it. 

Our new laserboard has built in motor drivers and screw terminal outputs so the motor connectors can be put directly with no crimping. 



The endstops would still require some figuring out, and crimping or wiring. 



A DSP is not going to be easier to install. 


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/DS2Kf3nF65c) &mdash; content and formatting may not be reliable*
