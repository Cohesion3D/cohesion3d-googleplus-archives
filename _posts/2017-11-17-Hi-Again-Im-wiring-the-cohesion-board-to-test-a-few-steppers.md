---
layout: post
title: "Hi Again, I'm wiring the cohesion board to test a few steppers"
date: November 17, 2017 05:51
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Hi Again,



I'm wiring the cohesion board to test a few steppers.  I'm using drv8825 drivers.  My test motors are 3A but this will change to 2.0A later on.



If I wire +24 into the aux power header and use USB for the +5v for normal logic, do I need to add the voltage regular?  If so, are there any docs on this?  









**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 17, 2017 05:53*

This isn't the smoothieboard, there is already a regulator, no need to add one, and the board logic will still work without USB connected. 


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/A1NSzb2kHsD) &mdash; content and formatting may not be reliable*
