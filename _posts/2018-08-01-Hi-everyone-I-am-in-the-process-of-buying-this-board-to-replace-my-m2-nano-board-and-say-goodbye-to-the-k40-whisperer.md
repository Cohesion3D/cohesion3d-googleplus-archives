---
layout: post
title: "Hi everyone, I am in the process of buying this board to replace my m2 nano board and say goodbye to the k40 whisperer"
date: August 01, 2018 04:27
category: "FirmWare and Config."
author: "Ismail Yil"
---
Hi everyone,



I am in the process of buying this board to replace my m2 nano board and say goodbye to the k40 whisperer. However, I have a question.



I just bought a rotary device which plugs into y-axis (no additional driver or power needed). It gives me inconsistent results as seen in the picture.  Can C3D fix this problem? And do I need additional driver or power to get this rotary working?





![images/6ca11d0269fbd94919c271c771268f2a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6ca11d0269fbd94919c271c771268f2a.jpeg)



**"Ismail Yil"**

---
---
**Tech Bravo (Tech BravoTN)** *August 01, 2018 05:12*

The C3D mini will support a rotary (and a z axis) without having to disconnect y. An external power supply, external stepper motor driver, and an external stepper motor driver header adapter will be required and approved ones are available at [cohesion3d.com](http://cohesion3d.com)


---
**Tech Bravo (Tech BravoTN)** *August 01, 2018 05:12*

Here is documentation on it: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**Tech Bravo (Tech BravoTN)** *August 01, 2018 05:14*

Also special firmware and config files are required: [lasergods.com - C3D 4 Axis Firmware & Config](https://www.lasergods.com/configuring-4-axis-firmware-config-file-on-the-cohesion3d-mini/)


---
**Ismail Yil** *August 01, 2018 05:27*

**+Tech Bravo** can only the driver adapter work? I just plug in the rotary to the A. The rotary doesn’t need extra power supply or driver with the stock k40 laser. It doesn’t make sense if it needs extra boost with C3D. What do you think?


---
**Tech Bravo (Tech BravoTN)** *August 01, 2018 05:32*

Cohesion3d always advises to use an external power supply and stepper for z axis and rotary. The stock board, power supply, and motors in a k40 are weak. The motion control for z and rotary, considering they have stout motors, will tax the stock electronics. You "can" do it but there have been many critical failures from not following cohesions recommendations.


---
**LightBurn Software** *August 01, 2018 18:40*

If you don’t push it too hard it can work, but engineers for the original K40 have admitted that the stock power supply was not rated to power the steppers, but they did it to cut costs. Adding another stepper driver to that increases the power load and the likelihood of a dropout. So it’s not “required”, but you may cause yourself additional frustration.



Using the rotary on the A axis means a 3rd driver, so there is more power load, but it also means not constantly having to swap the cables, and better control over the precision of that axis. In the image posted above, the user likely scaled the image in Y to account for the circumference of the material, but you’d also need to adjust the line interval value (DPI) as well, and that math is harder. Using the A axis, the software does all that for you.


---
*Imported from [Google+](https://plus.google.com/110005552157264622079/posts/UiFP2m75fvw) &mdash; content and formatting may not be reliable*
