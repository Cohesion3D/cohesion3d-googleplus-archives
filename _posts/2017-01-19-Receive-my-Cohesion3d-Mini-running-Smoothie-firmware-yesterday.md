---
layout: post
title: "Receive my Cohesion3d Mini running Smoothie firmware yesterday"
date: January 19, 2017 15:19
category: "General Discussion"
author: "Jim Fong"
---

{% include youtubePlayer.html id="TTSkw-9NvW4" %}
[https://youtu.be/TTSkw-9NvW4](https://youtu.be/TTSkw-9NvW4)



Receive my Cohesion3d Mini running Smoothie firmware yesterday.  Bench testing with LCD panel and Ethernet web access.  No problems getting the board running at all. 



Applied Motion STM-17S-3AN high performance stepper motor with built in driver was used for speed testing.  10microstep (2000 steps per rev) 20volt supply. Using Cohesion3d external stepper adapter board. 



Step pulse output was 99,663khz.  This was the fastest I could get the firmware to reliability output a pulse train.  Nice square wave output captured by Tek Scope and frequently counted by Fluke Multi-Counter 



Rpm meter says the stepper motor is spinning at 2990rpm



Verification 

99,663khz / 2000 x 60 = 2989.89rpm





I will be using the Cohesion3d Mini for a custom laser build. I needed to verify how fast the smoothie firmware can output step pulse and the ability to connect to external drivers.  I will be using Copley brushless servo drivers and Parker brushless motors with very high ppr encoders.  A high step pulse will be required.  



















**"Jim Fong"**

---


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/UE9f4gy68bq) &mdash; content and formatting may not be reliable*
