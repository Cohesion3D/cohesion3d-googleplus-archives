---
layout: post
title: "Help, I think I just hurt my C3D board ;( I just had a situation where the high voltage power lead slipped off of my laser tube"
date: September 03, 2018 23:24
category: "C3D Mini Support"
author: "Trevor Hinze"
---
Help, I think I just hurt my C3D board ;(



I just had a situation where the high voltage power lead slipped off of my laser tube.  I noticed it right away and shut off the laser, waited a minute and opened the cover.  I grabbed the loose lead like an idiot and got a really nice shock.  Unfortunately when this happened my arm was across my gantry and I seem to have fried the Inductive limit switch for my X axis.  No big deal as far as the end stop is concerned but I think I may have damaged the input pin on the C3d board.  Now when I home the laser it already thinks the x axis is homed even when the switch is completely disconnected.  As a result the laser homes the Y as it should but just moves the x about 5mm in the positive direction just like it would if it was backing it off of a limit switch.  What I am wondering is if there is a way to somehow redefine which pin is used for the x axis home so that I could use one of the many other unused pins on the board.  I cant believe that I only damaged one little section of the board, a testament to how well made it is.  Thanks in advance for any guidance you can provide!!!!





**"Trevor Hinze"**

---
---
**Trevor Hinze** *September 03, 2018 23:28*

I failed to add in my original post that in the probing menu x min shows =1 all the time  but the more I think about it maybe that could be caused by a bad switch?  Is it safe to jumper the pin, or apply +5vdc to it to see if the state changes?


---
**Trevor Hinze** *September 03, 2018 23:35*

Now I am just think out loud, sorry.  I switched the two limit switch pins around and sure enough the X min pin will now change from 0 to 1 meaning that I only hurt the end stop, not the board... Amazing stroke of luck!!!!


---
**Ciaran Whelan** *September 03, 2018 23:58*

That is shocking news.. 


---
**Trevor Hinze** *September 04, 2018 00:52*

**+Ciaran Whelan** indeed it was , elbows are not intended for that kind of voltage :)




---
**Don Kleinschnitz Jr.** *September 04, 2018 10:37*

**+Trevor Hinze** you are very fortunate, had that supply been on this would have been an awful story. These supplies are lethal!


---
**Griffin Paquette** *September 04, 2018 11:56*

Ray was very smart and broke out the other two endstop pins for you. You can use those and just reconfigure one of them to be your new X endstop in the config file. The only issue is unlike the 4 main endstops, those two do not have diode protection so if you use a voltage higher than 3.3V for your sensor, you need to use a voltage divider to not hurt the pin. 


---
**Trevor Hinze** *September 04, 2018 13:21*

**+Don Kleinschnitz** which is why I shut it off and waited a minute.  I have plenty of respect for the power supply.  What I did not take in to account is that since it was just hanging in the air it needed to have the residual voltage discharged.  That p.s. does scare the  crud out of me though.


---
**Trevor Hinze** *September 04, 2018 13:22*

**+Griffin Paquette** that is great forethought.  Fortunately the board seems to be ok, and amazon will hopefully solve !y endstop problem on Thursday :)


---
*Imported from [Google+](https://plus.google.com/106981345031249926577/posts/ca7KG72T5Bk) &mdash; content and formatting may not be reliable*
