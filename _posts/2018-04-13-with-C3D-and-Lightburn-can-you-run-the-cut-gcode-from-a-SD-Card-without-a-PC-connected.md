---
layout: post
title: "with C3D and Lightburn can you run the cut gcode from a SD Card without a PC connected?"
date: April 13, 2018 01:57
category: "K40 and other Lasers"
author: "Bill Keeter"
---
with C3D and Lightburn can you run the cut gcode from a SD Card without a PC connected?



I selected the file from the GLCD after homing the laser. It runs the file but the laser is never activated while it's moving around running the scripts to engrave and cut. Do I manually have to add the start up gcode that we used to use with LaserWeb?





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 02:06*

Do you have one of my first batch board where we did that white pwm cable to replace the pot? 


---
**Bill Keeter** *April 13, 2018 02:11*

This is a C3D I picked up from your site in December I believe. As this is for my new 60w laser I just finished building :D



I do have one of your originals though.. but that's for my K40. Both C3D boards have the wire run to P2.5 



I'll post a video in a second of it running. It looks fine. Just no laser firing (but the physical test fire button works fine)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 02:17*

If there is a potentiometer hooked up and L is what is being pulsed... then the question is very simple: does it work properly when you stream from Lightburn over USB cable? There should be no difference between USB and SD card. 



There should be no start up gCode that is necessary. 


---
**Bill Keeter** *April 13, 2018 02:44*

**+Ray Kholodovsky** Nope. It's not firing over USB either. Hmm...  One thought. On a 60w Laser PSU do I connect C3D P2.5 to the TH or TL line on the laser psu?


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 02:45*

We're looking for LOW = LASER FIRES, interpret accordingly. 



Send a G1 X10 S1 F600 command to see if the laser fires, that's all we need to make work. 


---
**Bill Keeter** *April 13, 2018 04:01*

**+Ray Kholodovsky**  As I understand it TH is Laser High and TL is Laser Low.



I tried running a LaserWeb file on the machine. Still not firing. Copied over my K40 Config and updated the settings. Not firing. hmmm..



So I have P2.5 to Laser Low. So to verify the Laser low works I bridged it to ground. Yep it manually fires.



Oh WAIT. Could this be an issue due to me powering the C3D by terminals to a 24v PSU? Could it be cus i'm using the 24neg line instead of the shared earth ground running between all my PSU's? 


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2018 04:28*

You need the laser psu and the 24v separate psu to share a common ground for this to work. 


---
**Bill Keeter** *April 13, 2018 06:11*

**+Ray Kholodovsky** yeah, all the PSU's have the AC lines daisy chained, Plus the earth ground. 



Now if I put the 24+ and 24- into the C3D terminals the board turns on. If I change the 24- to the shared ground line it doesn't. Should I leave the 24+ and 24- alone and add a earth ground line somewhere else on the board to make it work as a ground for the P2.5?


---
**Bill Keeter** *April 13, 2018 06:31*

I wonder if **+Don Kleinschnitz**'s diagram would work in this case. Run the 24- from the 24V PSU to the C3D. then connect that same line to the ground on the Laser PSU. (sorry if that's what you were trying to explain above)



[3.bp.blogspot.com](https://3.bp.blogspot.com/-jIMxuIiL77c/WIt3dSRGvTI/AAAAAAAAjSA/UpFGs6zTdZg8GwHAEIS3h6RBJp-6IWemACPcB/s1600/20170120_064850.jpg)


---
**Bill Keeter** *April 13, 2018 15:19*

UPDATE: It works! Once I added a 2nd ground line to match Don's diagram I got it to fire.


---
**Don Kleinschnitz Jr.** *April 14, 2018 09:32*

**+Bill Keeter** :)!


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/9k1WjQcBj6r) &mdash; content and formatting may not be reliable*
