---
layout: post
title: "Hello all, I have had a Cohesion3D mini board for about a month now and everything has been working great until last night when I started having a problem where, when running a job, some of the cut lines were way off"
date: September 06, 2018 06:31
category: "C3D Mini Support"
author: "Andrew Hayden"
---
Hello all, I have had a Cohesion3D mini board for about a month now and everything has been working great until last night when I started having a problem where, when running a job, some of the cut lines were way off alignment. I tried reinstalling Lightburn and the first job ran ok but the second one did it again... does anyone have any idea what’s going on here? Although it looks like I have moved the material during the job, I can assure you this is not the case. Here are some photos comparing a properly aligned job with one where it’s gone out of alignment.

The lighter vector engraving lines are cut at 45mm/sec and the darker cuts are at 12mm/sec. No settings were changed between these jobs.



![images/d8557a1c4188a4fc69b88d616a644d29.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d8557a1c4188a4fc69b88d616a644d29.jpeg)
![images/6d4aec0dd4c3698bd758f62d536d403f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d4aec0dd4c3698bd758f62d536d403f.jpeg)

**"Andrew Hayden"**

---
---
**Sebastian Szafran** *September 06, 2018 07:29*

This might be a pulley-to-motor mechanical connection getting a kind of loose. I got similar results in the past milling PCB on a CNC machine.    You might not feel that there is a loose at all, but with higher acceleration speeds it will affect your job. As a test try to lower the speed and power and see if there is any difference in job alignment. 


---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2018 12:50*

There are a number of things that can contribute to this and we will need more details in order to assist. 



Show us your setup - picture of the board, wiring, etc... tell us which hardware you’ve installed exactly. 



24v from stock power supply causes issues and can brown out the board. 



Expanding, perhaps you need to increase the current on your stepper drivers, but can’t do that with the stock psu. 



As a side note we recommend isolating the board from the machine frame... use nylon standoffs or screw the board to an acrylic plate so the board screws aren’t touching the metal frame of the machine. 


---
**Don Kleinschnitz Jr.** *September 06, 2018 13:49*

Check the coupler on your Y stepper so insure it is not slipping.


---
**LightBurn Software** *September 06, 2018 16:38*

With the number of lines shown, I’d guess it’s an acceleration issue when seeking from one cut to the next. Many small cuts like that can cause slip if the acceleration is too high. What firmware do you use with the board?


---
**Jonas Rullo** *September 06, 2018 20:46*

Looks like a loose belt, or some kind of drive connection problem. When things start going wrong in certain directions, one or more things is tightening or loosening depending on the force in that direction at that moment. Check belts and grub screws on steppers, etc. 


---
**Andrew Hayden** *September 07, 2018 03:20*

Thank you for all the comments.



 I have slowed down from 45mm/second engraving to 40mm/second and 12mm/second cutting to 10mm/second that that seems to have done the trick.



Does the acceleration speed chamge with the cutting speed or does that slways stay the same?



The belt is not loose or the pulleys but I'll have a look at the stepper motors and "grub screws" (once I know what they are )



I am using the latest Lightburn Software which I bought with the cohesion3d mini starter pack




---
**LightBurn Software** *September 07, 2018 03:25*

Acceleration is independent of speed, and lowering would likely allow you run significantly higher speeds without losing steps.


---
**Andrew Hayden** *September 07, 2018 04:48*

**+LightBurn Software** Would that not be counterproductive though in that the overall job would take longer if the acceleration speed is lower? 

I guess it would depend on how much faster you end up being able to cut. For me, 12-14mm/second is the fastest I can cut the 3mm mdf that I use with my k40 without overdriving the tube.

Just wondering if lowering the acceleration would help in my case as right now I seem to be able to do 10mm/second without any missing steps.

The other thing I am not clear on is why I am still able to have a job where one layer cuts at 40mm/second and the other at 10mm/second running with no issues, but by changing the 10mm/sec up to 12 causes me to lose steps even though the 40mm/sec layer stays the same.

Could it be related to the power of the cut, as switching the 10mm/sec cut up to 12 required me to increase the % of power?


---
**LightBurn Software** *September 07, 2018 04:52*

Would you rather have a car that maxes out at 30mph, and accelerates from 0 to 30 in 2 seconds, or one that goes 200mph, with 0 to 100 in 10 seconds? The answer depends a lot on how often you stop & start, compared to how often you hit straightaways.



As for your power, yes, you could actually be pulling enough to make the steppers skip. The stock K40 power supply was never intended to drive even the original board + steppers, but they did it anyway to cut costs.


---
**Don Kleinschnitz Jr.** *September 07, 2018 10:32*

**+Andrew Hayden** put a DVM on the steppers power supply and see if you can detect a droop.


---
**Andrew Hayden** *September 08, 2018 03:19*

**+LightBurn Software** I like your car analogy.

Interesting what you say about the stock psu.


---
**Andrew Hayden** *September 08, 2018 03:20*

**+Don Kleinschnitz** not a bad I idea! Might have to try that. Cheers


---
*Imported from [Google+](https://plus.google.com/116886129894437647553/posts/XatidadYVQa) &mdash; content and formatting may not be reliable*
