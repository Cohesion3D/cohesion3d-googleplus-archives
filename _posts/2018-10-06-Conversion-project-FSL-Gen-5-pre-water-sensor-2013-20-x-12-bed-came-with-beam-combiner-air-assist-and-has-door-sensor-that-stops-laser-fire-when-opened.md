---
layout: post
title: "Conversion project: FSL Gen 5, pre-water sensor (2013), 20 x 12 bed, came with beam combiner, air assist and has door sensor that stops laser fire when opened"
date: October 06, 2018 18:42
category: "K40 and other Lasers"
author: "Em W"
---
Conversion project: FSL Gen 5, pre-water sensor (2013), 20” x 12” bed, came with beam combiner, air assist and has door sensor that stops laser fire when opened. Home is in the HV upper right side near the tube, and in image 3 you can see the homing sensor part just under the tube holder bracket on the rail.



References for current FSL configuration: 

Please refer to the pictures I posted at [https://www.emsplace.com/fsl-gen5/index.html](https://www.emsplace.com/fsl-gen5/index.html) for what the inside of my current laser looks like. If you click on one of the pictures it will open up larger ones and have the arrows to move through them all. There is a total of 26. Best viewing on a tablet or desktop.



I have compiled some FSL config info that may help/be needed to config the C3D Mini and/or Lightburn software? [https://www.emsplace.com/fsl-gen5/config.html](https://www.emsplace.com/fsl-gen5/config.html) ALSO: this schematic posted by Tech Bravo on google forum is more or less the same as mine [https://www.lasergods.com/fsl-gen-5-hobby-laser-unofficial-schematic/](https://www.lasergods.com/fsl-gen-5-hobby-laser-unofficial-schematic/) 



FLS current X, Y stepper motors: [https://www.peterparts.com/CatalogPages/62/3244.pdf](https://www.peterparts.com/CatalogPages/62/3244.pdf) (0.9 degree 42mm series , Nema 17 type) LDO-42st33-1334MB. Power plug uses a F10AL 250V fuse.



If anyone reading this thinks I am in way over my head to try this conversion  (or it won’t work so don’t try) please let me know. As long as I can ask what will seem like rudimentary questions, I am willing to do homework and try to figure this out. And yes, I may be over complicating this in my head right now.



Planning my order and have Q’s:



C3D site says: bundle includes the Mini Board itself, 2 A4988 drivers (will use for X, Y and also want to add a rotary at some point), and a MicroSD Card with the laser configuration preloaded for k40.



-	Q’s: re preloaded config: Since I don’t have k40, can I go in a make changes on sd card. I need to figure out what those are and the config file posted above and the stepper pdf together with the schematic shared by Tech Bravo is what I have to work with, although, I don’t know what everything means at this point. I do know how to use mini/micro cards and edit software files so that should be simple. It is the actual config entries that need to be determined/modified since I don’t have the K40. 



~ Other things I will also order: the high power USB cable and power supply upgrade. 

<s>Lightburn license Q: sounds like the C3D Mini uses Gcode so that version is all I need for now? Or if I get the GLCD too would I need the Gcode and DSP license?</s>



<s>~ Bundle purchase option/selection:</s>



<s>-Include 2 Additional A4988 Drivers Q: (these go on the controller board like the ones for X, Y correct, and what I would need to add rotary?) and/or is this selection just to have extras on hand?</s>



<s>-Include external stepper adapter – where does this go. On the controller board? Do I need it now if want to add rotary, or is the A4988 good for now?</s>



<s>-Include external stepper driver – Does the driver come loaded on the sd card when ordered with the bundle? If not, how/where would that driver go and where to get it. I assume on the sd card or config file somewhere? I would get this together with the external stepper adapter, correct, if I want to go the external route?. Thinking for simplicity I should go with the additional A4988 Driver/s for the rotary and do this later? pros/cons...</s>



	(side note: on FSL I would need to use the Y axis, and the rotary came with the P block already attached to swap out when wanting to use the rotary, which I have not used yet.) Or, perhaps like the FSL I can swap out the Y axis on the C3d in the event I ever use the rotary?  – prefer to have it’s own driver but asking if this would work?



~ Bundle purchase option/selection:

GLCD display, GLCD adapter - I am not understanding if smoothieware is used  with just the C3d or with both the controller and the GLCD display option. Reading references about GRBL-LPC firmware and have Q on that below.



<s>Sounds like the bundle comes with smoothieware  for just using C3d? But if using the GLCD then use the grbl-lpc firmware?  I see from my notes below "grbl does not support the GLCD display panel?" so is that correct.</s>

<s>-C3d says: Smoothieware drivers (but not windows 10?) – what does that mean. Since Lightburn says can be loaded on up to two computer. I would like to do file/design work on a Win 10 computer, and then transfer those files/import/open into Lightburn on the computer attached to the laser, most likely Win 7 or 8. </s>

<s>-Is smoothieware controlled/placed/read from the config sd card file or Lightburn software program. Confused about how this all plays together as you can tell.</s>



<s>~ Mechanical limit switch and mechanical end stops on C3d site: should I go ahead and just order those to have on hand in case my current ones are not suitable? The price is right just to have on hand, but thoughts?</s>



<s>~ GRBL-LPC firmware vs smoothieware</s>

<s>Can someone please explain in more simple terms how to understand the difference and what to consider if I need to decide between this and smoothieware. Is one for if I use the Lightburn software, and one for if I use the display GLCD. As long as smoothieware will do quality raster engrave, even if slower, i can live with that for now - more below.</s>



<s>Note: looking at the google forum: if I am I understanding correct, smoothieware comes with the C3d Mini and will do raster engrave (which I need) but at lower speed/setting thing ( what about quality of engrave)? Where using grbl-lpc firmware (and grbl does not support the GLCD display panel?) is faster and more reliable than smoothieware for raster engrave? </s>

<s>-Basically: I want to keep it simple for this initial conversion, but do want to be able to have good quality engrave (I do jewelry and smaller items up to 4-5 iinches – so not talking about large engraving  - speed is not as important at this point but quality is).</s>



<s>I will most likely use the Lightburn software most of the time, but am considering the GLCD display, but not if it will initially complicate the set-up, since I am sure there will be enough to deal with. Assume I can add a GLCD later if wanted and/or switch from smoothieware to GRBL-LPC firmware later?</s>



<s>~ PWM or how to adjust power and speed? </s>



<s>Ray mentioned something about that in my first post, but I didn’t quite get what he was saying. Therefore, specifically, do I purchase a physical part for this, and if so, where and what part should I order. Or is it set in Lightburn software - with wiring from the controller to the power supply that controls power/speed using Lightburn? </s>

<s>-In a google post Ray says: “I always recommend installing a 10k pot and mA meter so that you can get maximum grayscale contrast.” So I also want to add/buy/install a mA meter too but don’t know where to purchase or how/where to install these items?</s>



<s>~ JST connectors:</s>

<s>The FSL has Phoenix blocks and from reading C3D it notes using JST. I looked at some Youtube and have a few questions:</s>

	Seems like I will need to take the wire/s from the P block/s and rewire the ends for insertion into the JST connectors. And if so, it looks like I should order the connector pack from C3d? What else do I need? 

-	A crimping tool for the JST? Where to purchase?

-	What goes between the stripped wire end and the JST connectors – part/component wise. Where to purchase and size needed?

Comments on this subject? 



Sorry to be a bother with all of these questions. Just want to be sure when I order I get all the stuff I need so that when I start the project all the tools/parts are here. I have an Ohm meter (but admit I will need to learn how to use it better for this project). Anything else I need or am forgetting? I did already pick-up some nylon spacers and nylon screws for the C3d controller board mounting.



I may also need more wire, since some of the current wires may need to be extended. I am looking at tinned copper wire in the respective size per the schematic posted by Tech Bravo. Would that be correct? I assume I can splice/extend the wires for this project if needed?



Truly, thank you in advance for any input on this!





**"Em W"**

---
---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 06:11*

It looks like you do your research, ask relevant questions, and have an overall sense of where you are going with this so no, i don't think you are in over your head. We shall see :) :)



You can make changes to the Smoothie configuration file to customize it to your machine. It is a simple text file. GRBL has no config file. You alter GRBL by sending commands through the LightBurn console. You should start with smoothie and get everything going before even thinking about grbl (for now).



The power supply is a great idea. Install the basic kit first (especially since this is a non-typical install) and install the power supply after the machine has been tested :)



LightBurn Gcode version is all you need.



2 A4988 drivers come with the bundle. They are for X and Y. You can get spares but you need the external drivers for rotary and/or z axis (along with the power supply upgrade).



The external stepper driver is the hardware component to run stepper motors. It does what the A4988 does but is heavy duty and more flexible.



You can run the rotary on the Y axis but the neat thing about cohesion is that it has a dedicated A axis (and one for a z table too) for rotary which makes that unnecessary. 



The GLCD only functions with Smoothie. You can run GRBL with it installed but it will have no function.



The C3D comes with a micro SD card preloaded with standard smoo firmware. You are welcome to use GRBL, It is available. There is also a 4 axis Smoothie flavor if you plan to use a z axis and/or rotary. These are not big hills to climb and can be loaded at will.



-Windows 10 already has drivers installed for the board. Other Windows operating systems will need the driver installed. No different than loading a printer driver usually. This is totally independent of LightBurn. 

The firmware is read from the card so the card needs to remain in the controller at all times. Smoothie has a firmware file and a config text file that is editable. GRBL has only a firmware file.



The Cohesion3D is capable of using mechanical and/or optical endstops. if yours are working I would use them.



Smoothie does have speed limitations for complex raster engraving but the quality is great. Cutting is not an issue. GRBL is faster for raster but try Smoothie first and see if its even an issue.



Yes the GLCD can be added later and it is wise to do the basic install, check it, and then add peripherals so you are on the right track.



The Cohesion3D Mini (along with the software) handles laser power. A hard limit will need to be set and forgotten and that is outside the scope of the controller. This is where the addition of a potentiometer and an analog milliammeter come into play.



Crimping tool:

[ebay.com - XH JST VH Dupont Amp 2.5 2.54 3.96mm 30~18AWG Pin Crimping Tool SK-225L &#x7c; eBay](https://www.ebay.com/i/123159485460?chn=ps)



20AWG wire is usually sufficient for all connections up to 24VDC and not exceeding 2A. Most of the motion control and switching circuits going to the C3D fall within this category with the exception of the 24v main power. You can splice when necessary. A Western Union (Lineman)splice (soldered and heat shrinked is preferred).


---
**Em W** *October 07, 2018 16:42*

**+Tech Bravo** LOL, we shall see... Yes we will:-)



You took my gibberish and really simplified everything - excellent! I am confident now about what I want to order.



I do have just a few Q's, easy ones:



1) The part that the crimping tool will "crimp". I am assuming i need a part the the wire goes into, gets crimped, then is inserted into the JjST connectors? If so, do you have a link for what part is needed/compatible for the job?



2) Re: Laser power; you mention "This is where the addition of a potentiometer and an analog milliammeter come into play.". I am assuming these are parts, and if so, do you have a link/s to the parts to order. I would like to have them on hand too. I assume they will be needed?



3) Yes, I will get the 20 gage wire thanks to your confirmation. What gage should I get for the 24V just in case I need extra. 18 gage? Any other size/s you recommend to have on hand. I already have solder and heat shrink on hand.



That's it, so easy, right? For now:-) Thanks again!



Oh, just curious. When I look at my post above, I am seeing a lot of strike out lines. Is that something you did to make tracking all my questions easier. I wouldn't blame anyone, if so - I was worried no one would respond due to my ramblings, and possibly being too confusing with my questions.




---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 17:17*

1) The pins as well at the connector blocks are included in the connector pack on C3D's site. That should be all you need: [http://cohesion3d.com/connector-pack-for-cohesion3d-mini/](http://cohesion3d.com/connector-pack-for-cohesion3d-mini/)



2a) Pot: [https://www.ebay.com/itm/3590S-2-103L-10K-Ohm-BOURNS-Rotary-Wirewound-Precision-Potentiometer-Pot-10-Turn-/191673437644](https://www.ebay.com/itm/3590S-2-103L-10K-Ohm-BOURNS-Rotary-Wirewound-Precision-Potentiometer-Pot-10-Turn-/191673437644)



2b) Meter: [https://www.ebay.com/itm/DC-20mA-Analog-Needle-Panel-DC-Current-Ammeter-85C1/301831885863?hash=item4646951427:g:uIYAAOSwHPlWeWF-:sc:USPSFirstClass!37129!US!-1](https://www.ebay.com/itm/DC-20mA-Analog-Needle-Panel-DC-Current-Ammeter-85C1/301831885863?hash=item4646951427:g:uIYAAOSwHPlWeWF-:sc:USPSFirstClass!37129!US!-1)



3) The external power supply is rated 24VDC @ 4 Amps. 18AWG should be fine. Here is a table with wire sizes and their ratings: [powerstream.com - American Wire Gauge Chart and AWG Electrical Current Load Limits table with skin depth frequencies and wire breaking strength](https://www.powerstream.com/Wire_Size.htm)



The strikeouts showed up on your original thread. I think admins and moderators can delete posts but only you can edit your own content


---
**Em W** *October 07, 2018 18:04*

OK, great! Yeah, not sure how the strikeouts got there... Talk to you down the line when I get back and all the parts have arrived. This answers everything for now, and I can't thank you enough!


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/eacHpDDvRVn) &mdash; content and formatting may not be reliable*
