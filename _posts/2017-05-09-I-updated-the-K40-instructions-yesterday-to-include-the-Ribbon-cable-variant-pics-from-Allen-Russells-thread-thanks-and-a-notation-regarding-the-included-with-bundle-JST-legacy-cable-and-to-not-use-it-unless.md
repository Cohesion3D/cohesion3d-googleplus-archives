---
layout: post
title: "I updated the K40 instructions yesterday to include the Ribbon cable variant (pics from Allen Russell's thread, thanks!) and a notation regarding the included-with-bundle JST \"legacy cable\" and to not use it unless"
date: May 09, 2017 22:18
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I updated the K40 instructions yesterday to include the Ribbon cable variant (pics from Allen Russell's thread, thanks!) and a notation regarding the included-with-bundle JST "legacy cable" and to not use it unless specifically instructed to.





**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/bzawnViWkXh) &mdash; content and formatting may not be reliable*
