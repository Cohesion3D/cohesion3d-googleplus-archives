---
layout: post
title: "I've noticed in a few threads mention of installing a second 24VDC PS to run the C3D Mini from, instead of powering it from the K40 PS due to potential power issues with the K40 PS running everything"
date: February 23, 2018 20:38
category: "K40 and other Lasers"
author: "Seon Rozenblum"
---
I've noticed in a few threads mention of installing a second 24VDC PS to run the C3D Mini from, instead of powering it from the K40 PS due to potential power issues with the K40 PS running everything. 



Instead of getting a second 24VDC PS to run the C3D Mini from, would it not just make more sense to get a better overall PS to replace the one that comes with the K40 and still run everything from the one PS?



I have my K40 sitting here ready to upgrade, and my C3D Mini left the US on the 22nd, on the way to me in Australia , so while I am eagerly awaiting it's arrival, I'm trying to research and ensure I have everything I need here to do the mod as soon as it arrives.



To be honest, adding a new PS (cost and installation) isn't a problem for me, but the descriptions I've read on how to hyjack the wiring to have each PS power the correct "stuff", with shared ground scares me a little ;)



P.S. I asked at the bottom of another thread, but it might have gotten lost or overlooked, so I'll start a new thread for my Q.





**"Seon Rozenblum"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 20:39*

[https://plus.google.com/117720577851752736927/posts/emzYWQAAXuW](https://plus.google.com/117720577851752736927/posts/emzYWQAAXuW)


---
**Seon Rozenblum** *February 23, 2018 20:45*

**+Ray Kholodovsky** Yeah I've read that post through a few times, and got put off by the end both times, but maybe that's just because I don't have the parts in front of me to see exactly what is being described ;)



"working out which of the blue wires to cut"... sounds like a Mission Impossible movie script ;)



I do a lot of digital electronics work, but am SCARED OUT OF MY MIND when playing with mains power and power supplies. My survival instinct kicking in hard ;)



So that post also covers adding a second PS, but not the Q of just replacing the main one with a better one that does everything? Any thoughts on that? Or is that just a bad idea?


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2018 20:50*

I see. Most people aren't replacing the main one.  It can still handle driving the laser tube. We just want a better 24v supply for the board and motors. 



Don or Hakan on the k40 group could advise you if that's something you want to undertake.  I don't see the need to. 



If you've got some time we're working on a power upgrade bundle that uses a DC brick so there won't be any fiddling with mains required.  No firm timeframe though. 


---
**Seon Rozenblum** *February 23, 2018 20:55*

**+Ray Kholodovsky** Thanks! Ok, I'll ask the Q over on the K40 group about thoughts on replacing the entire PS - just for knowledge. 



Re waiting for the Power Upgrade bundle, I'm pretty impatient to get cutting with the K40, so I'll just grab a new 24VDC PS and do the "wire dance" and reach out for help if I need.



I'm super excited to get your C3D Mini into my K40 Ray! Everything I've read and watched suggests that the C3D with Lightburn brings the K40 to a whole new level of awesome.



Thanks for making such a great product and for offering such fast support :-)


---
**Don Kleinschnitz Jr.** *February 24, 2018 00:08*

**+Seon Rozenblum** not sure I understand your question thoroughly. 



Rather than add a DC supply to run the C3D you would like to change the LPS to one that will handle the Laser and the C3D, right?



If so, that is a good idea but the stock style K40 LPS is the only one that I have seen with a DC output. The stock supplys capacity is marginal. 



I also am not sure why you are wary of adding a separate DC supply. I would much rather have my controller power physically separate from my 20,000v laser power :)! 


---
**Seon Rozenblum** *February 24, 2018 02:38*

**+Don Kleinschnitz** Thanks for jumping in Don. I'm not wary of using a seperate supply for the controller... I think it's a great idea. I am wary of wiring up the second controller properly, but I'll do it. I'll get help if I need it.



I have a tendency of blowing stuff up when I play with power and mains. I don't want to blow my controller and then have to buy a new one and wait another 3+ weeks for it :D



You know there's always that guy that hurts himself the second he tries to do anything? The klutz? Well that's me and power!


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 02:42*

220v is no joke 


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/5sL1ovZ94z1) &mdash; content and formatting may not be reliable*
