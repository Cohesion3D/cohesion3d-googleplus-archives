---
layout: post
title: "Is there a way to make it travel from top to bottom without it jumping around to different areas to burn I think this would really help the constancy of the mark?"
date: August 11, 2017 04:56
category: "K40 and other Lasers"
author: "William Kearns"
---
Is there a way to make it travel from top to bottom without it jumping around to different areas to burn I think this would really help the constancy of the mark? I would assume this is the board processing the image? See photo for example. It jumped around from top to side at least four or five times.

![images/2c4f8f32328810cf73806b747afa0447.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2c4f8f32328810cf73806b747afa0447.jpeg)



**"William Kearns"**

---
---
**Arion McCartney** *August 11, 2017 05:05*

Are you using LW4? If so, try the vector raster merge feature if that file is a vector you are trying to engrave.  


---
**William Kearns** *August 11, 2017 05:06*

yes i am using LW4


---
**William Kearns** *August 11, 2017 05:08*

the preview looks promising will try that and update thanks 


---
**Arion McCartney** *August 11, 2017 05:13*

Cool. I had the same frustration, but the awesome devs implemented that feature which eliminates this exact issue with vector engraving.  Either that feature or convert vector to bitmap for engraving. The LW feature is much easier IMO.


---
**Ashley M. Kirchner [Norym]** *August 11, 2017 06:54*

For the record, the board does not 'process' anything, it simply follows instructions from the software. LW has a slider on the Files preview pane, after you generate the gcode, play with the slider and you can see exactly what LW is doing and the instructions it will be sending to the C3D board. 


---
**Alex Krause** *August 11, 2017 15:38*

Use a high quality png or bmp file and it will raster as one image 


---
**William Kearns** *August 11, 2017 15:48*

**+Ashley M. Kirchner** awesome is there a way to simulate mechanical engraving?


---
**Ashley M. Kirchner [Norym]** *August 11, 2017 17:22*

Yeah, turn off the laser output and send the job to it. I don't see any benefit in doing that as you can see exactly what it's doing within LW with the simulate slider.


---
**William Kearns** *August 11, 2017 17:47*

No I mean something to replicate the look of mechanical engraved items 


---
**Ashley M. Kirchner [Norym]** *August 11, 2017 18:34*

Ah, not that I'm aware of, no.


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/KdCYQe6yXyC) &mdash; content and formatting may not be reliable*
