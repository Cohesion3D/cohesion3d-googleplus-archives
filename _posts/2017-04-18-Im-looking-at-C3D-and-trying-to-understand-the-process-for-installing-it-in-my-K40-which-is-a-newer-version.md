---
layout: post
title: "I'm looking at C3D and trying to understand the process for installing it in my K40 which is a newer version"
date: April 18, 2017 03:00
category: "C3D Mini Support"
author: "Steve Clark"
---
I'm looking at C3D and trying to understand the process for installing it in my K40 which is a newer version. 



1)  As I understand... it so far (and I could be wrong) is it's basically is you pull the old board out and plug this one in using the same connection plugs as the old one. Fire it up and adjust a few parameters and your good to go. Is this correct??



I'm not an electrical background person but can do some simple (very simple) testing and installation.



2)  Now, G-Coding I am very good and experienced at...even taught it to machinist in night school. So the question I have is what format do you download it into the C3D? DXF? IGIS files or what? I have a old 2D CAM program that I can modify the post in and I'm dabbling with Fusion 360.



Thanks







**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 03:04*

Hi Steve. Could you put up some pics of your machine (wiring, stock board, psu) so I can take a look? 



1. Usually yes, but I will wait to see your machine before confirming. 

2. You don't put the DXF or whatever file directly to the C3D, there's an intermediate step called CAM. This is where the LaserWeb software comes in. It takes your DXF file and you set up the job from there. Then C3D takes the gCode output from LW. 


---
**Steve Clark** *April 18, 2017 03:09*

yes I'll do it tomorrow. 


---
**Steve Clark** *April 18, 2017 03:19*

Yes, I understand Cad and Cam. The program I have outputs the Cam (the G code cut path) from the Cad drawing a, but it may or may not be compatible with C3D's needed input even with me rewiring the post configurations.. So I get maybe needing to use something like LaserWeb.


---
**Steve Clark** *April 18, 2017 03:22*

Sorry, some mistypes that's 'rewriting' not 'rewiring'. 


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 03:32*

Depends on what you want to do. If you're like me and all you do is strict cutting of parts then you should be fine. Any CAM for CNC should be able to work with this board and lasering. I even wrote a post processor for vectric aspire that I use. 

A lot of other people though, and one of the main points of this upgrade, so rastering: image engraving. And for something like that you'd really want LaserWeb. 

Whatever works. 

As for a specific example, and again it's similar to CNC:

G0 X10 Y30 F600 is a travel move. Laser not firing. 

G1 X10 Y30 F600 S0.4 is a cut move. Laser fired at 40%. If you can output something like that, you should be able to work out of whatever software you want. 

May still need LaserWeb or one of several specific programs to send the gCode to the board though. 


---
**Steve Clark** *April 18, 2017 03:40*

![images/bc13daf8927236dbd8b92f8d2c6398ea.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc13daf8927236dbd8b92f8d2c6398ea.jpeg)


---
**Steve Clark** *April 18, 2017 03:41*

![images/4ad8d53c602ed81395a1362c4304cdbb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4ad8d53c602ed81395a1362c4304cdbb.jpeg)


---
**Steve Clark** *April 18, 2017 03:43*

There is a z axis Light Object table in this also.



![images/616faa27cb1a95b56537f523ebc9b76c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/616faa27cb1a95b56537f523ebc9b76c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 03:46*

This does indeed seem to be the newer version based on the color scheme but the wiring looks to be identical to what is in our k40 guide (link is pinned to top of this community). So yes it swaps right over to the C3D. 

For the z table black box driver we have an external stepper adapter that should make wiring that easy. 


---
**Steve Clark** *April 18, 2017 03:58*

OK, that all sounds good. I sent pic's tonight. You may notice I'd added to the basic unit but have not messed with the control yet. I started using this machine for lettering on a product I'm releasing soon. Now I just want to play with the k40 on the side but I have a strong dislike of the control.

![images/f220cc2b92028e4968b4fb98690222f5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f220cc2b92028e4968b4fb98690222f5.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 04:00*

Yep. Be prepared for some tuning after the install. What you said in #2 sums it up well. That's really all I can say. It's a bit more involved than just plugging in a microwave and requires the right mindset. 


---
**Steve Clark** *April 18, 2017 17:54*

I understand. 



This is as I understand it so far thanks to you and your website. Plus this excellent post:



[cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions.  : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



The C3D Mini Laser Upgrade Bundle is a drop in replacement/upgrade to the existing limited control board.  This controller uses G-Code commands that are downloaded from a CAM software program like LaserWeb (freeware). If you run this on windows, other than 10, you will need to download Smoothieware . OK I see that that is installed in the kit already.



It will run up to 4 axis but the kit is made up for 2 as that's all you need for the K40 .  



I would need to get The GLCD adapter for adding the screen  such as a Anycubic or Osoyoo 12864 LCD display. 



Question: I have a motorized Lightobject table setup with buttons for moving it up and down. What would be the advantage to adding the Z axis driver and control direct to the controller?  I can guess a few such as auto Z positioning,  multilevel engraving and focal point control by G-code within a program.



BTW - thanks for your time and response! i'm getting close to knowing what to order. 


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 18:02*

If you run this on windows, other than 10, you will need to download <b>Smoothie Drivers</b>.  Onto the computer.  

Smoothieware is the firmware on the board.  That's taken care of.  



Yep.



Yep. 



You can control the Z direct from LaserWeb.  Like I said for that we have an external stepper adapter. 



FYI the 4th axis is for a rotary.  Still experimental on the software side, but the board supports it. 


---
**Steve Clark** *April 18, 2017 18:15*

All interesting, thanks. One last question (maybe-grin)...



Where would I fine a list of the  G-Code's and other usable codes explained? Got a link? Even though I know the codes some change with different controls. I've looked but cannot find them so far. 


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 18:16*

From the documentation page: [smoothieware.org - supported-g-codes [Smoothieware]](http://smoothieware.org/supported-g-codes)



You'll probably want to read up heavily on that site. 


---
**Steve Clark** *April 18, 2017 20:50*

Thanks, I was on that site but missed the G and M-Code page somehow. I can see using some of the M codes for other things like air on air off relay, ect.


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 20:52*

Yep. 


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/Jj7xA3Tv3j5) &mdash; content and formatting may not be reliable*
