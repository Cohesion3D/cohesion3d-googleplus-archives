---
layout: post
title: "Apologize for the long post but need explaining...."
date: March 28, 2018 16:46
category: "General Discussion"
author: "Jim Fong"
---
Apologize for the long post but need explaining....



I’ve always had random halts while cutting/rasters on my c3dmini/k40 Laser using smoothieware.   Changed computers/usb cables/added 24 volt power supply/new stepper drivers etc.  The random halts always happens when running smoothieware firmware but never with grbl-lpc.  



It didn’t matter if I used Laserweb4, LightBurn etc as gcode sender.  



I thought I got rid of the issue by removing some USB background running tasks on the computer but it still happened. 



I figured it must be USB related so I removed the smoothie USB SDCard mass storage driver using windows Disk Management.  This effectively removes the smoothieware SDdisk drive access under Windows. Ran the machine for many hours and NO halts.  



Looking further, I find the  Smoothieware documentation warns of sdcard access while sending gcode over usb.  Mass storage device should be unmounted at all times.   I figure the random halts were due to any number of Windows background tasks accessing the smoothieware mass storage drive.  Anti-virus program, disk checking, Windows search etc.   grbl-lpc firmware doesn’t have sdcard mass storage option so that is why I never have random halts using grbl.   



Smoothieware happens to have a compile time option to remove the mass storage disk.   DISABLEMSD=1



I ended up compiling a version of smoothieware with DISABLEMSD. This version is also 4th axis rotary capable.  Download file, rename to firmware.bin, copy to sdcard, reset C3d board.   This will flash the new firmware.  No smoothie usb mass storage device will show up under windows anymore.   You can always re-flash the old firmware to get that option back. 



Hopefully your random halts will go away like it has for me.  Good luck. 



[https://www.dropbox.com/s/juub2of8m06zrm7/firmware%20c3d%20smoothie%204axis%20nomsd%203-20-18.bin?dl=0](https://www.dropbox.com/s/juub2of8m06zrm7/firmware%20c3d%20smoothie%204axis%20nomsd%203-20-18.bin?dl=0)





**"Jim Fong"**

---
---
**Joe Alexander** *March 29, 2018 07:53*

Great info, thanks for the research and for the Public Service Announcement **+Jim Fong**!


---
**E Caswell** *March 31, 2018 21:15*

**+Andy Shilling** know you were having problems at one time. Did you get them resolved? Might be worth looking at if not.

**+Jim Fong** thanks for this, I haven't had this as Got windows to ignore it from the start. still have the very odd drop out when big drives in my shop are switched. Know what effects it now so can manage it easily. 


---
**E Caswell** *April 19, 2018 21:10*

**+Jim Fong** do you have a config file for the Grbl setup?  I have a 3 axis grbl firmware but can't find a config file. Cheers.,


---
**Jim Fong** *April 19, 2018 21:16*

**+PopsE Cas** there is no external config file used for grbl-lpc. The settings are within the firmware and you change them with the $ command in a terminal.   They are saved automatically when modified.  




---
**E Caswell** *April 19, 2018 21:36*

**+Jim Fong**  only run grbl for a few weeks but only on x & Y axis, needed a axis so switched to smoothie and then never bothered after that.  Got a couple of larger rater rotary jibs so wanted to optimise it with grbl as smoothing still slower than I wanted it to be at times. Thanks for the response it looks like I have a big learning curve on Grbl coming.:-O




---
**E Caswell** *April 29, 2018 20:25*

**+Josh Smith** take a ok at this Josh.


---
**Josh Smith** *May 01, 2018 08:55*

still having issues with my machine noitced alarm indication on the front panel I cannot do 1 cut without it stopping would running Grbl fix this. 


---
**Jim Fong** *May 01, 2018 15:30*

**+Josh Smith** need to find out what error that Alarm is. Grbl isn’t going to fix if it is a hardware issue. 


---
**Josh Smith** *May 01, 2018 15:49*

**+Jim Fong**  so what you want me to do?




---
**Josh Smith** *May 03, 2018 08:26*

**+Jim Fong** **+Ray Kholodovsky** stuck with a machine i cannot use so what is the answer should i go over to GRBL to see if this fixes the hardware issues. or what should i attempt?


---
**Timothy Lathen** *September 15, 2018 22:29*

this is a .bin file my machine doesn't use a .bin where is the most recent file for my controller 




---
**Joe Alexander** *September 16, 2018 06:01*

**+Timothy Lathen**  you put the bin on the sd card and when the board boots up it loads the new firmware and changes the file to a .cur file. does that help?


---
**Timothy Lathen** *September 17, 2018 03:41*

Yeah I got it but just didn't have the config file after the update it is working great 




---
**Joe Alexander** *September 17, 2018 04:20*

glad your back up and running :)


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/CHzEkdbrhKe) &mdash; content and formatting may not be reliable*
