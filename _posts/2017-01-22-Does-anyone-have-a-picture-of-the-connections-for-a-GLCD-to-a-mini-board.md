---
layout: post
title: "Does anyone have a picture of the connections for a GLCD to a mini board ?"
date: January 22, 2017 21:24
category: "General Discussion"
author: "joe mckee"
---
Does anyone have a picture of the connections for a GLCD to a mini board ? 



Thanks 



Joe 





**"joe mckee"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 21:38*

![images/6b338a4ff2604accf804f9b00f4a5d2f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6b338a4ff2604accf804f9b00f4a5d2f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 21:39*

![images/1d2864728f63bfe577df7e03f9fa791b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d2864728f63bfe577df7e03f9fa791b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 21:39*

![images/11e038f8c810d45e2fb276fa93987ce2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11e038f8c810d45e2fb276fa93987ce2.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 21:42*

There's a 6x2 set of male pins labeled LCD expansion. The module goes onto there, with left point to the left as oriented by the boards being with the text readable, upright. 

On the module, EXP1 is on the left and EXP2 is on the right. Match these up to the labels on the back of the LCD. 


---
**joe mckee** *January 22, 2017 22:09*

Thanks for the fast response...I have 2 x 10 pin cables and the mini board has two connectors and are at right angles to each other ..all the pictures I can't see the bottom cable as it doesn't look at a right angle .

![images/11a90eee23a15ff110d492072a2f0c1b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11a90eee23a15ff110d492072a2f0c1b.jpeg)


---
**joe mckee** *January 22, 2017 22:09*

mini board 

![images/c1b01733b7c4d652439f6f137a7fd82b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c1b01733b7c4d652439f6f137a7fd82b.jpeg)


---
**joe mckee** *January 22, 2017 22:10*

other pictures in case it help someone 

![images/6cac9a04d0345ac002191d93ea965c3e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6cac9a04d0345ac002191d93ea965c3e.jpeg)


---
**joe mckee** *January 22, 2017 22:10*

stepper Y 

![images/a6aefa24d54db7ed90fa7ba4536efbef.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a6aefa24d54db7ed90fa7ba4536efbef.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 22:11*

First, have you installed the GLCD adapter onto the Mini?  This is a separate board. 


---
**joe mckee** *January 22, 2017 22:20*

That would explain two things why I had an adapter left over and why none of the pictures matched up ..thanks I will get on that soon ..it's coming together ..


---
**joe mckee** *February 05, 2017 16:23*

so the board is in I think its correct - and it lights up when the power is applied ..does the GLCD need any boot code or just some files to select from as the display is blank .




---
**joe mckee** *February 05, 2017 17:28*

All ok the brightness was so far up all i had was a bright screen .




---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 00:18*

Pictures of the back of the glcd with and without cables plugged in please. 


---
**joe mckee** *February 06, 2017 00:30*

All fixed Ray it was working just too bright to see the menu ..


---
*Imported from [Google+](https://plus.google.com/116374151501135747020/posts/QCWfRZ3GtBq) &mdash; content and formatting may not be reliable*
