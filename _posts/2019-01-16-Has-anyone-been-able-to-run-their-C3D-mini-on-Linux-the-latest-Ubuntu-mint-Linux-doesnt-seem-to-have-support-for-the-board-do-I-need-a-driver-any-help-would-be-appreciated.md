---
layout: post
title: "Has anyone been able to run their C3D mini on Linux the latest Ubuntu mint Linux doesn't seem to have support for the board do I need a driver any help would be appreciated"
date: January 16, 2019 23:21
category: "C3D Mini Support"
author: "Timothy \u201cMike\u201d McGuire"
---
Has anyone been able to run their C3D mini on Linux the latest Ubuntu mint Linux doesn't seem to have support for the board do I need a driver any help would be appreciated





**"Timothy \u201cMike\u201d McGuire"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:37*

I don’t use Linux locally so I don’t know about the latest versions/ if they break anything. Smoothie is supposed to work on Linux natively with no drivers, and they develop the smoothie firmware on Linux. 


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/eGZzmQFLijj) &mdash; content and formatting may not be reliable*
