---
layout: post
title: "Does the GLCD for Cohesion 3D Mini come with the cables necessary to connect together?"
date: July 25, 2018 21:37
category: "K40 and other Lasers"
author: "Mario Gayoso"
---
Does the GLCD for Cohesion 3D Mini come with the cables necessary to connect together?





**"Mario Gayoso"**

---
---
**Tech Bravo (Tech BravoTN)** *July 25, 2018 21:41*

Yes it does


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 21:42*

You need to buy the GLCD Adapter though. The cables come with the GLCD, and the knob too!


---
**Griffin Paquette** *July 26, 2018 01:09*

Yep as Ray said you need the Mini to GLCD adapter


---
**Mario Gayoso** *July 26, 2018 21:04*

**+Tech Bravo** Thanks for the heads up! I corrected my drag chain yesterday applying your technique and others in a new resulting one ! I will keep updates and upload a youtube video.

**+Ray Kholodovsky** Thanks, I did buy both :) **+Griffin Paquette**  Thank you!


---
**Griffin Paquette** *July 26, 2018 22:24*

No problem! Enjoy it!




---
*Imported from [Google+](https://plus.google.com/+MarioGayoso/posts/UNyBfNL8dWP) &mdash; content and formatting may not be reliable*
