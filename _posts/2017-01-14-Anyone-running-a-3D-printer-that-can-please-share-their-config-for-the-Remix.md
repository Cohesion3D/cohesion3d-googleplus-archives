---
layout: post
title: "Anyone running a 3D printer that can please share their config for the Remix"
date: January 14, 2017 23:38
category: "General Discussion"
author: "Ariel Yahni (UniKpty)"
---
Anyone running a 3D printer that can please share their config for the Remix





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *January 14, 2017 23:50*

Also what are you using for slicer


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2017 23:53*

You can use the stock smoothie config file from their website/ GitHub. 

If there's something specific you need help with, let me know. 

Here are some of my printer configs. They are very machine specific so I do not recommend blindly using them, but you can take a look:  [drive.google.com - Ray's ReMix Configs - Google Drive](https://drive.google.com/open?id=0B0o_gv35VFOMZzNMR0xoTFhJZk0)


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2017 23:54*

I use Slic3r. Cura and Simplify3D also should work, just make sure to do an update to the latest firmware to ensure no issues with S3D 


---
**Dushyant Ahuja** *January 15, 2017 12:40*

**+Ariel Yahni** while I haven't linked my config, this page might help. [3dprinterchat.com - Smoothiefy your 3D Printer](https://3dprinterchat.com/2016/10/smoothiefy-your-3d-printer/)



I use slic3r 


---
**Alex Krause** *January 15, 2017 20:32*

**+Samer Najia**​


---
**Samer Najia** *January 15, 2017 20:34*

I have 2 config files, one for my Prusa and one for my SD3 rebuild.  Will that do?


---
**Ariel Yahni (UniKpty)** *January 16, 2017 01:16*

**+Samer Najia**​ the prusa please


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2017 01:17*

"makerfront i3" in my link is also a prusa. Of sorts. 


---
**Samer Najia** *January 16, 2017 01:17*

Ok.  This is still in testing but I will send it to you...what is yr email?


---
**Samer Najia** *January 16, 2017 01:18*

**+Ray Kholodovsky**​is it better to post my config up there?


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2017 01:19*

No opinion either way.  I still recommend starting with the stock config and modifying according to specific needs.  And again, when you need something specific, just ask us.  Please.  It's better that way.


---
**Samer Najia** *January 16, 2017 01:21*

I can definitely recommend what Ray is saying here?  He is very very very (very) good about walking you through...I have done web screenshare sessions to solve issues and it helped a lot.  Ray will always help.


---
**Ariel Yahni (UniKpty)** *January 16, 2017 01:27*

Thanks guys


---
**Samer Najia** *January 16, 2017 03:15*

Ok so Ariel do you want to start with Ray's samples?


---
**Ariel Yahni (UniKpty)** *January 16, 2017 11:30*

**+Samer Najia**​ yes got them


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/BeQ7zZW9Ei3) &mdash; content and formatting may not be reliable*
