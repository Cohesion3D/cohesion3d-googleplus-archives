---
layout: post
title: "Hi Ray Kholodovsky and everyone, I'm curious about Cohesion3D remix board and thinking it might be more easy to fit my need"
date: April 12, 2017 05:07
category: "General Discussion"
author: "Xiaojun Liu"
---
Hi **+Ray Kholodovsky** and everyone, 



I'm curious about Cohesion3D remix board and thinking it might be more easy to fit my need.  I've just downloaded Cohesion3D source code from github. But I found myself a little bit confused. Does anybody knows which files are the latest or usable? Are the rev2 version of the .sch , .bld  and BOM  files found in the root directory the latest?   





**"Xiaojun Liu"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 05:11*

I'll send you a Dropbox link in the morning. I'm not positive if the Cohesion3D github had the latest. 


---
**Xiaojun Liu** *April 12, 2017 05:12*

**+Ray Kholodovsky** 👌 thx！😄


---
**Xiaojun Liu** *April 13, 2017 04:08*

**+Ray Kholodovsky** Remind in case you forgot :)


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 04:18*

It's morning somewhere... :)

This should be it: [https://www.dropbox.com/sh/sp9kfoxc4ihlqrw/AAApjO8Mn0raxKPxMYFKgFWWa?dl=0](https://www.dropbox.com/sh/sp9kfoxc4ihlqrw/AAApjO8Mn0raxKPxMYFKgFWWa?dl=0)


---
**Xiaojun Liu** *April 13, 2017 04:33*

**+Ray Kholodovsky** Alrighty;-) I assumed you are somewhere near US but... not important. Thx again for your kindness!


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 04:44*

Yep, New York time. 

So what are you thinking? Going to try to make your own ReMix? 


---
**Xiaojun Liu** *April 13, 2017 04:52*

**+Ray Kholodovsky** Yes, I have a friend who knows how to make pcb,  so I may want to make 10 pieces or so for my #delta and #DICE projects if it is cheaper than ordering. Originally I considered MKS robin which offers changeable motor driver and touch screen but it's close-source with firmware...Also I want to try multi-extrusion 


---
**Ray Kholodovsky (Cohesion3D)** *April 13, 2017 04:54*

I built about 40 of them by hand before we started the factory manufacturing

And then about as many of the small board. Not sure I could do it again, but yes, lots of fun. 


---
**Xiaojun Liu** *April 13, 2017 04:59*

**+Ray Kholodovsky** wow! I'm not able to soldering that well so I'll trust my friend;-) But i agree with you it is fun! If somebody can teach me I'll definitely try it by hand 


---
*Imported from [Google+](https://plus.google.com/+XiaojunLiu/posts/fTghDnUy6ho) &mdash; content and formatting may not be reliable*
