---
layout: post
title: "Hey Guys - A few questions please: 1) Is there a pin-input for reset so i can wire an external button?"
date: November 24, 2017 06:35
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Hey Guys - A few questions please: 



1) Is there a pin-input for reset so i can wire an external button?

2)  Where would I connect a fan?  On the mosfets?

3)  Is the attached picture an input just like the end stops inputs?  perhaps one good for a touch probe? 





![images/ba7b584b254ca3348c0987015612b3da.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ba7b584b254ca3348c0987015612b3da.png)



**"Todd Mitchell"**

---
---
**Griffin Paquette** *November 24, 2017 14:16*

The kill button has a male header next to it to wire an external kill switch, however no breakout for reset to my knowledge. 



As for a fan, the screw terminal on the bottom left corner of the board has 2 pin headers just above it to hook up fans and other peripherals. Check the corresponding pin number on the pinout sheet to configure it in the Smoothieware config file. 



The end stop ports are all tolerant for 24V, however you will either need to pull power from another spot on the board, or use the solder jumper to switch the voltage to whichever you would like to use. 


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/2tiPhywduiA) &mdash; content and formatting may not be reliable*
