---
layout: post
title: "Hi, I just discussed with Ray how to connect my external servo controller to the remix board with external stepper adapters"
date: December 05, 2016 18:44
category: "General Discussion"
author: "Marc Pentenrieder"
---
Hi, 

I just discussed with Ray how to connect my external servo controller to the remix board with external stepper adapters.



We got to the conclusion, that it would be best to use "open collector, option 2" (see pictures) with a 5V psu and smoothieware configured as open drain on the EN,DIR(Sign),STEP(Pulse) pins.



It would be nice if someone can have a look at it and proove it right or possible wrong.

Any suggestions are apreciated.



![images/589e0527da8accea5271e4bd3c4ceb92.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/589e0527da8accea5271e4bd3c4ceb92.jpeg)
![images/10995a59eefa86c82b79bc6b70bea7c2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/10995a59eefa86c82b79bc6b70bea7c2.jpeg)
![images/21e79d76ee289dab713185d68103acb5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/21e79d76ee289dab713185d68103acb5.jpeg)

**"Marc Pentenrieder"**

---
---
**Marc Pentenrieder** *February 07, 2017 18:57*

These are the four possible connection options. 

![images/929fd55fcd87525de4730103aa6a9aae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/929fd55fcd87525de4730103aa6a9aae.jpeg)


---
**Marc Pentenrieder** *February 07, 2017 18:59*

I want to try to talk to the servo controller with the remix board as a line driver (left 2 options). Is this possibe or do I have to use the option on the lower right?


---
**Marc Pentenrieder** *February 07, 2017 19:53*

Hi Jim, do I have to use line drivers or can i only use the signals from remix board as a 'line driver'? Lenght of shielded cable will be about 5 feet. Open collector option is limited to 200khz;-(


---
**Marc Pentenrieder** *February 07, 2017 20:01*

**+Jim Fong**​ What will happen if i connect 2cn-7 to step and 2cn-8 to ground and 2cn-11 to dir and 2cn-12 to ground? What will a line driver do different ?


---
**Jim Fong** *February 07, 2017 20:07*

**+Marc Pentenrieder** my test with the Cohesion3d board was only with cabling less than 1foot on my test bench. 



I have to look up the datasheet on the arm chip and see what the pin current sink ability is to verify but should not be a issue.  I'll hook up a Applied Motion brushless driver with a long cable using Open Collector tonight and verify. 



The smoothie firmware only can go approximately 100khz step pulse rate so you won't hit the 200khz driver limit. 




{% include youtubePlayer.html id="TTSkw-9NvW4" %}
[youtube.com - Cohesion3d Mini Smoothie testing](https://youtu.be/TTSkw-9NvW4)




---
**Jim Fong** *February 07, 2017 20:15*

**+Marc Pentenrieder** well the output pins of the processor can only put out so much current. Usually it does not have enough current to reliably turn on the optocoupler in the servo driver.  When you hook it up open collector option 2, the external 5 volt PS will supply the current to turn on the opto fully on. 



Line drivers are externally powered so will have no problem driving the opto. 






---
**Marc Pentenrieder** *February 07, 2017 20:21*

I have an signal level converter which will power step and dir to 5V/50mA. Is that enough to drive the optocoupler in the servo driver and use line driver option?


---
**Jim Fong** *February 07, 2017 20:28*

**+Marc Pentenrieder** yes with 50ma driver.  Looks like the opto requires only 15ma to turn on.  Give it a try.  




---
**Marc Pentenrieder** *February 07, 2017 20:35*

Thanks for your help, I will try it tomorrow witl line driver low speed pulse 👍


---
**Jim Fong** *February 08, 2017 16:48*

FYI



I hooked up a driver last night using open collector (really open drain since this is mosfet) with longer cabling and had no issues with the mini.  You need to change the config.txt so the output pins are "open drain"


---
**Marc Pentenrieder** *February 08, 2017 17:30*

Sorry, I didnt manage to test it out today. Ray already instructed me about [config.txt](http://config.txt). Thank you very much for testing things for me.


---
**Jim Fong** *February 08, 2017 17:34*

If you use a line driver, you would keep it standard output and not open drain config.   You may have to invert so it is active low pulse, but that depends on your particular driver. 


---
**Jim Fong** *February 08, 2017 18:27*

Don't forget the 180ohm series resistor to limit current to 15ma when using 5volt drive voltage.  This is very important.  


---
**Marc Pentenrieder** *February 08, 2017 19:19*

Do u mean I also have to use the 180ohm resistors when connectig as line driver? I cant see it in the documentation, or is it only important when using open drain?


---
**Jim Fong** *February 08, 2017 19:28*

**+Marc Pentenrieder** the "line driver" documentation is assuming you are going to use a 20ma Rs422 differential line driver.   No resistor required.  



You are using a TTL 5volt output driver with 50ma drive.   A opto is just a special LED.   You will have to limit the current to 15ma. 



Using ohms law.  E=IR

5/15=330ohms



They already put a 150ohm series resistor inside the driver as basic protection.  



333-150= 183ohms.   Closest standard resistor value would be 180ohms.  






---
**Marc Pentenrieder** *February 08, 2017 20:42*

If i connect the 5V signal to 2 servo controllers because of 2 motors on one axis, will the calculation be 5V/30ma=166ohm, which means 166-150=16 ohms for 2 motor axes?


---
**Jim Fong** *February 08, 2017 20:57*

Better way is just use two 180ohm resistors, one going to each drive.  On the very very very slim chance that one of the optos does goes bad,  you won't have a very low resistor value feeding higher current to the other drive, thus blowing the opto.  



More likely scenario is if you disconnect one of the drives for maintenance. Then you have too low of a resistor value feeding current to the other drive 


---
**Marc Pentenrieder** *February 08, 2017 21:01*

Okay, thanks. I will try tomorrow and tell you the results.


---
**Marc Pentenrieder** *February 09, 2017 19:44*

**+Jim Fong** **+Ray Kholodovsky**​

I managed to get the servo to turn somehow, but it is stuttering, if no signals are send.

I will make a circuit diagram and have a look at it, if something is wrong with the wires.

Everything looks good until i connect the servo controller to the remixboard.


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 19:47*

Tony's finding was that you may need to make the pin o for open drain or that + inverted thus !o

Both options may need to be tested once the wiring is confirmed. 

Thanks **+Jim Fong** btw I added you on hangouts.


---
**Jim Fong** *February 09, 2017 19:51*

I don't think I asked what model servo driver you are using.  Stuttering is usually just PID adjustments to get the motor to run smooth.  Does the servo driver software have auto tuning capability???



I usually adjust the PID settings to get the motor to be stable before even hooking up to the controller.  Adjusting PID is not always easy.  Auto tuning is great if you have it. 


---
**Marc Pentenrieder** *February 09, 2017 19:59*

**+Jim Fong** The motor runs smooth, when I send commands from remix board. It is only stuttering if no commands are sent. Sometimes it even moves smoothly without sending any commands ;-( 


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 20:02*

Aha!   Well that is very different from just "stuttering" which I understood as motor not moving.


---
**Marc Pentenrieder** *February 09, 2017 20:02*

As long as there is no cable between remix and servo controller, everything looks ok.

The relays are working and the leds (from level shifter/line driver) are working as expected.


---
**Marc Pentenrieder** *February 09, 2017 20:07*

By stuttering I mean, there is slight movement(+-0,5mm) and a light purring.


---
**Jim Fong** *February 09, 2017 20:09*

**+Marc Pentenrieder**  check your grounds. It's getting spurious noise from somewhere.  This is where having  a oscilloscope comes in very handy. Stuff like this is kinda hard to diagnose sometimes. 




---
**Jim Fong** *February 09, 2017 20:12*

**+Marc Pentenrieder** slight movement like that and buzzing is PID tuning usually.  Post a video so I can see. 


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 20:15*

I thought this was the whole point of the 2 differential lines - to cancel out any external noise.


---
**Jim Fong** *February 09, 2017 20:17*

**+Ray Kholodovsky** Marc isn't using differential line drivers. I don't use them either, kinda expensive, all my servo drivers are just standard 5 volt signaling on my CNC's.  I'm cheap lol


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 20:20*

Well that would explain it.  Based on our chat over the last few days I was under the impression this is what we were moving over to.


---
**Marc Pentenrieder** *February 09, 2017 20:25*

I am using a Dorna EPS-B1 servo controller. 

I will try to make a video of stuttering right now.


---
**Jim Fong** *February 09, 2017 20:27*

**+Ray Kholodovsky** diff line drivers are nice to have but not a requirement. If this was a noisy factory floor with 100foot cables, then of course it would be necessary.  Most people just use standard 5volt step/direction for both stepper and servo drivers for home CNC/3d printers. 


---
**Jim Fong** *February 09, 2017 20:33*

**+Marc Pentenrieder** oh this is very high end AC servo driver with lots of settings. I have to read the manual.....very well could be mis configured. 



Do you have rs485 or  CANbus cabling/interface on your PC to properly configure this driver.  



This is a really nice driver. 


---
**Marc Pentenrieder** *February 09, 2017 20:46*

Yes I have a rs485 cable and Software. 


---
**Jim Fong** *February 09, 2017 20:54*

**+Marc Pentenrieder** ok good because you will need it. 



Has this motor been installed and used before?  Load up the software and do a quick auto tune to get the ballpark settings configured first. 



I have to go to work in a few so I'll take a look at the manual when I get home tonight. 


---
**Marc Pentenrieder** *February 09, 2017 20:58*

**+Jim Fong**​ No, it has never been used, it was shipped from chinese manufacturer direct to me ;-)


---
**Jim Fong** *February 09, 2017 21:02*

**+Marc Pentenrieder** ok there might be certain limitations on how the step/direction inputs are to be used for this particular driver.  The manual will clarify much.  



I've only used high end Parker AC servos (most of my servos are DC brushless) before so I'll try to help you as much as possible. This driver you have is something I'm very unfamiliar with.  


---
**Marc Pentenrieder** *February 09, 2017 21:07*

I think its getting worser the longer it stays on. First there was no stuttering at all, then i sent commands and everyting was okay for about 60 sec. Then the stuttering began and i made the first video (without speech). After the video I sent some other move commands, which were okay, but then I had "ghost commands" (see other video). I will send a link to the videos when upload has finished.


---
**Jim Fong** *February 09, 2017 21:11*

**+Marc Pentenrieder** no turn it off for now.  Since it was never configured yet, the PID settings and such are factory default. It needs to be programmed correctly for the servo motor you have.  You can damage the motor.  


---
**Marc Pentenrieder** *February 09, 2017 21:19*

**+Jim Fong**​ just for information, the problem occurs only with a cable between remix and servo controller. The remix works fine alone and the servo controller also works great when used in manual mode (Jog mode). I stopped my tests for today,


---
**Jim Fong** *February 09, 2017 21:24*

**+Marc Pentenrieder** ok so atleast the initial factory settings are good. Im thinking that there is a configuration to correctly enable the step/dir inputs. Drivers like this usually comes from the factory with analog input enabled. 



Load up the software and make sure to choose the step/dir for input.   There is going to be lots of different configuration options. 


---
**Jim Fong** *February 10, 2017 16:26*

**+Marc Pentenrieder** I got a chance to read  the manual.  This drive looks like a clone of a early Yaskawa Servopack driver.  Not as advanced as I thought it was.  



Page 88, section 8.4 is the page for the parameter setting for step and direction.  You need to set this for it to work.  There are many settings to make this drive perform good and I don't even.know where to start.  The manual is very hard to understand.  


---
**Marc Pentenrieder** *February 10, 2017 16:45*

I will check the settings as on page 88. Have you seen the shared folder? I have uploaded the videos and documentation there!


---
**Jim Fong** *February 10, 2017 16:54*

**+Marc Pentenrieder** not sure where that is, looking for some link.....I'm using by Google groups app on the iPhone. 


---
**Jim Fong** *February 10, 2017 16:58*

**+Marc Pentenrieder** found it under "other updates" google drive link. 


---
**Marc Pentenrieder** *February 10, 2017 17:19*

Please have a look at the circuit_[diagram.pdf](http://diagram.pdf), if there is an mistake I made.

On the left side is the external stepper adapter on the remix board which is also powered by the same 24V power supply as the rest of the circuit.


---
**Jim Fong** *February 10, 2017 17:40*

**+Marc Pentenrieder**  diagram looks good from what I see. 


---
**Marc Pentenrieder** *February 10, 2017 17:49*

What do you think does double insulation (lower left side of servo controller) mean?

Do I have to use another 24V power supply switched by the relay?


---
**Jim Fong** *February 10, 2017 18:08*

**+Marc Pentenrieder** I think it means grounded and separated from the main 220 supply. 



The low voltage side (24&5 volt) must be separated/isolated from the high voltage (220 mains).   It depends on how your electrical codes in your country and you must adhere to them.  That is out of my specialty and beyond my ability to give correct advice. I only know what is recommended here in the USA for industrial control systems. I don't even know all the requirements here either.  I leave that up to the professional electricians wiring up the machines. 


---
**Marc Pentenrieder** *February 10, 2017 18:22*

Fyi the servo controller is 380V and one phase also powers the 24V power supply.




---
**Marc Pentenrieder** *February 10, 2017 18:23*

Do you know a good source, how to use an osciloscope. I can get one, but dont know how to use it.

By the way, do you think an SU4 as an replacement for the SU2 (dir/step)will help?


---
**Jim Fong** *February 10, 2017 18:48*

**+Marc Pentenrieder** I have a old analog Tektronix scope. At this point, I really think its configuration and tuning of the driver rather than any problems with the Cohesion3d board. A scope will not help with tuning and configuration.  


---
**Marc Pentenrieder** *February 10, 2017 18:57*

Okay, i will have a look into the parameters of the controller. But I dont understand why the servo moves, even if there is no signal from remix board and if i cut the signal cable, it  immediately stops moving and making sounds.


---
**Jim Fong** *February 10, 2017 19:02*

**+Marc Pentenrieder** it could be the servo analog inputs are still enabled and stray step signaling is causing it to move slightly.   Ground the analog inputs to verify. 


---
**Marc Pentenrieder** *February 10, 2017 20:10*

Where do you mean does the power for the hanging inputs come from. What do you mean with "ground the analog inputs"? Should i ground the shielding from signalcable?


---
**Jim Fong** *February 10, 2017 20:46*

**+Marc Pentenrieder** the servo driver can take move command via step/dir, rs485, CANbus and +-10volt analog inputs.    You have to look up pinout for the large connector and find the analog input pins.  Ground them to see noise interference is causing the problem.   You have to disable the analog inputs in the hardware configuration.  


---
**Marc Pentenrieder** *February 10, 2017 20:54*

Okay, i will try it. Thanks  for your suggestions.


---
**Marc Pentenrieder** *February 10, 2017 21:01*

Do you know what signal is coming from the remix (PA200 on page 88)?

Perhaps you can help me with your osciloscope with that?!


---
**Jim Fong** *February 10, 2017 21:02*

**+Marc Pentenrieder** also occurred to me that you are using the enable line.  When plugged Into the controller board and enabled, the drive becomes active.  Unplugging cable from controller deactivates the drive inputs, no movement.  


---
**Jim Fong** *February 10, 2017 21:31*

**+Marc Pentenrieder** PA200 is a software configuration parameter, not a hardware signal.  Since I  am unfamiliar with the software configuration for this driver and the manual is difficult to understand, you may have to contact the manufacturer for help on that. 


---
**Marc Pentenrieder** *February 10, 2017 21:50*

**+Jim Fong**​ I thought I have to set the signalform the smoothieware sends in PA200


---
**Jim Fong** *February 10, 2017 22:03*

**+Marc Pentenrieder** PA200 should be set to 

d0100



Plus/sign

Use CLR signal

Low speed channel 



PA000 should be set to h0000



I think but should check with manufacturer 


---
**Marc Pentenrieder** *February 10, 2017 22:31*

Thanks, i have only seen PA200 on page 88. The documentation is really difficult to understand. I think PA000 should be h.1000 because it is 380V (page 158). I will have a look at it tomorrow, it's half past eleven p.m. now. Thanks for now


---
**Jim Fong** *February 10, 2017 22:38*

**+Marc Pentenrieder** that why you need to check with manufacturer.  Documentation is horrible.  It could possibly be wrong too.  

There are so many other settings to check. I don't even know where to begin.   



YAskawa drives (and similar clones like this) were never meant for end user installs.  They were. Installed in machines and configured by technicians that were trained on these drives.  Very complicated drives. 


---
**Marc Pentenrieder** *February 11, 2017 17:34*

**+Jim Fong**​ **+Ray Kholodovsky**​

The analog inputs (speed and torque) are not soldered to the wire. So I grounded the high speed pins and the cable shielding. Now the servo is moving smooth, without ghost moves and dont make any noises ;-) Thank you very much Jim.



Now I have to recheck the parameters with the chinese supplier, this will take some time ;-(


---
**Ray Kholodovsky (Cohesion3D)** *February 11, 2017 18:06*

Aha wonderful!


---
**Jim Fong** *February 11, 2017 18:18*

Great! Glad to of helped.  


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/NHyCv6RDmYS) &mdash; content and formatting may not be reliable*
