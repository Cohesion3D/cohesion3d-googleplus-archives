---
layout: post
title: "I'm getting extra dark or kind of burnt areas when engraving wood"
date: July 05, 2017 00:55
category: "C3D Mini Support"
author: "laurence champagne"
---
I'm getting extra dark or kind of burnt areas when engraving wood. It's only at the points where the laser is finished with a line and is moving up or down to begin its next path across.  With my regular settings it just looks like charred areas. I've included a pic with my laser diameter set up quite a bit which gives a clearer picture of what is going on.

It happens with raster engraving and laser cut fill engravings.



Are there any settings I can adjust that will fix this issue?



![images/4d43fab4d8063dbe820116aaf47a6baa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4d43fab4d8063dbe820116aaf47a6baa.jpeg)



**"laurence champagne"**

---
---
**Jeff Lamb** *July 05, 2017 12:23*

Hi.  I found that the best way to stop this darkening on the edges was to use an overscan of a few mm.


---
**laurence champagne** *July 06, 2017 17:50*

**+Jeff Lamb** What exactly do you mean by using an overscan? 


---
**Jeff Lamb** *July 06, 2017 21:22*

**+laurence champagne** There is a setting in laser web. It's amongst the process settings underneath where you at power and speed etc. 


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/XZnJKWwAhAi) &mdash; content and formatting may not be reliable*
