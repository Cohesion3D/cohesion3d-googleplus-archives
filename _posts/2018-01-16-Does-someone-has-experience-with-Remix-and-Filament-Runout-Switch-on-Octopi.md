---
layout: post
title: "Does someone has experience with Remix and Filament-Runout-Switch on Octopi?"
date: January 16, 2018 17:19
category: "FirmWare and Config."
author: "Christian M\u00f6gele"
---
Does someone has experience with Remix and Filament-Runout-Switch on Octopi?

I tried this setup as described on the Octopi-plugin page ([https://plugins.octoprint.org/plugins/enclosure/](https://plugins.octoprint.org/plugins/enclosure/)) and it worked like expected.

Today morning during printing I tried to change Filament with manual input of M600 and the printer went crazy until I reset the Remix.

Does this function not properly working on Smoothieware or is using Octopi a problem? Should I connect the sensor directly to the Remixboard and enable Filamentsensor in config.txt?





**"Christian M\u00f6gele"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2018 22:39*

I'm not sure exactly what "went crazy" means. 

I have used M600 on ReMix just fine. But you need to configure it in config.txt. Check the smoothieware docs for pause and resume. 



Next... I have not used the Octoprint plugin. I have connected a switch directly to unused endstop port on Remix and configured that to be my runout sensor. Again, smoothie docs  


---
*Imported from [Google+](https://plus.google.com/101873307253403395337/posts/hQgz27W8uc7) &mdash; content and formatting may not be reliable*
