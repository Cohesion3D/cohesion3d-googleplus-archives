---
layout: post
title: "Originally shared by Ren Jurack My attempt of cooling six hot TMC2100 stepper drivers on a smoothiederivat with slow and silent airflow"
date: April 03, 2017 21:00
category: "Show and Tell"
author: "Ren\u00e9 Jurack"
---
<b>Originally shared by René Jurack</b>



My attempt of cooling six hot TMC2100 stepper drivers on a smoothiederivat with slow and silent airflow.

#cohesion3d-remix #biggerisbetter 



![images/659fa73aaeba88da4a7b178acf8f590e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/659fa73aaeba88da4a7b178acf8f590e.jpeg)
![images/cc1fe9f3b800899ed6173bd9061f4bb0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc1fe9f3b800899ed6173bd9061f4bb0.jpeg)
![images/42828bb80f796119f303ea31ac4d410b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/42828bb80f796119f303ea31ac4d410b.jpeg)

**"Ren\u00e9 Jurack"**

---
---
**Ashley M. Kirchner [Norym]** *April 04, 2017 00:46*

Next step: water block!


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 02:16*

Don't give him more ideas. He's already water cooling all other parts of the machine. 


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 03:02*

Then I am not giving him ideas, just some encouragements. :)


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 03:03*

Good answer. 


---
*Imported from [Google+](https://plus.google.com/+ReneJurack/posts/edKdwDaJ8Hs) &mdash; content and formatting may not be reliable*
