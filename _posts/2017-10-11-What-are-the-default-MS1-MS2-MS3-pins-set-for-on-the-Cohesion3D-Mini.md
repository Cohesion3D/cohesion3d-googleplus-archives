---
layout: post
title: "What are the default MS1, MS2, MS3 pins set for on the Cohesion3D Mini?"
date: October 11, 2017 16:56
category: "C3D Mini Support"
author: "ThantiK"
---
What are the default MS1, MS2, MS3 pins set for on the Cohesion3D Mini?  Can't seem to quickly find this information in the documentation.





**"ThantiK"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2017 16:57*

All connected by default. Solder/ trace jumpers on the bottom of the board. 


---
**Griffin Paquette** *October 11, 2017 18:09*

Like Ray said all are set to pull high. 


---
**ThantiK** *October 12, 2017 17:43*

Looks like I toasted those two driver ports.  Fuck.  With smoothieware, I connected, did a G28, with 3 drivers in each port, 3 motors connected, and only the "Z" axis moved.  A axis didn't move either but I assume that's because it's probably configured as an extruder by default.



SOOOOOOOO...looks like I need to recompile GRBL-LPC from [https://github.com/cprezzi/grbl-LPC/](https://github.com/cprezzi/grbl-LPC/) by the end of this.  Where are the pin definitions for the C3D mini located, and what make flags do I need to pass to build for the mini?

[github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases)


---
**Ray Kholodovsky (Cohesion3D)** *October 12, 2017 17:45*

Pinout diagram on the docs site.


---
**ThantiK** *October 12, 2017 17:47*

Not asking for the pinout diagram, that's clearly labeled -- I know how to edit smoothie so that it changes pins, that's no problem.  GRBL-LPC though, as I understand, needs to be recompiled in order to change pins?


---
**Ray Kholodovsky (Cohesion3D)** *October 12, 2017 17:51*

Yes, it does.  Ah... 

**+Jason Dorie** / **+Jim Fong** would know. 


---
**Jason Dorie** *October 12, 2017 17:52*

It’s all in the config.h file or possibly the cpu_map.h file depending on which version.


---
**ThantiK** *October 12, 2017 17:58*

**+Jason Dorie** thanks - It looks like cpu_map.h was what I was looking for.  config.h allows me to set which board it's building for, so this is the information I was hunting -- thank you so very much!



I won't be able to add a motorized Z or rotary table until I replace the board, but I might have some laser jobs lined up to take care of that.


---
**Ray Kholodovsky (Cohesion3D)** *October 12, 2017 19:17*

Use external drivers or a stepstick on a breakout board connected to other gpio pins on the board. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/VxeEDNAD7gi) &mdash; content and formatting may not be reliable*
