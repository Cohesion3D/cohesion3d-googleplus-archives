---
layout: post
title: "Hello. Have bought a Cohesion3D mini and put it in my K40, which is the flat cable model and control the engines with the Y driver"
date: October 24, 2017 22:14
category: "C3D Mini Support"
author: "TheLandbo"
---
Hello.



Have bought a Cohesion3D mini and put it in my K40, which is the flat cable model and control the engines with the Y driver. Everything was plug and play and I could turn the laser on/off and drive the trolley around the T2Laser JOG panel. However, when I send a g-code file to K40, the laser turns on and stays on until the g-code file is over and the laser goes to home position and turns off. It was not exactly what I put my nose up after.



I have had Zax, the author of T2Laser to look at the case and make a new version of his program that could support the mini. But it's a little in blind, because we can not figure out what's in the g-code that keeps the laser on. Is there one that has one or two small g-code files that can run correctly on the mini I need to borrow to check if there is a g-code problem?



All comments are welcome. Regards Leif.

![images/2473c0bb620dc708ab3dcd72d4811688.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2473c0bb620dc708ab3dcd72d4811688.jpeg)



**"TheLandbo"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2017 22:26*

Hi Leif.  



Here is something we have from "laserweb basics": [https://www.dropbox.com/sh/aet6p4z24mxi977/AADIeMMsmz_pyubXf4N7-AS9a?dl=0](https://www.dropbox.com/sh/aet6p4z24mxi977/AADIeMMsmz_pyubXf4N7-AS9a?dl=0)



I sent a board to Ralph Freshour who writes BenCutLaser, and when he was done with it the plan was to send that over to Zax to check it with all the T2 stuff. 



I need gCode in this format:



Typical travel move: G0 X10 Y20 F6000 

Typical cut move: G1 X10 Y20 S0.6 F600

We need the S value to indicate the % power level. In smoothie (default firmware C3D comes with) That's a 0-1 value.   In grbl-lpc this is between 0 and 1000. 



For sending raster to smoothie, it should use the fast-stream protocol to avoid stuttering. [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/smoothie-stream.py)



You can test the above very simply on your laser by using a gCode terminal such as the one in LaserWeb or pronterface.  The G0 line should move <b>without</b> firing.  The G1 line should move while firing. 



Zax is welcome to contact me directly, or if you can point me to a forum where you all are communicating I can jump in there. 


---
**TheLandbo** *October 24, 2017 22:41*

Thank you, Ray. Then we have something to work with.  / :-)



Best regards, Leif.


---
**TheLandbo** *October 24, 2017 22:53*

**+TheLandbo** 

Oh, totally forgot Ray. Forum is [benboxlaser.us - Benbox Laser Machines/CO2 Lasers/3D Printers - Index](http://benboxlaser.us)



Regards Leif.


---
*Imported from [Google+](https://plus.google.com/109955553605838159775/posts/Wi9gLSxmJq9) &mdash; content and formatting may not be reliable*
