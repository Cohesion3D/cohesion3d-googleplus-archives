---
layout: post
title: "Up-grade issues. with USB Drop-outs (Edit) Thanks to everyone for helping out, rather than read it all but if you are having USB connection drop outs this is what options you have"
date: March 21, 2017 20:45
category: "C3D Mini Support"
author: "E Caswell"
---
Up-grade issues. with USB Drop-outs

#usbdropout



(Edit) Thanks to everyone for helping out, rather than read it all but if you are having USB connection drop outs this is what options you have.



If your PC has not got PCIe 3.0 then install a new card, (link yo one I installed lower down)



Also either purchase a "data" only cable or cut in to the cable and disconnect the red and black cables out of your USB to USB B cable that supplies the board. What I would recommend it trialing this on an old cable first as if yours doesn't work then you haven't lost anything.



There is another option of installing a 4.7 cap in the VCC.Gnd line but that's messy when the other two above resolved the problem.



(End of Edit)



I am about to give up on this C3D board now. Tried everything that I have read about and looked through the K40 upgrade material until I am full to the back of the teeth with it.

I had to replace the M2Nano to get some jobs done today as lost so much time on it trying to get it to work in cognito with me.

Running widows 10-64 bit. Was using LW3 but changed to LW4. have more joy with 3 than 4 as can't seem to get it to do what I want. when I get more time I will post issues on there. (It maybe me trying to get used to LW full stop with files)

But the biggest problem is trying to get a job done complete without the Laser crashing, tried feeding direct from card and from PC but the jobs keep being interrupted.

I have found out today that whenever any thing is turned on/off within my workshop it interrupts the job, even when its direct from the card. (I even when my heater is on / off with the thermostat :-( not a happy bunny)

My workshop and house electrics have all been checked out and now been certified as being in perfect condition by a certified electrician.



Pic of my connections. I have no problems with operating the bed from any direction and all homing and park all works with no issues. it all goes wrong when I try to operate it via the G code. More pics to follow.



![images/ddaa528980f037ccfe6a5ca003bc7f4f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ddaa528980f037ccfe6a5ca003bc7f4f.jpeg)



**"E Caswell"**

---
---
**E Caswell** *March 21, 2017 20:47*

Whenever I power up my PC this is displayed on the GLCD. I have to disconnect the USB every time. Tried two Ramps screens and does the same with both of them

![images/053c0e7432d7454acd871f71cf85a565.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/053c0e7432d7454acd871f71cf85a565.jpeg)


---
**E Caswell** *March 21, 2017 20:48*

Keep getting "Halt Fault" and LW4 needs to be halted, I have to disconnect the USB, powerdown the K40 and then re-start everything.

![images/34e1743e16c57ed8190e0ebb22170366.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/34e1743e16c57ed8190e0ebb22170366.jpeg)


---
**E Caswell** *March 21, 2017 20:50*

On LW3 I get the box below, Lw4 it just disconnects the machine then have to discionnect the USB and power down the machine.

![images/0c291433fb6776d6a13556d515d53c98.gif](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0c291433fb6776d6a13556d515d53c98.gif)


---
**E Caswell** *March 21, 2017 20:51*

Apart from chucking the C3d card in the bin on the next walk past it, I'm not sure what else I can do. 


---
**E Caswell** *March 21, 2017 20:55*

every time I try to run a job on the C3d, either with machine connected or direct from the board I have to ask all my family not to switch anything on/off otherwise it will crash.  Run a job today with no one but me in the house, heating off in the shed and never touched a single switch and it run through a raster job.  If the C3d is so sensitive to those kind of deviations then its not going to work for me.

Even switching the internal light on in the K40 made it crash.. Very frustrated customer Ray..


---
**Jonathan Davis (Leo Lion)** *March 21, 2017 21:09*

**+PopsE Cas** resell the board instead of tossing it, then at least you can get some of your money back from the sale 


---
**Jonathan Davis (Leo Lion)** *March 21, 2017 21:10*

**+Ray Kholodovsky**  


---
**Ray Kholodovsky (Cohesion3D)** *March 21, 2017 21:21*

Sorry to hear you are having trouble, please stick with it a little longer, I will do my best to help.



First, allow me to address some of the specific points: 



Lines on the GLCD when you power up via PC:  Some computers provide less power over the USB Port than others.  GLCD needs a decent amount of power to start up and operate.  If you power up the laser and let that power the C3D board... does it work?  <b>Then</b> if you plug in USB, does the LCD work properly?  



"Keeps getting" halt:  We are aware of the the halt coming up when you initially hit connect in LaserWeb.  Just clear it and move on.  Does it happen at other times too? 

If so, potential troubleshooting strategies include trying a different USB cable or, do you have another MicroSD Card you can try?  



For the MicroSD just format to FAT32 and you need to put the config file from here on: [dropbox.com - C3D Mini Laser Batch 2 - Provided Config](https://www.dropbox.com/sh/4ai2p4q6l3cez26/AABvKZq5sAgypU2yxnPHgYrua?dl=0)



These two things can help troubleshoot a decent amount of issues.  

I'd like to hear back a bit from you about these points I brought up before we delve into the electrical side of things. 



Again, here to help.



Best,

Ray


---
**Kostas Filosofou** *March 21, 2017 22:08*

**+PopsE Cas**​​ i have all the above issues.. i have found a 'solution' ...  i open all the peripherals of the laser first.. water pump , air pump etc.. and then i reset my board and i start to run my job.. 


---
**Kostas Filosofou** *March 21, 2017 22:11*

**+PopsE Cas**​​ i have also read today in k40 laser group on Facebook that helps also to ground the whater container with a ground cable in the water.. i will try it also tomorrow


---
**Ashley M. Kirchner [Norym]** *March 22, 2017 00:10*

I tracked down my random disconnect issues to static. Unfortunately the equipment is in a room that has carpet (that I can't remove) and being in a very dry state, just about anything I touch will cause a discharge. Over the years I installed anti-static mats in my work area, my desk (different room), and work bench. Electronics don't behave well with static. I've build many a controllers only to watch them not work because somewhere along the line I fried a component. Not often, but it has happened.



That said, it seems you are having more of a power spikes issue since you mention it losing connection whenever something power cycles. Transient power spikes can be mitigated with a surge suppressor. A good power strip will do that (to some extend.) Shorter/shielded USB cable might help. Also try a different USB port on the computer, in case the one you're using might be flaky.



Unfortunately this is one of those cases where there are too many variables. Electronics are sensitive.


---
**Andy Shilling** *March 22, 2017 10:42*

I too had disconnect issues on a win 10 64 bit system, unfortunately mine and to be corrected using my Macbook Pro which isn't the best solution as I now have to stop what I'm doing and take the Mac out to the workshop just to run a job. I have tried running from the SD card but a couple of jobs have stored doing it that way as well.



Rather than bin it, is there somebody local to you whom could test it for you in their environment to see if it is your system etc.


---
**E Caswell** *March 22, 2017 13:17*

**+Ray Kholodovsky** 

"please stick with it a little longer" Had to drop back to M2 nano and do some jobs so will have another go when I get the time.



"If you power up the laser and let that power the C3D board... does it work?" Yes it does. 



"Then if you plug in USB, does the LCD work properly? " Yes it does.!



But not using the laser every day/minute of my life I use the PC for other stuff as well. So what you are saying is I have to disconnect the C3d board after every time of use then ? 



So how much wear is that going to have on the USB 'B' port on the board?



"Keeps getting" halt:  We are aware of the the halt coming up when you initially hit connect in LaserWeb.  Just clear it and move on."



I wished it was that simple Ray, even after reconnecting and re-setting the halt I find before it connects I have to unplug USB and power down K40 then re-boot everything, even LW4.Then I have to restart getting the job together again. Damm annoying to say the least.



 "Does it happen at other times too?"



How many other times can it not happen??



"If so, potential troubleshooting strategies include trying a different USB cable or, do you have another MicroSD Card you can try? "



Been there done that, I have a short 500mm shielded lead and also I have an 8gb, 4gb and the card that came with it as well. All the same happens with whatever I try.



"For the MicroSD just format to FAT32 and you need to put the config file from here on: drop box" 



OK I will try again.








---
**Andy Shilling** *March 22, 2017 13:34*

What sort of power connector do you have on the back?  I have a 3 pin kettle plug with a built in 10amp fuse and that kept arcing out, that made mine go wrong also until i swapped it out for a standard fitting with sepperate fuse.


---
**E Caswell** *March 22, 2017 13:52*

**+Andy Shilling** Same as you Andy, but no arching. I can connect direct with a fused plug to see what that does. Will try anything once. Got a couple of jobs first via M2 nano. spent so much time on this its becoming a very expensive up-grade that doesn't seem to work at the moment. 


---
**Andy Shilling** *March 22, 2017 14:08*

**+PopsE Cas**​ yes i bet it feels like that, but I'm sure Ray will see you through it. Where in the world are you as my biggest problem is getting to chat to people without the hrs gap in between due to time zones.


---
**E Caswell** *March 22, 2017 14:24*

**+Andy Shilling** UK Andy. 


---
**Andy Shilling** *March 22, 2017 14:31*

Ok cool not anywhere near Kent are you?


---
**E Caswell** *March 22, 2017 14:36*

**+Andy Shilling** no, i am in the midlands. 


---
**E Caswell** *March 22, 2017 19:52*

**+Andy Shilling** Tried connecting direct to the Emergency switch via a fused supply, same things happen. Thanks for sharing you findings.


---
**Andy Shilling** *March 22, 2017 20:06*

Ok sorry it's not any help, if you get stuck and would like me to plug it into my machine just give me a shout, for the cost of postage it might be worth checking out.


---
**Ashley M. Kirchner [Norym]** *March 22, 2017 20:33*

As an absolute last resort, you may also look at the power supply in the laser. If you are getting power spikes that are disconnecting the board, those are coming from the PSU. And if it's not suppressing transient spikes, something's wrong with it. This is why I suggested a surge suppressor (if you aren't already using one.)


---
**E Caswell** *March 22, 2017 20:43*

**+Ashley M. Kirchner** Thanks, It works perfectly well when nothing else is being switched in my house or workshop. even the k40 light being switched makes it crash.  I have an electrician baffled and he is now looking at putting a surge protector on the incomer for me. But he says that wont stop the problem of even switching the K40 lights on, the laser switch even stopped it yesterday. Checked all power connections to see if any were loose as well.


---
**Ashley M. Kirchner [Norym]** *March 22, 2017 21:04*

Exactly, that tells me something is wrong with the power supply in the laser itself.


---
**Andy Shilling** *March 22, 2017 21:07*

I'm not convinced on the power supply, unless the C3D is drawing more power than his M2nano making it react differently.


---
**Ray Kholodovsky (Cohesion3D)** *March 22, 2017 21:09*

The Laser Power supply is limited to 24v 1a.   It is possible that yours is very close to that max and would explain why any brownout in the house electrical system also causes a brownout on the electronics side.  If this is the case, a separate 24v power supply to power the Mini may be appropriate.  Some k40 machines actually come with a separate 2nd 24v PSU for powering the motors.  Again, we are int he land of conjecture here given how many variables are in play, but the comment about  "even switching the K40 lights on" does suggest this. 


---
**Andy Shilling** *March 22, 2017 21:19*

I've got my old 24v PSU i can post up to you if you want to try running the two.


---
**E Caswell** *March 22, 2017 21:43*

**+Ray Kholodovsky** I will try that one.  **+Andy Shilling** thanks for the offer Andy, I may take you up on that.  I will speak to my leccie friend in the morning, sure he will have something as he is bringing the capacitor and surge protection so see what that will do. 


---
**Andy Shilling** *March 22, 2017 21:58*

**+PopsE Cas** No problem just give me a shout if you need it.


---
**E Caswell** *March 23, 2017 18:55*

**+Ray Kholodovsky** **+Ashley M. Kirchner**

Update on today's attempts. 

With LPS powered on, C3D connected to PC via LW4. 

Tested outputs from LPS to check voltages, noticed that as soon as connected the DMM to LSP input connection the board disconnected. Thinking it was a conincidence, tested again and did exactly the same again and again.



This also did it when we was checking any other voltages from the LPS.



Checked for dry joints on LPS and all good with no signs.



The workshop electricity supply is now independent and on a MCBO supply. 



I Now don't get the house causing problems. However the workshop still does. 



**+Andy Shilling** Andy, I have a 24v dc power supply coming so will try board on independent supply, thanks for your offer, if I still have problems I will get in touch.



However,  Leccie not convinced that will resolve it as board disconnecting from PC even when not completing a job.



Once we realised it was disconnecting when not carrying out any jobs, we tested many times and happened time and time again, again remember it was every time when no job was being carried out. 



It does exactly the same when switching anything in the workshop, regardless of what it is. 



now I have only the workshop causing problems it's me in control, however if anyone have any further ideas as I am fast running out of them.




---
**Andy Shilling** *March 23, 2017 19:07*

**+PopsE Cas**​ glad your getting somewhere (ish) offer stands if you need it. I doubt it would make any difference at all but have you tried swapping your stepper drivers around, i notice you've got 4 on but only using 2. That and the 5v supply going into the C3D are the only real difference to my board.


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 19:11*

The Mini does not take any 5v in from the k40 power, so that should not be applicable. 


---
**E Caswell** *March 23, 2017 19:18*

**+Andy Shilling** that's a thought, will remove the other two tomorrow morning see what diference it has.  Only put them there for safe keeping as planning a z table. 

However if this C3D doesn't work that won't happen and it will be ripped out and go back to stock equipment. 

Spent so much "none productive time" on it to get to this stage. Ended up switching to M2nano again today just to get jobs done. The problem I have is even the oil filled radiator thermostat trips it out, so just to get it to work I will have to switch everything off and in the winter that won't go down very well. 

TBH I'm not impressed with the board, it shouldn't be susceptible to this kind of problems.


---
**Andy Shilling** *March 23, 2017 19:21*

**+Ray Kholodovsky**​ yes I know but nobody seems to have a clue on this one so rather than do nothing surely comparing it to another board that is working can't do any harm can it?


---
**E Caswell** *March 23, 2017 19:22*

**+Kostas Filosofou** do you have any problems when anything else in the room is switched on/off Kostas? Be interesting to know.


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 19:23*

**+Andy Shilling** not objecting, just making sure everyone has the correct info. 


---
**Andy Shilling** *March 23, 2017 19:30*

I know sorry my response seemed a little sharp when I re-read it. I just know how frustrating it is getting this disconnection problem. I tried mine on the pc again last night and rather than disconnect it kept pausing; if I then pressed the pause button on LW3 and then the play button it carried on with the job in hand until finished. It done this 3 times on 2 jobs but then the third job sailed through.


---
**Kostas Filosofou** *March 23, 2017 20:03*

**+PopsE Cas**​​ yes ... When I turn on my mill or my table saw for example when i run a cut on the laser.. I lost the connection with the board 


---
**Kostas Filosofou** *March 23, 2017 20:09*

Ah and just to clarify that i have separate power supply for my  C3D  and is about 30-40cm far​ from the rest laser electronics..


---
**E Caswell** *March 23, 2017 21:49*

**+Ray Kholodovsky** it seems there is a potential reoccurring problem when using the board caused by either harmonics or other electro interference from other [sources.as](http://sources.as) per Kostas comments above. **+Kostas Filosofou** 



Kostas thanks for the feed back, seems from your responses then it may be a problem I will need to work around if I want to persevere with the board. I will connect a second supply for the board only to see what that does. 



**+Andy Shilling** thanks for your responses, without them I am sure I would be no further forward.






---
**Don Kleinschnitz Jr.** *March 24, 2017 11:33*

Been watching this thread......

...............

"Tested outputs from LPS to check voltages, noticed that as soon as connected the DMM to LSP input connection the board disconnected. Thinking it was a coincidence, tested again and did exactly the same again and again."

 

This is a very strange and significant symptom because DMM's have very high impedance and should have no impact on the PS or the system:

I would try and understand this symptom because if the PS is this sensitive to a DMM impedance it certainly will react to most any noise.



1) what voltage were you measuring

.............................

There may be more than one problem in this caper. 

Machine equipment (mills, saws, etc) are notorious for generating common mode noise that effect electronics. For example GFI's will tend to trip in these environments.



The stock K40 machine has no input filter like most industrial equipment that contain control electronics. One idea is to install one:

[amazon.com - Uxcell AC 115/250V 20A CW4L2-20A-S Noise Suppressor Power EMI Filter: Amazon.com: Industrial & Scientific](https://www.amazon.com/Uxcell-CW4L2-20A-S-Noise-Suppressor-Filter/dp/B016EJ5DU2/ref=pd_sim_328_3?_encoding=UTF8&pd_rd_i=B016EJ5DU2&pd_rd_r=KER7Y22TFKWY7CRB3SR0&pd_rd_w=wF0Y8&pd_rd_wg=vUJdm&psc=1&refRID=KER7Y22TFKWY7CRB3SR0)



.........................

This does not explain why one controller is susceptible and another isn't. However, in a noisy electrical environment even well designed controllers can be susceptible in different ways to different noise bands and types.



Hope this is some help....


---
**E Caswell** *March 24, 2017 12:27*

**+Don Kleinschnitz** thanks for your information Don, it's much appreciated. 

..............................

1) what voltage were you measuring

There may be more than one problem in this caper. 

Machine equipment (mills, saws, etc) are notorious for generating common mode noise that effect electronics. For example GFI's will tend to trip in these environments.

..............................

when I test the LPS input voltages with the DMM, as soon as I put the probe on the live terminal the board disconnects. 



Thanks for the link, I have fitted ferrite rings on the incomer but the link you provide may be a good improvement. Duly ordered.








---
**Don Kleinschnitz Jr.** *March 24, 2017 12:38*

**+PopsE Cas** even with the filter (which is good to have anyway) something is not right if just touching the LPS inputs with a meter causes a disconnect..... we must be still missing something. 


---
**Andy Shilling** *March 28, 2017 13:04*

**+PopsE Cas**​ hey did you get this sorted. I'm still getting freezing problems with mine unless I use my Mac.


---
**E Caswell** *March 28, 2017 19:27*

Hi **+Andy Shilling** no, still waiting for second power supply.

Tested without other axis controllers.

Tested with a lap top, and desk top PC

Tested with a new shorter (40cm) fully shielded cable.

Split the ring main in the workshop just in case it was creating a magnetic field.

Tested with workshop ring main off powered laser from house on extension lead and lap top to operate.

I think we have tried everything possible and are clear out of ideas, but will reserve comment when I have fitted the PSU and filter.

I have had an offer of a full smoothie board converted for 24v laser so I may take that offer up and see if that works. 


---
**Andy Shilling** *March 28, 2017 20:01*

Ok let me know how you get on, if you end up selling the C3D i may be interested in it for my 3d printer I'm building, but I'd rather hope you get it sorted.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 20:19*

Likewise, hoping we can figure out what's going on here. Awaiting your further feedback once the 24v psu comes in. 


---
**Andy Shilling** *March 28, 2017 20:28*

**+Ray Kholodovsky**​ while your about is the C3D ok for 3d printers and if so what software would be best? I've got that crappy Hictop Mendal at the moment and I'm building one of Tom Salanderer's i3 mk2 clones and thought the C3D might improve on the Arduino/ ramps setup.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 20:44*

Absolutely **+Andy Shilling**. We have a few i3 types and deltas running it. 


---
**Andy Shilling** *March 28, 2017 20:51*

**+Ray Kholodovsky**​ Cheers buddy.


---
**Kostas Filosofou** *March 30, 2017 18:57*

**+PopsE Cas** a small update from me.. I bought a EMI filter (20A)  like **+Don Kleinschnitz** suggests , i connected to my main power socket of the laser with no luck... The issue remain the same as before. after i power on the laser (+ water pump, exhaust) i lost connection with the C3D. 


---
**E Caswell** *March 30, 2017 19:21*

**+Kostas Filosofou** thankyou for the up date. I haven't been able to get mine done yet as other priorities to catch up on spent far too much time and money on something I expected to drop it in as a "plug and play"  so will have to wait as I'm using the M2 Nano at the moment. Hopefully try it out again once get the 24v power supply, filter and some free time. 

I could work around it by switching off my heating and switch appliances on/off of other shop equipment while it's doing it's job, but to be honest that would cut any other work time down in the shop as my SWMBO uses the shop at the same time I am in there. Along with me doing other work while it's doing the jobs.


---
**Don Kleinschnitz Jr.** *March 30, 2017 20:16*

**+Kostas Filosofou** I am distressed that the filter did not work but it is a good thing to have in your machine. I assume that you connected the filters ground to the frame near where it is installed with a short and good size wire?



Keep in mind that the source of noise can be different for you and **+PopsE Cas**.



Just to insure we refresh our understanding, when you turn on the laser all is good until something in the shop is turned on and then it disconnects. This is even before running a job and firing the laser.



Is your pump, exhaust connected to the AC outside of your machine?


---
**E Caswell** *March 31, 2017 07:30*

**+Don Kleinschnitz** thanks for the reply Don,  for your info.



.......................................

Just to insure we refresh our understanding, when you turn on the laser all is good until something in the shop is turned on and then it disconnects. This is even before running a job and firing the laser

.........................................



Yes, once connected no matter what I turn on/off in the shop it disconnects, even tho there is no job running or laser firing.

Switching shop lights, small power tools and even the heating starting and stoppping via it's stat.

Only shop electrics now cause problems. 



I have tried running the extractor and pump from both the laser outlets and also running from shop electrics and still same problem.



All my electrics have been tested for grounding and safety by a certified electrician and have all passsed and I am now the proud owner of a certificate to say so..




---
**Don Kleinschnitz Jr.** *March 31, 2017 12:02*

**+PopsE Cas** ugggh!



Stabbing..... does it reset if its not connected to the USB port?



**+Ray Kholodovsky** how can he tell if the board reset? I am assuming the noise resets the board and that is why he is getting disconnected?


---
**E Caswell** *April 01, 2017 06:57*

**+Don Kleinschnitz** when running jobs direct from the memory card it goes in to "Halt" mode when any interference occurs.

I have to reset it on the GLC and then it clears, but the job has to be restarted again.


---
**E Caswell** *April 03, 2017 12:13*

**+Ray Kholodovsky**

I now have a little time and machine availability to trial run the separate 24v power supply. 

To make sure its connected correctly can you confirm what you want me to connect and where please?  



Is it just the 24 supply G / + or all connections.?



Not got the inline filter yet tho so that will have to wait-



My plan would be to leave L and 5v connected to LPSU and just connect the +/- to new power supply


---
**E Caswell** *April 03, 2017 20:31*

**+Ray Kholodovsky** connected +/- to 24v and left the L and 5v connected to the LPSU. 

Still no change, any further suggestions? Waiting for filter that **+Don Kleinschnitz** suggested but now completely out. 


---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2017 20:47*

**+PopsE Cas** Hello again, let me get caught up here...



Wiring: Would advise hooking up +24v and Gnd from the new psu to the Mini (this can be done via the screw terminals on the left) and yes L stays from the old PSU.  Also need to have a shared ground between both PSU's.  


---
**E Caswell** *April 04, 2017 13:30*

**+Ray Kholodovsky** 

Connected via the screw terminals as directed Ray, still the same issue. drops out no matter what I use in the shop. 



Powered the board up directly on the work bench and it still DO's. (That's not even connected to the laser. Laser unit was also powered off) 



Anyone else come up with any suggestions?



I maybe the only one using this board in a workshop and using other equipment at the same time. 



Bear in mind SWMBO was using a pyrography machine yesterday and the heating element DO'd the board when it was switching. 



Seems that its all hinging on the filter that Don suggested now.




---
**Andy Shilling** *April 04, 2017 13:56*

Doesn't the board run off 5v pulled from the 24v? If that's the case could you connect the Glcd and power it from your laptop then turn a couple of thing on in the shop to see if it resets itself.



Wouldn't that then confirm without doubt that it's a problem on the board? 


---
**Don Kleinschnitz Jr.** *April 04, 2017 13:56*

**+PopsE Cas** may have been asked already but do you have access to a oscilloscope? 


---
**Don Kleinschnitz Jr.** *April 04, 2017 14:04*

**+Andy Shilling** **+PopsE Cas** I think that it is already clear from this last test that the noise is conducted through the power supply whether its the separate supply or the laptop. 

If powering it from the laptop prevents the problem from happening then I would says its power supply it better filtered. 

In either case we need to figure out how to filter that board. 

That's why we need to understand what it looks like on the board.



BTW occasionally when I switch on my vacuum my smoothie (not a C3D) will reset but not as often as this.


---
**E Caswell** *April 04, 2017 17:03*

**+Don Kleinschnitz**. I don't have an oscilloscope Don, but I do know someone who does have. I will ask the question. Is there anything specific you want to test.

One thing with the separate power supply it doesn't DO when operating the extractor or the light now. But that doesn't really help as could have turned them on prior to starting a job. 



 **+Andy Shilling** I have already tested the board without the GLCD and does the same. Thanks for the response tho as it's appreciated.

Out of interest tho, I can't definitely say it's the board, once I have tested independently with another PC or laser then I can't say it's the board. In all honesty I don't believe it is. 

However it would be nice to hear a bit more from **+Ray Kholodovsky** to see if he has any further ideas. 


---
**Andy Shilling** *April 04, 2017 17:18*

**+PopsE Cas**​ sorry not making it clear plug the board into your laptop with the glcd in place but not the 24v supplies, then turn your lights or any other thing that usually created the problem on and see if the board resets with just the, 5v supply from the usb. I just can't see that you would be having problems with the new psu in place. I know the filter will suppress any noise but if it's a new psu what are the chances it will recreate the same scenario? 



Can the new psu run the whole show or is it just a 24v supply?


---
**Don Kleinschnitz Jr.** *April 04, 2017 17:40*

**+PopsE Cas** we want to look at the power supplies and see if we can see high frequecy spikes when you switch stuff on.



Then we would look at the internal power on the board for the same.



BTW do you have access to some capacitors like .01 and .1 uf?


---
**E Caswell** *April 04, 2017 19:06*

**+Don Kleinschnitz** thanks, I will speak to my friend see if he can take a look. 

**+Andy Shilling** already done that and it doesn't trip, not until 24v is powered to the board.

New 24 only supply as didn't fancy buying a new LPS unit. 



**+Ray Kholodovsky** I have taken the board to a different house, different PC with laserweb4 running. Tested with fully shielded short 400mm lead and a long 1000mm lead. 

Set it up as a bench top trial as I did above with 24v supply and still drops out.

After loads of discussions we decided to set it up with lead acid battery powered 24v supply with 2a protection and it still dropped out.



Out of interest it dropped out just by operating a hair dryer and also a vacuume cleaner in the same room.(trying to simulate workshop operating) 

 Every time it was switched on we reconnected the board and then switched it off and it dropped out again.

Well I have utilised 2 electrical engineers, process control technician, and a computer nerd (sorry nick in case you read this) and we are clear out of ideas.



Do you have any  ideas?






---
**Don Kleinschnitz Jr.** *April 04, 2017 19:32*

**+PopsE Cas** whoa,  are you saying that it is sitting alone on the bench connected to a battery and it dropped out when a hair dryer was turned on?

It was connected to your laptop USB? 

When you say dropped out do you mean lost connection at the USB? Do you have a panel and does it still work after that happens and what does is do?

Uggh! This gets weirder with every post....


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 19:37*

**+PopsE Cas** first off, thanks for the time you've spent debugging this.  I think it's fair to say this is "above and beyond" what anyone would expect.  



I'd like to quickly address something you said a bit higher up:

"when running jobs direct from the memory card it goes in to "Halt" mode when any interference occurs.

I have to reset it on the GLC and then it clears, but the job has to be restarted again."



I want to make sure we mean the same thing when you say the board resets.  Is it this, that you find the board halted?    That alone is not the same thing as a full on "reset". 



So when you go to reproduce this "drop out" like you said in the above post - what exactly happens? 

Do you observe the LEDs on the board go dimmer, or perhaps the GLCD do that?  That would be a brownout in the truest sense of the word.   

Do you find the board halted? 

Is the text on the GLCD still normal or is there any "static" on there? 

Again trying to figure out exactly what is happening. 


---
**Andy Shilling** *April 04, 2017 19:48*

This is a really silly question but....... **+PopsE Cas**​ if you are swapping your boards about you are also doing the same with the +24v and gnd aren't you. 



Any image i can find on the M2nano the pinout is different for the power inputs.

Might be me finding older boards but you never know.


---
**E Caswell** *April 04, 2017 20:43*

**+Ray Kholodovsky** 

You see I really want this to work as I know Lw 4 will give me marked benefits and surely improvements over the stock kit. having said that I have done all my work recently on stock kit and the finance officer is asking questions of the validity of my purchase..



<s>--------------------------------------------------------------------------------</s>

I'd like to quickly address something you said a bit higher up:

"when running jobs direct from the memory card it goes in to "Halt" mode when any interference occurs.

I have to reset it on the GLC and then it clears, but the job has to be restarted again."



I want to make sure we mean the same thing when you say the board resets.  Is it this, that you find the board halted?    

+++++++++++++++++++++++++++++++++++



As per the second pic at the start of the thread Ray, it goes in to "HALT" and then I have to clear it. the same happens if the reset button is pressed o the C3D board.



<s>-----------------------------------------------------------------------</s>

That alone is not the same thing as a full on "reset"..

+++++++++++++++++++++++++++++++++++



I am not picking at if its a "full on re set"or not.  What I am saying Ray is the laser will not take a full job regardless of any other kit being used in the workshop.  weather connected to the PC or not. I cant get it to run without the PC connected so tell me what I can do to make it work without it being so. it keeps going in to "HALT" mode.. Nothing you have said has even got close to showing me what will or will not work.



<s>--------------------------------------------------------------------------</s>

So when you go to reproduce this "drop out" like you said in the above post - what exactly happens? 

Do you observe the LEDs on the board go dimmer, or perhaps the GLCD do that?  That would be a brownout in the truest sense of the word.   

Do you find the board halted? 

Is the text on the GLCD still normal or is there any "static" on there? 

+++++++++++++++++++++++++++++++++

The board disconnects with Laser web 4, it shows "machine disconnected" in the information zone.

I have not noticed the LED's as I have not been asked to look or check them out.

The GLCD shows normal text and appears bright.

in the "jog" screen on Lw4 there is a status it asks me to reset the alarm, (where the "abort job" would normally be.)

I press to reset that and then have to reconnect the machine in Lw4. .

The board shows "halted" in the GLCD when the above status is active. however LW4 resets then and I don't have to take any actions on the GLCD. 

________________________________________



Again trying to figure out exactly what is happening. 

+++++++++++++++++++++++++++++++++

What is exactly happening is whenever I power on or power off any electrical item in the workshop (or anything near by as per bench top trial)  the board drops out, halted or call it what you will. 

Simply put the board seems to "freeze" stops communicating with the laser if connected direct and not using the PC

Disconnects with the PC if connected to the PC.



One common fault seems to be that the "halt" message" is up on every cycle.



This happens even when the board is NOT connected to a laser with a separate independent 24v power supply.



I really don't see why you cant understand the problem..Ray. the board drops out of communicating with anything that is connected to it.



Until tonight's test I really thought it might be a problem with my system, computer or something we are missing. but I am not sure now it may be a fault in the card.




---
**E Caswell** *April 04, 2017 20:57*

**+Don Kleinschnitz** thanks for the reply Don,

________________________________________________

whoa,  are you saying that it is sitting alone on the bench connected to a battery and it dropped out when a hair dryer was turned on?

++++++++++++++++++++++++++++++++++++

Yes I am, I am as amazed as you are.

______________________________________________

It was connected to your laptop USB? 

+++++++++++++++++++++++++++++++++++++

NO, connected to a friends desktop PC with Lw4 and it gave a message of "machine disconnected"

_______________________________________________

When you say dropped out do you mean lost connection at the USB? Do you have a panel and does it still work after that happens and what does is do?

+++++++++++++++++++++++++++++++++++++

Yes, it looses connection to the USB and "machine disconnected"  The GLCD shows "halt"

Friends machine running on Lw 40.48.

_______________________________________________

This gets weirder with every post....

++++++++++++++++++++++++++++++++++++

Tell me about it....You want to try and explain to SWMBO when there is blue language coming from the corner of the workshop. (out of a blue box) On a day by day minute by minute.

But I know there are allot of people out there that give allot more and as my mind is always intrigued with problems I need to get to the bottom of it. 


---
**E Caswell** *April 04, 2017 21:00*

**+Andy Shilling** thanks for the reply Andy, looked back through my notes and yes we checked that out, pin out is correct as everything works when not interrupted with power switching. 

I have even had a raster job out of it today but that was with no one else but me in the shop. (I can control switching of everything)


---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2017 19:22*

**+PopsE Cas** please email info@cohesion3d.com with your name, order #, and a reference to this thread so that we can discuss how to proceed.  


---
**E Caswell** *April 05, 2017 19:50*

**+Ray Kholodovsky** don't that Ray.


---
**E Caswell** *April 07, 2017 17:17*

**+Ray Kholodovsky** we may have a solution. :-) happy bunny. Will test further over weekend but it's looking good. And the filter hasn't arrived yet. :-)


---
**Andy Shilling** *April 08, 2017 13:49*

Can you elaborate **+PopsE Cas**​ I've just tried to do a job and I'm now getting a halt at the same place everytime. I'm doing a 20 repeat cut and after 6 cuts it just stops even though it's the same file.




---
**E Caswell** *April 09, 2017 12:39*

**+Andy Shilling**  hi Andy. Sorry only just seen this as weekend not gone to plan. 

Looking at your problem I believe it's a different one than mine. As mine would run through all jobs and worked well until anything was switched On / off 

If that's what is happening then it may well be the same. 

I haven't been able to get done what I wanted so can't say that mine has been resolved just yet so will hold back until I get a good test and consistency in operations. 

Having said that Friday looked good 


---
**Don Kleinschnitz Jr.** *April 09, 2017 13:07*

**+PopsE Cas** did I miss it? What was the attempted solution, I'm dying to know?


---
**E Caswell** *April 12, 2017 14:44*

**+Ray Kholodovsky** **+Don Kleinschnitz** **+Andy Shilling** **+Kostas Filosofou**

Gents,  UPDATE,

I had the privilege of having a retired electronics engineer come and have a look at my drop out issues. 

It seems as though this is quite a common problem and went directly to the problem and resolved it by installing a temp 470µf  in the 5v supply just before the board as a temp fix but recommended that it would not cure all problems.

Advising me that my PC USB module needs to be upgraded to a 3.0 PCIe unit. See link.

I have installed the board and now have no drop outs when powering any equipment up.



He and the team identified a few years ago when he was working for a company designing motherboards.When the VCC dropped to 4.75v it gave a spike of frequency that interfered with the Data+ and causing it to drop out,. (he didn't give the frequency) It was particularly bad on USB that was fitted to the cases rather than direct off the boards.  However direct fitted USB ports could equally effected.



When I tested the C3D at my friends his PC was the same build as mine so the fault was still there. I have since tested on a new PC with windows 10 and no faults at all.



His comments were that with the design and build boards such as Arduino, smoothie and the likes are not fully USB compliant so you will experience the problems.



[amazon.co.uk - CSL - 4-Port USB 3.0 (super speed) card PCIe express controller card &#x7c; Interface card USB 3.0 &#x7c; New model / New driver &#x7c; USB hub internal: : Computers & Accessories](https://www.amazon.co.uk/gp/product/B00W4YFNAW/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)



Another link here explaining some of the faults.. bear in mind its links he has sent me and they don't really mean much to me.



[http://www.usb.org/developers/docs/whitepapers/power_delivery_motherboards.pdf](http://www.usb.org/developers/docs/whitepapers/power_delivery_motherboards.pdf)





USB compliance site  

[http://www.usb.org/about](http://www.usb.org/about)


---
**Don Kleinschnitz Jr.** *April 12, 2017 16:08*

Gosh I wonder who posted this ................



"......I think that it is already clear from this last test that the noise is conducted through the power supply whether its the separate supply or the laptop. 

If powering it from the laptop prevents the problem from happening then I would says its power supply it better filtered. 

In either case we need to figure out how to filter that board. 

That's why we need to understand what it looks like on the board."



" .... we want to look at the power supplies and see if we can see high frequency spikes when you switch stuff on."



"Then we would look at the internal power on the board for the same.



BTW do you have access to some capacitors like .01 and .1 uf?"



:):):)



I would still add the .01 and .1 uf :)




---
**E Caswell** *April 12, 2017 17:08*

**+Don Kleinschnitz** I know where you are coming from Don. But I still have the same power supply and no filters installed. What I understand is that it's the USB ports that are suffering with the high frequency spikes when the voltage drops  that is taking out the Data+ On them.



The info from him is that the boards could really do with having 4.7uf on them to help eliminate the problems. Just his opinion with experience with this problem. 



I gave him all the pointers that I had been given. He again said it's not the main power supply to the Laser or to the PC it's frequency interference on the USB on power drop. Wasn't going to argue with him as it's cured my problem 



When I tried a lap top it didn't cure the problem for me. Or running a job direct from the board. It still dropped out and still does by the way. 




---
**Andy Shilling** *April 12, 2017 17:36*

Not sure that's the problem I'm having, even running off the SD card mine would halt.


---
**E Caswell** *April 12, 2017 17:56*

**+Andy Shilling** my problem was echoed even by running direct of the board Andy. Still does as well. 


---
**Don Kleinschnitz Jr.** *April 12, 2017 20:11*

**+PopsE Cas** to be clear putting the 470uf cap on the 5V input of the C3D ... OR  .... replacing your USB board totally fixed your problem??



I was just tweaking the community in the post above :) as I suspected that the problem was related to a power supply drooping somewhere.



I totally agree with the notion that all these supplies need low and high frequency filters on the DC  inputs  :).


---
**E Caswell** *April 12, 2017 20:37*

**+Don Kleinschnitz**  

<s>-----------------------------------------------------------------------------------</s>

"to be clear putting the 470uf cap on the 5V input of the C3D ..."

<s>--------------------------------------------------------------------------------</s>

He installed the cap on the inlet to the C3D board, pic below. This resolved the drop out issues from the PC and enabled me to run a job. However we could NOT run from direct from the card. as that would still go in to "halt" mode when any shop switching was carried out.



 <s>-----------------------------------------------------------------------------------</s>

 "OR  .... replacing your USB board totally fixed your problem??"

<s>-------------------------------------------------------------------------------------</s>

Installed the new USB 3.0 board and removed the temp Cap and still working with only one drop out and that was when I switched off the Laser extractor. (It's not happened since, however  haven't been able to run it much due to other commitments.) Will test further over the coming weeks.



I have just spoken to him, The other point was that he also tested removing the VCC and Gnd  from the USB lead to the board and it had the same result, however he thought that they would be required so he re-connected them. 

That is the way he tested to see if the voltage drop was the problem. 

![images/27c00effa4bcfae35021de216d7c39a1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/27c00effa4bcfae35021de216d7c39a1.png)


---
**Don Kleinschnitz Jr.** *April 12, 2017 21:21*

**+PopsE Cas** Got it .... thanks


---
**Russ “Rsty3914” None** *April 14, 2017 01:48*

Line filter ?


---
**E Caswell** *April 14, 2017 10:57*

**+Russ None** 

NO, still don't have any inline filter installed and still working correctly. When the filter arrives I may well put it in and test it out but its working as it is so will leave well alone.  Having said that it hasn't had much use as need to get my head around other software.


---
**Kostas Filosofou** *April 20, 2017 15:08*

I following the post from the start as i was having same exactly issues with **+PopsE Cas** .. I had try to separate my C3D board with dedicated Power supply (24v) i had try EMI filter as **+Don Kleinschnitz** suggests with no luck ....But I try also to install a USB 3.0 PCIe card and after 3 days of testing I can say for sure that this was the problem!! Finally!! thank you all who contributed to find a solution...


---
**E Caswell** *April 23, 2017 11:40*

**+Ray Kholodovsky** **+Don Kleinschnitz** **+Kostas Filosofou**  With my curiosity being what it is I have test run the card today without the VCC and Gnd supply and only a data cable on the old USB ports. 

That worked and only dropped out when big band saw started, cold also live with that.**+Andy Shilling** knew you had some problems with drop out, might be worth a try. 

Another advantage is that the PC can be run up without the need for powering up main laser up first. Helps with GLCD power up. 




---
**Andy Shilling** *April 23, 2017 11:42*

**+PopsE Cas** So you just have the data wires connected then? I'll have to give this ago as it might enable me to use my windows machine rather than the mac. Cheers Pal.




---
**E Caswell** *April 23, 2017 14:44*

**+Andy Shilling**  I test run it today with just data connected, did the job as well but in fairness I didn't give it a long testing out but the results looked good.   I have the PCIe 3.0 card which works as well. connected back up to that currently.


---
**Andy Shilling** *April 23, 2017 14:56*

**+PopsE Cas**​ thanks, my issue is I only have laptops so the pcie card isn't an option.


---
**Don Kleinschnitz Jr.** *April 23, 2017 15:32*

**+PopsE Cas** have you tried disconnecting the USB power from the controller (there is a jumper on board). That will keep the PC from driving the board. 


---
**Andy Shilling** *April 23, 2017 15:34*

**+Don Kleinschnitz**​ is that jumper shown on the pin out diagram?


---
**Don Kleinschnitz Jr.** *April 23, 2017 15:37*

**+Andy Shilling** I believe that it's a cut away land. **+Ray Kholodovsky**​ told me. I'm not at home so can't look at schems. Check the board silkscreen.


---
**Andy Shilling** *April 23, 2017 15:40*

**+Don Kleinschnitz**​ thanks I'll have a look when I'm home.


---
**E Caswell** *April 23, 2017 16:18*

**+Don Kleinschnitz** would have been nice to have known this Don. **+Ray Kholodovsky** I sent him an e mail but hasn't replied or given any feedback (on here or via e mail) to what I have experienced. If I had known that it would have been one we could have tried. 


---
**Ray Kholodovsky (Cohesion3D)** *April 23, 2017 16:22*

Trace at the bottom under the USB port area labeled USB PWR, cut it right down the middle. This removes powering from them USB and depends on laser to be on to power the board. 


---
**E Caswell** *April 23, 2017 18:07*

**+Ray Kholodovsky** I gather that's irreversible then Ray?


---
**Ray Kholodovsky (Cohesion3D)** *April 23, 2017 18:09*

It is a solder jumper, so one could always solder the 2 pads together afterwards. 


---
**Don Kleinschnitz Jr.** *April 24, 2017 13:35*

**+PopsE Cas** sorry, this option did not dawn on me until I read about disconnecting power in the USB connector. 




---
**Andy Shilling** *April 24, 2017 13:46*

**+Don Kleinschnitz**​ better late than never 😉 


---
**E Caswell** *April 24, 2017 14:52*

**+Don Kleinschnitz** no worries Don, it was something that the electronics engineer looked at and said we could try but when he looked at the board thought it best to just put a Cap on the feed supply. If someone doesn't have that experience then I would recommend to try the data only cable first as it's often one that can be done so much cheaper and easily. Also doesn't effect the board. 


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/LPPoapfpFir) &mdash; content and formatting may not be reliable*
