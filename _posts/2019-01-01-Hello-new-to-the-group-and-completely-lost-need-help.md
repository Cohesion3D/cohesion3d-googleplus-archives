---
layout: post
title: "Hello, new to the group and completely lost, need help"
date: January 01, 2019 22:17
category: "K40 and other Lasers"
author: "Kyle"
---
Hello, new to the group and completely lost, need help.

I installed a C3D LaserBoard in my k40 in early december and I'm using the lightburn trial.  Everything worked great for a few weeks and then things changed.  The alignment of my cuts are now inconsistent, sometimes slightly off, other times way off from the design in the software. The second cut is almost always lower on the y axis, sometimes .5mm, sometimes as much as an inch.  Occasionally, it is off on the X axis as well. The two times I've experienced it being off on the X axis it was so off that it slammed into the stop switch and just continued to try and go until I manually stopped the operation.  In the video you can see a cut mark on the side clamping plate from this happening.

This week I am experiencing a new issue.  When I open lightburn and try to run my first cut, the motors make a different(louder) noise, then it moves about an inch on the y and x, then goes up on the y until it hits the stop and continues to try and go past the stop switch.  This only happens the first time, if I stop the cut, home it, and try it again, it does not do it again. 



Start first cut video: 
{% include youtubePlayer.html id="aUEySsSEVO0" %}
[https://www.youtube.com/watch?v=aUEySsSEVO0](https://www.youtube.com/watch?v=aUEySsSEVO0)

Using:

windows 10

smoothie

C3D power supply upgrade kit

origin lower left

home upper left



Thanks in advance for any help





![images/1c38955cd2becb58f72ae81527d027d9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1c38955cd2becb58f72ae81527d027d9.jpeg)
![images/e66c4c49c66dcd09d3d72fd4b198b2a8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e66c4c49c66dcd09d3d72fd4b198b2a8.jpeg)
![images/30936be9fbfc6cab6e2ea10b73567411.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/30936be9fbfc6cab6e2ea10b73567411.jpeg)

**"Kyle"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2019 20:09*

There is a pretty wide net of options being cast here.  Based on your video, it would seem that your limit switch might not be recognized. This has been known to happen on these machines.



[lasergods.com - Testing K40 Endstops With LightBurn](https://lasergods.com/testing-k40-endstops-with-lightburn/)


---
**Kyle** *January 02, 2019 20:59*

Thank you for responding!



When I enter M119 I get:

X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Y)P1.27:0 (Z)P1.28:1 (Z)P1.29:1



X and Y register correctly when I follow the steps




---
**Kyle** *January 02, 2019 22:58*

and I do not have a Z switch


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2019 01:37*

Now, are you able to move the head to actuate the switches? It is possible the drag chain or something else is getting in the way. Try M119 again by moving the head to the switches this time (I assume your pressed the switches with your fingers last time).


---
**Kyle** *January 03, 2019 01:43*

I did both and both ways showed X and Y register 


---
**Kyle** *January 12, 2019 22:01*

Soooooo, do you have any other suggestions?  My machine has not been working correctly for almost a month and has now been unusable for well over a week now.  It seems like the controller and lightburn are not working together.

I am still having issues similar as before but now different.  I create a simple square and sometimes it runs the cut where it should be and sometimes it does not and starts the cut in a different location than located in lightburn. Sometimes it will make the same louder noises in the stepper motor as the video, then start the cut of the square to the left of the location in lightburn.  Sometimes it slams into the Y limit and tries to run the cut off of the wood and onto the bed I made.  It's all over the place, seems to do whatever it wants.

I'm sure you are busy but the inconsistency of operation and damage it has done to the cutting bed I made is very frustrating.  I purchased this controller to upgrade my nano even though I had no issues with it.  Now my machine does not work.  Everything worked great for 2 weeks when I first installed it and then it didn't, and I changed nothing to cause it.  And now it is having different issues.


---
**Kyle** *January 12, 2019 22:01*


{% include youtubePlayer.html id="9_otmA3bomU" %}
[youtube.com - 20190112_144041.mp4](https://youtu.be/9_otmA3bomU)


---
**Kyle** *January 12, 2019 22:02*

these 2 cuts were run back to back of the same square shape from lightburn


{% include youtubePlayer.html id="YNHX-vAo4wI" %}
[youtube.com - 20190112_144156.mp4](https://youtu.be/YNHX-vAo4wI)


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 22:12*

Ok, it's not a homing issue.   Looks like its just skipping steps.  Possibly because of rapid moves (the travel in between ones where the laser isn't firing). Let's try to decrease those. 



On the board's config.txt file (power down, pull the memory card, put it into the computer):

line 4 is the seek rate.  Try bringing that down to a much lower value like 6000 



You should save, safely eject the drive, and put the card back into the board then power up. 



In a similar way, and systematically, one at a time, you can try lowering the acceleration value on line 19 to 1000


---
**Kyle** *January 12, 2019 22:28*

Thank You!  Will Try it now


---
**Kyle** *January 12, 2019 22:48*

I adjusted the seek rate to 6000.  When I first turn the machine on and run the cut it slams into the Y limit switch.  Stop the cut and run it again and it works perfectly.  Run it again after that and it works perfectly again.  If i turn the machine off, let it sit, then turn it back on it tries to slam into the Y again.  Stop the cut, run it again, and it works perfectly.  I lowered the acceleration value and it did the same thing, a little less violently though.


{% include youtubePlayer.html id="-9zl9GcvpN8" %}
[youtube.com - 20190112_153430.mp4](https://youtu.be/-9zl9GcvpN8)


---
**Kyle** *January 12, 2019 22:53*

Tried running the same cut several times, restarted the machine several times, and it is always going to the same location that is displayed in lightburn, so that's a plus


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 22:56*

Maybe go ahead and wipe the card and put clean firmware and config files from batch 3 in the dropbox (bottom of the install guide) to make sure nothing got screwed up with them.  No other files on the card.  Make sure the LEDs count up the first time you boot with the firmware.bin file


---
**Kyle** *January 12, 2019 23:31*

Sorry Ray, I'm not seeing the files at the bottom of the install guide I found.  Is this the guide you are talking about?



[cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D LaserBoard : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000800429-k40-upgrade-with-cohesion3d-laserboard)


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2019 23:34*

Oh, sorry, brain thought you had a Mini. 



Here you go: [dropbox.com - laserboard stock files smoothie batch 1](https://www.dropbox.com/sh/a7q9fzwc8x9xoje/AACXAvIufHBeUzVT3XSodbZRa?dl=0)


---
**Kyle** *January 12, 2019 23:47*

Replaced the config files and ran all the same tests, everything seems to be working now!!!  THANK YOU very much for the help!


---
*Imported from [Google+](https://plus.google.com/101610805183413833896/posts/Ay14rZdi7PY) &mdash; content and formatting may not be reliable*
