---
layout: post
title: "Cohesion3D ReMix getting ready to drive a monster of a CNC"
date: January 03, 2017 21:36
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Cohesion3D ReMix getting ready to drive a monster of a CNC.



<b>Originally shared by Øyvind Amundsen</b>



My DIY CNC milling machine project - stated installing the electronics. All fine so far :-)

Stepper and controller power OK

Next I'll wire the drivers and steppers for a test setup.

Figure how to connect the VFD using serial

![images/2e1c3f7085bfc41b9e2c2b43e5eff9f8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2e1c3f7085bfc41b9e2c2b43e5eff9f8.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jonathan Davis (Leo Lion)** *January 03, 2017 21:37*

Nice and hopefully it all works as expected it to.


---
**Øyvind Amundsen** *January 08, 2017 21:22*

Some progress - all motors running fine, tested using the LCD/control and from bCNC software on my computer. Next I have to study the rest of the firmware settings + try if I can control my Toshiba VFD using rs232



![images/ec84765e1a534d0d6b9cdec99f30d422.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec84765e1a534d0d6b9cdec99f30d422.jpeg)


---
**DDSC Ltd** *September 06, 2018 08:21*

did you manage to connect your VFD up?

I have same issue and don't know how to connect via MODBUS or PWM




---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/cZum4mRuvCg) &mdash; content and formatting may not be reliable*
