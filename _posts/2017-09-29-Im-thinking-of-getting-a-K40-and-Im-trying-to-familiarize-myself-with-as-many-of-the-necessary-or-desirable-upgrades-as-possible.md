---
layout: post
title: "I'm thinking of getting a K40 and I'm trying to familiarize myself with as many of the necessary or desirable upgrades as possible"
date: September 29, 2017 22:25
category: "K40 and other Lasers"
author: "Karl Lautman"
---
I'm thinking of getting a K40 and I'm trying to familiarize myself with as many of the necessary or desirable upgrades as possible.  Can someone explain, or point me to a discussion of, the principle benefits of the Cohesion3D mini vs. the stock controller?  I gather it cuts corners more accurately and cleanly, and accepts Gcode (both of which I appreciate are big deals), but what else?  Thanks.





**"Karl Lautman"**

---
---
**Jason Dorie** *September 29, 2017 23:26*

The Cohesion3D, when running Smoothie or GRBL firmware, is capable of controlling the laser power as the speed varies, meaning you get the consistent application of power across your job, and supports grayscale engraving.  The original controller is only capable of moving horizontally, vertically, or at a 45 degree angle - all other moves are emulated in the software as a combination of these, so the movement is a bit rough.  The Cohesion will do proper linear moves, arcs, and so on, so the movement is much smoother.



It also allows you to use a variety of different programs, like LaserWeb, Chilipepper, Vectric Laser, and so on to generate jobs, instead of being locked in to the crappy vendor software.  It is much more configurable than the original.


---
**Karl Lautman** *October 03, 2017 15:21*

Thanks, Jason.  I know Vectric makes CNC products (I use Cut 2D and 3D), but I didn't know they made a laser product, nor can I find any info on one.  Can you provide a link?


---
**Jason Dorie** *October 03, 2017 15:24*

It might've been a partnership thing with Darkly Labs: [darklylabs.zendesk.com - Outdated Cut2D-Laser downloads](https://darklylabs.zendesk.com/hc/en-us/articles/212898838-Outdated-Cut2D-Laser-downloads)


---
*Imported from [Google+](https://plus.google.com/112333897061732296673/posts/UvQgxe1B9CQ) &mdash; content and formatting may not be reliable*
