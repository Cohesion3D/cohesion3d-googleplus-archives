---
layout: post
title: "Cohesion3D Mini Laser Upgrade Sorry if this question has been answered before"
date: March 23, 2017 08:47
category: "C3D Mini Support"
author: "Kevin Hutchinson"
---
Cohesion3D Mini Laser Upgrade



Sorry if this question has been answered before. I'm looking to clone my K40 laser for a larger volume and was wondering if this board can also be used with the DRV8825 drivers in place of the A4988's?



Thanks!





**"Kevin Hutchinson"**

---
---
**Kostas Filosofou** *March 23, 2017 09:17*

**+Kevin Hutchinson**​ yes... I use with my C3D mini the DRV8825 drivers ..Works perfect!


---
**Kevin Hutchinson** *March 23, 2017 10:04*

Thank you Kostos ... I thought that was probably the case... Though since asking I think I will go with the external driver adapter for additional flexibility


---
**Kevin Hutchinson** *March 23, 2017 10:05*

Kostas! My apologies for spelling your name wrong


---
**Kostas Filosofou** *March 23, 2017 10:14*

**+Kevin Hutchinson** :) no worries..


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 14:18*

Yep! Can also use Trinamic or external drivers as you've discovered. Will want a fan to cool the DRV's, and we have some case designs for the Mini that will make that easy. Will dig up the link when you need it. 


---
**Kevin Hutchinson** *March 23, 2017 16:25*

Thanks Ray... That may be soon as you probably know I ordered the mini with external driver adapter s this morning 


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 16:27*

Yep. We'll get it shipped out before heading up to MRRF (3d printer fest) this weekend. 


---
**Kevin Hutchinson** *March 23, 2017 16:27*

Am going to check Thingiverse after lunch to see if anything pops up


---
**Kevin Hutchinson** *March 23, 2017 16:29*

Thank you... 


---
*Imported from [Google+](https://plus.google.com/106204478849964973474/posts/8VWqCkpxGKg) &mdash; content and formatting may not be reliable*
