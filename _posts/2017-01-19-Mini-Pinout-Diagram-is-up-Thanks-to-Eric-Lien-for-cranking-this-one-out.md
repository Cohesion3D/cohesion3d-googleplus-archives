---
layout: post
title: "Mini Pinout Diagram is up! Thanks to Eric Lien for cranking this one out"
date: January 19, 2017 04:28
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Mini Pinout Diagram is up!



Thanks to **+Eric Lien** for cranking this one out. 



It's at our support site here: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)









**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Eric Lien** *January 19, 2017 05:15*

Glad I could help. Now to take some time to actually learn how to use inkscape correctly... I was a man fumbling in the dark today helping. But I think a few days practice and I might actually know something :)


---
**Ned Hill** *January 19, 2017 05:26*

Thanks, I was going to ask about a pinout diagram.  Question, what does the kill button actually kill?  


---
**Marc Pentenrieder** *January 19, 2017 09:34*

**+Ray Kholodovsky**​ Wouldn't it be nice to know the signal level also (3,3V or 5V)?! 


---
**Carl Fisher** *January 19, 2017 15:10*

Looks great. Any chance on a breakout showing the ribbon cable pins?




---
**Eric Lien** *January 19, 2017 16:06*

**+Carl Fisher** If **+Ray Kholodovsky** gets me the info... I will get it added to the file.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 16:08*

**+Ned Hill** it's a "stop right away" emergency stop type of thing. 

**+Marc Pentenrieder** if it's a green signal pin it's most likely running at 3.3v. 

**+Carl Fisher** **+Eric Lien** I think this is crowded enough, let's do it as a separate drawing file. Per usual that means whipping up a CAD model of the adapter, etc. I'll add it to the list. 


---
**tyler hallberg** *January 21, 2017 21:29*

whats the pinout for the pwm output plug? All it says is center is 2.4. I want to make sure the wires are correct to the PSU correctly


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 21:35*

It's connected to the MOSFET 2.4 output and that is also level shifted to 5v via pull up resistor. It is the same signal as MOSFET4. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/RRozuLXZwPv) &mdash; content and formatting may not be reliable*
