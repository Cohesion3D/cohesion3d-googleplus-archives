---
layout: post
title: "Pre sales Q: Does a full spectrum Gen 5 Hobby laser (purchased in 2013) have a chance of being able to be upgraded to Cohesion3D Mini Laser Upgrade Bundle and use the Lightburn software"
date: October 02, 2018 16:46
category: "K40 and other Lasers"
author: "Em W"
---
Pre sales Q: Does a full spectrum Gen 5 Hobby laser (purchased in 2013) have a chance of being able to be upgraded to Cohesion3D Mini Laser Upgrade Bundle and use the Lightburn software. 



I see lots of talk about the K40 which seems to have a lot of parts in common with the FS gen 5 (for example they seem to use the same power supply, same 40w tubes and etc. I found the stepper motor specs (0.9 degree 42mm series , Nema 17 type) LDO-42st33-1334MB. Uses F10AL 25V fuse).



The machine I have already has a beam combiner, air assist and such. Basically just wanting to swap out the current controller card (also has a digital display that is connected to the controller card with a USB cable) and the software that it uses.



The current controller card has blocks for the X, Y, contoller, 24V on one side of the card, and at right angle on card has the USB female plug that goes to the digital display, and another Phoenix block with a wire from the PSU's AC block  "L" pin (that connects the negative electrode of the laser tube) on one pin and another wire that goes to the tube itself - last two pins of this four pin block on the controller card is blank.



I also have a rotary never used yet that I understand would use the Y block (rotary already has the Phoenix block wired ready to plug into the Y axis. 



Everything on the current controller card is wired to Phoenix contact pluggable terminal blocks. Same on the 40W power supply. Has a 40 CO tube. 



Currently, when it starts up it assigns itself an IP address and will communicate with the software via ethernet (or USB).



Right now I am sitting on a very expensive brick. Won't go into any troubleshooting issues -  just want to basically update to newer components and get away from proprietary stuff if possible.



Basically just want to know if anyone thinks I can go the Cohesion/Lightburn route before I start to spend the funds.



Please - some opinions and thoughts. Greatly appreciated.





**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 17:22*

Hi Em, thanks for posting to the group. 



I've been asked this before and one person recently sent me pics of the electronics inside the gen 5. Assuming yours are the same as what I saw:



Yep, the laser psu is the one we know from the K40.  It's wired differently though. Lots of wires I don't know about and no L connection.  On the K40's and overall for best resolution when engraving, we have a potentiometer hooked up to LPSU "IN" which is used to set the max current and then the C3D pulses L to get you power control/ grayscale within that range.

The homing switches are different. 

There's a lot of other stuff - lid switch, water flow sensor... The C3D doesn't control any of this - it should all be done in hardware to immediately kill the laser tube via the LPSU. 



2 steppers, 2 switches, and L for laser fire. That’s all the C3D does.  You'd be ditching a bunch of other stuff including network, screen... but you'd be able to use LightBurn software which is what everyone seems to want to use, and that's why we are here. 



Also, the C3D needs our separate power brick (the Power Supply Upgrade Kit) because the LPSU cannot provide enough 24v amps to power the board without browning out and causing issues. 



C3D can toggle an air assist (depends on how it's controlled though) and has native support for a Z table and Rotary (meaning you can control the rotary directly via A and not have to switch it back and forth with Y). 



Does this sound like something you can figure out? 

What's your timeframe on wanting to get all this done?










---
**Em W** *October 02, 2018 18:55*

**+Ray Kholodovsky** WOW, thank you SO much! I am going to be away from the office soon until the middle of the month. So will get down to business after that time. 



I am going to trace/diagram out where all the wires go on the blocks and should have a better handle on things like the limit switch/s, and so on. It did not come with a water flow sensor. I can live without the lid switch if necessary (yes I understand the risks:-) and no other users to worry about.



The main thing you talk about that is today confusing me most - is "we have a potentiometer hooked up to LPSU "IN"" (is this something I can purchase and use with your card or however it hooks up?- if I am understanding correct?) since for my laser,  power supply settings are currently controlled in their proprietary software. I don't see any power adjustments on the actual laser - at least not that I have found or used. 



Maybe there is something in the digital display functions for power settings, but I don't use that. The laser/software works independent from the digital display/functions that are hooked to the controller via the USB cable I talked about - but is not necessary to operate the laser. In other words, if I pulled out the digital display panel, the laser would still work (well if mine was working which is another story and why I am here), I just can't see the IP address and the status stuff that the display shows.



I do have an IP Q: with your controller: does the laser need an IP, and if so how do I determine what it is. Right now I know because the digital display tells me and I can just plug that into the software since the software needs that info to connect via the ethernet cable. I am not sure where it gets that IP info from, or if it needs it for a USB to computer hookup, since I only connected to the computer via USB cable once and can't remember now? 



What I have seen so far of the C3D documentation and pictures, and with help from the forum, I think I should be able to do this. I understand I may live without a few things, but if at the end of the day I have a more simple configuration and a laser that I can actually use (can't right now) I will consider this a success.



I will have a few more questions when I go to place the order. I have put a mock order in my cart, but have a few Qs about the add ons when ordering, and will follow-up on this upon return.



 But yes, your reply is giving me hope. I will have more specific things to share (pictures and wiring info) once I get that done and ready to order. Also, perhaps other Gen 5 users would like this information too.



PS, I watched many of the videos on the Lightburn YouTube channel last night and it really seems much better than the FS software. I was able to follow most of what was being talked about and actually excited to use that software. 



One last general Q: I want to buy some wire for this project since I know some things will need to be rewired or extended and not sure what to get. Looking at 20-22 AWG copper (though most seems to be tinned sranded) in multiple colors. Thinking general purpose, just to have on hand? This is not factoring AC power supply and/or HV wire - more just related to the connection block general wires or what may be needed for my purchase from you. The tube ground... should that be 18AWG since I want to replace that - the one right now has been spliced (that way upon purchase) between the controller block and the tube for whatever reason and is not long enough for a tube change - which I am also thinking about.



Really liked hearing about the rotary connection. Thanks again, Em




---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 23:03*

i'm gonna chime in if ya don't mind :) Here is an unofficial diagram of the gen5 hobby laser marked up with some c3d specific mods: [lasergods.com - FSL Gen.5 Hobby Laser UNOFFICIAL Schematic](https://www.lasergods.com/fsl-gen-5-hobby-laser-unofficial-schematic/)



Water flow (if desired) would go in series with the other interlocks and those interlocks would stay intact so no going without. Use P and G for the interlocks.



You can purchase a pot and install it on the 6 pin connector pins 1, 2, and 3 (see diagram). A current meter is a must and it goes in series with the tube ground (+ toward tube and - toward LPSU).



TCP/IP protocol on the C3D is not recommended so you will connect via USB.



I think 20AWG wire for 24v connections is okay and yes i would use 18 for the tube ground.



Always double check your connections as my info is for reference only.


---
**Em W** *October 03, 2018 02:22*

**+Tech Bravo** I am glad you chimed in. Since you have the Gen 5, I consider that this direction with c3d may work. I understand doing my own checks and thank you for the information. Between you and Ray responding, I can now start planning and make a decision.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 02:29*

**+Em W** we will help all we can. the documentation i found is supposedly for the gen5 and i do not actually own one,  just to clarify. provided that the diagram is mostly correct i dont see modding for a c3d to be too big of a problem and it would unlock a whole new world for you i believe. also, the 10K pot is arguable.  there seems to be a lack of consensus on the value but i'm sure we can narrow it down.  i look forward to your project :)




---
**Em W** *October 03, 2018 04:03*

**+Tech Bravo** Yes, project:-) Since I will be away from the office until the middle of the month, I will be able to tackle it then. That schematic is excellent BTW - pretty much like mine with some minor variation. Nice to know there are knowledge people willing to provide input. Thanks again!


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/fyRjfzzTCL2) &mdash; content and formatting may not be reliable*
