---
layout: post
title: "What is the appropriate way to connect an external 24V power supply to the C3D Mini?"
date: April 06, 2017 21:42
category: "C3D Mini Support"
author: "Peter Grace"
---
What is the appropriate way to connect an external 24V power supply to the C3D Mini?  Should one cut the K40 +24V wire and connect the external power supply via the "Main Power" header, or is it safe to leave the K40 plug as-is and add the external supply in "Main Power?"





**"Peter Grace"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 21:47*

I would actually disconnect the k40 power connector (the big white boxy connector) from the C3D Mini. 



Then I would connect the 24v and Gnd to the screw terminals on the left of the Mini, the Main Power ones. 



You can connect L from the original psu to the bed - 2.5 mosfet terminal on the bottom, 4th from the left 



It is <b>not</b> safe to leave both 24v sources in play. They do need to share a ground though. 



What kind of psu and wiring does your machine have? Pics would help. 


---
**Peter Grace** *April 06, 2017 22:39*

Do I need to do anything else asides from attach L to the 2.5 terminal?  Config change in the file?  I will provide pics in a few minutes when I have it wired to test.


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 22:44*

No, per those instructions there will be no changes to config. 


---
*Imported from [Google+](https://plus.google.com/+PeterGrace2013/posts/DG1fmBuuPnH) &mdash; content and formatting may not be reliable*
