---
layout: post
title: "I bought the mini bundle and installed it in my K40"
date: July 06, 2018 18:45
category: "K40 and other Lasers"
author: "John Cahall"
---
I bought the mini bundle and installed it in my K40. I cannot get it to download a driver and I cannot get it to connect to the laser. I went to smoothieware. Org and downloaded the driver they recommended. I'm stumped. Please help. 





**"John Cahall"**

---
---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 18:53*

What version of windows are you running?


---
**Joe Alexander** *July 06, 2018 18:57*

what operating system are you using? and did you verify that firmware.bin and config.txt are on the SD card?


---
**John Cahall** *July 06, 2018 20:30*

I just reformatted my sd card FAT32. Still won't connect. What's next?


---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 20:31*

After formatting did you load the [firmware.bin](http://firmware.bin) and [config.txt](http://config.txt) files back on it?


---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 20:39*

get the files from here and copy them to the microsd card: [dropbox.com - C3D Mini Laser Batch 3 - Provided Config](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AADLn0lRlfl7f3y-IrX5fVWIa/C3D%20Mini%20Laser%20Batch%203%20-%20Provided%20Config?dl=0)




---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 20:42*

then power up and try. after that please report back with the results. if you still have trouble we will need to ask a few questions to help so please answer the questions before doing anything else so we can try to assist.




---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 20:43*

okay i saw your post on a facebook group. i will jump over there




---
**John Cahall** *July 06, 2018 20:58*

I reloaded those files again. Shut it down and went to Lightburn. Window-Devices Cohesion3D (Smoothie) and Cohesion3D (GRBL) Tried them both and Lightburn still says disconnected.


---
**Tech Bravo (Tech BravoTN)** *July 06, 2018 21:00*

Ok i  moved to facebook on your newest post. This is best answered in the cohesion group (facebook one is more reactive) but i provided manual driver install instructions on the fb post in lightburn support


---
**Joe Alexander** *July 06, 2018 21:58*

if your using the cheap blue usb cable try replacing it with a quality one, preferable with ferrite on both ends. The cable has been a common issue.


---
*Imported from [Google+](https://plus.google.com/106691667969444964893/posts/AwRwX6MuVq7) &mdash; content and formatting may not be reliable*
