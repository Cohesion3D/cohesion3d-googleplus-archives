---
layout: post
title: "Thanks to Ray for getting me the .BIN and the Config file for 4 axis"
date: December 06, 2017 12:56
category: "FirmWare and Config."
author: "Colin Rowe"
---
Thanks to Ray for getting me the .BIN and the Config file  for 4 axis.

All loaded up ok.

Connected up A and Z axis (see photos)

Z is no problem and moves as it should.

A moves but does not show any movement on screen. I can jog via Laserweb4.

(please see video 
{% include youtubePlayer.html id="Zc0USKY-g7A" %}
[https://www.youtube.com/watch?v=Zc0USKY-g7A](https://www.youtube.com/watch?v=Zc0USKY-g7A))

There is also no A on the reprap screen . I mostly run from screen as a standalone.

when I post a program with A moves it looks ok on Laserweb4 but the program stops at the first A move,

What am I missing?

Help please.





![images/a78af2640016e21136e749d35a85db76.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a78af2640016e21136e749d35a85db76.jpeg)
![images/d978011c286657aa175818a1d8cfec71.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d978011c286657aa175818a1d8cfec71.jpeg)
![images/fa0d7b31364c374abd8e65da45294467.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fa0d7b31364c374abd8e65da45294467.jpeg)
![images/ed1915b1eb989cf2ef63fed2a82fdf61.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed1915b1eb989cf2ef63fed2a82fdf61.jpeg)
![images/2cc1fd6cfd967d962ed1a4fdede8d33a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2cc1fd6cfd967d962ed1a4fdede8d33a.jpeg)
![images/c19d69cc842a6a5c8a9d476a2496c595.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c19d69cc842a6a5c8a9d476a2496c595.jpeg)
![images/ffcc71922c0b86c0f49257f0ab8f50ca.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ffcc71922c0b86c0f49257f0ab8f50ca.jpeg)

**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2017 18:37*

Looks like the screen does not have any place to put A, this is probably how Smoothie is written and is not easily changed. 



If you can jog then I would think the board is working and responding to commands properly.



You could send a M114 command and see what the report back is, there should be XYZ but check if there is an A position returned as well. 



With that information, you can ask the LaserWeb devs about the lack of feedback in the program.



Then I would check the settings in LaserWeb (and also the steps per mm in the config.txt for A) and the gCode output, maybe some value is off and you are getting extremely tiny moves that you cannot see. 


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/guxC42RvqXq) &mdash; content and formatting may not be reliable*
