---
layout: post
title: "Today I installed the Cohesion 3D pcb in my K40 lasercutter"
date: April 21, 2017 13:38
category: "K40 and other Lasers"
author: "Andy Knuts"
---
Today I installed the Cohesion 3D pcb in my K40 lasercutter. Now I'm having horizontal banding issues while engraving. Anyone has an idea what could be the cause? 

Have a look at this picture. Just a test on cardboard. On the right it was engraved with the original K40 pcb + LaserDRW and on the left Cohesion 3D pcb + Laserweb 4.. As you can see on the left side, there's a banding issue that I can't seem to figure out...

![images/3211618df42899138821864d95556ff5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3211618df42899138821864d95556ff5.jpeg)



**"Andy Knuts"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 21, 2017 15:52*

The lines on the left are due to the spot size. Laser diameter in LW can be anywhere from 0.1 to 0.5mm, 0.2 is a good start and adjust from there. This is focusing or defocusing and has such an effect on rasters. 


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/Hr7nFKy4xcq) &mdash; content and formatting may not be reliable*
