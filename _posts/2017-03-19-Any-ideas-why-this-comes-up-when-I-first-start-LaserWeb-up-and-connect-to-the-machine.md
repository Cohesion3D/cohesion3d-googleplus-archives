---
layout: post
title: "Any ideas why this comes up when I first start LaserWeb up and connect to the machine?"
date: March 19, 2017 21:57
category: "K40 and other Lasers"
author: "Brad Reed"
---
Any ideas why this comes up when I first start LaserWeb up and connect to the machine?

![images/a145b0695425a86098c4e77a5896cd9b.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a145b0695425a86098c4e77a5896cd9b.png)



**"Brad Reed"**

---
---
**Ariel Yahni (UniKpty)** *March 19, 2017 23:00*

**+Brad Reed**​ this is on smoothie correct? 


---
**Brad Reed** *March 19, 2017 23:31*

**+Ariel Yahni** I believe so, it's what was pretty loaded onto the Cohesion3D controller


---
**Ariel Yahni (UniKpty)** *March 19, 2017 23:34*

**+Ray Kholodovsky**​ do you remember why this is? Maybe **+Claudio Prezzi**​?


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 00:14*

This is a laserweb + smoothie thing. When you hit connect LW sends a series of commands to check what the firmware is. Smoothie doesn't like this, and enters halt due to unrecognized gcode. Just hit green option 1 to continue. 


---
**Ariel Yahni (UniKpty)** *March 20, 2017 01:08*

Yes but there was a reason for this I just want to remember dose can provide the correct answer. I'm not getting this on my grbl


---
**Todd Fleming** *March 20, 2017 02:47*

I thought the smoothie issue was fixed at the expense of grbl users: [https://github.com/LaserWeb/lw.comm-server/issues/17](https://github.com/LaserWeb/lw.comm-server/issues/17)


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2017 03:34*

Todd a quick note that all the C3D Mini are running a smoothie cnc edge build from December-January timeframe. 


---
**Tony Sobczak** *March 20, 2017 04:28*

I am using LW4 and that's where it rears its head.  Doesn't happen on 3. Using C3D mini. 


---
**Claudio Prezzi** *March 20, 2017 07:45*

Peter is right, this dialog doesn't exist in LW4!


---
**Claudio Prezzi** *March 20, 2017 07:48*

Do you use the right url and port? Port is 8080 not 8000! And clear your browser cache.


---
**Claudio Prezzi** *March 20, 2017 08:06*

Ok, found something. It's possible to start the electron version but instead of using the exe's window, you can start a separate browser and open the old testenvironment (LW3) on localhost:8000. This is not for use yet and will be changed to a slim mobile/tablet remote app.


---
**Claudio Prezzi** *March 20, 2017 12:56*

Right, but I guess that's what **+Brad Reed** did to get this dialog ;)


---
**Claudio Prezzi** *March 20, 2017 17:24*

Oh yes, I didn't realize that it was not Brad who said he is using LW4.


---
**Brad Reed** *March 20, 2017 18:36*

Thanks for the help everyone! I'll just put up with it haha.



Not sure if anyone would be able to help with my latest post?


---
*Imported from [Google+](https://plus.google.com/105285651935261086449/posts/DjMxkcqxv3Q) &mdash; content and formatting may not be reliable*
