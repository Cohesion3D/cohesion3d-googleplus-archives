---
layout: post
title: "Anyone using smoothie ware based controller in a working CNC milling machine?"
date: February 01, 2017 16:44
category: "General Discussion"
author: "\u00d8yvind Amundsen"
---
Anyone using smoothie ware based controller in a working CNC milling machine? 





**"\u00d8yvind Amundsen"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 01, 2017 17:10*

**+Carl Fisher** is working towards it with a c3d mini. You may also want to ask in smoothieware g+ group. Any question I can answer?


---
**Carl Fisher** *February 02, 2017 02:46*

yeah so I've been dragging my feet a bit because frankly I'm tired of soldering for a bit and have other priorities. I'm converting from a TinyG to an older version of the C3D mini but at the same time converting to XLR style connections on everything and adding an a-axis rotary. I had to walk away from it for a bit but I'll get back to it soon I hope.



Planning to run it off LW4


---
*Imported from [Google+](https://plus.google.com/102309741047174439430/posts/97jkp5wVS3x) &mdash; content and formatting may not be reliable*
