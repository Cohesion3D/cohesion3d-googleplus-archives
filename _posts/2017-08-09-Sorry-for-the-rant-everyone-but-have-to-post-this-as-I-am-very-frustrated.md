---
layout: post
title: "Sorry for the rant everyone but have to post this as I am very frustrated!"
date: August 09, 2017 13:44
category: "C3D Mini Support"
author: "Kma Jack"
---
Sorry for the rant everyone but have to post this as I am very frustrated!



After days of frustration I have finally got the homing to work BUT not happy at all! 



The gantry homes at the top left as but is playing up very badly.

I loaded a photo in to try it out, it's about 150mmx100mm (I think). I create the gcode and press on "run job", it starts but where it does start is anyone's guess. First try, it moved about a third of the way down and engraved but can't finish because it runs out of material at the top and just goes forwards and backwards not able to move any higher. 



Second attempt, it started right at the botton on the laser bed and for some reason I can't begin to imagine, it's doing the print about one third of it's original size using the same gcode! There is just no repeatabilty with it, just starts anywhere it wants randomly and engraves at the size it wants and not the size it's supposed to! 



Now all of a sudden when I start LW4, I have a grid but soon as I connect to the machine the grid disappears and I have a blank workspace, can't even see the image I load but I can see the blue "cursor" (if that's what it's called) at the top left hand corner. I have tried scrolling to see if I can get the grid back but it's nowhere to be found! 



I'll play around with it for a couple of more hours and if it still plays up, it's gonna go flying against the wall! 



My son bought  this for me as a present and spent about 130 - 140 pounds (GBP) on it including postage and tax but it is worse than the stock cheap Chinese board that came with the laser. At least with the stock board worked first time and I can use the laser to do some work. 



So far, the Cohesion3D mini has been a massive disappointment and I really wished my son had listened to me and never bought the damn thing. It has caused me days and days of headache and still not got a single engraving out of it! 



For a board that is supposed to be far superior to the stock Chinese board and "drop in replacement" it has been nothing but a great disappointment, frustration and a massive headache! And from what I've read here in the forum and on FB page I am not the only one who has been suffering, there seems to be plenty of others just as frustrated as I am! 



Yes, the team here are very good and willing with helping (and very much appreciated) but how long am I (us/all sufferers) have to keep playing around and getting nowhere? 



Very, very disappointed! 





**"Kma Jack"**

---
---
**Ariel Yahni (UniKpty)** *August 09, 2017 14:29*

**+Kma Jack**​​​ you have all the right to this rant but as a user like you sometimes I need to accept when Im into something over my head.

-LaserWeb has at least a thousand active users if not more

-Cohesion has plenty of users ( don't know the number )

In the majority of cases, they are very happy



To overcome any issue you need to disect the situation and go step by step. But most important is that you need to understand how it works to find the solution. If you don't know, then you need to be extra specific on explaining what's happening so others can help.



This is an aftermarket product upgrade, being drop-in replacement does not mean you will not need to learn something new.



Once, and if you settle with the board you will need to learn how to do basic and complex stuff with LaserWeb , that also is time you need to dedicated to it as a new tool.



Please calm down, understand where you are and let others help you


---
**Kma Jack** *August 09, 2017 15:13*

I don't mind learning something new **+Ariel Yahni** I do that everyday with all my hobbies which I have many of, but I never expected so many problems with it. Soon as one problem is fixed the next one pops up. For a board that costs nearly half of what the whole machine does I expected it to have a lot less problems. If it was something cheaper I would have thrown it away and put it down to experience and never spent so much time on it. 



I think I have been specific on all my questions/posts and posted photoes where it was needed. I did get help and worked some of it out myself but be honest, how much learning/problems should an expensive board like this bring with it? 



I have followed instructions to the Tee, have read countless articles and watched videos, yours as well as from others, so how much more am I supposed to read/watch/ask? 



If I can learn to use a crappy cheap Moshi board and the software that came with it, I think I have the capability to learn this board and LW but there has to be an end to the problems or it's just not fun any more. 



I have been using computers since 1983, built my CNC about 6 years ago and it worked first time. I bought a 3D printer and learned how to use it pretty quickly and none of them has ever given me such grief! 



LW4 is set properly, the cables are connected to the board properly so what else is there to do to get it working and doing what it's supposed to do? It should work but it doesn't, it just does what it wants and not what I want!




---
**Ariel Yahni (UniKpty)** *August 09, 2017 15:33*

Let me tell you and don't take it the wrong way but neither of all the users that can help you are there with you and we/they are trusting you are providing all theres needed to troubleshoot this. 



Asuption is the mother of all fuckups.



Don't assume the other person understands.



Images, videos, step by step are very important to get a better understanding of what you have.



Again I mean no disrespect to you or anyone else but experience varies with each situation as each one is different.



We have all seen users in your current situation. Most are enjoying their newly acquired workflow . 



You have 2 options: either abandon this upgrade and continue using the stock or take a break, clams down and refocus. 



That's not to say that there could be a legitimate situation with your board or LaserWeb that requires some in depth look, that could be an unseen bug or board problem but that will not get addresses if the guys that can troubleshoot it don't get all the info required. 



At this point it does not look like that's the issue


---
**Kma Jack** *August 09, 2017 15:55*

**+Ariel Yahni**, all the questions I asked were answered meaning that they were understood and if there was one that wasn't I was asked to clarify which I did. 



I do not have any problems with the help I got here, all have been excellent with replying. My problem is that after solving so many issues/problems, the damn thing is still not working as it should!



There are situation where no photo can be taken to show what's wrong. ie: homing. When I click on the homing button, I don't have a camera in my hand ready to shoot, I expect it to go to ty he same place every time but it doesn't. Most of the time it goes to the to left but then sometimes it goes to the bottom left and just tried to go further than it can but can't because it reached the end. If I knew when it's gonna do that I could have a camera ready and video it but I can stand there with a camera ready every time I click on t he homing button. 



Where I could, I took photoes and posted them or even videos but I knew that it would happen so was ready for it.  



My last problem was stuttering, I took a video of it and posted it here, Ray replied saying that it's a communication problem, how the board and LW4 communicate with each other so not user fault but actually something with the software/board. 



I am going to try what he suggested, not really happy taking the long route but will try in the hope that it'll work and I can start using the board. If that doesn't work properly either then I'm done with the board for a while. I might go back to it one day but maybe in a few weeks time when I've forgotten the frustration!


---
**chris B** *August 09, 2017 18:08*

**+Kma Jack** from what you're describing where it's hitting the edges of the print area it sounds like it's not homing correctly. Check that your homing in laserweb is specifically G28.2 and not just G28 which is how 3d printers home. For a laser cutter just using G28 tells the printer to go to where it thinks home is, G28.2 makes it actually go and hit the end stops to know where home is. To save myself the headache of always doing this you can create a on_boot.gcode file with just the G28.2 command in it and it will cause the machine to home automatically every time it starts up.  


---
**Kma Jack** *August 09, 2017 18:56*

**+chris B**, thanks for the reply. I am trying out grbl to see it it's better but till try your suggestion re the on_boot_gcode file. 



I did have G28.2 in the setup and it worked ok but then sometimes it played up when I clicked on the "home all" button. 


---
**Tammy Mink** *August 10, 2017 12:13*

I understand your frustration, like you I have had troubles every step of the way (granted some were unrelated to the board itself, but I have those have been resolved). I am starting to wonder if ANY configuration will yield a reasonably usable main board for the laser,  each solution seems to have critical issues.


---
**Don Kleinschnitz Jr.** *August 10, 2017 14:44*

I think I would start with some simple positioning tests.

Create simple Gcode moves load them into LW and test if it moves correctly. 

Let us know if or if not that works. 


---
**Kma Jack** *August 10, 2017 19:28*

**+Don Kleinschnitz**, I am doing that at the moment but without creating a gcode moves. I am instead moving a small design around the UI of the LW and running the test over and over again in different places. It seems to be behaving itself since I followed **+Ray Kholodovsky**'s suggestion and changed over to the grbl firmware. I won't know how reliable it is till I have used it for a few days but I am not gonna try doing any proper jobs with it yet till I am sure




---
**Kma Jack** *August 10, 2017 19:32*

**+Tammy Mink**, there are people who swear the board and LW work for them but unfortunately there are many who have loads of problems. I think my problems are solved (hopefully) but not gonna pop the bubbly yet, not until I am 100% sure that it will do the jobs the board was designed and sold for. 


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/ixzQ186AWYU) &mdash; content and formatting may not be reliable*
