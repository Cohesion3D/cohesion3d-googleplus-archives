---
layout: post
title: "Wink wink. Originally shared by Cohesion3D Can you believe that Cohesion3D is coming up on our 2 year anniversary?"
date: November 21, 2018 21:26
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Wink wink. 



<b>Originally shared by Cohesion3D</b>



Can you believe that Cohesion3D is coming up on our 2 year anniversary? We've got something special to announce very soon. 

![images/29c18e20ab9e43645fb6486f4ad6e5d5.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/29c18e20ab9e43645fb6486f4ad6e5d5.png)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**James Rivera** *November 22, 2018 05:58*

Smoothieboard v2? 😃


---
**Marc Pentenrieder** *November 22, 2018 14:33*

Cohesion mini2 based on smoothie2? Will there be a remix2 ?


---
**ThantiK** *November 23, 2018 01:19*

It's definitely a cohesion board; has the K40 ribbon on the left, and some other standard connectors I recognize.  Barrel plug on the right suggests wider voltage input options.  Is that a full-size SD on the bottom?  They <i>are</i> more rugged and more widely used in laptops so I could understand that move.  What I want to see is what drivers are integrated here...



Looks like almost dead center is an optional additional pololu-style driver breakout (X, Y, Z, E, optional A/E2?) - though it could be generic expansion.  X, Y, Z min and max stops at the bottom.



It actually looks as if some handy headers are provided on two of the motor outputs at top right as well.  Step/Dir/Enable maybe?


---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2018 01:53*

Eh, you're no fun. [http://cohesion3d.com/cohesion3d-laserboard/](http://cohesion3d.com/cohesion3d-laserboard/)


---
**ThantiK** *November 23, 2018 05:29*

Ah, ethernet/lcd expansion!  Step/Dir/Enable expansion! (lol!)



And I had guessed Trinamic drivers but didn't want to say anything incase they weren't...haha!


---
**Marc Pentenrieder** *November 23, 2018 17:53*

Nice, keep up the good work 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/AjEHyC8gTSU) &mdash; content and formatting may not be reliable*
