---
layout: post
title: "I keep getting a message saying that my board has been disconnected in the middle of raster jobs"
date: July 03, 2017 04:01
category: "C3D Mini Support"
author: "laurence champagne"
---
I keep getting a message saying that my board has been disconnected in the middle of raster jobs. Anyone know what is going on?

![images/4890f72d3537fcc2e96456fe6133004d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4890f72d3537fcc2e96456fe6133004d.png)



**"laurence champagne"**

---
---
**Joe Alexander** *July 03, 2017 04:03*

are you running it over a usb cable? i swapped my cable with a proper shielded one and added ferrite to both ends and havent had this issue since. 


---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 04:08*

Seconded, if this is the USB cable that came with the k40, well, it causes this to happen a lot. 

What Joe said ^ 


---
**laurence champagne** *July 03, 2017 04:10*

**+Joe Alexander** 

I've ran somewhere near a thousand jobs with the same cable on the chinese nano board and never had a problem.



I'm beginning to think I'm in over my head with the cohesion board and esp. laserweb.


---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 04:26*

The Cohesion is a higher speed board than the M2Nano, and we have seen the Chinese USB cables have caused issues, that <b>literally any other USB cable that isn't a blue Chinese one</b> should resolve. 


---
**laurence champagne** *July 05, 2017 00:58*

**+Ray Kholodovsky** 

Thank you so much, it was in fact the cheap chinese cable that was causing the problem. I switched and it's working great.


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/LtiTdgbc8Nd) &mdash; content and formatting may not be reliable*
