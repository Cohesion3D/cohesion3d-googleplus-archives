---
layout: post
title: "Is there any way to reverse the direction of the rotary encoder in the GLCD - I would like a clockwise turn to move the cursor down instead of the default ?"
date: May 25, 2018 13:00
category: "C3D Mini Support"
author: "LA12 Sports Images LA12 Sports Images"
---
Is there any way to reverse the direction of the rotary encoder in the GLCD - I would like a clockwise turn to move the cursor down instead of the default ?



Thanks





**"LA12 Sports Images LA12 Sports Images"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 13:44*

Yes. In the config file under panel swap the encoder A and B pin #’s with each other. 


---
**Don Kleinschnitz Jr.** *May 25, 2018 14:04*

Oh my this always annoyed me...

#GLCDEncoder


---
**LA12 Sports Images LA12 Sports Images** *May 25, 2018 19:50*

Brilliant - thankyou.


---
**Joe Alexander** *May 25, 2018 23:43*

how about making it less sensitive? one click-turn is like 3 menu items so it gets tricky sometimes :P


---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2018 00:37*

That's called panel encoder resolution, it's not in the default smoothie config file, but you can add that line in.  The value you want is 4, instead of the stock value 2. 


---
*Imported from [Google+](https://plus.google.com/102441991153902344952/posts/VAPatipheFk) &mdash; content and formatting may not be reliable*
