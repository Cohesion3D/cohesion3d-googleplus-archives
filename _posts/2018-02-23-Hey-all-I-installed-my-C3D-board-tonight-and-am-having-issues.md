---
layout: post
title: "Hey all, I installed my C3D board tonight and am having issues"
date: February 23, 2018 10:56
category: "C3D Mini Support"
author: "Lochlin Black"
---
Hey all,



I installed my C3D board tonight and am having issues. 



Operating System: Windows 10 Pro

K40 with standard ribbon and 4 pin

I have tried a different USB



When I boot the board with an SD Card in the slot all lights flash and then just the red and 3v3 stay illuminated. No device is found on PC. L1, L2, L3 and L4 never light back up.



When I boot without an SD Card all lights flash then L1 lights solid followed by L2 and L3 blinking. L4 doesn't light up again. A drive does show in the PC but it isn't accessible.



I am suspecting a faulty SD card but I don't have a card reader so can't flash the card, I will buy one tomorrow if I need to. 



I took a video of both sequences but I don't think it adds any more value than above.



Can anyone shine some light please?



Thanks,

Lochlin





**"Lochlin Black"**

---
---
**Richard Vowles** *February 23, 2018 17:40*

It does very much sound like the firmware, ray tests the boards thoroughly.


---
**Lochlin Black** *February 24, 2018 00:01*

Ok so it was a faulty SD card. Wouldn't read in a card reader either. Have temporarily put my gopro card in with firmware and it is working now. 


---
*Imported from [Google+](https://plus.google.com/110174902314522901329/posts/XjPRukjYpcZ) &mdash; content and formatting may not be reliable*
