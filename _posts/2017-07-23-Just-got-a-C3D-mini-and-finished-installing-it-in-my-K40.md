---
layout: post
title: "Just got a C3D mini and finished installing it in my K40"
date: July 23, 2017 03:11
category: "C3D Mini Support"
author: "Tom Arnold"
---
Just got a C3D mini and finished installing it in my K40.  A quick test and it seems to behave and speaks to Laserweb and I used Printrun to test the steppers.



The issue I'm seeing is garbage on the LCD.  I read through the older posts and I see this has come up before.  It doesn't matter if I power the K40 first then plug in USB or not, it still comes up with two bars of garbage.



Are there some known troubleshooting steps for this or do I just try running the C3D from a separate 24v supply ( planned anyway so I can install my Z table ) ?

![images/030a43b6407282fd5f86fb4d4c15d147.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/030a43b6407282fd5f86fb4d4c15d147.jpeg)



**"Tom Arnold"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2017 04:59*

Hi Tom! 



Can you describe if this always happens, or if there are cases where the LCD does boot and you see text? 



Is there a constant beep, does the beep come off, or something else? 



A quick run off my 1 am debugging list/ thought process:  :)



You got the screen from us so it was tested as working but on a different board before shipment. 



Have not yet had a physical Mini-side problem sending the signals to the GLCD so this is less likely. 



Corrupted SD Card is a possible problem, this is why I asked about the beeping.  I could also have you send M119 and M114 commands via pronterface to see what kind of response string you get back.  



At some point in time I am going to ask for pictures of your wiring, what computer you are running, whether the disk drive shows up in computer when you plug in USB, screenshots of Pronterface after querying those gCode commands, etc. May as well pre-empt that. 



In short, I need to make sure your setup is actual working before advising you swap over to the new psu. I think it's a great idea, but you should have everything working on stock machine first. 




---
**Claudio Prezzi** *July 23, 2017 08:03*

It could be that just the wrong Display type is configured in configured.txt.


---
**Cris Hawkins** *July 23, 2017 14:09*

Or the ribbon cables are swapped where they plug into the C3D....


---
**Tom Arnold** *July 23, 2017 15:45*

Yeah, it was pretty late last night when I posted.  I should have provided more details.  My bad.



Display beeps for about a 3 count.  If I watch the c3d as its beeping, the beeping stops when the green LED next to the pushbutton lights up and at that time the other status lights go to on-blink-blink-on-red.



The display never shows text.  Not even a hint.



Testing on a Mac.  Disk shows up with config.txt and FIRMWARE.CUR.



73308cca0476347f80e38150abfecbc5 FIRMWARE.CUR



Confirmed config.txt has the Panel enabled, and the settings look correct.



>>> M119

SENDING:M119

X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1



>>> M114

SENDING:M114

ok C: X:0.0000 Y:0.0000 Z:0.0000



The Ribbon cables aren't swapped from the docs.  If I do swap them, display doesn't do anything ( no beep )



Lovely cellphone photos attached.

![images/dc377d27c0d97c8af8837c4ac74fc12d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dc377d27c0d97c8af8837c4ac74fc12d.jpeg)


---
**Tom Arnold** *July 23, 2017 15:45*

![images/54027b18b7da6122b0c6e6a8f2387709.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/54027b18b7da6122b0c6e6a8f2387709.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2017 16:27*

Can you:

Pull the microsd card out, put it in computer, format fat32, and then place the 2 files firmware.bin and config.txt onto it

Put back into the board, power up using only the laser power (USB not plugged in). 



I don't know if/ how Mac does formatting weird so if you have another card you can use for this/ a PC, might be worth it.  



This is just a quick thing much like "have you tried turning it off and on again?" before we delve deeper. 



GLCD is enabled in config, no need to change anything there. 


---
**Tom Arnold** *July 23, 2017 16:31*

Is there a size limitation on what cards can be used?  I have some 16gb cards.  Can easily format on a PC ( once I cable it back up that is... ) and try this.


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2017 16:33*

No problem, I know people using 32gb as well.   Please format and copy the files on the PC. 


---
**Tom Arnold** *July 23, 2017 17:15*

Okay.  Display is working now with new SD card formatted FAT32 with the v2.3 files from the getting started doc.  Its on a 16gb card of sketchy pedigree but it works ( CENTON ) and boots up both via K40 power and just USB power.



I got curious so I looked at the original card.



First test : just replace the files on the card with the v2.3 ones that worked above.  Power from K40.  Result : Long beep. lights go to good pattern ( on-flash-flash-on-red ).  Garbage on display.



Second test : reformatted card FAT32 cleanly. ( originally formatted FAT ).  Copied files on.  Result same as above.



Third test : reformatted as above.  Power from USB.  Result same as above.



In all 3 cases it seems to boot up ( firmware.bin gets renamed to firmware.cur and lights go to good pattern ) but the display displays garbage.



C3D just does not like this 2gb card it came with for some reason.



I'm going to start on hardware mods right now, get the new 24v power supply and the stepper driver physically mounted as well as mount an power inlet filter with switch so I can turn on the 24v power supply and then use the front rocker to turn on the laser power supply separately so I can test things without having to cycle a Lowest Bidder supply all the time.



Can you confirm that the C3D doesn't use 5v power from the K40 supply so if I run 24v direct I can just remove the K40 DC power cable entirely?  I'm also planning to cut the USB power trace so I can turn this thing off and not have to remove the USB cable.



Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2017 17:31*

A possibility, and I'm not deferring blame here, is the Mac connection.  Mac likes to index the drive and this has been known a) to cause slower job streaming speeds and b) other board freezes.  Thus it is important to dismount the drive. 



This would be the most "niche"/ localized card issue I've seen yet. 



The ones we've diagnosed so far include:

Card completely dead and causing short on board's 3.3v rail. 

Card not seen on pc, a format fixed the issue.

Can see files on card and get motion but some issue like GLCD not showing text.  M114 was reporting back but M119 was not.  Format fixed issue and GLCD worked after that. 



(btw I was manually flashing all cards myself so it definitely worked before shipping but something happened on the other end that I'm having great difficulty putting a finger on)



So M119 reporting back and the LEDs doing their thing and the buzzer not being on constantly suggests that C3D/ Smoothie was indeed reading the config file on the card. But some obscure GLCD line wasn't getting read properly?  <b>Can you imagine how deep of a microscopic silicon issue this would have to be?</b>  I find this funny and intriguing and a bit sad all at the same time. 





re 5v: That's correct. You want to do this: Give the Mini 24v and Gnd to the Main power in screw terminal at the top right. It will regulate its own 5v, do not try to feed it 5v.  You can run the L wire to the 4th screw terminal "2.5 -" of the bottom row. 



Your laser psu and new psu need to share a common ground.  That's all. 



Feel free to attach a pic for confirmation before powering up. 



Be careful not to cut any traces when doing the usb jumper cut, it's kinda packed above the jumper. .  



Cheers!


---
**Tom Arnold** *July 23, 2017 17:59*

For future people reading this thread that use a Mac, you can disable spotlight indexing of externally attached disks by adding /Volumes to the Privacy section of Spotlight in Preferences.  As far as other cruft that OSX likes to drop around, look at Tinkerbox or BlueHarvest to keep it clean(er).



I'm wondering if for some reason the 2gb SD card is power hungry maybe?   Shot in the dark on that one.  I'll play with it again once I have made the power changes just to see if there is an improvement/change.



Thanks for the reminder on ground.  That would have been a fun problem to diagnose...


---
**Don Sommer** *July 25, 2017 01:29*

These sound like the  problems i have been having i can recopy firmware and control over the original and it will work untill next boot or two then its corrupt it wont boot.   tried  2 different brand16g  ones with new files copied to them same screen as above and buzzer going off any ideas  windows 10 x64


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 01:32*

Don, have you upgraded to a separate power supply yet? 


---
**Tom Arnold** *July 29, 2017 19:34*

Weird.  I thought I replied to this thread with a question and I don't see it.  Let me try again.



Why are we running to the L wire on the supply instead of to the actual ground pin?


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2017 19:38*

What about the L wire? 

That's how we control firing the laser from the board. 


---
**Tom Arnold** *July 29, 2017 21:05*

<b>facepalm</b>  Of course.  Easy enough to pop the +5 and +24 pins out of the stock connectors and just re-use that cable for the ground and laser connection.



So, I've installed a 24v Delta power supply.  It runs to my two external stepper controllers and to the C3D.  I have a solid 24.3v at the C3D.



With either SD card I get identical results now.  On powerup, display beeps and I get two garbage bars on LCD ( like shown in original photo ).  Power cycle or press the reset button usually results in a proper display.



At least it is predictable and can live with that.  I don't have to power cycle the original Laser supply since that has an additional power switch


---
*Imported from [Google+](https://plus.google.com/112624015909891269236/posts/FszUc4sZtYW) &mdash; content and formatting may not be reliable*
