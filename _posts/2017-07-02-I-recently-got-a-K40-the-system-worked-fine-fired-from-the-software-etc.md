---
layout: post
title: "I recently got a K40, the system worked fine fired from the software etc"
date: July 02, 2017 18:50
category: "C3D Mini Support"
author: "Ryan Palmertree"
---
I recently got a K40, the system worked fine fired from the software etc.



When I installed my Cohesion3D I am unable to fire from the menu, nor am I able to fire from LaserWeb even after fixing the settings.  I can control the motors, and it is reporting correctly on the GLCD, just wont test fire from anything but the original button on the machine.



I have seen several comments around needing to re-majig a wire from the power connector to a different location to get the system to fire, is this still the case?



If it isn't where should I start troubleshooting?



I also noticed that my natural home on the machine is the upper left, the Cohesion board wants to take it back to bottom left.  Is there any way to flip this in the firmware?



Thanks!





**"Ryan Palmertree"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 18:55*

This stuff should go in the FAQ (note to self).  Here goes:



You don't need to change any wires.  That was something that was limited to the first batch from back around the New Year.   



All machines have home switches at the back left. If you are homing (G28.2) to the front, then you need to change the orientation of the Y motor plug.  With all power off, rotate it 180 degrees. 



How are you trying to fire?  Attach pictures of the board wiring, etc... 


---
**Ryan Palmertree** *July 02, 2017 19:15*

I'm attempting firing via the board itself, navigating through with the GLCD.



Here is the board

![images/395c653c83abbff158d5dc6b3e899ba7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/395c653c83abbff158d5dc6b3e899ba7.jpeg)


---
**Ryan Palmertree** *July 02, 2017 19:15*

Here is the power supply. 



I followed L to the correct location so the board /should/ be able to control the laser (I think).

![images/f8bef8d1f35ca4a9d678c828e34029f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8bef8d1f35ca4a9d678c828e34029f4.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 19:16*

What is the k40 pot set at? Can you fire if you use the physical test fire button on the k40 panel? 


---
**Ryan Palmertree** *July 02, 2017 19:17*

Here is my actual endstop switch on the machine, I'm thinking I'll need to re-wire two micro switches.  Unless I'm misunderstanding how to move this endstop to work.





![images/8707d31deca02e36be973d9578fd9f53.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8707d31deca02e36be973d9578fd9f53.jpeg)


---
**Ryan Palmertree** *July 02, 2017 19:17*

Thank you for the quick reply!




---
**Ryan Palmertree** *July 02, 2017 19:23*

It's set to ~6ma, the machine gets weird below that.  I am able to fire via the machine itself.


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 19:46*

Try setting it to 10mA.



Also, get LaserWeb setup, connect over USB, be able to jog, and then issue a command G1 X10 S1 F600 from the terminal at the bottom right. 


---
**Ryan Palmertree** *July 02, 2017 21:08*

Money!  That got it, thanks for the assist!  Should be able to get it working in LaserWeb now.


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2017 21:10*

Yeah, so here's the deal. The board's default config caps power to 80%  

You can find the line in the config.txt file for min power 0.8 and change it to 1 if you want. I set it that way so that people wouldn't run their tube at full power and burn it out quicker. In short, don't max out the pot. 


---
*Imported from [Google+](https://plus.google.com/108523384656507675919/posts/HXJdW5KLsYs) &mdash; content and formatting may not be reliable*
