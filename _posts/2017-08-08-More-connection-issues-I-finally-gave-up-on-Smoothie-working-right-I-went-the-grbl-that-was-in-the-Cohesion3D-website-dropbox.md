---
layout: post
title: "More connection issues. I finally gave up on Smoothie working right, I went the grbl that was in the Cohesion3D website dropbox"
date: August 08, 2017 22:40
category: "C3D Mini Support"
author: "Tammy Mink"
---
More connection issues. I finally gave up on Smoothie working right, I went the grbl that was in the Cohesion3D website dropbox. I deleted the firmware files, I left the config file on there and copied the grbl firmware over. I rebooted, and seems it installed because I can't see the minisd card in windows anymore. Now I can't seem to get LaserWeb to see the board at all.   The lights on the motherboard are different, the top is green solid and the bottom is solid red (top being the direction of the USB port).





**"Tammy Mink"**

---
---
**Todd Fleming** *August 08, 2017 22:41*

Which Windows version?


---
**Tammy Mink** *August 08, 2017 22:59*

Windows 7 32bit. I got the lights on the motherboard back by reinstalling the default config and firmware...so that's something. Maybe I'll try again and see what happens




---
**Todd Fleming** *August 08, 2017 23:01*

You need to manually install the driver on 7 using [https://github.com/gnea/grbl-LPC/blob/master/VCOM_lib/usbser.inf](https://github.com/gnea/grbl-LPC/blob/master/VCOM_lib/usbser.inf)


---
**Tammy Mink** *August 08, 2017 23:17*

Since I had to go back to original I'm trying the newer smoothieware again.. but I may very well have to go to Grbl anyways. Thanks for the driver link




---
**Tammy Mink** *August 08, 2017 23:53*

I got it to connect but LaserWeb won't turn the laser on. What command do I put in the settings for tool aka laser on and off?


---
**Todd Fleming** *August 09, 2017 00:35*

Check the grbl config. Make sure $32=1 and $10=0. In gcode settings, add M4 to Gcode Start, M5 to Gcode end, remove M2 from Gcode end, and make sure Tool On and Tool Off are empty. Change PWM max S value to 1000.


---
**Tammy Mink** *August 10, 2017 12:09*

Thanks. I may come back to this, I once again but I guess I have some renewed faint home I might get the updated smoothie working. ...I am really starting to wonder if any configuration will actually will work right anymore.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/Q6m1BSzwPFv) &mdash; content and formatting may not be reliable*
