---
layout: post
title: "Hi Ray Kholodovsky , I rewired my bltouch signalpin from pin 2.4 to pin 1.20 and everything works as expected if motors are disabled (M18)"
date: January 11, 2018 16:16
category: "C3D Remix Support"
author: "Marc Pentenrieder"
---
Hi **+Ray Kholodovsky**,



I rewired my bltouch signalpin from pin 2.4 to pin 1.20 and everything works as expected if motors are disabled (M18).

But as soon as i turn the motors on (M17) nothing happens, when I send M280 S3.0 (push pin down) on the bltouch until I send (M18), then the pin deploys.



It looks to me as if the bltouch does not get signal when motors enabled.



Here is al Link to my config.txt:









**"Marc Pentenrieder"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2018 12:30*

Can you try a different pin also? 


---
**Marc Pentenrieder** *January 13, 2018 14:08*

**+Ray Kholodovsky** I will go with an rc servo and endstop now to get finished with the machine. This combination works even when motors enabled.

I will give the bltouch another try some time in the future. I want to see the machine running now;-)


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/BB3m5CvyaBG) &mdash; content and formatting may not be reliable*
