---
layout: post
title: "Just took delivery of my K40. Cohesion3D board is on the way"
date: October 13, 2018 01:31
category: "K40 and other Lasers"
author: "Kieron Nolan"
---
Just took delivery of my K40. Cohesion3D board is on the way. Anyone have this type of set up? Looking for advice on what to connect /disconnect.



It has two power sockets mounted on the back, presumably for the pump and fan. Once I determine where in the loom they're connected I'll know if they're safe to use or not. 



![images/fc251cdb70fb597bcf7052e61d73b459.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fc251cdb70fb597bcf7052e61d73b459.jpeg)
![images/fc56a32e0cb64fb151c10ca73e064227.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fc56a32e0cb64fb151c10ca73e064227.jpeg)
![images/f6f07e2538cc5cd8a0f465aaddffd277.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f6f07e2538cc5cd8a0f465aaddffd277.jpeg)
![images/ecb02bc3abb02032a3b5dd3b378b77b8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ecb02bc3abb02032a3b5dd3b378b77b8.jpeg)
![images/a549b6996b96defac3e2c542eae8d59f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a549b6996b96defac3e2c542eae8d59f.jpeg)

**"Kieron Nolan"**

---
---
**Tech Bravo (Tech BravoTN)** *October 13, 2018 01:36*

you have a standard M2 Nano controller so the install could not be more "textbook". You should not have any issues. Just do one step at a time, take good pics of the controller before ( you already have a pretty good one here) to identify the wiring.  may want to read through so you know what to expect: [http://cohesion3d.com/start](http://cohesion3d.com/start)




---
**Kieron Nolan** *October 13, 2018 01:38*

Fantastic, can't wait to get the new board! 


---
**Tech Bravo (Tech BravoTN)** *October 13, 2018 01:41*

generally speaking, i would recommend checking all of the wiring in the machine, adding a current meter, and checking out a few of these links:



[k40laser.se - Warning for new machine owners - grounding issues - K40laser.se](https://k40laser.se/diy-how-to/warning-for-new-machine-owners-grounding-issues/)



[https://k40laser.se/lens-mirrors/mirror-alignment-the-ultimate-guide/](https://k40laser.se/lens-mirrors/mirror-alignment-the-ultimate-guide/)



[https://k40laser.se/air-assist/](https://k40laser.se/air-assist/)



[https://www.lasergods.com/k40-control-panels-analog-vs-digital/](https://www.lasergods.com/k40-control-panels-analog-vs-digital/)



[https://www.lasergods.com/beginners-guide-to-the-k40-laser-engraver/](https://www.lasergods.com/beginners-guide-to-the-k40-laser-engraver/)



[https://www.lasergods.com/laser-lenses-optics-and-focus/](https://www.lasergods.com/laser-lenses-optics-and-focus/)



[https://www.lasergods.com/laser-water-coolants-additives/](https://www.lasergods.com/laser-water-coolants-additives/)



[https://www.lasergods.com/grounds-and-usb-communication/](https://www.lasergods.com/grounds-and-usb-communication/)






---
**Tech Bravo (Tech BravoTN)** *October 13, 2018 01:44*

and the sockets in the back are typically junk and wont handle any current to speak of. its advisable to use a good plug strip for accessories. use your own discretion there because those are different than the 2 wire plugs i am familiar with. they may have beefed them up a bit :) 


---
**Kieron Nolan** *October 13, 2018 01:44*

Thanks... Currently fixing the ground issue and will work through the rest 


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2018 02:46*

I would recommend reading the instructions thoroughly at the link Brian provided, isolating the board from the frame by one of the suggested methods, and asking here if you have any questions. 


---
**Allen Russell** *January 18, 2019 04:50*

Here's some upgrades you could use

![images/82ba1a4529c4f386b051756aee01efd5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82ba1a4529c4f386b051756aee01efd5.jpeg)


---
**Allen Russell** *January 18, 2019 04:50*

![images/b27fe1f650a1bfdcc14c0977101cf74f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b27fe1f650a1bfdcc14c0977101cf74f.jpeg)


---
**Allen Russell** *January 18, 2019 04:51*

![images/866bf4ed6ca429cae5d7988ba5865ca8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/866bf4ed6ca429cae5d7988ba5865ca8.jpeg)


---
**Allen Russell** *January 18, 2019 04:51*

![images/42ba69f1db7b4acf2382f2251f02b0fd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/42ba69f1db7b4acf2382f2251f02b0fd.jpeg)


---
**Allen Russell** *January 18, 2019 04:51*

![images/a25e54307b35a8b5125d79bf6a3da9b4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a25e54307b35a8b5125d79bf6a3da9b4.jpeg)


---
**Allen Russell** *January 18, 2019 04:52*

![images/26b87cba23efe2d983ea9a351cec7e4a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26b87cba23efe2d983ea9a351cec7e4a.jpeg)


---
**Allen Russell** *January 18, 2019 04:52*

![images/94e04549ace621722971cc827e26da63.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/94e04549ace621722971cc827e26da63.jpeg)


---
**Allen Russell** *January 18, 2019 04:52*

Hope this gives you some ideas

![images/851b4f263163ebf1b6cfecd84526a26f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/851b4f263163ebf1b6cfecd84526a26f.jpeg)


---
**Allen Russell** *January 18, 2019 04:53*

![images/616a82d6abe70f385565760ddcea6a9e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/616a82d6abe70f385565760ddcea6a9e.jpeg)


---
**Kieron Nolan** *January 18, 2019 04:58*

Thanks - I've already make a few upgrades, and will hopefully post images soon, but am going through a separation and move, so will have more time to play in a few weeks!


---
*Imported from [Google+](https://plus.google.com/113795912126148646501/posts/6GdfWQbgeFb) &mdash; content and formatting may not be reliable*
