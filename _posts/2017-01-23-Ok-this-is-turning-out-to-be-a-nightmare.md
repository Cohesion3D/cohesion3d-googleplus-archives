---
layout: post
title: "Ok, this is turning out to be a nightmare"
date: January 23, 2017 21:40
category: "General Discussion"
author: "Chad Traywick"
---
Ok, this is turning out to be a nightmare. Either Laserweb is not going to work with this board or this board is not going to work with laserweb. All I do is chase one issue after another. I cant engrave with power lower than 10% unless I drop to 150mm/s or lower, if I dont the laser may or may not fire. 10% is to high of power to engrave lower than 300mm/s and even at that speed its an absolute rough mess. So if I lower the power I have to lower the speed, which has the same outcome as a the higher speed and 10%. I cant change the beam width to anything lower than .13 or it comes out with jagged jumps to the left and right or the laser hardly fires. I have already changed the the wire over to L and the whole 9 yards. Changes in setting that are made that should not effect the laser firing does. Changes to speed, power that should not effect the laser firing or the outcome of the paths does.



Im not new to motion control even slightly. I run a 3D farm of 18 printers that I keep going 24/7 I know the ends and outs of firmware. This is just getting a little crazy. When I can say that my laser worked way better with the original board and using the crappy Chinese software there is a problem. 





**"Chad Traywick"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 22:07*

Hi Chad,

I understand the concerns and I am here to help. 

Would you be able to put together a folder for me with the example file itself you are trying to engrave, some of the gCode's you have outputted, and the (range of) values you have used in LaserWeb?  I will go down to the laser and try to reproduce what is happening.  I appreciate you bearing with me as I work to figure out what is going on and how to resolve it.

Thanks,

Ray


---
**Tony Sobczak** *January 23, 2017 22:51*

Following 


---
**Alex Krause** *January 23, 2017 22:57*

Typically a K40 fires around 3-4 ma but can be as low as 2ma if the water is the correct temperature and you are working with the right material that shows an engrave at that low of power... This goes for the m2 nano or Moshi board as well...


---
**Tony Sobczak** *January 24, 2017 06:56*

Following 


---
*Imported from [Google+](https://plus.google.com/101895536937391772080/posts/L1QGKFWQgps) &mdash; content and formatting may not be reliable*
