---
layout: post
title: "Hello, I have just got the cohesion3D mini as a replacement board for my K40 nano m2"
date: September 25, 2017 16:15
category: "C3D Mini Support"
author: "ki ki"
---
Hello, I have just got the cohesion3D mini as a replacement board for my K40 nano m2. I have installed it correctly and laserweb detects it (the firmware version is displayed in the laserweb dialogue). I have changed the confi file pwm from 200 to 400 and the high pitched noise has gone. I now have the issue where in laserweb I jog the axis and I can move the rail forward and to the right but not back to home, the test laser button also does not work.

I guess I am missing something in the setup but I followed the instructions.

Any help would be appreciated.

Thanks





**"ki ki"**

---
---
**Griffin Paquette** *September 25, 2017 18:21*

You might want to check your endstops. If it won't move left it might be assuming the home is triggered all the time and therefore not be able to move past what it thinks is 0. 


---
**Ray Kholodovsky (Cohesion3D)** *September 25, 2017 19:26*

Can you please show pictures of the board and wiring, and clarify that both axes will not jog the second direction?   



Can you send a G28.2 command via the terminal (bottom of laserweb window) and tell me what happens?  This should home the machine.  It will touch the switches and then back off and touch them again at a lower speed, so a noise near the corner isn't necessarily grinding, but could be touching off the second time. 


---
**ki ki** *September 26, 2017 09:34*

thank you for the replies, I will check my endstops but I don't think there is anything wrong with them. I will also take a picture of the board so you can verify the cabling. I will also try the instructions and report back. I did notice that my config file is broken, there is lots of 'junk' text at the end and has the last third of the file missing so I will load the config file from the online dropbox link and then run some tests. Thanks again for the quick replies.


---
**Joe Alexander** *September 26, 2017 10:34*

don't forget to set the parameters in LaserWeb4 itself under settings-machine. set homing to G28.2(or $H if G28.2 doesnt work) and set the test power and duration. That should enable the buttons.


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2017 11:56*

Yes if this is the case then please format the card fat32 and replace the files on it. Make sure to always "safely dismount"/ eject the card before unplugging the USB cable, otherwise corruption can occur. 


---
**ki ki** *September 26, 2017 16:00*

I have loaded the files from dropbox, double checked the laserweb and tried moving the axis, same as before, I can move towards the front and to the right but once I try moving back towards the home position nothing responds any more. homing G28.2 gives an " error:Unsupported command - g28.2" error. I am also unable to get the laser to fire either in laserweb or on the "test switch" on the k40 panel. I think the wiring is correct, I will post a picture, change the homing to "$H" and format the card and put the files back on it.

Thank you again for the help and support.


---
**ki ki** *September 26, 2017 16:04*

![images/95db3fcf8a45f13c8f0efb3331cbaaa4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/95db3fcf8a45f13c8f0efb3331cbaaa4.jpeg)


---
**ki ki** *September 26, 2017 16:24*

Success!!! I formatted the sd card, reloaded the files from dropbox, used $H for homing and now, the laser fires, homes and I can move all the different axies!! WOOT!! Thank you all for your help, I would still be trying to figure it all out if it was not for your help.

On a laserweb related note, I am wondering what the "cut rate option I need to fill in is and what to set it at? I saw on a YTube video that it was set at 100%, is this correct?

Off to test some engravings!

=)


---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2017 16:27*

G28.2 should also do homing the same way as $H , please try that at some point to satisfy my curiosity. :) 



Can you screenshot the LW setting you are referring to? 


---
**ki ki** *September 26, 2017 16:32*

ok, strange, I changed the homing back to G28.2 and it also works.... This is changed in the "GCode homing" section of the Gcode in the settings option.

Also, the laser will fire but it will not engrave anything, it also moves faster on the Y axis than on the X axis.


---
**ki ki** *September 26, 2017 16:34*

Also when firing the laser makes a high pitched noise, I will change the laser_module_pwm_period setting to 400 and see if this helps but I do not know what the correct setting is, this is trial and error.


---
**ki ki** *September 26, 2017 17:42*

Looking into the laserweb doc before posting any laserweb questions here.... =)




---
**Ray Kholodovsky (Cohesion3D)** *September 26, 2017 17:44*

In our documentation site the k40 --> troubleshooting/ QA page describes the pwm tuning procedure. You keep it at 200 unless your grayscale is not coming out with enough resolution then you would increases to 400 and try again. Can't hurt. 


---
**ki ki** *September 26, 2017 20:12*

thanks, I will go and tune the pwm properly.


---
**ki ki** *September 27, 2017 16:56*

I am trying to set the PWM settings but when I put G code into laserweb, the text box under the status information that scrolls up as you use the board, I do not get any reaction from the laser. Ie, I type G21, then the next series of code 'G0 X0 Z0' and there is no movement (I take it this is homing, I tested this by moving the laser head off home and then typing G0 X0 Z0 but nothing happens. Also and I do not know if this is related but when I use laserweb to engrave something it just burns on the X axis without moving the Y axis at all... giving a burnt line along the X axis. I am not sure if I have a cohesion3d config issue or a laserweb config issue (I did follow the laserweb setup  so I think laserweb is good).


---
**Ray Kholodovsky (Cohesion3D)** *September 27, 2017 17:26*

So...  The block of text in the pwm tuning file was meant to be run as a .gcode file, no need to type line by line in manually.  I should post such a file as an attachment. 



G28.2 moves to the switches which are back left. At this point in time the machine is at 0, 200.  

If you say G0 X0 Y0 (you wrote Z0, not sure where the typo happened) it should move to the front left.  Keep in mind if you move the head by hand, it will never know that the position has changed, so everything needs to be done via jog commands. 



Here's what we need to do: 

Unplug usb, restart the machine, and replug the USB cable, so that a full power cycle happens. 



Set your pot or panel to 10mA (roughly half power)



G28.2     head should touch switches back left. 

G0 X0 Y0 F600   should move front left while not firing. 



G1 X10 S0.6 F600    this should fire while moving. 



If this works, then you can run another line with a different target (X20) and a different S value between 0 and 1 and see if you observe the lighter/ darker lines and the needle on the current gauge showing the proper value. 



It works as a % proportion so if you're at 10mA and you use S0.6 you should observe roughly 6mA on the needle.  I have it capped to 80% in config.txt so that someone doesn't run their laser at full power and blow the tube prematurely.  Now that I've explained this bit to you (DON'T SET THE POT TO FULL POWER) we can make that change to the config file later. 



Please report back as to what happens and if/ when something different than expected happened. 


---
**ki ki** *September 28, 2017 17:05*

Hi Ray, I gave your instructions a go, I think the code you supplied woks fine. From the home position the laser head comes forward to the front and burns successively more powerful lines in a row starting at the front towards the back. It is difficult to set the pot to half power but I do get a percentage increase in power as it engraves. I think everything went as it should. As it looked good I went and loaded a .jpg into laserweb. Once I click generate in laserweb the area to engrave goes red and all that happens on the laser is it moved left to right (not top or bottom, laser does not engage.).  Below you can see the cut rate field I need to fill in before I can engrave anything. I have tried 100. I get the results above (left right movement no front back or laser) at 100, maybe it needs to be a higher number? Any ideas are more than welcome, I cant wait to get this running! =)

![images/d7c16c9fd50011191123f70455882fc0.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d7c16c9fd50011191123f70455882fc0.png)


---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2017 17:38*

Can you confirm whether you have your feedrate in settings configured to be mm/ min or mm/ s ?


---
**ki ki** *September 30, 2017 08:59*

Hi Ray, I have left it at the default mm/min. Do I need to change this? thanks


---
**Claudio Prezzi** *September 30, 2017 09:35*

Cut Rate is the speed, not the power. If your feedrate setting is mm/min, then 1000 is a good cut rate to start with.

It is normal that picture engraving mainly scannes left<>right, but after every line it moves a tiny step to the back (0.2mm by default).


---
**ki ki** *September 30, 2017 15:12*

Thank you for clearing this up for me Claudio, I will launch a burn and see what happens.


---
*Imported from [Google+](https://plus.google.com/101815393777110076549/posts/bxUNmEuGYyH) &mdash; content and formatting may not be reliable*
