---
layout: post
title: "Got the board in, go to engrave and this is how the machine runs"
date: January 18, 2017 06:04
category: "General Discussion"
author: "tyler hallberg"
---
Got the board in, go to engrave and this is how the machine runs. No matter what I change the speed to, it does this. What am I doing wrong? Maybe a bad config file? Bad laserweb settings? Been trying to mess with different settings one at a time now for about 3 hours and nothing seems to help. Any help is much appreciated! 



![images/ff698148eaf07c23a64b778d5e75a0fe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ff698148eaf07c23a64b778d5e75a0fe.jpeg)
![images/355323d056ade8ec90506fd36eb5427f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/355323d056ade8ec90506fd36eb5427f.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-QFqY8zIAIKM/WH8FYygpqeI/AAAAAAAAAXM/UOgAk3o0JWM7wh3ysiFsKkJGL-VzYA89gCJoC/s0/20170117_220101.mp4**
![images/81efc19bf4fee366732c436f6da5788e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/81efc19bf4fee366732c436f6da5788e.jpeg)

**"tyler hallberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 06:06*

Hey Tyler I'm on a call right now I'll go through and answer your inquiries in the next few hours.  Cheers, Ray


---
**tyler hallberg** *January 18, 2017 06:07*

no rush, throwing in the towel for the evening, have work in 6 hours. Ill be tinkering with it tomorrow evening when I get home from work.


---
**tyler hallberg** *January 18, 2017 07:05*

![images/f8bcc59b1c81a55629965eb046c7543d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8bcc59b1c81a55629965eb046c7543d.jpeg)


---
**tyler hallberg** *January 18, 2017 07:11*

![images/4ce99e0e5dc27f5b9a82816b78c97672.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4ce99e0e5dc27f5b9a82816b78c97672.jpeg)


---
**tyler hallberg** *January 18, 2017 07:13*

Not even a little. Do these numbers need to be about the same? Can tab I've got set at 250, I'll try making them both that 


---
**tyler hallberg** *January 18, 2017 07:17*

Changed both to 250, problem still there unfortunately 


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 07:46*

Hey Peter, 

Thanks for jumping in. 



Tyler - your settings looks good to me, especially with the M3 and M5 in the right spot. 



What if you try the same gcode file, but save it and copy it to the sd card that shows up, and run it from the sd card? This could quickly rule out any USB related concerns. 


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 07:47*

Here is some info on how to play a file on the sd card: [http://smoothieware.org/player](http://smoothieware.org/player)


---
**Robin Sieders** *January 18, 2017 17:24*

When I was messing around with engraving last night in LW3 I had to keep the light and dark settings within about 10mm of each other, otherwise I'd get quite a bit of stuttering as the machine tried to accellerate and slow down over a small area.


---
**Robin Sieders** *January 18, 2017 17:38*

**+Peter van der Walt**  Any inkling of when LW4 will be ready for prime time? Would love to have the variable power by colour feature.


---
**Robin Sieders** *January 18, 2017 17:45*

**+Peter van der Walt** Ah excellent, something to tinker with this evening then. And a beer is definitely in order for **+Claudio Prezzi** 


---
**Robin Sieders** *January 18, 2017 17:48*

Definitely a busy guy!


---
**tyler hallberg** *January 18, 2017 17:51*

I did try everything set at same speed, same problems. I havnt gotten to try the lw4 yet. At work for about 9 more hours then I'll try 


---
**tyler hallberg** *January 19, 2017 02:42*

tried lw4 code, same problem. Possibly problems in the config file then?


---
**Robin Sieders** *January 19, 2017 02:46*

Try changing your Travel moves speed to 250mm/sec. I was getting lots of jerkiness during engraves until I changed it.


---
**tyler hallberg** *January 19, 2017 02:53*

250 has been my base number the whole time, so thats not it either unfortunately 


---
**tyler hallberg** *January 19, 2017 03:58*

I did do some test cuts, seems to cut fine, everything runs smoothly, its just the engraving thats not working


---
**tyler hallberg** *January 19, 2017 04:09*

Ive tried about 15 different pictures thinking the same thing, ive tried hi res pictures low res pictures, it does the exact same thing everytime


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 04:15*

I'd still ask you to save one of the gcode files to the sd card and run it from there (I explained this in previous comments above) just so that we can rule any comms/ USB stuff out.  And if you see the same performance as you did when you were running it from LW directly, then we can indeed rule out USB as a culprit.


---
**tyler hallberg** *January 19, 2017 04:35*

trying to make sense on how to get a file to run from the SD card, will update once I get it


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 04:39*

Copy the file onto the hard drive that shows up on your computer when you plug the board in.  

To run, check the play documentation here: [smoothieware.org - Player - Smoothie Project](http://smoothieware.org/player)

Use the LaserWeb terminal to send the command.

ie: play /sd/gcodefile.gcode


---
**tyler hallberg** *January 19, 2017 04:58*

laserweb keeps kicking back saying unsupported command, ill keep trying different ways of putting it in


---
**tyler hallberg** *January 19, 2017 05:01*

it keeps deleting my spaces when using the laserweb command prompt. testing other ways of getting in


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 05:02*

Pronterface. [koti.kapsi.fi - Index of /~kliment/printrun](http://koti.kapsi.fi/~kliment/printrun/)



Put the @ in front as per the article. 


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 05:03*

Or any other serial console like, Teraterm. 

But Pronterface first. 


---
**tyler hallberg** *January 19, 2017 05:15*

ok, some different news, it runs to a point doing it that way, laser on the whole time


---
**tyler hallberg** *January 19, 2017 05:15*

This was inputted as white 0 black 20%

![images/02a3248dfa5f1ed97d61c7aa4d74f1e8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/02a3248dfa5f1ed97d61c7aa4d74f1e8.jpeg)


---
**tyler hallberg** *January 19, 2017 05:19*

This is what comes out

![images/2345c303f82fa27833bb00964cd5e043.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2345c303f82fa27833bb00964cd5e043.jpeg)


---
**tyler hallberg** *January 19, 2017 05:41*

I just updated laserwebs, seems to be running from program now, but constant laser on, so its not shutting down to miss the white. one problem down one problem up.


---
**tyler hallberg** *January 19, 2017 05:48*

Removed m3 and the m5 from start and end g code and put them in pre and post g code and got some more progress

![images/59fccd263cb40e5d8d8ec7df163a1f74.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/59fccd263cb40e5d8d8ec7df163a1f74.jpeg)


---
**tyler hallberg** *January 19, 2017 05:51*

Now the only problem seems to be the power of the laser no mater what the setting is its the same power. 12% same as 30%


---
**tyler hallberg** *January 19, 2017 05:54*

Could this be because of one of these wires? I have the digital display k40. There is 4 wires coming from controller on lid, three of which go into the one plug that was replaced with Smoothie plug and 4th goes into separate plug

![images/0e975b8c8da2ab78c0392c7e43c99587.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0e975b8c8da2ab78c0392c7e43c99587.jpeg)


---
**Claudio Prezzi** *January 19, 2017 12:03*

Just to be sure, you did install [firmware-cnc.bin](http://firmware-cnc.bin) on your board, not the normal [firmware.bin](http://firmware.bin), right?


---
**tyler hallberg** *January 19, 2017 14:03*

With nothing in the pre and post g1 fields the laser does not kick on. No pausing, the machine runs great at 250mm/s with those fields inputted. What do you suggest then? 


---
**tyler hallberg** *January 19, 2017 14:05*

I guess back to the drawing board tonight when I get home from work


---
**tyler hallberg** *January 19, 2017 17:56*

I'll have to see if I can find a pin out got my power supply to double check wiring 


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 17:59*

Thanks Peter. Tyler, indeed let's see some pictures of your machine, wiring, and board please. 


---
**tyler hallberg** *January 19, 2017 18:27*

I posted them in the og thread. I'll post them again. Also someone mentioned the firmware. It's running the firmware that was on the card. Does that need to be changed? 


---
**tyler hallberg** *January 19, 2017 18:27*

![images/c5de2220d5bca0dbedf834d7c7e9bb4a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c5de2220d5bca0dbedf834d7c7e9bb4a.jpeg)


---
**tyler hallberg** *January 19, 2017 18:28*

![images/b4c755dc87aa8c3b6ec0f9f391b15bc9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b4c755dc87aa8c3b6ec0f9f391b15bc9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 18:28*

It is running the cnc firmware already. Do not change that, do not change any config options on the card either please.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 18:32*

Wiring looks good to me.   But you said you have a digital display, can I see a pic of that from the panel and the wire coming from it please?


---
**tyler hallberg** *January 19, 2017 18:37*

I'll get photos tonight on that 


---
**tyler hallberg** *January 19, 2017 18:39*

Digital display does not work now as the 3 wire plug that goes to psu from Smoothie board was the same plug the display plugged into 


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 18:41*

Understood.   I have the same PSU and wiring style as your machine, but I have an old school pot and not a digital display, so I am interested in seeing that.


---
**tyler hallberg** *January 19, 2017 18:42*

Ebay picture of the display, I'll snap some photos of the board tonight 

![images/e8ddbb29b0f86d660a3ea80079fe522e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e8ddbb29b0f86d660a3ea80079fe522e.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 18:45*

Gotcha.  I'm just trying to figure out if it does something differently.  What I could have you do is try the alternate wiring configuration - unplug pwm cable, hook the display back up.  Pop out the L wire from the power connector on the mini side, and pop it into - P2.5/ Bed terminal and we set that for use as PWM.  That would also rule out any inconsistencies between what the display outputs, etc....  More on this tonight...


---
**Greg Stephens** *January 19, 2017 20:12*

I was having jerky movements when engraving.  Changing the Laser Beam Diameter from .15 to .2 made a difference 


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 20:21*

Cool. And remember that value can range anywhere from .1 to .5


---
**tyler hallberg** *January 20, 2017 01:59*

![images/ed389df45c38f781fc20616f99cd8fe7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed389df45c38f781fc20616f99cd8fe7.jpeg)


---
**tyler hallberg** *January 20, 2017 01:59*

![images/fd6627a47f44ec9cb3789f01b7a6e8eb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd6627a47f44ec9cb3789f01b7a6e8eb.jpeg)


---
**tyler hallberg** *January 20, 2017 02:00*

![images/6c94ccc4f49782752997cfc841e06b1b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6c94ccc4f49782752997cfc841e06b1b.jpeg)


---
**tyler hallberg** *January 20, 2017 02:00*

![images/5db2e7cdbe67109cb3dc677339324434.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5db2e7cdbe67109cb3dc677339324434.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 02:01*

Tyler, so that looks like a 4 pin jst coming out of the display-panel. Where does it go? 


---
**tyler hallberg** *January 20, 2017 02:03*

4 pin, 3 of them go to the plus on the psu that the plug for the cohesion board plug into, other wire goes into plug next to 3 plug


---
**tyler hallberg** *January 20, 2017 02:03*

![images/aca708c2ee11662f634c0e9799284a60.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aca708c2ee11662f634c0e9799284a60.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 02:34*

Gotcha.  Sounds like it's time to put those back the way it was originally and route L from the mini side 4 pin power connector to the -bed/ 2.5 mosfet.  Please see the pinout diagram and this excerpt  (screenshot) for the general idea, it's also mentioned in other recent comment threads here.: [s3.amazonaws.com - s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077520804/original/ngpiZEyrN729NMTYzqpLckxY0faes3CIYQ.png?1484875212](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077520804/original/ngpiZEyrN729NMTYzqpLckxY0faes3CIYQ.png?1484875212)


---
**tyler hallberg** *January 20, 2017 02:55*

We may have solved 85% of the puzzle, stand by


---
**tyler hallberg** *January 20, 2017 02:59*

A step in the right direction. It is controlling the laser now, which is great, but for some reason it's got all these odd lines on it. Possibly the laser width setting? 

![images/26e54ada90c8cee2af2523bf9c70a76d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26e54ada90c8cee2af2523bf9c70a76d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:02*

Yes, fiddle towards/ within 0.1 and 0.5 values for it.  


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:05*

I think smaller than your previous value is what you need.


---
**tyler hallberg** *January 20, 2017 03:14*

So the picture I posted was @.2 just tried .1 and .15 and both studded like mad. No other changes other than the width setting.


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:49*

Ok.  Sounds like we've solved the hardware side of things.  **+Peter van der Walt** can I trouble you to jump back in for a bit? We've got Tyler on the 1 wire PWM going to L wiring configuration, and now need to just tune the engraving a bit.   Troubleshooting rasters is not my strong suit I'm afraid.


---
**tyler hallberg** *January 20, 2017 03:53*

Going to try to run a code out of lw4 to see if it helps anything again, I'll post a video shortly of what it's doing


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:54*

Sounds good. I've been a bit under the weather so I'll be hitting the hay soon. I'm glad we made progress today. Keep posted. 


---
**tyler hallberg** *January 20, 2017 04:06*

seems to me there isnt really much control on laser power when engraving. setting min to 0% and max to 15% gets same results as setting min to 0% and max to 85%.


---
**tyler hallberg** *January 20, 2017 04:08*

and setting .1 with laser width and running a simple line graphic runs great. When loading a picture in though to engrave is when it stumbles on itself.


---
**tyler hallberg** *January 20, 2017 04:58*

running file from laserweb its almost impossible to run file, download the gcode and run off sd card, runs but studders a little which puts a lot of pinholes in whatever im engraving. Still no real control over laser power. I think it has to do with having the display plugged in still as I can change the laser power during engraving from the display.


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 17:01*

I have a sneaking suspicion regarding the 0.1 not working well. The pots on the drivers, by default they should come with the flat facing out, toward the edge of the boards. Turn them 45 degrees clockwise. This will give the motors a bit more current and may improve higher speed performance. Do that with all power off. Ideally you would use a ceramic screwdriver to avoid any shock, but just ground yourself and the screwdriver by touching some metal. 


---
**tyler hallberg** *January 20, 2017 21:47*

Will try when I get home


---
**tyler hallberg** *January 21, 2017 02:57*

Got the screen hooked up and looks like changing the laser power on there is working, also turned the pots 45*  like mentioned. Will try a file here shortly to see if it made any difference.


---
**tyler hallberg** *January 21, 2017 21:52*

Photo of my digital control board

![images/f0bb4372c3d5f74d5a22829bcfb2bc56.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f0bb4372c3d5f74d5a22829bcfb2bc56.jpeg)


---
**tyler hallberg** *January 21, 2017 21:53*

So this is the 5 pin plug for the board, only 4 of which are used, top 3 and bottom. The p- is tripped when I push down the test button, and also is the single wire going into my psu

![images/0647b3f2fc452b536df7be9623d3c139.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0647b3f2fc452b536df7be9623d3c139.jpeg)


---
**tyler hallberg** *January 21, 2017 21:54*

![images/772298cfdf24eab8ba8705e040e8e591.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/772298cfdf24eab8ba8705e040e8e591.jpeg)


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/SeiHP1dLUST) &mdash; content and formatting may not be reliable*
