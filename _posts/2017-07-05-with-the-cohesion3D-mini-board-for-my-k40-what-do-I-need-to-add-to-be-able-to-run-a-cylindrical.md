---
layout: post
title: "with the cohesion3D mini board for my k40 what do I need to add to be able to run a cylindrical"
date: July 05, 2017 04:53
category: "General Discussion"
author: "William Kearns"
---
with the cohesion3D mini board for my k40 what do I need to add to be able to run a cylindrical 





**"William Kearns"**

---
---
**Joe Alexander** *July 05, 2017 05:53*

you'll need a rotary device to start. Wire that to the z axis and with a config file tweak and some fine tuning it can be done. There is a rotary plan shared on the K40 group that can be laser cut, i made one myself and it seems relatively reliable for the couple hours it took to build. I believe its also on thingiverse.


---
**Ashley M. Kirchner [Norym]** *July 05, 2017 07:01*

While not required, I much prefer it get wire to the "A" slot, leaving the "Z" axis for ... a z-table. Z for Z, A for something else, accessory if you will. But other than that, a stepper driver (likely external), the rotary, and the C3D adapter. For more information, please visit the C3D Documentation page for wiring external drivers:

[cohesion3d.freshdesk.com - Wiring a Z Table and Rotary with External Stepper Drivers : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000739904-wiring-a-z-table-and-rotary-with-external-stepper-drivers)


---
**E Caswell** *July 05, 2017 21:16*

**+William Kearns** check this out.  Give you an idea of what I did to get a rotary axis running [plus.google.com - K40 rotary axis up-grade See link to files for my #K40Rotaryaxis up-grade. P...](https://plus.google.com/106553391794996794059/posts/bt7obpUNAwt)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/jeAUPjMYEM8) &mdash; content and formatting may not be reliable*
