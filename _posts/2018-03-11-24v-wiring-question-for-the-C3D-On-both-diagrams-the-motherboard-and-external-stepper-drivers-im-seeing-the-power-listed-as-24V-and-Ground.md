---
layout: post
title: "24v wiring question for the C3D. On both diagrams, the motherboard and external stepper drivers i'm seeing the power listed as +24V and Ground"
date: March 11, 2018 23:44
category: "K40 and other Lasers"
author: "Bill Keeter"
---
24v wiring question for the C3D.



On both diagrams, the motherboard and external stepper drivers i'm seeing the power listed as +24V and Ground. If i'm running a separate 24V PSU am I actually connecting the "Ground" for the C3D and Stepper drivers to Ground or just the -24V line. (I know, it's probably a dumb question but with this 60w laser nearly done, I rather not take any chances.)



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570)



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5084755152/original/d6KGPB7LWektZ7SVDGRzgIOsw0m_0hiELA.png?1498250922](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5084755152/original/d6KGPB7LWektZ7SVDGRzgIOsw0m_0hiELA.png?1498250922)





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2018 23:46*

There's no -24 only +24v and Gnd. 



The other guide shows step by step wiring. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/aXbxnRHRRxE) &mdash; content and formatting may not be reliable*
