---
layout: post
title: "I'm in testing right now, but I wanted to share this photo as I've not seen a photo of the configuration with external power"
date: June 26, 2017 22:49
category: "Show and Tell"
author: "John Milleker Jr."
---
I'm in testing right now, but I wanted to share this photo as I've not seen a photo of the configuration with external power. A huge thanks to **+Ray Kholodovsky** for the quick new board and some really good brainstorming ideas as well as others on this group.



Long story short, got a few simple jobs cut and then sent through my problem cut. The board with existing power was a no go. It performed 'better' but certainly still an issue. Next test is external power.



Hooked up an external 24v 6a power supply, unhooked the power cable from the power supply, pulled the laser lead from the power connector and put it into the P2.5/BED Mosfet as I've seen described, but haven't seen a photo of the setup and wanted to post to help others.



DO NOT perform this modification on the photo alone. Read the documentation and Ray's instructions on the procedure. STUDY the pinout of the board and ALWAYS test your power supply to make sure you're putting positive and negative where it needs to go. 

![images/290179e399b31642355ef8d1f5c65f8b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/290179e399b31642355ef8d1f5c65f8b.jpeg)



**"John Milleker Jr."**

---
---
**E Caswell** *June 27, 2017 06:20*

**+John Milleker Jr.** you will find the external power supply much better and a much smoother operation when it's working. 



Here is my configuration with separate power supply. 



[plus.google.com - After all the trials and tribulations of getting the board operational I fina...](https://plus.google.com/106553391794996794059/posts/15Bu53RBHbY)


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/CreZTMv2PxY) &mdash; content and formatting may not be reliable*
