---
layout: post
title: "Hi, As requested by Ray Kholodovsky I am posting my issue with my K40 laser cutter here"
date: November 14, 2017 17:44
category: "C3D Mini Support"
author: "Reuben C"
---
Hi,



As requested by **+Ray Kholodovsky** I am posting my issue with my K40 laser cutter here.



I have a K40 Laser cutter upgraded with a Cohesion3D mini board. Using laserweb4 the K40 was able to cut and engrave perfectly well, but never homed properly (this version has optical end stops)



At any rate, in trying to fix the homing problem by adjusting the config file I am now in the following situation:



The k40 connects to laserweb4 as normal

If a 'jog' code is sent the gantry does not move

If a job is sent the gantry still doesn't move but the laser fires appropriately.



I have swapped out the A4988 stepper drivers and reflashed the original config.txt and firmware but with no luck.



Any help you may be able to offer would be much appreciated.



Attached is a photo of my board.



Thankyou! 

![images/d8826c8916197b5adf24c23582b8c178.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d8826c8916197b5adf24c23582b8c178.jpeg)



**"Reuben C"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 17:46*

Thanks for posting here. Please turn on the k40 power and tell me what LEDs you see on the board and what they are doing. 


---
**Reuben C** *November 14, 2017 17:51*

Thankyou for the quick response! When I power up the K40 (without plugging in USB) there is no LED activity


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 21:27*

That's certainly a concern. This exact wiring configuration has worked before?  Have you plugged the black cable in backwards perhaps?  



If you have a multi meter it would be good to disconnect the plug from the board and verify you are seeing 24v between the 24v and Gnd pins of that k40 power connector. Let's start there. 


---
**Reuben C** *November 14, 2017 21:40*

This wiring configuration was working fine. I do have a multimeter - which wires should I be checking between please?


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2017 21:42*

Please consult the Mini pinout diagram on our documentation site. 



Find the k40 power connector and which pins should be 24v and Gnd. Then pull the connector and meter those pins. 


---
**Reuben C** *November 20, 2017 21:40*

I can confirm I am getting 0v when testing for Voltage between what should  be the 24V wire and GND.


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2017 21:45*

Your laser power supply is not providing any 24 volts when it should be. Thus, motors will not turn. 



Since the laser still fires I would suggest getting a separate 24v 6a power supply to power the board and motors.  



But you may want to consult the K40 Groups for advice to make sure there are no other concerns with your situation. 



[https://plus.google.com/communities/118113483589382049502](https://plus.google.com/communities/118113483589382049502)


---
**Reuben C** *November 20, 2017 23:59*

Hi Again - good news...After finding a Pin-out diagram for the K40 power board too I was able to establish that the board was outputting 24V correctly. Tracing wires led me to a malfunctioning switch that I had no idea was even part of the 24V loop... bypassing that has got the gantry moving again - now I just need to figure out why the X axis endstop isnt working! Thanks for your help.


---
*Imported from [Google+](https://plus.google.com/110447302542239112632/posts/QkHxNVaSHAL) &mdash; content and formatting may not be reliable*
