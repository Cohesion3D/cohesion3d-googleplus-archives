---
layout: post
title: "I love this board in my K40"
date: August 15, 2017 16:17
category: "K40 and other Lasers"
author: "Aerial Optics"
---
I love this board in my K40.  Okay thats out of the way I do have a question.  I have a newer K40 that has the digital % display.  Is this still the master over the board, so if I have the display at 30% and Laser web set to 20% is that the 20% of the allowed 30%?





**"Aerial Optics"**

---
---
**Don Kleinschnitz Jr.** *August 15, 2017 16:35*

Yes, should be!


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2017 21:17*

Yep, exactly. It's always a proportion. The digital % replaces the pot, it's still the master and the wiring of the board does not change. 



And glad you like it! 


---
*Imported from [Google+](https://plus.google.com/103391458129974640821/posts/2Gb7ttkH6sR) &mdash; content and formatting may not be reliable*
