---
layout: post
title: "howdy gang, so I'm a bit at a loss right now"
date: March 26, 2018 03:23
category: "C3D Mini Support"
author: "Carlos Porto"
---
howdy gang, so I'm a bit at a loss right now. Got my c3d-mini hooked up, got first moves and the end stops working from what I've done so far. The main issue right now is the the bed and hot-end are not heating up. Thermistors seem to be working, since they are changing and everything seems to be connected properly. I verified the wires are not broken. Could it be a bad board?



![images/42062a6f7007c2905e454e656acc9a85.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/42062a6f7007c2905e454e656acc9a85.jpeg)
![images/c3e4d5094bd96ecdf94e29ff81e40e62.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c3e4d5094bd96ecdf94e29ff81e40e62.jpeg)

**"Carlos Porto"**

---
---
**Gregor** *March 26, 2018 08:09*

Hello, you also have to power the FET-IN.

Nothing else works.



So connect VIN and FET-IN to your power source.

Then it should work.


---
**Ray Kholodovsky (Cohesion3D)** *March 26, 2018 11:44*

Yes.



I also question why you have the white endstop wires plugged into XMX and YMX when X Min and Y Min are available and those are protected 


---
**Griffin Paquette** *March 26, 2018 14:04*

Gotta apply power to the leftmost screw terminals on that row. I would also move the endstops over otherwise you will have to reconfigure in config. 


---
**Carlos Porto** *March 26, 2018 14:23*

Thanks for the info. I didn't realize it needed to be powered. Out of curiosity, why does it need additional power connection. Mind you I'm more of a layman then an electrical engineer. 








---
**Griffin Paquette** *March 26, 2018 14:46*

Having the separate input allows you to run either different voltages and/or more current through the mosfets. If you only used one screw terminal for the whole board, you would be limited to how much power can go through those mosfets without melting the input terminal.


---
**Carlos Porto** *March 26, 2018 16:20*

That's genius! Previous boards I've used where never like this. So this is a great solution. And I can confirm I've got heat working :)










---
**Carlos Porto** *March 26, 2018 17:08*

So my second question as Ray previously pointed out, was about the end stops. when they are plugged into the X-min and Ymin it is always being reported as on when I do a M119 command. When I have it connected to the X/Ymax it works properly. I'm thinking it's something to do with the configuration? This is what I have below which is default setup:



endstops_enable                              true       

delta_homing                                 true       

alpha_min_endstop                            nc!        

alpha_max_endstop                            1.25^      

alpha_homing_direction                       home_to_max

alpha_max                                    0          

beta_min_endstop                             nc         

beta_max_endstop                             1.27^      

beta_homing_direction                        home_to_max

beta_max                                     0          

gamma_min_endstop                            nc         

gamma_max_endstop                            1.29^      

gamma_homing_direction                       home_to_max

gamma_max                                    333.8      

alpha_max_travel                             1000       

beta_max_travel                              1000       

gamma_max_travel                             1000       


---
**Ray Kholodovsky (Cohesion3D)** *March 26, 2018 18:15*

The aforementioned protection inverts the logic. Add ! to flip. 



Don't say nc! just nc


---
**Carlos Porto** *March 26, 2018 20:07*

Sorry about that, it was a mistype on my part. After reading documentation on smoothie for endstops, seems it needed to be a pull-down and reversed. Go figure, it's been a pull up on the previous board. Anyway, looks like should be good to go from here.


---
**Carlos Porto** *March 27, 2018 01:54*

And thanks again guys, finally got this beast printing again.

![images/6f08f83f48e63f5921b1dd1fe8c2dd9c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6f08f83f48e63f5921b1dd1fe8c2dd9c.jpeg)


---
*Imported from [Google+](https://plus.google.com/112207937847873893334/posts/2gPbbrGETnJ) &mdash; content and formatting may not be reliable*
