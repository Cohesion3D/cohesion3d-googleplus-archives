---
layout: post
title: "So, After messing around to get Z-homing working with grbl on my k40 with the C3d Mini, I thought I would just post up a little grbl cheat sheet I used to help me while I was working on the issue"
date: November 02, 2018 23:24
category: "FirmWare and Config."
author: "Brent Dowell"
---
So, After messing around to get Z-homing working with grbl on my k40 with the C3d Mini, I thought I would just post up a little grbl cheat sheet I used to help me while I was working on the issue.



It has my current settings in it, Just thought it might be useful.



Has the invert mask settings, alarm codes and error codes all in one place.



Oh, I haven't got my Rotary (A axis) running yet, but will update this once I do.



[https://docs.google.com/spreadsheets/d/1u5wntRKi7sLr3ojq5Rsy4Jq6FwKQIVBvWUg8aXhcpEs/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1u5wntRKi7sLr3ojq5Rsy4Jq6FwKQIVBvWUg8aXhcpEs/edit?usp=sharing)



Link to the firmware I'm using

[https://www.dropbox.com/s/ci5rslds2i71flt/firmware%20xyza%20xyz%20homing.bin?dl=0](https://www.dropbox.com/s/ci5rslds2i71flt/firmware%20xyza%20xyz%20homing.bin?dl=0)







**"Brent Dowell"**

---
---
**Brent Dowell** *November 04, 2018 01:26*

I got my rotary working today. Only change I had to make was I can't use the rotary and the z table at the same time, because there's not enough room under the laser. I added another firmware file to the dropbox that has the A step/mm set to 8.888 and does not try to home the z axis.



Using onboard steppers for both the Z and the A axis.



Firmware for Rotary

[https://www.dropbox.com/.../firmware_xyhomeing_rotary.bin.](https://www.dropbox.com/.../firmware_xyhomeing_rotary.bin.)..



Rotary is from here 

[https://www.thingiverse.com/thing:3174149](https://www.thingiverse.com/thing:3174149)



![images/35a08d021f2afd5f7e55ec7578a40503.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/35a08d021f2afd5f7e55ec7578a40503.jpeg)


---
*Imported from [Google+](https://plus.google.com/117727970671016840575/posts/6eRCURMqcnz) &mdash; content and formatting may not be reliable*
