---
layout: post
title: "Has anyone here used tmc2100 drivers with their board?"
date: March 19, 2018 15:22
category: "C3D Mini Support"
author: "Brian Albers"
---
Has anyone here used tmc2100 drivers with their board?  I bought a few of them and dropped 2 in my cohesion mini board, confirmed it was installed in the correct direction only to have the GLCD emit a loud continous beep (not just the boot up beep).  I shut it down, removed the driver and I just got the startup beep.  Installed a DRV8825 driver and only get the startup beep and the axis moves back and forth like normal.     Any ideas as to why this is happening?





**"Brian Albers"**

---
---
**Anthony Bolgar** *March 19, 2018 17:26*

If I remember correctly there is a jumper wire that needs to be added to the driver to remap on of the pins that is not in the standard location. Ray has the info somewhere.


---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2018 19:17*

Could be a short somewhere with the TMC.  Don't power up again.  Show pictures of it and how it's plugged into the board. 


---
**Brian Albers** *March 19, 2018 19:29*

I will post pictures when I get home. I matched up the step/dir pins on the both boards and the board had the one pin removed for use with a ramps board outlined in this post:

[https://www.google.com/amp/www.instructables.com/id/Install-and-configure-SilentStepStick-in-RAMPS-TMC/%3famp_page=true](https://www.google.com/amp/www.instructables.com/id/Install-and-configure-SilentStepStick-in-RAMPS-TMC/%3famp_page=true)



However I'm seeing the note that that pins removal is no longer necessary. Have 2 backup tmc2100s that haven't had the pin removed that I can try as well.


---
**Jim Fong** *March 19, 2018 19:41*

I’m using some tmc2208’s in my k40 for a few months now. Good ones from digikey. The cheap eBay one died early on. 


---
**Brian Albers** *March 19, 2018 19:59*

These were admitted purchased off eBay, found some real tmc2100s on digikey, I'll order those and try.  The drv8825 drivers even at 1/32 microstep sound like shit on the accel/decal.  Whole assembly groans, not sure if that's just gantry design or what.


---
*Imported from [Google+](https://plus.google.com/109986797182285961346/posts/L92Hr9kWxa9) &mdash; content and formatting may not be reliable*
