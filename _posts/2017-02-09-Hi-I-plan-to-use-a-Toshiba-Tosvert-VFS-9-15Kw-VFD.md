---
layout: post
title: "Hi, I plan to use a Toshiba Tosvert VFS-9 1.5Kw VFD"
date: February 09, 2017 09:37
category: "CNC"
author: "\u00d8yvind Amundsen"
---
Hi, I plan to use a Toshiba Tosvert VFS-9 1.5Kw VFD. Anyone tested this in combination with Smoothie? The manual says there is an RS232c interface - possible to connect this to the Remix rs232?

![images/0c505bbf0bd274461ffc8f6ab8d8dd9d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0c505bbf0bd274461ffc8f6ab8d8dd9d.jpeg)



**"\u00d8yvind Amundsen"**

---
---
**Stephane Buisson** *February 09, 2017 13:51*

[http://smoothieware.org/spindle-control](http://smoothieware.org/spindle-control)

[smoothieware.org - spindle-module [Smoothieware]](http://smoothieware.org/spindle-module)



also search youtube with "Bouni VFD Smoothie" for several videos.


---
**Jim Fong** *February 09, 2017 19:28*

I'm pretty sure that VFD supports Modbus. That is really the best way to communicate.  The smoothie firmware does support modbus but I haven't tried it out yet. I have a VFD/spindle on my cnc but I use a different controller over modbus. 



Do make sure the parameters are setup correct for your spindle. Wrong settings can burn it out really fast. I've seen postings on cnczone were spindles were toasted in one minute by completely wrong settings. 


---
*Imported from [Google+](https://plus.google.com/102309741047174439430/posts/VFqUCr16XyT) &mdash; content and formatting may not be reliable*
