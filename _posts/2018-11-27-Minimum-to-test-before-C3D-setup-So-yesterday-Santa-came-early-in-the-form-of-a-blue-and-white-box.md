---
layout: post
title: "\"Minimum\" to test before C3D setup? So yesterday, Santa came early in the form of a blue-and-white box"
date: November 27, 2018 22:07
category: "K40 and other Lasers"
author: "Richard Perry"
---
"Minimum" to test before C3D setup?



So yesterday, Santa came early in the form of a blue-and-white box.



I've been reading a ton on line (here and other places) and ordered a Mini upgrade bundle the same time I ordered the laser. (It's not arrived yet.)



I would like to get the laser setup and run a quick test so that I can provide feedback to the shipper (or identify a problem).... but I'm not really wanting to fully install software etc knowing that I'm about to tear things apart to install a new controller board, install Lightburn, etc. Plus.... Chinese software, etc etc.



Can folks recommend a "quickie" test that should tell me if the unit is DOA without the installation of software? I'm thinking of doing the following:



1) Physical inspection - packing materials/etc, gantry, cracks in tube

2) Electrical/Ground check (grounding lug to neutral, loose wires, etc)

3) Temporary water system and exhaust hookup

4) Painter's tape on "Mirror 1"

4) Turn on/plug in, see if machine "homes"/limitswitches work

5) Laser switch on

6) Set power to "5%" (I have the digital display)

7) Pulse "Laser Test Switch"

8) See if there's a burn mark on the tape.

9) Shut things down, drain water, wait for postman to deliver C3D board.



That should work, right? Suggestions/changes would be appreciated. Again, this is only to check that the mechanicals of the machine work so that I don't have to start a claim with the eBay vendor.



Thanks in advance, looking forward to learning much from all of you...



Richard Perry





**"Richard Perry"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 22:32*

Hey, I just watched mine home on power up and test fired, then took the stock board out and put the first C3D prototype board in some 2 years ago.  I was lucky. 


---
**Joe Alexander** *November 28, 2018 09:37*

For #2 make sure you keep the chassis ground to earth ground in your house. yes most houses (in the US) have ground tied to neutral at the distribution box, but i'd venture to say its safer and less potentially problematic to keep out any other tie points.again assuming your house also has a proper ground line. Worst case I'd tie a local grounding rod to the chassis if your house ground was insufficient(rarely the case).

     Also you could probably get away with doing an alignment of your beam while waiting for the new controller board to come in.


---
*Imported from [Google+](https://plus.google.com/115442261504955032163/posts/7xypFixyj65) &mdash; content and formatting may not be reliable*
