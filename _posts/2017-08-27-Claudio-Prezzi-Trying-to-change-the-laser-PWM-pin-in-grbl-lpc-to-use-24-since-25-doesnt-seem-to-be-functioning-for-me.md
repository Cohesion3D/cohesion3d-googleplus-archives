---
layout: post
title: "Claudio Prezzi Trying to change the laser PWM pin in grbl-lpc to use 2.4, since 2.5 doesn't seem to be functioning for me"
date: August 27, 2017 18:23
category: "FirmWare and Config."
author: "Robin Sieders"
---
**+Claudio Prezzi**​ Trying to change the laser PWM pin in grbl-lpc to use 2.4, since 2.5 doesn't seem to be functioning for me. Changed the mapping to channel 5 in cpu_mapping.h, and it builds fine, with no errors. When I plug it back into my cohesion3d mini, I get the top two lights flashing green back and forth once, then nothing. Trying to connect through lw4 tells me no firmware detected, and connecting via PuTTy just hangs until it eventually kicks me out. Any suggestions you can offer, if you have time?



Thanks





**"Robin Sieders"**

---
---
**Claudio Prezzi** *August 27, 2017 19:32*

Which version of grbl-LPC did you use?


---
**Claudio Prezzi** *August 27, 2017 19:33*

Try my version from [https://github.com/cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC). There you have easy compile switches in config.h to select the board and machine.

[github.com - cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)


---
**Claudio Prezzi** *August 27, 2017 19:36*

You still can change specific settings in cpu-map.h and  defaults.h


---
**Robin Sieders** *August 27, 2017 19:39*

Will do, thanks. I had cloned your repository from the master branch to pull down the CMSIS folders/files, as well as the beta07_4 axes source files. I built in both the beta, and master branch folders with the same results, but I'll go through config.h and see what I missed.


---
**Claudio Prezzi** *August 27, 2017 20:02*

To change the PWM output from pin 2.5 to 2.4 for a Cohesion3d Mini you need to change line 437 in cpu_map.h to PWM1_CH5. (see [https://github.com/cprezzi/grbl-LPC/blob/master/grbl/cpu_map.h#L437](https://github.com/cprezzi/grbl-LPC/blob/master/grbl/cpu_map.h#L437))

[github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/blob/master/grbl/cpu_map.h#L437section)


---
**Claudio Prezzi** *August 27, 2017 20:04*

In config.h you should only need to select the right board and machine (starting at line 35), nothing else.


---
**Robin Sieders** *August 27, 2017 20:09*

Thanks, it built fine this time, copied the settings I wanted into config.h like it mentions in the comments at the bottom. Still no laser firing though, so will keep troubleshooting.


---
**Robin Sieders** *August 27, 2017 20:12*

I think I missed the line 35 portion in config.h going to go back and look at that. 


---
**Robin Sieders** *August 27, 2017 20:15*

Oh, I guess copying the definition from default.h and cpu_map.h into config.h, then commenting out those lines would achieve the same result anyways.


---
**Claudio Prezzi** *August 27, 2017 20:34*

Probably yes, but having the config in cpu_map.h and defaults.h is much more convenient.


---
**tyler hallberg** *August 27, 2017 22:40*

watching, same problem I am having with the 2.5 not doing anything for me


---
**Robin Sieders** *August 27, 2017 23:26*

I swapped back to smoothie for the time being, although oddly enough, 2.5 is working for me now in smoothie, but 2.4 isn't


---
**tyler hallberg** *August 28, 2017 05:43*

I got my GRBL to run ok, not perfect, but slowly changing things to get better results


---
*Imported from [Google+](https://plus.google.com/+RobinSieders/posts/YcrNEh1s1zg) &mdash; content and formatting may not be reliable*
