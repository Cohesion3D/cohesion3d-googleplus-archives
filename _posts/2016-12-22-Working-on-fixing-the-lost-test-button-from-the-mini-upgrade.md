---
layout: post
title: "Working on fixing the lost test button from the mini upgrade"
date: December 22, 2016 02:39
category: "General Discussion"
author: "Kelly S"
---
Working on fixing the lost test button from the mini upgrade.  Have the board working fine standalone and just have to fix the pwm wiring and should be good to go.  :)  Edit: Old test panel is in and working, have a test button with variable power control again.

![images/7bdef0d8ac7eb77d6eba0d1fcc7b7f5d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7bdef0d8ac7eb77d6eba0d1fcc7b7f5d.jpeg)



**"Kelly S"**

---
---
**Kelly S** *December 22, 2016 02:46*

The original test button had a laser kill switch on it, that will no longer work when the unit is running as pwm is being fed from two separate sources. the C3D mini and this little test panel.  In case of emergency situations the kill switch will still be required to shut down quickly.  But I will be happy to have a test button again.  :)


---
**Kelly S** *December 22, 2016 03:11*

Small update.  Dont work?  very odd the wire harness that plugs into that board goes directly to the PSU and had no interaction with the original k40 mini board.  


---
**Kelly S** *December 22, 2016 03:26*

Got it figured out!  Laser switch has to be off for the board to send the signal.  woot have test fire again!  Now to figure out how to dangle it till I expand the cutting area and make a new panel.


---
**Kelly S** *January 28, 2017 00:19*

Update to this, when it is connected it takes over regulating the power like it did before the upgrade, only solution is to unplug it.  Will try to get a 4 pole toggle switch for it for the future. 


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/ZQeFg43ju44) &mdash; content and formatting may not be reliable*
