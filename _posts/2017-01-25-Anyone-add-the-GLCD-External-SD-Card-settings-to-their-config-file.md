---
layout: post
title: "Anyone add the GLCD External SD Card settings to their config file?"
date: January 25, 2017 15:24
category: "General Discussion"
author: "Bill Keeter"
---
Anyone add the GLCD External SD Card settings to their config file? Any issues? Will I be able to cut with a loaded LW file on it?



It wasn't included in the default C3D config so just want to make sure there wasn't a reason.





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 15:49*

That's not supported and isn't even wired up so it won't work on the Mini. You are best using the MicroSD card already on the Mini. 

Remember that you can also copy files to that card by plugging the board in over USB. 


---
**Bill Keeter** *January 25, 2017 15:59*

well nuts. That's one of my favorite features on my UM2 clone. Just walk over to the 3d printer with a SD card. Load file and print. Don't need to walk my laptop over or anything.



Hmmm.. maybe i'll setup LaserWeb on a RPi with an attached  7" touch screen. Sounds like there's a few others using a RPi.


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 16:00*

Once again, Bill, you can still do this, but with the MicroSD card that sits in the Mini. And that doesn't need any additional config. 


---
**Don Kleinschnitz Jr.** *January 26, 2017 14:21*

**+Ray Kholodovsky** can you load and modify a .gcd file on the SD card from Ethernet?


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/SsRfdKHYX5U) &mdash; content and formatting may not be reliable*
