---
layout: post
title: "Laser isn't actually turning on - any thoughts?"
date: March 01, 2018 01:22
category: "C3D Mini Support"
author: "Gary Helriegel"
---
Laser isn't actually turning on - any thoughts? 



Bought a used K40 with a Cohesion3D Mini with GLCD installed. System powers up, controller boots correctly, and Laserweb (theoretically configured correctly, according to [https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration](https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration)) is able to move axes around and do dry runs of various cut files. When attempting to cut a file, the Laser line on the GLCD varies appropriately- when idle, it usually displays 'Laser S0.8000/0.00%', when the controller thinks it's cutting it displays, as an example, 'Laser S0.2400/25.00%', so I know the controller believes that it's turning on the laser. It flickers to S0.8/50.00% for a moment when I hit the laser test in Laserweb. The laser LED on the power supply doesn't light up at all when running a job.



Nothing I've done seems to be able to actually power the laser other than hitting the onboard 'test' button on the power supply. This causes the laser LED to light up, and the laser turns on and burns/cuts as long as I hold the button down. The wired-in test switch does not work, only the one on the power supply PCB.



I'll get some images of the wiring on the board when I get home. 



The guy I bought it from is coming by tomorrow to help troubleshoot, but is there anything that I can look at beforehand to try and identify the problem? I've checked connections, and everything is seated properly, and looks like it matches the installation instructions.





**"Gary Helriegel"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 01:24*

Just send a G1 X10 S0.6 F600 line and the head should move while firing. 



Also try Lightburn software at some point. 


---
**Don Kleinschnitz Jr.** *March 01, 2018 01:31*

If your wired in test button does not work I suspect you have something wrong with your enable loops. Exactly what that may be depends on your LPS. 

Post a picture of the supply's input connectors and I can help you troubleshoot it.



Here is a table that may help depending on your supply type.

My guess is that K+ or P+ is not enabled i.e at ground.



If you have a supply with T and H pins then we will need a different diagram.



![images/7ad4419c878cdbe33ab4df780245675e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7ad4419c878cdbe33ab4df780245675e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 01:33*

Don, I read that as the test fire button <b>does</b> work. 


---
**Gary Helriegel** *March 01, 2018 01:38*

There is a wired 'test fire' button connected to the original square red switch, and there's also a tiny one directly on the power supply. The wired switch does not work, the PCB button does work.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 01:39*

Ah, I see. In that case, Don is the guy and what he says now makes sense. 


---
**Gary Helriegel** *March 02, 2018 01:33*

Thanks for the diagram- that's helping troubleshoot, at least. Turns out what I thought was the laser test button was actually the laser power. Unfortunately, that wasn't the entire issue, as the switch itself is good and it's properly returning 5v when open and 0v when closed, but the laser will not fire via the board in either position.



I was looking at the contacts in the connectors under the thought that maybe something was badly crimped or knocked loose during transport after I bought it, and after bringing the main power connecter to the laser into the office, a coworker noticed a tiny little spur off of one of the wires which in the right position would have shorted to the wire next to it. This is in the end connecting to the Cohesion3d board. The pinout on the website (the 'K40 Power' plug, [https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)) shows the two wires as Ground and NC (with the other, unaffected wires being +24v and L). Is it possible  that when shorted together, it's causing unexpected behavior in the board or power supply? I'll be recrimping all four connections tonight.



Picture is badly focused, but shows the strand of wire crossing above the plastic housing.









![images/8f76e2ccbcdb008a8b4dcdbf222e7585.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8f76e2ccbcdb008a8b4dcdbf222e7585.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 02, 2018 01:34*

You can hook up 24v, Gnd, and L to screw terminals as per the last pic in the install guide. 


---
**Gary Helriegel** *March 02, 2018 10:16*

**+Don Kleinschnitz** Thanks again for the pinout with explanations- that got me what I needed. Turns out the L terminal on the connector was a little worn, and wasn't making good contact in addition to the possible grounding issue. Recrimped all and laser now works as expected.


---
*Imported from [Google+](https://plus.google.com/118018140785058364487/posts/6KiRE3hXk7W) &mdash; content and formatting may not be reliable*
