---
layout: post
title: "I am going to be upgrading to the Cohesion3D mini today..."
date: July 03, 2017 13:58
category: "Laser Web"
author: "Tammy Mink"
---
I am going to be upgrading to the Cohesion3D mini today... and I've gotten LaserWeb4 installed and I downloaded the machine config files from the Cohesion3D site.  My simple question is where do I put the files so LaserWeb sees them?





**"Tammy Mink"**

---
---
**Ariel Yahni (UniKpty)** *July 03, 2017 14:01*

**+Tammy Mink**​ you load them into LW as you would do on any other application. 


---
**Ray Kholodovsky (Cohesion3D)** *July 03, 2017 14:08*

Wait... we don't have any machine config files for LW posted. 

Are you referring to the Dropbox link with the config and firmware file? That's for the MicroSD Card of the board, and should already be pre loaded. 


---
**Tammy Mink** *July 03, 2017 14:51*

Oh ok. Thanks for the clarification.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/Z7Qpt8E29KD) &mdash; content and formatting may not be reliable*
