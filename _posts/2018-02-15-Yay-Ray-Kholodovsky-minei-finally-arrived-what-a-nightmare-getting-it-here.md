---
layout: post
title: "Yay Ray Kholodovsky minei finally arrived - what a nightmare getting it here"
date: February 15, 2018 00:46
category: "K40 and other Lasers"
author: "Richard Vowles"
---
Yay **+Ray Kholodovsky** minei finally arrived - what a nightmare getting it here.







![images/b6272ae29a3f3a88442eb767aa08d969.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6272ae29a3f3a88442eb767aa08d969.jpeg)
![images/21d5103ce14c4a48036edbd08293f14e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/21d5103ce14c4a48036edbd08293f14e.jpeg)

**"Richard Vowles"**

---
---
**Richard Vowles** *February 15, 2018 00:52*

Note the firmware date on  that board!


---
**Ray Kholodovsky (Cohesion3D)** *February 15, 2018 01:41*

Just had a customer make a similar comment on fb about the datecode on their tube. 

I'm convinced they write them months in the future. 


---
**Richard Vowles** *February 15, 2018 01:43*

It shipped on 2017-12-22. 


---
**Richard Vowles** *February 18, 2018 07:22*

So I managed to get it installed, i'm not entirely convinced its working properly in terms of X/Y - home seems to be back /left. Laser also not firing. It has an opaque lid - so I can't see what is going on inside without my CO2 laser goggles and everyone else out of the room, which will mean it will have to wait until later :-(


---
**Richard Vowles** *February 18, 2018 09:14*

**+Ray Kholodovsky** sorry I'm going to reach out here. The x is homing but not the y. The two red cables are together as per your diagram but my yellow and white are in different places. As such my table is unusable. 


---
**Richard Vowles** *February 18, 2018 09:15*

![images/7971cf0d4c7e9ec4636a378fcc08d2da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7971cf0d4c7e9ec4636a378fcc08d2da.jpeg)


---
**Richard Vowles** *February 18, 2018 09:15*

![images/b5546e4bf739c7ad2e0bbe9b6b55dfc3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b5546e4bf739c7ad2e0bbe9b6b55dfc3.jpeg)


---
**Richard Vowles** *February 18, 2018 09:16*

![images/e19e989f6debf394031b624d61ee0000.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e19e989f6debf394031b624d61ee0000.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2018 14:46*

"Not homing" - more details? Can you jog the Y?  


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2018 16:50*

I figured it out.  Your endstop wiring is slightly different. 



Do this: [https://plus.google.com/u/0/+RayKholodovsky/posts/1sBj7vYtxgq](https://plus.google.com/u/0/+RayKholodovsky/posts/1sBj7vYtxgq)


---
**Richard Vowles** *February 19, 2018 05:19*

Ok - managed to get back to this today and I put a 2-wire JST on the board, connected to a terminal block and wired that extra pin in. I can jog around, but LlightBurn is moving the <i>wrong way</i> when homing and keeps smacking the head into the front of the frame. Nothing I can do seems to tell it go to to the BACK where the endstop actually is - it seems to ignore the setting that tells you where the endstops are located. 



It also won't save them when you change them, i have to delete and re-add the board. So I'm stuck again... Is it possible that the Y is around the wrong way? I can't imagine that is the case, its a stepper driver.


---
**Richard Vowles** *February 19, 2018 05:20*

Oh - and when I set the "home" to back/left, the jog buttons jog the right way (up goes back, down goes front), it just simply won't home to the back. Setting it front/left makes the jog buttons go the wrong way.


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 05:23*

The origin in Lightburn should be front left. You need to flip your y motor cable, it's backwards. 


---
**Richard Vowles** *February 19, 2018 06:02*

I thought it had to be, but the wiring on your site shows the two reds together so I blindly followed :-)



I have swapped it around and now it homes properly!





My next step is now I cannot get the laser to fire with the software. I make a shape, its a Cut path, and press Start and it makes the movements but never fires the laser. Not even a smidgen of a mark - Test fire works fine however. AFAIK I cannot get that cable in the wrong way around, so any further ideas...?


---
**Richard Vowles** *February 19, 2018 09:20*

I spent some more time trying to adjust the current indication (2% all the way to 82%), changing light burn from 10% - 100%, but I just can't get anything to happen with the laser itself. All the movement seems fine. 


---
**Richard Vowles** *February 19, 2018 17:19*

Ok, going to assume I've burnt through my goodwill here, so I'll put the old board back in and at least see if I can get it working with that. 


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 18:00*

Just disconnect from board and touch L to ground and see if the laser fires that way. That would be all the board is doing. 


---
**Richard Vowles** *February 19, 2018 18:17*

Yep. That totally works. I'll go look up the gcode commands in smoothie to turn it on and off and see if that works. If it doesn't then maybe my config (from 2017-12-22) is wrong, if it does I'll try laserweb and see if that works instead. Now I know the laser actually works and is controllable via all the pins I'm a lot less worried, I only have 5 more days before purchase protection runs out.


---
**Richard Vowles** *February 19, 2018 18:18*

Oh, and a super thanks for your support, you have been utterly awesome!


---
**Richard Vowles** *February 19, 2018 22:23*

So I found the GCODE in the Smoothie documentation - "FIRE %" and "FIRE OFF". Neither of these do anything - they indicate they are but they do not engage the laser. I see bother LaserWeb and LightBurn output the 50.27 (when I set it to 27% power - I just wanted something I could easily find), but again, they don't engage the laser. The GND -> L definitely works. Is it possible I have a faulty pin on the board? should/could I change the config and wire it to a different pin? The smoothie conversation is: 



WARNING: Firing laser at 80.00% power, entering manual mode use fire off to return to auto mode

ok

turning laser off and returning to auto mode

ok




---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 22:27*

What you need to do is send this line G1 X10 F600 S0.6 - the head should move while firing.



If you already have the L wire out, what you need to do is connect to the 4th screw terminal from the left along the bottom row. 


---
**Richard Vowles** *February 19, 2018 22:36*

That command moved but didn't fire the laser. So you mean the P2.5/BED terminal?


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 22:37*

That is the one yes.  

In this case we should try an alternate pin on the C3D.  But first, please try replacing the files on the memory card with fresh ones from the bottom of the instructions. 


---
**Richard Vowles** *February 19, 2018 23:03*

BOOM!



Perfect! That worked!!!! Thank you so much! I now have a fully functional laser!!!


---
**Denys Fedoryshchenko** *February 21, 2018 15:19*

**+Richard Vowles** "I thought it had to be, but the wiring on your site shows the two reds together so I blindly followed :-)"

I had same issue. I suggest, if **+Ray Kholodovsky** can mention it in /start manual.


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 15:20*

Chinese wiring has no "standard", the technician uses whatever color wire he wants on that day, instead pay attention to the orientation of the plug. 


---
*Imported from [Google+](https://plus.google.com/+RichardVowles/posts/EWsJii7FVT7) &mdash; content and formatting may not be reliable*
