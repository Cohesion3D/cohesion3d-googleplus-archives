---
layout: post
title: "Is there a way to control the a axis for a rotary?"
date: July 31, 2017 00:39
category: "C3D Mini Support"
author: "John Austin"
---
Is there a way to control the a axis for a rotary? could not find a way to jog it.





**"John Austin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 31, 2017 01:31*

Rotary needs  a special firmware build.  Then the A axis jogging should show up in LaserWeb. 


---
**John Austin** *July 31, 2017 01:34*

Do i have to have someone to create it or is that something updating laserweb does? Sorry all of the questions buy I find ally got back to try and get the board working g


---
**Ashley M. Kirchner [Norym]** *July 31, 2017 07:44*

This is something that you can do yourself. You can follow the instructions from the Smoothie web site here: [smoothieware.org - compiling-smoothie [Smoothieware]](http://smoothieware.org/compiling-smoothie)



The compile command you will need is:

make clean ; make AXIS=4 CNC=1 (the web site instructs you to type 'make clean all' however that just compiles the default firmware. So use the above command.)



Let me know if you get stuck.


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/E7DpdtQu8kU) &mdash; content and formatting may not be reliable*
