---
layout: post
title: "Hello everyone. I'm a member of the EcoFabLab Brugge in Brugge ( Belgium )"
date: December 30, 2017 17:03
category: "General Discussion"
author: "Philippe Guilini"
---
Hello everyone.

 I'm a member of the EcoFabLab Brugge in Brugge ( Belgium ). We have a broken down Full Spectrum 4th generation hobby laser. Controller and psu were sent to Full Spectrum. After a long, long,long wait, they send us an email telling us that they were still unable to get the board working, but that they would continue trying. If they could'nt repair the board, they wanted us to ship the complete laser to them, so that they could upgrade the laser. That would cost us 2500 euro without two times shipping and handling ! ! 

We are all volunteers and work with very limited crowd funding.

My question is : has anyone from this community adapted a cohesion3d board to a FS 4th generation laser ? We now  have a big bunch of cables with connectors that don't seem to fit to a cohesion board ( from looking at the pictures that is ).

Thanks for helping us out and friendly regards from,

Philippe.





**"Philippe Guilini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 17:43*

Does it look like this? [Cohesion3D](https://plus.google.com/u/0/communities/116261877707124667493/s/Ben%20Delarre)



**+Ben Delarre** did it, I linked the search for him in this group above, should show 3 threads with pictures. 



Fundamentally we just need to drive 2 motors, read some endstop switches, and pulse the L line to fire the laser.  Not that hard even if you need to recrimp/ cut some wires to make it happen. 



Best if you can show pictures of your machine and wiring. 


---
**Ben Delarre** *December 30, 2017 19:21*

Yes it took me a couple of hours total. I didn't even get any new plugs in the end, just depinned the ones the fsl had already and chopped them down to fit.



I also removed the plate the original board was mounted to and drilled new mount points for the cohesion board.



All in all a very painless upgrade and all the buttons on the top of the machine still work.


---
**Philippe Guilini** *December 31, 2017 13:57*

**+Ray Kholodovsky** Hi, thank you for your reply. Ben Delarre also responded. Next tuesday is our weekly encounter in the fablab. I'll try to make some pictures from the actual machine and the "spaghetti" of cables an wiring.  :-)




---
**Philippe Guilini** *December 31, 2017 13:59*

**+Ben Delarre** Hi Ben, thank you for your reply. Next tuesday is our weekly encounter in the fablab. I'll try to make some pictures from the actual machine and the "spaghetti" of cables an wiring. :-).  I suppose the first thing to do, is to find out where every wire originated .


---
**Philippe Guilini** *January 03, 2018 09:54*

Hello everyone,



Yesterday evening, one of our members had a smoothieboard he didn't use anymore. We ended up by using this board and succeeded  in controlling the motors of the laser. 

This means that we won't be using a cohesion board after all.

But I did wish to thank you people for your efforts to help us out. We all really appreciated this ! So, sending you all a big "thank you" is the least we could do.



Kind regards,



Philippe.

 


---
**Pete Prodoehl** *April 14, 2018 02:12*

I've got a Full Spectrum 4th generation hobby laser as well, and I'm considering the Cohesion3D board. How has it been working so far? What software are you using? (I'm totally comfortable doing all the rewiring, soldering, etc.)


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2018 02:25*

Software we recommend is Lightburn. 



Looks like you've already found the relevant thread for doing all the rewiring :) 


---
**Pete Prodoehl** *April 14, 2018 13:06*

**+Ray Kholodovsky** Yes, thank you! I'll be ordering soon!




---
**Philippe Guilini** *April 15, 2018 09:45*

Hey Pete,

We've made the changes to a smoothie board and we are using lightburn. This

software is by far the best to work with !

Grtz,

Philippe


---
**Pete Prodoehl** *April 15, 2018 12:32*

Thanks, Philippe! Mind if I bother you with questions if I get stuck?




---
**Philippe Guilini** *April 16, 2018 10:24*

No problemo ! If we can help, we will surely do so.




---
*Imported from [Google+](https://plus.google.com/109415938778659747209/posts/MyKx4xwde9Z) &mdash; content and formatting may not be reliable*
