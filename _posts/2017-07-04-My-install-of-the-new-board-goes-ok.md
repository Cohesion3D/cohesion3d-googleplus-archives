---
layout: post
title: "My install of the new board goes ok"
date: July 04, 2017 00:38
category: "C3D Mini Support"
author: "Tammy Mink"
---
My install of the new board goes ok. A few minor issues. My endstops don't seem to register. Also my coodinates (maybe side effect of endstops issue?) seem wonky on the GLCD...the top left reads as X 0 and Y 290. LaserWeb seems confused about the coordinates too. I'm sure I'm just missing something.. thanks.





**"Tammy Mink"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 00:40*

If you issue a G28.2 does the head move to the rear left? 

The homing touches off twice so if it doesn't appear to stop right away. 


---
**Tammy Mink** *July 04, 2017 11:41*

One thing I did have to do was replacement of that cheap blue USB as I it stopped  communication during a short test job and I learned from here how bad that cable it. As far as if I put G28.2. it goes to the top left and grinds like crazy. A side question. If I set the laser power on the machine to itself 50% and to 50% in LaserWeb is the laser working at 25% or 50%?


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 14:34*

Ok, can you get Pronterface please (just get the binary for your OS, unzip, and it runs from the folder) and issue an M119 command? It will report the endstop states, I would like to confirm what we are reading when the head is clear (in the middle), when it is to the far left, and when it is to the far back. 



So ideally you would set the pot/ digital panel at 10-13mA. Then you would use the % in LaserWeb to set a proportion of that. There is one exception - I limited the max to 80%  in the config.txt file on the board memory card. You can find that line - laser max power 0.8 and set it to 1 to remove this. 


---
**Tammy Mink** *July 04, 2017 16:36*

As a note my endstop problem could be my fault. My K40 had all sorts if issues and I was chopping off connections and redoing the connections but I only had 2 terminal plugs and lined them up not using one. First off I see your connections go opposite my old board did (which would not be a problem if I didn't chop stuff). While the endstops worked on the old board I think it best I wait for my proper connectors but I'm the meantime I can get pronterface and see what it says. :)




---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 16:37*

Let's see pictures of your board/ wiring please. 


---
**Tammy Mink** *July 04, 2017 16:56*

Here is what code I got from M119  

X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1




---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 18:29*

Tammy, what we need is an M119 for each case:

-when the head is clear (in the middle), 

-when it is to the far left, 

-when it is to the far back. 



You can just check for the value to be 1 when each respective switch is pressed for X Min and Y Max. 


---
**Tammy Mink** *July 04, 2017 19:30*

Clear:SENDING:M119

X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1



Far Left: SENDING:M119

X_min:0 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1



Far Back: SENDING:M119

X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1



Far Left and Far Back: SENDING:M119

X_min:0 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 19:32*

Ok, it looks like your X endstop is in the Y port, and your Y endstop is not being read. 

Pictures of your board and wiring please? 


---
**Tammy Mink** *July 04, 2017 19:34*

 I am also having an issue with it stopping in the middle of a lasering. It just did it twice...LaserWeb disconnects from the machine. I already have replaced the usb cable and made sure it wasn't on a USB hub. Seems to get a random amount through the job.




---
**Tammy Mink** *July 04, 2017 20:02*

Also I was going to temporarily try and bypass the USB problem to finish an item using the GLCD but can't get it going. I exported the gcode, saved it to the microsd (same one that houses the firmware and config files) . On the GLCD I go to Play and it says SD\.... and nothing is there  I think that what I'm supposed to do but I'm unsure...from what I read we need to use the minisd as opposed to the SD card slot. I might be doing something wrong. 


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 20:12*

Pictures of your board and wiring please. 


---
**Tammy Mink** *July 04, 2017 20:20*

it's kind of ugly but on the endstop wires from top to bottom (starting from second down) go white wire with long dash, white cord with an x, then both the short dash and short wire with an x connected to one (these two were originally together so I left the pairing that way).  The last and bottom one is unconnected to anything  

![images/51f9a9c233a50c7203a0e0fbcbdd0961.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/51f9a9c233a50c7203a0e0fbcbdd0961.jpeg)


---
**Tammy Mink** *July 04, 2017 20:26*

As for the can't play from the GLCD here is two related pictures. 

![images/7143a413a80598f10c6ae5a06e84430c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7143a413a80598f10c6ae5a06e84430c.jpeg)


---
**Tammy Mink** *July 04, 2017 20:26*

![images/641a8f1ac89aa9aa05308cd79e1fe0a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/641a8f1ac89aa9aa05308cd79e1fe0a7.jpeg)


---
**Tammy Mink** *July 04, 2017 20:27*

I have no screenshots for it disconnected in the middle of jobs... was not sure it would be helpful there.


---
**Tammy Mink** *July 04, 2017 21:40*

I made progress on the playing gcode from the minisd...I deleted the gcode...rebooted the LCD via pulling the connections...it did it's long beep... then I added the gcode file and it saw the file now


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 21:43*

Well, first off let's fix your endstops.



Per the pinout diagram: [cohesion3d.freshdesk.com - Cohesion3D Mini Pinout Diagram : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)



Please attach each connector to the Sig and Gnd of the respective "X-MIN" and "Y-MIN" connectors at the bottom edge of the board. 



Please do all wiring and stuff with power off and usb disconnected. 



Then, M119 should register a 1 when the switch is pressed.



You're welcome to attach a pic for me to confirm the wiring if you'd like. 


---
**Tammy Mink** *July 04, 2017 23:19*

I think that's how I have it wired now (in theroy) but I'll take another stab at it, plus tomorrow the proper connectors should arrive which should help. A side note, right now I'm running a larger job from the GLCD and so far so good.


---
**Ray Kholodovsky (Cohesion3D)** *July 04, 2017 23:26*

Per your picture, you have endstops connected to the 5 pin "k40 endstops" connector on the left. I am saying to move to the ones along the bottom edge "endstops". 


---
**Tammy Mink** *July 05, 2017 22:56*

Here are the numbers I am getting currently after playing with the wires more.. both directions still grinding against the endswitch. 

Center, no endswitches: X_min:0 Y_max:0 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1

Both Endswitches: X_min:1 Y_max:1 Z_min:1 pins- (X)P1.24:1 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1

Top Endswitch:X_min:0 Y_max:1 Z_min:1 pins- (X)P1.24:0 (X)P1.25:1 (Y)P1.26:1 (Z)P1.28:1 (Z)P1.29:1

Left Endswitch:X_min:1 Y_max:0 Z_min:1 pins- (X)P1.24:1 (X)P1.25:1 (Y)P1.26:0 (Z)P1.28:1 (Z)P1.29:1




---
**Ray Kholodovsky (Cohesion3D)** *July 05, 2017 23:41*

Perfect. It's reading both. 


---
**Tammy Mink** *July 06, 2017 00:14*

Ok so what is the next step ...it tries to keep going even if an endstops is engaged.


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 00:22*

Usually, when people think this, it's just moving back slowly, will touch the switch a 2nd time, and then stop. Just give it an extra moment. 



If this is not the case, then please take a video of the situation.  


---
**Tammy Mink** *July 06, 2017 00:50*

Still not acting like they are working (they worked before the switch over). Here's the video 
{% include youtubePlayer.html id="VCBNuG-KdN0" %}
[youtube.com - Endstops not working](https://youtu.be/VCBNuG-KdN0) . I know my x-axis is grinding a bit in the video but I took a few takes and to I need to go and recalibrate the belt after all that grinding against the left.


---
**Tammy Mink** *July 06, 2017 00:52*

While it's unlikely both endstops died at the same time at the switch over, I do have 2 new endstops I ordered as a "just in case" part if you want me to try them.


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 01:01*

Ok yep that's grinding. But we determined that the board is reading the switches using the M119's above... Strange. 



You're sending a G28.2 command? 


---
**Tammy Mink** *July 06, 2017 01:11*

I am in LaserWeb moving the axis by 100mm


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 01:14*

Well that's why it's doing that! Just send a G28.2 to home. 


---
**Tammy Mink** *July 06, 2017 01:18*

The G28.2 command seems to properly stop movement it seems. From the GLCD, LaserWeb, and pronterface it does not. Also if I manually press the endstop while it's moving in the direction of one it still keeps going like the endstop isn't hit. 


---
**Tammy Mink** *July 06, 2017 01:20*

As a side note before the upgrade whenever I turned the machine on it would home itself... and it doesn't do it anymore. Not sure if it should or not.


---
**Tammy Mink** *July 06, 2017 01:28*

So will the endstops not work when using pronterface, LaserWeb and the GLCD when it's moving the ganty normally? 


---
**Tammy Mink** *July 07, 2017 01:40*

Anything else I can do about the endstops ? Any idea how I can get LaserWeb to stop disconnecting , for now I bought yet another USB cable in case it helps.


---
**Ray Kholodovsky (Cohesion3D)** *July 07, 2017 02:18*

Hi Tammy, 



There are a few different things going on here:



1.  Jogging and Homing are not the same thing.  Jogging is a dumb command of "blindly move how far I told you to" and it is not seeking the switches at all.  Homing (G28.2) is when it is specifically moving until it hits the switches so that it knows where 0,0 and in this case 0,200 is.  



2. Most people are using G28.2 at the beginning of their job to find the corner of the machine and that is that, the rest of the job happens within the confines of the work area because you would not position your design in negative space in LW.  



3.  It sounds like what you are asking for is a "when jogging, do not let the axis grind past the switches".  We should be able to set this up in the config.txt, the relevant stuff is here: [smoothieware.org - endstops [Smoothieware]](http://smoothieware.org/endstops#limit-switches)



You would remove the # to uncomment and set to true those lines for alpha and beta.  You could also add your 2 additional switches to the other ends of the X and Y axes, we have 2 more "full" endstop ports on the Mini labelled Z MIN and Z MAX that can be used and configured for the Xmax and Ymin endstops that you might add. 



4. If you'd like the machine to home itself on power up,  you can create  a file: on_boot.gcode with the contents G28.2 and it will run this on every power up/ board reset.  But by default the board will not do anything unless it is commanded to.  User's choice whether they want to set up auto homing on power up/ reset.



Regarding disconnects, could you provide more information: what is the computer you are using, the OS, a screenshot of the job set up in LW....  



Thanks!


---
**Tammy Mink** *July 07, 2017 10:14*

Thanks for the info on how to tweek the endstop system. For the disconnect, I'll soon run the long job again get screenshot and other info all together etc. I have not had the issue since I switched to a second newer USB but I've not run a job that takes more then 10 mins. 


---
**Ray Kholodovsky (Cohesion3D)** *July 07, 2017 11:59*

The official statement is to use a shielded USB cable with ferrites on both ends for maximum noise reduction, etc. 


---
**Tammy Mink** *July 07, 2017 12:25*

Ok I'll go buy another cable if this cable fails which now I'm assuming it will since no ferrites. I just had bought one that said shielded from Amazon but I don't see the ferrites on it so guess time to go shop lol


---
**Ray Kholodovsky (Cohesion3D)** *July 07, 2017 12:27*

Right, that might help, I was just saying it's high up on smoothie's list of ways to mitigate noise/ interference.


---
**Tammy Mink** *July 07, 2017 12:31*

Ok I went and ordered this USB cable. Sounds like it meets the criteria,  I'm on cord #4 now LOL. 1st one was the cheap blue one, then one I borrowed from my printer, and was a generic Amazon one (which I admit I never tested on long jobs..but I am skipping ahead to the proper cable since you mentioned it). Hopefully 4th time is the charm LOL. [amazon.com - Amazon.com: Tripp Lite USB 2.0 Hi-Speed A/B Cable with Ferrite Chokes (M/M) 6-ft. (U023-006): Electronics](https://www.amazon.com/gp/product/B003MQ29B2/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1) . At least in the meantime I can shove big jobs onto the GLCD.


---
**Ray Kholodovsky (Cohesion3D)** *July 07, 2017 12:32*

That would be the the way to go, I can tell you most of our power users are putting their longer jobs onto the microsd card and running from the GLCD. 


---
**Tammy Mink** *July 07, 2017 12:38*

I could see why they might even if the USB connection was solid (like if windows freezes or the numerous other things that can happen to a PC in a 3 hour lasering etc)


---
**Tammy Mink** *July 12, 2017 01:42*

Suddenly after a day of G28.2 working it doesn't.. (only a day because I tried to install an air assist and cussed mirror issues but that's another story) ..it makes like a reving noise when the command is issued but does not move...both axises seem unimpeded and able to move just fine. Secondly the USB disconnects have become more frequent even with the new USB cord and happens during even super tiny jobs now.


---
**Tammy Mink** *July 12, 2017 01:56*

Windows 7 home premium on a Dell XPS desktop w a core i5




---
**Tammy Mink** *July 12, 2017 02:02*

Other things of note. The windows is 32 bit and the smoothie board drivers had to be installed manually as the exe would not work. Windows firewall is off nor anything on there that I would think would mess with the ports. 


---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2017 02:36*

Ok, can we see a video of what's happening when G28.2 is issued?  


---
**Tammy Mink** *July 12, 2017 05:07*

Here you go. First I show that the x and y seem to move fine then I issue a G28.2 a few times from Pronterface. 
{% include youtubePlayer.html id="B4Ie51lzMFA" %}
[youtube.com - G28.2 Not working](https://youtu.be/B4Ie51lzMFA)


---
**Tammy Mink** *July 12, 2017 10:09*

Just a note, I have been running G28.2 a few more times today and it seems each time it seems to move just the tinist bit in Y directions twords the font and to right. Running M119 still seems like my endstops are ok.




---
**Tammy Mink** *July 12, 2017 10:12*

Wait I take that back about M119..it shouldn't say 1 on the X & Y when it's not hitting the limit switches....something must have somehow gotten messed up...




---
**Tammy Mink** *July 12, 2017 10:13*

What is weird is that even if I unplug all the end stop wires I am getting "X_min:1 Y_max:1" from M119




---
**Tammy Mink** *July 12, 2017 10:19*

The connection on the motherboard for the endstops seems clear and undamaged. What else might cause M119 to read as the endstops are triggered even if they are not plugged in? Or does it always read 1 if something is wrong (obviously if that's the case I need to return to tinkering with the wires.. not sure how it would have gotten messed up but anything is possible)?


---
**Tammy Mink** *July 12, 2017 21:14*

Not sure exactly what I did (maybe a connection was just loose). G28.2 is working again :)


---
**Tammy Mink** *July 12, 2017 22:01*

The problem that still remains is the constant disconnection of the USB even on tiny jobs. I even tried running my USB wires several ways and in every USB port on the PC, 4 different wires. I'm out of ideas. 


---
**Tammy Mink** *July 13, 2017 23:51*

**+Ray Kholodovsky** In my attempts to align my mirrors I noticed something very telling about the disconnects. I mostly noticed because I had the minisd card drive in windows open and the sound on while doing test fires. No wires whatever touched, the mere act of firing the laser is causing the USB to disconnect. This happened numerous times even if it's random. 


---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2017 00:11*

A loose wire would make sense.  If switch is not connected it would read 1 in this case.   



Wonderful.  It could be a voltage spike, or it could be that the power supply (which is highly bad) is browning out when firing and the board has insufficient power to operate.  Yes, there is still USB providing power, but it's still an overall bad situation. 



The K40 PSU is barely enough to power the Mini as is.  I highly recommend to people to switch out to a 24v 6a LED-style power supply (roughly 20 bucks on ebay) to power the board and any other external peripherals they may add. 


---
**Tammy Mink** *July 14, 2017 00:22*

I made a video if it helps 
{% include youtubePlayer.html id="CoAAbtpFG_4" %}
[youtube.com - Laser fire disconnects USB](https://youtu.be/CoAAbtpFG_4)


---
**Tammy Mink** *July 14, 2017 00:23*

I'll look into getting a new power supply


---
**Tammy Mink** *July 15, 2017 00:58*

The latest and newest problem (though only once thus far). While running a small job off the GLCD (no wires were being touched)... the machine stopped in the middle of the 3 min job...and all the files disappeared from the minisd card... plugged in USB card and windows told me to format it. 


---
**Ray Kholodovsky (Cohesion3D)** *July 15, 2017 01:01*

And what's the situation now?  Can you observe the LED's on the board? Can you read the files off the card if you plug in again/ go through a reader directly? 


---
**Tammy Mink** *July 15, 2017 01:06*

Also since this now the GLCD display seems to be fading in and out some but I haven't tried resetting the connections yet.  After taking the minisd card out a few times and resetting the machine also...seems the files came back


---
**Tammy Mink** *July 15, 2017 02:37*

It seems ok..sort of for now...it forgot there were files on the SD card again but at least not during a lasering


---
**Tammy Mink** *July 15, 2017 05:19*

It happened again, and now I realize it's tied to the USB issue, as I noticed each time the USB was plugged in even though I was using the GLCD. On that note is there anyways I can stop having to open up LaserWeb and regenerating my Gcode every time pretty much I run it. The problem is everything the machine has an issue it seems to mess up the coordinates (disconnects, aborts, power off, hits outside the limits, forgets the mini sd card,  almost any issue). Maybe I am doing something wrong. In LaserWeb I have it running G28.2 at the beginning and end of every gcode. In LaserWeb I also run G28.2 and then set that to 0. I position for example a circle I want to cut from there. I go and generate and export the Gcode, and now I realize I need to disconnect and unplug the USB. That part seems to work ok, once maybe twice if I am lucky. I run the lasering from the GLCD...something inevitably happens and even if I home the laser, further running of the gcode the positioning seems off...It homes itself fine but it tries to cut the circle in some weird off the grid position. My only choice is to go and get the USB to reconnect and then start the whole process again.


---
**Tammy Mink** *July 15, 2017 12:16*

Yesterday I got a replacement stock motherboard and last night since I couldn't sleep I hooked it up just to see what would happen. At least thus far using Laserdrw I've no disconnect issues. I'll keep on testing but I can probably rule out an issue with the USB cable and ports itself.


---
**Tammy Mink** *July 15, 2017 19:47*

You can ignore all this... because now the lasertube all seems to have died. This is less then a month old so I'm going to try to get eBay and PayPal to help get my money back and in the meantime find a different model and reorder hopefully a less glitchy one. I swear this laser machine is insane.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/XhqiTavQjmD) &mdash; content and formatting may not be reliable*
