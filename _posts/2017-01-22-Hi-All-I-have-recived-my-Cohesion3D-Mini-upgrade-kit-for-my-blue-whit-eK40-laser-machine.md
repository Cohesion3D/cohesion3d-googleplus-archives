---
layout: post
title: "Hi All, I have recived my Cohesion3D Mini upgrade kit for my blue/whit eK40 laser machine"
date: January 22, 2017 02:16
category: "General Discussion"
author: "Michael \u201cBlackBeard\u201d"
---
Hi All, 

I have recived my Cohesion3D Mini upgrade kit for my blue/whit eK40 laser machine. I removed the original board the laser came with and successfully installed the new Cohesion3D Mini board. It works well. The laser head moves in the X and Y directions and seems to be just fine. However, it does not control the laser at all (not power, nor on/off). I did cut off the laser power cable connector on one end, stripped the middle wire, and connected it to the input in the power supply. But it does not work. I then wired back the original potentiometer 3-wires into the power supply, and the test-switch (2-wires), and the laser works manually if I set the power with the POT knob and press the test switch. What did I do wrong that the laser is not being controlled by the Cohesion3D Mini board?





**"Michael \u201cBlackBeard\u201d"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 02:19*

Hi Michael,

Sounds good that you put the pot back.  Did you follow the instructions in several other threads here to put the "L" wire to - bed 2.5 mosfet and make the change in config.txt accordingly? 

Next question - how are you trying to fire the laser via the board control?

We will need pics of your machine, wiring, etc...


---
**Michael “BlackBeard”** *January 22, 2017 15:50*

Which is the L Wire? I did not see any explanation on it. Also, I am not familiar with LaserWeb at all, and not sure what  I have to change in the config file. The picture below is of the power supply. 

![images/3610a2afa038890c41e69e3f353a849e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3610a2afa038890c41e69e3f353a849e.jpeg)


---
**Michael “BlackBeard”** *January 22, 2017 16:36*

**+Ray Kholodovsky**, I assume that the Yellow wire on the left most connector is the L wire (the silnkscreen says L). I wired it into the MOSFET 2.5. Then Changed the config.txt file and changed laser_module_pin = 2.4! to laser_module_pin = 2.5 . Now when I try to connect to the machine from LaserWeb it says Cannot Find File :-( 


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 16:55*

That's unrelated. Restart your computer and everything. I'll get back to you tonight. Out for the day again. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 17:24*

Hi Michael, 

So a bit more details - if LaserWeb says it can't find the file that's a LaserWeb acting up issue or problem with serial communications.  

Hence I recommended unplugging and restarting everything. 

To make sure of the other detail: to be absolutely safe, one should make the changes to the config file, save the file, then close the editor and safely eject the drive in Computer.  Finally, reset the board either with the small reset button in the top right, or by removing all power from it (all LEDs off).  

Plug the board into USB, can you still see the drive, open the config file, and verify that the line looks correct? 


---
**Michael “BlackBeard”** *January 23, 2017 19:01*

**+Ray Kholodovsky**, was I right in assuming that the L Wire is the yellow on in that picture I posted above?




---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 19:03*

NO NO NO. On the left is the Ac Stuff. Don't touch that. Pink wire on the far right is L. 


---
**Michael “BlackBeard”** *January 23, 2017 19:31*

Will try tonight and will update. Thank you


---
**Michael “BlackBeard”** *January 26, 2017 01:12*

**+Ray Kholodovsky** It still does not work. I have changed "laser_module_pin" from "2.4!" to "2.5". The file was saved. I ejected the USB. Powered down the machine. Reconnected the USB. And checked the file again and it was saved. I have wired the pink wire, the right most wire in the connector, to P2.5 BED (as can be seen in the picture). The XY moves, but the laser won't fire. Now, (1) the 3-pin PWM wire is not connected to the machine at all. (2) The machine's POT/knowb, Test switch, and Laser Switch are connected to the machine. Am I missing something?

![images/44afe7b9f81bfe3d99645906c1f52c01.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/44afe7b9f81bfe3d99645906c1f52c01.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 05:48*

Hi Michael,

Something looks off the way that pink wire is connected.  You need a wire going exactly the way your drawn arrows are.  One end going to the right most pin of the PSU (your pink arrow) L and the other end to that bed 2.5 pin on the mini.

If I am correct it looks like you just made a loop where both ends are on the mini.




---
*Imported from [Google+](https://plus.google.com/+MichaelKlimovitksy/posts/C2HRPFNseWH) &mdash; content and formatting may not be reliable*
