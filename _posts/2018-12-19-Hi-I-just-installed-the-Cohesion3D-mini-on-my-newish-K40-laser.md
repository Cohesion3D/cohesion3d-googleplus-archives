---
layout: post
title: "Hi, I just installed the Cohesion3D mini on my newish K40 laser"
date: December 19, 2018 05:04
category: "C3D Mini Support"
author: "James Vanderbie"
---
Hi, I just installed the Cohesion3D mini on my newish K40 laser. No issues during the install, but I can not seem to get the laser to fire while running LightBurn. I do not think it's a setting with LightBurn, but more possibly an issue with the Cohesion3D communicating to fire the laser. 



Things to note:

-The laser <b>will</b> turn on (test fire) if I press Test directly on the power supply.

-The Laser Indicator light seems dim to me (compared to in the past).

-Video of laser in "action" here: 
{% include youtubePlayer.html id="vKelNDly68k" %}
[https://youtu.be/vKelNDly68k](https://youtu.be/vKelNDly68k)



Does anyone have any suggestions on what I can check to solve this issue?



Thank you!



![images/2a7d642c0d7c6ed232ccd987162ee970.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2a7d642c0d7c6ed232ccd987162ee970.jpeg)
![images/4aa86347026d9e9fdcb823550b40062e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4aa86347026d9e9fdcb823550b40062e.jpeg)
![images/ca1dda88c6bbb766ebe7543812ba2a00.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ca1dda88c6bbb766ebe7543812ba2a00.png)

**"James Vanderbie"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2018 05:08*

I’m about to sleep so I haven’t watched your video. 

Here are the top reasons that the laser does not fire:

Potentiometer/ digital panel on the laser power is set too low. 

L wire in the power connector has gotten loose/ not making contact. 



Send this gCode line in LB Console: 

G1 X10 S0.6 F600



Head should move while laser fires. 



Also the stock usb cable is know to cause communications issues, not saying that’s responsible for this, but keep in the back of your head for later. 


---
**James Vanderbie** *December 19, 2018 05:23*

**+Ray Kholodovsky**  Hi Ray. 

Hi Ray. First, thank you for taking the time to respond. 



<b>Regarding the issue</b>



1) Potentiometer/ digital panel: I set this to 99.9 just to see if it makes a difference, and then used your command below. 



2) Using your command of "G1 X10 S0.6 F600" does move the laser +10, but the actual laser does not fire. I can see the laser indicator light (on panel) does brightly light up. 



3) L wire in the power connector: I have not checked this as of yet (I did attempt to push the cables in slightly, but that did not do anything). If the L wire is not seated/connected correctly, would the Laser Indicator still light up?



Thanks again for all your help!


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2018 13:56*

Fast forwarding a little bit, power down first, disconnect the large power connector from the mini and within the power connector touch L to Gnd. You can see the mini pinout diagram to identify these. This replicates what the board does and should fire your laser. 


---
**James Vanderbie** *December 19, 2018 17:03*

**+Ray Kholodovsky** 



I followed your suggestion and jumped the L to Gnd (image attached) on the power connector cable, but unfortunately, this did not work for me. 



A few things to note:

* I can fire the laser by using the Test button located directly on the power supply without issue.

* The "Laser Test Button" located on the panel <i>does not work</i>, so I wonder if there is a problem with the cables somewhere. 



As far as I am concerned, this is not a Cohesion3d problem, but more a problem with either my potentiometer, cables, or water flow sensor. I am open to suggestions on where I should go first based on your knowledge. 



Thank you!



![images/db25b73964f9693c728439eb2a20113e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db25b73964f9693c728439eb2a20113e.png)


---
**James Vanderbie** *December 19, 2018 17:15*

**+Ray Kholodovsky** 



Well color me stupid!

The issue was the water flow sensor. I jumped the sensor and everything works without issue!

* I can confirm that water is flowing so I know this is not a pump issue but more an issue of the sensor (which I will need to replace). 



Thank you for all your help!






---
**James Vanderbie** *December 19, 2018 17:40*

**+Ray Kholodovsky** 



What should the setting be on my LCD potentiometer while using Cohesion3d with Lightburn? Should it be set to its highest, and lightburn will do the rest?


---
**Joe Alexander** *December 19, 2018 21:11*

use the lcd as a maximum power setting, most people run it around 10-15ma to prevent the tube from being overdriven. but without an actual ammeter you cant tell what % that will be on the lcd... imo cut the lcd for a potentiometer and analog ammeter, very reliable.


---
*Imported from [Google+](https://plus.google.com/118299973612404464569/posts/fVdedvVT1io) &mdash; content and formatting may not be reliable*
