---
layout: post
title: "Still having corruption issues on the flash card i formatted 2 cards one samsung 8gig and a sandisk also a 8gig fat 32 and copied the files from the original to them both neither one boots upp the mini but look identical on"
date: July 22, 2017 15:26
category: "FirmWare and Config."
author: "Don Sommer"
---
Still having corruption issues on the flash card   i formatted  2 cards  one samsung 8gig and a sandisk also a 8gig fat 32  and copied the files from the original to them both   neither one  boots upp the mini but look identical on the computer and all show fat32 in windows 10 properties   by not booting the glcd  powers up  backlight  no image and screams  and no communication to pc   original works  after it is put in the pc and doing a check and repair drive and deleting any gibberish files that appear on the card  it must  be getting corrupt either on booting or on shutdown     any ideas why i cannot make a 8gig fat 32 flash card  boot it has the same files as the original





**"Don Sommer"**

---
---
**Ashley M. Kirchner [Norym]** *July 22, 2017 16:17*

Grab a fresh [firmware-cnc.bin](http://firmware-cnc.bin) file from Smoothieware's web site and put that on the card with your current [config.txt](http://config.txt) file. 


---
**Ashley M. Kirchner [Norym]** *July 22, 2017 16:18*

Oh, make sure you rename the file to [firmware.bin](http://firmware.bin) ... 


---
**Don Sommer** *July 25, 2017 01:20*

i did that on a 16g fat32 formated   it didnt boot and  glcd  is lighted and blank and beeper going off didnt change to firmware.cur when i checked back  on the computer.  i renamed the firmware.cur to org and put the downloaded firmware on the orig 4g card it

changed it to firmware.cur  booted up  tried two different  brand16g flash    what program did you fat32 format it with ?






---
**Ashley M. Kirchner [Norym]** *July 25, 2017 02:07*

SD Formatter - [https://www.sdcard.org/downloads/formatter_4/](https://www.sdcard.org/downloads/formatter_4/)


---
**Don Sommer** *July 29, 2017 12:11*

I tried usind [sdcard.org - Home - SD Association](http://sdcard.org) formatter  it formatted the card ok  copied the firmware and control  when booting from mini  the leds look normal but the display is blank and screaming i can see the drive on the computer i can  copy a file to it   when connect with lw4 it disconnects and  will not connect again till a restart of the k40. i have tried using different cables.    it  it connects with the original flash ok except when it get corrupted  once in a while it it doesnt like  my fat32 samsung 16gig or the  sandisk 8 gig even though they all look identical on the computer  on some failed boots i have all the leds on steady what does that mean? it also acts up with the  glcd unhooked from the  mini.  when it boots up from the 4g original flash it runs multiple laser files ok  from lw4 or sd  i have 24.2 volts  and 4.98 on the pot any other  ideas 




---
**Ashley M. Kirchner [Norym]** *July 29, 2017 17:48*

Out of curiosity, is the C3D being powered from the main LPSU, or a separate 24V PSU?


---
**Don Sommer** *July 30, 2017 01:28*

Lpsu


---
**Ashley M. Kirchner [Norym]** *July 30, 2017 01:37*

Ok, my recommendation is to use a separate 24V supply. Those LPSUs that the machine comes with are the absolute bare minimum needed to run the thing and they are, quite frankly, just shit LPSUs. Get a good Meanwell 24V PSU and hook up your electronics to that, leaving the LPSU to only drive the laser.


---
**Ashley M. Kirchner [Norym]** *July 30, 2017 01:39*

And if you intend on adding a rotary and/or z-table, getting a separate PSU becomes a must.


---
*Imported from [Google+](https://plus.google.com/102954017085080987074/posts/boaXMmMYDyQ) &mdash; content and formatting may not be reliable*
