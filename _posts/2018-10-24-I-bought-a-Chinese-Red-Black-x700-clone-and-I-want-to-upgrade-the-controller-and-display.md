---
layout: post
title: "I bought a Chinese Red/Black x700 clone and I want to upgrade the controller and display"
date: October 24, 2018 11:52
category: "General Discussion"
author: "Ken David"
---
I bought a Chinese Red/Black x700 clone and I want to upgrade the controller and display. Do you have a controller and display that will work on the machine? If Yes, what is the cost and what do I need to do to install it? Ideally, I'd like to have the Remix model and display.





**"Ken David"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2018 16:48*

Thank you for posting to the group. Pictures would be helpful please. 


---
**Ken David** *October 24, 2018 21:22*

Hi Ray, here are some pictures to clarify.

![images/fabd62907b1805f12b39c110d109c863.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fabd62907b1805f12b39c110d109c863.png)


---
**Johan Jakobsson** *October 25, 2018 10:50*

Looks like a Topwisdom TL-410C controller.

They´re used in Redsail lasers (which are also red/black). Perhaps that's the machine you have?

afaik, it comes with autolaser . A truly horrible piece of software.




---
**Ken David** *October 25, 2018 14:50*

**+Johan Jakobsson** It is a Redsail clone, not the real thing. I agree, that Autolaser is truly awful, have you used both Autolaser and RDworks? Is RDWorks better?


---
**Ray Kholodovsky (Cohesion3D)** *October 25, 2018 17:44*

Yep.  What's your timeframe on wanting to get this done? 


---
**Ken David** *October 26, 2018 12:30*

**+Ray Kholodovsky** I'd like to get this done asap. I'm suffering with this awful software and it is preventing production. I am an engineer and pretty handy so I think I can install it with no trouble. When can you get the material to me?

Another question - I've read all the docs I can find but I'm not sure what the new workflow will be. Will I need to generate G code in Lightburn, save the G-code file to a SD card then place the SD card in the board slot? Is there a way to "print" directly from Lightburn or any other software?


---
**Ken David** *October 26, 2018 13:12*

oh, one more thing - How does one interface with the controller? I know there is the GLCD but I don't see any screens on it that can be used for adjusting settings on the machine. Is the only interface via the Lightburn software?


---
*Imported from [Google+](https://plus.google.com/107680477820853796226/posts/VnYJHuSHmnu) &mdash; content and formatting may not be reliable*
