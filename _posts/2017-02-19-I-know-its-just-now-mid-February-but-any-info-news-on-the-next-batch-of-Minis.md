---
layout: post
title: "I know it's just now mid-February but any info/news on the next batch of Minis?"
date: February 19, 2017 19:42
category: "General Discussion"
author: "Dave Durant"
---
I know it's just now mid-February but any info/news on the next batch of Minis? I feel like a kid before xmas here. 







**"Dave Durant"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 19:43*

Getting here end of February. Shipping that week into the first few days of March.  


---
**Dave Durant** *February 19, 2017 20:18*

Excellent - thanks for the quick reply!


---
*Imported from [Google+](https://plus.google.com/+DaveDurant/posts/f7BtSShBUJE) &mdash; content and formatting may not be reliable*
