---
layout: post
title: "Hello team, I just received the \"Cohesion3D Mini Laser Upgrade Bundle\""
date: April 24, 2018 14:03
category: "K40 and other Lasers"
author: "Ejal Nvt"
---
Hello team,



I just received the "Cohesion3D Mini Laser Upgrade Bundle".

Replacing the M2 with the Cohesion was very simple. However I got a problem with one of my endstops. My X and Y endstops are SN04's. The X endstop works but the Y axis doesnt even light up. I tried the M2 board again and there both end stops light up and work.



Does anyone know what I need to change exactly in the config file to make these endstops work? I've played a bit with adding a ! to some endstop settings but I only succeeded in getting the X axis endstop to work.



Thanks for the help.





**"Ejal Nvt"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2018 14:13*

Pictures of them and wiring please. 


---
**Ejal Nvt** *April 24, 2018 14:15*

**+Ray Kholodovsky** 

![images/392fffc01ed89125ff6b5d91bc66c1e9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/392fffc01ed89125ff6b5d91bc66c1e9.jpeg)


---
**Ejal Nvt** *April 24, 2018 14:15*

**+Ray Kholodovsky** 

![images/4590ac688c1e5b17de2361b72303e11c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4590ac688c1e5b17de2361b72303e11c.jpeg)


---
**Ejal Nvt** *April 24, 2018 14:17*

**+Ray Kholodovsky** 

![images/6ca579c42a5b4a3226e455a634cb70b1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6ca579c42a5b4a3226e455a634cb70b1.jpeg)


---
**Ejal Nvt** *April 24, 2018 14:19*

**+Ray Kholodovsky** I've uploaded a few pictures. I switched the endstop cables from the M2 directly to the Cohesion3D board. There is no other way I can install them on the Cohesion3D board as they only fit one way. 


---
**Ejal Nvt** *April 24, 2018 15:19*

**+Ray Kholodovsky** is this enough information to help me troubleshoot or do i need to make other pictures? 


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2018 15:32*

I think this problem is: [plus.google.com - I made this diagram for a k40 upgrade customer. On the not-well-known HT-Mas...](https://plus.google.com/+RayKholodovsky/posts/1sBj7vYtxgq)


---
**Ejal Nvt** *April 24, 2018 15:44*

**+Ray Kholodovsky** the board in my laser is a m2 nano.. I've attached a picture. The post you are referring to is about another board? Any other ideas? The endstop I'm using are NO endstop if I'm correct. Could it be something in the config file? 

![images/b0670032ad94a4571a2aa3b52a24be9f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b0670032ad94a4571a2aa3b52a24be9f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2018 16:19*

I realize that, but it looks like your endstops have a wire going to the bottom pin in the connector, this is not connected on the Mini, so you need to connect it to ground like the diagram I linked you shows. 


---
**Ejal Nvt** *April 24, 2018 16:27*

**+Ray Kholodovsky** in the meantime I've checked all connections and wiring diagrams of the endstops. I've connected them directly to the pins on the Cohesion3D board and in pronterface i see the endstops with the m119 command. 0 when triggered and 1 when not triggered. So that seems to be working. The only issue i have now is that lightburn seems to ignore the endstops while homing?? Is there a place where i need to configure lightburn to use the endstops? 

![images/8666b75de5e4618e34cc2b227e3c5df3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8666b75de5e4618e34cc2b227e3c5df3.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 24, 2018 16:51*

The ! after the pin number in the config file would invert the logic of the endstops for alpha min and beta max. 

Either add or remove the ! 



Then save, eject, and reset the board. 



See smoothieware endstop article for more info. 



Just send command G28.2 via console to test homing. 


---
**Ejal Nvt** *April 24, 2018 17:07*

**+Ray Kholodovsky** I had the x and y endstops switched.. Stupid me.. They work now :) thanks for the help :) 


---
*Imported from [Google+](https://plus.google.com/115761277252076290507/posts/jjVbXbf5njo) &mdash; content and formatting may not be reliable*
