---
layout: post
title: "Hi. Not sure if this is the right place to post or not but I think it's a hardware issue"
date: September 04, 2017 21:32
category: "C3D Mini Support"
author: "Jeff Lamb"
---
Hi. Not sure if this is the right place to post or not but I think it's a hardware issue.



I've been running the c3d board for a while from my laptop without issues (running grbl lpc). I've decided to repurpose a dell pc I've had knocking about to be a dedicated laser machine.  I've done a fresh install of windows 10 and everything is good.  Unfortunately when I try to connect to the c3d board with laserweb4 it doesn't detect it at all on the com port.   I've double checked it on my laptop and it works fine. Same usb cable which is a good quality one.  Any ideas? 





**"Jeff Lamb"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2017 21:37*

Grbl lpc requirements are a bit different than Smoothie. 

For smoothie win10 doesn't require drivers. Grbl might. **+Claudio Prezzi** can you advise regarding grbl-lpc drivers on win 10, and provide a link to drivers so that I know the answer in the future? 



Jeff, is the laptop still available to test with?  On the desktop, what do you see in devices and printers? 


---
**Jeff Lamb** *September 04, 2017 21:51*

Laptop is still available and it works fine.  I haven't done anything different on the desktop to what I did on the laptop i.e. plugged it in and let windows do it's thing.  I'll have a look at the desktop ref devices in a few minutes.


---
**Jeff Lamb** *September 04, 2017 21:52*

I've also tried installing the driver from grbl-lpc github but it isn't digitally signed so windows says no.


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2017 21:54*

Well a driver that didn't install could certainly be the case. I don't run 10 but on win 8 you have to go through a safe boot options menu to allow unsigned drivers. 


---
**Jeff Lamb** *September 04, 2017 22:00*

The grbl-lpc site does state that windows has a driver built-in, but if it doesn't install to try the other one. It did install, and it's the same driver and version as the laptop.  Will look at a way to force the install though just to be sure.


---
**Ray Kholodovsky (Cohesion3D)** *September 04, 2017 22:02*

Maybe wait for Claudio's response then. 


---
**Claudio Prezzi** *September 04, 2017 22:19*

Usually no driver is needed for grbl-LPC, but we had some reports about problems with dell computers (when using Smoothieware, not grbl-LPC).



You could try to create a file called ".env" in the LW4 installation folder with the line "RESET_ON_CONNECT=1" in it.


---
**Jeff Lamb** *September 04, 2017 22:52*

That did the trick. Many thanks.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 00:58*

Thanks Claudio. 


---
**Jason Dorie** *October 01, 2017 06:12*

**+Claudio Prezzi** - What does the reset_on_connect actually do?  Are you just toggling the DTR line?


---
*Imported from [Google+](https://plus.google.com/100451757440368369818/posts/hrNU2nHLgYf) &mdash; content and formatting may not be reliable*
