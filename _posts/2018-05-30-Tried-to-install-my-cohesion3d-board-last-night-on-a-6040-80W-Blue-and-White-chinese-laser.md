---
layout: post
title: "Tried to install my cohesion3d board last night on a 6040 80W Blue and White chinese laser"
date: May 30, 2018 00:18
category: "K40 and other Lasers"
author: "Michael Flood"
---
Tried to install my cohesion3d board last night on a 6040 80W Blue and White chinese laser.



Y axis was  moving opposite down when pressing up in lightburn software. I flipped the cable. Then Y axis driver got really hot and started smelling like burned circuits. 



Now when I boot up laser the L1 led is off instead of solid. And the laser says busy when connected to lightburn.



Going to replace stepper driver hopefully nothing else was fried.  





**"Michael Flood"**

---
---
**Jim Fong** *May 30, 2018 00:38*

When you flipped the cable, hopefully you have powered down the board/laser completely.  



There are known instances of unplugging a stepper motor, while power is on,  killing drivers.   This can happen to almost any board and not just the Cohesion3d. 










---
*Imported from [Google+](https://plus.google.com/117536043949504143314/posts/Srz13MAThMw) &mdash; content and formatting may not be reliable*
