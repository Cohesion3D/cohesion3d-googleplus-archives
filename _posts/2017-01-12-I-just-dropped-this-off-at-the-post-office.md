---
layout: post
title: "I just dropped this off at the post office"
date: January 12, 2017 21:38
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I just dropped this off at the post office. All of the November pre orders and through December 5th have been shipped. 

Tentatively, I think I can handle the rest of December tomorrow and January on Monday. 

![images/b6ae41e384755142afde27446bf45d84.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6ae41e384755142afde27446bf45d84.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Alex Hodge** *January 12, 2017 22:38*

Awesome. I'm pretty excited. I think we're about to see a flurry of activity on this group. :)


---
**Ashley M. Kirchner [Norym]** *January 12, 2017 22:52*

Bet the postal guys freaking love you ... <grin>


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2017 22:55*

They didn't even blink. Maybe it helps that I know who all of them are. 


---
**Douglas Pearless** *January 13, 2017 06:25*

Fantastic!!


---
**chris B** *January 16, 2017 13:02*

Hey **+Ray Kholodovsky** I ordered one of your laser mini upgrade boards. I was 2 seconds away from ordering another smoothieboard when I saw your variant, and I'm super impressed you managed to keep it the same size as a moshi-board. Here's to hoping you have enough boards for january since I only got my order in 2 days ago.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/BfcL6xxPzqj) &mdash; content and formatting may not be reliable*
