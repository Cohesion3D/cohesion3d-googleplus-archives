---
layout: post
title: "New board is working great. Only issue I have is with the test fire part of the glcd"
date: May 14, 2017 23:13
category: "K40 and other Lasers"
author: "Anthony Bolgar"
---
New board is working great. Only issue I have is with the test fire part of the glcd. No matter what strength I specify (1/4 1/2/3/4 or full)  it sends 

full current of 19mA. Any ideas why this is happening? I am pulsing L on the PSU. 





**"Anthony Bolgar"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 23:15*

Pot is hooked up right? 


---
**Anthony Bolgar** *May 14, 2017 23:50*

POT eliminated and tied to 5V (Same as having POT set to max) This worked using a smoothieboard 5X


---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 00:01*

I don't recommend that. I recommend having the pot in play to set your current max, say 10-15mA.  With the pot on highest (which is the same as what you are doing) you are drowning out the pwm and it is common that it will not respond. 



But alas, you can also try changing the pwm period from 200 to 400. That's in the config.txt file.


---
**Anthony Bolgar** *May 15, 2017 01:01*

OK, I understand now. Will try it with the POT back in as a limiter and give it a try. Thanks for the quick response as always :)




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/AZknnMig9bd) &mdash; content and formatting may not be reliable*
