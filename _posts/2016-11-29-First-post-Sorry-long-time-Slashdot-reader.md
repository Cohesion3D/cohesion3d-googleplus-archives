---
layout: post
title: "First post! Sorry, long-time Slashdot reader..."
date: November 29, 2016 19:10
category: "General Discussion"
author: "Todd Fleming"
---
First post!



Sorry, long-time Slashdot reader...





**"Todd Fleming"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 19:12*

Welcome Todd! 

Hey do we need to get you a smoothie for the new k40?


---
**Marc Pentenrieder** *November 29, 2016 19:24*

Hi Ray, thanks for the invitation.


---
**Todd Fleming** *November 29, 2016 19:35*

I have a donated 4-axis Smoothieboard, plus a donated middleman and level shifter are on the way.


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 19:38*

**+Todd Fleming** allrighty, that'll probably be quicker.  Let me know if you want this when it's available: [http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**greg greene** *November 29, 2016 22:04*

I be here




---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2016 05:51*

What application does he need it for? (K40 or other?)


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2016 05:58*

Yeah **+Claudio Prezzi** please contact me so we can discuss. 


---
**Claudio Prezzi** *December 04, 2016 17:09*

**+Ray Kholodovsky**, you got a hangout message from me.


---
*Imported from [Google+](https://plus.google.com/101442607030198502072/posts/6b8oU26qC2Z) &mdash; content and formatting may not be reliable*
