---
layout: post
title: "Hi all, I just finished wiring up my lightobject table from ebay (I guess wavedynamic is the same thing)"
date: October 16, 2017 18:32
category: "C3D Mini Support"
author: "Mark Agostini"
---
Hi all,

I just finished wiring up my lightobject table from ebay (I guess wavedynamic is the same thing). When I try to move the table the stepper just makes a groaning sound and doesn't move. 



-I tried loosening the belt to be slack and the motor still won't spin. 

-I modified the config, the dip switches are set correctly on the TB6600.



Anyway, I'm not really sure what to try next. Thanks! -Mark





![images/3487fffb09453969a511ade631edd869.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3487fffb09453969a511ade631edd869.jpeg)
![images/6279d3018f99d1ebdf36bae694f06d64.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6279d3018f99d1ebdf36bae694f06d64.jpeg)
![images/07d46b25fc29649a03fb62d19b824820.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07d46b25fc29649a03fb62d19b824820.jpeg)

**"Mark Agostini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2017 18:35*

Wires seem to be roughly going to the right places. 

Did you save the config file (those !o at the end gamma pins are very important) of the , safely dismount the card/ drive, and reset the board? 

Sometimes TB drivers are bad. **+Ashley M. Kirchner** and I have had a lot of bad ones that energize and will never move. 

Do you have another stepper motor you can test the driver on? 


---
**Mark Agostini** *October 16, 2017 18:44*

-I'm pretty sure I had everything powered off when I put the sd card back in to mini (I added all the !o to the ends, but I'll double check them)

-I do have a spare TB6600 I can try

-I also have this motor



[amazon.com - Nema 17 Stepper Motor Bipolar 2A 59Ncm() 48mm Body 4-lead W/ 1m Cable and Connector for 3D Printer/CNC - - Amazon.com](https://www.amazon.com/gp/product/B00PNEQKC0/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)


---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2017 18:45*

Yes, please give all the combinations a shot. 


---
**Mark Agostini** *October 16, 2017 23:08*

It turns out the stepper was wired wrong.  It could be the ebay version has a different color scheme. Anyway, I used a multimeter to find the winding pairs and this is what I ended up with. It goes up and down now...the Z wobble is a bit crazy though.

![images/25378af75db12a1e2663dc81b04bf35a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/25378af75db12a1e2663dc81b04bf35a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 16, 2017 23:12*

Great! 


---
*Imported from [Google+](https://plus.google.com/116292346631917737133/posts/8CA9dgXMDm5) &mdash; content and formatting may not be reliable*
