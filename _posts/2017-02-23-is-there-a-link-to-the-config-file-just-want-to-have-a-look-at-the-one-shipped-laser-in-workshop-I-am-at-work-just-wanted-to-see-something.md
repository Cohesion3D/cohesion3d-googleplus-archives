---
layout: post
title: "is there a link to the config file, just want to have a look at the one shipped , laser in workshop I am at work, just wanted to see something"
date: February 23, 2017 12:40
category: "FirmWare and Config."
author: "Colin Rowe"
---
is there a link to the config file, just want to have a look at the one shipped , laser in workshop I am at work, just wanted to see something.

Thanks

Also what firmware should I be running, I think i read there were 2 versions?







**"Colin Rowe"**

---
---
**Don Kleinschnitz Jr.** *February 23, 2017 13:16*

Maker sure you read the entire smoothie site:

[http://smoothieware.org/](http://smoothieware.org/)



Perhaps not obvious but you want the CNC version:

[smoothieware.org - getting-smoothie [Smoothieware]](http://smoothieware.org/getting-smoothie)



Make sure you read the Laser cutter section the config is in the bullet-ted list: 

[http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)



Your actual configuration will depend on your machines particulars. 


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 14:49*

There's a Dropbox link floating around: [dropbox.com - C3D Mini Laser v2.2 Release](https://www.dropbox.com/sh/42l2ps3l53xq79t/AAAP9pLoaG9BDDwyrR3d85Sla?dl=0)


---
**Colin Rowe** *February 23, 2017 15:05*

Thanks, what firmware?






---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 15:07*

It's the smoothie CNC firmware from github edge branch. 


---
**Lance Ward** *February 23, 2017 17:15*

I've pre-ordered my C3D board.  I don't have to mess with firmware for now, correct?  Smoothieware already pre-loaded so it's plug and play?


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 17:17*

Put memory card in board.  Put board into laser.  Be done. :)


---
**Lance Ward** *February 23, 2017 17:18*

Awesome! :D


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 17:22*

You get the email last night with the shipping update Lance?


---
**Lance Ward** *February 23, 2017 18:06*

I did Ray, Thanks.


---
**Lance Ward** *February 23, 2017 18:07*

Looks like the challenge is in installing LaserWeb, not the C3D. ;)


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/FmxPTkkCinq) &mdash; content and formatting may not be reliable*
