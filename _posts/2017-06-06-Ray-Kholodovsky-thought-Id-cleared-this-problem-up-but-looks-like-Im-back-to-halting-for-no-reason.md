---
layout: post
title: "Ray Kholodovsky thought I'd cleared this problem up but looks like I'm back to halting for no reason"
date: June 06, 2017 20:12
category: "C3D Mini Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky**​ thought I'd cleared this problem up but looks like I'm back to halting for no reason. Any more ideas on what to try now?



Image is 150x150 300ppi run at 5000mms



I'm going to try running it a little slower but I don't think this is to fast do you?



![images/5b41ff5639f0b685d06161591c8354a4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5b41ff5639f0b685d06161591c8354a4.jpeg)
![images/ce5d0538277fdcf6ea6b7371772af8f3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ce5d0538277fdcf6ea6b7371772af8f3.jpeg)

**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2017 20:16*

Try grbl-lpc, bottom release here: [github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases)


---
**Joe Alexander** *June 06, 2017 20:17*

nice image andy, if rastering it must be a huge gcode file! And i would say 500mm/s is max for a k40(unless you want it to dance away from you and shake itself apart id imagine). did you add a 0 or mean mm/m?


---
**Ariel Yahni (UniKpty)** *June 06, 2017 20:17*

5000mms?


---
**Joe Alexander** *June 06, 2017 20:18*

also I see a stutter about an inch up from the bottom, easier to see on the edges. belt slip maybe?


---
**Andy Shilling** *June 06, 2017 20:19*

Yup mm/m you got me lol.  I'm going it will end up as Disneyland Paris if I can finish it.  I don't know anything about grbl is it as simple as copy the firmware onto the SD card or do I need to reconfigure everything?


---
**Andy Shilling** *June 06, 2017 20:20*

It could be belt slip but the whole image is very stuttery with the head very stop start .


---
**Andy Shilling** *June 06, 2017 20:24*

[drive.google.com - Laser - Google Drive](https://drive.google.com/folderview?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)

Just uploading a video now and you'll see what's going on with it whilst engraving.


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2017 21:30*

That would be the stutter.  Yes, the rough idea is to put the new firmware.bin file onto the sd card and reset the board. 


---
**Claudio Prezzi** *June 06, 2017 21:30*

**+Andy Shilling**​ Slow down massively! Something around 50mm/s is about the max you can expect from smoothieware withith LaserWeb. Reduce the speed even further with the feed override, until movement is smooth.


---
**Andy Shilling** *June 06, 2017 21:47*

**+Claudio Prezzi** I put 5000 mm/s but should have been mm/m, Im running it now at 3000mm/m 50mm/s by my conversion. The video above with the stop start is the start of the 3000mm/m  run so the speed doesn't seem to help with that although it looks like it might finish the job this time




---
**Claudio Prezzi** *June 06, 2017 21:51*

Did you try to reduce even more, like 25mm/s (1500mm/min)? Should definitely not stutter!


---
**Andy Shilling** *June 06, 2017 21:53*

Had to speak to soon didn't I, this time it stopped with an Alarm on the display. I'll try the new firmware tomorrow and report back.

![images/4b0b33cc2e9a39ea24999d255ceca80d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b0b33cc2e9a39ea24999d255ceca80d.jpeg)


---
**Claudio Prezzi** *June 06, 2017 21:53*

How long is your USB cable? Is it of good quality?


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2017 21:54*

And which version of LW are you running? 


---
**Andy Shilling** *June 06, 2017 21:57*

 Front end 4.0730

Back end 4.068 

And no not tried slower yet as only just failed the 3000mm/m.


---
**Andy Shilling** *June 06, 2017 22:00*

I've just seen on the command line that it's saying the kill button had been pressed, I've never seen that before.

![images/c4e0c67c3d5f3362a0231e34853612a7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c4e0c67c3d5f3362a0231e34853612a7.png)


---
**Andy Shilling** *June 07, 2017 18:24*

**+Ray Kholodovsky** Ok so Im really dumb :) but how do I add the GRBL firmware to my sdcard?

dragging and dropping isn't doing anything.


---
**Ray Kholodovsky (Cohesion3D)** *June 07, 2017 23:21*

Copy and paste then?  If you can't do it over USB put the microsd card into a reader and put it into your computer. 


---
**Andy Shilling** *June 08, 2017 04:24*

That's why I'm questioning myself, when I put the card back into the machine and power on I get no display other than the GLCD back light and that's it. 



I'm just checking I've downloaded the right firmware? Right to the bottom of the page last [firmware.bin](http://firmware.bin) file.



I'll try later from a Windows pc just to make sure my Mac isn't causing the issue.


---
**Ray Kholodovsky (Cohesion3D)** *June 08, 2017 04:26*

Oh, I have no idea if the GLCD will work on grbl. 



I didn't write this, you should seek support in the LW group. 


---
**Andy Shilling** *June 08, 2017 04:36*

Ok I'll see what they have to say over there as I used the display and controls quite a bit and without it means I couldn't run straight from the SD card.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/5KkghB43EiM) &mdash; content and formatting may not be reliable*
