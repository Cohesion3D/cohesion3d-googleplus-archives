---
layout: post
title: "Ray, after installing the SD card it looked like it worked fine BUT, the y axis only goes one way, maybe not getting the dir signal"
date: December 02, 2016 21:14
category: "General Discussion"
author: "Steve Anken"
---
Ray, after installing the SD card it looked like it worked fine BUT, the y axis only goes one way, maybe not getting the dir signal. Tested X on smoothie and works on both motors then tested y on both and it only moves one direction, then moved driver to z and both work fine using z. It is ONLY the Y axis that will ONLY move one direction. It cannot be the connectors because they work fine and it can't be the driver chip because it works when moved to the z axis.





**"Steve Anken"**

---
---
**Steve Anken** *December 02, 2016 21:32*

Laser does not seem to fire either. I tried a G1 X10 S255 F100 but no fire, just moves. I have to go do some other stuff. I'll get back to this tomorrow.


---
**Carl Fisher** *December 03, 2016 02:49*

For the laser, make sure you're issuing an M3 command to enable the laser before using the G1. Then use M5 to disable it.   So...   M3 G1 X10 S1.0 F100 M5




---
**Steve Anken** *December 03, 2016 03:17*

Thanks Carl, I'll try it tomorrow when I get back to the machine. But defaults are G1 on and G0 off in the settings. Is this just for the jog fire test or should I add the M3 and M5 in the setting so all G1 moves have to have that wrapper in the code? 



This has been a point of confusion and I forgot what I did on the first laser I converted. I know things are always changing so I want to make sure I set this up correctly for this guy.



 The Cohesion board sure makes it easy compared to what I did with my first Smoothie but I still got hiccups. I do think the new LaserWeb3 server.js improved connecting and disconnecting but I wish it had just worked with the defaults. As it was the Chinese slobbered silicon all over the connections that took me way too long to take off so I ran out of time and the client had to go to a doctor appointment. I took the board so I can try some tests with motors tonight.


---
**Carl Fisher** *December 03, 2016 03:19*

If you look in the settings, LW should have a place to put the laser enable and disable codes. M3 goes in enable, M5 goes in disable. This will automatically be added to the generated gCode however when you're doing manual commands you have to remember to issue them yourself.


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:34*

**+Steve Anken** I tested your board in my laser personally at which point in time X and Y axes were working fine.   I think the quickest way to move you forward will be to reassign the Y axis to the Z socket in config.  I can walk you through some diagnostic steps at a later date and if we don't get it working I can replace the board when we get the inventory in.  I can walk you through the quick config change to make the y - z swap happen. 


---
**Carl Fisher** *December 03, 2016 03:36*

Are you moving your driver between Y and Z or do you have 3 drivers installed? I'm wondering if the Y driver might be bad


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:38*

I shipped Steve a board with 2 A4988's installed and tested by me, so he probably swapped the Y over to Z.  If so then that would prove the driver is fine. 

I'm not entirely sure where the issue occurred. 


---
**Carl Fisher** *December 03, 2016 03:39*

ok, was just throwing out some general troubleshooting ideas :)


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:40*

Yeah, it's a good question Carl.  Will wait for Steve to confirm the answer to it. 


---
**Steve Anken** *December 03, 2016 03:43*

Just measured the Y Dir pin - looking at 1.281, the x and z are 3.21v. Something is not right.


---
**Steve Anken** *December 03, 2016 03:46*

Both drivers work. X and Z work. Y Dir pin NOT outputting 3.2v like x and z. Nothing obvious on the board like a bad connection.


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 03:51*

Yeah that's weird.  We can try the following:

Load the stock smoothie config file onto another Microsd card and load up that, try jogging and see if the issue persists. 

If it does, then we can just swap the Y and Z sockets in config for the time being and if needed I'll replace the board in a few weeks. 


---
**Steve Anken** *December 03, 2016 04:06*

Yeah, I was hoping it was like Chilipeppr so I can just map the z motor to the y axis step and dir. I'll try the SD card change to let you know if it's something in the config. But I have no problem just remapping to get him going and don't need a new board just to replace something I can work around. Is it just swapping the Beta and Gamma pins? (and be sure to invert p0.20)


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 04:14*

**+Steve Anken** you got it! There's a step dir and en defini on for beta and gamma. Swap the 3 sets.  Then save, safely eject the card in windows, and reset the board. 


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 04:15*

**+Peter van der Walt** and **+Carl Fisher** exactly, M3 in the start gcode and M5 in the end gcode. Pwm will take care of laser off during the job cycle. 

Carl this realization most likely happened after I initially got you set up...


---
**Steve Anken** *December 03, 2016 04:18*

Worked, I get the 3.2v jogging the y with the driver on the "gamma" or z driver. 



Very cool about the laser, I thought I had remembered going through this and taking it out but I didn't know you needed it to do the jog fire test trick.


---
**Carl Fisher** *December 03, 2016 14:36*

**+Peter van der Walt** interesting. I have it in laser enable and disable. Would that have anything to do with the jerky movement on raster?


---
**Ray Kholodovsky (Cohesion3D)** *December 03, 2016 16:28*

So the 2 pin wiring is still fine... But we want M3 in start gcode, M5 in end gcode, and blank enable and disable fields. 


---
**Steve Anken** *December 04, 2016 00:15*

Got it working.  He is supposed to get online and post his results to this group. I added the m3 and m5 to start and stop gcode setup after testing the laser fire with Carl's example gcode. Did not have a lot of time to look at the changes since I last used it.



 It is still a kick for me to see it etching vectors once it all works. I like the musical sounds of the motors with certain images. Thank you once again Ray, Carl and Peter. 




---
**Steve Anken** *December 04, 2016 02:51*

**+Peter van der Walt**, I am for anything that gets this maker technology into the hands of crafts and art people, students and teachers and others.



Ray's board was wonderful (with his fantastic support) to install and it will help give a young man some income. I keep losing money on these conversions because the people doing this are hurting as it is. I really want you to know that this is not a game for them and they are not computer savvy but have other talents and experience in other fields. You are helping them pay their bills, and that is a very good thing you are doing, thank you.


---
**Carl Fisher** *December 04, 2016 03:21*

Glad you got it sorted out. It's a good board from my testing so far.


---
**Ray Kholodovsky (Cohesion3D)** *December 04, 2016 03:33*

We're getting there... Just waiting on the final sample from the assembler. Then I'll be happy. Well I'll be happy when the 2 samples and 98 production ones work too. 


---
**Steve Anken** *December 04, 2016 07:44*

Yes, we all build on what others have done and this team seems to be doing well. The User Friendly k-40 conversion helps bring in more users and the EXE install will really help. I just found out my user cannot follow the install instructions so I will have to do it for him. I could not resolve what he had done over the phone.



The larger success of this project will depend on how well the project scales to a larger, less techie, user community. For us to get this into schools is a REAL bitch and liability is a real impediment to widely adopting any potentially dangerous technology that does not have a track record for covering all the paperwork bases.


---
**Steve Anken** *December 04, 2016 15:33*

I have not tried it yet and was waiting for it to become stable but as is the install is not for most of my clients. It's OK, I tell them they are on the edge and should expect some glitches. It's a lot of time and work to get new folks up to speed when they don't even know what vectors and bitmaps are and never used the command line.


---
*Imported from [Google+](https://plus.google.com/109943375272933711160/posts/CwYLrztb5Ky) &mdash; content and formatting may not be reliable*
