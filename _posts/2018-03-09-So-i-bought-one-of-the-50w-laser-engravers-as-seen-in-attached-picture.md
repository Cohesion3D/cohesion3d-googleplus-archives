---
layout: post
title: "So i bought one of the 50w laser engravers as seen in attached picture"
date: March 09, 2018 02:34
category: "General Discussion"
author: "Event Horizon"
---
So i bought one of the 50w laser engravers as seen in attached picture.

Should it work with the Cohesion3d mini just as a k40? Would i follow the same instructions? Am i on the right track here?



![images/38605fe965474b677d7fc8d84e7fb04a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/38605fe965474b677d7fc8d84e7fb04a.jpeg)



**"Event Horizon"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2018 02:39*

We've had some people upgrade the bigger ones for sure but it's hard to give a definite answer without seeing the electronics inside.  



I'll put it this way: best case scenario it has a separate 24v power supply and the M2Nano and you just have to swap over a few wires and it works with the plug-in-able green drivers in the bundle. 



If it's 36v/ some weird board then you can still do it all,  you might just need to install external stepper drivers for all axes as shown here: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



So, it's definitely possible to do, just a question of how easy it will be, which I recognize is our value for a lot of people. 


---
**LightBurn Software** *March 09, 2018 14:50*

From the look of it, that unit has a Ruida controller. You’d do well to try that first - it’s a great controller.


---
*Imported from [Google+](https://plus.google.com/117526759917595764780/posts/bMMT3HPMo32) &mdash; content and formatting may not be reliable*
