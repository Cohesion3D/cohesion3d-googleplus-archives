---
layout: post
title: "Received this nearly a month ago and just got a chance to open it up"
date: December 12, 2017 17:37
category: "General Discussion"
author: "Kelly Burns"
---
Received this nearly a month ago and just got a chance to open it up.  All companies should package like Ray.  This is the difference And certainly worth paying a little more.  Anxious to get started with my new conversion project.  

![images/c5969e8229044b65cbbdce7c401ce25a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c5969e8229044b65cbbdce7c401ce25a.jpeg)



**"Kelly Burns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2017 17:42*

Personally tested by me too.  It, uh, is a fairly extensive process. 


---
**Richard Vowles** *December 12, 2017 17:43*

I hope that is a business address and phone number. There are often creepers about.


---
**Kelly Burns** *December 12, 2017 18:13*

Honestly, I don't worry about it. If "Creepers" want my address they can get it easily enough. If someone want to visit me, fine.  I need some a good target to test out my new gun and firearms training. Now the phone on the other hand.  I assume I'm safe here among friends ;)

    


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/W7TkZoQB4wv) &mdash; content and formatting may not be reliable*
