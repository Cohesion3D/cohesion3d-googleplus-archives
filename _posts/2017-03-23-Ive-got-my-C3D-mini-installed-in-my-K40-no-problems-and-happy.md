---
layout: post
title: "I've got my C3D mini installed in my K40, no problems and happy"
date: March 23, 2017 20:52
category: "C3D Mini Support"
author: "oliver jackson"
---
I've got my C3D mini installed in my K40, no problems and happy.  I've just got the watterott tmc2100 3.3-5v version silent step sticks.  On the wiki on github, it says they must not be powered without the motor voltage connected or high current can flow into the logic section and destroy it.



One solution is to hack a cable.  I would prefer to find there is a board mod.  Is there a jumper I haven't spotted yet?  If not can I just remove D1 and can a jumper be considered for the next revision?  thanks





**"oliver jackson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 21:09*

I have 2100's in a few printers and have done "stupid stuff" on occasion, no concerns. Anecdotal only... what you want is the USB PWR jumper on the bottom. You'd have to cut the trace. Then the board will only be powered from 24v in. 



The counter is that you should make sure to disconnect from software as killing k40 power will power off the board. I prefer having both power sources as it keeps comms going. 



 But it's there for cases like this or for running with a pi whose USB port has a very weak amount of power available. 


---
**oliver jackson** *March 23, 2017 21:47*

perfect thanks, exactly what I was hoping for


---
**Griffin Paquette** *March 24, 2017 00:08*

I'll be doing some serious testing on this subject in the coming weeks. Just wanted to sub to the post and report back on my findings by thrashing the 2100's with "stupid stuff"



-Griff


---
*Imported from [Google+](https://plus.google.com/117912755306057047954/posts/HKmGTZvQvcs) &mdash; content and formatting may not be reliable*
