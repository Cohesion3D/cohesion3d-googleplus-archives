---
layout: post
title: "Hello all! New member here just checking in, also i have a quick question"
date: November 15, 2018 00:12
category: "C3D Mini Support"
author: "Tim Shepherd"
---
Hello all!

New member here just checking in, also i have a quick question. I have the k40 and bought the mini board , upgraded power supply and lcd screen pakage. I finished wiring everything up and powered it up, all good except the lcd speaker will not stop making a buzzing sound, i cant figure out how to get it to stop. 

Sorry if this is an answered topic already i cant seem to figure out how to seach on google+ yet.

Thanks everyone and looking forward to meeting everyone!

Tim





**"Tim Shepherd"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2018 00:18*

Find the buzz pin line in config and change it to 1.31 without any symbols after it. 


---
**Tim Shepherd** *November 15, 2018 01:25*

**+Ray Kholodovsky** thanks Ray ill check that out when i get home from work. 


---
**Tim Shepherd** *November 15, 2018 17:22*

Worked like a charm! Thank you


---
*Imported from [Google+](https://plus.google.com/104043869405329045983/posts/DUFgddagiYH) &mdash; content and formatting may not be reliable*
