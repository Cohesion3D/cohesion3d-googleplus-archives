---
layout: post
title: "Need some basic help with Stepper Driver dip switches"
date: February 13, 2018 19:00
category: "K40 and other Lasers"
author: "Bill Keeter"
---
Need some basic help with Stepper Driver dip switches. Basically I don't know what Pulse/Rev number to use . Is the C3D setup for a particular micro stepping amount? (Sorry if these values are easily available. Maybe I was just using the wrong keywords when doing a google search.)



I'm nearly finished building a custom 60w laser cutter based on the OpenBuilds project FreeBurn. I have two NEMA23 motors for the X and Y. Along with two stepper drivers (DQ542MA). Both motors and drivers are from OpenBuilds store. 



[http://openbuildspartstore.com/dq542ma-stepper-motor-driver/](http://openbuildspartstore.com/dq542ma-stepper-motor-driver/)

[http://openbuildspartstore.com/nema-23-stepper-motor/](http://openbuildspartstore.com/nema-23-stepper-motor/)



Anyway, any help with the Pulse / Rev number would help me finish setting the dip switches for the stepper drivers. 





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 19:10*

It's almost irrelevant with external drivers because you define it: you need to know the steps per mm for your mechanics, then you define your microstepping value (16x is most common) and multiply the two which goes into the board's config file. 



This article shows an example for a different driver, and may help you with the wiring/ pins config: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary: Step-by-Step Instructions : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)


---
**Bill Keeter** *February 13, 2018 19:34*

I found this on another site and was going to be my step/mm value as i'm using GT3 belts and 20t pulleys.



(200step/rev * 8microstep/step) / (20teeth/rev * 3mm/tooth) = 26.666666667microstep/mm



Which I guess would mean a pulse/rev of 1600.. else I guess I could change the microstep to 16 and get a value of 3200 (assuming i'm understanding these values correctly).


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 19:40*

Off the top of my head a GT2/ GT2 belt with 20 tooth pulley at 16x microstep is 80 steps per mm in the config file.  



Adjust accordingly - 160 steps/ mm if you go to 32x microstepping, for example.



[prusaprinters.org - RepRap Calculator - Prusa Printers](https://www.prusaprinters.org/calculator/)


---
**Bill Keeter** *February 13, 2018 20:08*

Hahaha. I always forget to go back and use that PRUSA Calculator page. Noticed that smoothie is listed as using 1/16 or 1/32. Also my belt is a 3GT (GT2-3M) with a 3mm pitch. What's the default microstep value in the C3D config you ship? Assuming it's 1/16 from looking at your z table instructions. 



If so, that puts me at 53.33 steps/mm. And a pulse/rev of 3200. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/KuHWeTPPTAa) &mdash; content and formatting may not be reliable*
