---
layout: post
title: "Hello. Having trouble using Cohesion3D Mini with LightBurn"
date: May 12, 2018 14:31
category: "C3D Mini Support"
author: "J\u0101nis Kar\u0146ickis"
---
Hello.



Having trouble using Cohesion3D Mini with LightBurn.

When engraving/cutting - it justs stops randomly. LightBurn showing - Busy, but nothing happening.

After i pull out USB, it homes and i have to start from the beginnig. 



Any one else having this kind of problems?





**"J\u0101nis Kar\u0146ickis"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2018 14:15*

This could be from a number of things:



Using the USB Cable that came with the K40 is known to cause issues.  Please use a high quality USB cable.

What are the details about your computer - Windows, Mac, what manufacturer?  



If Windows, turn off USB Power Save mode.


---
**Jānis Karņickis** *May 20, 2018 19:43*

**+Ray Kholodovsky** Thanks for the tip.

tried this weekend! Helped!

Thanks a lot!


---
*Imported from [Google+](https://plus.google.com/117995752420452028846/posts/EonHK8WGYEW) &mdash; content and formatting may not be reliable*
