---
layout: post
title: "I think I am missing something. I got the Cohesion3D Mini installed on my K40"
date: January 21, 2018 04:35
category: "C3D Mini Support"
author: "Chuck Burgess"
---
I think I am missing something. I got the Cohesion3D Mini installed on my K40. I can move X/Y with the GLCD add on. The lights are blinking as expected etc. If I plug in the USB, it connects and LightBurn says Ready. But as soon as I try to move the X/Y (or anything else), I get a Disconnected. What am I doing wrong? I have tried it with the main power on and off.



I know the board is connecting to the main power too because I get the red light on the mini when I turn the K40 power switch on. Any ideas?





**"Chuck Burgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 04:37*

I'll need a lot more details.  Pics of the wiring, machine, what software you are using, is it the USB cable that came with the K40...etc...


---
**Chuck Burgess** *January 21, 2018 04:58*

The machine is wired exactly like the pics show in the  install guide ([https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145373/original/yyv0cZeRKC2JDRjyf8vOBuJyMHGJiNDmug.JPG?1489517776](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145373/original/yyv0cZeRKC2JDRjyf8vOBuJyMHGJiNDmug.JPG?1489517776)) . I am using LightBurn software (registered version). The green LEDs are blinking as expected. The GLCD I ordered with the mini (and connected as instructed in the install process) lights up and works. It will jog the X/Y gantry just fine. For some reason, the LightBurn software will not stay connected.



[s3.amazonaws.com](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145373/original/yyv0cZeRKC2JDRjyf8vOBuJyMHGJiNDmug.JPG?1489517776)


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:00*

What USB Cable are you using? Is it the blue one that came with the k40?



What kind of computer do you have, and what OS?


---
**Chuck Burgess** *January 21, 2018 05:07*

Here is a video of the unit booting up when plugged into the USB on my Mac.

![images/5acab4ceadbb43028047044dc86589f4](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5acab4ceadbb43028047044dc86589f4)


---
**Chuck Burgess** *January 21, 2018 05:09*

The USB is the blue one that came with the laser. I am using a Mac MINI running High Sierra.




---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:11*

First thing you should do is get a better USB Cable, preferably one with ferrite beads on the ends.


---
**Chuck Burgess** *January 21, 2018 05:14*

LIke this one?

![images/3743cd800914e8e119ebbb8594fea794.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3743cd800914e8e119ebbb8594fea794.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:16*

That is a ferrite bead, yes, but the blue leads me to believe this is also a cheap cable from China. 


---
**Chuck Burgess** *January 21, 2018 05:20*

Ok, I will try another cable.


---
**Chuck Burgess** *January 21, 2018 05:21*

Wow! That's all it was. Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:22*

Common problem, happens often.


---
**Chuck Burgess** *January 21, 2018 05:23*

If I click the down arrow and the Y moves towards the back of the machine, does it mean I have the Y connected to the board reverse of what it should be?


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:24*

Possibly.  When you home, the head should move to the back left.  Does it?


---
**Chuck Burgess** *January 21, 2018 05:29*

Yes


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 05:30*

Then your cable is correct.  In Lightburn, the origin should be the bottom left dot.




---
**Chuck Burgess** *January 21, 2018 06:35*

It's not connecting again. Everything running fine on the GLCD in terms of movement etc, but now LightBurn is no longer connecting. I have tried multiple cables. It seems sporadic at best.


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 06:39*

What version of Lightburn? 


---
**Chuck Burgess** *January 21, 2018 16:41*

I am using the latest version: 0.5.9 . If I shut everything down and then bring it all back up, LightBurn will show Ready. Then when I click on one of the move buttons, get position, or the Home button, it says Disconnected.


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 16:42*

Is your laser on? Does the head move to home? 


---
**Chuck Burgess** *January 21, 2018 17:29*

Before I installed the Cohesion3D Mini, when I turned on the laser it would move to home. After I installed the Cohesion3D Mini, when I turn on the machine, it does not move to home.  I have to select the move to home on from the GLCD option menu.


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2018 17:31*

That's correct. C3D does not home on boot by default. 





**+LightBurn Software** do you have any thoughts? 


---
**LightBurn Software** *January 21, 2018 17:35*

I would try connecting with a simple terminal program and see if you can send a home command from there, or G0 moves. If you can’t then it’s not a software issue.



It sounds like a comm issue, but those are usually bad cable, bad port, etc. Try connecting to a different port in the PC as well.


---
**Chuck Burgess** *January 21, 2018 22:38*

**+LightBurn Software** like from the terminal in LightBurn or is there another way?


---
**LightBurn Software** *January 21, 2018 22:41*

I meant any standard serial terminal. The one in LightBurn does a little filtering, and only sends when you hit "Enter"


---
**Chuck Burgess** *January 21, 2018 23:59*

I'm not familiar with the process of connecting to the Cohesion3D via a terminal. I run a Mac so I have a terminal (similar to Linux command line). But I cannot find any information online or any instructions on how to connect to the card. Can anyone point me in the right direction?




---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2018 00:02*

[smoothieware.org - pronterface [Smoothieware]](http://smoothieware.org/pronterface)


---
**LightBurn Software** *January 22, 2018 00:07*

In this  case terminal means old-school modem / serial connection tool. I'm not sure what's available on the Mac, but simple app that can talk to one of the tty ports should do it.


---
**Chuck Burgess** *January 22, 2018 00:34*

I have tried 3 different cables. I am trying to connect with Proterface and I see this in the log:  Serial error: could not open port /dev/tty.usbmodem1431: [Errno 16] Resource busy: '/dev/tty.usbmodem1431'. I'm not sure what might be going on.


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2018 00:36*

Is Lightburn program closed? Restart computer, open just Pronterface, and try again. 


---
**Chuck Burgess** *January 22, 2018 00:52*

Working after reboot. Pronterface connects and moves the axis.


---
**Chuck Burgess** *January 22, 2018 01:03*

LightBurn does not connect still.


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2018 01:03*

You have to disconnect in one program before you can connect in another program.  Are you doing this?


---
**LightBurn Software** *January 22, 2018 01:04*

That might've been all it was. I also didn't realize you were on a Mac - Using Smoothie on a Mac has been known to be a bit off, because the Mac tries to access the SD card while the Smoothie is running. It has been known to cause stability issues.


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2018 01:06*

Ah, interesting point.  Try dismounting the drive that shows up when you plug the board in, then try again. 


---
**Chuck Burgess** *January 22, 2018 02:31*

Still not working. Here is the details of what I have tried:

- turned everything off and restarted

- turned on laser and connected it via USB to my Mac

- ejected the SD card (so it's not reading from it while trying to connect)

- turned on pronterface to confirm it connects (it does)

- disconnected pronterface and quit application

- loaded LightBurn, console says waiting for connection, but never connects

- disconnected USB, reconnected and LightBurn still doesn't connect

- shutdown LightBurn and reloaded pronterface to see if it will still connect

- pronterface connects, moves the axis... then I disconnected and quit



Anything I should change?


---
**Chuck Burgess** *January 22, 2018 02:32*

Oh, and every time I connected the USB, I ejected the SD card connection so it wasn't connected while trying to connect using the applications.


---
**LightBurn Software** *January 22, 2018 02:35*

That's really odd. Have you tried manually selecting the port in LightBurn instead of letting it auto-connect? Between the Devices button and the drop-down that contains your laser name, there should be a box that says Auto, but you can also manually choose a port. Try that.


---
**LightBurn Software** *January 22, 2018 02:41*

I just connected mine automatically to make sure it works. When you connect via Pronterface, does it have an output window or log that you can capture and copy here?


---
**Chuck Burgess** *January 22, 2018 18:24*

**+LightBurn Software** I have tried connecting by changing the selection manually. It still won't connect. I did notice the name of the port on the Pronterface is different than what is available in LightBurn. I don't know if that has something to do with it? Lightburn only shows the tty option.

![images/f461d83f682d9997c11fa1ac5a0de5fc.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f461d83f682d9997c11fa1ac5a0de5fc.png)


---
**LightBurn Software** *January 22, 2018 18:30*

What does it show in Pronterface? I filter things like Bluetooth devices, but any tty device should show up there. The list is only populated when you start LightBurn or when you hit “Ok” in the devices dialog, so make sure the port isn’t in use when you start LightBurn. Take a screenshot of the list you get (or device name) in Pronterface please.


---
**Chuck Burgess** *January 22, 2018 18:36*

The photo I posted above is the pronter interface. Here is what I see in  LightBurn. It shows it's missing he /dev/cu.usbmodem1431 port that the laser is actually using.

![images/46a32fdbc5369443f7c23d5b4643d9c2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/46a32fdbc5369443f7c23d5b4643d9c2.png)


---
**LightBurn Software** *January 22, 2018 18:38*

I’ll try to look at this tonight. On my system, the Smoothie connects through the tty, I believe, but I’ll check.


---
**Chuck Burgess** *January 22, 2018 18:41*

**+LightBurn Software** Sounds good. Thanks!


---
**Chuck Burgess** *January 23, 2018 21:36*

**+LightBurn Software** any updates?


---
**LightBurn Software** *January 23, 2018 22:13*

Wasn’t able to look at this last night (life intrudes sometimes) but will this evening.


---
**Chuck Burgess** *January 23, 2018 22:35*

**+LightBurn Software** no worries. I totally understand. Thank you for your efforts. It's much appreciated.


---
**LightBurn Software** *January 24, 2018 03:21*

I've made a change to the code so it will now prefer the CU (call up) device over the TTY (call in) device. I honestly didn't know the difference before today, and I'm embarrassed to say I chose wrong. The next release, 0.5.10, will include this change. If you PM me, I'll send you a link to a build you can try to see if this fixes your issue.


---
**Chuck Burgess** *January 24, 2018 15:01*

**+LightBurn Software** Nice! Thanks so much! PM coming.


---
**Chuck Burgess** *January 25, 2018 01:20*

It works! Thanks for the update! Everything is connecting fine and working like a charm! Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2018 02:45*

This is great to hear. 


---
*Imported from [Google+](https://plus.google.com/+ChuckBurgess1/posts/W44V8onjzeK) &mdash; content and formatting may not be reliable*
