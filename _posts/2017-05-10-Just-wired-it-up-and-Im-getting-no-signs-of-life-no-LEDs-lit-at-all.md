---
layout: post
title: "Just wired it up and I'm getting no signs of life (no LEDs lit at all)"
date: May 10, 2017 22:43
category: "C3D Mini Support"
author: "elgwido Whittington"
---
Just wired it up and I'm getting no signs of life (no LEDs lit at all). I checked it with a meter and the far left pot is between 23 and 24v. Any ideas?



![images/e7f499a732ffe0318f61d43c6cfe5790.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e7f499a732ffe0318f61d43c6cfe5790.jpeg)



**"elgwido Whittington"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2017 22:45*

You need to move the 24v and Gnd from there to the screw terminal at the top left of the board. 


---
**elgwido Whittington** *May 11, 2017 02:16*

Worked like a charm. Thanks


---
*Imported from [Google+](https://plus.google.com/112657300312517888099/posts/AMDyVxeeTQm) &mdash; content and formatting may not be reliable*
