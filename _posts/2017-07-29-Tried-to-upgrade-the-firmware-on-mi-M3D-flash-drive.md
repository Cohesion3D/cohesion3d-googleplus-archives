---
layout: post
title: "Tried to upgrade the firmware on mi M3D flash drive"
date: July 29, 2017 15:51
category: "General Discussion"
author: "bbowley2"
---
Tried to upgrade the firmware on mi M3D flash drive.  Had to use an external reader.  Now the lights on the board aren't functioning properly.  I have 1 green and one red.  My computer doesn't see the boards flash drive anymore.  What did I do.  Both the board and LasewWeb 4 have been working fine.





**"bbowley2"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2017 15:56*

What did you try to upgrade to? 


---
**bbowley2** *July 29, 2017 17:27*

 

firmware-latest.bin 

update firmware-latest 

a month ago 


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2017 17:29*

I don't know what that latest is, we use firmware-cnc.bin and you have to rename it to firmware.bin. 


---
**bbowley2** *July 29, 2017 17:50*

I'll give it a try.


---
**bbowley2** *July 29, 2017 18:09*

no change.  




---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2017 18:11*

format the card FAT32, put just the firmware.bin file, and then put it in the board and power up.  

Better yet, copy the firmware.bin file from the dropbox link at the bottom of the k40 upgrade instructions. 


---
**bbowley2** *July 29, 2017 18:11*

It seems to be working.  If I can get LW4 to run again I'll test it.


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2017 18:13*

If it works then you will need to make sure the config.txt from the dropbox is also on the card, and then power up the board.


---
**bbowley2** *July 29, 2017 20:28*

k


---
**Ashley M. Kirchner [Norym]** *July 30, 2017 05:44*

In case you haven't gotten there yet, you need to grab the 'firmware-cnc.bin' (or 'firmware-cnc-latest.bin') from [https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin), rename it to 'firmware.bin' and put it on the SD card together with your config.txt file. Then, with the board turned OFF, insert the card and turn things back on. Make sure the board is completely OFF (both power as well as USB connection) before you remove/re-insert the SD card.


---
**bbowley2** *July 30, 2017 15:35*

Thanks.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/1HKKuLzqjRv) &mdash; content and formatting may not be reliable*
