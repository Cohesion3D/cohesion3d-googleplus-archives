---
layout: post
title: "K40 laser user...I was able to install the cohesion3d board ok and get it set up in laserweb4,, home it and move around laser head etc., I then decided to try to put grbl-lpc on the micro sd and now no longer working"
date: June 10, 2017 03:32
category: "C3D Mini Support"
author: "Kevin Lease"
---
K40 laser user...I was able to install the cohesion3d board ok and get it set up in laserweb4,, home it and move around laser head etc., I then decided to try to  put grbl-lpc on the micro sd and now no longer working.  I first copied the files from cohesion3d micro sd onto on mac desktop to keep a copy of them, deleted the original files off the micro sd then  put the grbl-lpc binary on the micro sd card, reset and rebooted, not recognized in laserweb4,... put the two original files back on the micro sd and back into cohesion3d, reset and reboot, now only red led on, 4th green led turns off when I plug usb cable in, don't see the middle flickering green leds anymore, any suggestions?

Thank you





**"Kevin Lease"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 10, 2017 04:26*

Are the "original files" you copied a firmware.bin or a FIRMWARE.CUR?  You'd need a firmware.bin to flash back to Smoothie.   There's a Dropbox with these files are the bottom of the instructions. Try those.  Put them on the card then power up. The green leds should count up in binary, then the middle ones should flash. 


---
**Kevin Lease** *June 10, 2017 11:23*

ok, that fixed it, thank you very much


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/7CrSCHSpakz) &mdash; content and formatting may not be reliable*
