---
layout: post
title: "NEW TO THE LASER GAME Have a C3D mini and lcd display"
date: September 06, 2017 18:54
category: "C3D Mini Support"
author: "Dave Klotz"
---
NEW TO THE LASER GAME



Have a C3D mini and  lcd display.

Turned it off yesterday and when I turned it on the display powered and backlit but nothing on display.



Just bought machine used from someone who modified pretty much everything. 



So far i have removed and inspected and reset cables and conections to both ends.  Removed display and looked for obvious loose solder joints or burnt components on C3D mini AND GLCD.



Looking to troubleshoot further...Or maybe i went deep before checking here😎



Also FYI...maybe related maybe not.



Have never been able to control machine via USB  or Ethernet cable. Shows up as external drive when hooked to computer  Can send data to card and have run machine from LCD display up until i fired it up and display was blank.





**"Dave Klotz"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2017 19:05*

Welcome!



Ok, do I understand correctly that you've used the machine with the board before?  It worked before, but stopped working yesterday/ today? 



Here's what I need:

 

Pics of the wiring.  



Power up the k40 and describe what lights on the board are doing.  



Then with k40 power off, just plug in usb and describe the LEDs again.



What does the GLCD do now? Can you see the usual text and stuff on it? 



For USB... tell us about the computer and operating system.  Sometimes drivers are needed. 



Ethernet... Going to get back to this one.  In short it needs to be enabled in config.txt, the module needs to be plugged in the right way, etc... 


---
**Dave Klotz** *September 06, 2017 19:35*

Can't get pictures to load. Give me an email. 



 Yes worked until today. 



Dell latitude e6410

4gb ram

Windows 7 pro





Testing per your instructions:



Fired up without usb...lights... red on solid next two solid green. Next two fast blink in unison far right one solid green

GLCD is powered but blank.  



Power down plug in usb...

Without powering up k40.



Computer recognizes drive. First led dark...next 2 led are solid green next two flash in unison last one on solid.

GLCD no power.



Power up k40. 

1st light turns red status same on others

GLCD IS powered but blank 




---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2017 19:52*

info at cohesion3d dot com



Put the memory card into your computer, backup the files currently on it, then format it fat32, and put clean files on:   



They can be found at the dropbox link at the bottom of here:



K40 Upgrade with Cohesion3D Mini Instructions: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions.  : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)




---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2017 19:54*

Then put the card back in the board, power up the k40 and watch the LEDs.  The green ones should count up in binary and then the usual sequence should resume. 


---
**Dave Klotz** *September 06, 2017 20:42*

Figured out my blank display problem. 



It was a moron chip that kicked fired off.



The other day while cleaning up i moved files to a folder and took the config files with it. Doh.  Put them back.   Back burning. 



Will revisit communication issues in a few.


---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2017 20:54*

For USB, Windows 7 will need drivers: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)


---
**Dave Klotz** *September 07, 2017 13:17*

I moved the original config file back and was burning. 



Decided to update to your config file and firmware.



Now have no Laser.

head moves and runs gcode file but laser not firing


---
**Dave Klotz** *September 07, 2017 13:22*

switched back to old config file now burning




---
**Ray Kholodovsky (Cohesion3D)** *September 07, 2017 14:01*

Ok, stick with the old one. Maybe you've got one of the original boards which is wired a bit differently. 


---
**Dave Klotz** *September 07, 2017 14:04*

now on to communication.  I have attempted to install the smoothie driver and it fails.


---
**Ray Kholodovsky (Cohesion3D)** *September 07, 2017 14:05*

Then uninstall it and do a manual installation like that page shows. 


---
**Dave Klotz** *September 07, 2017 14:08*

![images/dddf6294ffd95e20143aae999085ac37.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dddf6294ffd95e20143aae999085ac37.jpeg)


---
**Dave Klotz** *September 07, 2017 14:09*

shows that it is installed....




---
**Ray Kholodovsky (Cohesion3D)** *September 07, 2017 14:13*

Uninstall the driver from "programs and features" then do a manual installation like how the smoothie drivers site I linked you shows. 


---
**Dave Klotz** *September 07, 2017 16:26*

been there did that. Switched to older backup computer with windows 10.  first try communicating with machine without installing anythin other than lw4


---
**Ray Kholodovsky (Cohesion3D)** *September 07, 2017 16:29*

Windows 7 needs the drivers, Windows 10 does not.  Need more information about what's happening.  Try connecting via Pronterface and jogging the machine. 


---
*Imported from [Google+](https://plus.google.com/101670628236802931441/posts/Xe6KTG1eMHR) &mdash; content and formatting may not be reliable*
