---
layout: post
title: "Hey all, decided to upgrade my smoothie firmware this weekend, so I downloaded the new config.txt from support and smoothie cnc firmware"
date: August 09, 2017 10:22
category: "C3D Mini Support"
author: "chris B"
---
Hey all, decided to upgrade my smoothie firmware this weekend, so I downloaded the new config.txt from support and smoothie cnc firmware. After flashing it made my LCD garbled and laser fire continuously, So i went back to my saved copy (from when I first got my board in the first shipment batch). Everything worked again. Forgive me since I haven't really checked the forums since I got and installed the board back then, since it's been working all these months. 



Apparently the laser pin in my old config is 2.4! and in the new config it's 2.5,  and at some point it fell out of favor with the community to use the 3 pin pwm connector on the mini board and instead to keep the pot connector. Seeing as the pot has long been thrown away, am I fine with keeping the 3 pin pwm connector on the mini board? Or should I remove it and use the new 2.5 laser pin config.txt? Will any wiring need to be changed other than removing the pwm connector (which will just leave the connection on the PSU open since again, I don't have a pot)?  Or should I just forgo all those changes and use the updated cnc firmware and keep using my old config.txt with the 2.4! laser pin and pwm connection to the board?  



I've pretty much only been using the laser to cut wood and acrylic the past 6 months and have had no issues the way it's been, but I'm hoping to start rastering images soon, hence I thought why not upgrade, they've probably improved something firmware wise since January. Thanks all for any help!





**"chris B"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 13:30*

The config file is different now to account for the changes you described... but if you just want to update firmware then keep your config and put a new firmware-cnc.bin (rename to firmware.bin) on the card and reset the board. 


---
**chris B** *August 09, 2017 17:54*

Thanks for the reply **+Ray Kholodovsky**, so just to be sure, to use 2.5 as the laser firing pin I need to buy a pot, correct? Plenty of pretty knowledgeable people seem to really stress how it's better to use one but don't say why.. is it because people will often use the laser at 100% burning the tube out quick otherwise? 


---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 17:58*

In order to use the "2 wire" method that you have, your laser pin is 2.4! (pwm cable) and on the first gen of boards you also have a laserfire switch going to pin 2.6.   This should be what your config says.



The reason we made the switch was that having the pot lets you improve contrast when engraving. 



The config should limit power to 80% by default. Be aware of that and set that limit in LaserWeb accordingly.  



Just try with what you've got, it should be workable.  


---
**chris B** *August 09, 2017 18:42*

Hah, first thing I did was change the laser max power from 0.8 to 1.0 when I got it. I've never used more than 80%, but my tube is more than a year old now and probably has over 1,000 hours of work on it.. so i figure I've gotten at least half the life out of it. Good to know about the engraving, so now I know there is a tangible benefit to it. I'll give it a go as is and if I'm not getting satisfactory results I'll know to buy a pot and redo my config.


---
**Tammy Mink** *August 10, 2017 12:07*

For the garbled GLCD, I finally figured out that if I have the USB connected, then for some reason the GLCD garbles and won't boot right. Seems if I just am sure the USB cord is unplugged when it boots, it doesn't have the problem. Took me awhile to figure that out.


---
**Ray Kholodovsky (Cohesion3D)** *August 10, 2017 16:51*

I am aware of the garbled lcd happening when starting the board from a USB connection, if the computer cannot provide enough power for everything (Mini and glcd) to boot successfully. 



Starting the board by powering from 24v seems to work better as this and the regulators on the Mini can provide enough power for the initial surge.  


---
*Imported from [Google+](https://plus.google.com/111989435565409807078/posts/Jv8aJr8Fdyd) &mdash; content and formatting may not be reliable*
