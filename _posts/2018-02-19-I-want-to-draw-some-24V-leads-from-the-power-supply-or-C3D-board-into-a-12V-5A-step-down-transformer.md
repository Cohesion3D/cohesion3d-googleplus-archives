---
layout: post
title: "I want to draw some 24V leads from the power supply or C3D board into a 12V 5A step down transformer"
date: February 19, 2018 03:31
category: "General Discussion"
author: "Joel Brondos"
---
I want to draw some 24V leads from the power supply or C3D board into a 12V 5A step down transformer. From which "leads" on the power supply or C3D board could I run that . . . and in either case, would it affect the power draw or operation of the C3D board?





**"Joel Brondos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 00:24*

Have we already established that the stock psu is utter shit and that you should get a separate one to power your board? 

In that case, do feel free to get a 24v 6 or 10a power supply (normally I recommend 24v 6a but if you want to do other stuff...) and use the transformer.  



I can't speak to the power draw or noise since I don't know what transformer and what you're powering with it... I personally like to keep it all separate. 


---
**Joel Brondos** *February 22, 2018 02:10*

**+Ray Kholodovsky** I just replaced the stock psu because it died on me - as testimony to your critical analysis.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/SmB5dhmuC39) &mdash; content and formatting may not be reliable*
