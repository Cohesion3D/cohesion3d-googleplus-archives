---
layout: post
title: "Not sure this is a C3D or LW4 question..."
date: March 18, 2017 15:50
category: "General Discussion"
author: "Lance Ward"
---
Not sure this is a C3D or LW4 question...   What is the best way to implement air assist control?  I have a solid stat relay and an air pump that I would like to switch off an on with the cutting/engraving process automatically. I noticed there was an air assist option in the LW4 setup for assigning a g code to it but not sure what to do with it.  Most everything else working great so far!  Thanks.





**"Lance Ward"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 15:52*

Use a spare pin of the C3D to turn the ssr on and off, map it to a on and off gcode that you put into the start and end gcodes in LW 


---
**Lance Ward** *March 18, 2017 15:54*

Thanks **+Ray Kholodovsky** , Are there any established standards for this? What pin an what g-codes?


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 15:56*

I'm not sure how the particular ssr works - if you can drive it from a mosfet I would use 2.7 which is the last set of blocks on the bottom screw terminal. For all you need just re enable to switch.laserfire in config, set it to 2.7 pin, and put M3 as your start Goode and M5 as end. 


---
**Lance Ward** *March 18, 2017 16:05*

**+Ray Kholodovsky** Thanks Ray... I'll give it a try!


---
**Lance Ward** *March 18, 2017 18:04*

**+Peter van der Walt** Thanks! Not sure that I even need it now but what is the Air Assist settings intended in LW4?   Would I setup switch module for air assist for M80 and M81 then add these to the LW4 air assist settings?  Or just add M80 and M81 to start and end g code?  ...Or just do what Ray suggested?


---
**Lance Ward** *March 18, 2017 18:37*

**+Peter van der Walt** Thanks so much!  So just don't use the LW4 Air Assist settings at all? Leave Air Assist turned off and just insert M80 and M81 into gcode start/end settings?  Thanks again.


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 18:37*

Agreed.  Is the air assist UI in LW mapped to M80/ M81?  In LW3 I did not see any settings for it so either it's hardcoded or its just a UI element with no functionality yet? 


---
**Lance Ward** *March 18, 2017 19:02*

**+Ray Kholodovsky** I think most of these SSRs just use an LED optocoupler on the front end and take some current at 3-32 volts.  If the MOSFET output at 2.7 is open drain, I can probably just wire ssrin-  to 2.7 and ssrin+ to 5V through a resistor.


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 19:04*

Yep, use the fet2 - pin which is the mosfet circuit. And put 5v or 12v or whatever you want from elsewhere. Be careful that any additional load on the psu may overload it and blow.  


---
**Lance Ward** *March 31, 2017 19:30*

**+Ray Kholodovsky** Any particular reason that the 3D printer hotend control is enabled in the config.txt?  It uses 2.7 for the hotend control.  Can I just disable it? Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *March 31, 2017 19:41*

You can set false for that sure and then do your switch. Why is it turned on? Because we tried to keep the stock config and cnc firmware disables 3d print stuff anyways. 


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/WY3So9LcyYc) &mdash; content and formatting may not be reliable*
