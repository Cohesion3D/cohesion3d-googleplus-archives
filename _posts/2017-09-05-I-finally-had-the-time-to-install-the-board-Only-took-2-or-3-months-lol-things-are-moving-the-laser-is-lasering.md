---
layout: post
title: "I finally had the time to install the board (Only took 2 or 3 months lol) things are moving the laser is lasering"
date: September 05, 2017 13:23
category: "Laser Web"
author: "Erik McClain"
---
I finally had the time to install the board (Only took 2 or 3 months lol) things are moving the laser is lasering.



My question  is: It seems that the link "[http://cncpro.co/](http://cncpro.co/)"  is no longer valid.   Is there a new location for information on how to use LaserWeb4.  



Now that I have lased a black square I would like to learn how to use the other features such as lasering gray scale :)



Thanks.





**"Erik McClain"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 13:38*

Ah yes, that recently went down and they are migrating back to github wiki for the documentation. 


---
**Erik McClain** *September 05, 2017 14:12*

Ah, so just need to wait?  Do you know if anyone made a good 3rd party write up or tutorials then.  Please don't to search for me,  only if you know them off hand.


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 14:23*

Here we are: [github.com - LaserWeb4](https://github.com/LaserWeb/LaserWeb4/wiki)



This looks like what was on [cncpro.co](http://cncpro.co), see if it gets you started. 


---
**Ray Kholodovsky (Cohesion3D)** *September 05, 2017 14:37*

And here is a backup of [cncpro.co](http://cncpro.co): [cncpro.yurl.ch - Home - LaserWeb / CNCWeb](https://cncpro.yurl.ch/)



I think you wanted: [https://cncpro.yurl.ch/documentation/cam-operations/40-laser-operations/8-laser-raster-settings](https://cncpro.yurl.ch/documentation/cam-operations/40-laser-operations/8-laser-raster-settings)



Let me know if you find the videos helpful as well. 


---
**Erik McClain** *September 05, 2017 15:38*

Awesome! Thanks a lot!


---
*Imported from [Google+](https://plus.google.com/102506401249278564565/posts/7R5dQoj6KoV) &mdash; content and formatting may not be reliable*
