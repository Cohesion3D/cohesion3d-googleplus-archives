---
layout: post
title: "Hello, Ok got my Cohesion working with my K40, I noticed that vector engraving is really better than rasters for jobs that envolved lines and circles So the jump to Cohesion3D is a good jump for my needs"
date: August 04, 2017 09:32
category: "K40 and other Lasers"
author: "Fabien Taylor"
---
Hello,



Ok got my Cohesion working with my K40, I noticed that vector engraving is really better than rasters for jobs that envolved lines and circles

So the jump to Cohesion3D is a good jump for my needs.



Just noticing that now the machine is more noisy vibrating for both raster or vector engraving



Can this be "filtered", kind of ? This seems to be not good for bolts, nuts, I am afraid the machine is going to get play everywhere



Thanks









**"Fabien Taylor"**

---
---
**Cris Hawkins** *August 04, 2017 15:08*

When switching to the C3D, you are also switching to new stepper motor drivers. If the current setting on the new drivers is too high, it can cause resonances that will show up as vibration. I reverse engineered the current setting on the original MOSI board drivers, and the Y axis is set at 1 amp with the X axis at 750 mA. The X and Y asxis motors are different size and the seem to require different current settings. Try reducing the current on your drivers and see if it makes a difference. Also depending on which drivers you got with your C3D, you may not be able to get rid of it.


---
**Cris Hawkins** *August 04, 2017 15:23*

CORRECTION: The board that I reverse engineered  the driver current was the M2 MINI controller board, not the MOSI board.


---
**Fabien Taylor** *August 05, 2017 10:01*

Thanks, how reducing current? With the trimmers on the drivers?


---
**Cris Hawkins** *August 05, 2017 13:37*

Yes.



If you search "pololu current setting" on YouTube, you will find numerous tutorials....



I recommend using the actual Pulolu tutorial. I don't believe the method shown in the C3D docs is adequate. And remember on my machine, the X and Y axes are different settings (X Axis = 750 mA, Y Axis 1 A).


---
**Fabien Taylor** *August 05, 2017 14:33*

Thanks for that Cris! I will need to come back from holidays for that😀


---
**Fabien Taylor** *August 05, 2017 14:34*

Just to know, your K40 is not noisy after calbrating the driver currents?


---
**Cris Hawkins** *August 05, 2017 14:45*

Fabien,

My setup uses external drivers so it is not an apples to apples comparison. But it indeed was excess vibration that had me reverse engineer the current settings for these motors. And yes, I did notice a difference with the proper current setting.



Also, the Pololu style stepper drivers that came with my C3D mini use the same driver chip as on the stock M2 mini that came with the K40. If they are indeed the same chip (I suspect the originals are counterfeit) the only other variable I know of is the current setting.


---
*Imported from [Google+](https://plus.google.com/108052364627466740668/posts/DKQqDTeSv1X) &mdash; content and formatting may not be reliable*
