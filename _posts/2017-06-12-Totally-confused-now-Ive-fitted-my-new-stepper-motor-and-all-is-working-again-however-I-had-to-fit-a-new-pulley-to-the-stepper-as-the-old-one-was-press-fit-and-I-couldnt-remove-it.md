---
layout: post
title: "Totally confused now. I've fitted my new stepper motor and all is working again, however I had to fit a new pulley to the stepper as the old one was press fit and I couldnt remove it"
date: June 12, 2017 15:48
category: "FirmWare and Config."
author: "Jeff Lamb"
---
Totally confused now.  I've fitted my new stepper motor and all is working again, however I had to fit a new pulley to the stepper as the old one was press fit and I couldnt remove it.  I made sure I got one with the same number of teeth (20 teeth).  For some reason it seems the steps/mm are way out.  I've currently got it set to 82 (instead of the usual 157.75 or 160).  Any ideas?  BTW - I'm running grbl-lpc.





**"Jeff Lamb"**

---
---
**Anthony Bolgar** *June 12, 2017 16:14*

Check your micro step settings, sounds like your steps are set to half of what it was before


---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2017 16:15*

A 20 tooth GT2 pulley at 16x MicroStep would have a steps per mm value of 80. 


---
**Jim Fong** *June 12, 2017 16:16*

My guess is that you bought a 1.8 degree stepper (200 steps per revolution).  The original must of been a .9 degree (400 steps revolution). 






---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2017 16:17*

Ah, what Jim said makes a lot of sense too. 

But MicroSteps are constantly 16x so that's not a concern. 


---
**Jeff Lamb** *June 12, 2017 16:37*

**+Jim Fong** ah. That makes sense. Is it a problem having 1.8 steppers? I found it quite hard to find reasonably priced steppers for 24v (or are all steppers ok with 24v. Most seem to be listed as 12v). Thanks


---
**Joe Alexander** *June 13, 2017 03:48*

most stepper motors are usable at 24V. The key is that the higher the voltage rating generally the higher the impedance, I find lower voltage steppers(3.6V area) have around 2-4ohm impedance and generate less heat from running. i had a 12v 22ohm on my laser X-axis and it would get really hot. swapped for one with lower voltage and 2.4ohm and it barely gets warm while running whisper quiet, all while running at 24v.


---
*Imported from [Google+](https://plus.google.com/100451757440368369818/posts/crfK2AoBn3W) &mdash; content and formatting may not be reliable*
