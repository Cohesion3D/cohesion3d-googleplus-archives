---
layout: post
title: "Playing with LaserWeb4 tonight. Still learning the ins and outs of it as it's a different workflow"
date: March 11, 2017 06:53
category: "Laser Web"
author: "Kris Sturgess"
---
Playing with LaserWeb4 tonight. Still learning the ins and outs of it as it's a different workflow. Just playing with rasters for now.





**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 06:56*

Cool. This is the LW group if you have any questions/ feedback for the devs: [LaserWeb/CNCWeb](https://plus.google.com/communities/115879488566665599508?iem=1)


---
**Kris Sturgess** *March 11, 2017 07:02*

**+Ray Kholodovsky** Oh I'm over there as well. ;-)  Just watched the 2hr video Ariel posted.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/fiQWcoSTWJZ) &mdash; content and formatting may not be reliable*
