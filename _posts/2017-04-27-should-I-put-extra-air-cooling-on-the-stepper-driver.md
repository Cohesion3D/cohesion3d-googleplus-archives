---
layout: post
title: "should I put extra air cooling on the stepper driver?"
date: April 27, 2017 14:54
category: "General Discussion"
author: "Maker Fixes"
---
should I put extra air cooling on the stepper driver?







**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2017 14:57*

The included A4988 are fine with just the heatsinks and passive cooling. If you want to use something else like Drv8825 or Trinamic these need a fan absolutely. 


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/75Nu3JTHsYs) &mdash; content and formatting may not be reliable*
