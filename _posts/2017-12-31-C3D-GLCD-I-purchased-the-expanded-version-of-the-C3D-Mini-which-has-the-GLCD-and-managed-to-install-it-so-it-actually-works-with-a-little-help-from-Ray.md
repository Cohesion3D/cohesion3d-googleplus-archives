---
layout: post
title: "C3D GLCD I purchased the expanded version of the C3D Mini which has the GLCD and managed to install it so it actually works (with a little help from Ray)!"
date: December 31, 2017 20:48
category: "C3D Mini Support"
author: "Joel Brondos"
---
C3D GLCD



I purchased the expanded version of the C3D Mini which has the GLCD and managed to install it so it actually works (with a little help from Ray)!



Besides the read-out display, there is (1) a slot for an SD card, (2) a potentiometer, and (3) a stop button.



(1) I imagine the SD card is so that I could have a project saved and print from the SD card rather than the computer, but how would I select the file which I want to be printed and then tell it to print?



(2) When I turn the potentiometer, I see the % number increasing or decreasing, but it doesn't seem to affect the laser burning as far as I can tell.



(3) Is the Stop button supposed to stop the laser? Or are these things superfluous?



(Sorry for the flurry of posts. I promise not to flood the community with much more -- it's just that the learning curve has more to it in these early stages, and I hope the answers to my questions might help others down the road as well.)





**"Joel Brondos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 31, 2017 21:14*

1. Big card socket not connected (poor performance), use the microsd card in the board. 

2. That configures the speed override for the job... I think... 

3. Should be able to configure the button to pause or stop in the config file. 



Lots of docs in [smoothieware.org](http://smoothieware.org) 

[smoothieware.org - start [Smoothieware]](http://smoothieware.org)


---
**Ashley M. Kirchner [Norym]** *January 01, 2018 00:20*

Regarding #2:

If you are spinning the knob while the machine is idling, or when it's running, you are changing the speed at which the job is being run (speed of the laser flying back and forth.)

If you depress the button, you will notice the screen changing to a menu where turning the knob will now select those menu items and subsequent depressing of the button acts as an 'enter' button.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/jS4Q1X1sTqn) &mdash; content and formatting may not be reliable*
