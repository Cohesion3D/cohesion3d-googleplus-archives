---
layout: post
title: "Cohesion3D will have a booth at New York Maker Faire this weekend at the NY Hall of Science, and we'll be heavily showing off lasers, boards, and LightBurn (it's even on our banner)"
date: September 20, 2018 03:52
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Cohesion3D will have a booth at New York Maker Faire this weekend at the NY Hall of Science, and we'll be heavily showing off lasers, boards, and LightBurn (it's even on our banner).  All I can say is I promise you'd meet some cool people from the laser and 3DP community. Comment if you're attending, looking forward to meeting you!



The Cohesion3D booth will be located in Zone 2, 3D Printer Village.  And here's a pic of the booth we had at Bay Area Maker Faire last spring.

![images/65a46e943f5c187fb59b6bc8c05be063.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/65a46e943f5c187fb59b6bc8c05be063.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/XVP4GeHNy8m) &mdash; content and formatting may not be reliable*
