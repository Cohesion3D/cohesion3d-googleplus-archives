---
layout: post
title: "Problem. The machine correctly performs the contours of the picture, but only when the laser is off"
date: October 17, 2018 16:26
category: "C3D Mini Support"
author: "Vasily Ivanov"
---
Problem. The machine correctly performs the contours of the picture, but only when the  laser is off. When the laser is turned on, the machine is stopping, in LihgtBurn the task time is running, but the board is not available (status in software - disconnected). Is it possible than I use an external power supply 12V / 5Amp (24 volts from PSU is not connected, ground of laser PSU and power supply are linked)  or because of the memory card (2GB)? Are there any memory card requirements?





**"Vasily Ivanov"**

---
---
**Joe Alexander** *October 17, 2018 18:31*

are you using a quality USB cable? the cheap blue ones that come with the machine tend to have this issue.


---
**Vasily Ivanov** *October 17, 2018 18:57*

So..My next CNC machine works well on this cable. 


---
**Joe Alexander** *October 17, 2018 19:59*

I am simply pointing out a known issue with noise, you can use it all you want but not trying a quality cable kinda prevents this from being tested as to whether it is the issue or not. feel free to search in this group and others, its been posted about dozens of times.


---
*Imported from [Google+](https://plus.google.com/109550403531973425921/posts/HkoSKotztuq) &mdash; content and formatting may not be reliable*
