---
layout: post
title: "Hello everyone, I got my Cohesion board a few days ago, and I can't get it to work"
date: May 14, 2017 19:56
category: "General Discussion"
author: "Collin Turner"
---
Hello everyone, I got my Cohesion board a few days ago, and I can't get it to work.  I've gotten the board to connect to both LW3 and LW4, but the motors wont move.  I have the ribbon cable connected to the board that goes to the Y axis, and the X axis 4 wire cable connected to the driver that came with the board.  I have turned up/down the pots because I didn't have any other ideas, and still nothing.  I am up for troubleshooting, but I don't know enough about this board and engraver to do it on my own.  I am lead to believe it's something between the board and the motors, because I can connect and talk to the board with LW4, it just won't jog the motors.  Any help is appreciated, this could just be a noob problem.





**"Collin Turner"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 19:56*

Picture of board and connections please? 


---
**Collin Turner** *May 14, 2017 20:00*

![images/0d12c8653c7b807099ca59f4e8070138.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d12c8653c7b807099ca59f4e8070138.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:02*

Is the machine turned on? I don't see a red light on the board. From LaserWeb4, can you send a M119 from terminal and can you see a response string? 


---
**Collin Turner** *May 14, 2017 20:04*

I assume this is what you mean by the terminal Ray, as I didn't think LW4 let you access the terminal like LW3 did.

![images/231017d90de5fd523e41bdd7a51669a9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/231017d90de5fd523e41bdd7a51669a9.jpeg)


---
**Collin Turner** *May 14, 2017 20:05*

And that was of course after I turned the machine back on.

![images/f83a843b212291bb3cc914f253e83815.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f83a843b212291bb3cc914f253e83815.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:07*

Yeah that's the one. Side note LW4 should also have it. I see the initial connection lines, I'm wondering why there wasn't a response sent back for M119. How about M114? 


---
**Collin Turner** *May 14, 2017 20:23*

My computer decided it would be a nice time to update, smh, now it won't even connect to the board because it can't find COM6, I'll see what I can do.


---
**Collin Turner** *May 14, 2017 20:41*

Ray, M114 also did nothing to the motors.

![images/8ddf71276464ce6757c7d1b400eb7658.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ddf71276464ce6757c7d1b400eb7658.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:42*

What is the computer and operating system you are using? 


---
**Collin Turner** *May 14, 2017 20:44*

It is a Dell Inspiron 15 running Windows 10 Home.

![images/807558cdf5e142838a6c6abfbbfc22ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/807558cdf5e142838a6c6abfbbfc22ce.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:45*

Ok, for Windows 10 you did not install any smoothie drivers, correct? 

Did you get the GLCD for the board? 


---
**Collin Turner** *May 14, 2017 20:51*

No sir, no drivers were installed to my knowledge.  I got the adapter for the LCD, but no I don't actually have the screen yet.


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:56*

The screen would allow you to jog around, this would be a quick way to verify everything is working. 

I have a suspicion that communications are not going through from LW after the initial connect but I'm not sure about it. 

Please get the latest LW4 and try sending these commands again. They aren't to make the motors move, I'm just expecting to see a response string. 


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 20:57*

[github.com - LaserWeb4-Binaries](https://github.com/LaserWeb/LaserWeb4-Binaries/releases)


---
**Collin Turner** *May 14, 2017 21:01*

I just installed 4.0.731 this morning, hoping LW3 was just the problem.  Unfortunately LW4 has the same issue.  Even when I click the jog buttons from here it will print out the jog string, but won't move the motors.

![images/69cc0db3c07f487fb3b4068aae919434.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69cc0db3c07f487fb3b4068aae919434.jpeg)


---
**Collin Turner** *May 14, 2017 21:02*

And no matter what I click, it always thinks it's at 0,0 which makes me agree that the board isn't receiving the commands.

![images/e8fe74ea4ced4a74148ff40e04d88e56.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e8fe74ea4ced4a74148ff40e04d88e56.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 21:39*

What USB cable are you using?  Is it the one that came with the K40? This causes trouble sometimes.  


---
**Collin Turner** *May 14, 2017 21:44*

Oh yes, I'm using the blue one that came with it.


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 21:45*

My first recommendation is to try a proper USB cable that has ferrites. 


---
**Joe Alexander** *May 14, 2017 23:59*

is that ribbon cable in backwards? i can't quite tell. Also I would check the motor end connections in case they came loose, was the cause of many unresponsive problems for me.


---
**Claudio Prezzi** *May 15, 2017 07:46*

First try to make sure the communication works correctly. If LW4 doesn't get a known welcome string withing 2s, it disconnects the machine (and writes that to the monitor).

To connect,go to the Com Tab, select the port and click connect. You should get a green Machine connected and the line with the firmware. If that doesn't work, klick disconnect and connect again.


---
**Claudio Prezzi** *May 15, 2017 07:49*

While connected, you can press CTRL-X which should reset the board, so you receive the welcome string again.


---
**Claudio Prezzi** *May 15, 2017 07:50*

Smoothieware needs to be in Grbl Mode, but I can see you have installed firmware-cnc, so this should be the case.


---
**Collin Turner** *May 16, 2017 23:36*

Ray, I tried two more USB cables to no avail.  Joe, I'm 90% sure the ribbon cable is right, the traces are facing towards the board.  Claudio, Ctrl+X resets it like you said, but I didn't receive another welcome screen.  Also, how would I go about switching into GRBL mode?  Thank you guys.


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2017 23:42*

Collin is there another computer you can try this on? 

You can also try using software called Pronterface (no install needed, just download for your OS, unzip, and run from the folder).  You can connect and jog from there as well. 

[kliment.kapsi.fi - Index of /printrun](http://kliment.kapsi.fi/printrun/)

You don't need to switch smoothie into grbl mode.  CNC firmware is already installed.  


---
**Collin Turner** *May 16, 2017 23:54*

Ray I have installed Pronterface, and it will let me connect to the machine, but when I try to jog it says error: Alarm lock.  I couldn't find anything obvious that let me disable the alarm.

![images/c2edd49cf212bd77c3cbe3c29e9dc267.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c2edd49cf212bd77c3cbe3c29e9dc267.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2017 23:55*

Can you see if the lone green LED by the button on the top right of the board is blinking? 


---
**Collin Turner** *May 16, 2017 23:57*

Yes sir, the light at the top of the board is blinking.


---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2017 23:58*

M999 to clear alarm, or hold the Play button (the black one) for about 10 seconds. Either way the light should stop blinking. 


---
**Collin Turner** *May 17, 2017 00:02*

I sent M999 and now the LED is solid.  I tried jogging the motors after that and I'm still not getting anything.  No more errors though.


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:04*

Can you now send an M119 or M114 and get a response? 


---
**Collin Turner** *May 17, 2017 00:05*

Is this the result you were expecting?

![images/858cd74668b48a9b55c9e97b38c23038.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/858cd74668b48a9b55c9e97b38c23038.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:07*

For M114 yes. M119 should have sent back a text string as well with the endstop states. 



Can you send the jog command, for example jog X up 20, and then send M114 again please. 


---
**Collin Turner** *May 17, 2017 00:11*

I tried jogging the X axis in every way I know how, but I'm still getting the same.  Do these Chinese lasers have encoders on them?


---
**Collin Turner** *May 17, 2017 00:11*

![images/07cf8992968323f77ae429294423b94f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07cf8992968323f77ae429294423b94f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:12*

What is the M114 response? 


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:12*

Ah there we go.  


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:13*

Do you have another computer you can try this on?  Something is being finicky. See, you literally sent the jog command to move 20 or whatever and it still thinks it's at coordinates 0. 


---
**Collin Turner** *May 17, 2017 00:15*

I could probably get my hands on my buddies laptop tomorrow.  Do these engravers have encoders or endstops/homing switches on them?  I'll see about his laptop tomorrow, thanks again for the continued support!


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:17*

No encoders, just dumb steppers. Which is why it should think it moved even if it didn't. 

There are switches back left, yes. A G28.2 command should home towards there. Keep a hand on the power switch in case it moves some different direction. 

I'd also recommend getting that 12864 graphic LCD so you can do all this stuff without relying on computer. 


---
**Collin Turner** *May 17, 2017 00:19*

Unfortunately that command didn't make any axis move in any direction.  My LCD should be here by Friday.

![images/472599fef7cefdd14a39f66f40d35889.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/472599fef7cefdd14a39f66f40d35889.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 00:20*

Yep, some weird communications thing is happening. Coolio. Looking forward to seeing you up and running/ over this hurdle. 


---
**Claudio Prezzi** *May 17, 2017 07:11*

**+Ray Kholodovsky** Could it be a missing/broken SD card or a missing config.txt?


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 14:16*

Does the drive show up in My Computer?  You can definitely try a different memory card and the files are at the bottom of the instruction manual, there's a Dropbox link. 


---
**Collin Turner** *May 17, 2017 14:18*

It shows up as a drive in the OS, but it won't let me access it and always asks me if I want to format it.  It says the file system is not recognized or something along those lines.  So you guys could very well be right on that, is it supposed to show up with files?


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 14:22*

Yes, you should see a config and a firmware file on there. 

What USB cable are you using? 


---
**Collin Turner** *May 17, 2017 14:23*

It does that exact same thing on all USB cables where I can't see anything on the card.  How should I go about fixing that SD card?


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 14:30*

Lots of variables in play now. Still want you to try another computer.  Try taking the card out and putting it back in (do this with power off). 

You could also get a new MicroSD Card and just put it in, see if that shows up. You'd have to get the config file on there but hopefully it shows up as a drive. 


---
**Claudio Prezzi** *May 17, 2017 15:29*

You could also plug the MicroSD card into your PC (if you have a reader) to check the content.


---
**Collin Turner** *May 17, 2017 23:32*

I plugged the micro SD card into my computer and nothing showed up.  I plugged another micro SD card into my computer and it showed up just fine.  The fact that I can't even see the micro SD card in windows explorer kind of puzzles me to be honest, I've never seen anything like that.




---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 23:41*

Yep that's weird.  I literally put the files onto those cards myself and they go into the anti static bag for shipping.   Try your other card, format it FAT32, and put the 2 files from within here on it:  [dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)

Then put it in the board and power up again. 


---
**Collin Turner** *May 17, 2017 23:48*

You don't think the board would be able to fry the SD cards do you?  I'm just paranoid about putting my new 32GB card in there for now, and might just go buy a 4GB card tomorrow. 


---
**Ray Kholodovsky (Cohesion3D)** *May 18, 2017 01:58*

Since I don't want to be blamed for anything that <b>might</b> happen, go ahead and get that new cheaper card. 


---
**Collin Turner** *May 19, 2017 00:50*

Ray, I have put the files on the SD card and it works perfectly first try.  Thank you all for the support and Ray for standing behind your product even after all of this trouble, I really appreciate it!


---
**Ray Kholodovsky (Cohesion3D)** *May 19, 2017 00:52*

Great to hear. So this thing with the card happened once before. I personally program it, send to customer, not recognized on their end. Thought it was a one time fluke. Not sure what to make of it. 



Anyways, good call Claudio!  This will now not be assumed and check the card on computer will be a debugging step. 



Cheers!


---
**John Hunter** *July 12, 2017 20:47*

I think you are describing my problem.  I ended up not changing cards, just re-formatting the micro sd and it worked, although, it started having connection problems again, so i may just try a new card in it.   Though, as you know, i've switched to a larger laser so, there are bigger fish to fry at the moment :)




---
*Imported from [Google+](https://plus.google.com/106338687452907889031/posts/VikuythRg2h) &mdash; content and formatting may not be reliable*
