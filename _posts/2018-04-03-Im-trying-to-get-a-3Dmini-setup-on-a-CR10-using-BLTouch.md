---
layout: post
title: "I'm trying to get a 3Dmini setup on a CR10 using BLTouch"
date: April 03, 2018 08:42
category: "C3D Mini Support"
author: "Jonas Rullo"
---
I'm trying to get a 3Dmini setup on a CR10 using BLTouch. When I run G30 to check the probe Z offset, I get expected results. However when running G32 to probe multiple points, the Z jumps up to Z18 and tries to probe in mid air. This is exactly opposite of what should happen. I don't think the initial Z is getting saved properly. Running G92Z0 with the nozzle on the bed shows Z is zero by way of M114. I also save with M500 and M665. To reset the BLTouch  I need to raise the nozzle off the bed to extend the pin and start the G32 probing. I have no idea why G32 would send the Z so far off in the wrong direction. Z configs are here: [https://hastebin.com/xiqahuleka.coffeescript](https://hastebin.com/xiqahuleka.coffeescript)

I have found very little in the way of posts regarding the combination of BLTouch, CR10 and Smoothieware. What other parts of the configuration should I check more closely? 





**"Jonas Rullo"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2018 03:01*

Hey Jonas.  I have trouble with blocks of text, so if I miss something please break it out for me in bullet points. 



Have you read [smoothieware.org - zprobe [Smoothieware]](http://smoothieware.org/zprobe) ?  It's fairly comprehensive. 



What does M119 say for probe state?  



This is how you're supposed to configure a BLTouch: [https://plus.google.com/113792662927481823969/posts/QYfbMzt39yM](https://plus.google.com/113792662927481823969/posts/QYfbMzt39yM)



Break the issues down into bite sized chunks that we can understand and fix. 



It's all separate - 

1 - get the BLTouch to be commanded properly. 

2 - get probing working properly - the one machine I have a probe on we do 3 point leveling and it's all manually scripted in my start gCode.  Smoothieware is a bit different than other firmwares in this regard.  


---
**Jonas Rullo** *April 05, 2018 07:01*

Yes, I've been over the zprobe page many many times.



I setup BLTouch using the code graphic you linked to. Found same on [smoothieware.org](http://smoothieware.org). I'm using +5v and Ground next to the P1.23 pin shown. 



Pin up, pin down Gcode commands for BLTouch all work as expected.

Zmin is triggered when using G30.

Zmin shows triggered when up and not triggered when down.

The BLTouch seems to be working as normal.



I found that G30 only works when jogging the head down within 10mm of the bed which I remember being set in the config. 



M119 gives: 

X_min:0 Y_min:0 Z_min:0 pins- (X)P1.24:0 (Y)P1.26:0  Probe: 0

Probe on M119 will show 1 if I jog the nozzle to the bed and the BLTouch gets triggered. 



M114: 

ok C: X:0.0000 Y:4.0000 Z:-14.0000 E:0.0000

Z depends on where I started from, or where I issued G92Z0



I don't seem to be able to save where Z0 is even after jogging the head to the bed.



I moved the nozzle to the bed, I send G92Z0, then jog Z up a few mm, then M280S3.0, to un-trigger the BLTouch. Sending G32 goes up a little, across a little and down just short of triggering the BLTouch. I have leveling-strategy.rectangular-grid.initial_height 5

in the config. 

My other Z configs are in the link given above. 


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2018 15:21*

I know on the one machine I have with an inductive probe I am doing 3 point leveling (the bed is quite flat).  



I have the 3 points configured in the smoothie config file.  I call 



G32



then after that I set the height difference of the nozzle from the probe



G92 Z5.6



and this seems to work.  



I would recommend updating to the latest firmware and config file and doing a new bare minimum scratch build of the config to see if you can get the behavior to change at all.  



After that, you could also post to the Smoothieware g+ group.  I must admit the intricacies of probing behavior are a little bit outside my knowledge. 


---
*Imported from [Google+](https://plus.google.com/115922912883670862956/posts/gS83uGE3PEd) &mdash; content and formatting may not be reliable*
