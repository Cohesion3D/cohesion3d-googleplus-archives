---
layout: post
title: "I'd be grateful if another pair of eyes could cast them over my photograph of the power connect"
date: February 26, 2017 18:49
category: "C3D Mini Support"
author: "Bryan Hepworth"
---
I'd be grateful if another pair of eyes could cast them over my photograph of the power connect. It would appear to only fit one way around. There is a small hook/lip at the top and it does slide over the pins. A pinout list would also be quite nice.



Colours are Green Black Red Yellow in the picture



Thanks



Bryan



![images/056e1c85d355c5f867cdf20070197960.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/056e1c85d355c5f867cdf20070197960.jpeg)
![images/6b0f2da2c4e9a273e60f08ddee172082.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6b0f2da2c4e9a273e60f08ddee172082.jpeg)
![images/6734ca43015a28a6af95d1a07c6d9869.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6734ca43015a28a6af95d1a07c6d9869.jpeg)

**"Bryan Hepworth"**

---
---
**Chris Mann** *February 26, 2017 19:02*

Are the wires on power supply side labeled? 


---
**Bryan Hepworth** *February 26, 2017 19:20*

silkscreen says 4 G 5V L left to right


---
**Chris Mann** *February 26, 2017 19:42*

So thats 24v G 5v and L follow those wires from psu to the spot goes into cohesion board and make sure 24v G 5v and L are all going to proper spot. Just sec i will post pinout of cohesion board


---
**Chris Mann** *February 26, 2017 19:44*

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 19:56*

I can't see the connector as clearly as I want in those pics. I will say that it should be the same orientation as off the m2nano board. If there is a tab it should push toward the wall side of the connector on the c3d. 

If any doubt, meter the psu side for 24v and gnd and connect according to pin out diagram. Can also run own wires for 24v, gnd, L. You know about the bit for L to 2.5 bed - terminal? 


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 19:57*

Thanks **+Chris Mann**. And sorry to keep you waiting so long, I will get my machine up and running with a board from the new batch this Wednesday and work with you on calibration/ settings then. Thanks for your patience. 


---
**Bryan Hepworth** *February 26, 2017 20:13*

Cheers Chris, much appreciated. Ray I was going from the pdf instructions from the cohesion3d website so far. Not sure what the L to 2.5 bed - terminal is though?




---
**Bryan Hepworth** *February 26, 2017 23:59*

Chris thanks for that diagram - didn't realise there was one of those available, that was a Godsend and just proved the connections were correct


---
**Bryan Hepworth** *February 27, 2017 00:01*

**+Ray Kholodovsky** 

Ray could you explain the L to 2.5 bed terminal for me when you get five minutes? Pictures should follow tomorrow for you for your wiring.



Thanks

Bryan




---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2017 05:00*

One of the wires in the harness there is "L" on the psu. Connect L from the psu to the -bed 2.5 MOSFET pin. (Bottom screw terminal #4)

In config.txt, change the laser pin from 2.4! to 2.5 without the exclamation mark. 

I've explained this about 20 times in this group if you could look through some older comments/ threads. :)


---
*Imported from [Google+](https://plus.google.com/114125155844297036799/posts/cNQNxqv7Nzu) &mdash; content and formatting may not be reliable*
