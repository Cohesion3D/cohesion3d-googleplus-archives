---
layout: post
title: "Hey All, So I got my Cohesion3D Mini Board for my K40 from NY Maker Faire this year and just got around to installing it"
date: November 04, 2018 04:06
category: "C3D Mini Support"
author: "Zach Grossi"
---
Hey All,



So I got my Cohesion3D Mini Board for my K40 from NY Maker Faire this year and just got around to installing it. Install is completely finished and everything is working as intended (cuts, engraves, no problems with power adjustment, etc), except for the GLCD.



I've triple checked the connections, EXP1 on the GLCD goes to EXP1 on the Adapter, and EXP2 on the GLCD to EXP2 on the adapter, and the adapter is oriented on the correct header facing AWAY from the USB connector.



The issue is that even though the GLCD gets power since the backlight comes on, nothing comes up on the screen. I tried adjusting the contrast pot with no luck. I also checked the config on the microSD card and the config.txt says that "panel" (GLCD) is enabled. Any ideas on what this could be? Is my GLCD borked?



Any help is appreciated.





**"Zach Grossi"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2018 14:56*

Could you attach pics of all your wiring? 


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2018 14:57*

And which firmware are you using? 


---
**Zach Grossi** *November 04, 2018 23:09*

I'm using the default firmware (so Smoothie, i believe) and the default configuration. I never changed any settings on the microsd.


---
**Zach Grossi** *November 04, 2018 23:09*

![images/c079d5e7949a47226da015e53248d0a1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c079d5e7949a47226da015e53248d0a1.jpeg)


---
**Zach Grossi** *November 04, 2018 23:09*

![images/71685635c446377a5f2258532e7c6de9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/71685635c446377a5f2258532e7c6de9.jpeg)


---
**Zach Grossi** *November 04, 2018 23:09*

![images/bf3ffe987c4ba00bfef4b388adada381.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bf3ffe987c4ba00bfef4b388adada381.jpeg)


---
**Zach Grossi** *November 04, 2018 23:10*

![images/46375bad0f9ca0ecbf686b83534ab50f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/46375bad0f9ca0ecbf686b83534ab50f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2018 23:17*

Would you shoot a video of the screen and the board (the way you have it set up with the screen on lpsu is a nice view of both, as long as it won't short...)  as you turn on power to the board?   Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2018 23:18*

The next thing I would have you do is to reformat the memory card and put new firmware and config files on there, you can find them in a dropbox at the bottom of the install guide. 




---
**Zach Grossi** *November 04, 2018 23:33*

I can definitely give the reformat and reload a shot if you think that's a good next step


---
**Zach Grossi** *November 04, 2018 23:40*


{% include youtubePlayer.html id="EVdSW4r_ylM" %}
[youtube.com - Cohesion 3D Board and GLCD Powerup](https://youtu.be/EVdSW4r_ylM)


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2018 23:48*

Yes please.  Always have the board off before putting the card in/ out.


---
**Zach Grossi** *November 05, 2018 00:04*

Unfortunately, I'm met with the same result. GLCD powers on but no characters or graphics onscreen


---
**Ray Kholodovsky (Cohesion3D)** *November 05, 2018 00:08*

One last thing, try fiddling the trimpot in the lower left of the screen.  Might be a contrast thing. 



Failing that, email me (info at cohesion3d dot com) with your address and a reference to this thread and I'll send you a  new screen and GLCD adapter. 


---
**Zach Grossi** *November 05, 2018 00:15*

I did mess with that too. Before and after the reformat. Same results.


---
**Joe Alexander** *November 05, 2018 07:00*

could it be one of the oddball reprap glcd's with a backwards connector? was a weird issue that I believe prevented the data from posting on the GLCD.google it for more info about it, one fix was a small mod to the cable(snipping off the keyed part and inserting it in upside down). just dont do it unless you have checked all avenues first.


---
**Zach Grossi** *November 15, 2018 22:19*

**+Ray Kholodovsky**

So I just got the replacement board in today and I swapped it in. When I powered it up, i was met with some clicking from the speaker (roughly once a second?) and the display showed a couple of Chinese or Japanese characters and a bunch of "noise" (random pixels on). I powered the machine off, unplugged and replugged in the glcd adapter, and restarted the machine back up and now the glcd won't show anything again.



I tried reformatting the sd card again and loaded up the firmware and config files: same result.



I also tried a spare microSD card that I had laying around just in case: same result.



I'm at a loss for what the issue is...Is it possible something is wrong with the C3D Mini Controller itself? That would be weird because otherwise the machine runs perfectly fine.


---
**Ray Kholodovsky (Cohesion3D)** *November 18, 2018 02:16*

The C3D board itself has never before been the cause of the issue. It is possible, just hasn’t happened yet. 

I’m not too sure what’s going on. I’ll ponder and let’s figure out a plan over email. 


---
*Imported from [Google+](https://plus.google.com/100344235592492502233/posts/cVh7D3Us9RG) &mdash; content and formatting may not be reliable*
