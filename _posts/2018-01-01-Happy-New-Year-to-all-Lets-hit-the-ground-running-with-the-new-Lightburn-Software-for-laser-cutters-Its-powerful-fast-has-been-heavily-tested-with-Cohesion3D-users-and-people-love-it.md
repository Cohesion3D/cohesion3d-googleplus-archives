---
layout: post
title: "Happy New Year to all! Let's hit the ground running with the new Lightburn Software for laser cutters: It's powerful, fast, has been heavily tested with Cohesion3D users, and people love it"
date: January 01, 2018 17:53
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Happy New Year to all!  



Let's hit the ground running with the new Lightburn Software for laser cutters: [http://cohesion3d.com/lightburn-software/](http://cohesion3d.com/lightburn-software/) 



It's powerful, fast, has been heavily tested with Cohesion3D users, and people love it.  You can download the trial from our page there, and purchase it if you like what you see.  



Demo Video:


{% include youtubePlayer.html id="htlnCqnM2y0" %}
[https://www.youtube.com/watch?v=htlnCqnM2y0](https://www.youtube.com/watch?v=htlnCqnM2y0)



![images/32411e3124c0a12db6b6e6f1d0408e48.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/32411e3124c0a12db6b6e6f1d0408e48.jpeg)
![images/e7e42007d67e2a0212e09340f11d1548.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e7e42007d67e2a0212e09340f11d1548.png)
![images/ded4f7f31b7976590b77ca3a137d12b6.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ded4f7f31b7976590b77ca3a137d12b6.png)
![images/3b284dc5f21e993e28d420955019244d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3b284dc5f21e993e28d420955019244d.png)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Kelly Burns** *January 01, 2018 21:51*

I had hoped to get in on testing so I was waiting to try it. Finally got a chance today. Definitely a great step forward.  Your Board in an inexpensive laser combined with LightBurn and  you a have an excellent Low-Cost and reliable solution for beginners.    


---
**Russ “Rsty3914” None** *January 04, 2018 00:11*

Happy New Year !!


---
**Jeff Harbert** *January 07, 2018 13:59*

Just started playing with it yesterday and I like it a lot so far. Nice work.


---
**Abe Fouhy** *January 15, 2018 03:33*

Sweetness!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Xa423nngMV4) &mdash; content and formatting may not be reliable*
