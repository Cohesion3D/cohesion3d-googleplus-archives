---
layout: post
title: "is there anyplace I can connect a water flow sensor on the K40 replacement kit"
date: September 01, 2017 23:07
category: "K40 and other Lasers"
author: "Aaron Van Tassle"
---
is there anyplace I can connect a water flow sensor on the K40 replacement kit





**"Aaron Van Tassle"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 02, 2017 00:48*

I'd keep it separate. Either it has its own cutoff or go through an arduino, and you run that in series with the laser enable on the psu.  No flow = laser stops firing. It might also be possible to connect that signal (depending on what it is) to a pin on the C3D Mini board to pause the job if that happens. 


---
**Rien Stouten** *September 02, 2017 07:55*

**+Aaron Van Tassle** What kind of sensor are you using?


---
**Aaron Van Tassle** *September 02, 2017 21:21*

I was going to use a hall effect sensor, but guess I'll just go with the a pressure sensor I got from light objects


---
*Imported from [Google+](https://plus.google.com/+AaronVanTassle/posts/7PTwzKP5wK6) &mdash; content and formatting may not be reliable*
