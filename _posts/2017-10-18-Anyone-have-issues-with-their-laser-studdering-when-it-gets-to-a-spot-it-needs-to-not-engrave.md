---
layout: post
title: "Anyone have issues with their laser studdering when it gets to a spot it needs to not engrave?"
date: October 18, 2017 03:45
category: "General Discussion"
author: "tyler hallberg"
---
Anyone have issues with their laser studdering when it gets to a spot it needs to not engrave? Its almost like it doesn't know what to do. Smooth until it hits a white spot stops for a millisecond goes over it and starts to engrave again, even though I have Laserweb to "Burn white" it seems to stop to turn the last off and then back on again. I think this may be part of my issue with the picture shifting. 





**"tyler hallberg"**

---
---
**Ariel Yahni (UniKpty)** *October 18, 2017 14:56*

Is this an image that you removed the background? Could it be there's tiny dots still there?


---
**Georg Petritsch** *November 12, 2017 08:26*

Yes i have the same issue. Really strange behavior. I figured out that if you reduce speed to a minimum of 8mm/s or below, it runs smoother. For me it looks like smoothie has problems with buffering.?


---
**tyler hallberg** *November 12, 2017 16:54*

I completely delete backgrounds on images when engraving. I have since stopped using Laserweb and have not had the problem again.


---
**Georg Petritsch** *November 12, 2017 17:08*

What do you use instead of laserweb?


---
*Imported from [Google+](https://plus.google.com/107113996668310536492/posts/cCxrdFKNHiH) &mdash; content and formatting may not be reliable*
