---
layout: post
title: "I have a K40 with Cohesion3D Mini board I just installed per the instructions on your site, it was working fine for about 2 cuts then i turned off the laser came back and it was no longer responding to commands"
date: November 21, 2017 01:30
category: "C3D Mini Support"
author: "Matt Varian"
---
I have a K40 with Cohesion3D Mini board I just installed per the instructions on your site, it was working fine for about 2 cuts then i turned off the laser came back and it was no longer responding to commands. I could still connect to the board, and when i hit a control key in laserweb 4 to move the x/y axis nothing happened. After some trouble shooting I pulled the micro SD card and tried to load it on my computer(linux) it appeared to only be 512mb(not the 2gb it's marked as) after trying many things I can not read the SD card and can only assume that it's failed/corrupted. I had another micro SD card, formatted it to fat32 put on the firmware/config.txt files from your site tossed it back on the C3D and boom it's working again like before. Is SD card failure a regular thing? Is there anything I can do to avoid this? If this happened in a few months I wouldn't sweat it, but at this point the C3D board has been on for under 1hr total. 





**"Matt Varian"**

---
---
**Alex Raguini** *November 21, 2017 03:06*

Make sure you dismount the SD card from your computer prior to shutting down the laser.  I had a similar issue until I made certain the SD card was dismounted. 




---
**Matt Varian** *November 21, 2017 12:18*

Hmm, I never see the card "mount" from my computer when using the laser, again using linux so maybe a bit different but I'll look into it.  I connect to the laser via usb/serial but never see the SD card that way.




---
**Joe Alexander** *November 21, 2017 19:04*

I believe there is also a line in the config.txt file that will prevent it from mounting over usb, perhaps this will fix future issues?


---
**Ray Kholodovsky (Cohesion3D)** *November 21, 2017 23:09*

Hello! 



First off, sorry that this happened. I hope with a new card you will remain up and running well. 



The removing power from the board while the card is mounted could certainly cause corruption. 



I personally am of the opinion that the K40 Power Supply is complete garbage with regard to providing enough power (the 1 amp it should put out is barely enough for our 2 motors and the board logic) and the generally unclean nature of the power that is put out... 



When a person is experiencing any sort of stability issue, installing a separate 24v6a power supply to power the board and motors is usually the first thing I recommend. 



I do not believe there has been a single report of SD Card Corruption/ failure from a C3D board used outside of the K40.  We have a larger userbase of K40 upgrades than of 3D Printers, so I'm not discounting that it might just be "less issues due to a smaller sample size", but still. 



What Joe has suggested is actually a firmware build disablemsd - [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/FirmwareBin) - however we are using the CNC build, and such a build is not available from the devs. 



[http://smoothieware.org/compiling-smoothie](http://smoothieware.org/compiling-smoothie)  

What I am thinking is:



make clean

make CNC=1 DISABLEMSD=1



might do it.  But we don't know if this is the cause of your problem and so if that will solve it.  But I think it can help certain other users particularly those of Mac OS. 



Sorry, not familiar enough with any Linux distro to know whether it would mount automatically, on Windows and Mac OS it does for sure.



Now for the fun answers:

Some people (outside of C3D) say that Smoothie chews SD cards for breakfast. 

I have personally flashed cards (as in, I've handled them myself, know they were working, put files on them then put them in anti static packing, and the person reported it was dead when they received it. 

I'm at a loss with things like this. I think it's still around a 1-2% rate for out of the box problems with the card. 



 Please keep me posted on any developments. 


---
**Alex Raguini** *November 22, 2017 06:16*

I can personally attest to card corruption on windows 10 and mac osx if the card is not dismounted prior to shutting down the controller.  Sometimes the corruption causes erratic behavior, other times the controller fails to boot.  Discipline in dismounting the card prior to controller shutdown it important.




---
**Ray Kholodovsky (Cohesion3D)** *November 22, 2017 14:43*

**+Alex Raguini** I built the CNC and NOMSD version, will send to you for testing. 


---
**Alex Raguini** *November 22, 2017 18:42*

I'll test today.




---
**Alex Raguini** *November 22, 2017 20:31*

**+Ray Kholodovsky** I sent you PM in email.


---
*Imported from [Google+](https://plus.google.com/+MattVarian/posts/UzSFPHiDBbj) &mdash; content and formatting may not be reliable*
