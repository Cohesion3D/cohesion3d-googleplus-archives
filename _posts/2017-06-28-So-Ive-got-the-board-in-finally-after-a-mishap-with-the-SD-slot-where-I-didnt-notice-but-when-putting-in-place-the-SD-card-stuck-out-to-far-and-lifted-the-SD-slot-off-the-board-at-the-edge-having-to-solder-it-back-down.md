---
layout: post
title: "So I've got the board in finally after a mishap with the SD slot where I didn't notice but when putting in place the SD card stuck out to far and lifted the SD slot off the board at the edge having to solder it back down"
date: June 28, 2017 22:48
category: "C3D Mini Support"
author: "Tim Anderson"
---
So I've got the board in finally after a mishap with the SD slot where I didn't notice but when putting in place the SD card stuck out to far and lifted the SD slot off the board at the edge having to solder it back down.



I have a different problem now (unrelated as worked fine after putting the card reader back) where X doesn't move, I have 4 drivers on the board, two unused and none of them work when put into the X spot, Y works great and have switched X and Z round in the config file for now so my X motor is running from Z.



Any ideas since the drivers all appear to be fine what might have popped on the board and how I did it?





**"Tim Anderson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 28, 2017 22:52*

Walk me through the operational lifecycle of the board? 

When did you get it, did the X socket work ever, how long have you been using it: before and after the SD socket mishap, etc...


---
**Tim Anderson** *June 28, 2017 23:00*

Oh it isn't very old, I only received it yesterday!



The X socket did work fine yes, I've been trying to set it up and had gotten pretty close but earlier it did a raster and after it finished I tried to send the head to home and noticed an error, it hadn't moved the X at all, at this point I could not jog or anything, swapping the drivers round didn't help but it is working fine now with the X plugged into Z.



Hmmm, used it a little before the SD card problem and a fair bit after trying different settings, The SD card problem was just the two solder points at the front of the socket that hold the shield in place, the actual pins were untouched.


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 02:05*

Yep, so this is an interesting one.  

1. Our assembler tests all the boards.  So they verified that the mosfets turned on, all the motors turned, and that the endstops were read. 

2. Then more recently we started "preloading" the boards with the drivers to order, which means if you received your board with the green driver modules installed, then it was on our bench and all motors turned, etc... 

3. So if the X socket has now stopped working, chances are the issue will trace back to the pins of the microcontroller. Perhaps it tried to pull too much current 



In conclusion, you did the right thing re-keying the X to use an alternate socket.  The good news is that we have plenty enough extra pins on the Mini board - if you decide to add a Rotary or Z table later, chances are that at least one of those things if not both will require an external driver like TB6600 and we can pull from 3 spare pins on the board to wire that, easy. 



I will pre-emptively say this.  The power supply in the k40 is shit.  I believe it to be the cause of a lot of unexplained issues that have been happening.  You may as well upgrade that sooner rather than later to avoid issues.  Get a 24v 6a and use that to power the Mini.  Been done plenty of times by other users. 


---
**Tim Anderson** *June 30, 2017 06:33*

That's pretty much the conclusion I came to, thanks Ray and in no way was I blaming it on a faulty board, it did work fine when I first got it lol



I was wanting to try GRBL as I can't get a smooth raster with Smoothie but will have to recompile to move X to the other pins... 



All good fun, I will look into the power supply also, I noticed that mine is one of the ones without a fan. 


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 13:18*

Are you streaming your raster over USB or have you tried putting it on the memory card and running it via the GLCD? 


---
**Tim Anderson** *June 30, 2017 14:58*

Both are very stuttery (although better from sd), actually it's better at say 0.2 upwards but then I get lines inbetween each pass so I tried 0.1, I don't get the lines but it is almost like it can't keep up.. 



It is a little better when running really slowly at say 40mm/s but it would take hours to finish a small piece at that speed, I hear GRBL is quicker at similar quality?


---
**Tim Anderson** *June 30, 2017 18:43*

Ok so compiled GRBL with it using A in place of X, much much better! I have a couple of questions but I got an almost passable result at first try 😊


---
*Imported from [Google+](https://plus.google.com/109492805829132327540/posts/bmn2EzLvf4C) &mdash; content and formatting may not be reliable*
