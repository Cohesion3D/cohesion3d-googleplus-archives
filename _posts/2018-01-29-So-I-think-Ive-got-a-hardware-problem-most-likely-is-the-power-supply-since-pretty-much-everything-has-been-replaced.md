---
layout: post
title: "So... I think I've got a hardware problem, most likely is the power supply since pretty much everything has been replaced"
date: January 29, 2018 19:53
category: "C3D Mini Support"
author: "timne0"
---
So... I think I've got a hardware problem, most likely is the power supply since pretty much everything has been replaced.



since installing I've only had a max of 16ma of power.  I started with 21ma when I had the original board, then when I installed the ramps board and faffed with lots of firmware, eventually it was at 16ma.  I then upgraded to Cohesion3D and it's working well, but still at 16ma.  However the tube shortly after decided to crack for no discernible reason, though there were two cracks, one was where the water piddled out, the other in the co2 chamber, so I assumed the Co2 res had leaked and we'd lost power that way.  Replaced the tube with a 50w tube. Drilled lots of holes etc and connected it up.  Power? 21ma.  Woo.  Put software restriction in and limited it to 95%, power now? 16ma.  Change power to 100% again... 16ma.  Do two cuts, first cut 25%, 4ma.  Engraves fine at nice slow speed.  Next day, repeat same cut, look down and to my horror it's now running at 35ma.



Is this definitely a power supply issue?  It's another bleeding expense if so, but don't want to do it unnecessarily...









**"timne0"**

---
---
**Don Kleinschnitz Jr.** *January 29, 2018 21:53*

This is hard to say. If this is a standard LPS you can monitor the IN pin with a DVM and see if the voltage is changing properly. 0-5V is the power control range on that pin.



Need a picture of your LPS connectors and the control panel to best advise.


---
**timne0** *February 06, 2018 09:49*

I'm cutting vectors in g-files with a set power of 70% and on a straight line it goes from 10ma for a random period of time (like a minute or two) then to 32ma for a while, then might sit back at 10ma for a few complete cuts (15-20 minutes).  I'm suspecting it's the power supply coil itself rather than the board?




---
**Don Kleinschnitz Jr.** *February 06, 2018 12:34*

**+timne0** see note above regarding pictures of your laser supply's connectors.

Did you check the voltage on IN?

What coil do you mean?


---
**timne0** *February 24, 2018 08:05*

I was talking about the flyback transformer. But it makes sense that the laser supply isn't doing the best job for the 5v board supply. I'm adding the 24v this week.


---
**Don Kleinschnitz Jr.** *February 24, 2018 15:13*

I would watch the IN voltage during the change in current you describe above. If IN is not changing then I would suspect the LPS.



I assume you are driving a 50w tube with a 40w LPS?

If this is a stock 40w LPS the operating current is 16ma and MAX is 22ma. I would not run above 16-18. 

In any case the troubling symptom is that the current is not stable?


---
**timne0** *February 27, 2018 14:54*

**+Don Kleinschnitz** Correct, The issue is that I start running at 60% power and it's running at 18ma, then half way through a job, it drops to 11ma.  So I then stop, set to 95% and it'll continue to burn at 14ma.  Then it'll shoot up to MAXma randomly.  Will be setting it up tonight to run off the separate powersupply for the board.  If I continue to get the same problem I will replace the power supply as a matter of course.


---
**Don Kleinschnitz Jr.** *February 27, 2018 15:51*

**+timne0** are you thinking that your controller is changing the PWM control? If it is not repeatable I would doubt its the controller.

I would monitor the IN on the LPS to see if it is changing.


---
**timne0** *February 27, 2018 15:54*

It's not repeatable on the same cut using the same g-code. The point of the thread was to check it was likely the power supply and not something obvious I missed. I assumed when you pointed out the issue with the 24v that it was likely related, so I've not ordered the 50w powersupply yet. I assume you mean it's unlikely to be an issue caused by the 5v instead of 24v and therefore I need the new powersupply as it's probably the flyback transformer failing?


---
**Don Kleinschnitz Jr.** *February 27, 2018 16:14*

**+timne0** 

I am suggesting that you should put a DVM on the LPS '"IN" pin and see if it is changing during the job. Other than the gcode changing the power this is the only other thing that could be modifying the laser power. 

The 24V and 5V do not likely have anything to do with changing the lasers power. 

If the "IN" pin is not changing during the job then it likely is the LPS.


---
**timne0** *February 27, 2018 16:20*

OK.  I'm assuming this is fine with a hand held multimeter - I don't have a spare dedicated DVM about.  What's the voltage I should expect to see on "IN"?


---
**Don Kleinschnitz Jr.** *February 27, 2018 18:47*

A multimeter on volts scale should be fine. It should read from 0 to 5v when measured to the supplies gnd. 


---
**timne0** *February 27, 2018 19:02*

Ah not a 20kv line. Good stuff


---
**Don Kleinschnitz Jr.** *February 27, 2018 19:23*

**+timne0** this is the pin labeled "IN" on the LPS input. Stay away from the output :). 


---
**timne0** *February 28, 2018 19:41*

Video! 
{% include youtubePlayer.html id="sJvT4-zI8No" %}
[https://youtu.be/sJvT4-zI8No](https://youtu.be/sJvT4-zI8No)


{% include youtubePlayer.html id="sJvT4-zI8No" %}
[youtube.com - K40 ln monitoring](https://youtu.be/sJvT4-zI8No)


---
**timne0** *February 28, 2018 19:41*

That's engraving 


---
**timne0** *February 28, 2018 20:26*

This did actually replicate the fault. All at 70% power, 700 speed. 
{% include youtubePlayer.html id="k1apLyMd-CI" %}
[https://youtu.be/k1apLyMd-CI](https://youtu.be/k1apLyMd-CI)


{% include youtubePlayer.html id="k1apLyMd-CI" %}
[youtube.com - K40 power supply](https://youtu.be/k1apLyMd-CI)


---
**timne0** *February 28, 2018 20:27*

At exactly 1 minute it dropped from 18ma to 10ma. No change in g-code 


---
**timne0** *February 28, 2018 22:23*

**+Don Kleinschnitz**


---
**Don Kleinschnitz Jr.** *March 01, 2018 01:22*

**+timne0** 

You can watch your video at slow speed at this location:

[youtubeslow.com - Watch in slow motion](http://www.youtubeslow.com/watch?v=k1apLyMd-CI)



I can see that your LPS runs at about 18ma with 2.11 Volts on IN. Then at times it drops to 10 ma while nearly at the same voltage. I also observed that the ma-meter spikes to a negative value which is unusual while the IN is 2.16. (see snapshot below).



My first question is why is the IN voltage changing at all? That is controlled by the POT which you are not adjusting. 

Are you sure your are measuring the IN pin and is there anything else connected to it? 

Although the current seems to be acting independent of the IN value, IN should not be changing.



How is your controller controlling power to the LPS???  



I need to understand more about your configuration to be helpful:



1. Picture of the LPS input connector so I can tell what kind of supply we are troubleshooting

2. What controller are you using

3. How is the controller controlling the LPS power.





Here is a slow motion image capture showing negative current:

[https://photos.app.goo.gl/Tom1EdgfV9UqHMmi1](https://photos.app.goo.gl/Tom1EdgfV9UqHMmi1)




---
**Chuck Comito** *March 05, 2018 02:15*

**+timne0**​, I was wondering if you've gotten anywhere with this issue? I'm going through the same thing. **+Don Kleinschnitz** pointed me to the conversation. Unfortunately I haven't been able to find the time to test this weekend but I thought I'd ask you about your situation and if you've come up with a solution yet?


---
**timne0** *March 05, 2018 06:10*

Sorry, I've been a bit busy, cutter is at the hackspace and I'm only there 3 days a week. Back there tonight. In regards to the negative current, I suspect it's because it's a cheap multimeter and it's difficult to film and hold the multimeter onto the lps one handed.

I think it's changing because of the way my Lps is controlled by the board. I'll try and draw it up tonight.


---
**timne0** *March 05, 2018 06:14*

NB ray originally helped me setup and said it was an odd LPS and needed special setup. No POT connected, ended up cutting up the three wire connector and running the 3rd line off it to the LPS to control it. I suspect that's the answer.


---
**Don Kleinschnitz Jr.** *March 05, 2018 11:11*

**+timne0** the negative current I was referring to was on the panel meter not the DVM.

Perhaps your controlling the supply with its "IN" pin.



Don't forget a picture of the LPS connectors :).






---
**timne0** *March 05, 2018 12:55*

That's the LPS pins. Yes controlling via in through board. Haven't upgraded firmware or anything, is this something that has been fixed?![images/a2f5fe3eea07eb0b7243f7e9c874a3f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a2f5fe3eea07eb0b7243f7e9c874a3f4.jpeg)


---
**timne0** *March 05, 2018 12:55*

Powersupply![images/ec63a8b32fb9317c11918dfb7fc392aa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec63a8b32fb9317c11918dfb7fc392aa.jpeg)


---
**Don Kleinschnitz Jr.** *March 05, 2018 17:11*

**+timne0** 



Your supply appears to be this one:

[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/hprKvV4bmztQ6tqq1)



1.) It looks like you have a white (-.-.-) wire on the IN yet you said you have no pot? Where is this connected to?



2.) You also have a blue wire on the L pin, where does that go to.


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/7SaVYWoh9Mo) &mdash; content and formatting may not be reliable*
