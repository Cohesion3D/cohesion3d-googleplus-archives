---
layout: post
title: "I am still new to this and on a bit of a learning curve"
date: June 14, 2017 19:23
category: "C3D Mini Support"
author: "Teamthom playz"
---
I am still new to this and on a bit of a learning curve. I Updated my K40 laser cutter last week with the C3D mini upgrade kit. All working fine easy to install. Until last night it would not connect to the computer and there is now just the red led showing instead of the green flashing see pic. Is there anything i am missing? Any help would be much appreciated. 

![images/a6708c06299f75093d412db6183d8052.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a6708c06299f75093d412db6183d8052.jpeg)



**"Teamthom playz"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 19:38*

Walk me through some specifics please. 

How long have you been running ok, did anything different this time, just all the details you can think of please. 


---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 19:45*

More questions to follow: 

Just the red LED is on? Can you check closely, perhaps if the LED right below it is on as well?  Need an accounting of all the lights please. 


---
**Teamthom playz** *June 14, 2017 20:09*

thanks for the fast reply. just had another check and it is only the red LED on there are no others. I had the GLCD display attached but removed for use of taking the above photo. the unit that I have originally came with the 6 buttons for adjusting laser power as below.

![images/145ce6bbd44a38b236520d87db90c9da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/145ce6bbd44a38b236520d87db90c9da.jpeg)


---
**Teamthom playz** *June 14, 2017 20:14*

when I updated I made a new panel. I was going to recut out of perspex after I had got the size right. the top button is not connected to anything I was going to use it for air assist. I have been using it with a Mac mini and laser web. all has been working fine and I was more than happy with the results. I went to use last night turned on the machine but was unable to connect with laser web. when checked for loose connections found the red light on.



![images/5d16eef9d059135636a0520d9cd460de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5d16eef9d059135636a0520d9cd460de.jpeg)


---
**Teamthom playz** *June 14, 2017 22:05*

Just been having another look. The computer does not see the Sd card as a drive where it did before. If I disconnect the power plug and connect the usb cable to the computer nothing no lights illuminate on the board. I know the USB use to power the GLCD if the main power was switched off this no longer happens. 


---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 22:08*

Ok, can you try the MicroSD Card in a reader directly connected to the computer?  



Also, can you take the board out, disconnect the motor drivers/ any other modules, and hook that up to USB? 


---
**Alex Hodge** *June 15, 2017 00:00*

Weirdly, this just happened to my board as well. I was fine tuning the setup, trying out GRBL-LPC. Board stopped on a x axis move. Initially I thought it might be the stepper driver. Powered off, replaced the stepper driver with a spare I had on hand and powered it back up. Only 24v light, no others and computer doesn't see it via USB. Powered off again. I pulled and reseated the SD card. No change. Tried a different USB cable. No change. Tried no 24v just USB. No change. Pulled the x axis stepper motor cable and driver. No change. Disconnected all cables and components, plug via USB. No change. Hoping whatever fixes teamthom's board works for mine too. Tried a different USB port too (front to back), forgot to say that...


---
**Alex Hodge** *June 15, 2017 01:43*

I don't want to hijack this thread, but my SD card reads and writes fine in a card reader. I tried replacing the [firmware.cur](http://firmware.cur) with a clean copy of [firmware.bin](http://firmware.bin). no change. I think my micro is dead...


---
**Teamthom playz** *June 15, 2017 06:19*

Just checked the Sd card and without the motor drivers. The sd card id readable in the computer and the file looks the same as the downloadable one.  I have removed all the modules and cables.  Nothing is illuminated


---
**Teamthom playz** *June 15, 2017 07:50*

Are there any pinouts that can be checked? i have a multimeter. 


---
**Alex Hodge** *June 15, 2017 14:56*

Same here. BTW, physical inspection of the board doesn't show any scorched or damaged looking components.


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 14:58*

There is a pinout diagram on the documentation site. With everything disconnected, please use continuity mode on the multimeter and check for shorts between Vin and Gnd, 5v and Gnd, and 3.3v and Gnd. 



Gnd is the top left screw terminal, lower pin. Vin is the upper pin. 5v can be found at header at the bottom left. For 3.3v you need to touch the "head" of the component AMS1117


---
**Alex Hodge** *June 15, 2017 15:28*

**+Ray Kholodovsky** Thanks, I'll take a look after work today and post up my results.


---
**Alex Hodge** *June 15, 2017 15:34*

**+Ray Kholodovsky** Looking at the pinout diagram, AMS1117 is near the middle of the board? I don't see it specifically called out, but the shape looks right...


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 15:35*

It's under the "e" of Cohesion and to the left of the LCD expansion header. 


---
**Alex Hodge** *June 15, 2017 15:37*

**+Ray Kholodovsky** Yep. 3 legs and a tab on top yeah?


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 15:42*

Yep. And top tab is what I need you to touch. 


---
**Teamthom playz** *June 15, 2017 18:32*

Hi ok. Just to check as the pin marks on the board show gnd as the top pin and Vin as bottom. If that is as shown i have continuity between GND and 3v top tab. When i check between the  Vin and GND in the top left i have a Mohm reading. When checking the top GND with the bottom left 5v there is continuity between GND-GND and 12 Kohm between Gnd and 5v pin. As we are looking for shorts, the 3v looks like the culprit.  Any further advise would help as in is there a component  that may have popped?  


---
**Alex Hodge** *June 15, 2017 23:02*

**+Ray Kholodovsky** 3.3v is shorted to ground. I've got no continuity between the others...but 2ohms resistance between the top tab of the AMS1117 and any ground on the board.


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 23:21*

Please email me using the contact form on our website and reference this issue, your names, and order numbers please. 


---
**Alex Hodge** *June 15, 2017 23:35*

**+Ray Kholodovsky** Done. Thanks Ray, looking forward to figuring this out and getting back up and running.


---
**Teamthom playz** *June 16, 2017 09:17*

Thanks for your help


---
**John Milleker Jr.** *June 21, 2017 18:59*

What was the outcome of this? I just had the same thing happen to me. It seemed to happen when I disconnected the LCD but I had also changed some configs on the text file as well and maybe that was the issue as it was for you.



Now I'm completely dead. VMOT light and nothing else.






---
*Imported from [Google+](https://plus.google.com/115381163880507444934/posts/Ud4Ws7i57mp) &mdash; content and formatting may not be reliable*
