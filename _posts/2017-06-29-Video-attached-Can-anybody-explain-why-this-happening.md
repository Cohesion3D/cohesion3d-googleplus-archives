---
layout: post
title: "Video attached - Can anybody explain why this happening?"
date: June 29, 2017 22:14
category: "C3D Mini Support"
author: "Adam J"
---
Video attached - Can anybody explain why this happening? C3D mini installed and was previously working fine. Jittering / Stuttering while cutting, producing a horrible cut. Cut is a vector and has previously cut perfectly fine. Cut is on card at 64 m/s.



Any help greatly appreciated.





**"Adam J"**

---
---
**Ashley M. Kirchner [Norym]** *June 29, 2017 22:56*

Missing a lot of information here: What kind of file are you trying to cut, jpeg, png, bmp, svg? What software are you using? What settings are you using?


---
**Adam J** *July 04, 2017 17:05*

This turned out to be a dodgy driver board. After a hard reset, everything seemed okay. Later on, I couldn't cut through hardly anything. Checked my alignment which was spot on, cleaned mirrors and lenses. Still couldn't cut through barely anything. Decided to replace both drivers and the problem went away.


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/caDFEg15qU5) &mdash; content and formatting may not be reliable*
