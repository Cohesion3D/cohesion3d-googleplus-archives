---
layout: post
title: "I ask how to replace the existing motor in K40"
date: June 14, 2018 09:25
category: "K40 and other Lasers"
author: "Pavel Krybus"
---
I ask how to replace the existing motor in K40. I can not find this type on ebay. Thank you.I do not know his voltage or current Thank you

![images/e25310a7d80ff5728ced00513f0423ec.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e25310a7d80ff5728ced00513f0423ec.jpeg)



**"Pavel Krybus"**

---
---
**Don Kleinschnitz Jr.** *June 14, 2018 13:03*

See these specs on motors they <b>may</b> match yours.

Search the community as someone else has replaced their's.

+Frederick Deryckere

[donsthings.blogspot.com - K40-S Motors](http://donsthings.blogspot.com/2016/06/k40-s-motors.html)


---
**Anthony Bolgar** *June 14, 2018 18:59*

ANy basic Nema17 that uses around 1A of current and is the same physical size will do.




---
**Jim Fong** *June 15, 2018 13:54*

**+Anthony Bolgar** just to add, unless they changed it recently, the original stepper motor is a 0.9degree 400 full step motor. 



No problem replacing with a more common 1.8degree 200 full step motor but you need to change the step per mm setting in smoothieware.  


---
**Anthony Bolgar** *June 15, 2018 14:03*

Wouldn't 1.8*200 be the same as .9*400?




---
**Anthony Bolgar** *June 15, 2018 14:04*

Never mind, I'm a moron this morning.






---
**Don Kleinschnitz Jr.** *June 15, 2018 14:40*

I believe you can get direct replacements from the link I provided above?


---
**Pavel Krybus** *June 15, 2018 17:08*

tank you


---
*Imported from [Google+](https://plus.google.com/110535083322289727638/posts/FRqxq9syu45) &mdash; content and formatting may not be reliable*
