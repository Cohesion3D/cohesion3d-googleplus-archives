---
layout: post
title: "I installed my C3D in my new K40 (digital panel with M2Nano) and LaserWeb will let me move X and Y in the direction of the on screen buttons however Home All does not seem to work and as best I can tell the limit switches are"
date: October 29, 2017 22:15
category: "C3D Remix Support"
author: "David Fruehwald"
---
I installed my C3D in my new K40 (digital panel with M2Nano) and LaserWeb will let me move X and Y in the direction of the on screen buttons however Home All does not seem to work and as best I can tell the limit switches are being ignored.  Using the supplied Smoothie config from the laser upgrade bundle.





**"David Fruehwald"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 29, 2017 22:17*

Hard limits are off by default.  Jogging would indeed not stop if it hit a switch. 



Run G28.2 to actually home to the switches.  Head should move to rear left corner. 



After you are up and running I can walk you through enabling the hard limits in config, but first I would like to get the stock setup going. 


---
**David Fruehwald** *October 29, 2017 22:34*

is there a G-Code screen in LaserWeb or do I need to use Pronterface?


---
**Ray Kholodovsky (Cohesion3D)** *October 29, 2017 22:40*

At the bottom right. 


---
**David Fruehwald** *October 29, 2017 22:44*

I checked the stops with a multimeter and they worked as expected.  I figured out how to send G-Code and sent G28.2 and it did something.  Made lots of thumping noises and looked like it was doing some sort of calibration.


---
**Ray Kholodovsky (Cohesion3D)** *October 29, 2017 23:17*

It should touch twice (touch, back off slowly, touch again) then stop. 


---
**David Fruehwald** *October 30, 2017 00:21*

My laptop crashed on me so I have to fix that before I continue.


---
**David Fruehwald** *October 30, 2017 03:04*

Got laptop sorted enough to get LaserWeb going again.  It appears to work with G28.2 it’s just making extra noise because I don’t have the gantry tightened down completely.


---
**David Fruehwald** *October 30, 2017 15:05*

I think the noise I’m hearing is caused by insufficient stepper current.  I’ll adjust that to see if I can get smoother movement.


---
*Imported from [Google+](https://plus.google.com/110578764378886132074/posts/5NSXaBmLCpc) &mdash; content and formatting may not be reliable*
