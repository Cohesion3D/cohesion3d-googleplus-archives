---
layout: post
title: "When using GRBL-LPC for Raster engraving the laser will not fire"
date: April 28, 2018 00:51
category: "C3D Mini Support"
author: "Wayne Ryan"
---
When using GRBL-LPC for Raster engraving the laser will not fire. Everything else is ok in the Cohesion3D Smoothie but just wont fire the laser. When I try to HOME in the move screen it stays on BUSY 100% and wont clear. I have to unplug the USB from the board to re-boot. Anyone have this problem with a FIX for it. Hopefully with an external PSU will fix it but not sure





**"Wayne Ryan"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2018 01:20*

2 common things:

Don't use the USB cable that came with the K40, it can cause the not connecting issue. 

Did you change the device in Lightburn from Cohesion3D Smoothie to Grbl? 


---
**Wayne Ryan** *April 28, 2018 01:56*

**+Ray Kholodovsky** yes im on the grbl...headed to store now for high speed usb. Thanks for quick reply


---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2018 02:27*

Oh there's also a different driver for grbl-lpc... It's in our docs article... 


---
**Wayne Ryan** *April 28, 2018 05:00*

Thanks Ray I was just getting ready to pull my hair out until I saw your reply about the driver


---
**Wayne Ryan** *April 28, 2018 12:14*

After  I copied the new .bin file the board just gives one continuous beep. The computer will not even recognize the board now. What do I need to do to get the original Firmware back on the SD Card


---
**Wayne Ryan** *April 28, 2018 12:21*

**+Ray Kholodovsky** After  I copied the new .bin file the board just gives one continuous beep. The computer will not even recognize the board now. What do I need to do to get the original Firmware back on the SD Card


---
**Ray Kholodovsky (Cohesion3D)** *April 29, 2018 21:35*

The screen will not work on grbl firmware  and as I said there is a different driver required that you'll have to install. 

Do you not see the COM port in device manager? 


---
**Richard Magnanti** *May 08, 2018 01:26*

**+Ray Kholodovsky** 


---
**Richard Magnanti** *May 08, 2018 01:31*

Wayne, did you get this resolved?  I installed GRBL today.  When I home it homes to X but stalls on Y. It’s not stopping when it hits the Y home switch.  I also get “Busy 100%” on the move screen.  I reverted back to Smoothie and it working flawlessly. 


---
**Wayne Ryan** *May 08, 2018 03:09*

**+Richard Magnanti**  No I gave up I couldn't locate the driver that Ray mentioned and everytime I tried to flash the SD Card it just gives a continuous beep. So I went back to original Firmware for Smoothie. Does the GRBL need to be on a separate SD Card with config files? Just need the driver for GRBL-LPC  so I can go at it again


---
**Richard Magnanti** *May 08, 2018 15:09*

Do you recall when you homed it if it stopped when it hit the switches?  I’m struggling with GRBL also. Mine travels toward home and stops as expected when it hits the X switch.  When it hits the Y switch, it “grunts”, attempting to continue in that direction.  Like you I noticed the display is stuck at 100% and BUSY. 


---
**Wayne Ryan** *May 09, 2018 03:04*

I can't even get the GRBL to recognize on the SD Card...So it won't home at all just Alarms with a solid beep. Can you direct me to the driver you installed and then I can give you the info on what mine does


---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2018 02:19*

VCOM_lib/usbser.inf is located in this dropbox: 



[dropbox.com - firmware_grbl-lpc_3axis_c3dmini](https://www.dropbox.com/sh/vjtzgc2y1q9ytd1/AADFZ52Eneu_nDkHSChY7jsla?dl=0)



If you google "manual driver install" there are a number of resources to show you how to install the inf driver in windows.  



Grbl-lpc does not support the screen so you may want to disconnect that for the moment. 


---
**Wayne Ryan** *May 10, 2018 02:50*

**+Ray Kholodovsky** thanks I appreciate it...Do I need to do the GRBL-LPC on a seperate SD Card or can I use the original one




---
*Imported from [Google+](https://plus.google.com/101642450378746266949/posts/JUwNUip8S8p) &mdash; content and formatting may not be reliable*
