---
layout: post
title: "Hi Could you please tell me for the Cohesion3D Mini' on the K40 co2 laser what would be the best windows operating system to install on my dedicated pc??"
date: January 07, 2017 17:46
category: "General Discussion"
author: "Bill Parker"
---
Hi 

Could you please tell me for the Cohesion3D Mini' on the K40 co2 laser what would be the best windows operating system to install on my dedicated pc?? and then what is the most essential and useful software I can have set up ready for the day the conversion is done??.

Thanks

Bill





**"Bill Parker"**

---
---
**Carl Fisher** *January 07, 2017 18:19*

Any modern operating system would be fine. It really comes down to the software chain you plan to use. LaserWeb is probably the most powerful and most popular and it runs fine on Windows, Linux, MAC and even on a Raspberry Pi. 



The C3D board is hardware and uses serial port communications over USB to the best of my knowledge so the root operating system is pretty much irrelevant.



I would say GIMP, Inkscape and LaserWeb and you're good to go for just about anything you could want to do with a laser.


---
**Griffin Paquette** *January 07, 2017 18:30*

Carl pretty much covered it. The configuration for the board can be done on any operating system as it's just modifying a text file. After that it really boils down to the software you choose. LaserWeb is quite common and a great choice and like Carl said, runs on nearly everything. 


---
**Todd Fleming** *January 07, 2017 19:29*

Some people hit webgl performance problems on Linux


---
**Bill Parker** *January 07, 2017 21:49*

Thanks for replies. The mini pc I was looking at using has windows xp on it that i used for my grbl router but laser Web won't run on xp as far as I know that's why I was asking what operating system was best as I take it some software is too old for an up to date operating system and some software is too new for the likes of windows xp. 




---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 08:30*

Windows 7 or 10.  I don't use 10 personally, but either one should be good. 


---
**Bill Parker** *January 11, 2017 08:44*

Thanks I will go for 7 then.


---
*Imported from [Google+](https://plus.google.com/106432983261294316030/posts/E4uVF4o6wDo) &mdash; content and formatting may not be reliable*
