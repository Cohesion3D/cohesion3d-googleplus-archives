---
layout: post
title: "Anyone having issues with the latest build of LaserWeb4?"
date: April 06, 2017 03:04
category: "C3D Mini Support"
author: "Kris Sturgess"
---
Anyone having issues with the latest build of LaserWeb4?



I upgraded to 4.0.48 last night after using 4.0.45 all week just fine.



Now I'm dead in the water. 



Running K40 with C3D board.

Latest Firmware-cnc.bin

Latest LaserWeb4 4.0.48



Machine recognizes firmware and connects. 

Will not jog. 

Manually can test fire.



C3D LED status:

L1 - Green

L2 - Blinking Green Rapidly

L3 - Blinking Green Rapidly

L4 - Green

3V3 - Green

VMot - Red



I've re-booted computer.

I've manually reset C3D.



I'm going to reformat/reload SD card and re-boot to see if that helps.



Open to suggestions????



Kris









**"Kris Sturgess"**

---
---
**Frank Dart** *April 08, 2017 03:37*

I just got my c3d today and installed latest laserweb as well... same issue.  I threw the stock board back in just to make sure I didnt screw something up.

I'll probably give it another go tomorrow.


---
**Kris Sturgess** *April 08, 2017 12:41*

**+Frank Dart**​ I got it going. One of the settings reverted back when I upgraded. Check to see if you are running mm/sec or mm/min. I prefer mm/sec for my speeds.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/dfR1xFVLzFY) &mdash; content and formatting may not be reliable*
