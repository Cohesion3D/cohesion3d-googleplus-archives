---
layout: post
title: "This is the 3d time i am writing this comment ;-( Hi Jim Fong and Ray Kholodovsky , Pleas see the screenshot and use this link:"
date: May 01, 2017 21:09
category: "3D Printers"
author: "Marc Pentenrieder"
---
This is the 3d time i am writing this comment ;-(

Hi **+Jim Fong**​ and **+Ray Kholodovsky**​,

Pleas see the screenshot and use this link:

[https://drive.google.com/folderview?id=0B7r1He_FQF0BZmJWdTZSNnlpSUk](https://drive.google.com/folderview?id=0B7r1He_FQF0BZmJWdTZSNnlpSUk)

![images/c667e1734c23fd2b4e87599d0b22b3e4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c667e1734c23fd2b4e87599d0b22b3e4.png)



**"Marc Pentenrieder"**

---
---
**Marc Pentenrieder** *May 01, 2017 21:18*

**+Jim Fong**​

Commenting doesn' work well now in g+. Are you able to contact me in hangouts or any other chat tool?


---
**Jim Fong** *May 01, 2017 23:28*

Took a look at the data and the error looks to be very random.  In my opinion this doesn't look to be a smoothieboard/Smoothie problem.   



I really think there is a servo driver config issue.  These are really complicated drives to program and setup properly.  Look into the step pulse configuration values and make sure they are set up right.  




---
**Griffin Paquette** *May 02, 2017 13:15*

Completely agree with **+Jim Fong** I don't see how this would be a Smoothie issue. 


---
**Griffin Paquette** *May 02, 2017 13:15*

**+Ray Kholodovsky** 


---
**Marc Pentenrieder** *May 02, 2017 13:50*

I also don't think it's a smoothie issue, but I would be glad to get some input from people with servo knowledge.


---
**Marc Pentenrieder** *May 05, 2017 17:59*

**+Jim Fong**, **+Ray Kholodovsky**

SOLUTION: 

It was undocumented wiring between remix and servo driver.

Now I use the wiring from manual with rs422 converter (SU4-2-en.pdf in Google Drive folder) and it works like a charm. I also uploaded a new Measurement "Achsabweichung_20170505.pdf" in the folder.



The deviation (1mm) is measurement error i think, because I used a folding meter stick. But now I can start finetuning 8-D

Thanks for any advice i got.


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/Hsk4muQxdoH) &mdash; content and formatting may not be reliable*
