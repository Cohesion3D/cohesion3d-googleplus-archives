---
layout: post
title: "Please can somebody confirm the right orientation of my lan adapter on the remix board?"
date: January 10, 2017 15:44
category: "General Discussion"
author: "Marc Pentenrieder"
---
Please can somebody confirm the right orientation of my lan adapter on the remix board?

As soon as i enable network in the config the remix wont boot ;-(

![images/decf9b3f6feaa0e3d784db1117133d94.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/decf9b3f6feaa0e3d784db1117133d94.jpeg)



**"Marc Pentenrieder"**

---
---
**Eric Lien** *January 10, 2017 16:34*

Looks like you have it right, possible something is wrong in the config?

![images/3bd14223a5d16ce2058599d43c7d87f9.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3bd14223a5d16ce2058599d43c7d87f9.png)


---
**Marc Pentenrieder** *January 10, 2017 17:21*

If [network.enable](http://network.enable) = false everything works fine, even with lan board attached.

When i set [network.enable](http://network.enable) = true, remix doesnt boot and i dont even see anything on my rrd glcd only the backlight.


---
**Marc Pentenrieder** *January 10, 2017 17:22*

**+Eric Lien** Please send me a link to the picture. When i try to open the embedded picture i have black background and cant read the descriptions


---
**Marc Pentenrieder** *January 10, 2017 17:35*

**+Eric Lien** Thanks, I found it on Ray's Documentation page.

**+Ray Kholodovsky**​ Is it possible that the rj45 is upside down on the picture?


---
**Marc Pentenrieder** *January 10, 2017 17:43*

This is my networkconfig:

 

# network settings

[network.enable](http://network.enable) true # enable the ethernet network services 

[network.webserver.enable](http://network.webserver.enable) true # enable the webserver 

[network.telnet.enable](http://network.telnet.enable) true # enable the telnet server 

#network.ip_address auto # if dhcp 

network.ip_address [192.168.1.111](http://192.168.1.111) # the IP address 

network.ip_mask [255.255.255.0](http://255.255.255.0) # the ip mask 

network.ip_gateway [192.168.1.100](http://192.168.1.100) # the gateway address



Do you see something wrong?


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 17:47*

Turn it 180 degrees.   Yes the picture on the remix product page is wrong, sorry :) 

The documentation pinout should be correct though. 


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2017 17:52*

Pinout is at [cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com)


---
**Eric Lien** *January 10, 2017 18:08*

Nevermind. I hadn't noticed that his board was 180deg from the diagram. Teaches me to try and do tech support from my phone during a smoke break in midwest snow storm :)


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/b2bovrr6CgU) &mdash; content and formatting may not be reliable*
