---
layout: post
title: "Ray Kholodovsky don't suppose you have a link to the female counterpart to the 5pin K40 endstop connector and the 4 pin K40 power connector do you?"
date: March 25, 2017 01:19
category: "C3D Mini Support"
author: "Ben Delarre"
---
**+Ray Kholodovsky** don't suppose you have a link to the female counterpart to the 5pin K40 endstop connector and the 4 pin K40 power connector do you?



Looking to do the conversion of my Full Spectrum Laser 40w 4th Gen Hobby machine this weekend.



The endstop connector is wired GND, X, Y, GND and is 4 pin rather than 5, but I think I can just cut off the little tab and it will fit in the existing 5 pin K40 Endstop socket on the mini.



The power connector is 6 pin but of the exact same style as the K40 one, with a slightly different arrangement. It is wired to 4 pins on the PSU for 24v, GND, 5v and Laser. I figured I could get a K40 style connector and probably move the existing crimped pins straight over to the K40 4 pin connector without any recrimping!



If that doesn't work out, I would I assume connect the 24V and GND lines from the PSU to the main power screw terminals. Where then would I connect the L pin on the mini?





**"Ben Delarre"**

---
---
**Jonathan Davis (Leo Lion)** *March 25, 2017 03:04*

I've the four pin variant of the end stops on Amazon for a pack of five for about $10


---
**Ben Delarre** *March 25, 2017 03:04*

Need the 4pin of the PSU connector. Any ideas?


---
**Jonathan Davis (Leo Lion)** *March 25, 2017 03:11*

**+Ben Delarre** I know you could splice on in as a option, or their is the route of removing the one on the old PSu then transferring it over to the new one 


---
**Ben Delarre** *March 25, 2017 03:13*

I don't have a 4pin that would fit the mini board at all that's the problem. I could repin the 6pin to match the mini and then cut off the extra 2 pins on the plug to make it fit but that's starting to get destructive.


---
**Ray Kholodovsky (Cohesion3D)** *March 25, 2017 10:59*

At MRRF :) 

Back Tuesday. 



I see what you're getting at, have to take a look. 


---
**Griffin Paquette** *March 25, 2017 20:15*

I would strongly suggest you wait it out for Ray to check back on tues unless you find a direct adaption. I'll check on my FS on Monday to see if I can help. 


---
**Ben Delarre** *March 25, 2017 20:29*

Yeah I decided not to destroy the 6pin plug. I'm away on holiday next week so will put off the change over till after I get back.



It would be real nice to be able to get one of the 4pin plugs and just move the existing pins over to it. No crimping. No soldering. Drop in upgrade as it should be.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 04:11*

Back.  **+Ben Delarre** at your convenience can you send over some pics of the unique connectors so that I can start tracking stuff down? 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/SPYhspaWjwr) &mdash; content and formatting may not be reliable*
