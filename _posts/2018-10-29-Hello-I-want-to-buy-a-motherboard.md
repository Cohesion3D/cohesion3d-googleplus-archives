---
layout: post
title: "Hello. I want to buy a motherboard"
date: October 29, 2018 23:07
category: "C3D Remix Support"
author: "Pawel Misiak"
---
Hello. I want to buy a motherboard. I have a question, is it possible to connect an additional motor in the X axis (instead A). It builds a printer with independent heads. Is it config?





**"Pawel Misiak"**

---
---
**Anthony Bolgar** *October 30, 2018 00:15*

As far as I know you can only send commands for a single X axis. You can put two motors in parallel, but they will both do exactly the same thing. 


---
**Pawel Misiak** *October 30, 2018 04:56*

+Anthony Bolga**+Anthony Bolgar** 

Thank you




---
*Imported from [Google+](https://plus.google.com/117613851420139606503/posts/AgYE6MszXXo) &mdash; content and formatting may not be reliable*
