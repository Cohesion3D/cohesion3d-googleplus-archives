---
layout: post
title: "Skipping steps and losing hair.... Can anybody help me out"
date: June 11, 2018 08:47
category: "K40 and other Lasers"
author: "Mischa Repsev"
---
Skipping steps and losing hair....

Can anybody help me out.

X axis on my K40 is skipping a lot while engraving.

When doing vectors or cutting it seems to be working just fine.

So far I tried the most tips I could find in this and other communities.



First installed a 24V PSU to supply the cohesion3d mini and steppers.



Belt tension checked and adjusted (both ways, also after driver replacement)



Replaced the a4988 on the Cohesion3d mini with a drv8825 (adjusted current to optimal) 



Replaced the drv8825 to tmc2100 (again current adjusted) and switched between stealth chop and spread cycle. Much quieter now, but still skipping.



Installed the Jim Fong firmware with the SD card disabled.

Picture below is 150mm/sec (left from lightburn, on the right, same file from SD card without pc connected.

Before I go and exchange the stepper, and modify the gantry to run GT2 timing belts, and also try external stepper drivers, could it be a software issue?



I read some about GRBL, but not clear which version I'll need for this system, and if there is a config file I can change (pin settings especially) to move the y axis to the z output..

Might have pulled the last driver to fast from the board before power was gone..

In Smoothie it was fixed easily, but have no clue how to compile a new version of grbl-lpc with y on the z axis.



Regards,

Mischa



![images/5162d92aecb331145c647e609d7df69d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5162d92aecb331145c647e609d7df69d.jpeg)



**"Mischa Repsev"**

---
---
**Stephane Buisson** *June 11, 2018 11:43*

if you check all about your steppers (you didn't say about a stepper overheating), i would look into potential known issue of speed on usb port with smoothieware, gbrl 1.1f is the one if recall well.

also check k40 & laserweb community.


---
**Nick R** *June 11, 2018 11:59*

i had this exact issue.. and interestingly, i also changed to drv8825 steppers to try and solve the issue..   i have solved the issue by tightening the x-axis belt to the point where it isnt that easy to move the head along the x-axis with your hand (when its off an released)   the stepper motor has no issue still moving it, and now my cuts and engraves work without the skipping that is apparent like the original post.   I tried different firmware, added 24v external psu, slowed down cuts, upped the power on the original steppers.. you name it..  no dice


---
**Mischa Repsev** *June 11, 2018 12:11*

Thnx for the reactions so far **+Stephane Buisson**

The steppers are not overheating, current is set to manufacturer specs.

Heard about the issue with USB, so already tried the no SD card firmware, without luck.

Probably need GRBL, but have to change the config of it and recompile (no clue yet how, am electronics engineer, not a programmer)



Was trying to find where i can set the output pins in the source code of grbl-lpc and figure out how to compile..



**+Nick R** i have tried to tighten it to the point of absurd..(have used belt drives before) but without luck, so i guess this is software related.

With the original Nano board i could engrave, but not with greyscale..


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2018 12:20*

 Yes smoothie can only handle rastering so fast before issues. I would recommend trying the grbl-lpc firmware on our site. 



Which driver socket is not working now? 


---
**Mischa Repsev** *June 11, 2018 12:27*

**+Ray Kholodovsky**The Y socket is not doing anything anymore, reconfigured the config to sent it to the Z port in Smoothie, and works flawless.

So unless I can figure out a way to let grbl do the same trick, only option is a new board and then try grbl..


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2018 12:30*

I will have to do some digging.  They (dropbox links) are probably floating around in the comments of various threads here or on fb. 


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2018 12:31*

Here you go: [dropbox.com - firmware_grbl-lpc_c3dmini_4axis_ya_swapped](https://www.dropbox.com/sh/h4lfmyshphars5j/AAALmVJD4mQjD1Sw9xKXcts1a?dl=0)


---
**Mischa Repsev** *June 11, 2018 12:40*

Wow.. lightspeed reactions on this community..

Will try right away, work can wait..


---
**Mischa Repsev** *June 11, 2018 12:58*

**+Ray Kholodovsky**

Yay, firmware works like a charm, will start an engraving job as soon as the boss is not watching..;)

Could not even hope that i would get an answer this fast, let alone from 3 people..

Thanks for the assistance, will post a result soon.. (crossing fingers until it hurts)


---
**Jim Fong** *June 11, 2018 17:18*

If you haven’t already,  check your x axis pulley set screw. Tighten up and put some loctite on it.  


---
**Mischa Repsev** *June 11, 2018 17:35*

**+Jim Fong** is on my to do list..

For now is the grbl firmware doing the trick.

Missing the LCD though, but now finding out the correct pwm settings for greyscale..


---
**Claudio Prezzi** *June 12, 2018 09:59*

Did you check if the pulley on your X-axis is loose?


---
**Mischa Repsev** *June 12, 2018 10:25*

**+Claudio Prezzi** disassembled it completely and firmly re-attached it.

But the problem was seemingly in the firmware. Smoothie cannot cope with (my) graphic files it seems. Vectors are no problem.

Grbl-lpc did the trick, and all engraving jobs without any problems.

On the positive side, I already disassembled the complete machine, and squared, tightened and upgraded it where ever I could. (And got spare parts for a few years)

Runs a lot smoother now, and in a few days the controls are upgraded as well with all flow/temperature alarms in place.

Now time to get me a 8Mp fisheyecamera from aliexpress to use the awesome function in Lightburn..


---
**LightBurn Software** *June 13, 2018 06:50*

Smoothieware has a maximum throughput of 1000 gcodes per second theoretically, closer to 800 in practice. That translates to 254 dpi at 80mm/sec, which is not very fast. We designed the Newsprint dither mode to improve that - you can go about 150 to 200mm/sec at 254 dpi using Newsprint.



That said, GRBL-LPC is a more stable choice for engraving, and runs about 3x the speed of Smoothieware or better, and generally has fewer connectivity issues, though it’s a little less configurable, and you lose the LCD and headless mode.


---
*Imported from [Google+](https://plus.google.com/117223229094349978743/posts/KZ9jA1dhgxQ) &mdash; content and formatting may not be reliable*
