---
layout: post
title: "Ray Kholodovsky cohesion is here!!! Now I need some spare time for mounting it..."
date: January 24, 2017 19:17
category: "General Discussion"
author: "Antonio Garcia"
---
**+Ray Kholodovsky** cohesion is here!!! Now I need some spare time for mounting it... but quick question, do steppers need thermal paste?, it seems they come with some adhesive, is that enough? and the pot in the steppers, do they come already pre-adjusted? or i need to adjust them?

![images/8c41c29f2839f6620ab69191130e5337.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8c41c29f2839f6620ab69191130e5337.jpeg)



**"Antonio Garcia"**

---
---
**Antonio Garcia** *January 24, 2017 19:20*

![images/594ed979936f50969e0caa4fb793fb63.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/594ed979936f50969e0caa4fb793fb63.jpeg)


---
**Antonio Garcia** *January 24, 2017 19:23*

my power looks similar to other guys, so i guess i will try to install it leave the pot in place...

if I have any issue i will let you know :)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 19:25*

Agreed, this is becoming the preferred method. Leave pot alone. Run wire from L on the psu to bed - 2.5 and adjust config accordingly. 


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 20:03*

Re steppers, just the adhesive strip already there is fine for putting the heatsinks on. 

The flat on the pots faces up to the edge, this is ok, but maybe turn it 45 deg clockwise for some more power (needed at higher speeds) 


---
**Antonio Garcia** *January 24, 2017 20:03*

Can i remove all those pins in the power supply  (most to the right) and use only 24 volts for powering cohesion up (Vin), and then wire L to the pin 2.5? or do i need to use the connector which comes from the power to supply to the board?




---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 20:04*

Specifically regarding the 4 pins on the right, you can remove all those wires and run 24v and gnd to main power in on the mini, and L to 2.5, using your own wires. Sure. 


---
**Philipp Tessenow** *January 24, 2017 20:45*

**+Ray Kholodovsky**  is it correct, that I can not run the most right cable of the PSU (L) straight to the mini 4pin connector (labeld "K40Power")?



So it's best to remove the two left cables and just run my own L <-> 2.5 ?



Thanks for the help. ![images/ec77b5b8dbca9f0cd99806f24808a082.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec77b5b8dbca9f0cd99806f24808a082.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 20:49*

**+Philipp Tessenow** You cannot use that pin if you are keeping the pot and pulsing L, which is what I am now telling people to do.  You only need to remove that one wire, L, and run it to 2.5. 


---
**Philipp Tessenow** *January 24, 2017 21:04*

**+Ray Kholodovsky**  thanks. That's what I will do then tomorrow. 💪💪


---
**Antonio Garcia** *January 24, 2017 22:01*

**+Ray Kholodovsky** I´m not able to fire the laser, the movement are fine, but the laser isn´t work (it´s working if I press the physic test button) and the GLCD isn´t work either, but i checked the config file, and GLCD is true by default :(

I checked the continuity between L and pin 2.5 and it´s fine, i reseted the cohesion, and the if open the config file the value is set up to 2.5...

I´m trying to fire from laser web, and my config in laser web is this one...




---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:03*

How are you trying to fire the laser?

How is your GLCD wired?  What exactly is it doing (is the blue backlight on?)


---
**Antonio Garcia** *January 24, 2017 22:08*

![images/df98ab982d4e40f7db959e4472f36133.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/df98ab982d4e40f7db959e4472f36133.png)


---
**Antonio Garcia** *January 24, 2017 22:11*

i tested continuity between L and pin 2.5, and it´s fine, i reseted the board after updating the config file to upated to 2.5 instead of 2.4! and the value after i reseted it it correct...

I´m trying to fire the laser from the laserweb jog window...


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:15*

Run a command from the console at the bottom.  

For example.  

G1 X10 S0.5 F600


---
**Antonio Garcia** *January 24, 2017 22:19*

my GLCD is connected in this way, connected Exp1 from GLCD to Exp1 in the cohesion3d adaptor, and Exp2 from GLCD to Exp2  in the cohesion3d adaptor...



I don´t have any blue light in the cohesion board, all greens except one in red  (VMOT)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:21*

No, do you see a blue backlight on the GLCD?   If not take a picture of the back of the GLCD.  Sometimes they put the connectors on backwards...


---
**Antonio Garcia** *January 24, 2017 22:26*

![images/0a92668627d456940fd79b1ed425d9ae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0a92668627d456940fd79b1ed425d9ae.jpeg)


---
**Antonio Garcia** *January 24, 2017 22:27*

![images/ec79f882e6e85960cb57c6cfcce1adc2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ec79f882e6e85960cb57c6cfcce1adc2.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:28*

With the ribbon cables unplugged please. 


---
**Antonio Garcia** *January 24, 2017 22:29*

haha sorry!!! this command works

G1 X10 S0.5 F600




---
**Antonio Garcia** *January 24, 2017 22:31*

![images/de1950a53bd9acdce96968313299282e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/de1950a53bd9acdce96968313299282e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:37*

Yeah, the black box shrouds are rotated 180 degrees.  The notch opening should face inward.  


---
**Antonio Garcia** *January 24, 2017 22:42*

Ok, I´m gonna make a open notch in the other side in both connectors....




---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:45*

Or try to pull the shroud off the pins and put it on the other way, or RMA the GLCD as defective if you got it from Amazon.  Some other guys made new wires (**+Dushyant Ahuja** ?)


---
**Dushyant Ahuja** *January 24, 2017 22:50*

**+Ray Kholodovsky** no - I just pushed it in upside down. It was a tight fit - but worked.  Actually - now I no longer use the LCD. The primary use I had for the LCD was for Z baby stepping on Marlin - and Smoothie does not support that. Not will it likely. So I just use octoprint. 


---
**Antonio Garcia** *January 24, 2017 22:53*

It´s not working in any way hahah, i made some new holes for rotating 180, and the same issue... i bought it on ebay, so I will talk to the seller..

Regarding to the test button in laserweb, do i need something special in order that button works?

The command is working, so I´m guessing the wires are correctly installed, it´s not a big problem, because i have the physical test button, but i would like to know why it´s now working and if I have something in wrong in my config...

Thanks for your support anyway...


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 22:56*

Wonderful. 

Yeah, I haven't been able to get the fire command in LW to work myself.  So there's that.  Will continue looking into it...


---
*Imported from [Google+](https://plus.google.com/+AntonioGarciadeSoria/posts/JfqPBmRpaZV) &mdash; content and formatting may not be reliable*
