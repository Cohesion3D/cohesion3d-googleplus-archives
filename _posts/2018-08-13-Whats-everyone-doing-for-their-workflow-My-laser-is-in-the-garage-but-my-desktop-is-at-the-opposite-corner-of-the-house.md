---
layout: post
title: "What's everyone doing for their workflow? My laser is in the garage, but my desktop is at the opposite corner of the house"
date: August 13, 2018 03:29
category: "K40 and other Lasers"
author: "Exploding Lemur"
---
What's everyone doing for their workflow?  My laser is in the garage, but my desktop is at the opposite corner of the house.  I fiddled with LaserWeb on an RPi a bit but there's not a good way to set up a job from my desktop and then go into the garage to start it.

Do you use LaserWeb (or something else), export the gcode, and then copy it over to the C3D Mini?  Something different on an RPi connected to the C3D to set up the job and stream gcode (and are you using Smoothieware or GRBL-LPC)?





**"Exploding Lemur"**

---
---
**Tech Bravo (Tech BravoTN)** *August 13, 2018 03:35*

with the addition of the GLCD you can run the jobs off of the MicroSD card. i have tried the octopi as well and it kinda worked but ultimately any "remote" data streams are not reliable. one thing i have not tried is a router with a print server on it. there is a virtual usb over lan solution using a pi on the laser end and a driver on the pc side but i haven't explored it because i don't want to become complacent and burn my house down LOL




---
**Tech Bravo (Tech BravoTN)** *August 13, 2018 03:37*

**+Tech Bravo** NOTE: the glcd is smoothie only so GRBL is not an option for the above scenario.




---
**Joe Alexander** *August 13, 2018 04:00*

I have the laserweb server installed on a Raspi and link to it over the network to avoid having a long usb wire across my garage. I have found it to be very reliable, and less error prone than the long wire at times. i am also using smoothieware but I believe it also works with GRBL-LPC. But for safety I also have a wireless camera setup on a 7in lcd to monitor the job from my computer.


---
**Exploding Lemur** *August 13, 2018 04:45*

So maybe the best workflow for me would be create my file (DXF/SVG/whatever), generate gcode via Visicut/local LaserWeb/etc, get a C3D Ethernet adapter to upload the gcode via the Smoothie web interface, then start the job from the GLCD when at the laser. (My rule is the laser NEVER runs without someone physically present...I've got a Halotron clean-agent extinguisher right next to the laser, and 2 more standard extinguishers elsewhere in the garage)


---
**Joe Alexander** *August 13, 2018 05:45*

nah just put the gcode file on the sd  card in the board and run from glcd. no need to do an upload if you save it directly to the sd card, then run from the GLCD. also always power off the machine before removing the sd card, things have been known to happen... :P


---
**Exploding Lemur** *August 13, 2018 06:36*

**+Joe Alexander** do you run from an SD card in the GLCD, or from the SD card in the C3D Mini? My C3D isn't particularly accessible in the K40...


---
**Joe Alexander** *August 13, 2018 06:39*

the one from the C3d, never did test or try the one in the GLCD as I read it has issues. but most of my work goes through network and raspi to laser direct. helps that my computer is like 15ft away.


---
**Exploding Lemur** *August 13, 2018 07:05*

**+Joe Alexander** To make sure I understand your setup: You have your computer, Pi, and C3D on the network. You point your desktop browser to [http://yourpi:8000](http://yourpi:8000), and in the LW web interface set the server to yourpi:8000, and the machine connection to the C3D IP. You then configure and start jobs from your desktop. Is that right? If not, can you describe in more detail? (If it is, that's the part I can't do, because my computer is across the house and I don't want to start a job without being at the laser in case something happens)


---
**Joe Alexander** *August 13, 2018 16:28*

i point laserweb to server on raspi at ip:8000, then set machine connection to the relevant usb on the raspi. i can then run jobs from my computer. and side note I am using a smoothieboard not a C3D but they run the same firmware.


---
**Exploding Lemur** *August 13, 2018 19:05*

**+Joe Alexander** got it. Yep that's how I had it set up, but having absolutely everything browser-side made it unworkable for my situation. (Machine profile, job setup, gcode prior to hitting run, etc)


---
*Imported from [Google+](https://plus.google.com/106334870053586601668/posts/Kp65iEh2B6i) &mdash; content and formatting may not be reliable*
