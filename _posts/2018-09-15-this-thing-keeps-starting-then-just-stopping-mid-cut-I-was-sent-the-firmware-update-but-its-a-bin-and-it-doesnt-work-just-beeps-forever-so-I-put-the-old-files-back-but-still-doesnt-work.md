---
layout: post
title: "this thing keeps starting then just stopping mid cut I was sent the firmware update but it's a .bin and it doesn't work just beeps forever so I put the old files back but still doesn't work"
date: September 15, 2018 22:28
category: "K40 and other Lasers"
author: "Timothy Lathen"
---
this thing keeps starting then just stopping mid cut I was sent the firmware update but it's a .bin and it doesn't work just beeps forever so I put the old files back but still doesn't work. 

I am so frustrated 





**"Timothy Lathen"**

---
---
**Tech Bravo (Tech BravoTN)** *September 15, 2018 23:48*

[lasergods.com - C3D Standard Firmware & Config](https://www.lasergods.com/c3d-standard-firmware-config/)


---
**Timothy Lathen** *September 18, 2018 03:14*

**+Tech Bravo** 

Thank you this was the answer it fixed the problem but now I have a new one the only  way to get a correct print is to mirror horizontally in lightburn it wasn't that way before the update so I'm kinda confused. 

 


---
**Tech Bravo (Tech BravoTN)** *September 18, 2018 04:35*

[https://www.lasergods.com/checking-c3d-equipped-k40s-origin-in-lightburn/](https://www.lasergods.com/checking-c3d-equipped-k40s-origin-in-lightburn/)

[lasergods.com - Checking C3D Equipped K40’s Origin in LightBurn](https://www.lasergods.com/checking-c3d-equipped-k40s-origin-in-lightburn/)


---
**Tech Bravo (Tech BravoTN)** *September 18, 2018 04:37*

If that is correct then try this one [https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
*Imported from [Google+](https://plus.google.com/+TimothyLathen/posts/YaYumkgBems) &mdash; content and formatting may not be reliable*
