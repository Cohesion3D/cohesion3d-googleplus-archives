---
layout: post
title: "So far take #2 of installing my Cohesion3D mini went well (got a different machine)"
date: July 22, 2017 20:49
category: "K40 and other Lasers"
author: "Tammy Mink"
---
So far take #2 of installing my Cohesion3D mini went well (got a different machine). This one had the ribbon cable m2nano but I followed the instructions of the swap just fine. No issues yet anyways but only cut and engraved one small item so far. Here's hoping all continues to go well. :)





**"Tammy Mink"**

---
---
**Ashley M. Kirchner [Norym]** *July 22, 2017 22:59*

Awesome hearing it went painless. Let us know if you have questions.


---
*Imported from [Google+](https://plus.google.com/112467761946005521537/posts/F9AiMjpYLZU) &mdash; content and formatting may not be reliable*
