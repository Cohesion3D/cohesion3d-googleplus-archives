---
layout: post
title: "Old stock board, wiring new board doesn't match up"
date: February 10, 2018 20:55
category: "General Discussion"
author: "F D"
---
Old stock board,  wiring new board doesn't match up



![images/84a42043bf6e24697a8c4fcb7cbf4739.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/84a42043bf6e24697a8c4fcb7cbf4739.jpeg)
![images/4b2f481b34f0ebd3b30171c6aed4a56d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b2f481b34f0ebd3b30171c6aed4a56d.jpeg)

**"F D"**

---
---
**F D** *February 10, 2018 21:37*

Anyone have pictures of how this gets installed?


---
**Richard Vowles** *February 11, 2018 09:41*

Not me, yours looks more recent than mine!


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2018 17:45*

You have a Moshiboard.  This is a less common older board. 

It looks like you got the ribbon cable, and the small 4 wire plug goes to the Y motor output.  So far this is the same as shown in the instructions.



Now, the big plug should contain a 24v, Ground, and L wire.  Please check me on this, as I cannot read the labels on the board from the angle of your provided pictures.  





You need to connect 24v and Gnd and L to the Mini screw terminals as shown in the picture I have attached. 

 

You do not plug 5v anywhere in into the Mini.  Where do you think you are getting a 5v wire from? 





![images/fd730ba88bd41356c53e6d815bf9074d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd730ba88bd41356c53e6d815bf9074d.png)


---
**F D** *February 12, 2018 22:46*

Will try this thank you so much


---
**F D** *February 13, 2018 18:11*

I must be missing something, It still doesn't work. The board powers up, laser and motor don't. I feel stupid. 


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 18:14*

In order to cure that, read this, then respond again.



[plus.google.com - A public service announcement: Everyone, we want to help you, really, we do....](https://plus.google.com/+AshleyMKirchner/posts/DAwQYWRrZnD) 


---
*Imported from [Google+](https://plus.google.com/116399413604240668728/posts/ehcwnYsJ9zx) &mdash; content and formatting may not be reliable*
