---
layout: post
title: "Just an info post for anyone else with the same issue in the future"
date: November 16, 2017 00:34
category: "C3D Mini Support"
author: "michael dorazio"
---
Just an info post for anyone else with the same issue in the future.



I just swapped a RAMPS board with Lasersaur for the Cohesion mini.  It's working reasonably well (I'm not a fan of Laserweb), but I initially got a lot of vibration on G0 moves regardless of seek rate settings in config.  Turned out the Y driver on the board was bad, so I swapped it for another one I had lying around and it seems good now.  I'm using 4000 seek rate and it's smooth and quiet.





**"michael dorazio"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 16, 2017 19:08*

Thanks for the note, and be on the lookout for Lightburn when it launches in the coming week(s). 


---
*Imported from [Google+](https://plus.google.com/105988021320862241083/posts/SQ4iS2vz3uV) &mdash; content and formatting may not be reliable*
