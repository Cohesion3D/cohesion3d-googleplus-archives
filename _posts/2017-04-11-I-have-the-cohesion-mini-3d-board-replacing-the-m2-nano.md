---
layout: post
title: "I have the cohesion mini 3d board replacing the m2 nano"
date: April 11, 2017 15:00
category: "C3D Mini Support"
author: "Tom Davidson"
---
I have the cohesion mini 3d board replacing the m2 nano. the wiring conversion was minimal just the 12 line ribbon for the x axis motor and x & y endstops, the 4 position power connector 24v G 5v L, and the y axis connector. that all i did other than the discount display board. control via the rosary switch works fine, except test firing the laser. manually firing works. installed lw4 works great but no laser action.

do I need to put a jumper from L on the test button to L on the main power connector?





**"Tom Davidson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 15:02*

How are you trying to test fire? The fire button in LW is hit or miss.  Try sending a line G1 X10 S0.4 F600

That would fire while moving. 


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 15:43*

All of our laser users are on CNC firmware from the start. Yet for a bunch of people it either doesn't work or they can't get it to work. Phrasing aside, I am not blaming LW, I am saying, from the support perspective, it is easier to say "don't worry about the button, try this G1 line instead". 


---
**Ray Kholodovsky (Cohesion3D)** *April 11, 2017 15:48*

**+Peter van der Walt** can you set up an alternate way that you devs and I can chat? Email thread? GitHub? I do not want to disrupt the original post/ this thread with a full blown technical chat right here and now. Let us please do it off list. 


---
**Claudio Prezzi** *April 11, 2017 19:11*

**+Ray Kholodovsky** **+Peter van der Walt** If we just want to discuss some options, we can make private posts here on G+. Git issues are also public and could potentially confuse people.


---
**Claudio Prezzi** *April 11, 2017 19:15*

I think it's a good idea to execute a G1 command for debugging reason, but the fire command should also work. If it doesn't, then it is the wrong firmware (not cnc), a wiring problem, or a firmware config problem.


---
**Claudio Prezzi** *April 11, 2017 19:18*

The only thing that needs to be configured in LW4 is the "Tool Test Power" and "Tool Test duration". The defaults are zero for security reason.


---
*Imported from [Google+](https://plus.google.com/114888516915439661595/posts/An5eC77FBuY) &mdash; content and formatting may not be reliable*
