---
layout: post
title: "Hi i am currently working with laserburn and the c3dmini"
date: June 25, 2018 12:43
category: "C3D Mini Support"
author: "Marko Marksome"
---
Hi i am currently working with laserburn and the c3dmini.

Everything is working fine except, the engravings are crooked and the x axis is skipping.

I have read that this is because the smoothie firmware is not as good with faster speeds like the grbl firmware.



I have deinstaled the smoothie drivers from the pc, and changed the firmware to grbl on the sd card. Instaled the grbl driver on the pc and connected it to the usb port.

First off i havent been able to acces the sd card from the computer anymore.

I have checked the sd card and the firmware has been changed to the .CUR extension.



I have set the device in lightburn for grbl and went to ok.

Nothing happens. The k40 is unable to move or to do anything.

On the board itself only 1 green light is shining.



Can anybody tell me what could have went wrong?

Thanks





**"Marko Marksome"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 25, 2018 13:14*

Please show picture of board. Need to see exactly what lights are on. 


---
**Marko Marksome** *June 25, 2018 15:30*

So the light where 3v3 is written besides it, shines green when the board is just connected to the pc via usb, and when i turn on the laser the same exact light turns red. The pics dont catch it quite good.![images/501ea7a97b5c435deec67c277f8ea918.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/501ea7a97b5c435deec67c277f8ea918.jpeg)


---
**Marko Marksome** *June 25, 2018 15:30*

![images/0e7b864a087ad9467c54e5503f9aecf6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0e7b864a087ad9467c54e5503f9aecf6.jpeg)


---
**Marko Marksome** *June 25, 2018 15:36*

Driver on the pc![images/8d0affbb3b927afe66e5fb723d947b7b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d0affbb3b927afe66e5fb723d947b7b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 25, 2018 15:36*

I see. Try removing the memory card with power off, and then powering back on. Let me know if anything changes. 


---
**Marko Marksome** *June 25, 2018 15:47*

Its the same, but now i noticed that not the green light (3v3) turns red but the light above it (vmot)

Turns red and both are shining.

Vmot in red and 3v3 in green.


---
**Ray Kholodovsky (Cohesion3D)** *June 25, 2018 17:01*

Does the memory card show up if you plug it via a reader into your computer? 



I would recommend trying to flash the firmware again.


---
**Marko Marksome** *June 25, 2018 17:07*

Yes it does show up.

I have flashed the firmware 4 times now :)

Went to smoothie back aswell.

Smoothie is working but grbl not.


---
**Marko Marksome** *June 25, 2018 22:35*

Do i have to erase the config file from the sd when i am using grbl firmware?


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/ZMvpiKpGkb7) &mdash; content and formatting may not be reliable*
