---
layout: post
title: "I know which ports they need to be plugged into, just not how to install them on my machine itself"
date: December 12, 2016 01:31
category: "General Discussion"
author: "Jonathan Davis (Leo Lion)"
---
I know which ports they need to be plugged into, just not how to install them on my machine itself.



<b>Originally shared by Jonathan Davis (Leo Lion)</b>



So I am working at trying to install end stops on my Benbox Gearbest/bangood Laser engraving machine using's **+Ray Kholodovsky** Cohesion 3D controller board as the included one was defective. Now I know where to hook them too on the board but i can not seem to figure out how to install them on the machine itself.



![images/7fe5a6e786818ffc8655304844fe4473.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7fe5a6e786818ffc8655304844fe4473.jpeg)
![images/be7d2feb376e06bd64ee6e79f0b0ee0f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/be7d2feb376e06bd64ee6e79f0b0ee0f.jpeg)

**"Jonathan Davis (Leo Lion)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 01:35*

I think Ariel said it pretty well:

You will need to cut a piece of wood or acrylic and make a small  plate to mount the endstops and then screw them into the middle of the profile on each axis. 


---
**Jonathan Davis (Leo Lion)** *December 12, 2016 01:38*

**+Ray Kholodovsky** Like on the laser head itself or where the screws are holding down the guide cable? Or both?


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 01:41*

On the ends of the rails. 

X min is left

X max is right. 

Y min is front 

Y max is back. 


---
**Jonathan Davis (Leo Lion)** *December 12, 2016 01:43*

**+Ray Kholodovsky** ok? On laserweb3 it will go a -18 and a positive 60


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 01:49*

That's completely unrelated to the "where to physically install the endstops" - which I just answered above. 


---
**Jonathan Davis (Leo Lion)** *December 13, 2016 03:05*

**+Ray Kholodovsky** Yeah but by having the end stops will it the system within the chart in Laserweb3?


---
**Ray Kholodovsky (Cohesion3D)** *December 14, 2016 17:26*

With the endstops installed your machine should stop at the end of the rails, yes. 


---
**Jonathan Davis (Leo Lion)** *December 14, 2016 17:27*

**+Ray Kholodovsky** Alrighty and I'm thinking for now just taping them on then come back and make the holder do dads.


---
**Samer Najia** *January 15, 2017 22:11*

I would love to see the final outcome of this.  I have a similar unit that could use a C3D Mini


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/EnyqCf2HNJw) &mdash; content and formatting may not be reliable*
