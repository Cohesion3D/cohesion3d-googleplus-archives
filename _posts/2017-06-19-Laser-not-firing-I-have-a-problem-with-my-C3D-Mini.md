---
layout: post
title: "Laser not firing. I have a problem with my C3D Mini"
date: June 19, 2017 02:46
category: "C3D Mini Support"
author: "Carlos Alejandro Cardenas Hernandez"
---
Laser not firing. I have a problem with my C3D Mini. Sometimes have work correctly, but now, i can´t fire my laser. The laser works ok when i use my physical test fire button. I made some works today, but it stopped firing. 

I unistall and reinstall all the wires and re-check all of them, I even change my screws in the base for plastic ones for any ground or isolation problem. 

When i press in LaserWeb the test button i just have a very quiet noise(pip). I have change the values for test. (35%, 100ms, 300ms, 500ms; 60% 100ms, 200ms, etc.)



X and Y stages work ok.

And the second problem i have: sometimes the lcd doesn't start, and send just two lines, i add one picture , and just turn off and on the cutter and sometimes it work ok and sometimes i have to turn off  and on again.

Leds are ok, red, 2 and 3 blink, 1 and 4 on.



Does anybody have a clue about what can i do? 



![images/230683a78ecdf4807e6ab535393364a3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/230683a78ecdf4807e6ab535393364a3.jpeg)
![images/73ba15777b98d58c68cc3f2bdf55bd3e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/73ba15777b98d58c68cc3f2bdf55bd3e.jpeg)
![images/796795673e07c90b391ab1cef3b98a0f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/796795673e07c90b391ab1cef3b98a0f.jpeg)
![images/35e71ee864c6dccf6c92afc2165bc47c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/35e71ee864c6dccf6c92afc2165bc47c.jpeg)
![images/260082d348c9913aa2ad3d59985afe5d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/260082d348c9913aa2ad3d59985afe5d.jpeg)

**"Carlos Alejandro Cardenas Hernandez"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 02:52*

1. In order to use fire button in LW you need to configure LW settings first. 

2. I notice your LCD display is "fuzzy". Have you gotten it to show text? Maybe just need to restart power? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:13*

Hi Ray , i have configured LW with many values: 35%, 100ms, 300ms, 500ms; 60% 100ms, 200ms, etc. they works some times even works the cutting. But now i dont have any laser. I restart the power, but randomly show the two lines when it starts, and have to turn off and on...



![images/e8eea5fbacac92d5e3f6db1991cefbef.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e8eea5fbacac92d5e3f6db1991cefbef.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:16*

Does the test fire button on the machine still work, and reliably? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:18*

Yes, it works OK.


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:19*

In the troubleshooting guide, you will find the G1 line explanation and example.  Can you try running those lines? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:31*

I juts made the test, but doesn´t fire. 

I just try the test pushing manually  the test fire button in my machine and it looks ok.




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:32*

G1 X10 S0.8 F600 



Change the x values back and forth. Tell me if this fires. 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:36*

I use X10, X20 and X5 and nothing...


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:37*

So the head moves back and forth, right? But it does not fire? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:38*

Yes it moves OK but don´t fire.




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:41*

The green wire, L, can you unplug this and touch this to ground, see if the laser fires when you do this? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:43*

Ground next to 24V+?


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 03:45*

The ground of the laser psu, yes that is where it comes from :) 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 03:47*

I turned off, disconnect L, turned on and touch ground, and it fires...




---
**Ronald Whittington** *June 19, 2017 10:07*

My display does the same thing randomly and power cycle has been the only solution,USB or mains first makes no difference




---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 22:29*

Understood, Ronald.  In my experience it has been when the initial power (ie: USB from an underpowered computer) has not been enough to keep up with the power draw of the LCD on startup.  But given the "unknown" nature of the 24v coming from the k40 psu, yeah...



Carlos, I would like you to run an additional wire to connect +24v main power in (the 2 pin screw terminal) to FET Power In + (this is the #1 screw terminal at the bottom, closest to the corner).  Then we should have an LED indication to see when that mosfet is turning on. 



You can send me a picture before you turn it on so that I can confirm the wiring.


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 23:54*

The green one at the left...

![images/554c46015a0647acdf506b0094ebc321.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/554c46015a0647acdf506b0094ebc321.jpeg)


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 23:55*

Coming from 24V

![images/501976b4fa866c3cfb1189681b35e21f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/501976b4fa866c3cfb1189681b35e21f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2017 23:57*

Can you give me a wider view of the 2nd pic? 


---
**Carlos Alejandro Cardenas Hernandez** *June 19, 2017 23:59*

![images/8abe3ddbf87024fd9ee63d29309aa380.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8abe3ddbf87024fd9ee63d29309aa380.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2017 00:03*

Perfect. Ok so under the silkscreen "2.5" and above "PWR" there is an LED that should turn on when we send the various laser fire commands, whether it is the laser test button in LW or G1 command (I prefer G1 X10 S1 F600)

Can you try these:

1. In current state.

2. Disconnect L (other green wire) from the board. 

3. If still no red LED, please take out the MicroSD Card, Format it FAT32, and put new firmware and config files on it. Dropbox link at bottom of instructions.

Put card back in and power up/ reset the board and watch the LEDs count up on binary. 


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 00:20*

Ray, it turns on the red led, both in LW laser test and G1 X80 S1 F600 command. But not laser fire. I formated the SD and reload the files and nothing...first video LW laser test, second direct code G commnad





![images/1d2726c0302bfcb64486d6ee42f714dd](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d2726c0302bfcb64486d6ee42f714dd)


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 00:21*

![images/e4e221f2f00c0d0ea87e7f44057ed1c9](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e4e221f2f00c0d0ea87e7f44057ed1c9)


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2017 00:25*

Ok, this behavior looks correct.  Now please hook up the L wire again and repeat both.  Make sure that LED behaves the same. 


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 00:31*

I just make it and the led behave the same...


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2017 00:43*

This is the exact behavior we expect from the board.  So please show me the behavior of the laser when we run the G1 line...


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 00:49*

With L connected

![images/af902344dba996919e03144f8720cac4](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/af902344dba996919e03144f8720cac4)


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2017 01:02*

I'd like to see the actual machine and head/ work area.  Is it not firing at all right now, or what? 


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 01:12*

I sent this command: G1 X80 S1 F600, after it finished and stopped , i use the k40 test button manually to check it fire...

![images/4732fca4a257ab6f9f47ce334a89785a](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4732fca4a257ab6f9f47ce334a89785a)


---
**Ray Kholodovsky (Cohesion3D)** *June 20, 2017 03:23*

In config.txt there is a laser max pwm value currently set at 0.8  

Set it to 1.0

Then save, safely eject the drive/ card, and reset the board. 

Then please try a G1 command with S1 






---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 03:54*

I have the same result, doesn't fire...




---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 04:12*

Hi Ray, i just rechecked every connection and returned the board to his original position when i press the usb cable it works, its very strange, i think it moves a little the usb in the board and touch the screw and then works...




---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 04:17*

I think there is something not firmely attached in the board...

maybe some other componenet besides the capacitor....


---
**Carlos Alejandro Cardenas Hernandez** *June 20, 2017 04:25*

But doesn´t run consistent, like something loose...




---
**John Milleker Jr.** *June 20, 2017 22:43*

Sorry to join in with this, are the two garbled lines bad (I think Ray called it fuzzy)? I haven't seen it addressed. This will appear for me every now and then when I power up the laser itself but if I plug in the computer first so that the power from the USB powers the Cohesion board I get those lines 9 out of 10 times. I'll have to unplug it, wait and then power up the laser before plugging in the USB.



If it's bad, maybe it's a bigger issue as I'm having locking up issues running SD card files and getting X creep with USB sent files.




---
**Naveen Chandra Dudarecha** *June 23, 2017 20:48*

Hi Ray, I am facing exactly the same issue Carlos is facing. The steppers are all moving fine but the laser would not fire. I tried to run a file from LW4 and the laser does all the motion but the laser does not come up. On one occasion, I pressed the Laser power and test switch on top of the machine while the machine was running the file and it worked for few seconds and then disconnected LW4 from computer on its own. 

Since then the machine stepper movements work but with no laser. I am not sure if Carlos' issue is resolved and if it is, ls could you advice what helped. 




---
**Naveen Chandra Dudarecha** *June 24, 2017 11:41*

My problem solved last night. I added the M3 and M2 command in the Gcode section of the LW4. Notes from Carl Fisher- [cncpro.co - Live Jogging](http://cncpro.co/index.php/33-documentation/initial-configuration/laserweb-configuration/14-gcode) 


---
**Ray Kholodovsky (Cohesion3D)** *June 30, 2017 01:49*

Carlos,

Any progress with this? 

It does not sound like the mosfet is malfunctioning, seeing as how the red light does light up accordingly. 



I had these additional steps in mind:  If you'd like to rule out the mosfet, we can switch over to 2.4 and use the PWM cable to feed L. 



Alternatively, I would also try setting the pwm period in config.txt to the value 400.  Some lasers respond to this for the variable power better.  


---
*Imported from [Google+](https://plus.google.com/112727582042027248508/posts/HCM5JZ5u279) &mdash; content and formatting may not be reliable*
