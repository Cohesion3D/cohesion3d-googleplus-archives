---
layout: post
title: "Fusion360 CAM directly to Cohesion3D? Ive been searching around this community, but havent found a definite answer"
date: June 05, 2018 00:59
category: "K40 and other Lasers"
author: "David Ewen_P"
---
Fusion360 CAM directly to Cohesion3D?



I’ve been searching around this community, but haven’t found a definite answer.



Can I run my k40 with the Cohesion3D upgrade directly from Fusion360 using CAM?



Or, do I have to save my file to dxf on the SD card?

Or, in another option to save a dxf file to my computer and run it from LightBurn?



Thanks for the help

David





**"David Ewen_P"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2018 01:02*

The board takes gCode and gCode only. 



The best way to go about your situation is to export the DXF file, import it in Lightburn, set up your job, and send it to the board from Lightburn. 


---
**Don Kleinschnitz Jr.** *June 05, 2018 12:48*

I do not think that F360 drives machines direct, does it? That would be nice if it did???


---
**David Ewen_P** *June 05, 2018 23:42*

**+Don Kleinschnitz** 

Fusion360 has CAM built in, so it creates tool paths and Gcode. 

I believe the Gcode needs to get to a post processor for controlling the machine. I guess LightBurn would be one of those post processors.?





My guess is that within Fusion, I would have to figure out how to use the spindle speed to control the laser power, but everything else should be ok.



Here is a link to all the existing post processors on Autodesk's site for Fusion360.



I'm hoping one of these will work with the Cohesion3D board.



Any thoughts?


---
**David Ewen_P** *June 05, 2018 23:44*

**+Ray Kholodovsky**

thanks for the reply. Since Fusion can output Gcode, can Icopy to a SD card and run it from the Cohesion3D board? It doesn't look like LightBurn can import or process a Gcode file.


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2018 23:52*

Possible for cutting. I do this using Vectric aspire.  I wrote a post processor to put cuts into the G1 X10 Y10 S0.6 F600 format that sets the laser power. If you can figure out how to do something similar in F360... there you go. 



Then you can put the file on the board’s memory card and run it via the GLCD screen menus, or you can stream it over the USB cable via Lightburn. 



But again, you can also import your <b>DXF</b> file into Lightburn and set up your job there, and that will give you more control as far as what to cut, fill, etc...  


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2018 23:57*

In Vectric I tell it the spindle speed of 0-100% and then in the postp I translate that to a 0-1 range which is what the board/ smoothie firmware wants. 


---
**David Ewen_P** *June 06, 2018 00:42*

 **+Ray Kholodovsky** 

sorry if I'm highjacking this thread



I have the M2Nano board, no ribbon cable etc.

Is the Cohesion3D board a direct replacement for it?

I would like if possible to just unplug the M2 and plugin the Cohesion board and go.



Along with plug-n-play, I believe the Cohesion3D board will do the z-axis?



Do you want/need a photo of my board?



Thanks,

David


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 01:03*

Please consult the images and text on the product page and the install manual at [cohesion3d.com/start](http://cohesion3d.com/start).  



I want to answer yes, but I have a history of people yelling at me after I do, so I'll leave it up to your eyes and judgement. 


---
**David Ewen_P** *June 06, 2018 01:56*

**+Ray Kholodovsky** 

This is exactly what I have, so if your board replaces this, I'm getting one.



I'm planning getting the full blown all options included version.

Thanks for all your speedy replies.







[s3.amazonaws.com](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145373/original/yyv0cZeRKC2JDRjyf8vOBuJyMHGJiNDmug.JPG?1489517776)



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145291/original/Ekq3761gu8wHZ0bEGpQjov5O9zyZH1BNmA.JPG?1489517573](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080145291/original/Ekq3761gu8wHZ0bEGpQjov5O9zyZH1BNmA.JPG?1489517573)


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 02:00*

There’s your answer. Make sure your wiring looks like the “all JST no ribbon” that is shown in that guide. 


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2018 02:01*

Btw your email system may prevent you from getting emails from our storefront such as tracking info and your Lightburn key. 


---
**David Ewen_P** *June 06, 2018 04:33*

**+Ray Kholodovsky** 

Yes that may happen, but it is an awesome spam blocker. If you see the email that asks you to respond, please do. It will auto whitelist you. If not, I check it daily.


---
**David Ewen_P** *June 06, 2018 04:40*

**+Ray Kholodovsky** 

What’s funny is, when I 1st saw that photo, I thought it was my machine. I took a photo just like it yesterday. So mine is an exact match. I’m looking forward to get the new board. I don’t like the stock software. 



I’ve already made a few other mods. Did that before I even used it. 



My next idea is to make it big enough to do the projects I had in mind when I bought to test the process. I thought I would have to purchase a big laser, but this etched and cuts the materials I need. Except stainless, but I only need to etch that and I think I found a way to do it with the K40. 



Part of the next upgrade is to have a Z-axis that is around 12” and work area of ~ 30” x 24”. That may take a while to figure out. I’m new to all this...


---
**Don Kleinschnitz Jr.** *June 06, 2018 12:48*

**+David Ewen_P** 

Yes Lightburn will drive and manage the K40 machine.



You can run Gcode directly from Lightburn. The "Run Gcode" button. This function runs the code but does not display the job.  As Ray suggests the best method is to use a dxf file.



There are multiple K40 users in this forum that have expanded the size of bed, search the forum for examples.


---
**David Ewen_P** *June 07, 2018 00:19*

**+Ray Kholodovsky**



Can I use the Ethernet Expansion Board with the Cohesion3D controller? [cohesion3d.com - Ethernet Expansion Board](http://cohesion3d.com/ethernet-expansion-board/)

And control the laser via ethernet instead of USB?



Can you suggest a lense & mirror upgrade option?

You have 2 on your site. I’m just not sure which one would be best.



I do not have an air assist head yet, but plan on getting one.

If you have some suggestions, I’m open.



For the Z-axis on the board

Can this be controlled with a toggle switch?

If not, how is it intended to be controlled?

Is there a way to automatically set the height in the software or something?




---
**Ray Kholodovsky (Cohesion3D)** *June 07, 2018 00:47*

Smoothie Ethernet is slow and Lightburn does not support it. I don’t recommend it. 



The product descriptions on the lenses and mirrors spell it out very clearly in my opinion, have you checked that? 



The z is controlled by gCode. There are buttons in Lightburn. 


---
**David Ewen_P** *June 13, 2018 18:09*

Ray,

Have you done any work with Fusion360 CAM to your board?



Here's a couple links for reference.

Regarding the "smoothie board" - do you have any idea what the settings should be for the K40 laser?



[https://cam.autodesk.com/hsmposts](https://cam.autodesk.com/hsmposts)? (scroll until you see "Smoothie Mill/Laser")




{% include youtubePlayer.html id="2M0pwqe-r1M" %}
[youtube.com - How To Save As DXF For Waterjet Plasma & Laser — Fusion 360 CAM Tutorial — #LarsLive 164](https://www.youtube.com/watch?v=2M0pwqe-r1M)

This guy works for Autodesk and does lots of videos for Fusion360 & CAM. This one shows how to get DXF and CAM processes out to a laser, water jet, plasma.




---
*Imported from [Google+](https://plus.google.com/109639692587843616301/posts/ETauDDgRP9q) &mdash; content and formatting may not be reliable*
