---
layout: post
title: "Ok this is weird .. I am looking in the web and i can't find any proper solution.."
date: February 08, 2017 16:03
category: "FirmWare and Config."
author: "Kostas Filosofou"
---
Ok this is weird .. I am looking in the web and i can't find any proper solution.. 



I had connected my C3D mini in a PC and all worked fine with drivers .. now i use a dedicated PC for the laser with Win7 Pro  (exactly same windows with my previous) but i can't proper connect the board .. I have Download the smoothie drivers (smoothieware-usb-driver-v1.1.exe and i follow the exact procedure with the first PC) but i get error with the serial port (Smoothie serial failed to install) any ideas ?





**"Kostas Filosofou"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 16:39*

Need screenshots to be able to understand the issue. 


---
**Kostas Filosofou** *February 08, 2017 17:02*

Here you go **+Ray Kholodovsky**​ 

![images/1f091e3083c6ca65c6a0d94feede04a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1f091e3083c6ca65c6a0d94feede04a7.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 17:06*

Right click on it in device manager and uninstall it in the box that pops up. 

Unplug USB from board. 

Restart computer. 

Reset board/ turn k40 power on first. Then plug in USB. 

Repeat this if needed by reinstalling smoothie drivers after the restart then plug board in. 


---
**Kostas Filosofou** *February 08, 2017 17:10*

Ok I'll try it .. thanks **+Ray Kholodovsky**​


---
**Kostas Filosofou** *February 08, 2017 21:19*

**+Ray Kholodovsky**​​ i try exactly the procedure you discribe me with no luck.. any other ideas? I have also search in Smoothie wiki and I try to put manual the drivers but with no luck..


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/ZQj48FZ6R9X) &mdash; content and formatting may not be reliable*
