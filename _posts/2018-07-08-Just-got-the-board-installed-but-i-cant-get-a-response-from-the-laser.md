---
layout: post
title: "Just got the board installed but i cant get a response from the laser"
date: July 08, 2018 21:23
category: "C3D Mini Support"
author: "Lashley Guyton"
---
Just got the board installed but i can’t get a response from the laser. Trying to just Home it but nothing happens. 







![images/8d0b2ec89c812ede13a2d46891326608.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d0b2ec89c812ede13a2d46891326608.jpeg)
![images/20c18db0820fe363a70d73e59cb70771.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/20c18db0820fe363a70d73e59cb70771.jpeg)
![images/10c4e9367bd4b2359b60bc562b545c3a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/10c4e9367bd4b2359b60bc562b545c3a.jpeg)
![images/8127a4b177291d601bb5ef351953e00a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8127a4b177291d601bb5ef351953e00a.jpeg)
![images/1364d7afffe809c1c047cae9ec95ae1e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1364d7afffe809c1c047cae9ec95ae1e.jpeg)
![images/53b6084a090172a80668fecf764219ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/53b6084a090172a80668fecf764219ce.jpeg)
![images/5aab93c3c8056bd0bc51621a7f4e0303.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5aab93c3c8056bd0bc51621a7f4e0303.jpeg)

**"Lashley Guyton"**

---
---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:27*

How are you trying to home it? Control panel? Software (which software?) What operating system? Have you explicitly followed the documentation?


---
**Lashley Guyton** *July 08, 2018 21:33*

**+Tech Bravo** trying to Home using Lightburn.  Windows 10 Home. I went step by step on the install documents. 


---
**Lashley Guyton** *July 08, 2018 21:34*

Do I need to format the disk like it’s asking me to? 


---
**Lashley Guyton** *July 08, 2018 21:34*

![images/701e47bc89beaba7ea6127570936a83f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/701e47bc89beaba7ea6127570936a83f.jpeg)


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:36*

The micro sd card? Not usually. It comes preloaded with with the firmware and config files.


---
**Lashley Guyton** *July 08, 2018 21:38*

**+Tech Bravo** that’s what I thought so I didn’t format it. Just left it alone until I could ask you guys. 


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:39*

so when does that dialog box appear? what are you connecting to make that happen?


---
**Lashley Guyton** *July 08, 2018 21:41*

**+Tech Bravo** just plugged in the cable from the c3d. And opened Lightburn. Went through the set up in Lightburn and then tried to home it from lightburn. Closed the program and this window was open in the background. Nothing else is plugged into the computer. 


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:41*

since the image you provided shows the microSD card inserted into the cohesion3d board i will assume that this happens when you plug the usb from the laser into the pc?


---
**Lashley Guyton** *July 08, 2018 21:42*

**+Tech Bravo** correct. 


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:44*

do you have the ability to read micro SD via a reader or SD adapter directly from the PC?


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:46*

cancel that window and browse to the E: drive, open it and show me whats there.


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:48*

never mind that. we need to rewrite the files to the sd card so you will need to read/write the card via PC (not via usb cable and cohesion)


---
**Lashley Guyton** *July 08, 2018 21:52*

**+Tech Bravo** when I click on it here, it says it needs to be formatted. ![images/b4816ef4232d13175aa2e7aab3549a3d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b4816ef4232d13175aa2e7aab3549a3d.jpeg)


---
**Lashley Guyton** *July 08, 2018 21:53*

**+Tech Bravo** ok. I’ll have to figure out a way to do that. 


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 21:54*

okay you will need to insert the micro SD card into your PC to rewrite the card. (always power down the laser before inserting/removing the micro SD card).


---
**Lashley Guyton** *July 08, 2018 21:55*

**+Tech Bravo** alright. I saw the instructions to do that I think. So I just need to find a way to get the card directly to the computer. 


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:00*

batch 3 config: [techbravo.net - NOTE Lines must not exceed 132 characters ## Robot module ...](https://techbravo.net/lasers/files/config.txt)


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:00*

batch 3 firmware: [techbravo.net - techbravo.net/lasers/files/firmware.bin](https://techbravo.net/lasers/files/firmware.bin)




---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:02*

these are available in the link provided in the documentation. the links above are direct links i made to prevent a common mistake of copying the folder onto the card. these 2 files listed above must be in the root directory


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:03*

so download the firmware.bin and config.txt files above. insert your microSD into the pc, format it (FAT format) then copy the 2 files onto the card. reinstall card in cohesion board and power up.


---
**Lashley Guyton** *July 08, 2018 22:05*

**+Tech Bravo** ok. And the only way to format it is to plug it directly to computer?


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:06*

right click and save as for the config file or it will open in the browser


---
**Tech Bravo (Tech BravoTN)** *July 08, 2018 22:08*

**+Lashley Guyton** yes. for one reason the latest firmware disable windows mass storage so you will no longer see the sd card as a drive when its in the board. this is to help prevent com issues


---
**Lashley Guyton** *July 09, 2018 00:41*

Does this look right now?





![images/f87569ed053aef951216eeea69345904.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f87569ed053aef951216eeea69345904.png)


---
**Tech Bravo (Tech BravoTN)** *July 09, 2018 01:08*

Yes. 


---
**Lashley Guyton** *July 09, 2018 01:49*

**+Tech Bravo** thanks. Working like a champ now. Sorry to be a needy noob. 


---
**Tech Bravo (Tech BravoTN)** *July 09, 2018 01:50*

Glad you got ot going. No problem. That's why everyone is here


---
*Imported from [Google+](https://plus.google.com/114884907345259366479/posts/Qzo8LRmXWzt) &mdash; content and formatting may not be reliable*
