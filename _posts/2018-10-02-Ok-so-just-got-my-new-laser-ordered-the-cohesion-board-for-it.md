---
layout: post
title: "Ok so just got my new laser ordered the cohesion board for it"
date: October 02, 2018 02:24
category: "K40 and other Lasers"
author: "Chris P"
---
Ok so just got my new laser ordered the cohesion board for it. It all looks plug and play. But how does it control the power of the laser ? 

My laser has a digital meter and buttons for laser power, am I suppose to disconnect this panel ? 





![images/37096bc4974f13c06e4d3ecdcc7a5fa5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/37096bc4974f13c06e4d3ecdcc7a5fa5.jpeg)
![images/65f2d8fc57c9b5eb3e6e4f559f9281d5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/65f2d8fc57c9b5eb3e6e4f559f9281d5.jpeg)

**"Chris P"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 02:43*

Please just follow the instructions exactly and trust in the power of the C3D. 


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 03:15*

The C3D controls power thru L in the large power plug. You literally need to move the 4 wires from the stock board to the C3D. 



Also, since you have a larger machine, you will need to adjust for the new bed size in config: [https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/](https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)

[lasergods.com - Setting Custom Bed Size For The Cohesion3d Mini](https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 03:17*

Your panel stays and becomes a hard current limiter (set it and forget it). Cohesion takes care of the rest.


---
**chriskellyy** *October 02, 2018 22:47*

ok so installed and turned on, all it does is go to home and bounce off the limits for a minute, when it stops is says alarm lock and lightburn has no controll


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 23:32*

can you send pics of the c3d board connections please so i can compare them to the nano pics above?




---
**Chris P** *October 03, 2018 00:41*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/109920268070321490124/albums/6607924450374324337/6607924447663111954)


---
**Chris P** *October 03, 2018 00:42*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/109920268070321490124/albums/6607924672597464609/6607924672536689570)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:12*

okay so, with the laser off, manually articulate the head (VERY VERY slowly so you don't generate any electrical feedback to the board from the spinning motors) to the approximate center of the bed.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:20*

then go into lightburn and click the devices button in the bottom of the laser panel. Your device list should populate and show a device. Double Click that device in the list.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:20*

the first selection (cohesion3d smoothie) should be highlighted, if not go ahead and highlight it and click next.




---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:20*

make sure serial/usb is highlighted and click next.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:20*

make sure the x axis dimension is 300 and the y axis dimension is 200 (for now, we will change that later) and click next.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:21*

make sure front left is selected as device origin and turn off auto home on startup then click next. click finish. click ok.


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 01:21*

before we power up i forgot to ask a question: have you modified the config file at all? if so report back before going any further


---
**chriskellyy** *October 03, 2018 21:59*

no didnt modify anything,

did the above,and powered on, ok and moves in lightburn now.

now what?

just a fyi my bed is 400x600


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 13:09*

[lasergods.com - Setting Custom Bed Size For The Cohesion3d Mini](https://www.lasergods.com/setting-custom-bed-size-for-the-cohesion3d-mini/)



so the config file needs to be modified to reflect the proper bed size in the y direction...


---
**Chris P** *October 04, 2018 14:03*

Ok done. 

Thank you for this help you are a good fellow :)


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 14:04*

you are very welcome. let us know when you have everything squared away :)


---
**Chris P** *October 04, 2018 16:09*

I have changed the file for my bed size 

What is the next step. 


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 16:10*

did you test to make sure the head moves in the proper direction and homes correctly and such? i dont remember if i sent you that video on testing it or not




---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 16:11*

[lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Chris P** *October 04, 2018 16:32*

No no testing has been done yet I will do that when I'm home from work. We turned off auto home cause it would just bounce off the limit. 


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 16:43*

Yeah leave that off until you get everything tedted and adjusted


---
**chriskellyy** *October 06, 2018 01:56*

ok everything moves the right direction whats the next step




---
**Tech Bravo (Tech BravoTN)** *October 06, 2018 01:57*

I suppose turn on auto home on start and reboot lightburn and the laser and see


---
*Imported from [Google+](https://plus.google.com/109920268070321490124/posts/dXdk96yFwqH) &mdash; content and formatting may not be reliable*
