---
layout: post
title: "(Quick) Update #3: The assembler has informed me that the boards have all been assembled at this point and the testing has been good"
date: December 30, 2016 02:43
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
(Quick) Update #3:



The assembler has informed me that the boards have all been assembled at this point and the testing has been good.

We're actually running a bit behind due to the New Year holiday - they still need to package everything and get it shipped.  I've been told that they will ship on Tuesday, so if Fedex gets here by Thursday or Friday, well let's just say that I'll be working hard that weekend.  For anyone doing the math, that would work out to me receiving the boards exactly 6 weeks from launch (Thanksgiving day) and I'm just saying I'll take a few days to start shipping.

100 PWM Cables arrived today.  

I got 100 A4988's already but we're going to need way more than that and I'm looking at getting a bunch DHL'd over here so as to not hold everything up.  



Anyways, happy New Year to everyone, I'll keep you all posted, don't forget to vent those laser fumes.  I think "out with the old, and in with the new" is a particularly fitting phrase for us all. 



Cheers,

Ray

![images/85bb07067f8c7f2178488faeb104cb87.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/85bb07067f8c7f2178488faeb104cb87.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Steve Anken** *December 30, 2016 04:14*

The community is growing...


---
**Bill Keeter** *December 30, 2016 16:13*

Awesome. I'll be putting in my order this weekend. 


---
**Samer Najia** *December 30, 2016 20:18*

HNY Ray!


---
**Andy Shilling** *December 30, 2016 20:51*

Sounds awesome. HAPPY NEW YEAR.


---
**Chris Sader** *January 01, 2017 06:39*

**+Ray Kholodovsky** don't let those A4988s delay my order, I've got a bunch of them lying around. ;)


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 06:10*

Thanks **+Chris Sader** . I just ordered another couple hundred, should be here in a week if all goes well.  :)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Dzy9HTgsWnB) &mdash; content and formatting may not be reliable*
