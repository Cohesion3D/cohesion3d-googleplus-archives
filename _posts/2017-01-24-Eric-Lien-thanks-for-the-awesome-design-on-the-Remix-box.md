---
layout: post
title: "Eric Lien thanks for the awesome design on the Remix box"
date: January 24, 2017 13:12
category: "General Discussion"
author: "Ariel Yahni (UniKpty)"
---
**+Eric Lien**​ thanks for the awesome design on the Remix box. I did mod it to accommodate a 90x90 fan.

![images/fc2ee1619e6a8e6f4c79b0d4062e0f72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fc2ee1619e6a8e6f4c79b0d4062e0f72.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Samer Najia** *January 24, 2017 13:44*

I built one of these, but I think the next version I do will have a spot for a terminal block where all the wires will go and then all the wires will exit out of one side inside of 3 sides, maybe through a bundle near the USB....


---
**Ariel Yahni (UniKpty)** *January 24, 2017 13:52*

Well I could wrap everything , tie it to one of the posts on the side, but yeah better cable organizer would be good :)


---
**Dushyant Ahuja** *January 24, 2017 14:45*

I think all the cables are supposed to come out from the hole at the bottom right in your image. The others are simply for ventilation. That would allow you to easily open the case without having to take out the cable. 




---
**Bill Keeter** *January 24, 2017 14:47*

How much concern is there about overheating on the PCB that you would need a fan? I've got two 3d printers and have never had to run cooling on the boards.


---
**Dushyant Ahuja** *January 24, 2017 14:56*

**+Bill Keeter** the DRV8825 drivers require active cooling - hence the fan. 


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 15:27*

Yeah, the "semi-oval" cutouts are meant as the wiring ones - what Dushyant said. I'm finding it kind of tight especially with all my sleeved cables and we may want to add a 3rd one. 

**+Bill Keeter** A4988's run fine with Ambient cooling which is why I use them, everything else needs a fan. 


---
**Bill Keeter** *January 24, 2017 15:40*

Are you pushing or pulling the air with the fan?


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 15:43*

Well a fan typical does both those things :)

We want it to blow onto the drivers. So pushing down.


---
**Eric Lien** *January 24, 2017 15:44*

**+Ray Kholodovsky** I'm heading out of town but I can make the modification for the taller version before I go. What is the consensus on the additional height you want.


---
**Samer Najia** *January 24, 2017 15:46*

**+Eric Lien** I would say at least 30mm


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 15:55*

30 is a lot, I was thinking more like 15-20.  

If you can mirror the wiring cutout so that we have a 3rd one at top left, that would be great too. I'm not in a hurry. 


---
**Bill Keeter** *January 24, 2017 16:13*

Random thought. Could leave the top open with screw points for the fan. Then create a separate cover to act as a shield over the exposed fan. 


---
**Eric Lien** *January 24, 2017 16:22*

**+Bill Keeter** Fan is designed to go inside. There is another version already available with a lower profile for an externally mounted fan. Looks like I have a few changes... and unfortunately need to hit the road. I will look into playing with the design tonight at the hotel.


---
**Dushyant Ahuja** *January 24, 2017 16:47*

**+Samer Najia** you can always increase the Z scale on the existing models. It will increase the thickness of the top, but shouldn't be too much. 


---
**Eric Lien** *January 24, 2017 17:02*

**+Dushyant Ahuja** I made a version and sent it to Ray yo look over. Went 20mm higher. And added another "D" shaped wiring cutout per Rays request. Once he looks over it, perhaps he can get it up for you guys to look at.


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 17:03*

Printing it now, will report back and link it soon.  Thanks Eric!


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 21:34*

It's still "doing stuff"![images/8d436852e1cbaf9ce594b4c8324d42bf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d436852e1cbaf9ce594b4c8324d42bf.jpeg)


---
**Ariel Yahni (UniKpty)** *January 24, 2017 21:40*

Nice color


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 23:03*

Just finished. Like, bed is still hot, just finished. 

**+Samer Najia** yours is brighter but mine is deeper :)![images/98fe293526ac767a3cfbc931c31e8fac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/98fe293526ac767a3cfbc931c31e8fac.jpeg)


---
**Eric Lien** *January 24, 2017 23:13*

**+Ray Kholodovsky** now we get to see how our printers match up for tolerances putting your lid on the base I printed :)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 23:16*

**+Eric Lien** I already did that :)![images/b8806682c9bb1955b62511cf8ab92455.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8806682c9bb1955b62511cf8ab92455.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 23:16*

![images/08de3ef64733d400e442a8fd48896d1a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/08de3ef64733d400e442a8fd48896d1a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 24, 2017 23:18*

The "hills" are not perfect, a few tenths of a mm off. ![images/8ea8933cf4d28cf13ce3bfa396cd0730.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ea8933cf4d28cf13ce3bfa396cd0730.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 04:10*

Printed both parts, they snap together fine. I have not put electronics inside yet.

Here's the link guys: [drive.google.com - Fan Inside Taller - Google Drive](https://drive.google.com/drive/folders/0B1rU7sHY9d8qS2JRYXF6NjJYOGs?usp=sharing)

**+Samer Najia** time to get to work :)


---
**Marc Pentenrieder** *February 03, 2017 15:27*

**+Eric Lien** Hi Eric, is it possible to get the remix case as .stp files?

I have to include some adapter boards from ray into the case!


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 15:30*

I might be able to handle the export, check back later today. I also want to increase the wiring slot to full height, it is necessary if you have a tall heatsink (which I do :)) 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/7mcPChakTLp) &mdash; content and formatting may not be reliable*
