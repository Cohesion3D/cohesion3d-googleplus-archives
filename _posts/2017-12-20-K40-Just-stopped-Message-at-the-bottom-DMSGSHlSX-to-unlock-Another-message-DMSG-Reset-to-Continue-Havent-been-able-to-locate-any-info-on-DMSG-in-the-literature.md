---
layout: post
title: "K40 Just stopped. Message at the bottom DMSG:'SH'l'SX' to unlock Another message DMSG Reset to Continue Haven't been able to locate any info on DMSG in the literature"
date: December 20, 2017 17:06
category: "General Discussion"
author: "bbowley2"
---
K40 Just stopped. Message at the bottom DMSG:'SH'l'SX' to unlock

Another message DMSG Reset to Continue

Haven't been able to locate any info on DMSG in the literature.

If I disconnect the K40's power and unplug my C3D board I can get the K40 to give me jog control but when I execute RUN the K40 and the k40 locks up and I get one of the error messages. GRBL firmware.





**"bbowley2"**

---
---
**Joe Alexander** *December 20, 2017 18:45*

suggestion: dont copy and paste all over the place. most of us are in all the groups I believe so repetitive posting is rather annoying.


---
**LightBurn Software** *December 24, 2017 05:57*

It’s just “MSG: $H or $X to unlock”. Usually you get that if you hit a limit switch, so check to make sure those aren’t tripped.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/9LXHeGU1XXx) &mdash; content and formatting may not be reliable*
