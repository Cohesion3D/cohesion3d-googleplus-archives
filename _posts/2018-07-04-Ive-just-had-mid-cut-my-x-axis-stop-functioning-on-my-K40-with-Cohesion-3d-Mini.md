---
layout: post
title: "I've just had, mid cut, my x axis stop functioning on my K40 with Cohesion 3d Mini"
date: July 04, 2018 22:14
category: "C3D Mini Support"
author: "David Piercey"
---
I've just had, mid cut, my x axis stop functioning on my K40 with Cohesion 3d Mini.



Swapping a new stepper driver doesn't do anything different.    Mine uses a ribbon wire connection, but nothing there seems to have been moved/adjusted.    



I'd replace it, on principle, but haven't been able to source one that matches it (any ideas anyone?).



Is the stepper motor, other than the ribbon, the most likely culprit?    Any other thoughts/suggestions? 





**"David Piercey"**

---
---
**Anthony Bolgar** *July 04, 2018 22:53*

Make sure the stepper wiring is plugged into the stepper if it is equipped with a connector.




---
**Joe Alexander** *July 05, 2018 01:05*

if you have to replace it i used a 17HS13-0404S and a mechanical limit switch which allowed me to also eliminate the ribbon cable. motor is significantly quieter than its predecessor and doesn't get super hot due to its low inductance.


---
**David Piercey** *July 05, 2018 01:24*

**+Anthony Bolgar** Checked that. All was connected, but even forgoing the ribbon cable and plugging the x-axis stepper motor into the y axis on the board, the motor works fine...so neither x nor y-axis motors will work on the X-axis stepper (4 pin connection), the either on the ribbon connector for the X position, and both motors work on the y-axis stepper (4 pin connector).



**+Joe Alexander** I may hit you up for a bit more info if I go that route. Thank you.



 In fact, I took the stepper motor (x-axis) out, and it works fine when plugged directly in to the y axis, but not the x-axis (instead of the ribbon connection).  Similarly, while the y-axis stepper motor works fine on the y-axis, it also will not work on the x-axis.



This is independent of trying several stepper drivers that work on the y-axis fine, but nothing, still, on the x-axis.






---
**David Piercey** *July 05, 2018 01:30*

....It was almost a week of it working perfectly too.




---
**Anthony Bolgar** *July 05, 2018 02:08*

Are you running smoothieware or GRBL-Lpc? It is easy to remap the X axis to the Z or A if you are running smoothieware, and Ray has a GRBL-Lpc build somewhere that remaps the X axis that he made for someone else in your situation.


---
**David Piercey** *July 05, 2018 02:49*

**+Anthony Bolgar**   Smoothie.  I presume it's changes in the config.sys....  



Would this be the adjustment I need to make to move the x-axis over to the a-axis?



# Stepper module pins ( ports, and pin numbers, appending "!" to the number will invert a pin )

alpha_step_pin                               2.3              # Pin for alpha stepper step signal

alpha_dir_pin                                0.22!              # Pin for alpha stepper direction

alpha_en_pin                                 0.21              # Pin for alpha enable pin

alpha_current                                1.5              # X stepper motor current

alpha_max_rate                               30000.0          # mm/min




---
**Anthony Bolgar** *July 05, 2018 03:08*

Yup, that would be it. Should get you back up and running.




---
**David Piercey** *July 05, 2018 15:09*

**+Anthony Bolgar** Alas, still not working, bar very intermittently.



Should the ribbon cable still be working with the stepper motor switch?  

With it reconnected, nothing happens.  With the motor plugged in, directly, it'll spin,  but inconsistently, the console errors out and I have to restart everything (hardware/software).




---
*Imported from [Google+](https://plus.google.com/114972156206786232028/posts/VMqHAKM7LyF) &mdash; content and formatting may not be reliable*
