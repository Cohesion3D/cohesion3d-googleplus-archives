---
layout: post
title: "May be more of a smoothie question but I'll give it a shot"
date: December 06, 2017 15:41
category: "General Discussion"
author: "Carl Fisher"
---
May be more of a smoothie question but I'll give it a shot.



When using a rotary, does it calculate in degrees per step or do you have to manually adjust the config for steps per mm every time?



i.e. if my rotary setup is known to do 0.3 degrees per step then that is consistent. If I have to do mm/step then I have to adjust the config file every time to accommodate different diameter materials.





**"Carl Fisher"**

---
---
**Joe Alexander** *December 06, 2017 17:42*

depends on the program I would imagine. If you enable the a-axis in Laserweb4 you can create a raster operation, check the a-axis box and set the material diameter. it then should do the math for you, although I cannot attest to this myself.


---
**Carl Fisher** *December 06, 2017 20:55*

So if that's the case, what value do you use in the config for the steps per mm? I'd think there has to be a baseline value.




---
**Joe Alexander** *December 07, 2017 07:54*

my first guess would be to use a known diameter piece, load it up, and then mark it on the top. manually jog it the same amount as the diameter and see if your mark returns to the same location? verify with a different diameter material to make sure it still functions appropriately


---
**LightBurn Software** *December 10, 2017 03:01*

Would imagine you’d want to make the steps/mm value semi-meaningful so you could tune the acceleration value properly.  After that, as long as the software knows the scale to apply to convert from your design scale to the circumference scale, it shouldn’t matter.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/UMjWVEot5qo) &mdash; content and formatting may not be reliable*
