---
layout: post
title: "K40 Upgrade with Cohesion3D Mini the Micro SD snapped in to"
date: November 13, 2017 00:29
category: "C3D Mini Support"
author: "Bill Schober"
---
K40 Upgrade with Cohesion3D Mini the Micro SD snapped in to.  Can I use a larger than 2 GB card like say a 32 GB and can I download the files for it?





**"Bill Schober"**

---
---
**Bill Schober** *November 13, 2017 01:06*

Ok, I found the files and downloaded them.  Is there any limit on the size or speed of Micro SD chip? 




---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 01:35*

I wouldn't go higher than 32gb. Doesn't matter anyways because you'll format it FAT32. The files you need are in a Dropbox at the bottom of instructions. Cohesion3D.com/start [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Bill Schober** *November 13, 2017 01:55*

Thank you!  Do you know what a constant high pitched sound coming from the LED board means?  Is it because there is no Micro SD card in the Cohesion3D Mini?




---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 01:56*

Yes. With the config files on the card, it will beep for 3 seconds on power up then stop (once the board has booted) 


---
*Imported from [Google+](https://plus.google.com/+BillSchober/posts/WCnczrxqKYi) &mdash; content and formatting may not be reliable*
