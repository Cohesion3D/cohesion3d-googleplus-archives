---
layout: post
title: "I have the K40 laser and am interested in getting the Cohesion 3D or the mini upgrade"
date: December 08, 2018 22:25
category: "K40 and other Lasers"
author: "Tammy Novak"
---
I have the K40 laser and am interested in getting the Cohesion 3D or the mini upgrade.  I have heard very good things about this upgrade, but am not very technically inclined, so am not sure what I need for the upgrade.  I do not have a rotary, and although that may be something I may add down the road, I wouldn't need anything right now for that.  To my eyes, the wiring looks a little different.  Can you help me out?  



![images/12ed7b3f2026f8f588bcf603f77a26e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/12ed7b3f2026f8f588bcf603f77a26e7.jpeg)
![images/e405b556ad2c274e3839bd48039bee77.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e405b556ad2c274e3839bd48039bee77.jpeg)
![images/f6de7157cdf89d21d2bf15e74f0d627b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f6de7157cdf89d21d2bf15e74f0d627b.jpeg)
![images/feb749872950befa5cf9caf16ddc486d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/feb749872950befa5cf9caf16ddc486d.jpeg)
![images/c5e777f0338746c3dd90813f4272948b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c5e777f0338746c3dd90813f4272948b.jpeg)
![images/25bfa957d70daef662b5f76622f46b76.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/25bfa957d70daef662b5f76622f46b76.jpeg)

**"Tammy Novak"**

---
---
**Tammy Novak** *December 08, 2018 22:50*

Also, as you can see in my photo I have the digital control for power.  Would I be replacing this also with upgrade to Cohesion?


---
**ThantiK** *December 09, 2018 04:50*

Everything looks like a pretty direct 1:1 replacement.  It's the 'ribbon cable' variant found here: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions. : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



Your digital power control looks to be wired into the power supply directly, you don't even have to touch that.


---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2018 05:51*

The new LaserBoard is better in every way for only a tiny difference in price, so even if you don’t want to do Z table/ rotary it’s still the better value. 


---
**ThantiK** *December 09, 2018 20:50*

Seconding the Cohesion LaserBoard over the mini.  The Trinamic drivers are a <i>load</i> better, standard sized SD cards are cheaper to utilize and more laptops have standard sized slots than micro.  Additionally it has a bunch of expansion and breakouts.  I have no affiliation or stake in Cohesion3D, it's just a great board.



The upgrade info I linked earlier is essentially identical for your machine, however.  The LaserBoard also comes with 4 drivers included as part of the board, instead of the only 2 for the mini.  This is perfect if you want a Z table and Rotary down the line.


---
**Tammy Novak** *December 10, 2018 02:39*

**+Ray Kholodovsky** sorry for the confusion.  I really want to get rid of the digital power control as the display is messed up and a pain to work with.  Do you also carry the manual dial like some models have?  I would be more confident in the power level I am setting if I could replace or alleviate that.  And thank you so much for your help!  I am going to place an order for the new Laserboard shortly


---
**Ray Kholodovsky (Cohesion3D)** *December 10, 2018 02:44*

Yes, I just received some potentiometers to stock and I will list them on the website tomorrow. 

That will still require soldering. 



And to be clear, I recommend doing exactly one upgrade at a time. So you’d still install the LaserBoard with your digital panel first and get everything working before swapping out the digital panel for a pot. 



I am still working on a full “potentiometer drop in kit” which I hope would make the wiring easier for your digital display. 


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 01:49*

As promised, the bare multi-turn potentiometer is now listed on the website. 


---
**Sean Cherven** *December 12, 2018 13:28*

Doesn't the LaserBoard control the laser power? If so, why is a potentiometer needed?


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2018 15:38*

To set the max power, which the C3D will then control within that range. This allows for better grayscale resolution. 


---
**Sean Cherven** *December 12, 2018 15:43*

Ah, that makes sense. 


---
**Tammy Novak** *December 18, 2018 20:57*

**+Ray Kholodovsky**  Ray I received my order yesterday and the Laserboard went in smoothly as promised, that was great thank you.  I am having a difficult time finding installation instructions for the LCD Graphic, though.  Can you help me out?






---
**Tammy Novak** *December 18, 2018 21:24*

Connection looks simple enough, plugged this into the "LCD Graphic Expansion on the Laserboard, but I get no power to the Graphic Display.  

![images/1181b4853ce0e17bbf21726ede8f1432.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1181b4853ce0e17bbf21726ede8f1432.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2018 22:03*

The GLCD should be installed the same way as on Mini, can you check  this: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions. : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



and if that doesn't work then I'll need pics of your board and wiring.


---
**Tammy Novak** *December 19, 2018 04:24*

Here's a pic of my Laserboard... hopefully you can zoom in enough to see. I plugged the 2 ribbon cables into the adapter, then plugged the adapter into the Laserboard.  

 (Removed the LCD for the pic so you could see the plug)

![images/98b20eadb356bf1bfad9dfec23866a63.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/98b20eadb356bf1bfad9dfec23866a63.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2018 04:33*

And now a pic with the GLCD Adapter and cables plugged in, please.  



Have you traced that you've plugged EXP2 and EXP1 from the GLCD into the correct slots on the adapter? 


---
**Tammy Novak** *December 19, 2018 15:37*

Here's a short video.  I also double checked the connections... EXP1 and EXP2 on board to EXP1 & 2 on adapter

![images/b20f53709cb583e5487213c30e2e9aff](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b20f53709cb583e5487213c30e2e9aff)


---
**Tammy Novak** *December 19, 2018 15:45*

Weird thing Ray, plugged it back in to take a pic of it plugged into Laserboard, and it powered up.   Just like when I take my truck to the shop,  everything miraculously works!   lol   Thank you.  sorry to be such a bother!




---
*Imported from [Google+](https://plus.google.com/105496110568876018217/posts/ZZsJVDFhtfz) &mdash; content and formatting may not be reliable*
