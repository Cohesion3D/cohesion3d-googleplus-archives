---
layout: post
title: "Hi! I have a china machine (3040) 40w and have a bad program LaserDRW and want to change that"
date: November 07, 2018 17:31
category: "K40 and other Lasers"
author: "Lennarth Eriksson"
---
Hi!

I have a china machine (3040) 40w and have a bad program LaserDRW and want to change that.



I read about Lightburn softwere and the say i most update my machina hardwere to: 



[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/?fbclid=IwAR2xvKamFb_KQW188LUeG0cG5PGXMg-3_nOhs3tf7YUXBZLqPwRQTFo4G1g](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/?fbclid=IwAR2xvKamFb_KQW188LUeG0cG5PGXMg-3_nOhs3tf7YUXBZLqPwRQTFo4G1g)



My question now. It's this right to do?



The picture are my thing today.



Thanks

/Lennarth (Stockholm)



![images/b3c8de71fbf573ef1bb5f7189e5f7de9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b3c8de71fbf573ef1bb5f7189e5f7de9.jpeg)
![images/d1c9f495ba16cad5d837c94b56819106.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d1c9f495ba16cad5d837c94b56819106.jpeg)
![images/9fb3cb22dacd784a3b2353fe4e28a00a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9fb3cb22dacd784a3b2353fe4e28a00a.jpeg)

**"Lennarth Eriksson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 07, 2018 20:40*

I would like to see more pictures of the electronics inside the laser and a clearer picture of the board so I can read the writing on it. 


---
**Richard Wills** *November 07, 2018 21:01*

I have the exact machine, it is a direct swap.


---
**Kelly Burns** *November 08, 2018 01:54*

Yes.  Rays Cohesion 3D Board combined with Lightburn will be a major upgrade.   


---
**Lennarth Eriksson** *November 09, 2018 14:20*

Kelly! I have a extra stepmotor for the Rotary. I need more thing?

![images/ee466fdacf6e15eec8e42a700962705b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ee466fdacf6e15eec8e42a700962705b.jpeg)


---
**Lennarth Eriksson** *November 09, 2018 14:21*

**+Richard Wills** Nice! Have you do the upgrade!? And what thing you buy? Can you write a list so I now what I order.........Thanks!


---
**Richard Wills** *November 09, 2018 14:26*

**+Lennarth Eriksson** yes I have done the upgrade. What you order will depend upon what firmware you want to use. I use GRBL so I ordered everything in upgradeexcept the GLCD and adaptor. If you want to run smoothie you will need everything available.


---
**Lennarth Eriksson** *November 09, 2018 14:52*

I have try to get right in this order. Its okej? Right for me?

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/?fbclid=IwAR2xvKamFb_KQW188LUeG0cG5PGXMg-3_nOhs3tf7YUXBZLqPwRQTFo4G1g)



 




---
**Lennarth Eriksson** *November 09, 2018 14:54*

See my order......Its okej?

![images/3663c91f929d19aedf66b676323f9c3d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3663c91f929d19aedf66b676323f9c3d.jpeg)


---
**Richard Wills** *November 09, 2018 14:57*

**+Lennarth Eriksson** you will need lightburn to run the laser and you will need and external stepper for your rotary if you are running grbl firmware. I would recommend running grbl as the raster speeds are quicker than smoothie ware 


---
**Richard Wills** *November 09, 2018 14:58*

**+Lennarth Eriksson**  you will need power supply upgrade as well. But looks good


---
**Lennarth Eriksson** *November 09, 2018 15:22*

Okej.....I am don't understund if I have grbl firmware?! What you mean?




---
**Richard Wills** *November 09, 2018 15:24*

You will have to load GRBL firmware on board when you get it installed. You just download it from cohesions website. 


---
**Lennarth Eriksson** *November 09, 2018 15:41*

**+Richard Wills** I can not find were i download that?




---
**Richard Wills** *November 09, 2018 15:45*

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)


---
**Lennarth Eriksson** *November 09, 2018 15:51*

I read: "This is a 3 axis version (XYZ), no rotary support in this compile: " Mmm I have Rotary too.....



Aha I change cable from X to Rotary cable. IT.s I do today.



So this work mm....




---
**Richard Wills** *November 09, 2018 15:57*

**+Lennarth Eriksson** there is a 4 axis version if you send me your email I will email to you when I get home


---
**Richard Wills** *November 09, 2018 15:59*

[dropbox.com - firmware_grbl-lpc_c3dmini_4axis_odA.bin](https://www.dropbox.com/s/vlxct86ldz6qi40/firmware_grbl-lpc_c3dmini_4axis_odA.bin?dl=0). I found the link


---
**Lennarth Eriksson** *November 09, 2018 16:12*

Thanks a lot! I have download now!




---
**Lennarth Eriksson** *November 09, 2018 16:16*

Now I most do a full list what i to order........Somebody can help me write down so I get right parts. I live in Stockholm and I am not so god on this......to change something to the better. I have the machine for hobby! But I are very happy to everyone who hlp me here to get the right stuff! And I want to get another softwere and LaserDRW who are not so good.




---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2018 18:34*

All the grbl compile options for 4 axis are right here: 

[https://plus.google.com/u/0/101895536937391772080/posts/46r6JWFaHxK](https://plus.google.com/u/0/101895536937391772080/posts/46r6JWFaHxK)


---
**Lennarth Eriksson** *November 10, 2018 09:11*

**+Richard Wills** the list I have in the order list I can to order now!? I have a licens to the softwere there. And I have all thing i need i think. Can you check one moore and write down the missing....!? Thanks.


---
**Richard Wills** *November 10, 2018 09:14*

**+Lennarth Eriksson**  your list is good but you also need power supply upgrade also


---
**Lennarth Eriksson** *November 10, 2018 09:23*

Okej. I put that inte list and get a order today all ready! Thanks!




---
**Lennarth Eriksson** *November 11, 2018 21:28*

**+Ray Kholodovsky** I have do the order and pay It! Hope everything go quickly now to Stockholm! If you see the order and i miss something get in touch!?




---
**Lennarth Eriksson** *November 21, 2018 20:17*

I have buy a licens and get the code now on email. I can only use that one time?! I have two computers.....Rest of my order have not coming yet. So I wait before install the softwere I think.......


---
**Ray Kholodovsky (Cohesion3D)** *November 21, 2018 20:20*

1 key is good for 2 computers. 

[http://cohesion3d.com/lightburn-software/](http://cohesion3d.com/lightburn-software/)


---
**Lennarth Eriksson** *November 21, 2018 20:59*

**+Ray Kholodovsky** Aha! Thanks......




---
**Lennarth Eriksson** *November 27, 2018 17:37*

I get my thing I order now and I get to try this about a month. I have so much work to Christmas now. I take It under 28 dec-  6 jan 18/19. Hope I can get help if I have problem. I dont order a Graphic LCD....I need that? 


---
*Imported from [Google+](https://plus.google.com/+LennarthEriksson/posts/hLLHZRBtTsv) &mdash; content and formatting may not be reliable*
