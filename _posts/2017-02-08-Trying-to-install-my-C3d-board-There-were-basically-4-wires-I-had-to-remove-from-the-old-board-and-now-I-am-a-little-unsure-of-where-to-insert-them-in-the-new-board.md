---
layout: post
title: "Trying to install my C3d board. There were basically 4 wires I had to remove from the old board and now I am a little unsure of where to insert them in the new board"
date: February 08, 2017 10:09
category: "General Discussion"
author: "Andreas Benestad"
---
Trying to install my C3d board. 

There were basically 4 wires I had to remove from the old board and now I am a little unsure of where to insert them in the new board. One of them (green, red, pink  cable) has only one logical input. The others I just don't know. Could you please help me?

Thank you for your help!



![images/5db68930328070e4f1837c373fc6ce4f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5db68930328070e4f1837c373fc6ce4f.jpeg)
![images/4671f56d613ee0e4a47a4aacf26665a6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4671f56d613ee0e4a47a4aacf26665a6.jpeg)

**"Andreas Benestad"**

---
---
**Andreas Benestad** *February 08, 2017 10:29*

The power cable is supposed to go at the four pin connector there, right..? What should I do to make it fit? 

![images/05f06ccc0fd26cdea8be833b4879f15e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/05f06ccc0fd26cdea8be833b4879f15e.jpeg)


---
**Andreas Benestad** *February 08, 2017 10:31*

And these two guys... Where do they belong? 

(Btw, I have tried to make sense of Carl's Guide, but I have no ribbon cable like he has and I don't seem to be able to transfer his info to my machine...Sorry...)

![images/baee10aeb01d23996cff604bee2ad939.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/baee10aeb01d23996cff604bee2ad939.jpeg)


---
**Andy Shilling** *February 08, 2017 11:32*

Those two look like stepper motor cables, please confirm that they are first. The power plug will fit just cut it down to for pins but make sure the wires go to the right pins. Compare the C3D and moshi board for this.


---
**Andreas Benestad** *February 08, 2017 11:41*

**+Andy Shilling** pretty sure they are for the stepper motors yes. 👍

Do I simply put the cable for the y axis and x axis like this? What is the correct way of facing for the plug? (The picture shows that they fit both ways...) 

![images/4add1aeed18bcd1856c0645bd2790a3c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4add1aeed18bcd1856c0645bd2790a3c.jpeg)


---
**Andy Shilling** *February 08, 2017 11:45*

If they go to the stepper motors then yes but you might need to reverse them but you won't know that until you test it with the jog in laserWeb. 



Do you not have the 12 pin cable on your system?


---
**Andreas Benestad** *February 08, 2017 11:47*

Here is the old board before I took it out. No 12 pin cable.

![images/64b003f9de344d1d3e8b08c1fc7c6afa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/64b003f9de344d1d3e8b08c1fc7c6afa.jpeg)


---
**Andy Shilling** *February 08, 2017 11:58*

Ok so they are definitely steppers but I'm not sure how your end stops are connected. Do you have optical or mechanical endstops. But yes the stepper go where you put them on the C3D.


---
**Andreas Benestad** *February 08, 2017 12:00*

Mechanical steppers. 👍


---
**Andy Shilling** *February 08, 2017 12:01*

Ok that's cool so you should have another couple of leads from them to plug in too


---
**Andreas Benestad** *February 08, 2017 12:01*

![images/752f5d428cf45a4976a57990ab54ed8a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/752f5d428cf45a4976a57990ab54ed8a.jpeg)


---
**Andy Shilling** *February 08, 2017 12:06*

Cool you might need an extra sure on those endstops, check the pinout diagram for the C3D.


---
**Andreas Benestad** *February 08, 2017 12:14*

The stoppers have been inserted in another place. It was the only place thst cable fit, so pretty sure it is correct.

Now that all the cables are connected, could I try to fire it up? Or is there anything I just check before taking the plunge? 


---
**Andy Shilling** *February 08, 2017 12:17*

No you need a minimum of three wires coming out of the endstops. Other wise it will not work properly.


---
**Andy Shilling** *February 08, 2017 12:18*

![images/5504b3a8d49a21d8a8fa498435904630.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5504b3a8d49a21d8a8fa498435904630.png)


---
**Andreas Benestad** *February 08, 2017 12:21*

Yes, I have three wires (green, light pink and dark pink) coming from the end stops as shown in this picture. 

![images/1ab9cb572ed8f6ed512810d25376a0ac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1ab9cb572ed8f6ed512810d25376a0ac.jpeg)


---
**Andy Shilling** *February 08, 2017 12:23*

Is that 3 for the two endstops or 3 wires each? Sorry but it just doesn't seem right at the moment.


---
**Andreas Benestad** *February 08, 2017 12:31*

I really appreciate your help!!

It is the same cable that was used in the old board. It seems to be 3 wires total for the two endstops. 

 The y stopper has both wires going to this pin: green and dark pink.

The x stopper only has the light pink wire going to the this pin. Could they have combined the blue stopper wire with the blue wire coming from one of the stepper motors? There is only 2 blue wires coming out on the other end (when there really should be 3? One stopper and 2 two steppers)


---
**Andy Shilling** *February 08, 2017 12:33*

Correction my bad, that is correct I've just checked the pinout again lol. As far as I can see your good to go, before you turn it on and test I would move the head to the centre that way if it jogs the opposite way your not going to grind out on the frame.


---
**Andreas Benestad** *February 08, 2017 12:37*

Tried to fire it now. The CPU fan did not start and the button/display lights started flashing... ideas?

Where can I find the pin out diagram?


---
**Andy Shilling** *February 08, 2017 12:41*

Have you just unplugged the moshi board and tried to plug the C3D straight in, or did you confirm the +24v G and L pins?


---
**Andy Shilling** *February 08, 2017 12:43*

The G and 24v pounds are round the other way on the C3D 


---
**Andreas Benestad** *February 08, 2017 12:44*

Double checking the power cable pins now. Thought we had it, but it seems we might have done it wrong...


---
**Andreas Benestad** *February 08, 2017 12:50*

Success! 😊

Thank you so much Andy! I really appreciate it, would have been so lost without your help.

Now I just need to set this up and get to know Laserweb a little. ☺


---
**Andy Shilling** *February 08, 2017 12:55*

Glad to help, now you're at the same stage as me. **+Ray Kholodovsky**​ has posted on here somewhere the extra config you need to do to get laserWeb to home to the top, G28.2 is one of the settings but I'm not sure on the rest as I'm only on my phone not in front of my Mac.


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 16:21*

Ok so now the one thing is to move the L wire from the harness to the -2.5 MOSFET pin. 


---
**Andy Shilling** *February 08, 2017 16:35*

What is the deal with moving the wire to pin-2.5? Just wondering why the board has L on it if it's not used?


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 16:38*

Long story. In short we had good results with replacing the pot from the beta testers. So L was just an on and off pin and the pot cable was the PWM pin. Then all these other machines said otherwise. So we had to route L to a hardware capable PWM pin - hence the 2.5 instead of the 2.6 which is connected to the power connector there. Fixed this on the next batch of boards. 


---
**Andy Shilling** *February 08, 2017 16:43*

Ok I sort of understand that,  so do i not need the pwm going to pin 2.4 then on the pwm header?


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 16:47*

There are 2 options. First is the way it came. You replace the pot using the PWM cable. Then the power connector with L stays the way it is. 

Second way is keep the pot, do not run the PWM cable (this is the white one part of the bundle) but L now needs to be hooked up to a different pin on the board that supports fast pulses. 


---
**Andy Shilling** *February 08, 2017 16:54*

Fantastic so that's means L to pin -2.5 and pot connected = what ever the pot is set to will be the max the pwm can produce. Correct?


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 17:00*

Indeed. LaserWeb power level sets a proportion of the pot. The recommendation seems to be to keep the pot around 10mA to make the tube last longer. 


---
**Andy Shilling** *February 08, 2017 17:07*

Cheers **+Ray Kholodovsky**​that was the last thing confusing me, I think lol


---
**Andreas Benestad** *February 09, 2017 07:51*

**+Ray Kholodovsky** Just to be sure: When looking at the pinout diagram you mean taking the "L (Laser fire)" wire from the "K40 Power" and inserting into the "- (P2.5 / BED)" in the Mosfet section?


---
**Andy Shilling** *February 09, 2017 07:57*

Yes that's correct, take it out of the moshi plug you cut to fit the power socket on the C3D ( where you swapped the+24 and G) 


---
**Andreas Benestad** *February 09, 2017 08:09*

**+Andy Shilling** ok, done. What does that do..? 😊 


---
**Andy Shilling** *February 09, 2017 08:15*

Lol, that allows PMW, which means the C3D can control the power of the laser via laserWeb if your psu sorts it. If you look over the forum you will see people posting all kinds of products they can produce. ie grey scale images. Obviously takes some practice to get the results some of these guys are getting.


---
**Andreas Benestad** *February 09, 2017 08:22*

I see. How can I know if my PSU had got what it takes?

Also, now that we moved the L wire the laser does not fire. No response when pressing Laser test...

So far I find Laserweb kind of confusing...What kind of settings and configurations do I need to adjust..?  Cannot find any tutorial on this.  


---
**Andy Shilling** *February 09, 2017 08:36*

Sorry I don't know how you find out about the pmw on your psu, Ray would be the one to answer that but I'm guessing it's around 3am in the USA now. Any laserWeb config's should be on the forum or laserWeb wiki page.


---
**Andreas Benestad** *February 09, 2017 10:18*

I understand. Do you know why moving the L has caused the laser to stop firing? I still have not been able to cut/engrave anything. It does fire when I physically press the test button on the k40.



Also, I tried the G28.2 code and it does make the homing go up. However, it crashes into the x-stopper. Seems like it is not responding like the y-stopper. 


---
**Andy Shilling** *February 09, 2017 11:19*

Can you check continuity on the x endstop to start. As for the laser there might be a config change that is needed, I'll see if I can find out what it is.


---
**Andreas Benestad** *February 09, 2017 11:24*

I am sorry for my lack of knowledge, what do you mean by checking continuity on the x endstop? 


---
**Andy Shilling** *February 09, 2017 11:29*

It's fine buddy believe me my knowledge isn't much greater than yours, I'm just relaying the things that I've learnt.



[en-us.fluke.com - How to test for continuity with a digital multimeter](http://en-us.fluke.com/training/training-library/test-tools/digital-multimeters/how-to-test-for-continuity-with-a-digital-multimeter.html)


---
**Andy Shilling** *February 09, 2017 11:52*

Have a look at this post the first comment might be what you need to get you.

 going[plus.google.com - Hi - ive seen quit a few conflicting photos of wiring for the Cohesion K40 la...](https://plus.google.com/109973479751741687593/posts/QUczShvkW43)


---
**Andreas Benestad** *February 09, 2017 13:21*

**+Ray Kholodovsky** so I moved L to the Mosfet pin, but as a consequence I cannot even test fire the laser. The head seems to be moving correctly, but I have no laser power. Before moving L I was able to test fire from laserweb, not anymore. Suggestions? 😊 


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 18:10*

3am sounds about right. What did I miss? 


---
**Andy Shilling** *February 09, 2017 18:50*

**+Andreas Benestad**​ was having trouble with one of the end stops and after moving the L to P2.5 lost fire on the laser. I've tried pointing to the right solution but I'm not 100% it's the right information.


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 18:55*

Kill power to laser and manually move head to left and top, using M119 command to read endstop states at each. Should read 0 for not pressed, 1 for pressed. 


---
**Andreas Benestad** *February 09, 2017 19:26*

**+Ray Kholodovsky** where do I insert this command? Just insert "M119" in the command line at the bottom?

(Man, this is a steep learning curve. Thanks for your patience!)


---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2017 19:27*

Yes. Command then press enter. Look for feedback in the little terminal directly above the box. 


---
**Andreas Benestad** *February 09, 2017 19:30*

👍 Will do that tomorrow morning. (South Africa time)


---
**Andreas Benestad** *February 10, 2017 07:10*

**+Ray Kholodovsky** both the endstops states are reading correctly.


---
**Andreas Benestad** *February 10, 2017 09:27*

Tried to move the L wire back to were it was first and that does give me back the test fire function in LW. But still no cutting/engraving when running a file, only the head moving around.


---
**Andy Shilling** *February 10, 2017 09:36*

Have you changed the config text file on the SD card for the laser pin from 2.4 to 2.5?


---
**Andreas Benestad** *February 10, 2017 09:38*

**+Andy Shilling** yes I changed the laser_module_pin to 2.5


---
**Andy Shilling** *February 10, 2017 09:40*

Ok then I will have to leave this with Ray, sorry


---
**Andreas Benestad** *February 10, 2017 09:42*

**+Andy Shilling**​ yeah...frustrating that I am not making any progress... 😕

Yesterday you asked me to check the pmw on my CPU. What does that mean..? What is pmw?


---
**Andy Shilling** *February 10, 2017 09:57*

 I didn't want it to.



PWM is Pulse Width Modulation which is essentially variable duty cycle - a 50% PWM is on for 1/2 the time and off for 1/2 the time repeated at a rate of say 5kHz 



Copied from another post, it may mean your psu doesn't support pmw.


---
**Andreas Benestad** *February 10, 2017 09:57*

**+Andy Shilling** **+Ray Kholodovsky**​ at the moment I have nothing connected to the "K40 PWM Output" on the board. Ray advised me not to use the white PWM cable included in the bundle, which means it is empty.

If I was use this cable in the PWM Output on the board I can only see one logical place to stick the other end, which would imply removing the cable coming from the lcd display (see picture).

![images/f8eb5f7a0b05218947a05a86442ebdce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8eb5f7a0b05218947a05a86442ebdce.jpeg)


---
**Andreas Benestad** *February 10, 2017 10:03*

**+Ray Kholodovsky**​ is this picture enough for you to determine if my PSU has got what it takes? 

![images/16092d57493b818afdc63b43642b5829.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/16092d57493b818afdc63b43642b5829.jpeg)


---
**Andy Shilling** *February 10, 2017 10:12*

Were are the cables from your pot they will need in to the psu


---
**Andreas Benestad** *February 10, 2017 10:16*

What is a pot? 😶


---
**Andy Shilling** *February 10, 2017 10:19*

The potentiometer, the grey knob on top that controlled your power with the moshi board.


---
**Andreas Benestad** *February 10, 2017 10:22*

Ah, my setup was digital. I had no knob, just a little digital display and buttons. The cables from this display to the PSU are the two white cables right by my finger in the picture posted earlier.


---
**Andy Shilling** *February 10, 2017 10:26*

Ok I think people are having problems with the digital type, I don't know enough about them to give any advice, Ray will definitely have to help you now as this is probably why you can not fire from pin 2.5, sorry buddy.


---
**Andy Shilling** *February 10, 2017 12:35*

You could easily replace the digital display with an ammeter and a pot like this.



[ebay.co.uk - Details about  10 turn Potentiometer 3590S Wirewound Variable Precision multi-turn POT - UK](http://www.ebay.co.uk/itm/121839695836)


---
**Andreas Benestad** *February 10, 2017 13:11*

Ok.  Progress. I have now been able to get the tube to fire. I installed the white PWM cable that came with the bundle (contrary to Ray's advice). So that is good. One step in the right direction. 


---
**Andy Shilling** *February 10, 2017 13:28*

Bonus 👍


---
**Andreas Benestad** *February 10, 2017 13:38*

It is only firing one consistent power so far. So something tells me the PWM is not quite there yet...


---
**HP Persson** *February 10, 2017 21:39*

Have the same PSU too, not at the laser fire just yet but i´ll chip in if i find a working solution, or read yours if you get there :)


---
**Andreas Benestad** *February 12, 2017 16:06*

**+HP Persson** cool man. Let's find this out. 👍


---
**Andreas Benestad** *February 13, 2017 12:37*

**+Ray Kholodovsky** I never got your approval on my way of connecting the PWM cable. I ended up using the white cable that came in the bundle. Connected it from the board to the PSU, removing "half" of the five pin connector cable that was running from the digital display to the PSU. 

![images/c2253d6271f4744fbf3dac4cc5c27c22.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c2253d6271f4744fbf3dac4cc5c27c22.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 12:48*

Oh, so if you are running the PWM cable then you need to change the 2.5 back to a 2.4! in config. I can't keep these things straight in memory...


---
**Andreas Benestad** *February 13, 2017 12:56*

That makes sense. What will that do? 2.4!

(We simly could not find a way to use the existing cable like you originally suggested. Perhaps because of this being a k40 without the analog pot, but with a digital display? After trying the PWM cable from the board to the PSU we got it firing. But I think we might still have some issues with the power.)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:01*

Wait. I'm confused. You have to pick one thing to pwm (send laser pulse to). I recommend keeping the digital display. Use it to set the maximum power. Keep the PWM pin as 2.5. Then power you set in LW will be a proportion of the full power. 

Remove the included white PWM from the mix completely. 

This approach (keep the digital display) has worked better for the other people with it. 


---
**Andreas Benestad** *February 13, 2017 13:07*

Ok. Keeping the digital display would be sweet.

But then I guess the question for us is: should we insert a cable into the PWM input on the Controller board at all? If we need to connect something to this input, the only option for us seems to be using the white cable from the bundle.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:10*

No, you should:

Remove the included white PWM cable completely. 


---
**Andreas Benestad** *February 13, 2017 13:12*

Ok. We have removed the PMW cable as you suggest. The digital display is back on, but we are back to the issue of having no power. The laser does not fire whatsoever.


---
**Andreas Benestad** *February 13, 2017 13:16*

Picture of the board and PSU. No pmw connected to the board. And original setup from the digital display to the PSU. 

This is giving us no laser power whatsoever. 

![images/224eaafd9a9f285c29c2eca405a2d90e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/224eaafd9a9f285c29c2eca405a2d90e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:18*

What power is set on the panel? Can you fire using the physical test button on the machine? 


---
**Andreas Benestad** *February 13, 2017 13:20*

Yes. The physical test button on the machine works now. The power is set to 70% on the panel.

Nothing comes from the software.


---
**Andreas Benestad** *February 13, 2017 13:20*

This is the digital display of the k40.

![images/c821c9192a9e6f5c61206717196db94d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c821c9192a9e6f5c61206717196db94d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:22*

"Nothing comes from the software" - how are you trying to fire the laser? The test fire button in LW does not work for many. Try running a line such as G1 X10 S0.5 F600


---
**Andreas Benestad** *February 13, 2017 13:24*

When running that command the laser head moves, but the laser does not fire. Same with when running image/design files. The laser moves around, but no fire. 

All of this "worked" when using the white pmw cable, despite having some issues with the power levels.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:24*

And you've changed the laser pin back to 2.5 

Without the exclamation mark in config? 


---
**Andreas Benestad** *February 13, 2017 13:25*

Whenever we make changes to the config, do we have to restart/reset/do something?


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:27*

Yes. You make the change, save the text file, close it. Then safely eject the drive that shows up in computer. 

And MOST IMPORTANTLY you need to reset the board. You can do this using the little yellow button in the top right concern. 


---
**Andreas Benestad** *February 13, 2017 13:35*

ah... ok. that worked. Your last piece of advice was spot on. (I suspect more ignorant people like me might be making the same mistake, changing things around and not seeing any immediate results, thinking the change did not work...so yeah, I would strongly reccommend to stressing this point when helping others. :-) )



So we now have both the digital display and the laser is firing.

Now I only need to figure out the settings of laserweb in regards to percentage of power. From what you said, the digital display will now be my maximum power. How does that relate to the "power constraints" in LS? And also in regards to the S-override in the JOG-panel..?


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 13:40*

You can do something like set the digital display to 10mA. This is a safe value that should keep the beam from firing close to full power. 

Then use the power levels when setting up an LW job to set a percentage of that based on your material/ thickness/ results. 

In K40 groups you can find people sharing material currents and speeds, etc...

The S override is... Later...


---
*Imported from [Google+](https://plus.google.com/112581340387138156202/posts/eSNJCmN1aQZ) &mdash; content and formatting may not be reliable*
