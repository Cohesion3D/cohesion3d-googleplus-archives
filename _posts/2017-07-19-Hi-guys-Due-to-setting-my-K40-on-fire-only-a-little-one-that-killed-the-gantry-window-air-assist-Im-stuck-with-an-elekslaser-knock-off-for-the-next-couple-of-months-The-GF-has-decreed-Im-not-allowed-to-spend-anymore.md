---
layout: post
title: "Hi guys, Due to setting my K40 on fire (only a little one that killed the gantry/window/air assist), I'm stuck with an elekslaser knock off for the next couple of months (The GF has decreed I'm not allowed to spend anymore"
date: July 19, 2017 05:24
category: "K40 and other Lasers"
author: "Craig \u201cInsane Cheese\u201d Antos"
---
Hi guys,



Due to setting my K40 on fire (only a little one that killed the gantry/window/air assist), I'm stuck with an elekslaser knock off for the next couple of months (The GF has decreed I'm not allowed to spend anymore hobby money till we get back from holidays in October) 



I have a working Cohesion3d Mini that was powering my K40, anybody know if/how to wire it up? It's a 2 wire 3W 445nm laser module. It's always annoyed me about this one that there's no homing/origin so it always a guess where things will end up. I figure being big laserless for a couple of months, I'd see if I can swap the Cohesion3d Mini over and enjoy the benefits.





**"Craig \u201cInsane Cheese\u201d Antos"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2017 12:40*

Power both power input terminals on the Mini and hook up the diode to the 2.5 terminal + and -  

Motors and endstop connections are all available. Check the pinout diagram. 


---
**Craig “Insane Cheese” Antos** *July 19, 2017 12:59*

2.5 is the screw terminals marked as FET1/Bed if I'm reading the pinout correctly? And that's the same terminals it normally uses for Laser fire? 



Motors and Endstops are all easy enough to figure out. Just getting the laser to do it's thing had me stumped.



I'll see how I go hooking it all up tomorrow and see how it goes. 


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2017 13:02*

Yes.  Power In to Main at the top left and for the mosfet power at the bottom pins 1 and 2 of the screw terminals. 

Then + of the diode to terminal 3 and - to terminal 4. 



Take a pic so I can confirm if any questions at all. 


---
*Imported from [Google+](https://plus.google.com/114851766323577673740/posts/4jdgMxsV2ZY) &mdash; content and formatting may not be reliable*
