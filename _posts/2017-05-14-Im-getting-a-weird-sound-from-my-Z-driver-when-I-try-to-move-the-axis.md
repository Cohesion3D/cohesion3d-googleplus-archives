---
layout: post
title: "I'm getting a weird sound from my Z-driver when I try to move the axis"
date: May 14, 2017 23:46
category: "C3D Remix Support"
author: "Lukas M\u00fcller"
---
I'm getting a weird sound from my Z-driver when I try to move the axis. It beeps as long as it trys to spin the stepper. 

The axis moves very freely so I'm not sure what causes this 🤔


**Video content missing for image https://lh3.googleusercontent.com/-j0WxhihMlPU/WRjsbhRG-HI/AAAAAAAACCs/gIF3CwuY3lkFRqqHK0Wef45GSPjCeAxiQCJoC/s0/VID_20170512_151122%25257E2.mp4**
![images/b8676055ee414428b8f2e4c977fb2459.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b8676055ee414428b8f2e4c977fb2459.jpeg)



**"Lukas M\u00fcller"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 23:48*

Not enough current on the driver? Steps per mm set correctly?  Trying to move at too fast of a speed? 


---
**Griffin Paquette** *May 15, 2017 02:05*

Is the axis actually moving? Or is it skipping steps?


---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 02:07*

If you look closely you can see a different set of headers on the Z axis output.  Wondering if something got screwed up during that transition.  I know when Rene did this same thing he pulled up some pads. 


---
**Lukas Müller** *May 15, 2017 10:26*

They should have enough current and I've set the steps per mm to 1600 for a 2mm pitch spindle. But the header thing could be the issue. I'll test that ... Thanks for the fast help ;)


---
**Alex Krause** *May 15, 2017 15:02*

Are the poles of the stepper wired correctly with the corresponding pairs?


---
**Lukas Müller** *May 15, 2017 18:45*

Yes they are


---
**Lukas Müller** *May 16, 2017 13:36*

So now this ... 

I've tried different drivers and different pins on the board to drive them. 

This only works if I set the speed to 5mm/s with low acceleration. 

![images/85c752579b970af4552eb072a51892a4](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/85c752579b970af4552eb072a51892a4)


---
**Lukas Müller** *May 16, 2017 13:39*

Here is my config if that helps ... 

(I changed the gamma pins to try different drivers and headers on the board )



[dropbox.com - config.txt](https://www.dropbox.com/s/84m4o645mwg38xi/config.txt?dl=0)


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 03:52*

Way to debug issue:

Use stock config file from smoothieware github. Lower speeds (gamma max rate and z speed) and acceleration values. 

Confirm steps per mm (already discussed). 

The motor spins! So either you're trying to go too fast or you don't have enough current. That seems to be the main trade off out there. 


---
**Lukas Müller** *May 17, 2017 21:06*

Ok ... so stock config file did the trick. Thanks alot :)

But now I'm wondering whats wrong with my config.

I did notice that you did include the current setting even though this cant be controlled on your board ...


---
**Ray Kholodovsky (Cohesion3D)** *May 17, 2017 21:07*

Again, it's just the stock smoothieboard config file.  Keeps things simple. 


---
**Lukas Müller** *May 18, 2017 15:26*

Ok ... thanks for the help ;)


---
*Imported from [Google+](https://plus.google.com/101462068007380494279/posts/gboqE897car) &mdash; content and formatting may not be reliable*
