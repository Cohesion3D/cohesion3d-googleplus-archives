---
layout: post
title: "Hi Ray, I just ordered the mini laser upgrade bundle from your site"
date: December 09, 2016 12:59
category: "General Discussion"
author: "Naveen Chandra Dudarecha"
---
Hi Ray, I just ordered the mini laser upgrade bundle from your site. I am a complete newbie to lasers, tried k40 and lost hope of using it again with its board and software. 

Could you point me to any video or a guide to help install the your upgrade bundle?



Cheers

Naveen





**"Naveen Chandra Dudarecha"**

---
---
**Griffin Paquette** *December 09, 2016 14:29*

Figured I'd throw this on here:[https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0](https://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0)[dropbox.com - C3D Installation.pdf](http://www.dropbox.com/s/ngky9g3yh2gpa6r/C3D%20Installation.pdf?dl=0)


---
**Steve Anken** *December 09, 2016 15:54*

Ray should pin this dropbox link to the top of the group.


---
**Steve Anken** *December 09, 2016 15:56*

Add a link to Peter's current LaswerWeb3 install.


---
**Naveen Chandra Dudarecha** *December 09, 2016 17:13*

Thanks +Griffin 😊 i'll start digging it over the weekend


---
**Griffin Paquette** *December 09, 2016 17:50*

No prob! Enjoy the board!


---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 20:46*

Hey!  These guys beat me to it. Thanks for your order. 

Notes: the production board is going to look slightly different than the current customers' handbuilt ones, and will have a different config file, so I'm not going to publish that just yet. 

We're also having pinout diagrams and CAD Models made, much like for the ReMix board you can see here: [cohesion3d.freshdesk.com - Support : Cohesion3D](http://cohesion3d.freshdesk.com)

Hopefully I will have all the Mini and Laser documentation up on that link as well by the time we are shipping.  I wanted to make some changes to that PDF to account for the different k40 builds out there, but the idea there is solid. 


---
**Naveen Chandra Dudarecha** *December 09, 2016 21:32*

Cheers. 


---
*Imported from [Google+](https://plus.google.com/108859395222728856894/posts/3YNeAVSC2Rn) &mdash; content and formatting may not be reliable*
