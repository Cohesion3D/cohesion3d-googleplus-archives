---
layout: post
title: "So, uh, slight change of plans. All the Mini's have now shipped"
date: January 13, 2017 22:25
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
So, uh, slight change of plans. 

<b>All</b> the Mini's have now shipped. 



It looks like some will be arriving in the USA as early as Saturday, with a lot more coming in around Tuesday.   And a bit longer if you're located internationally :)



I will get the initial documentation up this evening on our support site: [cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com)



I'm going to be away from my computer for most of the weekend (more 14 hour days volunteering at robotics competitions, yay!) - 



If you see anyone that needs help at the various groups you all frequent, please send them over here.  And if at all possible, let's make one question thread for me to answer things more efficiently.  I'll get to it as soon as I can. 



Thanks to everyone who helped bring this project to fruition.  It's going to be great. 



Cheers,

Ray





**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/5YMovcXeEsH) &mdash; content and formatting may not be reliable*
