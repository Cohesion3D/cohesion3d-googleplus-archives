---
layout: post
title: "Hi, I've just swapped out my M2nano board for a cohesion3d board on a chinese laser (bed size is 600mm x 400mm)"
date: May 23, 2018 00:51
category: "C3D Mini Support"
author: "James B"
---
Hi,

I've just swapped out my M2nano board for a cohesion3d board on a chinese laser (bed size is 600mm x 400mm). The board powers on with all the correct lights, and it homes correctly to the top left. The origin is set to the bottom left. When moving the laser in LightBurn (Smoothie), the laser moves in the correct orientation which makes me think my stepper cables are installed correctly. I am also using 'absolute positioning' in LightBurn.



For some reason the laser only uses the top half (in the y-direction) of the bed. The x-axis has the full limits and works fine. For a test, I drew a square on the bottom of my plot (see attached images), but the machine thinks the bottom is at the midpoint of the y-axis.  I've checked the measurements of the square and it is drawing the dimensions to approximately the right size so it doesn't seem to be a scaling issue. Does anyone know why the full laser bed isn't being recognized? I haven't changed anything in the config file, is there something in there that can be adjusted? 



One modification I needed to do for wiring was to splice the grounding wires for the endstops together. The ground from each endstop was soldered to the single white grounding wire coming off the board. I don't think this would have any effect on the issue but mention it here just in case.



thanks,



James



![images/3d1039642540836b670c605f7db109e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d1039642540836b670c605f7db109e7.jpeg)
![images/2fc1dfa33effa95d719b05cf5c28b375.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2fc1dfa33effa95d719b05cf5c28b375.jpeg)
![images/c3732d118351c7a22a04f63d82894269.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c3732d118351c7a22a04f63d82894269.jpeg)
![images/b4c57318efc32198220a74ff6480912c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b4c57318efc32198220a74ff6480912c.jpeg)
![images/91fbc11bbaffdd556e0749d9808b5fb8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/91fbc11bbaffdd556e0749d9808b5fb8.jpeg)

**"James B"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 23, 2018 01:50*

A few things:



1.  The origin is the bottom left.  Origin not same thing as homing switch location. 

2. You need to change the values in the config file for your bed size.  X is alpha min and Y is beta max.  Then you put those same values into Lightburn device.

3. Your endstop connector appears to contain 5 wires.  This is unexpected, usually only the center 3 wires are in use with the grounds of both switches spliced into one pin.  Sometimes they are not and the bottom pin is in use.  I have never seen the top pin connected.  Please trace this and provide more information about where these wires are going and what the switches look like. 


---
**James B** *May 23, 2018 02:02*

**+Ray Kholodovsky** Thanks for the quick response. I changed the beta max in the config file and it fixed the problem. For some reason the x-axis has full range of motion without changing the alpha min so I left it [unchanged.An](http://unchanged.An) easy fix!



I had already addressed the endstop grounding issue with your diagram from [https://plus.google.com/+RayKholodovsky/posts/1sBj7vYtxgq](https://plus.google.com/+RayKholodovsky/posts/1sBj7vYtxgq). I simply spliced the two endstop ground wires into the single ground on the endstop connector. Thanks again for your help!



[plus.google.com - I made this diagram for a k40 upgrade customer. On the not-well-known HT-Mas...](https://plus.google.com/+RayKholodovsky/posts/1sBj7vYtxgq)


---
**Ray Kholodovsky (Cohesion3D)** *May 23, 2018 02:05*

I am very curious though what the 5th wire (top one) is.  Can you walk me through it?  


---
**James B** *May 23, 2018 02:17*

**+Ray Kholodovsky** Yeah, the fifth wire arrived disconnected. They other four wires on the harness were spliced to the appropriate wires for the endstops. My guess is that the manufacture just had access to this connector and didn't bother removing the fifth wire. They just taped it up so it was harmless and out of the way.


---
**Ray Kholodovsky (Cohesion3D)** *May 23, 2018 02:20*

Oh ok.  So you needed to do just the bottom one as shown in my post/ diagram.  On the next batch of boards currently in production I have made some small tweaks, including the bottom pin is now connected to gnd for this case. 

However, I also connected the top pin to 5v, as it is on the M2Nano.  For a moment I was worried that this was also going to Gnd on your machine.  Whew! Thanks.


---
*Imported from [Google+](https://plus.google.com/102670693678622244646/posts/QVoRK6qZQnE) &mdash; content and formatting may not be reliable*
