---
layout: post
title: "Just got the 3dmini board installed and the lcd just constantly beeps"
date: July 14, 2018 05:06
category: "C3D Mini Support"
author: "Kevin Birka"
---
Just got the 3dmini board installed and the lcd just constantly beeps. Everything else appears to be working. I can control the laser but cant stop the beeping





**"Kevin Birka"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 14, 2018 12:35*

Try this: 



In the board's config.txt file on the memory card, there is a line:



panel.buzz_pin                              0.27o



change this to 



panel.buzz_pin                              1.31



then save, safely eject the card, and power up the board.  I do not recommend taking out/ putting the card in while the board is powered.


---
**Kevin Birka** *July 15, 2018 14:52*

**+Ray Kholodovsky** that worked perfectly. Thank you 


---
*Imported from [Google+](https://plus.google.com/115352307708077490690/posts/13c2RuqfMe8) &mdash; content and formatting may not be reliable*
