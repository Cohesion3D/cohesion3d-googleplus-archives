---
layout: post
title: "I know I have been inactive on here for quite some time"
date: October 01, 2017 04:30
category: "3D Printers"
author: "Jonathan Davis (Leo Lion)"
---
I know I have been inactive on here for quite some time. But I am considering the prospect of building a custom 3D printer with a large print area to produce large objects such as costume head bases. With that said I do not know where to begin and nor do I know what parts I will need? So a bit of help from those who have worked and designed such machines would be nice and slection of tools are quite limited and i am wanting to keep it affordable. 





**"Jonathan Davis (Leo Lion)"**

---
---
**Dushyant Ahuja** *October 01, 2017 11:37*

[thegreatfredini.com - Maker Profile: The Maestro of 3D Printer Building– Jetguy!](https://thegreatfredini.com/2015/05/02/the-maestro-of-3d-printer-building-jetguy)/

Maker Profile: The Maestro of 3D Printer Building– Jetguy! | The ...




---
**Dushyant Ahuja** *October 01, 2017 18:44*

[flickr.com - Latest enclosed Core XY](https://www.flickr.com/photos/90025904@N04/albums/72157665872504001)


---
**Jonathan Davis (Leo Lion)** *October 01, 2017 18:54*

**+Dushyant Ahuja** I do not know how this information will be of help to me


---
**Dushyant Ahuja** *October 01, 2017 21:01*

**+Jonathan Davis** the links provide photo details of large format 3D printers. They're designed by "jetguy" - who has designed and built very large printers. If that doesn't help, here's another link:

[openbuilds.com - 3D PRINTER BUILDS &#x7c; OpenBuilds](http://openbuilds.com/?category=3d-printer-builds&id=272)



You can also search on [thingiverse.com](http://thingiverse.com)




---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/SwCxAeYix8E) &mdash; content and formatting may not be reliable*
