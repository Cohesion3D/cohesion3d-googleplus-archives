---
layout: post
title: "Hi Sir. I have a K40 laser machine"
date: March 17, 2018 20:37
category: "General Discussion"
author: "Alejandro Camarena"
---
Hi Sir.

I have a K40 laser machine. 

I need to connect a led indicator on the power laser switch and another on test switch.

How can I do?

Thanks





**"Alejandro Camarena"**

---
---
**Marc Miller** *March 22, 2018 18:18*

Can you give a bit more info?  Are you wanting to have the board turn on/off an indicator light based on the state of a switch?


---
*Imported from [Google+](https://plus.google.com/105246612865495647235/posts/5zckx3tFMei) &mdash; content and formatting may not be reliable*
