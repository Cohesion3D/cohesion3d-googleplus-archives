---
layout: post
title: "Have some questions concerning my new mini"
date: March 25, 2018 16:06
category: "C3D Mini Support"
author: "Jamie Richards"
---
Have some questions concerning my new mini.  It has never beeped at all, but otherwise seems to be working since it goes through the light sequence, plus I can engrave and cut using LightBurn.  Is there a setting for this feature or could the beeper be defective?  Also having some issues with power levels.  I tested it by manually setting it to 10ma, then set it to 3/4 power using the controller, but it pretty much stayed at 10, and when I put it at half, it goes to like 3/4 etc.  At low power, it was doing 5ma, so not sure what's going on there.  The tube also squeals a lot now when energized below 15ma.  My steppers make noise when idle, but I guess that's fairly common and nothing to worry about unless they get hot.  Using the stock supply, so could this be part of the problem?  I also need to do fast raster, so I guess I have to switch to grbl?  Thanks for any info you can give. :)





**"Jamie Richards"**

---
---
**Joe Alexander** *March 25, 2018 17:36*

try tweaking the power intensity, I find that my tube tends to squeal at various intensity levels. IE if I set to 15ma max and run a job 0-70% power or 0-80%, one may squeal and one may not.


---
**Jamie Richards** *March 25, 2018 17:59*

Thanks, I'll try that.  I also plan on doing a lot of grayscale stuff once I figure out how to make it change power correctly. 


---
**Ray Kholodovsky (Cohesion3D)** *March 25, 2018 21:20*

Pwm tuning guide in the FAQ article on the documentation guide 


---
**Jamie Richards** *March 26, 2018 06:17*

Thank you, I somehow missed it.  Set it to 400 and the power variance is a little wider now.


---
**Jamie Richards** *March 28, 2018 08:14*

Actually, a lot wider now.  Love it!


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/WBzZmh3c8s2) &mdash; content and formatting may not be reliable*
