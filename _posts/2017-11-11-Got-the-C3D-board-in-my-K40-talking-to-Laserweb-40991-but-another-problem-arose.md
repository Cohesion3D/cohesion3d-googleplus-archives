---
layout: post
title: "Got the C3D board in my K40 talking to Laserweb 4.0.991, but another problem arose"
date: November 11, 2017 19:58
category: "C3D Mini Support"
author: "Arjen Jonkhart"
---
Got the C3D board in my K40 talking to Laserweb 4.0.991, but another problem arose.



When I use the manual control options in LW, the X-axis responds appropriately, however, when I press Y+10mm it goes the same direction as Y-10mm. . . 



When I reverse the Y cable on the board, that particular "same direction" just reverses, so the still both go the same direction.



I also installed Pronterface, and the same problem presents itself. So, it is either the board, the wiring to the board (not changed), or the config file.



Any ideas?





**"Arjen Jonkhart"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 20:01*

I have been trying to figure this one out...  I test each board with the drivers but it's possible I missed something. 



Can you swap the X and Y drivers with each other and try again? 


---
**Arjen Jonkhart** *November 11, 2017 20:05*

I swapped the drivers, and the problem did not . Still the Y-axis


---
**Arjen Jonkhart** *November 11, 2017 20:06*

Remember, this is the same board as that has the bad SD card, which we figured out yesterday.


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 20:08*

Sorry about this. 

I'm going to send you a new bundle with everything. 


---
**Arjen Jonkhart** *November 11, 2017 20:11*

Thank you. When you are busy, sometimes things  slip through the cracks. Not a problem, and looking forward to this working.


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 20:14*

Knock on wood, we haven't had any such issues in a while.  Everything just kind of happened at once with your order.  Gremlins maybe? 

Thanks for your understanding. 


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 20:19*

By the way, this doesn't have to stop you.  You can just put the Y driver and motor into the Z socket , and then remap those pin numbers in the config.txt file.



Should look like this.   Gamma (z) still needs to be defined though, and you don't want the pins to overlap so I'd swap the beta (y) values back in to gamma. 



Said differently, swap beta and gamma step, dir, and en pin numbers with each other.  Keep the ! with beta dir. 



beta_step_pin                                2.2              # Pin for beta stepper step signal

beta_dir_pin                                 0.20!             # Pin for beta stepper direction

beta_en_pin                                  0.19             # Pin for beta enable






---
**Anthony Bolgar** *November 11, 2017 20:20*

Glad its not me for a change :)




---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 22:07*

**+Arjen Jonkhart** I shipped your replacement today. 


---
**Arjen Jonkhart** *November 13, 2017 23:24*

Great service - thank you. I had already put the old board back in, so I did not test the Z socket suggestion.


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 23:25*

Understood. Please keep me posted. 


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2017 02:53*

Friendly check in to see how everything is going.


---
*Imported from [Google+](https://plus.google.com/109364066707746072621/posts/aPPwz6DZNRW) &mdash; content and formatting may not be reliable*
