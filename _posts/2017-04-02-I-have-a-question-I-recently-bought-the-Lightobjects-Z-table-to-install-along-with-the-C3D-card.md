---
layout: post
title: "I have a question. I recently bought the Lightobjects Z-table to install along with the C3D card"
date: April 02, 2017 16:24
category: "General Discussion"
author: "Richard Pike"
---
I have a question.  I recently bought the Lightobjects Z-table to install along with the C3D card. The Z table came with the connector, but it was one sided, with the other being bare wires.   



I have bought the needed connectors to rectify this, but I can not find a good picture or instructions on which way it should be wired together.   Either straight pass through, or twisted before it connects to the board.



Any help would be appreciated.



![images/4b2fc805bc9c2021b44e1fbecbad7f88.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b2fc805bc9c2021b44e1fbecbad7f88.jpeg)
![images/1caeaae224e7f9d06564d2c393dc6a28.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1caeaae224e7f9d06564d2c393dc6a28.jpeg)

**"Richard Pike"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 02, 2017 16:34*

The quick answer is that they go by pairs - and ideally you would use a multimeter to find the pairs of wires that have continuity between them. Then those go as per the group A1A2 and B1B2 on our pinout diagram for the Mini. (See documentation site). 




---
*Imported from [Google+](https://plus.google.com/113450419015576024390/posts/dAEM3YuNxxP) &mdash; content and formatting may not be reliable*
