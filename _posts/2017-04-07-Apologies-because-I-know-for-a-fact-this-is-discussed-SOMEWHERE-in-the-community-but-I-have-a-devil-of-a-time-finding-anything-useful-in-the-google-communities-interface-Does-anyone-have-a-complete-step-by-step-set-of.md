---
layout: post
title: "Apologies because I know for a fact this is discussed SOMEWHERE in the community, but I have a devil of a time finding anything useful in the google communities interface :( Does anyone have a complete, step-by-step set of"
date: April 07, 2017 20:28
category: "K40 and other Lasers"
author: "Peter Grace"
---
Apologies because I know for a fact this is discussed SOMEWHERE in the community, but I have a devil of a time finding anything useful in the google communities interface :(



Does anyone have a complete, step-by-step set of instructions for moving the PWM output from the dedicated Mini PWM pins to the "other pins" --  I've seen some references to moving L to 2.5 and disconnecting the PWM wire, but when I do that the engrave comes out the same color and the mA needle meter bounces but mostly stays at the same current throughout the burn.



My machine shipped with a pot and an ammeter.  Here's pics of the wiring and of the burn.  The boxes are 1cm squares at S.05 - S.25 laser power at 500mm/s.  The ammeter showed approximately 6mA (bouncing) regardless of power setting.  I'm using LaserWeb4 to generate the g-code.  



The C3d Mini is powered via a dedicated 24V power supply, and the black box in the lower left corner is a TB6600 external stepper controller so that the beefier motor on the LightObjects Z-table gets enough juice.  The Z-table is working properly now, for what its worth.



![images/a03f1fdcc85a2f1f7ca716223e9fe729.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a03f1fdcc85a2f1f7ca716223e9fe729.jpeg)
![images/39d6524a3a43454829ceaece6eaffdf6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39d6524a3a43454829ceaece6eaffdf6.jpeg)

**"Peter Grace"**

---
---
**Joe Alexander** *April 07, 2017 20:37*

on your laser PSU the middle connector is unpopulated. I assume this is where the pot/pwm would normally be?(i have the G-G-G psu)


---
**Chris Harrison** *April 07, 2017 20:41*

I believe I have the same issue. Initially with the "K40 Power" connector I got no laser output at all when running a job, but did when clicking "Test Laser" in LaserWeb4. Upon connecting "L" on my high voltage power supply to -P2.5/BED and making the corresponding change in the config file (laser_module_pwm_pin changes from 2.4! to 2.5) I get laser output, but it doesn't seem to be variable; I did a test greyscale image using LaserWeb4 and it was completely black. My ammeter hasn't arrived yet unfortunately.


---
**Peter Grace** *April 07, 2017 20:41*

**+Joe** Yes, the three-pin header that is unpopulated towards the middle-right is where the pot plugs in, OR, where you plug the pwm cable that comes with the C3D Mini.  I was running with the PWM cable installed, but wasn't getting power variations, so I removed it after moving L to 2.5 to see if that worked better, but getting the same results -- an even coloring in the engrave.


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 20:49*

Gentlemen, hello!



Peter - first off, yay for people doing "weird stuff" with my boards.  Happy to see that.  My TB6600's just shipped from China, hopefully playtime in a few weeks. 



Don't run the PWM cable.  You want to put the pot back in play, and make sure the config file has 2.4! changed to 2.5  



See how we do it now in the new guide: [cohesion3d.freshdesk.com - K40 Upgrade with Cohesion3D Mini Instructions.  : Cohesion3D](https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)

And at the bottom of that is a link to the new config file 



Peter and Chris - if you're not getting grayscale or "ability to vary the laser power level from gCode/ Software" then you need to fiddle with the pwm period value in the config file.  The new default value is 200.  Some users have needed to go to 400 to see results. 


---
**Peter Grace** *April 07, 2017 21:13*

OK, so if the pot still needs to be used, what is its purpose?  What should I be setting the pot to?


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 21:15*

Start at 10mA.  It sets the current ceiling, then your % value in LW sets a proportion of that.


---
**Peter Grace** *April 07, 2017 21:44*

I have re-added the potentiometer and set it to 18mA when test firing. I burned a 10 square engrave pattern from 10% to 100% power, and the mA meter moved from 4mA to 5mA between those 10 blocks.  My config settings for laser are:



## Laser module configuration

laser_module_enable                           true

laser_module_pin                              2.5 

laser_module_maximum_power                    0.75

laser_module_minimum_power                    0.1 

#laser_module_default_power                   0.8 

laser_module_pwm_period                       400 






---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 21:45*

Set pot to 10mA. This is important. 

Then run G1 X10 S0.4 F600 lines (vary the X value - new target coordinate and S value - power level between 0 and 1, see what happens) 


---
**Peter Grace** *April 07, 2017 21:51*

trying now.


---
**Peter Grace** *April 07, 2017 22:14*

Here's the GCODE and the results: 

G21

G90

G0 X0 Z0

G0 X0 Y10

G1 X10 S0.1 F600

G0 X0 Y15

G1 X10 S0.2 F600

G0 X0 Y20

G1 X10 S0.3 F600

G0 X0 Y25

G1 X10 S0.4 F600

G0 X0 Y30

G1 X10 S0.5 F600

G0 X0 Y35

G1 X10 S0.6 F600

G0 X0 Y40

G1 X10 S0.7 F600

G0 X0 Y45

G1 X10 S0.8 F600

G0 X0 Y50

G1 X10 S0.9 F600

G0 X0 Y55

G1 X10 S1 F600

G0 X0 Y0 Z0

M2



![images/5fdacc263d3b2e41d41c28b13b4ac8f6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5fdacc263d3b2e41d41c28b13b4ac8f6.jpeg)


---
**Peter Grace** *April 07, 2017 22:17*

I think I was catching on to what you were trying to do, so I did the test at 10mA, then at 5mA, then at 3mA peak power.  5mA seems to be the "best" choice.  But how does this scale when I have a multi-step job and need to have full power for a cut operation?  It would be rather annoying to have to change the pot in between jobs; not sure if smoothie supports a tool change pause to give me time to do that or not, or if even LaserWeb would support that.


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 22:34*

Right, so this is where the "look at how good the laser power supply is" really comes out.  At higher pot settings it can tend to drown out the pwm from the board. 



I think you need to stay with 10mA on the pot, and try additional S values. For example 10% of 10mA is obviously more power than 10% of 3mA.  Try seeing how low of an S value you can go.  That is your intent here right?  You saw the lower limit of what came out with the pot at 3mA and now you want to achieve that via software with the pot set to 10mA?



Next step: comment out with a # the line laser_module_minimum_power                    0.1 

and repeat that last step. 



Next step is to possibly try an even higher pwm period value.  I don't think going to 600 or 800 will hurt anything, but no one's done it yet. 



It's quite a bit of trial and error, ahem, tuning, at this point in the process. 




---
**Joe Alexander** *April 07, 2017 22:44*

You can also try a range like 0-75% on engraves, I find it reduces the burnt parts by skipping the darkest spots in the artwork. Tweak accordingly.


---
**Chris Harrison** *April 07, 2017 22:46*

What's the recommended value for laser_module_pwm_period?



Also apologies if this seems like an attempt to hijack the thread; I have no idea how Google+ is supposed to work.


---
**Ray Kholodovsky (Cohesion3D)** *April 07, 2017 22:46*

200 comes default now, some users need to go to 400 if they don't see greyscale output at 200. 


---
*Imported from [Google+](https://plus.google.com/+PeterGrace2013/posts/dRNXVGXMKh8) &mdash; content and formatting may not be reliable*
