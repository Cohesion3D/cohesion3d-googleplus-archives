---
layout: post
title: "got the board install and tried doing the laser web through the cmd and can not get it to load"
date: March 13, 2017 00:55
category: "General Discussion"
author: "John Austin"
---
got the board install and tried doing the laser web through the cmd and can not get it to load.



Using the cmd is something i am not familiar with.

Download rd works and can not get it to see the laser.



When I turn the laser on it doesn't go to the home postion is that supposed to work like it did with the old board?









**"John Austin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2017 00:58*

Try LaserWeb4 installer: [github.com - LaserWeb/LaserWeb4-Binaries](https://github.com/LaserWeb/LaserWeb4-Binaries)



What operating system? 



It will not home on its own. 


---
**John Austin** *March 13, 2017 04:20*

Windows 7, think it is a 64

Ill try it next weekend. 


---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2017 04:21*

You'll need the smoothie drivers: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)

And then yeah use the appropriate (x64 as you say) build of LW4. 


---
**Bob Buechler** *March 13, 2017 15:26*

Will OSX users need the drivers as well?


---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2017 15:31*

Nope. Windows only, and not Windows 10. 


---
**Bob Buechler** *March 13, 2017 22:54*

Cool, thanks.


---
**John Austin** *March 13, 2017 22:55*

Just install this and it should see the laser? 


---
**John Austin** *March 16, 2017 17:46*

I may of missed it, but is there directions on configuring the laserweb 4 to see the laser?

have it in stalled but don't know what I need to do with the setting?


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2017 17:49*

Figure out if you need drivers ^^ (Windows but now Win 10).   Plug in board over USB.  <b>Then</b> open LW4. 


---
**John Austin** *March 16, 2017 17:56*

I installed driver and laser sits there I need to watch some video to see how to use it. Find a 2hr long one that I am going to try and watch. Got in there messing around what should the baud rate be? Using usb


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/EUEETAXECTY) &mdash; content and formatting may not be reliable*
