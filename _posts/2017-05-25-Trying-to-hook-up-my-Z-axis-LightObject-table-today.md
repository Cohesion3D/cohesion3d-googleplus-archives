---
layout: post
title: "Trying to hook up my Z axis (LightObject table) today"
date: May 25, 2017 19:42
category: "C3D Mini Support"
author: "Steve Clark"
---
Trying to hook up my Z axis (LightObject table) today. Everything “looks” right but obviously something is wrong because it is not moving.



In LW I turned on “Z Table” and in looking at the “Tool Offset” was not sure what to put in there so for a test I put in -50. And “0” for the Default Start Height”.



With all axis at zero settings, jogging works in X and Y but in Z although the readout shows it to have moved the stepper is not moving. No noise, no sound, nothing. I have to guess I have something in the wiring  wrong? (note the green wire is not a ground it was the only other color I had.)



I see this in the status window on LW at 1mm jog minus direction



jog(Z,-1,18000)

Work Offset: 0.00 / 0.00 / -0.00 / NaN

Work Offset: 0.00 / 0.00 / 0.00 / NaN



And this at 10mm jog minus direction.



jog(Z,-10,18000) 

Work Offset: 0.00 / 0.00 / -0.00 / NaN 

Work Offset: 0.00 / 0.00 / 0.00 / NaN 

Work Offset: 0.00 / 0.00 / -0.00 / NaN 

Work Offset: 0.00 / 0.00 / 0.00 / NaN



 The feed range warning threshold for the Z axis is at default…. Min 1 and Max 50000



?



![images/0d07e39a9c36b801e5d2167b02cd77db.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d07e39a9c36b801e5d2167b02cd77db.jpeg)



**"Steve Clark"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2017 19:45*

The driver is not powerful enough for the LO table. You need an external stepper driver and it can take signals from the board.


---
**Steve Clark** *May 25, 2017 21:44*

I should have realized that…



I got one. I also have a 24 volt power supply for it. The wiring from the C3D to the driver I’m not that clear on. I believe I understand the bottom connections in the picture, 24v power in and the stepper leads out. The top is where I need help the z axis controller coming in to the driver.

 

There was a push button with a single axis controller card hooked up to it and that is where the three and wires and the jumper came from. I did look the diagram on the C3D site…  but because of my lack of knowledge in this area I’m not fully understanding and don’t want to mess things up. 



![images/314b05423774daaedbfe51e1c6e9320f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/314b05423774daaedbfe51e1c6e9320f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2017 00:32*

This is covered multiple times in this group, please use the search function. Also see the smoothieware documentation. 




---
**Steve Clark** *May 26, 2017 01:44*

The search function here is not working and I have no clue  why... I looked at smoothiware documentation before I posted.  Some leads would be helpful...


---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2017 01:52*

[https://plus.google.com/communities/116261877707124667493/s/external%20driver](https://plus.google.com/communities/116261877707124667493/s/external%20driver)



[https://plus.google.com/101382109936100724325/posts/TcTqr4QhQ4L](https://plus.google.com/101382109936100724325/posts/TcTqr4QhQ4L)



[http://smoothieware.org/general-appendixes#external-stepper-drivers](http://smoothieware.org/general-appendixes#external-stepper-drivers)



You want what the smoothie link calls common anode.  



That means you tie Pul+, Dir+, and En+ to +5v (there is a header in the lower left of c3d mini with +5v as one pin, see labeling and pinout diagram of board, you will need to make a crimp pin wire for it)



Then you wire the Pul, Dir, and En - to Step, Dir, and EN on the Mini (via my external stepper adapter if you have it).  



Finally you probably need to add !o to the end of the Z pin definitions in config, save, dismount/ eject in Computer, and reset the board.  As well as setting steps per mm and any other relevant figures.


---
**Steve Clark** *May 30, 2017 16:18*

**+Ray Kholodovsky**  Thank you for your support. If I had been able to do the searches it would have saved bothering you.



I will have to back off on adding the z table control for the next few weeks because of other projects going on and I’ll need to read up on a few things for this one so I fully understand your instructions. 



BTW- I thought I send this reply last week...apparently I didn't hit the post button.




---
**Ray Kholodovsky (Cohesion3D)** *May 30, 2017 16:20*

All good man.  I'm happy to help, I just want people to learn to fish for themselves before I do it for them.  You learn more that way.  But never feel afraid to ask anything.  


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/X8Xrq1T5JvR) &mdash; content and formatting may not be reliable*
