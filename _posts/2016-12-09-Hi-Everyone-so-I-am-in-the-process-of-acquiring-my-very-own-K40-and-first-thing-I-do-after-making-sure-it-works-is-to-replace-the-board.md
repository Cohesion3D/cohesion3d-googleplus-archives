---
layout: post
title: "Hi Everyone, so I am in the process of acquiring my very own K40 and first thing I do after making sure it works is to replace the board"
date: December 09, 2016 00:09
category: "General Discussion"
author: "Jason Hamilton"
---
Hi Everyone, so I am in the process of acquiring my very own K40 and first thing I do after making sure it works is to replace the board.   I was originally going to order a smoothieboard but I came across this great looking cohesion3d board that looks to make the install process easier!  

So my question is can I go ahead and order the board without seeing what various K40 version I receive or should I wait?







**"Jason Hamilton"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 00:12*

Hey **+Jason Hamilton**

Thanks for the kind words.  We've made it work on just about every K40 we've seen across the various google and facebook user groups. There are some outliers - so you may have to drill a few mounting holes or run a few extra wires, but so far that's the "worst" I've seen. At its core the mini can drive motors and mosfets and provide a PWM output signal, that's what matters. 

Any other questions let me know. 

Cheers, 

Ray


---
**Jason Hamilton** *December 09, 2016 00:26*

Thanks Ray!  I can't wait to join the club and I am putting in my order in shortly!


---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 00:32*

Awesome Jason!  You can check around this group for the latest update from me regarding the manufacturing.

On a side note, how did you come across Cohesion3D?  I need to get the word out more and I'd love to hear how people are finding us now. 

Thanks!


---
**Jason Hamilton** *December 09, 2016 00:42*

I actually stumbled across your cohesion3d board in the K40 community.   I saw some recent posts mentioning your board then I found this community and subsequently your web store.   So mostly it is due to my researching the k40 to see if that's what I really wanted to buy.  The fact that there is a seemingly good community around the K40 and you seemed to be really involved so that pushed me to purchase your board and the K40.


---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 00:44*

Awesome, thanks. I like to be of help. Karma takes care of the rest :) 


---
**Kelly S** *December 10, 2016 05:39*

I can vouch for **+Ray Kholodovsky** helped me get mine very quickly and answered any question I had to the best of his abilities in a very timely manor. 


---
*Imported from [Google+](https://plus.google.com/106830550392764032937/posts/fNLqSP75pCe) &mdash; content and formatting may not be reliable*
