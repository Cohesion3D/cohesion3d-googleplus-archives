---
layout: post
title: "What's the pinout on the external stepper driver connector?"
date: November 29, 2016 22:51
category: "General Discussion"
author: "James Newton"
---
What's the pinout on the external stepper driver connector?





**"James Newton"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 22:57*

Here's what they look like installed on remix. It plugs into Pololu socket and breaks out STEP DIR EN and GND. ![images/4d1116f430c42d943319cd56f7957065.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4d1116f430c42d943319cd56f7957065.jpeg)


---
**James Newton** *November 29, 2016 23:04*

Yes, the Pololu socket allows my adapter cables for my drivers:

[ecomorder.com - 6064A: Adapter cable Pololu massmind](http://www.ecomorder.com/techref/ecomprice.asp?p=416076)



But it /looks/ like there is a 2x5 header in front of each one that might carry the step, dir, en, logic, gnd, etc... signals, and I was wondering if it happened to be PMinMO standard so the adapter wouldn't even be needed.


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 23:44*

Oh I see what you mean, those are actually for the Graphic LCD attachment. 


---
**James Newton** *November 29, 2016 23:45*

Ok, so Pololu standard it is, with the adapter cables. Where can we actually buy this thing, see the prices, specs, etc...?




---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 23:47*

Cohesion3d.com 

We've been up since thanksgiving day :) 

Just to clarify that the adapter is the pcb you see in that pic, not any cable. You would run wires from the screw terminal to your external drivers. [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3d.com)


---
**James Newton** *November 29, 2016 23:52*

Your adapter is. Yes. A person could purchase your adapter, and my driver (which the terminal block option on my driver) and then strip and run and screw 3 or 4 wires between the two for each axis. MY adapter uses a 2x5 header which matches the 2x5 header on my drivers, and comes with the cable that goes between the two. Then your motion controller can move big things... like 3D printers that print houses out of concrete (and no, that wasn't a hypothetical). Or even bigger with my PID controller and DC motor driver which uses the same cable. 

[http://techref.massmind.org/techref/io/stepper/THB6064/gallery.htm](http://techref.massmind.org/techref/io/stepper/THB6064/gallery.htm)


---
**Ray Kholodovsky (Cohesion3D)** *November 29, 2016 23:53*

Makes sense, just wanted to be sure. 


---
**Jim Fong** *January 05, 2017 02:14*

**+James Newton** hey James, I ordered a cohesion board with the socket adapters today. I'll do the usual hardware motor/driver testing when I receive it.  I'll be sure to test your 6064 stepper and PIC servo board with it.  I need to make sure there will be no issues with my Parker/Copley servo motors that I plan to use for my laser upgrade. 



JimF. 



I've been kinda busy so haven't had a chance to test your h-bridge boards you sent me.  Holiday season is always the busiest at work. 


---
**Ray Kholodovsky (Cohesion3D)** *January 05, 2017 02:15*

Welcome Jim. Take lots of pics :) 


---
**Jim Fong** *January 05, 2017 02:34*

**+Ray Kholodovsky** sure will.  i have dozens of different stepper and servo drivers/motors at my disposal.  Applied Motion, Geckodrives, Parker compumotor, Copley, Advanced Motion Etc... 


---
*Imported from [Google+](https://plus.google.com/+JamesNewton/posts/27UQ1k7E2rP) &mdash; content and formatting may not be reliable*
