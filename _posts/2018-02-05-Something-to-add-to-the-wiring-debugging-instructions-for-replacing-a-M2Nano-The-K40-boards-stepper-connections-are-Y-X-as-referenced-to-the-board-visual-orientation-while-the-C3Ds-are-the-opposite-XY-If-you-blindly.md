---
layout: post
title: "Something to add to the wiring/debugging instructions for replacing a M2Nano The K40 board's stepper connections are \"Y, X\" as referenced to the board visual orientation while the C3D's are the opposite, \"X,Y\" If you blindly"
date: February 05, 2018 05:17
category: "K40 and other Lasers"
author: "John Plocher"
---
Something to add to the wiring/debugging instructions for replacing a M2Nano



The K40 board's stepper connections are "Y, X" as referenced to the board visual orientation while the C3D's are the opposite, "X,Y"



If you blindly install them the way they were on the Nano, the machine will grind against the stops when HOMEing because the axis/home switch relationship is broken.  That is, the C3d thinks it is moving the X stepper, but the Y is the one that moves.  It moves until the <i>X</i> home switch is pressed, which won't happen when it gets to the end of travel; same for the other axis. 



The FIX is to check and swap the connectors going to the X and Y steppers on the C3Dmini board.



Note that, on both the Nano and the C3Dmini, the color codes on the X and Y cables are mirrors of each other.  Mine are  YWBR  and   RBWY as viewed from the top of the machine.  If you flip one over to make them match, that axis will move in the wrong direction....







**"John Plocher"**

---
---
**Joe Alexander** *February 05, 2018 06:44*

one can change the direction of a motor in the config file by inverting the signal, details at [www.smoothieware.org](http://www.smoothieware.org). wire colors vary from manufacturer to manufacturer which makes a universal guide difficult.

[smoothieware.org - start [Smoothieware]](http://www.smoothieware.org)


---
*Imported from [Google+](https://plus.google.com/+JohnPlocher/posts/ic3PoRssW9U) &mdash; content and formatting may not be reliable*
