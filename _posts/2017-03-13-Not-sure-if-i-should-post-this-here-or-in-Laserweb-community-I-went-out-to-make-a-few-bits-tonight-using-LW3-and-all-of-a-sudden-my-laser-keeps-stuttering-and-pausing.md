---
layout: post
title: "Not sure if i should post this here or in Laserweb community, I went out to make a few bits tonight using LW3 and all of a sudden my laser keeps stuttering and pausing"
date: March 13, 2017 21:11
category: "Laser Web"
author: "Andy Shilling"
---
Not sure if i should post this here or in Laserweb community,

I went out to make a few bits tonight using LW3 and all of a sudden my laser keeps stuttering and pausing. Video attached. It started off ok but then while running at 40mms it had the issues. I haven't done anything out of the ordinary and the file I loaded was a 300ppi  250x180 image in jpeg format. As the video starts the laser has stopped at this point although it should still be running and you see if I then pause it on the pc and restart it continues.



I have included the file if anybody wants to have a look. I did notice LW3 seemed to be running slow tonight on my win10 PC but I'm not sure this would cause the problem.



[https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU](https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)





**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2017 21:16*

Try saving the gCode from LW and running from the MicroSD card.  I would also try a known good file (if you have old gCodes saved this would be great as a basis for comparison).


---
**Andy Shilling** *March 13, 2017 21:23*

Ha Ha thought I'd be clever and try it from my mac, I've just installed LW4 on it and get the white screen and now LW3 won't start either. I'll go back out and try the pc.




---
**Andy Shilling** *March 13, 2017 21:34*

Clearly a LW-PC issue. I've just exported the g-code and everything is as it should be, I wonder what could be wrong then as I know there were no updates to the pc last night, looks like it's time to try LW4 on here and hope I don't get the white screen on this.

Cheers **+Ray Kholodovsky** 


---
**Andy Shilling** *March 13, 2017 22:15*

I stand corrected about 25% in the LCD had alarm at the bottom and once again it had stopped. This time it was running to speed but still stopped. 🤔


---
**Andy Shilling** *March 13, 2017 22:46*

**+Peter van der Walt** I downloaded it about 3 hours ago but as it didn't run properly I just uninstalled it, I think I'll wait until the official release before trying it again.




---
**Andy Shilling** *March 14, 2017 22:22*

**+Ray Kholodovsky** I've just had the same problem again with an image, this time from LW3 on my Mac, below is the GLCD showing alarm and I have added more pictures to G Drive linked below. I managed to do a quick greyscale on 3mm ply and that ran ok and the cards on leather but not the dice I was trying to do.



[https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU](https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)





Will this be a possible issue with the C3D or more likely the images I'm using. ( the Dice image is also in the folder on drive).

![images/831bdbc6668a085d96c44ff9f91cec3f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/831bdbc6668a085d96c44ff9f91cec3f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 22:32*

Separate issues. 

Alarm probably caused by invalid gcode getting sent or other weird stuff. Can you reproduce it?



I see no reason why a specific image would cause an issue. 



Run the image again. If you get a halt multiple times in the same spot, that would suggest something. Otherwise it's separate. 


---
**Andy Shilling** *March 14, 2017 22:39*

Ok I'll do some test tomorrow, I'm finding it strange that it didn't like the first image of the word cloud thing, then the greyscale of my kids came out first go but the dice on the leather stopped  half way through. All images run through LW3 so the G-code "should" be ok.


---
**Andy Shilling** *March 14, 2017 22:39*

Would it be worth trying a new USB lead?




---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 22:42*

Communications would have been my next go to - so either a "proper" shielded USB cable with ferrite beads, or just copy the gCode file to the MicroSD card and run from the GLCD.  Which is the first thing I suggested to you...


---
**Andy Shilling** *March 14, 2017 22:45*

I did copy the code of the word cloud to the Sd and that also failed, albeit at about 25% rather than the 15% from the PC but it still failed.


---
**Andy Shilling** *March 14, 2017 22:46*

**+Ray Kholodovsky**​

![images/c40a07bfdd5ad1a65d5657d82c6e6420.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c40a07bfdd5ad1a65d5657d82c6e6420.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 22:46*

Ok so you run from the sd and then you get a halt?  **+Peter van der Walt** thoughts? 


---
**Andy Shilling** *March 14, 2017 22:53*

I'll try and reproduce it again tomorrow but that seems to be 3 different image it doesn't like. Would it be beneficial if you were to generate the g-code and send it to me. Not sure it would make any difference but worth asking.


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 22:56*

Maybe. This is why I wanted that file repo gallery, for this reason. **+Ariel Yahni** **+Joe Spanier** **+Don Kleinschnitz** can you guys put together a few "k40 proven" rasters? I just need you to provide an image, list of settings, and known working gcode. 


---
**Andy Shilling** *March 14, 2017 23:00*

There isn't a Pixel limit on images is there? I think the word cloud was 300ppi 2912px x 2085px. 


---
**Ariel Yahni (UniKpty)** *March 15, 2017 00:27*

I'm not sure what the problem is, does it stutter or stops? Only with rasters? Only with smoothie?


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 02:27*

Andy read here: [plus.google.com - Is it possible to modify the Mini's config via USB, should the need arise? Or...](https://plus.google.com/+BobBuechler/posts/VFBkYQZSg4J)


---
**Andy Shilling** *March 15, 2017 09:56*

**+Ariel Yahni**​​ the laser seems to fire when it should but the motors stutter on some images then stop on others with alarm warning. **+Ray Kholodovsky**​​ I don't usually use my Mac but i will unmount the card tonight when I'm testing. I plan to use four files and run tests at different settings and on both Mac and PC anything else I should try like pwm timing? **+Peter van der Walt**​​ i will check the connections to the end stops also.


---
**Andy Shilling** *March 15, 2017 21:35*

**+Ray Kholodovsky** I have run the first round of tests, image 3,4 and 5 completed fine; image 2 just stopped where you see it. there was no alarm and as you can see in images 1,6 and 7 it actually just stopped. The lcd shows idle but LW3 and terminal just stopped. I cancelled the job in LW and then Homed the laser ( this worked so it was still connected).



I am now running image 2 again this time from the unmounted sd card, so far this is running ok and should be finished in the next half an hour.



First noticeable difference with the failed image and the running one is the laser travel is faster with the sd card, both were set at 60mms 



I'll keep this updated as the images are run but I am very confused at the moment.


---
**Ray Kholodovsky (Cohesion3D)** *March 15, 2017 22:02*

Were there supposed to be images attached to your post? 


---
**Andy Shilling** *March 15, 2017 22:04*

Yup sorry lol

[drive.google.com - Laser - Google Drive](https://drive.google.com/open?id=0B4PVxTUHzQtfUGNGUHZZanliNVU)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/hfUxqvPVj9d) &mdash; content and formatting may not be reliable*
