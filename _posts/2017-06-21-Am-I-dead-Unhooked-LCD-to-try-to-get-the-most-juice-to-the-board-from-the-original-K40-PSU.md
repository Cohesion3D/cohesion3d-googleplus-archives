---
layout: post
title: "Am I dead? Unhooked LCD to try to get the most juice to the board from the original K40 PSU"
date: June 21, 2017 18:43
category: "C3D Mini Support"
author: "John Milleker Jr."
---
Am I dead?



Unhooked LCD to try to get the most juice to the board from the original K40 PSU. Now I get no boot. Only RED (VMOT?) LED. Plugged the LCD back in and it beeps and then whines and stops. Now I can't even test anymore. So far today I've changed my Acceleration, Jerk and the Planner Queue Size but changed them all back when that didn't help. I also upped my X stepper motor from 45° to 90° clockwise about an hour ago but didn't go past 90.



What's going on now? Tried yet another SD Card and new firmware/config and I'm getting nothing. Not even the computer seeing it as a drive. **+Ray Kholodovsky** - What am I doing wrong with this thing? 



I just want a good 100mm/s raster engrave which is supposedly a quarter of what others are getting. Can we try another board Ray? I'll be happy to pay shipping. I don't want to abandon ship, others seem to be using this board just fine.

![images/a66bca392ddb30575f2eac35169fa502.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a66bca392ddb30575f2eac35169fa502.jpeg)



**"John Milleker Jr."**

---
---
**John Milleker Jr.** *June 21, 2017 19:13*

I just ordered a new board, hopefully if we can get this one working it'll be a spare. Or maybe can get a refund. But at least I'm not worried so much about getting back up and running with the simple rasters jobs I have next week that don't lock up the unit.



Update: Talked to Ray, he talked me off the ledge. We have a plan of attack. New board will be here in a couple of days and I'll send the problem one back. Thanks Ray!


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/44aMLJaHVUj) &mdash; content and formatting may not be reliable*
