---
layout: post
title: "I finally installed my C3D into my K40 after having it sitting around for several months, but I have a couple of problems"
date: May 26, 2018 11:05
category: "K40 and other Lasers"
author: "tony turner"
---
I finally installed my C3D into my K40 after having it sitting around for several months, but I have a couple of problems. When you start the job the laser turns on at the home position or at the park position and burns a line from there to the start of the job.

and when cutting the laser does not turn off between cuts.



I have pin 2.5 bed hooked up to the PWM pin on the power supply, And I have connected the K+ to pin 2.6 which I enabled in the config file.

I am totaly lost as to what is going on and what I have to do to fix the problem





**"tony turner"**

---
---
**Anthony Bolgar** *May 26, 2018 11:30*

What firmware are you running....Smoothie or Grbl-lpc?




---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2018 11:36*

There’s your problem, why did you connect k+ to pin 2.6? Please follow the directions exactly. 


---
**tony turner** *May 26, 2018 11:58*

**+Ray Kholodovsky** If I don't connect that pin to something the laser won't fire at all it needs to be grounded to fire the laser. smothie software that came with the original sd card


---
**tony turner** *May 26, 2018 12:12*

**+Ray Kholodovsky** when the k+ pin is held high the laser is off and when it is taken low the laser is enabled. I have none of the original wiring in the K40. When I look at pin 2.6 with a crow it is high until the head moves then it goes low if this could be made to go low only when the laser is firing via the PWM then all would be good


---
**tony turner** *May 26, 2018 12:51*

I must clarify something a bit further, when I first installed the C3D board the K+ is connected to ground and the PWM pin is connected to pin 2.5 and the head is at the home position and no program is running the laser fires at maximum power continuously. so I had to turn the laser off somehow.



Now after writing my initial problem i have now checked the pin 2.5 and found that when idle the pin is at 5 volts, and when the PWM starts to fire the laser the signal goes low. Should this not be the other way around that the pin is low and the PWM signal goes high to fire the laser. So it appears the output is inverted to what my lasre requires to control the power.

. 


---
**Don Kleinschnitz Jr.** *May 26, 2018 12:55*

**+tony turner** what laser power supply (LPS)are you using? Please post picture of connections.



Normally a smoothies PWM function is connected to "L" on the LPS. The PWM will turn on and off with a job so another laser control is not needed.



No other connections other than L and gnd is needed between the C3D and the LPS. 



The K and P functions are used for test fire and laser enable respectively and they operate independent of the controller.

 

Here is a schematic of an  <i>almost</i> stock K40:



[https://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/](https://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/)





In this troubleshooting PDF are definitions and pin-outs of two LPS types. You can use it to understand their function.



[drive.google.com - LPS Testing Flowchart.pdf](https://drive.google.com/file/d/1zfU4DWZcEo7YxYvh_9z2pDQ0S1UXHLno/view)




---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2018 13:49*

Let's take a step back.  Show pics of your laser, wiring, and psu please. 



On the normal stock K40 L being pulled to L fires the laser.   The board does not have anything to do with K+.  And there is a potentiometer that we keep in line in order to set the max power level, then the board pwm pulses a proportion of that to get software power control. 


---
**tony turner** *May 26, 2018 15:38*

this is what I have just taken out of my K40 an Arduino mega with a ramps board. as can be seen here the k+ is wired to the the ramps as is the PWM pin. there is no front panel controls at all in my K40 all the laser control was done with the ramp board. i have also had an Arduino uno with the CNC shield running GRBL wired up the same way with no front panel. and both set ups worked correctly. I purchased the c#d because I was looking for more speed in the raster engraving.

But so far the C3D does not seem to work as a replacement for these Arduino boards

![images/39401ceb7644b282b54dff2637139877.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39401ceb7644b282b54dff2637139877.jpeg)


---
**Don Kleinschnitz Jr.** *May 26, 2018 17:38*

**+tony turner** arduino and smoothie controllers are very different architectures with different I/O voltage levels. You cannot expect a smoothie to be a drop in replacement. 



That said, I see nothing unusual about the machine you are plugging into and following **+Ray Kholodovsky** instructions should be pain free and result in a successful upgrade.



In the drawing above the LPS is controlled by an Enable on <i>Laser Fire</i> and the PWM on <i>IN</i>. The laser currents local pot was evidently removed.



You arduino config controls the lasers power fully from software but there are goods and bad's to that approach. Most of us have kept pot controls for practical job setup and tube wear out reasons.



I would suggest that you configure the machine with both software (open drain on L) and pot current control (pot on IN). This will get you working with the C3D and provide you with the needed manual control of the lasers power offset. Then you can modify it to only software control if you want.



In a smoothie configuration you do not need Laser Fire for basic operation and you need the PWM to be on the L pin wired from an open drain. **+Ray Kholodovsky** site has info on all that. 






---
**tony turner** *May 27, 2018 01:06*

**+Don Kleinschnitz** Well I bought the C3D board after asking questions on in this group about my K40 being different to what was in the instructions, and was told by I think Ray that there would be no problems and all configurations can be handled by the C3D. As I am finding out now this is not true. You state above that the the Arduino and the Smoothie controllers have different I/O voltages that is correct Arduino uses 5 volts and the Smoothie controllers use 3.3 volts. So which is a correct replacement for the original  controller, I needed a 5 volt supply to run, and all the connectors are labelled 5 volts. The Smoothie controllers CPU runs of 3.3 volts, but all the connectors have 5 volts labelled so does this board do voltage level translation to make it compatible with the old controller.

And as far as being a drop in replacement well as you can see in the photo the original controller board is larger than the C3D, so the holes don't line up. My K40 originally had the digital front panel so never had a pot. I asked about this also but was told no problems.

From factory on the power supply the P+ and G pins are soldered together. 

I had also had to reconfigure the end stops in the configuration as mine are active low using the the same switches as shown on the pin out of the C3D board. all this was supposed to be configured  already that is why I bought the SD card with the board. there is no documentation as to what the configuration settings mean, like calling the axis's alpha, beta and gamma instead of X,Y, and Z.

my opinion at the moment is this board is a waste of money and load of rubbish and I should have bought something else. So will probably complain to Cohesion about this.

![images/14233c656a8bab8ba7ced616fd029248.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/14233c656a8bab8ba7ced616fd029248.jpeg)


---
**Anthony Bolgar** *May 27, 2018 01:58*

I have a digital panel on one of my lasers and had no issues wiring it to the C3D board. Please do not think I am being mean, but if you find it difficult to install the C3D board, anything else you buy will be a nightmare by comparison, the C3D board is as user friendly as you are going to find. That being said, I am sure that the users here can help you with the install and get you up and running.


---
**Don Kleinschnitz Jr.** *May 27, 2018 13:20*

**+tony turner** 

I don't think arguing the compatibility of the C3D is going to get your machine running properly.



Based on your original post and best I can tell your machine needs only 3 things changed to start working properly:



<b>1. The IN signal needs a voltage to control laser power</b>

   a. Add a pot to the IN

or

   b. Jumper IN to 5V, which will fix the LPS to full power

or

  c. Reconnect the digital panel

<b>2. The P and K+ signals need the correct voltage</b>

  a. Reconnect the digital panel

or

  b. Jumper [K+ to K-] and [P+ to Gnd]

<b>3. Connect the PWM signal from the C3D to pin L on the LPS as is instructed in the C3D documentation</b>



If you decide to make these changes let us know and we can continue to provide advice. These changes can be temporary to get the machine running correctly and then you can further modify to your liking.



<b>Note:</b> 1b, 2b are temporary fixes. 



All three of these have various instructions available. If you do not find them I & **+Ray Kholodovsky** can point you to them.


---
*Imported from [Google+](https://plus.google.com/110291018143272470366/posts/RyuBvpnLe5U) &mdash; content and formatting may not be reliable*
