---
layout: post
title: "Does anyone have pictures of things you've made with a laser running C3D?"
date: January 10, 2018 00:17
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Does anyone have pictures of things you've made with a laser running C3D? The product page could use some :)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *January 10, 2018 00:40*

I'll send you some pics of a few of the items I have made.


---
**Joe Alexander** *January 10, 2018 06:24*

I have several but they don't count as they were done on a smoothie 4xC ;)


---
**Andy Shilling** *January 10, 2018 17:31*

+Ray KKholodovsky I've got a few nice ones of bits we sold at Christmas, I'll put them on my drive when I get home and you can have them if they suit.


---
**Andy Shilling** *January 10, 2018 20:53*

**+Ray Kholodovsky**



Here you go buddy, take your pick if you like them.



[drive.google.com - Lasers Photos - Google Drive](https://drive.google.com/open?id=1lWXV67q5qJ7aZw3UPP9_YADHktsrlqjv)


---
**Todd Mitchell** *January 10, 2018 21:10*

**+Ray Kholodovsky** I'm 

happy to take some next time I do a laser cut.  I'm using the mini - is that what you seek?


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 21:13*

Thanks Andy!



Todd- Appreciated... I'm just looking for some nice raster engravings that people have done... nothing specific... if there's a specific work that I think of I'll let you know. 


---
**Todd Mitchell** *January 10, 2018 21:14*

**+Ray Kholodovsky**  Ahh, i dont have any raster engravings.  Just utility items thus far (control box for my other cnc routers, etc.)




---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 21:16*

I'm the same way... I just cut engineering parts, and that's when my k40 isn't in a box from the last trade show I was at. 



I rastered a bunny once. It wasn't good. 


---
**Joe Alexander** *January 10, 2018 21:36*

I'll post some pretty good raster engravings I did of The Walking Dead cast, even if I don't belong! heh


---
**Bill Knopp** *February 21, 2018 20:09*

Have not done much, but I think this one worked out quite well.

![images/c715e3a080e7ba35c07e5dfc33d0c784.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c715e3a080e7ba35c07e5dfc33d0c784.jpeg)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/44eBxvNjFjV) &mdash; content and formatting may not be reliable*
