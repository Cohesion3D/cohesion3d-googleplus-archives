---
layout: post
title: "Ok i installed my cohesion board inside my k40 and have the lcd hooked up (which i think is a cool feature to have...) couple things i ran into when installing it..."
date: April 04, 2017 16:22
category: "C3D Mini Support"
author: "michael chartier"
---
Ok i installed my cohesion board inside my k40 and have the lcd hooked up (which i think is a cool feature to have...) couple things i ran into when installing it...



I only have one jst-xh connector in my machine for the axis.... it looks looped together and tied into the same connector.... i have the ribbon connector installed but on the old board it had the text of something like... x motor... am i going to be able to run it this way with my new controller or will i have to run new wires? 



Also what is the wire for that comes with the board for? 









![images/846f7bec5891182729281f3472230ceb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/846f7bec5891182729281f3472230ceb.jpeg)
![images/4b8c88e68f3cb7fca80167f66364a6f8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b8c88e68f3cb7fca80167f66364a6f8.jpeg)

**"michael chartier"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 16:25*

Hi Michael. This is the ribbon cable setup, it is fairly common. There are some posts from Allen or Steven a bit below that have pics of how to do it. 

The one JST is the Y motor. 

And as long as the big power connector is oriented properly (notch pushing the wall out same as on the stock board) that's all it needs. 



The cable that comes with the kit is not used, set it aside for now. 


---
**michael chartier** *April 04, 2017 16:31*

Thank u!!!! Ill post pics tonight when i start running some tests on anodized aluminum, that is what i needed to know i appreciate ur fast response again!


---
*Imported from [Google+](https://plus.google.com/116057139333311855526/posts/8z77cyqmySJ) &mdash; content and formatting may not be reliable*
