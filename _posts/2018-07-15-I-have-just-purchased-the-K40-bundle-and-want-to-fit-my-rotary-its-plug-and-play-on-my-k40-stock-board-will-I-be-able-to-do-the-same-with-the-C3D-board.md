---
layout: post
title: "I have just purchased the K40 bundle and want to fit my rotary, it,s plug and play on my k40 stock board, will I be able to do the same with the C3D board?"
date: July 15, 2018 09:10
category: "General Discussion"
author: "John Camfield"
---
I have just purchased the K40 bundle and want to fit my rotary, it,s plug and play on my k40 stock board, will I be able to do the same with the C3D board? I have attached a picture of my rotary. or will I need the External Stepper Driver [4 Amp].

![images/3c360cc2bc199573358165aa0beac5fe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c360cc2bc199573358165aa0beac5fe.jpeg)



**"John Camfield"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 15, 2018 12:14*

Read the guide at Cohesion3D.com/start


---
**James Rivera** *July 15, 2018 18:22*

Aaaand here’s the link![cohesion3d.com - Getting Started](http://cohesion3d.com/start)


---
**Ray Kholodovsky (Cohesion3D)** *July 15, 2018 18:25*

Yes, thank you mobile. 


---
*Imported from [Google+](https://plus.google.com/103234827681964755838/posts/XuG455uXUfC) &mdash; content and formatting may not be reliable*
