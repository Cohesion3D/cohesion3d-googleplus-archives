---
layout: post
title: "Where do i find the latest firmware for the mini cohesion 3d board?"
date: July 31, 2017 01:26
category: "C3D Mini Support"
author: "John Austin"
---
Where do i find the latest firmware for the mini cohesion 3d board?

looks like my current firmware is 1-10-17 and configure is 3-1-2017.

I finally got the board and computer to talk. So like to see if my firmware is the latest. Also if i need to update it what do i need to do?

JOHN





**"John Austin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 31, 2017 01:33*

Recommend keeping everything "as known" until you are up and running well.  Only reason to update firmware at this stage is to get the rotary running (special firmware build for this). 


---
**John Austin** *July 31, 2017 01:35*

OK so get everything working for and then worry about the rotary


---
**Ray Kholodovsky (Cohesion3D)** *July 31, 2017 01:37*

Will answer all here. 



Yes, get basics up and running first.



Then, the info for smoothie firmware with rotary is here in the video description: 
{% include youtubePlayer.html id="UoGlDKjPGSE" %}
[youtube.com - Laserweb4 K40 Rotary Test](https://www.youtube.com/watch?v=UoGlDKjPGSE&t=9s)


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/eBcc6EErkSe) &mdash; content and formatting may not be reliable*
