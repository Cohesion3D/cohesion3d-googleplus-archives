---
layout: post
title: "Evening all - ordered the z table and rotary, I'm running smoothie on a Cohesion 3D and wondering if I can do the following - do I really need to use external stepper adaptors and drivers of can I just add 2 more A4988"
date: June 12, 2018 18:29
category: "General Discussion"
author: "LA12 Sports Images LA12 Sports Images"
---
Evening all - ordered the z table and rotary, I'm running smoothie on a Cohesion 3D and wondering if I can do the following - do I really need to use external stepper adaptors and drivers of can I just add 2 more A4988 drivers and use the z for bed height (controlled by jog function on the C3D LCD ) and the rotary on the A connector - using Lightburn ?





**"LA12 Sports Images LA12 Sports Images"**

---
---
**Joe Alexander** *June 12, 2018 18:46*

you should be able to as long as the motors for those axes don't exceed the amperage of the A4988's (which I believe is 2A max). as for Lightburn I couldn't tell you since I use Laserweb4. I'm sure one of the group members can shed some light on that facet.


---
**LA12 Sports Images LA12 Sports Images** *June 12, 2018 19:08*

**+Joe Alexander** Thank you


---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2018 21:21*

It’s not a 2a max, it’s much lower than that. You’re welcome to try the A4988’s to see if they can provide enough power. They might not be able to. It depends on your z table, how much current the motor draws...


---
**LA12 Sports Images LA12 Sports Images** *June 12, 2018 21:30*

Thanks Ray - are the Z and A ports enabled by default or do I need to use different firmware ?


---
**LA12 Sports Images LA12 Sports Images** *June 12, 2018 21:33*

They are Holgamods devices but considering using a Nema17 for the table instead of the monster standard unit - the rotary is a 17 as standard I believe




---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2018 21:37*

Please read the instructions at Cohesion3D.com/start [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**LA12 Sports Images LA12 Sports Images** *June 12, 2018 21:45*

Ok I'm using Lightburn so will try to muddle through


---
*Imported from [Google+](https://plus.google.com/102441991153902344952/posts/XqjEmQZitA4) &mdash; content and formatting may not be reliable*
