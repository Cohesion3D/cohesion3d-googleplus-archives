---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) Can anyone recommend a super basic scriptable gcode sender, with console?"
date: November 30, 2016 05:51
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



Can anyone recommend a super basic <b>scriptable</b> gcode sender, with console?



Essentially what I need is Repetier Host's Scripts 1-5 functionality where I can load some lines of gCode and then have buttons to run it.  Would be nice to have an RH style console there as well, and the communications obviously so it can talk to smoothie over USB. 



We're going to have the assembler do a functional test for each C3D Mini board so I'd really like to have a prepackaged program I can send them and tell them, press button 1 and verify each motor spins.  Press button 2 and watch for this... etc...





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Tiago Vale** *November 30, 2016 07:48*

You can record macros with octopi


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/8XBWzrVEKwp) &mdash; content and formatting may not be reliable*
