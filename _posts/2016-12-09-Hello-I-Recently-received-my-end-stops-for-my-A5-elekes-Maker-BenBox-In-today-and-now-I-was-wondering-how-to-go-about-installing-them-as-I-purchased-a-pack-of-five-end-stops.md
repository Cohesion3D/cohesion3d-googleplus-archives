---
layout: post
title: "Hello, I Recently received my end stops for my A5 elekes Maker BenBox In today and now I was wondering how to go about installing them as I purchased a pack of five end stops"
date: December 09, 2016 22:11
category: "General Discussion"
author: "Jonathan Davis (Leo Lion)"
---
Hello, I Recently received my end stops for my A5 elekes Maker BenBox In today and now I was wondering how to go about installing them as I purchased a pack of five end stops.





**"Jonathan Davis (Leo Lion)"**

---
---
**Griffin Paquette** *December 09, 2016 22:19*

Hey Jonathan would you be able to send some pictures of your setup and further explain what you are confused with? There are various types of end stops so adding some pictures of the end stops as well as your setup would best help us resolve your issue.


---
**Jonathan Davis (Leo Lion)** *December 09, 2016 22:26*

**+Griffin Paquette** Can do and yeah I know I forgot to post images.

Endstops i'm using: [http://a.co/eCul44r](http://a.co/eCul44r)

My machine from the original dealer site: [http://www.gearbest.com/3d-printers-3d-printer-kits/pp_290386.html](http://www.gearbest.com/3d-printers-3d-printer-kits/pp_290386.html)


---
**Griffin Paquette** *December 09, 2016 23:15*

Looking down at the board as shown below, you will place your X- axis endstop into the X-min port (this is the axis moving left and right). The red wire being your voltage should go to the right of the connector when looking at it face on as such. Your Y-axis endstop should be placed in the same orientation and in the Y-min port on the board (axis moving up and down). 

![images/50ba5dba1f6cb93c7e588de0035dd617.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/50ba5dba1f6cb93c7e588de0035dd617.png)


---
**Jonathan Davis (Leo Lion)** *December 09, 2016 23:30*

**+Griffin Paquette** So roughly 4 Endstops will be needed


---
**Griffin Paquette** *December 10, 2016 01:15*

You shouldn't need more than two for the X and Y (one each). The machine knows how far it can move away from each endstop before it runs out of room. This is all configured in the smoothieware configuration file (config.txt)


---
**Jonathan Davis (Leo Lion)** *December 10, 2016 01:16*

**+Griffin Paquette** alrighty and in LaserWeb i have had time where it would want to continue going when the track does not exist for it to continue. Or it would start in some odd spot thinking it was its home location


---
**Griffin Paquette** *December 10, 2016 03:03*

Ah, in that case, you should go ahead and install the max endstops as well.   X Max and Y Max ports with same wire orientation. 


---
**Jonathan Davis (Leo Lion)** *December 10, 2016 03:05*

**+Griffin Paquette** Yeah


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/f5F28bYrEGh) &mdash; content and formatting may not be reliable*
