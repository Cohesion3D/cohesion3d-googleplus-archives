---
layout: post
title: "Does anyone have a mounting template for the GLCD?"
date: June 01, 2017 20:58
category: "K40 and other Lasers"
author: "Keith Burns"
---
Does anyone have a mounting template for the GLCD? I would like to cut out a spot for it above the existing current meter and pot. Figured someone might already have a template that I can use to cut the holes for it so I don't have to create it from scratch.





**"Keith Burns"**

---
---
**Joe Alexander** *June 01, 2017 21:12*

check my earlier share of my google+ drive, on the control panel there is a section that has the cutout you want.


---
**Joe Alexander** *June 01, 2017 21:13*

ironically I found some other guys and did the same thing, took the outline i needed and transferred it.


---
**Ashley M. Kirchner [Norym]** *June 01, 2017 21:27*

This is my panel design. [drive.google.com - Control Panel-01.svg - Google Drive](https://drive.google.com/open?id=0B8NExy6DoX2RSlE5R0R3Qm1GLVU)


---
**Anthony Bolgar** *June 01, 2017 21:48*

My front panel is at [thingiverse.com - K40 Front Panel by funinthefalls](https://www.thingiverse.com/thing:1564287)




---
*Imported from [Google+](https://plus.google.com/105762576990882303746/posts/aWLMSdWbYP3) &mdash; content and formatting may not be reliable*
