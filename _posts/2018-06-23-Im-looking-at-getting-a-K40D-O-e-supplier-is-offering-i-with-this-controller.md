---
layout: post
title: "I'm looking at getting a K40D. O e supplier is offering i with this controller"
date: June 23, 2018 04:19
category: "K40 and other Lasers"
author: "Lynn Roth"
---
I'm looking at getting a K40D.  O e supplier is offering i with this controller.  Is this easily replaced with the cohesion 3d mini?  They also offer a ruida board, but that is 300 more.  I'd appreciate any thoughts.

![images/72570ce8b06da2e66ec5280909b721f7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/72570ce8b06da2e66ec5280909b721f7.png)



**"Lynn Roth"**

---
---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:23*

the cohesion3d mini is designed to be a direct, plug and play replacement for the m2 nano. i installed mine in around 10 minutes and no modifications were necessary. it is a very good idea to thoroughly familiarize yourself with the instructions. they are well written and step by step. [cohesion3d.com - Getting Started](http://cohesion3d.com/start)


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:27*

you can see more on my setup here: 

[techbravo.net - K40 Laser Info - Bravo Technologies](https://techbravo.net/k40)


---
**Lynn Roth** *June 23, 2018 04:28*

Sounds good. Can you confirm that this is the same board?   


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:31*

same board as what? do you have your laser yet or are you still exploring? the reason I ask it that the one in the pic you provided is not an M2 i dont think. that's not to say that it wont work but if the board pictured is not your board it really doesn't matter. 




---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:33*

this is the current nano m2 i think:

![images/24af1bfd0d181a8257a5f48318bd1506.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/24af1bfd0d181a8257a5f48318bd1506.jpeg)


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:34*

the k40d (and most other k40's) will come with the M2.




---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:34*

the best thing is to verify with the OEM before you purchase but i'm 99.9% certain it will be compatible without modifications.


---
**Lynn Roth** *June 23, 2018 04:35*

I do t have one yet, still exploring.  This is the controller in one that I was considering.  


---
**Lynn Roth** *June 23, 2018 04:37*

**+Tech Bravo** thanks for your help.


---
**Lynn Roth** *June 23, 2018 04:38*

Is there a preferred supplier for a K40D?  


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:38*

**+Lynn Roth** if that is the board they are putting in the K40D i would be surprised. that is an older board. if you got the pic from the OEM then it may be a stock image they had laying around. i'm not 100% positive however. they piece these things together with no rhyme or reason sometimes. the function of the boards is the same. the difference is usually just the connectors.


---
**Lynn Roth** *June 23, 2018 04:43*

**+Tech Bravo**  thanks.  This was the image they gave me.  I was surprised as well.  I even asked about the M2nano and they said no....  Perhaps time to look somewhere else.


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 04:43*

I have been looking for one. They seem to be scarce. I have a contact in china but i'd have to but 5 at the minimum and i don't want that kind of overhead for such a little profit margin LOL. They may sell you one. Email me (lasers [at] techbravo dot net and i will get you in touch with them


---
**Lynn Roth** *June 23, 2018 04:45*

**+Tech Bravo** thanks will do


---
**Anthony Bolgar** *June 23, 2018 06:29*

I have a M2Nano board I am not using right now.




---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 06:46*

**+Lynn Roth** email sent :)


---
*Imported from [Google+](https://plus.google.com/+LynnRoth/posts/9Zs9ZCCHfK6) &mdash; content and formatting may not be reliable*
