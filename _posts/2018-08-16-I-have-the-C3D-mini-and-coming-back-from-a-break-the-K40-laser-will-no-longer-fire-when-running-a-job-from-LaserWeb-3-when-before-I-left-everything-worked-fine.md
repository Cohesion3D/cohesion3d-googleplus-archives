---
layout: post
title: "I have the C3D mini.... and coming back from a break the K40 laser will no longer fire when running a job from LaserWeb 3 when before I left everything worked fine"
date: August 16, 2018 13:30
category: "C3D Mini Support"
author: "ki ki"
---
I have the C3D mini.... and coming back from a break the K40 laser will no longer fire when running a job from LaserWeb 3 when before I left everything worked fine. LW3 seems to have kept the same working config (but I need to double check this agains the info on the C3D site) so I don't think it is that. I checked all the wiring, fume extraction, water cooling and so on and cant find anything wrong. The laser does work as when I push the 'test' button the laser fires. I therefore suspect something linked either to the C3D mini board (lost config / firmware?) or the windows laserweb software. I have yet to look into this correctly but does anyone have any idea as to what the issue might be? 

Thanks for any help

=)





**"ki ki"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 17, 2018 14:28*

Check if your computer can see the firmware and config files on the memory card 


---
**ki ki** *August 27, 2018 07:15*

So after a huge amount of debugging I finally found the issue and am posting here as it might help others. Thanks for the help Ray but the config files were perfect. The issue was one of the green cables from the power supply to the C3D board was loose, as in falling out of the 'molex' plug. I soldered it back in and is it now working fine. Thanks for the help Ray. 


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2018 13:36*

The L wire for laser fire in the large power connector going to the Mini?  This is not uncommon. 


---
**ki ki** *August 27, 2018 14:03*

I believe so but would have to check the wiring diagram to be sure. it is green and at the end of the 'molex' type plug. Perhaps not uncommon but it took me a while to figure out as it was still hanging on in there just enough not to be noticed! thanks again for the help, its appreciated.


---
*Imported from [Google+](https://plus.google.com/101815393777110076549/posts/HSLDerqibSw) &mdash; content and formatting may not be reliable*
