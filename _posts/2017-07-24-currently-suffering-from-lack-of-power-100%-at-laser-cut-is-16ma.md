---
layout: post
title: "currently suffering from lack of power. 100% at laser cut is 16ma"
date: July 24, 2017 21:52
category: "FirmWare and Config."
author: "timne0"
---
currently suffering from lack of power.  100% at laser cut is 16ma.  Max power used to be 21ma on the ramps board and I had to ramp it back to 18ma (85% power).  Currently cutting with 1.0 power, 0.1 min and 1.0 max if none is specified. 



pastebin code is here: [https://pastebin.com/WscCUCci](https://pastebin.com/WscCUCci)



PWM doesn't seem to do anything.  200 and 400 are almost identical on output.  I've knocked it down to 100 on this.  Still no change.



I am controlling PWM via the PWM pin 2.5 - any help appreciated.  Rastering is working nicely, I get a good result for very light images if I set 9% min power, 15% max power, 300mm/sec - but if I go to 10% power it jumps up by a power of magnitude...









**"timne0"**

---
---
**Anthony Bolgar** *July 25, 2017 11:57*

Change your settings for laser power from 0.8 to 1.0 and you will get back the missing power. Ray has the max power set to 0.8 in the config file.



laser_module_maximum_power                    0.8             # this is the maximum duty cycle that will be applied to the laser


---
**timne0** *July 25, 2017 11:58*

**+Anthony Bolgar** I already have - is there a setting I've missed?


---
**Anthony Bolgar** *July 25, 2017 11:59*

It is set to 0.8 in your pastebin 




---
**timne0** *July 25, 2017 12:00*

Are you sure? I've just opened the one attached to this message and it shows:



laser_module_maximum_power                    1.0             # this is the maximum duty cycle that will be applied to the laser

laser_module_minimum_power                    0.1             # This is a value just below the minimum duty cycle that keeps the laser

                                                              # active without actually burning.

#laser_module_default_power                   1.0             # This is the default laser power that will be used for cuts if a power has not been specified.  The value is a scale between




---
**Anthony Bolgar** *July 25, 2017 12:02*

Sorry, I was looking at the first one you posted about the y axis needing flipping.


---
**timne0** *July 25, 2017 12:02*

yes :( sadly so.  Any other ideas?


---
**Anthony Bolgar** *July 25, 2017 12:11*

That was the only thing I could think of.


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2017 16:02*

Try running a G1 line for a full machine length - 300mm, and a slower speed. Observe the mA at the max/ middle. 


---
**timne0** *July 25, 2017 16:41*

Will do tomorrow when I'm next in. Birthday curry tonight!


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/cUzagUpLUNj) &mdash; content and formatting may not be reliable*
