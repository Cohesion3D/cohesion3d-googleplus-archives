---
layout: post
title: "LaserWeb config question! probably its not for this group, but just in case :)..."
date: January 24, 2017 23:46
category: "General Discussion"
author: "Antonio Garcia"
---
LaserWeb config question! probably it´s not for this group, but just in case :)...

I´m guessing that if put Laserpower 20%, we should get the 20% of my position in the pot, isn´t it??, because no matter what value put in that box, the laser fire with the config of the pot... In your LaserWeb config is working fine?



![images/a6e13fdd59b49878815dbeb5c213caea.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a6e13fdd59b49878815dbeb5c213caea.png)



**"Antonio Garcia"**

---
---
**Ariel Yahni (UniKpty)** *January 25, 2017 00:21*

Yes whatever you put into the  Pot will be the ceiling or 100% 


---
**Carl Fisher** *January 25, 2017 12:27*

But setting LW to anything over your pot setting is worthless as far as I understand it. You won't get a 0-100% of gradient up to the pot setting. You'll get 0-pot max and anything over that will still only to to the max pot setting. 



I.e. 80% in your gcode won't give you 80% of 20, it'll just give you 20% if that is your pot setting. 



Make sense?


---
**John Sturgess** *January 25, 2017 13:00*

**+Carl Fisher** that's not what i understood to be the case. You are setting the pulse duration here i thought. 100% will be what the pot is set at.


---
**Carl Fisher** *January 25, 2017 13:14*

I could be wrong but I'd love to know for certain so I can document it in LW


---
**Ariel Yahni (UniKpty)** *January 25, 2017 13:16*

**+Peter van der Walt**  Can you clarify the above please?


---
**Antonio Garcia** *January 25, 2017 19:01*

Hi guys,

my issue right now is, not matter what value i put in that power input box (LW), i always get the power is set up in the pot. So I´m guessing i have something wrong in my configuration.

But my understanding was, if set the pot to 15mA, that would be the ceiling.... so if in LW i 50%, i would get 7.5mA.... but at this moment I´m far of that hahaha, because I always get the same...

**+Ray Kholodovsky** any idea where should I look?? I´m not sure if the issue comes from LW or from my cohesion config..


---
**Ariel Yahni (UniKpty)** *January 25, 2017 19:03*

Post you laser section here from the config file


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 19:05*

Try this.  Get the head clear.  Zero out the coordinates.  Using the console in the bottom of LW:

Go run G1 X10 S0.2 F600

and then G0 X0 Y2 to put it back. 

And run G1 X10 S0.4 F600

And repeat.

And all the way up to S1.0

And see what the power level of each of those lines looks like.


---
**Antonio Garcia** *January 25, 2017 20:44*

**+Ray Kholodovsky** Different  power with different S... All of them are exactly the same... (Pot at 15mA)



![images/8e0f9870408bf8200e3408755f15e0b1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8e0f9870408bf8200e3408755f15e0b1.jpeg)


---
**Antonio Garcia** *January 25, 2017 20:48*

**+Ariel Yahni** this is my config file for the laser



![images/ac1a2a40174b5305cdd35a3b50b098d1.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ac1a2a40174b5305cdd35a3b50b098d1.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 20:49*

Now try running those all at S1 

but try some different values on the physical pot for each line. 


---
**Antonio Garcia** *January 25, 2017 21:00*

It's weird, almost all of them look similar except when pot is really low... All of them with S1.0

![images/28322056df5c33acc18ef60178912b0e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/28322056df5c33acc18ef60178912b0e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:02*

What is the power meter on the machine reading when you do this/ fire at the different pot values?


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:03*

Meaning, it's actually showing those values from 5 - 15mA?


---
**Ariel Yahni (UniKpty)** *January 25, 2017 21:07*

**+Ray Kholodovsky**​i would suggest he runs the grayscale barwe have used before that we it's easier to see. Also that way will look at how deep it burns as from the top it will look the same also


---
**Ariel Yahni (UniKpty)** *January 25, 2017 21:07*

[https://plus.google.com/+ArielYahni/posts/1ev6vHTNbff](https://plus.google.com/+ArielYahni/posts/1ev6vHTNbff)


---
**Antonio Garcia** *January 25, 2017 21:08*

In my pot, i did some marks, from highest value until lowest value, likely it´s not perfect, but for my measures and testing in different materials was fine...

below the blue mark, the laser doesn´t fire

![images/e4e2f5c7f2aa92f2950fda86f7cea928.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e4e2f5c7f2aa92f2950fda86f7cea928.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:11*

**+Antonio Garcia** what I meant was, for each of those lines you did with the pot at different values, what does the needle current indicator show? 


---
**Antonio Garcia** *January 25, 2017 21:16*

yes, for those lines, the first line pot was on 15mA, second one on 12mA, third one on 9mA and so on...and all of them with S1.0...



And for the first set of line,  pot was always on 15mA, and then i tried each line with different S, S0.2, S0.4 and so on..


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:19*

Yes, and what value(s) was the <b>CURRENT INDICATOR</b> showing at each time?  The thing with the needle to the left of the pot...


---
**Antonio Garcia** *January 25, 2017 21:24*

**+Ariel Yahni** I´m doing something wrong for sure hahaha, That what i got when engrave the Calibration10%.Bmp...

In parameters for rastering I put 200m/s Light, 50m/s Dark

![images/aef304d65fa5239f73f1d360b18382ea.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aef304d65fa5239f73f1d360b18382ea.jpeg)


---
**Ariel Yahni (UniKpty)** *January 25, 2017 21:27*

Use 300 black and 400 white at 10mA in the pot 100% in LW


---
**Ariel Yahni (UniKpty)** *January 25, 2017 21:28*

Sorry **+Ray Kholodovsky**​ I don't meant to be in the middle


---
**Antonio Garcia** *January 25, 2017 21:30*

**+Ray Kholodovsky** the current indicator was showing the correct values, I mean, if I put 15mA, it showed 15mA, if i put 10mA, it showed 10mA..


---
**Antonio Garcia** *January 25, 2017 21:31*

hahaha, my machine is OVER POWEREDDDDD hahaha


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:32*

**+Antonio Garcia**  And what about when you run those G1 lines with the pot set at one value (I guess it would have to be a high value) and the different S values?  What does the current indicator show then?


---
**Antonio Garcia** *January 25, 2017 21:35*

**+Ray Kholodovsky** Always the current indicator showing pot level... if i put pot level to 15mA, and run G1 X20 S0.2 F600, current meter shows 15mA


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:36*

**+Ariel Yahni** no worries, please continue.


---
**Antonio Garcia** *January 25, 2017 21:45*

**+Ariel Yahni** maybe i´m doing something wrong, because i can see any rastering hahaha, it looks all the same for me...

![images/36f12f9925a558e3aa10c657f982fff4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/36f12f9925a558e3aa10c657f982fff4.jpeg)


---
**Antonio Garcia** *January 25, 2017 21:49*

When I´m in this screen, then i generate the gcode, do we should click in Trace in Vector? if so, what value i should set in the "Feedreate:Cut"...

![images/bcada3de1b44737e31f699b6bc555be7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bcada3de1b44737e31f699b6bc555be7.png)


---
**Ariel Yahni (UniKpty)** *January 25, 2017 21:52*

No, if it's a raster you don't trace. Just generate and send. Trace is for cutting mostly or line engrave


---
**Antonio Garcia** *January 25, 2017 21:53*

**+Ariel Yahni** so that was the way i did then...


---
**Antonio Garcia** *January 25, 2017 21:55*

I´m using last version of LW3...

I pulled from master branch yesterday night...


---
**Antonio Garcia** *January 25, 2017 21:59*

my hw config, one more time, just in case someone could see any weird connection :(

![images/56fd5b703614913b138e6bb440c037c3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/56fd5b703614913b138e6bb440c037c3.jpeg)


---
**Antonio Garcia** *January 25, 2017 22:50*

**+Ray Kholodovsky** I´m gonna order another glcd, could you confirm me that, this one is fine..

[amazon.co.uk - Anycubic LCD 12864 Graphic Smart Display Controller Module for RAMPS 1.4 RepRap 3D Printer Mendel Prusa Arduino: : Business, Industry & Science](https://www.amazon.co.uk/d/3D-Printing-Scanning/Anycubic-Graphic-Display-Controller-Module-Printer-Arduino/B0151H6XWK/ref=sr_1_sc_2?ie=UTF8&qid=1485384294&sr=8-2-spell&keywords=ramps+glcd)



If i connect this one to the cohesion, and won´t work either, could be my cohesion faulty (it´s unlikely, but just in case)?? I´m run out of ideas, it seems  for everyone is working power level and pot, except to me :(




---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 22:53*

That does look like the right GLCD, yes. Obviously I have no experience with that seller in particular.  I will await your feedback.


---
**Antonio Garcia** *January 25, 2017 22:57*

Ok, i´ve just ordered it,  I will let you know if it´s working....


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 23:03*

One more thing you can try, in that config section change pwm period from 20 to 200.


---
**Ariel Yahni (UniKpty)** *January 25, 2017 23:07*

**+Ray Kholodovsky**​ is the config suppose to have the default power off? Just looking for something else to try. 



**+Antonio Garcia**​ where does.k8nes made via commands or using LW?


---
**Antonio Garcia** *January 27, 2017 19:33*

**+Ray Kholodovsky** I´m back :), yesterday i was off, but now I´m worst than 2 days ago, now when i connect cohesion to my laptop, is not recognized by windows :(, i didn´t do anything. one day ago shutdown the computer, today i started to follow with some test, and cohesion is not recognized.... any suggestion?

I tried to connect it to all my usb ports, I restarted the computer and same issue :(


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 19:34*

What is "not recognized" exactly?  Like the computer has a popup that says not recognized? 

Is the board powered from 24v or off when you plug in USB?


---
**Antonio Garcia** *January 27, 2017 19:38*

it´s when you connect something usb to windows, and windows doesn´t recognized, then you go to devices, and you see a big x in that usb devices :).... The other day, card was recognised as smoothiboard...



I tried in both ways, 24v powered and just usb powered... same issue..



When i can find .bin firmware to burn it in the cohesion again?



Windows 10 is my so


---
**Antonio Garcia** *January 27, 2017 19:41*

this is gonna be a little bit harder than plug and play :)


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 19:42*

What are the LEDs on the board doing? 



Here's a dropbox link with the 2 config options that we've been dealing with.  Firmware file is the same in either one. [https://www.dropbox.com/sh/42l2ps3l53xq79t/AAAP9pLoaG9BDDwyrR3d85Sla?dl=0](https://www.dropbox.com/sh/42l2ps3l53xq79t/AAAP9pLoaG9BDDwyrR3d85Sla?dl=0)


---
**Antonio Garcia** *January 27, 2017 19:49*

just a green led, the other day there were more green leds on, if it is powered 24v, there is a red led on... I´m gonna try flash that bin again, is it just drop that file (.bin) in the SD Card, isn´t it?


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 19:52*

Just <b>a</b> green LED meaning just one?  Could you take a picture and explain exactly what one(s) are on?


---
**Antonio Garcia** *January 27, 2017 20:07*

I´m back **+Ray Kholodovsky**, after flashing the firmware is working back.... now I´m going to continue with my test, increase to 200 ms the pulse, uncomment power by default and so on...


---
**Antonio Garcia** *January 27, 2017 20:16*

**+Ray Kholodovsky** i increased to 200ms the pulse, and now looks the S command is doing something... but it still high, i mean, pot to 15mA, with this command G1 X20 S0.1 F600, pot measure almost 5mA, can i increase a little bit more the pulse??


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 20:19*

You're welcome to play around with that. I learned this trick from **+Wolfmanjm**.  I don't know if there are any limits or recommendations regarding that value. 

Also **+Don Kleinschnitz** recommended you turn the pot down, as the S value changes should be more apparent at lower pot value.


---
**Antonio Garcia** *January 27, 2017 20:34*

**+Ray Kholodovsky** **+Ariel Yahni** almost there :), my pulse is 400ms, and my pot in 15mA, and now at least i see results :)...

is it possible damage the laser, if the pulse is too high?? 

![images/25f755aa5b56a0f4b8b74fb624bb6082.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/25f755aa5b56a0f4b8b74fb624bb6082.jpeg)


---
**Wolfmanjm** *January 27, 2017 20:54*

pulse width is in us not ms on smoothie, 200us seems to be what is recommended. I have the pot set to under 10mA, to test engrave on cardboard I use 5mA. It is NOT linear by any means.


---
**Antonio Garcia** *January 27, 2017 20:58*

**+Wolfmanjm** completly agree 200us, my fault :), ok so i will decrease to 200us, and then i will play with the pot.

My idea was leave pot in 15mA, and then play in software with different values...

Thanks again **+Ray Kholodovsky** **+Ariel Yahni** and **+Wolfmanjm** for your help :)

![images/f1e7a032bedc307748f7d8b860e81505.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f1e7a032bedc307748f7d8b860e81505.jpeg)


---
**Don Kleinschnitz Jr.** *January 27, 2017 22:16*

All: I promise you..... 



The actual power that the LPS outputs is the PRODUCT of the POT setting and the PWM set on the "L" pin. The POT in these LPS control its power with its internal PWM just like the PWM from the smoothie. 



The actual power output vs the pot position including the max power depends on the state of the laser and that is constantly changing. 



If the laser is new a max pot position might produce 25-30 ma. That same laser 1000 hrs later may only draw 10ma. The max power will be reduced proportionally. 



You need the pot to adjust intensity and let the software PWM provide the full range of control for engraving granularity. 



The sweet spot for PWM period is 200 us.



If your not using "L", using a level shifter on "IN" then what you have is even less predictable,  good luck.



I have shared mounds of info that shows theoretically and actually this to be true.


---
**John Sturgess** *January 28, 2017 15:39*

I've got my laser firing via Bed- 2.5 and have the same issue, regardless or putting S0.5 or S1 i always get the same current on the ammeter. I'll increase the PWM period and see what happens.


---
**Don Kleinschnitz Jr.** *January 28, 2017 16:11*

**+John Sturgess** makes sure that your power is not turned up to high and washing out the engraving. 

You need to turn down the intensity with the pot.


---
**Antonio Garcia** *January 28, 2017 17:55*

**+John Sturgess** for me increase pwm was the trick...


---
**Antonio Garcia** *January 28, 2017 17:56*

I increased to 400 but now I set it up to 200 and the adjust the pot depending if engraving or cutting, but a least now S parameter is working...


---
**Don Kleinschnitz Jr.** *January 28, 2017 17:58*

**+Antonio Garcia** go figure :) :)


---
**Antonio Garcia** *January 28, 2017 18:07*

**+Don Kleinschnitz** 👍


---
**Don Kleinschnitz Jr.** *January 28, 2017 18:40*

**+Peter van der Walt**  Science when mated with engineering is often misinterpreted for OVER-ENGINEERING  ......

....... I just couldn't resist.


---
**Don Kleinschnitz Jr.** *January 28, 2017 19:22*

**+Peter van der Walt** I'm sure you saw the math analysis. 

Yes I agree and have been encouraging folks to try it with the right wiring.

You also need the pot to insure that you are not saturating the image to where the surface cannot see the shades. i.e even at small DF if the energy is large enough to burn full black the subtle changes in grey will not show.


---
*Imported from [Google+](https://plus.google.com/+AntonioGarciadeSoria/posts/BEDNi17exo6) &mdash; content and formatting may not be reliable*
