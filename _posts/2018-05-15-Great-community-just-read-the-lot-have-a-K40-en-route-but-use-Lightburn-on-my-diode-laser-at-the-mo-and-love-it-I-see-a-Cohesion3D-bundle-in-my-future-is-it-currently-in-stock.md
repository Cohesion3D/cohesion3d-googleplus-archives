---
layout: post
title: "Great community, just read the lot- have a K40 en route but use Lightburn on my diode laser at the mo and love it, I see a Cohesion3D bundle in my future - is it currently in stock ?"
date: May 15, 2018 20:00
category: "General Discussion"
author: "LA12 Sports Images LA12 Sports Images"
---
Great community, just read the lot- have a K40 en route but use Lightburn on my diode laser at the mo and love it, I see a Cohesion3D bundle in my future - is it currently in stock ?





**"LA12 Sports Images LA12 Sports Images"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 16, 2018 13:23*

There’s a few left, we’re going to run out very soon and be backordered for up to a few weeks .  It’ll ship next week when I’m back from Bay Area Maker Faire. 


---
**LA12 Sports Images LA12 Sports Images** *May 16, 2018 14:33*

Thanks Ray - pre orded from a EU source to simplify shopping and customs etc


---
*Imported from [Google+](https://plus.google.com/102441991153902344952/posts/KUg1UrmAyaF) &mdash; content and formatting may not be reliable*
