---
layout: post
title: "Just installed my Mini in the K40"
date: August 25, 2018 21:48
category: "C3D Mini Support"
author: "Lynn Roth"
---
Just installed my Mini in the K40.  having an issue with the homing on X.  This worked fine on the stock board.

Pictures include stock board and wiring.  New board wired, 2 pics of the probe status screen.



Boots up fine. GLCD is working.  

Y Homes Fine.

X Does not home. 



Debugging that I have done:

Went to Probe...Status.  X_min shows 1, Y_max shows 0 with not endstop depressed.  (I believe I should see X_min = 0)



When clicking the Y, Y_max changes to 1 as I would expect.

When clicking the X endstop,  x_min does not change.



I checked continuity on X endstop and it acts the same as y endstop.  Changes correctly when depressing it.



Next, tried to short (signal to ground) on the X_Min (separate) header.  Value does not change.  When shorting same pins on Y and Z, the status changes correctly.



I probably could rewire it and reconfigure and have it use the Z endstop pins, but I'd like to keep things as stock as possible on the wiring...







![images/ab702f1878d01341a6424891e766dcbd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ab702f1878d01341a6424891e766dcbd.jpeg)
![images/e333df9f64055b364ad22ff239dfa397.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e333df9f64055b364ad22ff239dfa397.jpeg)
![images/333b47e55ccf7f4892243046954acf25.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/333b47e55ccf7f4892243046954acf25.jpeg)
![images/63621191cf6a2562d124f13bc51c9703.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/63621191cf6a2562d124f13bc51c9703.jpeg)

**"Lynn Roth"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2018 22:22*

When you tested the x header sig - gnd short, did you have the white endstop cable unplugged from the Mini? 


---
**Lynn Roth** *August 25, 2018 22:59*

**+Ray Kholodovsky** yes. Just double checked again.  


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2018 23:46*

Have you played with the config file at all?  Could you try replacing the files on the card with clean files (dropbox link at bottom of install guide, firmware and config file from batch 3), then put the card back, turn on the board, and run this test again? 


---
**Lynn Roth** *August 26, 2018 00:00*

Fresh files from Dropbox.  Same issue.




---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2018 00:09*

Ok, the quick fix would be to run a jumper wire from SIG in the X Min header to SIG in one of the other ones like Z Min, then see if Z Min value in the status screen starts changing. 



If it does then just change the pin # in config.txt accordingly, I can guide you more on this. 


---
**Lynn Roth** *August 26, 2018 00:21*

I was able to jump x_min over to z_min and make the change in the config.txt

The status screen works now. ( both x and z trigger, because I didn't change the pin for z...)

However, now when I home, it bounces back and forth (vibrates?) and doesn't really get to the X home.  I'm still debugging this, and will see what more info I can provide. (after spending some time with the family)



Thanks for the help so far.


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2018 00:25*

It's meant to back off and home a second hit at a slower feedrate, many people think it's grinding just because of the sound and slow movement. 


---
**Lynn Roth** *August 26, 2018 01:32*

I don't think that's what this is.  The Y seems to do as you suggested, but the x keeps searching until I power it off. 

Video shows from power on to power off (denoted by the lights.) [https://photos.app.goo.gl/Bqksaerb9sDAUcog7](https://photos.app.goo.gl/Bqksaerb9sDAUcog7)

[photos.google.com - New video by Lynn Roth](https://photos.app.goo.gl/Bqksaerb9sDAUcog7)


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2018 01:34*

That does seem to be very different, yes. This does not look like anything related to a homing-related searching.  Does the axis move fine and the full extents when you jog X?


---
**Lynn Roth** *August 26, 2018 02:00*

That jumping was because I had the motor offset by a pin.  Getting all four pins on fixes that and it jogs and homes correctly with the x and z end stops jumpered and reconfigured.



Is this a bad trace or something on the board?



Thanks for the help so far.


---
**Ray Kholodovsky (Cohesion3D)** *August 26, 2018 02:08*

I don’t know. It’s the first time we’ve had a bad endstop port in over 1,000 boards. It’s part of the test procedure at the factory and there’s a protection circuit that should make it near impossible to kill after that. 


---
*Imported from [Google+](https://plus.google.com/+LynnRoth/posts/2USdGTmSBFf) &mdash; content and formatting may not be reliable*
