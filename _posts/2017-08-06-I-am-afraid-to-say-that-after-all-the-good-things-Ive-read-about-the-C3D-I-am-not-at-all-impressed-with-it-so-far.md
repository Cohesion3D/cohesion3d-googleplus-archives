---
layout: post
title: "I am afraid to say that after all the good things I've read about the C3D I am not at all impressed with it so far"
date: August 06, 2017 16:11
category: "C3D Mini Support"
author: "Kma Jack"
---
I am afraid to say that after all the good things I've read about the C3D I am not at all impressed with it so far.



With Ray Kholodovsky's help I got it working last night and was able to jog but as it was too late I couldn't do anything else. 



Today, I switched on the laptop and the machine after the laptop had booted up. When I tried to connect to the C3D, I found out that the com port had disappeared, no idea why, was there last night. 



Sometimes it even said that it could not find the firmware even though the SD card was plugged in. 



I then tried to reinstall the driver again but Windows wouldn't let me. When I checked Device manager, there was an Unknown something or other. I tried to reinstall the drivers from there but it kept telling me that the best driver was already installed. I then turned both the laptop and the K40 off, rebooted the laptop, turned the K40 on and all off a sudden it started working again. 



I then loaded a small file created in Inkskape and tried to engrave it, I couldn't believe the noise coming out of the K40. The X axis sounded like a car engine running without a drop of oil in it. Why is it making that noise? It never did with the board it came with. It's the X axis that's making the noise



First time round, I chose 100mm/s for cutting speed, I then chose 200 mm/s but it made no difference what so ever, it was just as slow. 



I have taken the C3D back out again and put the original board back in so that I can get some work done. I might give it another go when I have more time but in the meantime any help as to why the com port just disappears, why the X axis is so loud and why the speed change didn't make difference would be greatly appreciated.



Also a link to a good LaserWeb video/article would be good too. I did watch a few on Youtube but I need something for a complete beginner please



TIA



Eric





**"Kma Jack"**

---
---
**Ariel Yahni (UniKpty)** *August 06, 2017 16:38*

**+Kma Jack**​ changing the engine of a car is a job for an expert, most users aren't. That being said **+Ray Kholodovsky**​ has done a extremely good jog of making it super simple, yet reading documentation and understanding how an engine works makes the swap easier.



Regarding LaserWeb, have you seen my basic videos? I tried making it simple to get you started. If you still need help let me know.


---
**Kma Jack** *August 06, 2017 16:51*

Yes, I did Ariel, that's how I managed to get the engraving done but there is still a lot of things I need to find out about. I am a total newbie with lasers, only had mine for a few weeks so need something that explains step by step on how to set up and use LaserWeb. I have a 3D printer printer and built my own CNC a few years ago so not a complete noob to the mechanical side of things. 



I am using K40 Whisperer at the moment and used LaserDRW so do have some idea but LaserWeb is more complicated.



I saw your video when you was talking with someone but sorry to say it's a bit confusing as we don't hear what the other guy asks. 



I also need to know why the X axis is making such a racket, why the port keeps disappearing etc. 


---
**Ariel Yahni (UniKpty)** *August 06, 2017 18:29*

**+Kma Jack**​ I have 2 basic videos, no one talks and has captions. Look for 2 videos called LaserWeb basics LaserWeb Tutorials: 
{% include youtubePlayer.html id="playlist" %}
[http://www.youtube.com/playlist?list=PLCm8o-62Fu2nwrii2o1TTF5u1QC8zxgK0](http://www.youtube.com/playlist?list=PLCm8o-62Fu2nwrii2o1TTF5u1QC8zxgK0)


---
**Kma Jack** *August 06, 2017 18:58*

I didn't see those, will watch them.



Thanks


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 21:20*

Communication problems aren't with the Cohesion3D board nor the Smoothie firmware. That's Windows USB driver acting up on you and as you discovered, resetting Windows made it work again. Say hello the Windows playing field, grab a bat and start swinging. The M2 doesn't work nearly as fast as the Mini so it never ran into those issues. You can't reinstall the driver without removing it first, and again, that's Windows.



The louder noise you're hearing is a combination of things. The Mini does drive the steppers at a higher current, which allows for faster raster jobs. Unfortunately this causes the steppers to product more noise and the cabinet itself being built so flimsy is amplifying that. I put a heavy 8x12 piece of 3/4" thick aluminum plate on the bottom of my machine, strictly to stop it from rattling so much. It helps. 



Based on everything you wrote, some of it things I've experienced myself, there's nothing there that tells me there's anything amiss. It's doing what it's supposed to do, it's working as expected. Basically you put a V8 engine in the body of a 3 cylinder car. Something has to give ... But there are things you can do to minimize some of that noise. The Windows problem however, not so much. I run my system on Win10 and don't need a driver for that.


---
**Kma Jack** *August 06, 2017 22:07*

Ashley



I stopped using Windows over 10 years ago when XP was still new(ish) when my bro gave me his old Mac. It was like changing from a bicycle to a Ferrari, haven't looked back since. I  am not used to Windows any more and never worked with anything newer than XP. 

I'd connect my Mac to it if it wasn't in a different room to the laser. 



I want to eventually get/build an adjustable bed and a revolving bed for the K40 so a chunk of alu plate will be just in the way.



 If I reduce the current will it help reduce the rattling? 



What if I changed the drivers to the quiet ones, will that help? 








---
**Ashley M. Kirchner [Norym]** *August 06, 2017 22:35*

I wouldn't reduce current. That has the potential to introduce step skipping during engraving jobs.



Changing drivers won't help, it really is the steppers and the metal cabinet. Everything rattles on that thing. Just watch your #2 mirror when the machine is working and you'll see it bouncing up and down and shaking. The whole thing is cheaply made.



There are other options other than Windows (or a Mac). You could run it off a Raspberry Pi computer, or even smaller, off of a C.H.I.P. computer. But those are for the more advanced users.


---
**Jorge Robles** *August 07, 2017 05:47*

Why don't use the mac then? Does not need drivers (nor any limux machine too)?


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 05:51*

Think he stated the Mac is in a different room.


---
**Jorge Robles** *August 07, 2017 05:52*

Oh, ok sorry.


---
**Kma Jack** *August 07, 2017 11:27*

**+Ashley M. Kirchner**, I think I saw a post from +Ray Kolodovsky talking about turning the current down but didn't read all of it at the time as I was looking for something else. I tried finding it but couldn't. 



re: Raspberry Pi, is there any instructions how to use it with the C3D? 

I have an old Pi1 Model B, will that work or do I need a buy a new one?



What's a C.H.I.P computer?  



I did 3 years electronics course in college and have been using computers since 1983. I built my own CNC a few years ago and made a few circuits using the Picaxe chips so not a complete noob with technology so don't mind a challenge. I just want something that works and is reliable. I hate Windows but have no choice at the moment till I find something better


---
**Kma Jack** *August 07, 2017 11:28*

**+Jorge Robles**, will it work with Ubuntu? 


---
**Jorge Robles** *August 07, 2017 11:29*

Of course :)


---
**Kma Jack** *August 07, 2017 11:45*

Thanks, used Ubuntu before so I'm familiar with it


---
**Ashley M. Kirchner [Norym]** *August 07, 2017 17:36*

[getchip.com - Get C.H.I.P. and C.H.I.P. Pro - The Smarter Way to Build Smart Things](https://getchip.com/pages/chip)

I got the LW server side to work on that, as well as an rpi ([http://cncpro.co/index.php/23-documentation/installation/36-install-raspberry-pi](http://cncpro.co/index.php/23-documentation/installation/36-install-raspberry-pi)). Then just load up LW in a browser from a network connected machine. However, as I mentioned, this is a more advanced setup. If you want to go down that route, you're on your own. My recommendation is to stick with Windows, Mac, or the Linux app image. 


---
**Kma Jack** *August 07, 2017 19:25*

Thanks Ashley, I will have a look at it and see if it's anything I want to take on. 


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/XSGJeXANoyA) &mdash; content and formatting may not be reliable*
