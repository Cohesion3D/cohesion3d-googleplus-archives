---
layout: post
title: "C3D Mini Laser Hardware FAQ Thread ( READ THIS THREAD )"
date: January 18, 2017 22:35
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
C3D Mini Laser Hardware FAQ Thread (<b>READ THIS THREAD</b>).

Hey folks, 

Let's get the questions a bit more centralized.  If you have a Mini for laser and have a question about the board, please comment here.  Please read the entirety of this thread to see if your question has already been answered before commenting.  It might be cool to use the +1 a comment option to indicate that you have the same question or that a provided answer was helpful.  

However, let us not clutter this thread, so please continue to make your own post in this group to show your machine, installation, and wiring situation.  

A "I have this type of power supply and machine and wiring, what do I plug in where?" should go in its own post.  And that type of post also 100% needs pictures of all those things. 



Thanks for making support a bit more manageable.  

Best,

Ray





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Lance Ward** *February 26, 2017 20:16*

Has anyone attached a thermistor (water temp.) or a protus wheel flow measurement sensor to the C3D for monitoring? Is it possible?


---
**Ray Kholodovsky (Cohesion3D)** *February 26, 2017 20:40*

**+Lance Ward** 

Thoughts: 

First, many people are of the opinion that safety and interlock devices should be a separate system and not software dependent.

The C3D Mini is a 3d printer capable board and as such does have 2 thermistor inputs. However we use the CNC build of Smoothie firmware which disables the temperature stuff. In theory you could use the regular firmware and set up a temp switch to send a pause? command if a certain temp is exceeded. Can't say I recommend this, but it's something you can look into. 


---
**Lance Ward** *February 26, 2017 22:31*

**+Ray Kholodovsky** Gotcha...  I have a small digital thermometer module I'll mount in the lid and I can order a liquid flow switch from ebay and wire it in the laser interlock system (if there is one). Can't wait to get the C3D Mini!


---
**Lance Ward** *March 04, 2017 20:03*

I think I installed everything properly.. It was pretty simple and actually plug and play.  However,  I'm not able to get things up and running.  I plug the USB cable into laptop with LW3 installed and Win10 complains about not finding driver but lets me talk to SD card on C3D board.   LW3 shows no available ports.  Also, no activity on LCD display (I've tried it with and without LCD installed).


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:06*

So for win 10 you did NOT install drivers, correct? 


---
**Lance Ward** *March 04, 2017 20:13*

Win 10 went through the USB detection and tried to find drivers but reports no drivers found.  For what it's worth, on C3D board, I have a solid red led on top with two bottom green LEDs solid, two middle LEDs flashing and top 2 green LEDs solid.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:15*

Yep, all lights solid except middle 2 flashing. Hang on I have a vid. 


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:17*

Led behavior: [photos.google.com - New video by Ray Kholodovsky (Cohesion3D)](https://goo.gl/photos/uii7W11iYutte4By9)


---
**Lance Ward** *March 04, 2017 20:18*

I have solid red LED on top.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:19*

Yep, that's for 24v power. That's fine. 

Are you seeing the drive show up in Computer like in my vid? 


---
**Lance Ward** *March 04, 2017 20:23*

My old eyes are not what they used to be...  bottom Green LED solid on, #2&3 Green LED flashing, #4&5 Green LED On solid.  if powered by USB, these are only LEDs.  As soon as I turn on K40 power, I get the red sold LED also.


---
**Lance Ward** *March 04, 2017 20:24*

Yes, the SD card shows up as drive in file explorer.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:25*

Yes, that's fine. 2 & 3 flashing is what we care about. 


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:25*

And can you show me a screenshot of the contents of the drive?


---
**Lance Ward** *March 04, 2017 20:28*

I can just tell you.  It's two files:  config.txt & FIRMWARE.CUR. That's it.


---
**Lance Ward** *March 04, 2017 20:31*

![images/19f67d2caae9dde91d11c6c8848e48c5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/19f67d2caae9dde91d11c6c8848e48c5.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:31*

Perfect.  Thanks. 

Now you also said no activity on the LCD... Got a pic of that setup? 


---
**Lance Ward** *March 04, 2017 20:33*

Picture of what exactly?  LCD screen backlight is lit up but screen completely blank.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:40*

I'll cut to a possible case: let's rule out that the config is not corrupted.  Delete everything from the sd card, put the config file back on from this dropbox link, and power cycle the board or press reset button on the board (yellow button top left corner). 



[dropbox.com - C3D Mini Laser Batch 2 - Provided Config](https://www.dropbox.com/sh/4ai2p4q6l3cez26/AABvKZq5sAgypU2yxnPHgYrua?dl=0)

See if that changes anything behavior. 


---
**Lance Ward** *March 04, 2017 20:40*

I'm going to try my old laptop (win 7)..  Stand by.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:41*

For Win 7 you will need to install drivers, please see here for link: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)


---
**Lance Ward** *March 04, 2017 20:54*

Win7 works with driver.  Head "Homes" fine but still nothing on display.  I tried downloading your dropbox files for SD card but they are saving as .html (and different file sizes) files for some reason.


---
**Lance Ward** *March 04, 2017 22:33*

I think there's some issue with some embedded system development drivers I already have installed on the WIN10 machine.  The WIN 7 machine works fine and I'll just use that for now.  However, the LCD display still does not work.  Just a blank display.  Adjusted contrast but still nothing.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 22:34*

So, picture of front of lcd, back of lcd without cables please. 


---
**Lance Ward** *March 04, 2017 22:50*

![images/810484f4fe5fe4c87f7e67c6bc9d69f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/810484f4fe5fe4c87f7e67c6bc9d69f0.jpeg)


---
**Lance Ward** *March 04, 2017 22:51*

![images/5e71abd33dff6451bfcbdb0c7f0164c9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5e71abd33dff6451bfcbdb0c7f0164c9.jpeg)


---
**Lance Ward** *March 04, 2017 22:53*

I added the resistor at top of board in attempt to improve contrast.  It worked as I can clearly see pixels in display now but still all blank.  With or without resistor, still blank.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 23:13*

I've never heard of putting a resistor there for contrast, one just fiddles with that pot on the lower left.  

Anyways, this is how the board gets mounted and EXP1 goes to the left and EXP2 goes to the middle.  You can see my EXP1 wire has the blue zip tie indicating it. 

![images/cc4f5df04ac6afe494cdca29720ee252.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc4f5df04ac6afe494cdca29720ee252.jpeg)


---
**Lance Ward** *March 04, 2017 23:20*

Yes, that's exactly what I have.  If I press the knob, I get a beep from speaker on LCD but display is blank.  Do I need to flash firmware in this display?  I bought this LCD from Amazon ([https://www.amazon.com/OSOYOO-Graphic-Display-Controller-connector/dp/B00J1X4MFQ](https://www.amazon.com/OSOYOO-Graphic-Display-Controller-connector/dp/B00J1X4MFQ)) One of the reviews said FW had to be loaded, another said the cables had to be reversed, and another had a fairly elaborate description of reversing only one cable.  I'm stumped.

[amazon.com - Amazon.com: OSOYOO 12864 LCD Graphic Smart Display Controller module with connector adapter & cable for RepRap RAMPS 1.4 3D Printer kit Arduino Mega 2560 R3 Shield: Industrial & Scientific](https://www.amazon.com/OSOYOO-Graphic-Display-Controller-connector/dp/B00J1X4MFQ)


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 23:22*

Haven't heard of firmware having to be loaded. Based on the pic of the back of the display the connectors are in the "proper" orientation. You can try flipping the cables at the GLCD end. And again, fiddle with contrast pot - clockwise, never heard of needing that resistor you put in. 


---
**Lance Ward** *March 04, 2017 23:27*

[forums.reprap.org - Controller LCD contrast issue](http://forums.reprap.org/read.php?13,448609)


---
**Lance Ward** *March 04, 2017 23:28*

I've had the contrast all over the place, no joy.  I tried flipping the cables, nothing; not even backlight.


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 23:31*

I'd RMA it and have Amazon ship you a replacement. As you said, these things are all over the place, which is why I recommend people get them on Amazon. 


---
**Lance Ward** *March 05, 2017 00:11*

It'll be heading back to Amazon Monday.  I already ordered a new one from a different Amazon vendor and it should be here Monday.  I'll let you know.


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 00:12*

Sweet. Yeah keep me posted on that one. 


---
**Lance Ward** *March 06, 2017 16:37*

The replacement LCD works.  Buying from Amazon was a good tip!  Thanks for the help!


---
**Lance Ward** *March 06, 2017 22:51*

Does anyone know how to reverse the direction of the encoder sensing?  Mine is operating backwards (from my preference anyway) and decreasing for clockwise while increasing for counter-clockwise. Is it just swapping the #panel.up_button_pin  and #panel.down_button_pin pins in the config.txt?  


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 22:53*

Here's what you want.  Change the 25 and 26 with each other. 



panel.encoder_a_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         3.26!^ 



As another note if you see the # at the beginning of a line that is commented and not in use.  All part of the smoothie config magic. 



Save, safely eject from windows, reset the board. 


---
**Lance Ward** *March 08, 2017 21:44*

Does the standard installation of the C3D Mini in the K40 allow for laser power modulation under control of LW3?  How is this done; line widths, line color?


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 21:47*

Yes, pwm control and LaserWeb is the whole point of the upgrade. 

LW is actual CAM, you set up the job parameters such as what you said in there. 


---
**Lance Ward** *March 08, 2017 22:37*

Thanks but does the PWM exist in a vanilla install? I mean.. I plugged my C3D Mini in and it appears to work nominally.  Figuring out how to best utilize it is another matter. I see there is a "laser power" setting in the cam section of LW3 but it doesn't seem to do anything.  My  only control of laser power appears to be from the knob on the panel.  I would actually like to  control the power dynamically in the design using colors, line width, etc.  Apologies if this is not strictly C3D question but I'm still not 100% with it yet.


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 22:44*

This is completely valid.  PWM tuning does take some work.  

Things to do:

Set the pot to 10mA.



Try running 

G1 X10 S0.4 F600

G1 X20 S0.8 F600



you should have 2 different power level burns.  If not.... Possibly need to set the pwm period in config.txt from 200 to 400. Save, eject, and reset the board. 


---
**Josh Speer** *March 10, 2017 18:26*

When I have the homing set to G28.2 it homes the laser towards the front of the case, however the optical switch is in the back. Is there some reason I don't want it to home in the back?




---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 18:29*

You do want it to home to the back.  Try flipping the Y motor connector at the board.  Power off before doing this. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/XRiVunGznKr) &mdash; content and formatting may not be reliable*
