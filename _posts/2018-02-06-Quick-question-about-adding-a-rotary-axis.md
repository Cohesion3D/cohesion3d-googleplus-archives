---
layout: post
title: "Quick question about adding a rotary axis"
date: February 06, 2018 14:08
category: "General Discussion"
author: "John Warren"
---
Quick question about adding a rotary axis.



As I get more and more frustrated with the stock controller and closer and closer to swapping in the c3d mini I noticed that all instructions say that when adding a rotary you should add a external stepper driver.  



Is this necessary with a rotary that uses a nema 17 motor?  





**"John Warren"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2018 14:56*

Depends, sometimes the onboard-style A4988 drivers can provide enough current and sometimes they cannot. Typically the Lightobjects items are more power hungry but some DIY options work fine. 

You should get the separate PSU though. 


---
**E Caswell** *February 06, 2018 17:30*

I have made my own rotary with a standard nema17 stepper. Use directly off the C3D board with no other hardware.  Search for rotary on here you should find it, along with drawings for what I used to make it. Simple stuff nothing special. I agree with **+Ray Kholodovsky** if using a bigger unit then you will need an external driver.


---
*Imported from [Google+](https://plus.google.com/114420019102377897867/posts/D9ubyvptvoe) &mdash; content and formatting may not be reliable*
