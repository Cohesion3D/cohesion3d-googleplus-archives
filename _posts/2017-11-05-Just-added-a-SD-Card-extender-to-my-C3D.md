---
layout: post
title: "Just added a SD Card extender to my C3D"
date: November 05, 2017 21:38
category: "General Discussion"
author: "Bill Keeter"
---
Just added a SD Card extender to my C3D. Grabbed a piece of adhesive tape and my K40 is all set.



Picked it up here on Amazon: [https://www.amazon.com/gp/product/B01D9JIUU0/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01D9JIUU0/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)





![images/1e54672b58de54f71402504be3d3cb50.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e54672b58de54f71402504be3d3cb50.jpeg)



**"Bill Keeter"**

---
---
**Don Kleinschnitz Jr.** *November 06, 2017 00:17*

I like that :).... in the amazon basket!


---
**Andy Shilling** *November 06, 2017 10:38*

Snap, arriving tomorrow. 


---
**Douglas Pearless** *November 09, 2017 05:00*

Interested to see how that performs as I would expect all sorts of RFI issues with a simple cable extension.


---
**Bill Keeter** *November 09, 2017 16:47*

**+Douglas Pearless** It was a concern of mine too, but for just a few bucks I figured it was worth a shot. I've used it so far for a half dozen jobs. No issues...  now just waiting on a vendor to send me more acrylic to cut. I'll add comments if I notice any problems.



One thing i'm curious about, maybe **+Ray Kholodovsky** knows. Does the C3D run the gcode from the file on the SD card? or does it load it into memory and run it internally?


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:48*

It's running from the card. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/Hy4RU4CqzL8) &mdash; content and formatting may not be reliable*
