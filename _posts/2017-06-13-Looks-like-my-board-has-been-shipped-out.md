---
layout: post
title: "Looks like my board has been shipped out"
date: June 13, 2017 05:04
category: "C3D Mini Support"
author: "Bromide Ligand"
---
Looks like my board has been shipped out. Is it possible to add 12v fans to this board? 0.02amps each. I want to add 2 -3 . One for intake, 1 for the board and 1 for the power supply.





**"Bromide Ligand"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 05:12*

The board runs at the 24v the laser runs at. 12v is not available. 

You can, get a 24v --> 12v regulator or get a separate 12v psu/ power source. Either way, the board is not involved. 

Also the laser power supply isn't that strong so adding more stuff to it can kill it. Separate power supply is recommended. 


---
**Bromide Ligand** *June 13, 2017 06:38*

Oh! thanks.


---
**Claudio Prezzi** *June 13, 2017 08:29*

You could connect two 12V fans in series and connect them to 24V ;) (20mA shouldn't make any difference)


---
**Bromide Ligand** *June 13, 2017 08:55*

I will just use a different psu and connect it to pump/fans, laser guide and led's :)






---
**Bromide Ligand** *June 13, 2017 09:34*

**+Ray Kholodovsky** Another question, is it possible to connect a Bluetooth/ wifi module to this for printing wirelessly ?


---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 12:42*

I wouldn't. 


---
*Imported from [Google+](https://plus.google.com/108393288879485897084/posts/FextnzMRYFZ) &mdash; content and formatting may not be reliable*
