---
layout: post
title: "On an EleksMaker A5 Pro with a maximum print area of 13cm x 20cm what would the setting need to be set as in Laser Web4 with a C3D board as the brains of the unit and without installing endstops"
date: December 17, 2017 20:16
category: "General Discussion"
author: "Jonathan Davis (Leo Lion)"
---
On an EleksMaker A5 Pro with a maximum print area of 13cm x 20cm what would the setting need to be set as in Laser Web4 with a C3D board as the brains of the unit and without installing endstops. 





**"Jonathan Davis (Leo Lion)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 18, 2017 18:27*

I'm not sure what you are asking. Can you clarify the exact question? 


---
**Jonathan Davis (Leo Lion)** *December 23, 2017 01:41*

**+Ray Kholodovsky**  what do I need to enter settings-wise for the laser web program to work with my system? 


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/hZmoYXbgBYm) &mdash; content and formatting may not be reliable*
