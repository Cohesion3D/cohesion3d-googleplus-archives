---
layout: post
title: "Looking for a little specific direction on how to apply a C3D Mini to a new laser build"
date: February 09, 2018 20:55
category: "C3D Mini Support"
author: "Tim White"
---
Looking for a little specific direction on how to apply a C3D Mini to a new laser build.  I'm interested in learning the following:

- Are there specifications for the mating connectors on the C3D Mini board?  I'd be buying from the usual sources (DigiKey, Mouser, etc).

- Has anyone put together a suggested wiring diagram for connecting the C3D Mini to a 50 watt CO2 laser power supply?

- Is it possible to configure the C3D board to run two steppers in parallel?



Background:  I'm in the process of building a laser cutter/engraver from scratch.  I looked at the K40, but there was too much risk in getting a lemon, and I wanted something with a larger engraving area anyway.  I'm anticipating using the OpenBuilds ACRO system as the x/y motion platform, which uses two steppers to actuate the y axis.  I'm interested to know whether there's a software configuration available for that, or if I should put the motors in parallel and up the current on the driver.



Given the fact that this is a new build, I'll be building my cabling from scratch.  My ideal scenario would be to get native connectors to terminate my cabling that match the C3D board.



I'm also interested in understanding the connections between the C3D board and the laser tube power supply.  I see that there's a PWM input to the power supply; it's my impression that the K40 comes with a power knob that either provides a voltage between 0-5V, or others are using PWM.  Do you need both inputs to the power supply, or does the C3D board provide the input via the connector on the board?  The PS I'm considering is this one:

[https://www.ebay.com/itm/50W-CO2-Laser-Power-Supply-for-Laser-Engraver-Cutter-Machine-MYJG-50W-110V-220V-/122586930144](https://www.ebay.com/itm/50W-CO2-Laser-Power-Supply-for-Laser-Engraver-Cutter-Machine-MYJG-50W-110V-220V-/122586930144)



My thanks for any input.



TW





**"Tim White"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 09, 2018 21:02*

"Wild Bill" over on the facebook group has done an Acro build and posted quite the detailed account:



[facebook.com - Log into Facebook &#x7c; Facebook](https://www.facebook.com/groups/1045892858887191/search/?query=acro)



Start there. 


---
**Robin Sieders** *February 10, 2018 03:53*

I can't speak to most of your questions, but having just completed my own custom build with a c3d mini board, I can confirm that you can run 2 steppers in parallel off of one connection. My Y axis is driven by 2 Nema 17's that I paired together using a screw terminal wiring block, then connected to the y axis connection on the cohesion 3d. Just need to remember to reverse the wires for one of the steppers to mirror its output.


---
**Tim White** *February 10, 2018 20:04*

**+Robin Sieders** Good feedback, and a good reminder on reversing the wiring.


---
**Wild Bill** *February 11, 2018 01:45*

For the motor's my machine has dual Y motor's as well and I did the same with both motor's on the same driver - with one set of wires reversed so the motor's run in opposite directions.



Looking at the power supply it does not have the POT connections like the K40 just a TTL input to control the laser. The only thing not clear from the documentation is if the TTL signal is active low or high like the diode lasers.


---
**Andy Shilling** *February 13, 2018 22:13*

**+Tim White** that Lps will be fine, I've just fitted the 60w version and it's the same connections. **+Don Kleinschnitz**  gave me a wiring diagram, I'm sure if you ask nicely he will repost it here for you. 


---
**Don Kleinschnitz Jr.** *February 14, 2018 16:55*

I just did one for a 150W but same connections.



H= the T in the drawing below



H is high true

L= grnd true

![images/82a7c8741fb386927f12a4b17d9c8044.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82a7c8741fb386927f12a4b17d9c8044.jpeg)


---
*Imported from [Google+](https://plus.google.com/107810046539384552772/posts/MczbCjRdZ7s) &mdash; content and formatting may not be reliable*
