---
layout: post
title: "I took the leap. I am now the proud owner of a K40"
date: January 13, 2018 06:48
category: "K40 and other Lasers"
author: "Chuck Burgess"
---
I took the leap. I am now the proud owner of a K40. Here is a photo of my controller. 

Fireware Ver: 2016.10.05 

Model: 6C6879-LASER-M2:9



Will the Cohesion3D controller hardware work for my machine? Please say yes, I use a Mac and really want to use gcode!

![images/299f6579d02b5451da3952430d9c73d1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/299f6579d02b5451da3952430d9c73d1.jpeg)



**"Chuck Burgess"**

---
---
**LightBurn Software** *January 13, 2018 07:51*

Yes, this controller should be a simple cable-swap with a Cohesion3D board. Took us longer to get at the bolts for the mounting plate.


---
**LightBurn Software** *January 13, 2018 07:52*

**+Ray Kholodovsky** should chime in just to be sure.


---
**Chuck Burgess** *January 13, 2018 07:55*

I am thinking it should also. It looks very similar to some of the phots in the documentation. 


---
**LightBurn Software** *January 13, 2018 07:57*

I just double checked mine - identical, except for the 2016 (mine’s a 2017) but all the cable connectors and placements are identical.


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2018 12:29*

My machine has the same exact wiring. Check the guide at Cohesion3D.com/start 


---
**Bill Knopp** *January 25, 2018 01:23*

So, to be clear, I have the same controller in my laser, and the power laser power is simply set by a 0-5V signal from the pot on the top of the laser.  that signal wire is connected to the FET1Bed (-) pin on the Cohesion Mini board, and that outputs a signal (0-5V) that controls the laser power correct?  I just want to be sure before I place my order.


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2018 01:26*

In short, the pot stays wired exactly and you only transfer the exact wires from the green board to the C3D Mini.  It's simple. 



You can check the install guide here: [https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)


---
**Bill Knopp** *January 25, 2018 02:43*

**+Ray Kholodovsky** Thanks, it looks simple, but how does the C3D board control the power of the laser?  The original board does not have a power output signal, as it is incapable of producing that, so where does the power signal to the power supply come from?  I am really excited to find an affordable board that will let software control the power, and thus let me get a better handle on the power setting used for different materials, as well as get the consistency that goes with that.  Thanks so much for this!


---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2018 02:44*

C3D pulses the L wire which is one of the ones in the 4 pin large power plug. 


---
**Bill Knopp** *January 25, 2018 03:16*

Thanks for the clarification!  Super support, answering so quickly.  I have placed my order, and am excited to upgrade my laser.  It is not a K40, but uses the same board, so I assume it will just be a simple change in the SD Card for the larger size.  Thanks again!




---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2018 03:18*

What machine is it? There are quite a few with the M2 board. Make a new post with pics and details :) 


---
**Bill Knopp** *February 08, 2018 02:52*

**+Ray Kholodovsky** It is a machine that I purchased from ASC365 in Canada.  No brand on it - Cut area of about 30cm by 50cm, but can be more in either direction, just not both because of the mounting hardware for the bed.  I will start a new post with pics.


---
*Imported from [Google+](https://plus.google.com/+ChuckBurgess1/posts/5yW5j4vrEsb) &mdash; content and formatting may not be reliable*
