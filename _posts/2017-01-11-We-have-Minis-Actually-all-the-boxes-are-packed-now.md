---
layout: post
title: "We have Mini's. Actually all the boxes are packed now"
date: January 11, 2017 23:58
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We have Mini's. Actually all the boxes are packed now. Will start shipping out tomorrow. Keep an eye out for shipment notification emails over the next few days. 



![images/5807f003d0b6fd5fa63cb59fe19a796d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5807f003d0b6fd5fa63cb59fe19a796d.jpeg)
![images/b3d2b34c772dc0b0c28e8be3c4c14032.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b3d2b34c772dc0b0c28e8be3c4c14032.jpeg)
![images/34484db5ccafd8ee256e61bbbe1868b2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/34484db5ccafd8ee256e61bbbe1868b2.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 23:59*

**+Alex Krause** and no, the 200 drivers that came the other day were not enough, had to dip into additional reserves :)


---
**David Komando** *January 12, 2017 00:44*

Can't wait! 


---
**Ned Hill** *January 12, 2017 01:05*

Excellent! 


---
**Coherent** *January 12, 2017 01:48*

I tried enlarging your photo.. couldn't read the labels. Which one's mine? lol

Thanks for the update.




---
**Richard Vowles** *January 12, 2017 07:28*

Got mine!


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2017 07:29*

Yep, generating shipping labels at 2 am :)


---
**Andy Shilling** *January 12, 2017 10:46*

Job we done there mate. Looking forward to receiving mine.


---
**Bill Keeter** *January 12, 2017 16:36*

great news. Guess I should go look at installing LaserWeb now. :D


---
**Kris Sturgess** *January 13, 2017 17:55*

Awesome. Received tracking # today. Guess I'll start 3D printing a case for it.


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 17:56*

**+Kris Sturgess** we have a CAD model for the board.... Give me your contact info or email me and I'll send it over.  It's not 100% yet so I'm not publishing it just yet.


---
**Kris Sturgess** *January 13, 2017 17:57*

Lumpy692002@[hotmail.com](http://hotmail.com) 

There was one on the website? 


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 17:58*

There are documentations on [cohesion3d.freshdesk.com](http://cohesion3d.freshdesk.com) but I have not published the Mini nor Laser Instructions just yet.  You can see what we did with ReMix though.


---
**Kris Sturgess** *January 13, 2017 18:00*

Ahh... I see.. 10-4


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Luq3zy1au6M) &mdash; content and formatting may not be reliable*
