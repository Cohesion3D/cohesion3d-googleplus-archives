---
layout: post
title: "Hi all, I'm trying to get my Cohesion3D & Laserweb working for the first time with my K40 and having some issues"
date: October 13, 2017 10:57
category: "C3D Mini Support"
author: "Deren Ash"
---
Hi all, I'm trying to get my Cohesion3D & Laserweb working for the first time with my K40 and having some issues.  I hooked everything up as described here, and all my connections look exactly the same as the pics here:

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



In Laserweb, I entered the settings as in here:

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration](https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration)



So, here are my issues:

1. The laser won't fire.  It won't fire if I click Laser Test.  I loaded an SVG file that is a simple circle.  It will trace the circle, but the laser never fires.  The laser does fire when I press the test button on the machine itself.  I did set the tool test power setting to 100% and tool test duration to 500ms.

2. When going around the circle, it stops for a moment at several points.

3. Now when I try to run a job, and just smashes against the axes at home.  It homes fine.  At some point when I was messing around trying to get 1 and 2 to work, I click the "set zero" button, it moved the machine outline (with "show machine" switched on) below the X axis, and no matter where I position the circle from the SVG it just slams against the axes at home position.  This has got to be some simple setting...but I can't seem to find it and I can't mess with 1 or 2 until I figure out this one.



Thanks in advance for your help!





**"Deren Ash"**

---
---
**Ariel Yahni (UniKpty)** *October 13, 2017 14:23*

1- I'm not sure laser test works 9n smoothie

2- most likely your circle is composed of smaller sections. Try uniting the path 

3- go to settings / machine and turn off Show Machine


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2017 22:11*

Deren! You were my first order when I opened the web shop. We can confirm that you have an original Mini by checking for "v2.2" notation on the back of the board. 



Try sending an M3 command, does that turn the laser on? Be ready to send the M5 or hit the enable switch to turn it back off. 



We'll need to move 1 wire and verify you have the new config file to get you on the new wiring plan. 



So let's start with making sure you indeed have an original board, and then I'll dig up the wiring plan for you. We've made a few small tweaks to the board design since last year. :)


---
**Deren Ash** *October 13, 2017 22:30*

**+Ariel Yahni** 3 seems to have solved itself, after reopening Laserweb (even though of course I did that before).  Maybe it was from quitting Chrome too while Laserweb was closed by coincidence.  On 2, I make a circle with AI which must be a bit smarter about exporting svg than the stupid old version of CorelDraw I was using to run CorelLaser.


---
**Deren Ash** *October 13, 2017 22:34*

**+Ray Kholodovsky** Yes it does indeed say 2.2 on the back.  I sent G3 and it didn't do anything.  And yes, this is that same board and I regret not installing it sooner!  I'll spare you all from the excuses.


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2017 22:35*

Once more please, 

M3 then G1 X10 S0.6 F600 



M not G for M3 


---
**Deren Ash** *October 13, 2017 22:40*

Oops, yeah that was I typo/brainfart in my comment, I sent M3.  I sent M3 again, then G1 X10 S0.6 F600, which sent the head to 10 on the X axis.


---
**Ray Kholodovsky (Cohesion3D)** *October 13, 2017 22:41*

And it did that without firing? Ok. 

Please send me some pics of your wiring and the k40 power supply and I'll let you know the changes we have to make. 


---
**Deren Ash** *October 13, 2017 22:50*

Yes, it did that without firing.

Here's the C3D wiring:

![images/793e58467cfed702950d4910a575afba.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/793e58467cfed702950d4910a575afba.jpeg)


---
**Deren Ash** *October 13, 2017 22:50*

Power supply:

![images/b95d1e098ac35a990369337fb8b3a010.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b95d1e098ac35a990369337fb8b3a010.jpeg)


---
**Deren Ash** *October 13, 2017 22:50*

Original Board before pic:



![images/1e0436d9d640095e17e1afa0917d8ca0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1e0436d9d640095e17e1afa0917d8ca0.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 15, 2017 00:25*

Hi Deren, 

Please identify the wire "L".  It is one of the 4 blue ones in that large power connector.  You can reference the Mini pinout diagram (Documentation site) to see where it connects on the board, and if you trace that wire, it should be the rightmost one on the K40 PSU side.  You'll need to disconnect that wire from the plug on the Mini end (aka cut the wire), and connect it the 2.5- screw terminal. 



This is the outcome we want: [plus.google.com - Photo - Google+](https://plus.google.com/photos/photo/115621505231890078993/6378666310719605618?icm=false&iso=true&sqid=116261877707124667493&ssid=b9e1e12f-e61c-486e-bb2b-ff208e4d930c)



At the same time, please get the new config and firmware files (there is a dropbox link at the bottom of the install instructions), delete the old files off the card, and then put the new files on the card. Then put the card back in the board and power up.  



Set the pot to roughly 10mA and send that G1 line as discussed, the laser should fire as the head moves. 


---
**Deren Ash** *October 17, 2017 21:56*

Mahalo Ray!  The laser is firing now.


---
*Imported from [Google+](https://plus.google.com/109581708182277964430/posts/iJCGB57ZCwS) &mdash; content and formatting may not be reliable*
