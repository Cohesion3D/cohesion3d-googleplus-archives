---
layout: post
title: "As I say, we do a full functional test on each board before shipping"
date: December 04, 2016 04:47
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
As I say, we do a full functional test on each board before shipping. 

This entails: 

Burn micro sd card with firmware and config, flash firmware to board. 

Insert A4988 drivers to board, wire up motors, power and GLCD. 

Get connected to computer. Test for motion in all axes, and both directions. Verify thermistors are reading, mosfets turn on, and also close each endstop contact and use M119 to confirm it is read. 

Flashing wifi module is a separate station. 

![images/4243feecd24a4746558140b7b3b4a8e6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4243feecd24a4746558140b7b3b4a8e6.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**René Jurack** *December 11, 2016 16:15*

I see, moFET3 is powered on your board aswell :D


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 00:10*

That's a weak pull down resistor and the pin being not defined in config. If you define the pin, it turns off. Otherwise, if you join up any load to it (larger than an LED) it also turns off. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/GKg7ipgob4R) &mdash; content and formatting may not be reliable*
