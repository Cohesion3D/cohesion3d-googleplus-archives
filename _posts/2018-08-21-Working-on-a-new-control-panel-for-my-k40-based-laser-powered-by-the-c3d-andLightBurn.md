---
layout: post
title: "Working on a new control panel for my k40 based laser powered by the c3d andLightBurn"
date: August 21, 2018 02:51
category: "K40 and other Lasers"
author: "Trevor Hinze"
---
Working on a new control panel for my k40 based laser powered by the c3d andLightBurn.  It is uv printed on the back side of clear lexan sheet.  Cant wait to pop in all of the buttons and gauges to see how it will look.



![images/38bee1c362091de92d7bc2968924e84d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/38bee1c362091de92d7bc2968924e84d.jpeg)
![images/10903b088d0cbc014a9ce14244f62519.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/10903b088d0cbc014a9ce14244f62519.jpeg)

**"Trevor Hinze"**

---
---
**Ciaran Whelan** *August 21, 2018 03:51*

Looks awesome. How do you UV print?


---
**Trevor Hinze** *August 21, 2018 04:36*

**+Ciaran Whelan** a printer which utilizes uv cured polymer ink.  It is cured with a high intensity uv light as soo as it is sprayed by the printhead.


---
**Ciaran Whelan** *August 21, 2018 07:59*

**+Trevor Hinze** I'm assuming this is an expensive process to build as DIY?  I've never heard of it before. 


---
**Trevor Hinze** *August 21, 2018 13:28*

**+Ciaran Whelan** you are correct, the process itself is as affordable as any inkjet printing, perhaps more so actually, but the hardware is not cheap.  I spent right at 2k to build mine with lots of scrounging and creativity.  Commercial units start at around 10k


---
**Joe Alexander** *August 21, 2018 19:30*

you built your own UV printer? color me impressed. I assume the light source is either shielded from hitting the printhead directly or toggled as needed? and what wavelength is your light source? I only ask as I make UV ink for a living :)


---
**Trevor Hinze** *August 21, 2018 22:39*

**+Joe Alexander** , thanks for the kind words.  There is a detailed build thread of my printer over at open [dtg.com](http://dtg.com), but for some reason when I place a link here it redorects to some other build thread.  I will try to get a link posted from a p.c. rather than my phone.



I use Nazdar uv ink that cures at 395 to 405 nm if my memory serves correctly. But I could be off, it is cured by a water cooled 60w led light block.  

[opendtg.com - UV LED PRINTER - Epson 3800 - Open DTG - Build your own Direct to Garment Printer](https://www.opendtg.com/viewtopic.php?t=482)


---
**Trevor Hinze** *August 22, 2018 04:31*

**+Trevor Hinze** here is a link to the printer build thread if by some odd chance you are interested.



[opendtg.com - My Epson 3800 UV printer conversion - Open DTG - Build your own Direct to Garment Printer](https://www.opendtg.com/viewtopic.php?t=1044)


---
*Imported from [Google+](https://plus.google.com/106981345031249926577/posts/bQMBVuPYpgm) &mdash; content and formatting may not be reliable*
