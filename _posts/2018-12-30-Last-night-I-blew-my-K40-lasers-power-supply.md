---
layout: post
title: "Last night I blew my K40 laser's power supply"
date: December 30, 2018 15:02
category: "K40 and other Lasers"
author: "Jason Dentler"
---
Last night I blew my K40 laser's power supply. Is the C3D laser board compatible with LightObject power supplies? 



[https://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-P71.aspx](https://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-P71.aspx)





**"Jason Dentler"**

---
---
**Don Kleinschnitz Jr.** *December 30, 2018 15:25*

Post a picture of your current LPS. 

Likely this supply will work but require rewiring.

A direct replacement supply from eBay or Amazon is under $100??


---
**Jason Dentler** *December 30, 2018 16:34*

**+Don Kleinschnitz Jr.** Thanks! This is the busted one. Assuming it's not just a blown fuse or loose wire, I want something better - preferably with a warranty like the one from LightObject.



I'm still running the original M2 Nano. The C3D laser board should be here this week  (ordered early last week)

![images/63a3ccdd8026ab671d698b7da17dfaed.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/63a3ccdd8026ab671d698b7da17dfaed.jpeg)


---
**Don Kleinschnitz Jr.** *December 30, 2018 17:18*

Does the LED on the supply turn on?

Does the laser fire from the test button down on the supply?



FYI; I would expect any of these supplies to last more than 6 mo.

I think LO is creating a perception of reliability by giving a warranty that is inside the usual life of this supply's design. 



You can get two of these supplies: 

[https://www.ebay.com/itm/40W-50W-PSU-CO2-Laser-Power-Supply-for-CO2-Laser-Engraver-Cutter-USA-Stock-/122514312988](https://www.ebay.com/itm/40W-50W-PSU-CO2-Laser-Power-Supply-for-CO2-Laser-Engraver-Cutter-USA-Stock-/122514312988)



For < $200 and they would not require rewiring. 

[ebay.com](http://ebay.com) - 40W 50W PSU CO2 Laser Power Supply for CO2 Laser Engraver Cutter USA Stock 756910778910 | eBay


---
**Jason Dentler** *December 30, 2018 17:30*

No LED light on the PS. No LED in the power switch of the panel. No firing. Fuse at the plug is good. Fuse inside the power supply is good. The machine is completely dead. Also, I found crispy wires inside the power supply near Transformer T2.



I found the same PS you linked, but on Amazon with faster shipping for a few bucks extra.

![images/a77b415f45ea16125bcb46508082b762.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a77b415f45ea16125bcb46508082b762.jpeg)


---
**Don Kleinschnitz Jr.** *December 30, 2018 17:43*

Your problem looks like more than just a LPS if the power on switch is not lit. 

Did you check the fuse in the main input plug?


---
**Jason Dentler** *December 30, 2018 17:45*

Yup. Fuse at the main plug is good. Fuse in the power supply is good.


---
**Richard Wills** *December 30, 2018 21:20*

Have you checked your emergency switch. It is a common failure point also?


---
**Don Kleinschnitz Jr.** *December 31, 2018 15:37*

**+Jason Dentler** check the ac input to the lps. The left connector. 


---
**Jason Dentler** *December 31, 2018 17:15*

**+Don Kleinschnitz Jr.** Genius! Okay. That got me somewhere. I didn't have a connection between the black wire and the live (left, since it's upside down) side of the AC input. It turns out to be a bad plug. A little extra solder and it's good as new.



That still doesn't fix the crispy wires inside the power supply, but a replacement is out for delivery and should be here in the next 4 hours.




---
**Jason Dentler** *December 31, 2018 17:16*

**+Richard Wills** Sorry Rich... My machine doesn't have an eStop switch. It's super basic. It's on my list to upgrade when my Cohesion board arrives and I tear in to the electronics more.


---
**Don Kleinschnitz Jr.** *December 31, 2018 20:55*

Does the LED on the lps come on now?

Have you retried firing? 


---
**Jason Dentler** *January 01, 2019 05:54*

**+Don Kleinschnitz Jr.** My new lps came in, so I swapped it. The new one fires just fine. The burned one is getting returned & they're shipping a replacement, so I'll have a spare.


---
**Don Kleinschnitz Jr.** *January 02, 2019 13:53*

**+Jason Dentler** good deal Happy New Year!




---
*Imported from [Google+](https://plus.google.com/104283467399504580220/posts/JNyPRqfm2hg) &mdash; content and formatting may not be reliable*
