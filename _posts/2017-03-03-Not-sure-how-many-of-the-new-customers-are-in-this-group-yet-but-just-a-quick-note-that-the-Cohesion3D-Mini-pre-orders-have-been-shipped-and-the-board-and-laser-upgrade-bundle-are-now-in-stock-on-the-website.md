---
layout: post
title: "Not sure how many of the new customers are in this group (yet) but just a quick note that the Cohesion3D Mini pre-orders have been shipped and the board and laser upgrade bundle are now in stock on the website"
date: March 03, 2017 02:10
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Not sure how many of the new customers are in this group (yet) but just a quick note that the Cohesion3D Mini pre-orders have been shipped and the board and laser upgrade bundle are now in stock on the website.  





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Allen Russell** *March 03, 2017 04:21*

Can't wait to get Mine  :-)




---
**John McKeown** *March 03, 2017 14:48*

Mine arrived a few weeks ago but due to house move etc I'm just getting around to installing it today. I'll be scouring this page for tips :) Thanks again! 


---
**John Austin** *March 03, 2017 20:43*

If I order on the 2nd of March, has it been shipped?




---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 20:44*

Hey John, yours will go out on Monday.   You'll get an email with tracking when it ships. 


---
**John Austin** *March 03, 2017 20:52*

Thanks Ray. Now time for me to read on installing and running it.

any good videos or tutorials?




---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 21:12*

There is a documentation link on the website.  I am going to make new docs but can't give a firm timeline at this moment.  Take everything you read with a grain of salt, for example we no longer recommend replacing the pot.


---
**Russ “Rsty3914” None** *March 04, 2017 04:15*

Thanks bud !


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 04:16*

That's my line! :)


---
**Russ “Rsty3914” None** *March 04, 2017 11:30*

Ok.


---
**Lance Ward** *March 04, 2017 15:57*

Got my C3D board in mail today.  Gonna give it a go this weekend after I download instructions. :)


---
**Russ “Rsty3914” None** *March 04, 2017 19:27*

Still waiting for mine :/


---
**Russ “Rsty3914” None** *March 04, 2017 20:44*

Be here on Monday !

Is there interlock for estop or door switch ?


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 20:47*

The "correct" way would be to put a door switch and wire it in series with the enable switch.  Door opens, laser stops firing. 

There is a kill button on the board and a header right behind so that you can wire your own big red button to halt smoothie right away.  But again the "correct" way for an ESTOP is something that kills all power to the machine. 


---
**Russ “Rsty3914” None** *March 04, 2017 20:48*

Right...and it is ok to kill power to board, right ?


---
**Russ “Rsty3914” None** *March 04, 2017 20:49*

I am adding power contactor that will do just that...kill power to laser and halt smoothie...






---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/8n2RT8XcAqq) &mdash; content and formatting may not be reliable*
