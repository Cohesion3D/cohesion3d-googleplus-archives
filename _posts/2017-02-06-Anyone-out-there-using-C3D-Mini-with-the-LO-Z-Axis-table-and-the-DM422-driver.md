---
layout: post
title: "Anyone out there using C3D Mini with the LO Z-Axis table and the DM422 driver?"
date: February 06, 2017 04:41
category: "C3D Mini Support"
author: "Tony Sobczak"
---
Anyone out there using C3D Mini with the LO Z-Axis table and the DM422 driver?  How did you wire it up? I have 5V to OPTO, ENA open, DIR to Dir3, PUL to STEP3.  Red wire to A+, Yellow to A-, Orange wire to B+ and Brown wire to B-.  I can't get to to move at all via GLCD or laserweb3. Any assistance would be appreciated.





**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 04:47*

Find datasheet for driver. Attach pictures of your wiring here.  I know using 5v is one option, but it also requires setting pins as open drain by making config changes. 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 05:00*

Also, advise you do not power up until someone here reviews your wiring. If Peter says there needs to be a ground wire, and you have a 5v going somewhere, then yeah, don't power up till we can take a look. 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 05:07*

And finally, 

**+Jim Fong** 

**+Øyvind Amundsen** 

**+Marc Pentenrieder** 

These are the gentlemen with external steppers being used with Cohesion3D Boards. 

Jim has a Mini, the rest have ReMix. I trust them. 


---
**Øyvind Amundsen** *February 06, 2017 05:58*

There is two alternatives. In this you have common (-)



![images/2df3c5079adb5e65153dd427d5f1aeb5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2df3c5079adb5e65153dd427d5f1aeb5.jpeg)


---
**Øyvind Amundsen** *February 06, 2017 05:59*

Common (+)  

![images/b90489fc4fc12792a79d673278797bc2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b90489fc4fc12792a79d673278797bc2.png)


---
**Øyvind Amundsen** *February 06, 2017 06:02*

It depends on the stepper drivers. If the drivers work with 3.3V input signals you can use the common ( - ) . If they need +5V signals to work you have to use a common +5V and the ( - ) pins for input signals.




---
**Tennessee Tony** *February 06, 2017 19:32*

**+Peter van der Walt**  The only ground on the driver is the one next to the 24v input.  That ground is common to the 5V, 24V and chassis.  I checked to make sure.




---
**Tennessee Tony** *February 06, 2017 19:49*

**+Øyvind Amundsen** The DM422  only has the ENA, PUL , DIR and OPTO (+5v) inputs, no   - sides. See attached.  This is the way I am connected



![images/25ae6c89e65d353cb2c8f6fc68a2d8aa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/25ae6c89e65d353cb2c8f6fc68a2d8aa.jpeg)


---
**Tennessee Tony** *February 06, 2017 20:02*

**+Ray Kholodovsky**  This is from the DM422 guide.  I blocked what I am using.   I also posted the sheet from LO where they show the connection to their DSP.

![images/174f01ecd5df05c49b55a5cb553306d9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/174f01ecd5df05c49b55a5cb553306d9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 20:13*

You need open drain for that.  Go into the config file on the board and add an o after the pin.  So:

gamma_step_pin                               2.2o

gamma_dir_pin                                0.20o



Save, eject, reset the board, etc... 

Take a pic of your actual setup and wiring too. 


---
**Tennessee Tony** *February 06, 2017 20:28*

From the LO website on the Z-Axis table:





Suggested motor driver: Mini 2 Phase 2A 1-axis Stepping Motor Driver (optional). 



Dir 3     =  Yellow

Step 3  =  Green

EN 3     =  Blue

 

![images/69b5378596bba72dae90d7e944b0330a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69b5378596bba72dae90d7e944b0330a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 20:30*

Nope. Consult the pinout diagram very closely. Looking at the board with the text upright steep and dir are at the top, enable is at the bottom. Check the diagram again. Follow the documentation link at the top of the cohesion3d website. 


---
**Tennessee Tony** *February 06, 2017 20:36*

From the LO Z-Axis web page:



Motor connection: A+/A- Red/Yellow,  B+/B- Orange/Brown



+Vdc is 24v



Enable is not connected as suggested in the driver doc, but can be hooked up. OPTO is +5v. 

![images/efe4b28018e124e705e2cdcdee6d21b7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/efe4b28018e124e705e2cdcdee6d21b7.jpeg)


---
**Tennessee Tony** *February 07, 2017 21:06*

**+Ray Kholodovsky**  Is there a problem with the wiring as posted above. 



If the DM422 driver is a problem what do you suggest I use instead?  And where to get it?




---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 23:35*

You showed yellow, green and blue wire. Yellow should be dir and green should be step. 

Blue is not enable. Enable is at the other end of that header. This is what I was trying to explain. 


---
**Tennessee Tony** *February 08, 2017 00:51*

**+Ray Kholodovsky**​ That photo is upside down. The card is marked,  lowest in original is at the top of the card.  It's marked DIR 3. See attached. 



![images/dec3c5445b4b632b8646cc48d075573b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dec3c5445b4b632b8646cc48d075573b.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 00:54*

I repeat. Yellow wire is DIR. Green wire is STEP. 

<b>The 3rd socket is not enable. Enable is at the other end of the header strip.</b> Where it says Mini on the board. 


---
**Tennessee Tony** *February 08, 2017 00:55*

OK I'll give it a try.  I figured the designation on the board was the sign. 


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2017 00:59*

And this is why I originally say to consult the pinout diagram. 


---
**Tony Sobczak** *February 09, 2017 17:41*

It's working great now thanks to **+Ray Kholodovsky** and **+Jim Fong**.  It looks like adding an ! to the following has it working:



gamma_step_pin                               2.2!o

gamma_dir_pin                                0.20!o



I really appreciate the time and patience of both of these individuals.






---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/ExrgB4VQbg4) &mdash; content and formatting may not be reliable*
