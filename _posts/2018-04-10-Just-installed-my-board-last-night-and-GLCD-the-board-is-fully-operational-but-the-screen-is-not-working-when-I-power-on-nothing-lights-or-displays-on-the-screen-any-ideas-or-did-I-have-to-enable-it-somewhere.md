---
layout: post
title: "Just installed my board last night and GLCD the board is fully operational but the screen is not working when I power on nothing lights or displays on the screen any ideas or did I have to enable it somewhere?"
date: April 10, 2018 08:56
category: "C3D Mini Support"
author: "Josh Smith"
---
Just installed my board last night and GLCD the board is fully operational but the screen is not working when I power on nothing lights or displays on the screen any ideas or did I have to enable it somewhere?





**"Josh Smith"**

---
---
**Joe Alexander** *April 10, 2018 09:51*

there is a section in the config file that needs to be enabled for the GLCD, look near the end called something like panel.enable. IIRC the default settings should work fine for a standard reprap GLCD.


---
**Josh Smith** *April 10, 2018 09:57*

**+Joe Alexander** looks enabled on default config file will check tonight when I get home. thanks for the tip. 


---
*Imported from [Google+](https://plus.google.com/104342512578563937192/posts/A8AsGDPQHnh) &mdash; content and formatting may not be reliable*
