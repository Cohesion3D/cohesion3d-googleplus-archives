---
layout: post
title: "Ive got my cohesion 3d mini connected to my k40 with ribbon hookup"
date: March 06, 2018 14:36
category: "C3D Mini Support"
author: "Brass Honcho"
---
Ive got my cohesion 3d mini connected to my k40 with ribbon hookup.  Have all green lights on the board.  The problem im having is that the laser position is showing 180y when its in home position and the laser power setting seems to be controlling the speed and the speed setting seems to be controlling the power.  I have rotated the 4 pin connector at y and it does not make a difference.  A am also not able to set the origin.





**"Brass Honcho"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 17:34*

Actually, Y 200 is the rear of the machine and the 0,0 is the front left.  



Please start by reading this: [https://github.com/LightBurnSoftware/Documentation/blob/master/CoordinatesOrigin.md](https://github.com/LightBurnSoftware/Documentation/blob/master/CoordinatesOrigin.md)


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 17:36*

Explained in this video:  
{% include youtubePlayer.html id="4gNqs4GQfjE" %}
[youtube.com - LightBurn Tutorial: Machine and Job Origin (Corrected Audio)](https://www.youtube.com/watch?v=4gNqs4GQfjE)


---
**Brass Honcho** *March 07, 2018 11:56*

That video helped me understand the origin issue; thats not a problem now. I still have an issue with the laser power and the speed being reversed.  When i set the speed at 10 the gantry moves the same speed but the laser strongly engraves. when i set the speed at 500 the gantry speed is unchanged but the laser power is very low.  could the wiring be different going to my power supply somehow?


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2018 15:49*

Did you select Cohesion3D (Smoothie) as your device in LB? 


---
**Brass Honcho** *March 08, 2018 13:13*

yes


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2018 15:37*

Could you take a video of the issue?  I need more information to understand. 



Fundamentally, if you just send a G1 X10 S0.4 F600 and then X20 S0.8 F600 you should see a weak burn and a stronger burn, and power reflected on the mA meter if your machine has one. 


---
**Brass Honcho** *March 16, 2018 15:47*

ive attached a video.   i cant adjust the speed at all.  and it engraves from bottom to top.


{% include youtubePlayer.html id="IMaa1XJjXrk" %}
[youtube.com - VID 20180316 113211534](https://youtu.be/IMaa1XJjXrk)


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2018 19:32*

Yeah, Smoothie can't take the data for a raster engraving above like 100mm/s. 



So, just switch over to GRBL-LPC Firmware: [cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)






---
**Brass Honcho** *March 16, 2018 22:20*

smoothie is 5 times slower than stock k40 control boards?  Is that the fastest board cohesion 3d sells?


---
**Ray Kholodovsky (Cohesion3D)** *March 16, 2018 22:25*

The remix has the same chipset, so yes. As I said, just load the grbl-lpc firmware from the article - Dropbox link I provided and all should be good.  


---
**LightBurn Software** *March 16, 2018 22:46*

The board isn’t the issue here, the firmware is. Smoothie is a great choice for cutting, as the motion planner generates very smooth output, however it comes with a high CPU cost, meaning it’s quite speed limited for image engraving.



GRBL-LPC runs on the same chips, but has much less CPU overhead. If you plan to do much engraving, it’s the better choice, and easily runs 3x faster than Smoothie.



Regarding your “bottom to top” note, that’s the default because most machines exhaust in the rear and this results in the cleanest cuts because the particulate in the smoke isn’t being drawn across the tougher surface of the engraved area. If you still prefer top-down, change the scan angle to 180 degrees.


---
**LightBurn Software** *March 16, 2018 22:47*

If you do switch to GRBL, you’ll also need to choose the C3D / GRBL device in the device setup.


---
**Brass Honcho** *March 20, 2018 15:38*

I have updated the micro sd with the gerbil firmware and it seems a little faster but now its engraving deeper on a stop then it does on the middle of the engrave.  Notice how every part of a character that the laser stops on is twice as deep.

![images/85fbedc55dda251b5b0473112fda82db.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/85fbedc55dda251b5b0473112fda82db.jpeg)


---
**LightBurn Software** *March 20, 2018 16:33*

Enable 'overscan' in the scan settings.


---
**Brass Honcho** *March 20, 2018 16:45*

i just noticed the leds are not lighting green.  When i copy the firmware it tells me that the file has properties that cant be copied to the new location


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 17:00*

Yep. That's normal. 


---
**Brass Honcho** *March 20, 2018 19:44*

I cant find overscan anywhere.  I have only one red led lit the others are not lit.


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 19:45*

[github.com - Documentation](https://github.com/LightBurnSoftware/Documentation/blob/master/Operations.md)


---
**LightBurn Software** *March 20, 2018 19:46*

Double click the entry in the cut list on the top right, next to where it says “scan”. That will open up more options, including the overscan setting. Ray will advise about the LED status, but I believe that is normal.


---
*Imported from [Google+](https://plus.google.com/117535072813354948352/posts/1TaSf11repX) &mdash; content and formatting may not be reliable*
