---
layout: post
title: "Just a quick question, can the board be powered up without being physically in the laser?"
date: January 01, 2017 20:24
category: "General Discussion"
author: "Bryan Hepworth"
---
Just a quick question, can the board be powered up without being physically in the laser? I've read on the smoothie site not to plug stepper motors into the stepper drivers when powered, but thought I'd ceck that it would be alright to do so.





**"Bryan Hepworth"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 01, 2017 20:26*

You can run logic just by plugging in USB to your computer. You can also provide it 12-24v to the blue terminal in the top left. Is that what you were trying to ask? 


---
**Bryan Hepworth** *January 01, 2017 21:38*

yes it was Ray prior to plugging it into the machine itself, thanks so much for getting back to me.


---
**Ray Kholodovsky (Cohesion3D)** *January 01, 2017 21:39*

Wonderful. Pretty much the spirit is simply "do not fiddle with wiring while the board is powered". 


---
**Bryan Hepworth** *January 02, 2017 00:16*

:-) do not fiddle - liking that expression. Initial question wasn't particularly well phrased either reading over it now. I plan on writing a start to finish experience once I'm done, properly checked for correctness and grammar.


---
*Imported from [Google+](https://plus.google.com/114125155844297036799/posts/CCEDDkD4WTf) &mdash; content and formatting may not be reliable*
