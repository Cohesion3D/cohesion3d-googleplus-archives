---
layout: post
title: "Got my board today. Cant get the laser to fire or recognize the end stops"
date: January 19, 2017 00:50
category: "General Discussion"
author: "Chad Traywick"
---
Got my board today. Cant get the laser to fire or recognize the end stops. When I home all it just sets wherever its at and moves the head around a bit and thats it. If I jog it will just slam the head into both ends. As far as the laser it wont fire from the control or laserweb.





**"Chad Traywick"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 00:55*

Hi Chad,

Can we see some pictures of your machine electronics and the wiring going to the Mini board please?


---
**Chad Traywick** *January 19, 2017 00:58*

Sure thing.

![images/0c9ec27ddcc40010bece4cb80aebafaa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0c9ec27ddcc40010bece4cb80aebafaa.jpeg)


---
**Chad Traywick** *January 19, 2017 00:59*

![images/1afd0780e5655e89e05a48d7cc1abb28.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1afd0780e5655e89e05a48d7cc1abb28.jpeg)


---
**Chad Traywick** *January 19, 2017 00:59*

![images/593b57ce784c111bcd6f48e733115d7c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/593b57ce784c111bcd6f48e733115d7c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 01:14*

Let's start with homing.  Can you confirm these settings are in LW please.  At this point we just need to be sure that you are using the G28.2 command to home.   In order to test your switches, get the head out of the way in both axes, and type and send M119 into LW terminal. You should have a min x and max y in the line that shows up 0 if not pressed, 1 if pressed. Test each switch circuit by holding it pressed while sending that command. 

![images/795974ddab48e874ecb1d349a4343fc4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/795974ddab48e874ecb1d349a4343fc4.png)


---
**Chad Traywick** *January 19, 2017 01:14*

Here's a pic before install

![images/6e0ee4461b5417a726c962516427fe1d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e0ee4461b5417a726c962516427fe1d.jpeg)


---
**Chad Traywick** *January 19, 2017 01:19*

This is what it returns. Also I have optical endstops

![images/a2c61c3afd1895f035d3cbb6306c6bfb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a2c61c3afd1895f035d3cbb6306c6bfb.jpeg)


---
**Chad Traywick** *January 19, 2017 01:21*

And the settings are the same except for the travel moves, laser test power and laser test duration


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 01:23*

Chad, this is what it returns <b>when you do what</b>?  That's the important bit :)

Get your laser's head out of the way. Run the command.  It should read "X min 0 Y max 0".

Hold the X endstop.  Run the command.  "X min 1 Y max 0"

And the opposite for Y.

Ah, since you have optical endstops, then move the head around to the respective ends.  Turn power off for this, just keep the Mini plugged in over USB to your computer.


---
**Chad Traywick** *January 19, 2017 01:25*

Ok I can do that. What I was saying is what's in the picture is what it returns when I ran M119 with the endstops clear


---
**Chad Traywick** *January 19, 2017 01:30*

Here is both returns. First M119 with the endstops clear second with both endstops active

![images/47882228c3061dee01b82be76747f0a8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/47882228c3061dee01b82be76747f0a8.jpeg)


---
**Chad Traywick** *January 19, 2017 01:30*

The only thing I see it it has the y axis active either way


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 01:34*

Yeah, I noticed that.  Should be 0 when it's not engaged.

Questions: Homing worked properly (for both, aka including the Y axis) on the stock board?

Please power down including disconnecting in LW and pulling the USB cable.  pull the ribbon cable out of the Mini.  Get USB back up and run M119 again.  Let me know what it says. 




---
**Chad Traywick** *January 19, 2017 01:38*

Yes homing worked fine before. I have even swapped the original board to test everthing and it worked fine. Here is the results with the ribbon cable removed after powering down the board and machine. Both now show active.

![images/5ece82fa2c32c072fa6faf085b5b1aac.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ece82fa2c32c072fa6faf085b5b1aac.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 01:47*

Investigating, please standby for a bit. 


---
**Chad Traywick** *January 19, 2017 01:47*

Sure no problem 


---
**Bill Keeter** *January 19, 2017 05:04*

I'm having the exact same issue. I can jog the head around. But it's setting it's starting location as home. Also the laser won't fire.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 05:08*

Not sure what the 2nd sentence means. 



Here's how to test fire:![images/529478ceceeac9a6ac72b0382a75473a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/529478ceceeac9a6ac72b0382a75473a.png)


---
*Imported from [Google+](https://plus.google.com/101895536937391772080/posts/EBnniQUJL3v) &mdash; content and formatting may not be reliable*
