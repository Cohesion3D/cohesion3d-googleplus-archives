---
layout: post
title: "Ray Kholodovsky , I did not have a pleasant start to my day- a non booting computer"
date: March 08, 2017 15:07
category: "C3D Mini Support"
author: "Steven Whitecoff"
---
**+Ray Kholodovsky** , I did not have a pleasant start to my day- a non booting computer. I use an HP Proliant DL 380 server as a workstation*( One aspect of servers is they are designed to boot from anything, USB port, floppy, CD, LAN, and of course any drive or array). The way the Minis come they power up on USB connection and therefore my server wants to boot from it and of course it cant since it doesn't have a boot block. Took me a while to connect the dots while my caffeine made its way to my brain.



I have the Mini connected to a USB port INSIDE the server so disconnecting it on that end is a pain and I need the other ports available for other things. I've already tried several times to send jobs to it only to find I had unplugged it at the Mini....



SO I need to NOT power the Mini via USB. 



*Some DIY advice/info for CAD/CAM users...contrary to urban legand, servers can be configured to make ass kicking workstations and they are dirt cheap- I added some cables from the powersupply bus to provide power to a top shelf graphics card(the only shortcoming of a server) which then makes it a brute of a gaming machine too). 

Where else can you get a dual redundant powersupplied, Dual Xeon Quad core 16gb memory(EEC) w/Scsi array controller(supports other drives too) with hot swap drive and powersupply bays, 500gb SSD and 2tb Raid 5 plus 2 tb backup drive for way under a grand or $1k with $300 of graphics card???????????





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 15:43*

Quick answer: there's a solder jumper/ trace at the bottom of the board called USB Power. Cut the trace so USB doesn't power it. Will get you pics later. 


---
**Alex Hodge** *March 08, 2017 15:49*

...or just go into your BIOS and streamline your boot options/change your boot order. I work with HP Proliant DL380 servers every day. No reason to treat them any different than any other computer when it comes to boot options. But you know, your choice. Personally I wouldn't go cutting traces on my fancy new C3D Mini when all I need to do is change some BIOS settings.


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 15:55*

Yeah, I can't be held responsible for users making mistakes while cutting traces... Just because it's there doesn't mean I'm excited about people using it...  Anyways if you can fix it in boot order that's much better. 


---
**Steven Whitecoff** *March 08, 2017 17:16*

Well there are two issues, the boot which as Alex says is fixable with boot order but then if you have controller/drive issue preventing the boot sequence from getting to the USB you have to change the bios back to USB first. Merely an extra step or two. 



Second, I would prefer not have the Mini powered for literally 5000 hrs extra a year when it might get a hundred hours of use.

 Its a tough call- is it better to power on and off a lot or leave it powered. I've never seen a convincing argument for either. I tend toward leaving it on but in this case the extra drain of the Mini is stress on the server, I'm on my second mobo...so IDK. 

Cutting the Mini doesnt even enter my mind as a risk- I build my own electronic of all types:



 Ray can appreciate this: InfraRed 4 channel proportional "radio" control using a TSSOP package 12F630 MCU, all 100% homebrew masks(inkjet), double side flexPCB, hand soldierd with an pencil iron (0402 resistors are a challange!), homebrew holder/programmer and my own code mods. 0.11grams for the receiver and 0.08gm for the IR detector Since commercial 2.4ghz gear is now down to .25gm I no longer need to build these for micro models

![images/242e30d84d73ec6c5ed7bbc9894875ff.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/242e30d84d73ec6c5ed7bbc9894875ff.jpeg)


---
**Alex Hodge** *March 08, 2017 20:50*

Fair enough. There's also the option of an off the shelf USB switch. But I think you like the idea of cutting the trace. :)


---
**Steven Whitecoff** *March 09, 2017 02:30*

Actually **+Alex Hodge** I went looking for a switch right after my last post. Since you know DL380s you may know my gen3 (IRC) only has 5 USB ports so it made sense to make using an extra device a push button affair, and ever more so for using the motherboard internal port. But I see this warning on many: "SPECIAL NOTE: Not recommended for switching mass storage devices such as hard drives, flash drives, camera memory cards, etc." 

So I need to do a little research before going that route. I've also tried several multiport adapters but the HP's implementation of USB seem rather picky and none of them work well. My "best" one nearly got cooked when I put my Corellaser dongle in it, TG the dongle is ok. 

And come to think of it, I believe I have an open PCIe slot, so maybe its time for a USB3.0 card.


---
**Alex Hodge** *March 09, 2017 03:23*

G3 is far too old to be using anymore. You're taking about original Xeons with a single megabyte of cache and a 533 memory bus. It's gotta be a G5 or better to even consider actually using. You're probably having USB issues as you wouldn't even have USB 2.0. Heck you would have a PCI-X slot, which is NOT the same thing as a PCI-E slot. Good luck...I would venture you could get better performance from a raspberry pi 3 or one of those little Intel compute sticks.


---
**Abe Fouhy** *March 09, 2017 12:33*

**+Steven Whitecoff**​ I'm confused, why not just disable the option to boot from usb in the BIOS (boot order none for usb). You never use this option till you are reinstalling the OS or booting to Linux or equivalent. In the last 6 years I have not needed to boot to usb for any reason. Then no wires are cut, no switch and you can turn off your computer.


---
**Steven Whitecoff** *March 09, 2017 15:49*

G5 it is with X5460 cpus...that was a late night post and I've not had to remember the gen since I was setting it up. I guess its 3 gens old now but while I've always purchased CPUs one notch below the top at the end of the release year, I've never thought I had enough power. The DL380, aged and all has been a beast from day one. I look for trouble if any task takes more an a couple seconds unless web access or such is involved. To this day with win10 and Server 2k I am tickled pink with the power.

**+Abe Fouhy** your point is valid, and the way to go. But regardless of that the Mini would be powered at all times the computer is on. This is not what I would want and dont want the continuous extra load, small as it likely is, on the USB system as its on the motherboard.I tranfer pictures from my phone all the time which means its trying to charge it which is as much load as I'd like to see by itself, let alone everthing else. In fact I'm paranoid enough with all the USB stuff I use and plug and unplug that the best option is to add a USB 3.0 card so if anything dies, it will be the card not motherboard. I need more ports anyway and a half high 7 port card is only $40- time to pony up and let Ray worry about 5k hrs a year of on time. :) I doubt he is concerned because he knows more than me about his products. 


---
**Abe Fouhy** *March 09, 2017 15:57*

**+Steven Whitecoff** OK, I understand what the issue is now. Yea I think a usb card is the way to go and just plug it in when you need it. :)


---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 20:54*

Since I did promise this pic... You want to cut straight down the middle of that USB PWR. DO NOT hit blue at the top or bottom. Verify before and after with a multimeter on continuity. ![images/b6484934332d961bdf383d3df9600c10.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6484934332d961bdf383d3df9600c10.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 20:55*

Like so: ![images/cd705626f95fc7d3d2b071bcc7074da3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cd705626f95fc7d3d2b071bcc7074da3.jpeg)


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/Qj8eh7DNPUN) &mdash; content and formatting may not be reliable*
