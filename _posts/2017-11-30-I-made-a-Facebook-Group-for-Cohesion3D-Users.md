---
layout: post
title: "I made a Facebook Group for Cohesion3D Users"
date: November 30, 2017 02:43
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I made a Facebook Group for Cohesion3D Users.   It's not an official support group, but I recognize that G+ can be problematic for some, so let's see what happens. 



It's called "Cohesion3D Users" :



[https://www.facebook.com/groups/1045892858887191/](https://www.facebook.com/groups/1045892858887191/)







**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Sj1n2tWkMuk) &mdash; content and formatting may not be reliable*
