---
layout: post
title: "Hi Guys. I've managed to get my hands on an empty 50w 500x300 laser shell and plan on moving my k40 with C3D mini over to it"
date: July 25, 2018 14:26
category: "FirmWare and Config."
author: "Jeff Lamb"
---
Hi Guys.  I've managed to get my hands on an empty 50w 500x300 laser shell and plan on moving my k40 with C3D mini over to it.  I have been running grbl-lpc for ages, but cant seem to find a version for the C3D with 4th axis support for a rotary (I've built one but it never fitted into the k40 due to the height).  Can anyone point me in the right direction please.  Thanks





**"Jeff Lamb"**

---
---
**Jim Fong** *July 25, 2018 16:16*

[dropbox.com - firmware grbl-lpc c3dmini 4axis with XY homing.bin](https://www.dropbox.com/s/ojfphtqgr8bxnkm/firmware%20grbl-lpc%20c3dmini%204axis%20with%20XY%20homing.bin?dl=0)


---
**Ray Kholodovsky (Cohesion3D)** *July 25, 2018 16:30*

Jim compiled a bunch of grbl builds for us and I have them, somewhere. 



We need to know exactly what drivers you are using - for example if you have an external driver wired as we recommend for A that requires a different build that sets the pins open drain. 


---
**Jim Fong** *July 25, 2018 17:03*

The link above is basic 4axis firmware using polulo plug in drivers.  XY homing (no Z table homing).   It’s the one I use on my laser.  



As Ray has mentioned, we would have a version for external drivers and other configurations.  


---
**Jeff Lamb** *July 25, 2018 17:40*

Excellent. Thanks guys. I'm using polulus for the moment and see how it goes although I suspect I'll have to move to external drivers to power the larger steppers properly.


---
**Jeff Lamb** *July 26, 2018 21:29*

I managed to get the c3d and psu moved over today but no movement although it does try. I'm guessing that it just isn't powerfull enough with the polulus.  I've ordered some external drivers but will I need a different firmware version to use them? Thanks.


---
**Jim Fong** *July 26, 2018 22:09*

**+Jeff Lamb** my initial rotary testing was done using a large 3.5amp nema23 and drv8825.   The drv8825 can push out just a bit more current than a a4988 can.   It had enough to spin the motor around 100rpm.  Plenty for rotary testing.  Had to Set the acceleration and max speed really really low.   I have s bunch of external drives but kinda lazy to hook one up.  I don’t use the rotary very much.  



What external drive did you buy. Need to know so I can send the right grbl version


---
**Ray Kholodovsky (Cohesion3D)** *July 26, 2018 22:14*

It’s not a question of which driver as much as how it’s wired. If it’s common anode with a common +5v then you need the open drain. If you can wire it with a common ground you can use the normal firmware. 


---
**Jim Fong** *July 26, 2018 22:33*

**+Ray Kholodovsky** ya easier for me to ask what drive than explaining the difference. Lol. You did a better job than I could. 


---
**Jeff Lamb** *July 26, 2018 22:42*

They are dm542t. [https://rover.ebay.com/rover/0/0/0?mpre=https%3A%2F%2Fwww.ebay.co.uk%2Fulk%2Fitm%2F122556551961](https://rover.ebay.com/rover/0/0/0?mpre=https%3A%2F%2Fwww.ebay.co.uk%2Fulk%2Fitm%2F122556551961)

[ebay.co.uk - Details about Digital Stepper Motor Driver 1.0-4.2A 20-50VDC Nema 17,23,24 CNC Stepper DM542T](https://rover.ebay.com/rover/0/0/0?mpre=https://www.ebay.co.uk/ulk/itm/122556551961)


---
**Ray Kholodovsky (Cohesion3D)** *July 26, 2018 22:59*

You should be able to wire this with common ground (ie: not the way in our instructions) 


---
**Jeff Lamb** *July 27, 2018 07:54*

Forgive me as I'm a complete newb when it comes to the external drivers.  Do you mean to wire the negative side of each input (en, dir etc) to each other and to a 0v rail on the c3d, and then connect the positive side of each input to the c3d board?  And will the above firmware work with this?  Thanks.




---
**Ray Kholodovsky (Cohesion3D)** *August 02, 2018 17:04*

Apologies for overlooking (your +1 brought this back into my feed) - I believe the answers are yes. 


---
*Imported from [Google+](https://plus.google.com/100451757440368369818/posts/VcMEWDhEdgS) &mdash; content and formatting may not be reliable*
