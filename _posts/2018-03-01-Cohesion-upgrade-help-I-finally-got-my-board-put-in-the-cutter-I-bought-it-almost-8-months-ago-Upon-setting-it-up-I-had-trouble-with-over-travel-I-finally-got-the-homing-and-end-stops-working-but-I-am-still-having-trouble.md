---
layout: post
title: "Cohesion upgrade help I finally got my board put in the cutter (I bought it almost 8 months ago) Upon setting it up I had trouble with over travel I finally got the homing and end stops working but I am still having trouble"
date: March 01, 2018 21:21
category: "K40 and other Lasers"
author: "Brandon Speaks"
---
Cohesion upgrade help



I finally got my board put in the cutter (I bought it almost 8 months ago)  Upon setting it up I had trouble with over travel  I finally got the homing and end stops working but I am still having trouble with the over travel.  I am using Light Burn and I keep running into the x and y axis running into the edges of the travel area.  I attached a video.  Could this be a settings thing in Light Burn?



![images/bb83c3db9142502aa2b073389096b4b0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bb83c3db9142502aa2b073389096b4b0.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-4OSNSkXNdL0/Wphu4PkVbjI/AAAAAAAATCY/JiakEvIMvFsEFP1fUqhcSyo1LodERjjlACJoC/s0/20180301_151616.mp4**
![images/49e05e727d557cc063f9081ef4d71b46.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/49e05e727d557cc063f9081ef4d71b46.jpeg)

**"Brandon Speaks"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 21:26*

Start at one corner and then jog to measure the actual area your head can move in.  Let me know what you come up with.  The stock configuration is for 300X 200Y mm. 


---
**Brandon Speaks** *March 01, 2018 21:46*

Can i do this in Light Burn?  I came up with a bigger number when I used a tape measure.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 21:48*

Yes, I'm suggesting you move the head to one corner and jog it using the Move tab in Lightburn, and count how far you can get before it starts hitting the other end. 


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 21:49*

While you're doing that, tell it to jog 10mm each way and measure that it actually moved 10mm. 


---
**Brandon Speaks** *March 01, 2018 22:01*

Okay I had to check the documentation, I couldn't figure out how to jog using Light Burn.  I don't have a ruler that does mm, but I came up pretty close with my tape measure 9.5mm.  Also I am able to jog from the home position 331mm x 233mm 


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 22:02*

Yeah that sounds reasonable. And did you set up Lightburn to be 300x200mm? (Those are the defaults when you select a C3D device) 


---
**Brandon Speaks** *March 01, 2018 22:06*

Yes 300x200 with a front left origin.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 22:07*

Sounds good to me. 



Tell it to home then say G0 X0 Y0 in the console. 


---
**Brandon Speaks** *March 01, 2018 22:09*

It traveled to the front left position, it did not crash.


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2018 22:10*

Wonderful. So now what do you have to do to get the problem to happen?


---
**Brandon Speaks** *March 01, 2018 22:10*

My mistake, it did not make it all the way to the front left


---
**Brandon Speaks** *March 01, 2018 22:11*

![images/679f26fe15a8abaf91bb395cf41bb63a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/679f26fe15a8abaf91bb395cf41bb63a.jpeg)


---
**Brandon Speaks** *March 02, 2018 22:24*

As soon as I hit the start button when testing simple squares it will over travel.  Also did you see my note about it not traveling all the way to 0,0


---
**Ray Kholodovsky (Cohesion3D)** *March 02, 2018 22:26*

I was waiting for an answer to my question "So now what do you have to do to get the problem to happen?"


---
**Brandon Speaks** *March 05, 2018 23:52*

As soon as I try to cut something it does the same as before, over travels in both directions


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2018 03:19*

Got it.

I understand the bit about your gantry is bigger, and we can adjust that later, but I'm trying to figure out why it's overshooting now. 



And you've configured origin in Lightburn to be the front left radio button? 



  **+LightBurn Software** any thoughts? 


---
**LightBurn Software** *March 06, 2018 03:26*

Try this:



Click the home button. Put a piece of scrap material in the laser. Press the test-fire button on the front panel to mark the material.



Type G0 X0 Y100 in the console, hit enter. When it stops, test fire again.



Type G0 X100 Y100 in the console, hit enter. Wait for it to stop, test fire again.



At this point you should have 3 marks on your scrap - one at the Home position, one 100mm below that, and another 100mm right of the 2nd one. Measure these to verify.



If all of that works, chances are you’re trying to run with “Start From Current Position” and haven’t set the job origin properly.


---
**Brandon Speaks** *March 06, 2018 03:27*

Yes front left, and the x and y set according to the directions.  I might mention that I bought my cohesion controller quite a while ago, has there been any firmware upgrades?




---
**Brandon Speaks** *March 06, 2018 03:33*

The laser is currently not ready to fire, I can possibly get it aligned and the cooling set up by tomorrow evening.  Is there a way to check that the job origin has been set correctly besides firing the laser?


---
**Brandon Speaks** *March 06, 2018 03:35*

I found this setting and changed it to front left, everything appears to be working after I changed it to front left![images/a84729661965997855277237b97099e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a84729661965997855277237b97099e7.jpeg)


---
**LightBurn Software** *March 06, 2018 03:37*

If you look under the “Start”, “Pause”, etc, you’ll see a drop-down labeled “Start From”. If it’s set to something other than “Absolute Coords”, change it to that. Then draw your squares.


---
**LightBurn Software** *March 06, 2018 04:25*

I just watched the video - you're set to "User Origin", which defaults to 0,0, but you're also set to "upper-right" as the job origin, which means the laser is going to start at the upper-right of your job (where the green square is in the video). Since the job is below and to the left of that starting position, and that starting position is the origin, you have nowhere to go.


---
*Imported from [Google+](https://plus.google.com/112077980329285762811/posts/5YVesrcqHC6) &mdash; content and formatting may not be reliable*
