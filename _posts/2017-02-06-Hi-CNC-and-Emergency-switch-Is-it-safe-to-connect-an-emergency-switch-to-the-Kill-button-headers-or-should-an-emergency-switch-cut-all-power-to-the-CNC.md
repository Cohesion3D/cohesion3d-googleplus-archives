---
layout: post
title: "Hi - CNC and Emergency switch. Is it safe to connect an emergency switch to the Kill button headers or should an emergency switch cut all power to the CNC?"
date: February 06, 2017 13:27
category: "CNC"
author: "\u00d8yvind Amundsen"
---
Hi - CNC and Emergency switch. Is it safe to connect an emergency switch to the Kill button headers or should an emergency switch cut all power to the CNC?



I have not tested if it is possible yet, but I plan to control my Toshiba VFD from the Cohesion controller (rs232).





**"\u00d8yvind Amundsen"**

---
---
**John Sturgess** *February 06, 2017 15:14*

Me personally, a kill switch should kill everything. No good it stopping all motion control and the spindle still spinning.


---
**Erik Cederberg** *February 06, 2017 16:06*

A e-stop should stop everything in the fastest and most reliable way possible. If possible, it should only be dependent on hardware so there is no risk of software faults. 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 16:53*

I agree that a proper e stop cuts power completely and does not rely on software. 



I have been able to use the smoothie kill button to stop motion right away. I do not know what would happen to a spindle. Best guess is that all activity in the smoothie brain is suspended. So if it requires an additional command to stop spindle, I do not think this will happen. 


---
**Don Kleinschnitz Jr.** *February 06, 2017 17:06*

It depends on what you are trying to be safe from and if you are trying to meet regulations or improve your safety. 



Below is some standards information. Lots can be found by googling E-stop. These standards can be used as guides even if you are not trying to be certified. 



My general rule matches the above posts in that in an emergency I want to remove power from everything without trusting electronics or software.



In my K40 case the main power is my go to Emergency Stop switch.

I am going to install a motor kill for those cases where I want the steppers released and it is not an emergency.

Of course where the laser and HV is concerned you need interlocks and glasses. 



[http://www.infoplc.net/files/documentacion/seguridad_normativa/infoPLC_net_THE_EMERGENCY_STOP.pdf](http://www.infoplc.net/files/documentacion/seguridad_normativa/infoPLC_net_THE_EMERGENCY_STOP.pdf)



[literature.rockwellautomation.com - literature.rockwellautomation.com/idc/groups/literature/documents/wp/800-wp008_-en-p.pdf](http://literature.rockwellautomation.com/idc/groups/literature/documents/wp/800-wp008_-en-p.pdf)


---
**Øyvind Amundsen** *February 06, 2017 17:15*

I may use two.   One for the "software stop" and one that cut all power to the steppers  + spindle


---
**Øyvind Amundsen** *February 07, 2017 09:59*

I downloaded the Toshiba vfs9 user guide, and found that there is integrated emergency stop functions - So I guess a E-switch that cut power to the stepper motors + the emergency stop function on the VFD should be ok


---
*Imported from [Google+](https://plus.google.com/102309741047174439430/posts/bYfmzsZ7c7Y) &mdash; content and formatting may not be reliable*
