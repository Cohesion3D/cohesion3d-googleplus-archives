---
layout: post
title: "I finally got my K40 laser in and installed the Cohesion3D board and it looks like I'm only getting the solid red LED and none of the green ones"
date: October 23, 2018 20:00
category: "K40 and other Lasers"
author: "Reini Grauer"
---
I finally got my K40 laser in and installed the Cohesion3D board and it looks like I'm only getting the solid red LED and none of the green ones. It is connected to USB, I've tried several cables and known working USB ports and it doesn't look like it's going to start up.  Does it look like everything is hooked up right?  Is there something else I need to be doing here?





**"Reini Grauer"**

---
---
**Reini Grauer** *October 23, 2018 20:15*

Additional information: when it's plugged into USB only, with all the other wires removed, the chip highlighted in orange gets quite warm.  The chip highlighted in red gets quite hot.  Is something wrong with this board?

![images/163dc9b7ad0ba5d30af633dc04b4f186.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/163dc9b7ad0ba5d30af633dc04b4f186.jpeg)


---
**Reini Grauer** *October 23, 2018 20:18*

Actually, now that I'm feeling around, there are a lot of really hot things on the board with just USB power on.  Is that normal?  Some are too hot to touch.




---
**Anthony Bolgar** *October 23, 2018 20:24*

Sounds like you shorted out  something. But I am sure Ray will have a much better grasp as to what is happening. Can you take more pics of the installation, we will need to see all the connections including the ones to the PSU. I am sure he will respond soon, BTW, please don't post this in multiple places , it just creates more work for those who are trying to help :)


---
**Reini Grauer** *October 23, 2018 20:26*

Measuring the 5v source on the corner of the board, it only shows ~3v on my multimeter.  The 3.3v labeled traces show ~1v on my meter.  I think something is probably broken on this one.  Thoughts?




---
**Anthony Bolgar** *October 23, 2018 23:11*

Voltage regulator could be bad. 


---
**Reini Grauer** *October 24, 2018 03:20*

I had an old arduino sitting around with the same voltage regulator on it and swapped it on this board but still had the same problem.  I'm not sure what to do with it now, it's definitely defective.


---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2018 16:51*

Send an email with your order #, I’ll get you instructions to return the board for another one. 



The k40 likes to kill our boards, we’re recommending to isolate the board from

the frame with nylon standoffs. 


---
**Reini Grauer** *October 24, 2018 17:28*

It was isolated, I used the same plastic standoffs and screws that came with the original board.  I'm going to probe out the wires from the power supply to make sure nothing funky could have been coming from there, but I swapped the original board back in and it seems to be working correctly, so I'm not sure what could have happened.  



Order number 1232, but I have moved since I put in the order.  


---
**Ray Kholodovsky (Cohesion3D)** *October 24, 2018 17:34*

There's still metal nuts and screws going between the board and the frame.  The K40 LPSU is so bad that it spits out power spikes that travel through the frame (frame grounding is essential but still not a full solution) and kill the board because I thought grounding the board to the frame for a common ground was a good idea. 



Are you still in the USA? 


---
**Reini Grauer** *October 24, 2018 17:37*

Yes.  I sent an email to info@cohesion3d.com with the details.  



Good info on the standoffs, I'll probably source some nylon screws or maybe print some standoffs to make sure this doesn't happen again.  I was really excited to start burning stuff!


---
*Imported from [Google+](https://plus.google.com/107615990717524166780/posts/iqh5dWriMF1) &mdash; content and formatting may not be reliable*
