---
layout: post
title: "Hello cohesion3D team, i have ordered a K40 laser, and it should be delivered at the 19th of this month"
date: October 18, 2017 01:09
category: "General Discussion"
author: "Marko Marksome"
---
Hello cohesion3D team,

i have ordered a K40 laser, and it should be delivered at the 19th of this month. Since i have read that the original brain of the machine is bad, i will order a new one for it.

At first i have been looking on the Gerbil Board from Paul de Groot.

Then, i accidently stumbled onto your controller board. Now i dont know what to order.



To me, your product and your customer service seems to be better, the only thing that keeps me from ordering the Cohesion3D mini Board from you is that i dont know how the grayscale engravings look like.



Can you provide me with a video or pictures of greyscale engravings done with your board?

And also, what is your honest opinion on the quality of GS engravings made with your board? I ve seen many but to my eye the Gerbil board produces the best engravings that i have seen. Can your product top that? Can your product make better engravings?



What are the benefits if i order from you and not Paul?

Please clear my questions and let me see the quality of the engravings. If i like it i will definetly buy from you. 



Thank you very much!





**"Marko Marksome"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 01:51*

Hi Marko,

I think you should have a scroll through **+Jim Fong**'s profile: [https://plus.google.com/114592551347873946745](https://plus.google.com/114592551347873946745)



I also found a few really cool outputs from **+John Milleker Jr.**:

[https://plus.google.com/+JohnMillekerJr/posts/9jmvR7NzCWB](https://plus.google.com/+JohnMillekerJr/posts/9jmvR7NzCWB)

[https://plus.google.com/+JohnMillekerJr/posts/bMMAFxpVX78](https://plus.google.com/+JohnMillekerJr/posts/bMMAFxpVX78)



My honest opinion is that I spent too little time writing the product description on the website or marketing, spent it all on engineering the board and supporting our users. 



There's also quite a bit of activity on a few Facebook groups including one called "Chinese K40 Laser Group." and I pulled some of **+Tammy Mink**'s latest posts into this album: [imgur.com - Tammy Mink Engravings with Cohesion3D Mini in K40 Laser with Lightburn Software](https://imgur.com/a/NX3GW)



Tell me what you think of these :)


---
**Marko Marksome** *October 18, 2017 04:21*

**+Ray Kholodovsky** Wow i am impressed! Especially with the last one.

Now, i am convinced to buy one of the boards too ;) Thank you, you made my decision easier now.

Do you have an instruction on how to hook up the limit switches to the k40 you sell on your web site?

For normal use i need just 2 right? (X,Y)


---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 04:28*

I prefer not to make claims and then under deliver. Just know that what you saw are some of our most-power users after a lot of tuning and experience. 



k40 already has endstops built in, sometimes mechanical switches other times optical, and it's all already configured. 

If you want to use ours, they just plug right into the board. At most, I expect some minor changes to the configuration file. 



I will advise you to get the GLCD screen and adapter, it is more reliable to run large jobs/ raster engravings this way (via the memory card on the board) and by doing this you are no longer tethered to your computer (do not need the USB cable constantly connected/ computer in same room. 


---
**Marko Marksome** *October 18, 2017 17:31*

**+Ray Kholodovsky** Ok i just ordered a full pack with an lcd etc. Hey look at this, i hope this is not a bad sign :

"Your order number is: 666"

Wow.... lol thanks for your help!

If there is something i need to know i can ask here, right?




---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 17:34*

Cool. Support is through this group, yes. 


---
**Marko Marksome** *October 19, 2017 16:32*

Hey Ray,

When i log in to you site to see the order infos, it says waiting fullfilment. Does this mean you are still waiting for the money or it means you have to process it first? I have payed via paypal


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 16:38*

Not shipped yet. You should get an email with tracking when it ships. 


---
**Marko Marksome** *October 19, 2017 17:28*

Ok thanks


---
*Imported from [Google+](https://plus.google.com/110288443111992997281/posts/MBgmofaE6bZ) &mdash; content and formatting may not be reliable*
