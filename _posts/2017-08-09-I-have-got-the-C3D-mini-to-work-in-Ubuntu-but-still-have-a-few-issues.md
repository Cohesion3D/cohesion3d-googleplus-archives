---
layout: post
title: "I have got the C3D mini to work in Ubuntu but still have a few issues"
date: August 09, 2017 11:47
category: "C3D Mini Support"
author: "Kma Jack"
---
I have got the C3D mini to work in Ubuntu but still have a few issues. 

The laser stops at every line for about a second and then carries on. I am not sure if this is a software or hardware issue, sorry if I am posting in the wrong forum. 



Please watch the video to see what I mean about stopping and going again.

Any inputs/comments greatly appreciated 



TIA



Eric




{% include youtubePlayer.html id="DrucIFsL06g" %}
[https://www.youtube.com/watch?v=DrucIFsL06g&feature=youtu.be](https://www.youtube.com/watch?v=DrucIFsL06g&feature=youtu.be)





**"Kma Jack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 15:32*

Yep, this is stuttering, it is due to a difference in how LaserWeb and Smoothie choose to communicate with each other and it can't send the engraving fast enough. 



At this stage I advise one of these options:



If someone has the GLCD Screen, I advise them to set up the job in LaserWeb, save the GCode file, copy it to the MicroSD Card in the C3D board, and run from the GLCD Menus.



If you do not have the GLCD, you can still execute the file this way using the play command from the bottom right gcode sender in LW:



play /sd/gcodefile.gcode



[smoothieware.org - player [Smoothieware]](http://smoothieware.org/player)



The 3rd option is to flash the alternate firmware grbl-lpc onto the C3D board, and then you would be able stream these raster engravings at light speed.  



See it's not explicitly a hardware nor firmware nor software issue, it's just a difference in how each part chooses to communicate.  I understand the frustration, I really do. 



I think you're pretty close to working though, I recommend Option 2: just save your gcode file from LW, copy it onto the card and run the play command with the name of your gCode file. 


---
**Kma Jack** *August 09, 2017 16:50*

If it's a communication problem than it must be either hard or software issue, what else can it be? 



Don't get me wrong **+Ray Kholodovsky**, I am not having a go at you personally but there has to be something wrong somewhere for all these problems. I have tried Win7 and Ubuntu and have had/have problems with both. Win7 is driver problem but what's the problem with Ubuntu? Why doesn't it just work like the stock cheap Chinese board does? Why doesn't the cheap Chinese board and software have problems communicating with each other? It must make you think why, doesn't it?





Anyway, I tried option two, no joy at all. It either doesn't find the file or just sits there doing nothing at all. 



I can't connect my GLCD because I don't have the convertor that connects to the board. It's a bit too expensive to order it from America so not sure what I am going to do about that yet, might just have to make one myself.





I installed Pronterface in Ubuntu but found out they have changed some file so it won't work. There might be a workaround it but is not guaranteed. 



I am going to try grbl-lpc and if that don't work either, I am done with it. I have wasted enough time already trying this and that and the other. It's really getting beyond a joke! 



If it's not the board then it needs a software that it will communicate with without a problem and understand what it's being told. 



If it is the board then I think I think I will get my sion to send it back for a full refund but doubt very much he'll get the postage cost and the tax he paid for it. So in reality, we are stuck with a dud board, if that is the case! 



If it's the firmware than someone need to revise it and make it better to stop us wasting so much time trying to get it to work.



Sorry Ray, but if you can understand my frustration then you must understand my rant. 






---
**Kma Jack** *August 09, 2017 16:56*

Forgot the pic

![images/1c2519ea7b1825d9916441cf38200ba1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1c2519ea7b1825d9916441cf38200ba1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 09, 2017 17:44*

Before hopping over to grbl-lpc, let's get option 2 working. 

No one is saying you need to get the GLCD or Adapter for it. 



If you are getting file not found, please reset the board and try again.  So:



Make sure you have copied the file over and that you can see it on the memory card. 

Safely eject/ dismount the memory card in your OS.

Disconnect from the COM port in LW. (Comms tab, disconnect button).

Reset board by pressing little yellow button at top right corner of board. 

Wait a moment, then connect back in LW and try the command again. 


---
**Kma Jack** *August 09, 2017 18:05*

Well, I tired the grbl firmware. I downloaded it, copied it on to the SD car, stuck it back in the SD socket, connected the USB card, turned the K40 on and watched the LEDs as mentioned on the web page you and nothing. The only LED on is the red one below the 4 green ones. The green ones don't even light up never mind count in binary and then flash! 



I loaded a file and converted it to gcode and started the print job. The head does what it's supposed to but nothing else id happening, the laser isn't firing. The home button don't work either but I suppose there must be another homing command for it and not the G28.2 used by the other firmware. This may be the case for the laser not firing either, I did try entering M3 and M5 in the relevant boxes under settings/gcode but that didn't help either. 



At least it doesn't stutter and stop at every line now, it stop at every third line instead. 





![images/16fe28bc40dce7212b401f565eea0ab7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/16fe28bc40dce7212b401f565eea0ab7.jpeg)


---
**Jim Fong** *August 09, 2017 18:27*

Grbl-lpc requires. $H for gcode homing instead of g28.2.   Change that in Laserweb4 gcode settings





See this thread on setting up grbl laser firing



[plus.google.com - John Milleker Jr. - Google+](https://plus.google.com/+JohnMillekerJr/posts/9Cy34iFdqEa)


---
**Kma Jack** *August 09, 2017 19:16*

Thanks for that **+Jim Fong**. It now homes and fires but now I have a different problem. The head stops moving about every third line but burns a hole where it stops because it doesn't turn the laser off. 



I've entered M3 and M5 in the setting but didn't help. Do M3 and M5 work in grbl? 








---
**Jim Fong** *August 09, 2017 19:27*

**+Kma Jack** see my first post in the above thread. You need m4 s0 in the gcode start.  


---
**Kma Jack** *August 09, 2017 20:23*

Thank you **+Jim Fong**, will add that to the gcode.





What's that about "$30=0" you mentioned in that thread? Is it something I have to change as well and if so where do I find it please?




---
**Jim Fong** *August 09, 2017 20:45*

**+Kma Jack** that's is part of grbl-lpc configuration.  It should default to 1000. You don't need to touch it unless you want to fine tune grbl.  



Read up on grbl configuration if you want to know how and what the different $$ config does. 

[https://github.com/grbl/grbl/wiki](https://github.com/grbl/grbl/wiki)



[https://github.com/gnea/grbl-LPC](https://github.com/gnea/grbl-LPC)


---
**Kma Jack** *August 09, 2017 21:23*

I added the M4 S0 in to the gcode start and it did the trick. it's not burning a hole when it stops now.



Thanks for your help and the link, I'll go and read it, might need it in the future. 


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/WmA6QgufiWV) &mdash; content and formatting may not be reliable*
