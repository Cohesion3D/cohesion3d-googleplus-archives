---
layout: post
title: "I'm up and running! whoohoo! Just ran a cut from LW4, and it's great - except..."
date: June 13, 2017 16:29
category: "Laser Web"
author: "Dafydd Roche"
---
I'm up and running! whoohoo! Just ran a cut from LW4, and it's great - except...

When I do a test laser, I config for ~14mA or so (on the analog dial). But when I do a cut, and set LW4 to cut at 100% power, it's averaging less than 10mA.

Is there a setting that's limiting the PWM rate or something? What have I missed here?





**"Dafydd Roche"**

---
---
**Dafydd Roche** *June 13, 2017 16:34*

Wait. I think I found it in the config file ont he SD card. laser_module_max_power, right? Gonna try it out now :)




---
**Joe Alexander** *June 13, 2017 16:50*

It's always great when people answer their own questions! :)


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 17:01*

By default, the C3D Mini will limit the power output to 80%. You want to set the potentiometer to the upper threshold of the tube (or maybe just below that), and let the controller and LW do the power management for you.


---
**Dafydd Roche** *June 13, 2017 17:08*

Joe - thanks. 

Ashley - I like to set the max at about 14mA or so. I know it'll never go over that then. I'm told the K40 tubes lifetime doesn't really do well over 14mA! :)


---
**Joe Alexander** *June 13, 2017 17:23*

max recommended power is 23ma and its best to stay below 16ma, I regularly cut material at 15ma and it runs great.


---
**Dafydd Roche** *June 13, 2017 17:24*

Thanks Joe. 


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 17:26*

So do that ... run a cut job, and while it's running, adjust the potentiometer till you get your desired 14mA. Then leave it alone. Use the power settings in LW to change it (lower) when you need to. And if you get to a point where it won't go any higher despite turning the knob higher, then start playing with the max power on the config file. Go in small increments, like 0.8, 0.85, 0.875 ...


---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 18:19*

I'm good with people increasing their max values to 1, as long as they understand not to turn the pot up all the way and what the recommended max ranges are. 


---
**Dafydd Roche** *June 14, 2017 01:23*

mine needs all the power it can take. it currently takes 3 passes to cut through 3mm ply at 14mA...




---
**Joe Alexander** *June 14, 2017 01:49*

what speed?




---
**Dafydd Roche** *June 14, 2017 01:53*

i was running 700 or so... i could run slower i guess. no air assist either.




---
**Joe Alexander** *June 14, 2017 02:20*

im guessing thats mm/m not mm/s right? cause i cut 3mm birch ply 8mm/s@15ma with air assist and cuts in one pass(or 2 passes @10ma). double check your focus height with a ramp test and did you try flipping the focus lens yet? 


---
*Imported from [Google+](https://plus.google.com/113228590072160389068/posts/CuAW28fc8Yn) &mdash; content and formatting may not be reliable*
