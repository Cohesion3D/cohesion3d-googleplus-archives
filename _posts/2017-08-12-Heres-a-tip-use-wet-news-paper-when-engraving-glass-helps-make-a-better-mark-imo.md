---
layout: post
title: "Here's a tip use wet news paper when engraving glass helps make a better mark imo"
date: August 12, 2017 04:17
category: "K40 and other Lasers"
author: "William Kearns"
---
Here's a tip use wet news paper when engraving glass helps make a better mark imo.

![images/5f3c0700bf2bf340f9f19a315d1ade46.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5f3c0700bf2bf340f9f19a315d1ade46.jpeg)



**"William Kearns"**

---
---
**Marc Miller** *August 12, 2017 04:25*

Interesting.  Any guesses as to why?




---
**William Kearns** *August 12, 2017 04:33*

Transfer of heat and how it dissipates when it his the glass I believe glass has tiny explosions and this helps soften them I could be wrong


---
**Ned Hill** *August 12, 2017 11:55*

Nice #K40ShopTips


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/MWNrHJuTM19) &mdash; content and formatting may not be reliable*
