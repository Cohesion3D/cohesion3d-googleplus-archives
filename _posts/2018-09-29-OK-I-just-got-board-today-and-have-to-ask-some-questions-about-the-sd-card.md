---
layout: post
title: "OK, I just got board today and have to ask some questions about the sd card"
date: September 29, 2018 16:54
category: "General Discussion"
author: "Craig Swineford"
---
OK, I just got board today and have to ask some questions about the sd card. I read that in order to use the a axis for my rotary I have to have different files on my card.

1. Do I delete the existing file and put the new ones on or do I add the new ones?



2. My computer won take a card that small. What should I do now?





**"Craig Swineford"**

---
---
**Tech Bravo (Tech BravoTN)** *September 29, 2018 18:12*

1. [lasergods.com - C3D 4 Axis Firmware & Config](https://www.lasergods.com/c3d-4-axis-firmware-config/)



2. get a microSD to SD adapter or use a multi card reader to attach the microSD card to your computer.


---
*Imported from [Google+](https://plus.google.com/103069230675751952794/posts/iGtsg6LeKx9) &mdash; content and formatting may not be reliable*
