---
layout: post
title: "Quick question regarding laser tube wiring. I've finally figured out what I was doing wrong in software to get my K40 cutting well"
date: April 05, 2018 20:17
category: "K40 and other Lasers"
author: "Gary Helriegel"
---
Quick question regarding laser tube wiring.



I've finally figured out what I was doing wrong in software to get my K40 cutting well. However, I initially thought my tube was bad. I got a replacement tube from lightobject.  The replacement came pre-wired, whereas the particular K40 I have expects you to wrap the wire around the post on the tube. Doing some searching online to figure out how to connect the wire together safely (since this is the HV red tube line), I found that there is a 'standard' connector for the purpose ([https://www.amazon.com/voltage-connector-between-cutting-machine/dp/B01N2XOOYN/ref=sr_1_1?ie=UTF8&qid=1522959243&sr=8-1&keywords=high+voltage+laser+connector&dpID=51tSDSKerdL&preST=_SY300_QL70_&dpSrc=srch](https://www.amazon.com/voltage-connector-between-cutting-machine/dp/B01N2XOOYN/ref=sr_1_1?ie=UTF8&qid=1522959243&sr=8-1&keywords=high+voltage+laser+connector&dpID=51tSDSKerdL&preST=_SY300_QL70_&dpSrc=srch)). Unfortunately, I can't find one of these from a US seller, so I'll be waiting basically 2 weeks to get one, unless I pay like $40 in shipping.



Is there another accepted 'safe' way to join two of the HV lines together, such as a ceramic wire nut or something, or will I simply have to wait for the part? I'd prefer to keep the new tube, even if the old one still has some life in it, simply because the old one seemed to emit 2 slightly divergent beams- noticeable as 2 holes in the tape when aligning mirrors, but I want to connect it safely.





**"Gary Helriegel"**

---
---
**Anthony Bolgar** *April 05, 2018 20:22*

Slide a piece of silicon tubing over one of the wires,Twist the ends together in a mechanical joint, cover in silicone caulk, and slide the silicon tube over the joint (You can omit the tubing if you do a good job of covering the joint in silicon caulk)




---
**Joe Alexander** *April 05, 2018 20:30*

be smart and order a couple since you have to wait for the slow shipping, it will allow you to prepare a tube ahead of time. however anthony's idea will get you up and running, just ensure that there is no chance of it arcing to chassis.


---
**Gary Helriegel** *April 05, 2018 20:32*

Thanks for the quick replies- Will be ordering a few, but the silicon is nice and quick and will get me up and running for the weekend. Do I have to wait for it to fully cure  before powering on, or just the 'initial' cure time?


---
**Anthony Bolgar** *April 05, 2018 20:41*

24 cure is what I use.


---
**Joe Alexander** *April 05, 2018 20:45*

i'd give it a full 24 hours, better safe than sorry




---
**Don Kleinschnitz Jr.** *April 06, 2018 00:01*

I got that connector from eBay...don't recall it taking long. 


---
*Imported from [Google+](https://plus.google.com/118018140785058364487/posts/XLWE574JFvt) &mdash; content and formatting may not be reliable*
