---
layout: post
title: "I am about to install a new C3D mini board in my K40 laser cutter, powering it with a separate 24V PSU unit from that of the K40"
date: June 22, 2018 22:32
category: "K40 and other Lasers"
author: "Maurizio Martinucci"
---
I am about to install a new C3D mini board in my K40 laser cutter, powering it with a separate 24V PSU unit from that of the K40.  The C3D will then power and control the motors and the endstops via external PSU, however it must still be connected to the K40 PSU for controlling the laser.  I traced and checked all the connections involved and this (see pictures) seems to me the correct configuration. I'd appreciate your comments and/or warnings before I proceed with this operation.  Thank you!  



![images/4391a02398548ab3b4ee2f4e6b486ba3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4391a02398548ab3b4ee2f4e6b486ba3.png)

**"Maurizio Martinucci"**

---
---
**Anthony Bolgar** *June 22, 2018 23:26*

That looks correct to me. **+Ray Kholodovsky** does that seem right to you?


---
**Roger Kolasinski** *June 23, 2018 00:57*

I only ask this because of some familiarity with Arduino... Isn't it good practice when using two power supplies in a circuit to tie the grounds together?


---
**Anthony Bolgar** *June 23, 2018 01:10*

They are sharing the ground via a trace on the C3D board. The molex connector ground is directly connected to the screw terminal ground.




---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 01:39*


{% include youtubePlayer.html id="9PrlO7-683k" %}
[youtube.com - Using an External 24v PSU on a K40 with Cohesion3D Mini Controller](https://youtu.be/9PrlO7-683k)


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 01:40*

connect the external 24v psu to the screw terminals and cut the +24v wire at the k40 stock lpsu


---
**Maurizio Martinucci** *June 23, 2018 07:11*

Thanks  **+Tech Bravo** , in fact there was a mistake in the picture / scheme I posted earlier: the GND cable is NOT the black one but the white one, and the black one is the +24V, which is very misleading because usually black is for ground... attached a new pic with the correct version of the connections. I am doing a final check with a multimeter on the actual machines later today.


---
**Maurizio Martinucci** *June 23, 2018 07:12*

![images/c1784fb8448aec11400d40abd1874cdf.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c1784fb8448aec11400d40abd1874cdf.png)


---
**Tech Bravo (Tech BravoTN)** *June 23, 2018 07:12*

**+Maurizio Martinucci** yeah they use whatever they have laying around i think. No real standardization 


---
**Maurizio Martinucci** *June 23, 2018 07:13*

![images/bdd8d5ff27d658b5edbfeca05ad42f14.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bdd8d5ff27d658b5edbfeca05ad42f14.png)


---
**Maurizio Martinucci** *June 23, 2018 07:13*

![images/427c5644a8a26f66e932b2b35bbb0608.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/427c5644a8a26f66e932b2b35bbb0608.png)


---
**Maurizio Martinucci** *June 23, 2018 09:02*

indeed!  The very same connection is also shown here >> [https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)


---
*Imported from [Google+](https://plus.google.com/117027581754233433770/posts/fBHyiFiRUB8) &mdash; content and formatting may not be reliable*
