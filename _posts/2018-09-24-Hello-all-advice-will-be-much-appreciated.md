---
layout: post
title: "Hello all, advice will be much appreciated"
date: September 24, 2018 15:45
category: "C3D Mini Support"
author: "chris b"
---
Hello all, advice will be much appreciated. Setting up my new 3020D and want to fit the c3d from my old machine(used the ribbon connector),  I'm posting pictures of the current board and PSU as I'm struggling to get the wiring sorted out. 

The connector with the two blue and 2 black wires actually is a 3pin and not sure about the two brown that are split off to the red. 

  Help much appreciated





**"chris b"**

---
---
**Joe Alexander** *September 24, 2018 15:46*

post some pictures, else its up to us to imagine it and that never works out the way you want :P


---
**chris b** *September 24, 2018 16:01*

Really sorry didn't post pictures, combo of tiredness and self frustration. 


---
**chris b** *September 24, 2018 16:01*

![images/1559ddb754ef2ba39fcdd60958b0318b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1559ddb754ef2ba39fcdd60958b0318b.jpeg)


---
**chris b** *September 24, 2018 16:02*

![images/57845a3566c31dbc7a6cec28cf9c86b0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57845a3566c31dbc7a6cec28cf9c86b0.jpeg)


---
**Joe Alexander** *September 24, 2018 16:38*

Where do those 2 brown wires run off to? That connector with the 2 blue and 2 black should be your x and y limit switch wires(with a gnd and a signal wire going to each switch). The ribbon wire isn't used but that makes it easier in my mind as the stepper wires are on their own connector and easy to swap over(the two 4-pin connectors in pic one bottom middle) and theone bottom right would be your 5v, 24v, gnd, and LO from the laser PSU. The "LO" wire is the one you want to toggle to turn the laser on and off so it goes to the appropriate mosfet on the C3D. And if your using an external 24V PSU then you wont need that wire from the laser psu(max current of 1amp or it could blow the psu) does that help?


---
**Ray Kholodovsky (Cohesion3D)** *September 24, 2018 18:59*

One literally unplugs the 4 cables from the m2nano board and plugs them into the C3D. It is shown in our install guide. 


---
**Kelly Burns** *September 24, 2018 22:18*

This should be the simplest install with the C3D.  Literally plug and play.  



Edit.  Didn’t see rays comment.  Yield to him.  


---
**Ray Kholodovsky (Cohesion3D)** *September 24, 2018 22:21*

I'm perfectly happy to have you all hold the stop and go signs up on my behalf. 


---
**Joe Alexander** *September 24, 2018 22:36*

hey when one gets bored he directs the sheeple :P


---
**Ray Kholodovsky (Cohesion3D)** *September 24, 2018 22:38*

Now hiring for C3D crossing guards!  You have to pay for your own uniform but the cattle prod will be provided. 


---
**Kelly Burns** *September 25, 2018 02:11*

I don’t mind giving input, but I basically was saying the same thing.  I should read all comments before posting. 


---
**chris b** *October 04, 2018 18:10*

**+Joe Alexander** sorry didn't answer, have had an epicly crap week.

 The 2 brown wires spliced into the 24v and belong with the 2 blue 2 brown from the big green endstops. 

![images/39745085f4f1c480bb9e41500b9b5761.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/39745085f4f1c480bb9e41500b9b5761.jpeg)


---
*Imported from [Google+](https://plus.google.com/116957066831422624000/posts/HsahYCohMri) &mdash; content and formatting may not be reliable*
