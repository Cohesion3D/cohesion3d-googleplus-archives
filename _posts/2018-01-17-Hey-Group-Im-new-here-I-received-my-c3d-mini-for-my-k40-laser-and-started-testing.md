---
layout: post
title: "Hey Group, I'm new here. I received my c3d mini for my k40 laser and started testing"
date: January 17, 2018 14:44
category: "C3D Mini Support"
author: "Joe Wei"
---
Hey Group, I'm new here. I received my c3d mini for my k40 laser and started testing. It boots fine. Display and controls are working. Both the x and y stepper move in the right direction and referencing the machine is also working. 

So far so good i don't understand how to connect my laser supply. Coming from a marlin setup i have two lines. One is the laser enable and one is the pwm for the laser intensity. Why do i see pictures of c3d mini users still having connected the crapy chinese intensity poti? 





**"Joe Wei"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2018 14:46*

Our default wiring leaves a pot in play so that you can set your max power there and then the board will set a % of that. You get maximum grayscale contrast for engraving this way. 



Pictures of your wiring please. 


---
**Joe Wei** *January 17, 2018 15:25*

Ok here is a picture of my powersupply. Right above the <b>text</b> button (which means test shot) are shorts for the pins of the connector. I need to connect the L for laser enable and the IN which is pwn in. AFAIK

I´ve got the connector for the 3 pin connector which is labled K40 PWM on hand. TIA

![images/b62371e7511dc2ef42a64bd65d51d41c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b62371e7511dc2ef42a64bd65d51d41c.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2018 15:40*

The wiring that typically gets connected to the C3D is from the far right terminal, 24v, gnd, and L. 


---
**Joe Wei** *January 17, 2018 15:56*

My description seems correct [https://www.ebay.com/itm/40W-PSU-CO2-Laser-Power-Supply-Power-Source-110V-220V-Switch-Green-MYJG-40T-/122119865571](https://www.ebay.com/itm/40W-PSU-CO2-Laser-Power-Supply-Power-Source-110V-220V-Switch-Green-MYJG-40T-/122119865571) has a description with the functions of the pins: 



 G Signal Ground

The foot must connected with laser machine's enclosure and ground properly.



P Input Signal

Water Protction Switch.



L Input Signal

The control of laser: <b>Low Level, emitting laser</b>



G Signal Ground

The foot must connected with laser machine's enclosure and ground properly.



IN Input Signal

Laser Power supply control terminal can use 0-5V Analog Signal Control,also can use <b>5V PWM Signal to Control</b>



5V Output Power

5V,Max output Current 50mA 



Where do i connect the two wires to?


---
**Rich Simons** *January 18, 2018 02:05*

**+Joe Wei** I too would like to see a detailed schematic.


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 02:08*

We have a PWM output option on the Mini that we used to use, it is the center pin of p2.4 fet and you can run this to IN, then change the config file accordingly. 



There is a pinout diagram on our docs site. 



I am still recommending you install a (good quality) pot to Gnd, IN, 5v and run 24v, GND, L to the Mini as everyone else does. 


---
**Rich Simons** *January 18, 2018 02:47*

**+Ray Kholodovsky**   Hi Ray, I want to do K40 Nano-M2 Mini swap but I'm very new to this.  Please don't assume I know what you mean by "as everyone else does".  Are you saying to use a pot to control PSU-IN (which sets max power?) and Mini(some_control_line) -> PSU-L (to dynamically control power percent during operation?)?  If this is clearly laid out somewhere, I'd like to see it.  Or does everyone just know how it is done (except me)?  Thanks, Rich


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2018 02:49*

Hi Rich, 



Joe is asking me very specific questions about a previously modified no longer stock k40 laser. 



You should just read the guide at Cohesion3D.com/start



You don't have to do all these things, just swap the cables and off you go. [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Joe Wei** *January 18, 2018 20:40*

**+Ray Kholodovsky** 

Hi Ray, thanks for your support. I dug into the config file. But I`m needing info on the pinout of the C3D mini. The output which is labeled L(Laser fire) on the 4 pin connector is not labeled. Is it 2.6, 2.5 or something else?

I`ve wired my L to the L(Laser Fire) and my IN to 2.4. When i send G1 X100 i can see pwm on my scope, but L doesn`t get pulled low.

[smoothieware.org - laser [Smoothieware]](http://smoothieware.org/laser) shows configurations which seem to be different to the config file you supplied. Is this stock smoothie?

They use laser_module_pwm_pin you use laser_module_pin 

They use laser_module_ttl_pin you have switch.laserfire.enable 

switch.laserfire.output_pin 

switch.laserfire.output_type 

switch.laserfire.input_on_command 

switch.laserfire.input_off_command  which I can`t find in the smoothie pages. 




---
**Joe Wei** *January 20, 2018 20:59*

OK then I think I figured it out. 



You use the L (Laserfire) Pin to send the PWM to the laser thus regulating the emitted power by switching the laser on / off / on / off... , not by regulating the amperes send to the tube. That is done by the user via the potentiometer.

This solution leaves the original testfire button intact. But imho leaving the regulation to the pot means no consitent results over time.



I wired the L (Laserfire) to my L and the middle pin of the connector to the PWM in of my laser suply completly eliminating the pot.

My config.txt regarding the laser part now looks like this:



## Laser module configuration

laser_module_enable    true

laser_module_pwm_pin                   2.4!  

laser_module_ttl_pin                        2.5

laser_module_maximum_power    0.8 

laser_module_minimum_power     0.0        

laser_module_pwm_period             200   



I found using the test shoot facility of the firmware is really dangerous as it doesn`t get scaled down by the value of laser_module_maximum_power so that full power means full 5 V, half power a duty cycle of roughly 50% and so on.



Changing the S value during a job doesn`t affect the laser power instantly but after a few more finished lines during the job. 



I hope changing the Set power scale will allow me to reproduce fine details during engraving.



Do you have any experience which solution is the one which produces the best results? Your standart one leaving the pot in seems to me beeing seamless and not so error prone.

Can you tell me if I got something wrong?

Which signals are on the other two pins of the connector carrying 2.4? I`m thinking about using coax for the PWMnd line. Any thoughts on that?

Please excuse my english as it`s not my motherlanguage.

Rich Simons does that answer your question?

Edit: Seeing the picture on the left I just figured that many don`t seem to have the lcd connected and so can`t access the menu items i described.


---
*Imported from [Google+](https://plus.google.com/103807439630544304520/posts/PDcHUBGrqxs) &mdash; content and formatting may not be reliable*
