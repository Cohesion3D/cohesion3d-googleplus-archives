---
layout: post
title: "It appears this latest forced windows 10 update is wreaking havoc on my cohesion3d mini used with my laser cutter"
date: December 30, 2017 19:55
category: "C3D Mini Support"
author: "Todd Mitchell"
---
It appears this latest forced windows 10 update is wreaking havoc on my cohesion3d mini used with my laser cutter.



I get the following in device manager "Unknown USB device (device descriptor request failed)" and cannot connect to the laser cutter.



Tried the usual reboot, unplug, uninstall, <b>.</b>.  Anyone else bumping into this?  Ironically, the windows update was named 'Windows 10 Fall Creators Update'





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 19:58*

Did this upgrade you from an earlier OS (Win 7, 8) to Win 10 ?


---
**Todd Mitchell** *December 30, 2017 20:03*

**+Ray Kholodovsky** Nope.  It's always been windows 10


---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 20:05*

Try Lightburn. 


---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 20:05*

[lightburnsoftware.com - Trial version - Try before you buy](https://lightburnsoftware.com/pages/trial-version-try-before-you-buy)


---
**Todd Mitchell** *December 30, 2017 20:08*

I'm happy to try it yet I'm unsure how this is going to resolve the fact the OS doesn't recognize the device?  I also tried cncJS which would not show it either


---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 20:11*

I wanted to check whether it was related to a specific software or not. 



What are the LEDs on the board doing?


---
**Todd Mitchell** *December 30, 2017 20:12*

**+Ray Kholodovsky** Lightburn shows 'waiting for connection' and there is no response on the machine from the move commands or get position button.



The LEDs show solid - green, green, red


---
**Todd Mitchell** *December 30, 2017 20:32*

**+Ray Kholodovsky** It's working now.  The reset button was not working so i pulled the SD.  after reinserting it, the device was registered and functioning like normal.   Thanks again dude.




---
**Todd Mitchell** *December 30, 2017 21:02*

**+Ray Kholodovsky** - BTW: who is building that light burn software?  smooth interface and good price (@30usd) tho laserweb still meets my needs.



Know of anything like lightburn for cnc?  the layering part of composing a cut is a super cool feature.


---
**Ray Kholodovsky (Cohesion3D)** *December 30, 2017 21:13*

**+LightBurn Software** , a very smart individual that is open to suggestions. 




---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/iAfqkbtsy3S) &mdash; content and formatting may not be reliable*
