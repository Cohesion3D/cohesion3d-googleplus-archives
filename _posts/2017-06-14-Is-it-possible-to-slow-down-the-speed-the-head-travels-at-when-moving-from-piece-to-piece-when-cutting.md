---
layout: post
title: "Is it possible to slow down the speed the head travels at when moving from piece to piece when cutting?"
date: June 14, 2017 17:11
category: "C3D Mini Support"
author: "Adam J"
---
Is it possible to slow down the speed the head travels at when moving from piece to piece when cutting? I'm cutting symmetric shapes and think it may be messing up my shapes when the head darts from piece to piece?  





**"Adam J"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 17:12*

That would be the "travel speed" setting in LW or whatever software you are using. 


---
**Adam J** *June 14, 2017 17:32*

I'm using LW, I'm having trouble finding that setting. Thanks Ray.


---
**Todd Fleming** *June 14, 2017 17:46*

LW uses firmware's default G0 feed


---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 17:48*

Ah ok. In that case, we can make this change in the config.txt file. It's at the top, called travel or seek (for G0 moves), and I believe the default value is 4000. That's in mm/min. 


---
**Adam J** *June 14, 2017 20:56*

The reason I ask this is because I'm getting a lot of "spikes" on what should be a smooth edge. Hopefully this picture captures it, the white bump along the edge is when the head moves to cut the next piece, the "entry point" I guess you could call it. Tried slowing down G0 moves (seek) to 1000 but still having problems. Tried moving the objects further apart to no avail. Any ideas? Sorry if I should be posting this over at LW group...?

![images/fbe59071a9db63ad54fcebf290fa3bdb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fbe59071a9db63ad54fcebf290fa3bdb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 22:02*

I'd actually go to the K40 group with this question.  Lots of variables on the k40 side here that they can account for better than I can. 


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/B7cr1F3n5cT) &mdash; content and formatting may not be reliable*
