---
layout: post
title: "just received my cohesion3d laserboard and I installed it being careful to follow every step....I also purchased the lightburn software along with it...ive installed the board...double and triple checked all wires...plugged"
date: January 14, 2019 21:05
category: "General Discussion"
author: "steve juchemich"
---
just received my cohesion3d laserboard and I installed it being careful to follow every step....I also purchased the lightburn software along with it...ive installed the board...double and triple  checked all wires...plugged it in turned it on and I have only 1 red light and lightburn says disconnected....am I doing something wrong?...am I suppose to do something else...the sd card is installed also...please help...thanks





**"steve juchemich"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 21:20*

Please show clear pictures of your board, wiring, and machine. 


---
**steve juchemich** *January 14, 2019 21:37*

im using windows 10 ..I have followed all instructions to the T....and according to those the leds on the board should be either flashing or solid...I have only one red solid...ive now ran through the hook up once again and downloaded lightburn just in case it nay have downloaded improperly...rebooted my computer and turn the power on to the laser and board....still...only one solid red light....


---
**steve juchemich** *January 14, 2019 21:47*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646472529223442065/6646472530919650370)


---
**steve juchemich** *January 14, 2019 21:48*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646472613595574449/6646472611806494274)


---
**steve juchemich** *January 14, 2019 21:48*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646472694844790833/6646472696345679474)


---
**steve juchemich** *January 14, 2019 21:50*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646473123802993345/6646473124124696466)


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 21:51*

Power off.  Unplug the "endstop" cable that has the 2 green and 2 yellow wires.  Power the board up again. 


---
**steve juchemich** *January 14, 2019 21:53*

that did it


---
**steve juchemich** *January 14, 2019 21:54*

can I plug the endstops back in again now


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 21:59*

No. Your endstop cable is wired differently and was causing a short on the board. I need you to trace those wires and see what you can find out about the switches on the other end. 


---
**steve juchemich** *January 14, 2019 22:10*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646478337886696289/6646478338996230514)


---
**steve juchemich** *January 14, 2019 22:10*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646478453028430193/6646478454757720306)


---
**steve juchemich** *January 14, 2019 22:11*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646478566045234497/6646478564098383010)


---
**steve juchemich** *January 14, 2019 22:11*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646478697647035953/6646478699284036402)


---
**steve juchemich** *January 14, 2019 22:15*

3 spades on them for n/o or n/c I believe


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 22:19*

So are the yellow wires the X axis switch and the green wires the Y axis switch? 


---
**steve juchemich** *January 14, 2019 22:20*

wire closed ..top spade has a C middle NO


---
**steve juchemich** *January 14, 2019 22:25*

well if x is width then green is x


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 22:30*

Ok.  Are you able to insert the plug 180 degrees from how it was? 

It is a polarized connector that is meant to prevent you from doing this. That said, you might be trim the polarizing bits that stick out on connector with the wires that prevent you from plugging int the other way.  After this it might go in naturally with less force, and that would save you any rewiring or crimping. 






---
**steve juchemich** *January 14, 2019 22:32*

**+Ray Kholodovsky** ….ill give it a whirl...and report back in a few


---
**steve juchemich** *January 14, 2019 22:50*

**+Ray Kholodovsky** ok...I got it flipped...should I power it on?


---
**steve juchemich** *January 14, 2019 23:01*

I know this is jumping ahead...but while I got you can I just plug my rotary into the a axis on the board...here is a picture of the plug and rotary in the background..i know...one thing at a time...answer when we are done with the current switch problem...thanks a bunch


---
**steve juchemich** *January 14, 2019 23:02*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646491755696816881/6646491755498316690)


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 23:06*

In case my "+1" was insufficient, yes you can proceed with power up. 



One thing at a time :) 



Depends on how big the rotary motor is - what are the specs on it?


---
**steve juchemich** *January 14, 2019 23:20*

**+Ray Kholodovsky** ..ok I powered up and I have green lights flashing and 2 solid green  and that solid red....lightburn still says disconnected


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 23:23*

Now send a screenshot of your entire LightBurn window.


---
**steve juchemich** *January 14, 2019 23:33*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646499664012967057/6646499663639035986)


---
**steve juchemich** *January 14, 2019 23:50*

**+Ray Kholodovsky** ...should I reboot maybe


---
**Ray Kholodovsky (Cohesion3D)** *January 14, 2019 23:59*

Check Devices and Printers to see if you see the "Smoothieboard" entry and a com port if you double click the Smoothieboard entry and go to the 2nd tab.  



Check the contents of the SD Card on your computer to see if the computer can see it and the firmware and config files are there.  



Send screenshots. 


---
**steve juchemich** *January 15, 2019 00:08*

**+Ray Kholodovsky** ..its listed under blue tooth and other devices

**+Ray Kholodovsky** ...sd card has config and firmware files on it




---
**steve juchemich** *January 15, 2019 00:09*

if I click on the smoothieware under other devices it only gives me the option to delete or remove it


---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2019 00:10*

Go ahead and delete the files on the card and put fresh ones on just to rule out any corruption issues from your previous short. 



[dropbox.com - laserboard stock files smoothie batch 1](https://www.dropbox.com/sh/a7q9fzwc8x9xoje/AACXAvIufHBeUzVT3XSodbZRa?dl=0)



Put the card back into the board with the board off.  Then power up the board.  The LEDs should count up before flashing as usual. 


---
**steve juchemich** *January 15, 2019 00:11*

how and where do I get fresh ones


---
**steve juchemich** *January 15, 2019 00:23*

**+Ray Kholodovsky** done...but I didn't see a count down


---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2019 00:27*

Check the card again, and you need file extensions enabled.  Is the file firmware.bin or FIRMWARE.CUR ?


---
**steve juchemich** *January 15, 2019 00:31*

.cur


---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2019 00:36*

To be clear, you deleted the existing files, put new files including a new firmware.bin, and after turning on the board it is now FIRMWARE.CUR? 



This is all proper functioning and great. 


---
**steve juchemich** *January 15, 2019 00:42*

**+Ray Kholodovsky** ..yes...the file date is 1-14-19 also on both files just to make sure


---
**steve juchemich** *January 15, 2019 00:43*

so....why is lightburn not responding to the board


---
**steve juchemich** *January 15, 2019 00:58*

**+Ray Kholodovsky** ...smoothieboard is listed under devices and printers 


---
**steve juchemich** *January 15, 2019 01:18*

**+Ray Kholodovsky** ….I uninstalled lightburn and then reinstalled it and it still will not connect


---
**steve juchemich** *January 15, 2019 01:22*

starting to think this was one of my worst decisions ever


---
**steve juchemich** *January 15, 2019 01:57*

anyone with any ideas on why I cannot get lightburn to connect?......please help..seems like I wasted money at this point...kinda frustrated 




---
**Ray Kholodovsky (Cohesion3D)** *January 15, 2019 02:19*

I’ll give it some thought and get back to you. Enjoy your evening! 


---
**LightBurn Software** *January 15, 2019 02:22*

Show the console window - what does it say?


---
**LightBurn Software** *January 15, 2019 02:25*

I will say that your experience is extremely uncommon - 99% of the time you plug it in and it just works. For the cases where it doesn’t, there’s almost always an explanation for why, it just takes a bit of back and forth to figure out why.


---
**steve juchemich** *January 15, 2019 02:26*

disconnected


---
**steve juchemich** *January 15, 2019 02:27*

waiting for connection


---
**LightBurn Software** *January 15, 2019 02:27*

When I say “show the console window”, I mean, take a screen shot of it and post that.


---
**steve juchemich** *January 15, 2019 02:29*

ok..hang on


---
**LightBurn Software** *January 15, 2019 02:30*

In the Laser window where it says “C3D (Smoothie)” - right beside that is a box that says “auto”.  If you click that to expand it, does it say anything else? It should list the COM port that Smoothie is connected to. If it doesn’t, the board isn’t connecting to the computer properly.


---
**steve juchemich** *January 15, 2019 02:34*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100956401494105135927/albums/6646546503817110033/6646546504041489826)


---
**steve juchemich** *January 15, 2019 02:38*

it says com3


---
**LightBurn Software** *January 15, 2019 02:39*

Do you have anything else plugged in that might be COM3?  If not, what happens if you just select that?


---
**steve juchemich** *January 15, 2019 02:40*

I clicked on com 3 and the laser came all the way up to the front and started chattering


---
**LightBurn Software** *January 15, 2019 02:41*

Well, that means they’re talking to each other, but you probably have the limit switches or homing config set wrong. The board is configured for a stock K40. If that’s not what you have, there will be a little bit of config editing to get set up properly.


---
**LightBurn Software** *January 15, 2019 02:42*

Actually, if it came up to the front, you may just have the Y motor leads plugged in backwards, or the Y stepper reversed in your motor driver.


---
**LightBurn Software** *January 15, 2019 02:43*

Ray will be better at this part, but you have the board talking to the machine, and if it tried to home, it means LightBurn sent a command and the board acted on it, so you’re close.


---
**steve juchemich** *January 15, 2019 02:45*

this is a 300 x 500 and that's what I set lightburn for on setup


---
**steve juchemich** *January 15, 2019 02:46*

this is a laserboard...not the mini....suppose to run even cnc machines if I read it right


---
**steve juchemich** *January 15, 2019 02:46*

this had the M2 nano board in it also


---
**LightBurn Software** *January 15, 2019 02:47*

It will, yes, but it doesn’t just “know” what your machine is. You have to configure it.


---
**LightBurn Software** *January 15, 2019 02:47*

The page setup in LB just tells it how bit the machine is, but doesn’t affect the machine config. You have to do that in the config file on the SD card.


---
**steve juchemich** *January 15, 2019 02:50*

**+LightBurn Software** ..I do realize that....and I thank you for your help...can you point me to where I need to configure the drive


---
**steve juchemich** *January 15, 2019 02:58*

**+LightBurn Software** ...sorry...I have a delay in the messages on here...I have to keep refreshing just to see if anyone responded...im on two different computers trying to figure this out......


---
**LightBurn Software** *January 15, 2019 03:01*

There’s a file called config.txt on the SD card. alpha_max (something?) is the width, and beta_max or travel is the height.


---
**LightBurn Software** *January 15, 2019 03:01*

You could also try the C3D group on Facebook. Probably a little more active than here. G+ is pretty dead now.


---
**steve juchemich** *January 15, 2019 03:20*

**+LightBurn Software** ..thank you...I been at this all day...I may have to get some sleep and start again tomorrow....hoping ray may help me again...haha....


---
**LightBurn Software** *January 15, 2019 03:24*

He will - late for him now too, and rapid-fire frustrated posts tend to stress both of us out a little. :) Try to be patient, and recognize that your machine is different than the standard setup so there will be a bit of back and forth, but between the two of us we’ll get you sorted out.


---
**steve juchemich** *January 15, 2019 14:37*

**+LightBurn Software** ….Good morning and thank you very much...I don't mean to sound frustrated or pushy...I guess im just excited to try and get it running...k40 whisperer has been  a great thing...but I want something a little more powerful...…..again ...sorry for being short if I was....im ready for the next step whenever you guys are......thanks steve


---
*Imported from [Google+](https://plus.google.com/100956401494105135927/posts/8HRNFSxNwqQ) &mdash; content and formatting may not be reliable*
