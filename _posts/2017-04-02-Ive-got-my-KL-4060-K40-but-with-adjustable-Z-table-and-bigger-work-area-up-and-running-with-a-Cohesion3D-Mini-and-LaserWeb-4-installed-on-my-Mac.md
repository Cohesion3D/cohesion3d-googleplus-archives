---
layout: post
title: "I've got my \"KL-4060\" (K40 but with adjustable Z-table and bigger work area) up and running with a Cohesion3D Mini, and LaserWeb 4 installed on my Mac"
date: April 02, 2017 21:31
category: "Laser Web"
author: "Chris Harrison"
---
I've got my "KL-4060" (K40 but with adjustable Z-table and bigger work area) up and running with a Cohesion3D Mini, and LaserWeb 4 installed on my Mac. In LaserWeb I can jog the X and Y axes as expected, and when I press the "Laser Test" button I get the expected laser pulse and a mark on my material.



However, I'm having no luck creating a raster image; the axes move around as expected, but the laser never fires. I've tried with minimum laser power of 100, but to no avail. Are there any good "quick start" guides that users would recommend? The documentation at [http://cncpro.co/index.php](http://cncpro.co/index.php) seems to be a work in progress at the moment.



My smoothie config file has the following parameters in the laser section:



laser_module_enable				true

laser_module_pin				2.4!

laser_module_maximum_power		1.0

laser_module_minimum_power		0.1

laser_module_pwm_period			20



Any hints or tips greatly appreciated.



Thanks.







**"Chris Harrison"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 02, 2017 21:33*

Hey Chris. Based on your laser config I'm guessing you were from the first batch of Minis, the ones that were pre order up to around the new year? It will say v2.2 on the bottom of the board. 



We've got a few things to tweak. Can you show me your wiring please? 


---
**Chris Harrison** *April 02, 2017 21:58*

Please see attached. The end stops and stepper motors seem to be fine, so I'll disregard those. The "K40 Power" connector goes to the power supply on the left. The connections are 24V, GND, 5V and L. The "L" cable has a splice that runs over to the high voltage supply on the right (an MYJG-40), and goes to its "L" (“Low TTL level”) input. I also connected the grounds of both supplies, and removed a cable originally connecting 24V on the left supply to GND on the right (I assumed this was a manufacturing error).



Not shown is a control panel that controls the maximum power output.



I placed my order on January 14th. I can remove it to confirm the version number if that'll help.



Thanks for the assistance.

![images/3313fa1c71ef74cecdcbdc0b9e3195d9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3313fa1c71ef74cecdcbdc0b9e3195d9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 02, 2017 22:03*

Ok so the control panel is still connected, good. 

You need to run L from the Psu to the 2.5 - bed screw terminal. And change the config from 2.4! to 2.5 

Then save, safely eject, and reset the board. 



Try running a G1 X10 S0.5 F600 gCode and observe that it fires while moving. 


---
**Chris Harrison** *April 03, 2017 02:04*

That seems to have done the trick. Thank you very much!


---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2017 02:05*

Woohoo!  


---
*Imported from [Google+](https://plus.google.com/107474226711586520689/posts/gtFRqCUSyVt) &mdash; content and formatting may not be reliable*
