---
layout: post
title: "I'm thinking of buying a Cohesion3D board, but was wondering if it would be possible to use a fhermocouple (through a interface board) for my hotend temperature, instead of a thermistor"
date: March 26, 2017 11:16
category: "General Discussion"
author: "Rien Stouten"
---
I'm thinking of buying a Cohesion3D board, but was wondering if it would be possible to use a fhermocouple (through a interface board) for my hotend temperature, instead of a thermistor.

Anyone done this?





**"Rien Stouten"**

---
---
**Griffin Paquette** *March 26, 2017 11:35*

In a sense yes. You will need to use one of the spi lines on the board though. The SD spi line isn't useable but you could run it instead of the LCD. Here's a link to a little info on the Smoothieware forum on the subject: [forum.smoothieware.org - Temperaturecontrol - Smoothie Project](http://forum.smoothieware.org/temperaturecontrol#toc5)



Let me know if there is anything else I can help you with. 



-Griff


---
**Rien Stouten** *March 26, 2017 12:13*

**+Griffin Paquette** So it wouldn't be possible to use a thermocouple and a lcd at the same time?


---
**Griffin Paquette** *March 26, 2017 14:37*

That is correct **+Rien Stouten** 


---
**Rien Stouten** *March 26, 2017 16:14*

**+Griffin Paquette** In that case, no cohesion board for me. Even the old ramps could do that.


---
**Griffin Paquette** *March 26, 2017 20:05*

**+Rien Stouten** I stand corrected! Read the page wrong. It seems that you can actually plug a thermocouple into the thermistor ports you just need a AD8495 thermocouple amplifier and specify that you are using that in the config. Sorry for the confusion!


---
**Griffin Paquette** *March 26, 2017 20:07*

If you scroll down a little more on the page I sent earlier it should show a section called Thermocouple via Amplifier.


---
**Rien Stouten** *March 26, 2017 22:04*

**+Griffin Paquette** thanks. So if I understand it correctly, I could use a normal thermistor port and use that with a adafruit board?

That would be great. 

Now I only need to choose between the mini and the full-options one.


---
**Griffin Paquette** *March 26, 2017 22:14*

I have personally not done it but as far as I can read yes you are correct with that assumption.



I have always been a fan of the Remix just because of its versatility. It's basically there and ready for any upgrades you might want to do in the future. I've seen both boards inside and out and just can't shake my love for the Remix. If you know that you won't be upgrading your machine, the Mini is good, but if there is any doubt you can't go wrong with the Remix.



Let me know if you need any more help. Always happy to do so.


---
*Imported from [Google+](https://plus.google.com/+RienStouten/posts/2CzsbDj99xx) &mdash; content and formatting may not be reliable*
