---
layout: post
title: "Howdy folks I'm having a problem with head movement with the K40 Here's my setup Cohesion 3D board with glcd screen BIN/CFG files on CD Card Using a simple .svg file from INKSCAPE composed of a large rectangle with 6 inner"
date: February 02, 2017 18:28
category: "General Discussion"
author: "greg greene"
---
Howdy folks I'm having a problem with head movement with the K40



Here's my setup

Cohesion 3D board with glcd screen

BIN/CFG files on CD Card

Using a simple .svg file from INKSCAPE composed of a large rectangle

with 6 inner rectangles - I can email this to anyone who wants to try and run it - I can't get it to attach to this post



Workflow

Open LW3

Open File

Turn on K40

Open connection

generate GCode - I can also send that to anyone

Home head via home button

Jog head to lower left corner

Set zero

Run GCode



Results

Y axis stepper grinds - head does not move

Cut begins - outer rectangle cuts correctly

Ist inner rectangle cuts correctly

Y axis stepper grinds - moves a small amount cuts 2nd rectangle in wrong spot

3 rd rectangle cuts correctly = RELATIVELY to 1st rectangle

but in wrong spot for workpiece.



Observation

There is a problem when the y axis is within 20 - 30 mm of the bottom of the work area - that's when it exhibits the stepper grind



In settings I have the work area set to 230 mm (Y axis) and 300 mm (x-axis)

This is slightly smaller than the actual work area.



If someone can run the job and see if they get the same results it would be a great help in narrowing down the problem.  



Thanks for taking the time to look at this !





**"greg greene"**

---
---
**greg greene** *February 02, 2017 18:36*

here is a picture of the piece I want to make.

![images/7f17b921e3bec1572e862c7180a02cd3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7f17b921e3bec1572e862c7180a02cd3.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 18:43*

And screenshot of the entire laserweb window please.


---
**greg greene** *February 02, 2017 19:00*

Do you want to see the settings? of just the open cutting file and it position on the grid?


---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 19:00*

That 2nd thing.  The main window of LW with the job and the grid. 

Setting can't hurt too, because you had old values for that homing issue.


---
**greg greene** *February 02, 2017 19:01*

That's true, Ok I'll get that done ASAP


---
**greg greene** *February 02, 2017 19:22*

![images/63f1bf514ff8b6e6fbe0543b8465755d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/63f1bf514ff8b6e6fbe0543b8465755d.png)


---
**greg greene** *February 02, 2017 19:23*

![images/9c08d84687c6d912238de1ff3dea13fd.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9c08d84687c6d912238de1ff3dea13fd.png)


---
**greg greene** *February 02, 2017 19:23*

![images/460d52739525de3fac1b97f230f9f240.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/460d52739525de3fac1b97f230f9f240.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 00:29*

Resolved. Use 200 as the Y length. 


---
**greg greene** *February 04, 2017 00:48*

Also check the x axis length - I had it set at 300 - and still got some stepper stalling - changing that to 280 seems to have fixed the problem.

Thanks Ray !


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/TLoRAKHaHez) &mdash; content and formatting may not be reliable*
