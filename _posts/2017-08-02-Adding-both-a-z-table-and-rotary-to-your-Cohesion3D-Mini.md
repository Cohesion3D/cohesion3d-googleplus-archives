---
layout: post
title: "Adding both a z-table and rotary to your Cohesion3D Mini?"
date: August 02, 2017 05:09
category: "C3D Mini Support"
author: "Ashley M. Kirchner [Norym]"
---
Adding both a z-table and rotary to your Cohesion3D Mini? Need a step-by-step guide? We've got you covered! Follow along as I walk you through getting it all wired up:





**"Ashley M. Kirchner [Norym]"**

---
---
**Marc Miller** *August 02, 2017 06:20*

Nice clear write up **+Ashley M. Kirchner**.


---
**E Caswell** *August 02, 2017 13:47*

**+Ashley M. Kirchner** excellent write up, is there any chance you could add a download link as well as the print link please?

Easier to store and access the file on PC rather than print it off and store the stuff in a file in the workshop.


---
**Ashley M. Kirchner [Norym]** *August 02, 2017 19:02*

**+PopsE Cas**, I added a PDF link at the top of the page.


---
**E Caswell** *August 02, 2017 19:30*

**+Ashley M. Kirchner** Thanks, however the link doesn't work

![images/74698bcce18f8708d2e26acc61b6ca66.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/74698bcce18f8708d2e26acc61b6ca66.jpeg)


---
**Ashley M. Kirchner [Norym]** *August 02, 2017 19:33*

**+PopsE Cas**, try it again please?


---
**E Caswell** *August 02, 2017 19:42*

**+Ashley M. Kirchner** Spot on it works now. Thank you. :-)


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Lux9LKUszWC) &mdash; content and formatting may not be reliable*
