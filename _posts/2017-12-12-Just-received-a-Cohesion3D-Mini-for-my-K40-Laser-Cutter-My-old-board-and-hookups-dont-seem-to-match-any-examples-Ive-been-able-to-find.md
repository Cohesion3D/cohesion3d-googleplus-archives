---
layout: post
title: "Just received a Cohesion3D Mini for my K40 Laser Cutter My old board and hookups don't seem to match any examples I've been able to find"
date: December 12, 2017 03:03
category: "K40 and other Lasers"
author: "Dennis Moran"
---
Just received a Cohesion3D Mini for my K40 Laser Cutter

My old board and hookups don't seem to match any examples I've been able to find.

The ribbon cable makes sense.



The 4-Pin connector goes to the Y-Control position?



The other cable is 6-Pin and has 3 wires, Green, White and Pinkish? I'm assuming these are for power but no idea where they might go?

Any help would be appreciated

Thanks

Dennis



![images/648a396b4b85f684df869ddf7c8aeff0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/648a396b4b85f684df869ddf7c8aeff0.jpeg)
![images/9b6fcbfc4e45262cf75add9ea89fa51b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9b6fcbfc4e45262cf75add9ea89fa51b.jpeg)

**"Dennis Moran"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2017 03:08*

That's the much older moshiboard :) 



See if this thread helps you: 



[plus.google.com - Let me start with a big thanks for creating this board! It has renewed my in...](https://plus.google.com/107797353699813204127/posts/JhLrU2nwoaE)


---
**Dennis Moran** *December 12, 2017 05:23*

So it looks like:

Green = Ground

White = 24V

Pink goes to P2.5/BED connector on the Mosfet block?


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2017 13:33*

It seems that way, you may want to confirm the 24v and Gnd with a multimeter 


---
**Richard Vowles** *December 12, 2017 17:45*

I'm pretty sure this is the same as mine. I'm going to actually try and do something with it this holiday!


---
**John Sturgess** *December 12, 2017 21:03*

You can also use this for reference with the older boards.

[plus.google.com - Moshi Board V4.6 Upgrade. I thought I'd post some pictures up of my current c...](https://plus.google.com/u/0/112856126515261932166/posts/iimCuVWJsBk)


---
**Dennis Moran** *December 14, 2017 02:00*

I hooked the board as described above:

Green = Ground

White = 24V

Pink goes to P2.5/BED connector 



Got the software to connect to the K40

Can move head up, down, left and right

Laser test fire works



Just have to learn the new workflow to see if everything else works, pretty new to this...



Thanks to everyone that helped!

Dennis


---
*Imported from [Google+](https://plus.google.com/116921257581951229508/posts/1ujtXtVQKnp) &mdash; content and formatting may not be reliable*
