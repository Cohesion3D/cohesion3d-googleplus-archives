---
layout: post
title: "So I am all installed and up and running, however I'm having some issues"
date: October 02, 2018 20:01
category: "C3D Mini Support"
author: "chris hopkins"
---
So I am all installed and up and running, however I'm having some issues.  The 1st and most important is that my scans look terrible.  Choppy edges and awful resolution.  I'm 99.99% certain it's not mechanical as I have run over 4k jobs on this k40 and it was my highest resolution laser b4 the board switch. I have tried adjusting the potentiometers with no success. I have adjusted line interval, overscan, and reverse interval for hours w no success.  The laser makes a lot of noise while moving now that it didn't b4, like the steppers are struggling or something.  Even cutting vector lines are wavy.  There is zero play in the head & no loose mirrors or lens.  Help!



2nd issue is less concerning.  I have to hit "home" in Lightburn b4 and after anything I do or it just starts the job at the top of the bed or goes cattywhompis, like it has no clue where it is or the origin is lost.



![images/4f9445810acf8f41541395e42f2641c9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f9445810acf8f41541395e42f2641c9.jpeg)
![images/9dd88b90d1b1b58746c5480a0a90587a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9dd88b90d1b1b58746c5480a0a90587a.jpeg)

**"chris hopkins"**

---
---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 20:26*

The stepper drivers on the cohesion3d do cause noisier operation. That is very common. Raster Speeds over 300 or so with smoothie become iffy so you can dial back the speed or use grbl firmware. It processes scans way faster. If you are referring to adjusting the pots on the stepper sticks, put them back as instructed in the install directions and i will link instructions on how to measeure and set vref correctly to match the motors


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 20:27*

As far as wavy vector check to make sure the lens isnt loose in the holder.


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 20:27*

I will also link a video on checking origin and setting a home position, etc... in just a few


---
**chris hopkins** *October 02, 2018 20:41*

Thanks for the response!  



So what are the differences between smoothie and grbl?  If it processes scans faster, why set the board up w smoothie?  I'm assuming maybe both have a downside?  My k40 is just for running raster engraves and 99% on leather, we use our dsp ruida for cutting and more complex jobs.  I just need a reliable setup.  Moshi laser was super easy and produced super high res scans, but would glitch and cut random lines or just stop the job. This is why we switched to c3d mini, for a stable platform.


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 21:04*

yeah smoothie is fine for many people and it's the best "one size fits all" solution. the glcd display isn't compatible with grbl so that may play into it some. Smoothie has known limits for GCode processing throughout (about 800 to 1000 GCode lines per second). grbl does not suffer from this limitation. there are, as with anything, pros and cons. once dialed in the c3d is super stable. if you mostly do raster scans give this a look: [cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)




---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 21:05*

here is how to go back to smoo: [lasergods.com - C3D Standard Firmware & Config](https://www.lasergods.com/c3d-standard-firmware-config/)




---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 21:08*

here's a quick an easy vref video: 
{% include youtubePlayer.html id="AVlee67TQxs" %}
[youtube.com - How to set output current limit on A4988 stepper driver](https://www.youtube.com/watch?v=AVlee67TQxs)


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 21:08*

and here is a more detailed set of instructions on vref: [pololu.com - Pololu - Video: Setting the Current Limit on Pololu Stepper Motor Driver Carriers](https://www.pololu.com/blog/484/video-setting-the-current-limit-on-pololu-stepper-motor-driver-carriers)


---
**chris hopkins** *October 02, 2018 21:23*

Ok, I will work on all this. I have to admit, it definitely is disappointing the glcd won't work now.  As u can see in the pics, I spent some time reworking my panel for it. 


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 21:43*

Green red white wires into power in screw terminal - please explain? 


---
**chris hopkins** *October 02, 2018 21:51*

Hey Ray.  External power supply (red + & white -) and the green is back to ground on laser power supply.  I thought I had read that correctly.


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 21:53*

Yep, exactly what I wanted to hear. 


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 22:02*

oh i forgot this in case you want to run through origin and stepper verification and setting home after job: [lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)




---
**chris hopkins** *October 02, 2018 22:11*

Even at 200 mm/s I get no detail.  Do I still try grbl, or do I have another issue?


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 22:12*

Can you provide pics of the issue? 

I understood the original skipping concern, but not this "no detail" you just mentioned. 


---
**Tech Bravo (Tech BravoTN)** *October 02, 2018 22:13*

You may have another issue. If its not "skipping". Can you elaborate on the file dpi, lightburn settings, speed and power, etc...?


---
**chris hopkins** *October 02, 2018 22:40*

Here is what I'm talking about.  On tge fish on the left, look at the fins coming from under his head.![images/825866eb3bf3db88a4a58c8dd389e0f7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/825866eb3bf3db88a4a58c8dd389e0f7.jpeg)


---
**chris hopkins** *October 02, 2018 22:44*

This is a quick run on another laser that usually has fairly poor detail.  Both were run on Lightburn. The c3d was run at 200 mm/s at 20% power.  Line interval of .08, and 1.5% overscan.![images/d764cf0f7ff7318e1aad450a1744a0f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d764cf0f7ff7318e1aad450a1744a0f0.jpeg)


---
**chris hopkins** *October 03, 2018 13:57*

Any suggestions?  


---
**chris hopkins** *October 03, 2018 23:26*

I'm not sure why I am getting no responses?  I need help and thought this was the place...  My scans look terrible and tge only thing I changed was the board. Lens and mirrors are tight, rollers and carriage are smooth, I've set my speed to a snails pace, yet still scans are jittery.


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2018 23:43*

Are we talking about the fins being faded with the little sections of them cutting out?

Easiest way to debug is just going to be to switch to grbl using the article from [cohesion3d.com/start](http://cohesion3d.com/start), let me know if the issue persists, and then tune a few settings in LightBurn. Post a screenshot of the full LightBurn window as well. 

And I'm sorry, we're all really busy and sometimes it takes a full day to respond to questions, as it has now. 

[cohesion3d.com - Getting Started](http://cohesion3d.com/start)


---
**chris hopkins** *October 04, 2018 00:18*

I understand, its just super stressful with this machine down still.  So the LCD screen will just do nothing now?  Will it still show anything?  I'm so confused as to why it isn't producing decent scans.  Is that par for the course?  Is everyone else just cutting and marking vector lines?  This whole process costs more than 2/3rds the price of a new k40, i expected high quality functioning all around.


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 00:27*

this is not par for the course. with grbl the display is totally disabled. i'm sure you can have the output you desire. odd issues like this are the most difficult to overcome sometimes but at the end of the day it can be overcome. i realize its frustrating but hang in there. we will help all we can :). can you send me the lightburn file of your example?




---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 00:27*

you can drag it here: [lasergods.com - File Upload - LaserGods Laser Engraver Info Portal](https://www.lasergods.com/downloads/file-upload/)




---
**chris hopkins** *October 04, 2018 17:28*

Ok...   I am sinking quick here.  Even the vector lines are jagged.  It is 100% not mechanical/optics.  The board acts very erratic.  Going back to the homing issue. Sometimes after a job it goes back to x=0.00 & y=200, but sometimes it end up a fraction off or even negative for x. It never bounces off the end stops unless i actually hit home on Lightburn. When it doesn't home correctly, it has no idea what coordinates its on and if u hit frame or start job, it is grinding on the top of y.  Sometimes i have to hit home on Lightburn 2 or 3 times b4 it will regain its proper coordinates.  This isn't a matter of files or Lightburn settings.  I run a full time laser biz with multiple lasers running all day.  One question...  For the ground wire going back from c3d to the laser psu, does it go to the ground next to the laser fire wire, or the main ground on the left of the connections?


---
**chris hopkins** *October 04, 2018 17:35*

Right now I have the ground wire goong from the ground on tge c3d to the 3rd from far right (far right is white Laser fire wire).  ![images/e497e1d6ae47c0a8748f6d0a841635a2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e497e1d6ae47c0a8748f6d0a841635a2.jpeg)


---
**chris hopkins** *October 04, 2018 20:57*

Grbl has slighter better scan results (still nowhere near what it was b4), however is still not homing correctly. I have watched the video, set the origin, set the finish position, tried everything. How do I lower the acceleration, maybe that will help?  If set to absolute coordinates, it skips on y and doesn't go to proper position.   It also keeps telling "cut might be out of bounds on Lightburn, but its right in the middle of the page, and if I hit continue, it runs ok.  So not sure why that is popping up.  Please help. 


---
**chris hopkins** *October 05, 2018 16:57*

Anyone out there??  Ray?  Tech bravo?  Please help.  


---
**Ray Kholodovsky (Cohesion3D)** *October 05, 2018 16:58*

The ground wire should be exactly where the original k40 power pigtail had it. We don't change any wiring on the LPSU side.



It would help to have a screenshot of the entire LightBurn window to help debug. 



We have a group on FB as well and I know it might be easier to paste pics and screenshots using the FB interface. 



Fundamentally we just want to do one thing at a time, like:

Start a connection or if already connected type G28.2 into the console in LB and watch the head home to the rear left - does it do this without skipping? 


---
**chris hopkins** *October 05, 2018 17:19*

This is the 1st time I have fired it up today, and now it just remains as status "busy" after homing.  It won't do a thing.  I have restarted 4 or 5 times.


---
**chris hopkins** *October 05, 2018 20:47*

Ok, after sitting for a while and a bunch of restarts...   It finally stopped saying busy.  I typed G28.2 into the console and it says "error:20".  Head did not move.  If I hit the home button on Lightburn, it homes correctly without skipping.  If I hit the frame button on Lightburn, it skips like crazy and jumps around at tge top of y axis.  It does this 4 out of every 5 times. Occasionally it will go to job origin and frame correctly.  


---
**chris hopkins** *October 05, 2018 20:49*

Or if I just hit start, it also skips around amd doesn't make it to origin


---
*Imported from [Google+](https://plus.google.com/117993478615673588498/posts/JJ8qUB5X4d5) &mdash; content and formatting may not be reliable*
