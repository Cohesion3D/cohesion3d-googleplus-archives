---
layout: post
title: "I would like to tell my Cohesion 3d mini the true size of my machine so it uses the full travel"
date: May 09, 2017 12:45
category: "FirmWare and Config."
author: "ALFAHOBBIES"
---
I would like to tell my Cohesion 3d mini the true size of my machine so it uses the full travel. I have something around 225 x 330. Do I enter this in the sd card or do I just tell it that size in Laserweb4?





**"ALFAHOBBIES"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 12:57*

What machine do you have? Where are the homing limit switches located? 


---
**ALFAHOBBIES** *May 09, 2017 13:18*

I have the newer k40. Right now it is set for 200 x 300. I see where to change it in Laserweb but not sure in the config file on the SD card if I need to change something there too.


---
**ALFAHOBBIES** *May 09, 2017 13:20*

Homing limits are in the stock location in the upper left-hand side of the machine.


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2017 13:21*

Ok. You'll find the current 200 and 300 values in config.txt. 


---
**ALFAHOBBIES** *May 10, 2017 15:58*



I think I understand that 0 is set to the right then 200 is loaded as the top y position right after homing. So all I really need to change is 200 to 225 in my case. alpha_max



alpha_min                                    0                # this gets loaded after homing when home_to_min is set

alpha_max                                    200              # this gets loaded after homing when home_to_max is set


---
*Imported from [Google+](https://plus.google.com/118265639968468013312/posts/6G5e5nZc2Q9) &mdash; content and formatting may not be reliable*
