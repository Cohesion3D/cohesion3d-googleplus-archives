---
layout: post
title: "I cannot get my new Cohesion board to move the laser head across both the x and y axes"
date: April 19, 2017 17:57
category: "K40 and other Lasers"
author: "Phillip Meyer"
---
I cannot get my new Cohesion board to move the laser head across both the x and y axes.



Initially it would only move in the Y axis so after some research on this forum I decided to try swapping the X and Y A4988 drivers. After this I found that I can then move in the X axis but not the Y axis. Does this mean that one of the A4988 driver boards is faulty? Looking through recent posts, it looks like someone else had this problem and has ordered a replacement on eBay but I'm really surprised if these are coming out faulty. Should I just but a replacement and try that?



All the lights are working/flashing correctly (red and two green next to it are on, the next two are flashing and the last one is on). I have attached pictures of the original wiring and my wiring. I note that I have not got anything connected to the X socket in your board and I also didn't have anything to connect the white and pink cable that you supplied with the board so perhaps I'm missing some important steps or perhaps I just need to buy a replacement A4988 driver?



One final note, I obviously haven't tried printing anything but if I hit 'laser test' in LaserWeb nothing happens, shouldn't this fire the laser?





![images/16f971686200aa584126c0be1c8eb88f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/16f971686200aa584126c0be1c8eb88f.jpeg)
![images/643d06ea02436974cd630512e9450b68.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/643d06ea02436974cd630512e9450b68.jpeg)
![images/57aad4efe0abea17db8233ed8f7b1920.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57aad4efe0abea17db8233ed8f7b1920.jpeg)
![images/a7dcf2db814a9e16918995ef5285664f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a7dcf2db814a9e16918995ef5285664f.jpeg)

**"Phillip Meyer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:08*

Hi Phillip.  

That would be the quickest assumption, yes.  We've been through almost 1000 of these drivers by now and that other post you are referring to is the first time I've heard of what sounds like a bad driver..  I do apologize if that is the case and can send out replacements but it will be cheaper and quicker for you to spend the few bucks on one locally and I will reimburse the cost.  


---
**Jim Fong** *April 19, 2017 18:14*

For the laser test button to work, you need to configure tool test power/duration in Settings/Gcode



I use 50% and 5ms



I've had a few defective stepper driver modules before.  Usually poor soldering which I did fix myself.  Both were solder bridges between adjacent pins.  They are so inexpensive that I buy a bunch off eBay as spares and for other projects.  








---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:15*

Or just run a command such as G1 X10 S0.5 F600 to do a fire while moving. 


---
**Phillip Meyer** *April 19, 2017 18:30*

So do I need to a command to tool on and tool off in laserweb?


---
**Phillip Meyer** *April 19, 2017 18:33*

Ah, figured out where to put 50% and 5ms! Thanks.


---
**Phillip Meyer** *April 19, 2017 18:33*

I will take a look at the driver and let you know.


---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:37*

No tool on or off code needed. 


---
**Phillip Meyer** *April 19, 2017 18:45*

I have ordered another driver but can I check, is it definitely correct that I don't have a cable to attach to the X socket?


---
**Ray Kholodovsky (Cohesion3D)** *April 19, 2017 18:45*

The flat white ribbon cable handles the X motor and endstops. 


---
**Phillip Meyer** *April 19, 2017 19:47*

My soldering skills leave a lot to be desired but I can't see any obvious bad soldering on the driver.

![images/f92ca015ce6dfa118fda4298f2ca153d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f92ca015ce6dfa118fda4298f2ca153d.jpeg)


---
**Phillip Meyer** *April 21, 2017 09:07*

It was the driver board that was faulty, I ordered a couple of spares on eBay (I'm in the UK so the link someone else posted recently was perfect). It seems to work perfectly now (at least jogging back and forth). Thanks


---
*Imported from [Google+](https://plus.google.com/117194752728291709572/posts/BouPPypjCk1) &mdash; content and formatting may not be reliable*
