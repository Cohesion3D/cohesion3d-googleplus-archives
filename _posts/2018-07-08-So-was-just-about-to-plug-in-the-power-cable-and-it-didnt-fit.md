---
layout: post
title: "So was just about to plug in the power cable and it didn't fit"
date: July 08, 2018 16:53
category: "General Discussion"
author: "Aaron Cusack"
---
So was just about to plug in the power cable and it didn't fit. Anybody know how I fix this? 



![images/57e4c593b45333e25c7f827eb2e83a04.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57e4c593b45333e25c7f827eb2e83a04.jpeg)
![images/86d9b7769139b0cca93c20c15d36698a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/86d9b7769139b0cca93c20c15d36698a.jpeg)

**"Aaron Cusack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 08, 2018 18:18*

I can't find that post anymore, but here was my reply. 

![images/3cec90f5eda97913ce46e28ceb720095.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3cec90f5eda97913ce46e28ceb720095.jpeg)


---
*Imported from [Google+](https://plus.google.com/104714035697130129832/posts/GEKJS3aEYbU) &mdash; content and formatting may not be reliable*
