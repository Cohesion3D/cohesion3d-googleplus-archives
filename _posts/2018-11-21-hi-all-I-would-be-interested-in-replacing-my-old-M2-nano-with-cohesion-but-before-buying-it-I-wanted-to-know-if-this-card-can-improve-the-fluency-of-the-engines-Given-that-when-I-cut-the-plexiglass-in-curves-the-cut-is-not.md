---
layout: post
title: "hi all I would be interested in replacing my old M2 nano with cohesion but before buying it I wanted to know if this card can improve the fluency of the engines Given that when I cut the plexiglass in curves, the cut is not"
date: November 21, 2018 20:13
category: "K40 and other Lasers"
author: "Camarda Neon Salerno"
---
hi all I would be interested in replacing my old M2 nano with cohesion but before buying it I wanted to know if this card can improve the fluency of the engines Given that when I cut the plexiglass in curves, the cut is not perfect or is serrated unlike when you make a straight cut thanks in advance





**"Camarda Neon Salerno"**

---
---
**ThantiK** *November 23, 2018 01:25*

It's difficult to say.  If your cut isn't perfect or is having issues - the cause could be mechanical or even alignment related.  It's still worth the upgrade, in my own opinion.  Additionally, you have the option for different stepper driver carriers so you can really choose what kind of performance you're going to get out of it. (I highly suggest having them preinstalled though)



It's a really easy upgrade, and lightburn is a pretty awesome piece of software, especially with the camera features.


---
**Ray Kholodovsky (Cohesion3D)** *November 27, 2018 03:22*

This sums it up well.  Define the problem you want to fix, which you did, then figure out what's causing that problem. 


---
**Camarda Neon Salerno** *December 07, 2018 00:27*

**+Ray Kholodovsky** hello can you tell me the difference between the choesion mini and choesion standard


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2018 00:51*

What is the standard? 


---
**ThantiK** *December 07, 2018 03:41*

**+Ray Kholodovsky** I believe he means your non-mini board on the website.


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2018 04:00*

LaserBoard is now in stock.  Just get that.  It has everything. 


---
**Camarda Neon Salerno** *December 11, 2018 10:13*

**+Ray Kholodovsky** How does it work for shipping to Europe?


---
**Ray Kholodovsky (Cohesion3D)** *December 11, 2018 15:23*

Typically, you place the order on the website, I pack the box, put the shipping label on it, and after some time it arrives to you. 


---
**Camarda Neon Salerno** *December 12, 2018 14:10*

does some time mean how many working days?


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2018 16:24*

It depends on a lot of factors. Usually shipping time is a few weeks but this is not guaranteed and can take longer. 


---
*Imported from [Google+](https://plus.google.com/114112879039371909930/posts/DTEHmddyCEj) &mdash; content and formatting may not be reliable*
