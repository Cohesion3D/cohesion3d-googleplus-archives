---
layout: post
title: "Hi. Is it easy to change from smoothie to GRBL and viceversa ?"
date: May 11, 2018 12:16
category: "FirmWare and Config."
author: "syknarf"
---
Hi. Is it easy to change from smoothie to GRBL and viceversa ? I'm using smoothie and running jobs from the sdcard, but I want to make some image engraving with GRBL, so I need to change from one firmware to other depending on the job I want to make.

Can I have smoothie on a sdcard and GRBL in another and simply exchange the card and reboot? or is needed more actions for each change?

regards.





**"syknarf"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2018 16:23*

When the firmware flashes it renames itself on the card from firmware.bin to FIRMWARE.CUR.  So you'd have to rename the file back to firmware.bin each time. 



The instructions for grbl are now linked at [cohesion3d.com/start](http://cohesion3d.com/start)


---
**syknarf** *May 11, 2018 16:38*

So can I put both firmware on the same card and simply rename the extension in order to flash the one that I want each time?


---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2018 23:23*

Possibly. I would maintain files called firmware-smoothie.bin and firmware-grbl.bin



Then when you want one make a copy and rename it to firmware.bin


---
**syknarf** *May 12, 2018 07:33*

That's ok. I'll try. 


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/e5Tc7MWv9Wn) &mdash; content and formatting may not be reliable*
