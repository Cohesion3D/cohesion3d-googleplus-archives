---
layout: post
title: "I'm going to share a DOH! moment"
date: November 30, 2017 19:16
category: "General Discussion"
author: "Alex Raguini"
---
I'm going to share a DOH! moment.



Don't forget to disconnect the USB from the laser if, 1) you are cutting/engraving off the storage card, and 2) you plan on laying out additional tasks in your laser software!  It makes for an ugly end result.





**"Alex Raguini"**

---
---
**Ashley M. Kirchner [Norym]** *December 03, 2017 04:05*

I do that all the time, have it engraving from SD card while I'm setting up the next job in the software. As long as you don't tell the software to actually run the job, nothing should happen. I go as far as creating the fcode for the tasks and wait till the laser is done whatever it's doing before I tell the software to run the next job. 


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/fP1rg231tuV) &mdash; content and formatting may not be reliable*
