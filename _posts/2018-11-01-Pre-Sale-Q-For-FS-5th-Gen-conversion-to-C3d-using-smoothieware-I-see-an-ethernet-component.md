---
layout: post
title: "Pre-Sale Q: For FS 5th Gen conversion to C3d using smoothieware I see an ethernet component"
date: November 01, 2018 15:08
category: "C3D Mini Support"
author: "Em W"
---
Pre-Sale Q: For FS 5th Gen conversion to C3d using smoothieware



I see an ethernet component. Q: does that work with the C3d as an additional option to just using USB for computer connection. Will be using LightBurn.



And if so, what are the pros and cons of adding this item to the order - and is it simple or hard to add the component to the C3d mini and set up. 



Anything else needed for something like this in addition to the ethernet cable and items above?



Thank you in advance for any response, Em





**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2018 15:57*

Nope, Ethernet is not supported in LB, it doesn’t work well to begin with. 






---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/hD9MteRx56z) &mdash; content and formatting may not be reliable*
