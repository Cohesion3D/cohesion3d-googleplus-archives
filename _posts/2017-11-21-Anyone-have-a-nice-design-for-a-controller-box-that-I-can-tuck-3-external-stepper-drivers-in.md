---
layout: post
title: "Anyone have a nice design for a controller box that I can tuck 3 external stepper drivers in?"
date: November 21, 2017 06:31
category: "CNC"
author: "Alex Krause"
---
Anyone have a nice design for a controller box that I can tuck 3 external stepper drivers in? I have a remix board





**"Alex Krause"**

---
---
**Kirk Yarina** *November 21, 2017 16:44*

Check out the ultimate box designer in Thingiverse.  I had some trouble figuring out how to position the board standoffs but it looks like they may have clarified that.  There's a matching front panel designer to go with it.



[thingiverse.com - The Ultimate box maker by Heartman](https://www.thingiverse.com/thing:1264391)


---
**Ray Kholodovsky (Cohesion3D)** *November 21, 2017 17:26*

There's a DXF and lots of CAD on the documentation site, plus Eric may have posted some additional items on a recent thread here by Todd Mitchell. 

I am not aware of any existing designs that include external driver mounting. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/f8yGbQq87rp) &mdash; content and formatting may not be reliable*
