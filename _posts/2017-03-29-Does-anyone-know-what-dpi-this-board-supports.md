---
layout: post
title: "Does anyone know what dpi this board supports?"
date: March 29, 2017 23:54
category: "General Discussion"
author: "michael chartier"
---
Does anyone know what dpi this board supports? Ive read elsewhere that it supports 4000dpi but i want to be sure 





**"michael chartier"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2017 23:56*

DPI is irrelevant to the board. You put your designs into LaserWeb, that's where you have to tell it what dpi the image is at, then it makes the gCode. The board just executes the gCode for the job. 



I already answered this question over email. 


---
**Claudio Prezzi** *March 30, 2017 07:25*

The reachable resolution (dpi) is primarilly defined by your machine. Example: If you have stepper motors with 200 steps/revolution with a spindle that moves the axis 4mm/revolution, then you get a resolution of 4/200=0.02mm/step. The controller can "increase" the resolution by using microstepping (trying to reach a position between two steps), but this is not an exact, repeatable position and should not be called resolution. The default microstepping of the cohesion board is 1/32, so in the example above you could reach a theoretical resolution of 0.02/32=0.000625mm/step.


---
**Don Kleinschnitz Jr.** *March 30, 2017 14:03*

The laser spot it about 2mm ....

I would also imagine that the controller has to deal with much higher frequencies since its 1/4000 " at +300mm/sec. 

However it wont matter as you cannot image at this resolution because the K40 laser spot is to big and cannot be switched fast enough.


---
*Imported from [Google+](https://plus.google.com/116057139333311855526/posts/7dXrkWSaMYg) &mdash; content and formatting may not be reliable*
