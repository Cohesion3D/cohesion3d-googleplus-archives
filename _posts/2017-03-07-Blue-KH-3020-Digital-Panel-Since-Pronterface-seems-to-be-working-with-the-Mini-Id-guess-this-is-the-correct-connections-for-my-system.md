---
layout: post
title: "Blue KH-3020 Digital Panel Since Pronterface seems to be working with the Mini I'd guess this is the correct connections for my system"
date: March 07, 2017 13:16
category: "C3D Mini Support"
author: "Steven Whitecoff"
---
Blue KH-3020 Digital Panel



Since Pronterface seems to be working with the Mini I'd guess this is the correct connections for my system. Pronter face does throw a million errors but the jog controls work fine. So getting the laser to fire would be the next puzzle to solve.







![images/7439f62330fbbc651aa5a45105838bd7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7439f62330fbbc651aa5a45105838bd7.jpeg)
![images/31c180529e82716e153b5f75cf41dbce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/31c180529e82716e153b5f75cf41dbce.jpeg)
![images/0ec5c97743ab0dfedbb68e2bdef260ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0ec5c97743ab0dfedbb68e2bdef260ce.jpeg)
![images/212a629c22586ec2e56a064c13160bbe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/212a629c22586ec2e56a064c13160bbe.jpeg)
![images/1edf863c33e474a5d97b59a62f18fb07.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1edf863c33e474a5d97b59a62f18fb07.jpeg)

**"Steven Whitecoff"**

---
---
**Steven Whitecoff** *March 07, 2017 15:18*

Your link is taking me to MY google photos...not much help there. 


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 15:19*

First thing is that for the digital panel (and in general now) I don't want you to run that white pwm cable that came included with the C3D. Please remove that from the mix and hook the panel back up as it was. 


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 15:23*

Previous batch of boards had to cut the wire and do that. New batch does not have to do this. 



No cutting of wires...


---
**Alvin Manalastas** *March 07, 2017 15:25*

Oh, sorry. I thought they're the same. Deleted previous photos and comments


---
**Steven Whitecoff** *March 07, 2017 16:13*

Ok, I had plugged the PWM cable to the Mini only and never touched the machine...unplugged it now, all good. Next step?

HOWEVER, while  I get jog, communication to LW3  I have a solid red led on the MIni for which I've been reading my eyeballs off to find the meaning because I saw that info early on in looking for connecting my Y axis. BTW, that came down to applying common sense to a situation that one should not...so thankfully I got it right the first try instead of having to ask for another Mini with the smoke still in it :D


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 16:15*

Red light at the top of the stack of green leds? 24v power indicator. Aka when you turn the laser on. 


---
**Steven Whitecoff** *March 07, 2017 16:23*

sorry, just found the reference saying that..ok laser fire how to?


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 16:25*

Run a line from terminal at bottom of LaserWeb window: 

G1 X10 S0.4 F600 




---
**Steven Whitecoff** *March 07, 2017 16:28*

Wish I had to time right now to contribute by brooming together everything one might like to know to install and setup a Mini but I'm under the gun project wise...need to be cutting with this guy by tomorrow or will need to revert to Nano to get some stuff done. So far LW3 is like...what? you can design with this? Imported a SVG from Corel I use for basic testing and it came out nano sized. So yet another epic learning session needed.


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 16:33*

LaserWeb is proper CAM software. So you design somewhere like Inkscape or AutoCAD and then send the vectors (DXF/ SVG) or image to LW to set up the job. Then you can send the job to the C3D. 



You can see the documentation links in Peter's comment here: [https://plus.google.com/107810051940552772349/posts/T7YLdUDmJdY](https://plus.google.com/107810051940552772349/posts/T7YLdUDmJdY)


---
**Steven Whitecoff** *March 07, 2017 16:56*

think I'll  add a macro to my keyboard "SORRY", I guess its a case of TMI and some overload. Been cruising thru that  stuff Ray, guess I was looking for functionality  thats actually in LW4


---
**Steven Whitecoff** *March 07, 2017 16:57*

Meanwhile, G1 X10 S0.4 F600 does nothing, I do get an OK in LW3


---
**Steven Whitecoff** *March 07, 2017 17:05*

Physical test switch works of course 


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 17:30*

It does nothing nothing? You should have movement of the x axis and it should fire. 

You can change the X value (coordinate to go to) and S value between 0 and 1 (fire power level) 


---
**Steven Whitecoff** *March 07, 2017 17:40*

guessing the S0.4 is the laser fire? If so then I might have the laser parameters wrong.


---
**Steven Whitecoff** *March 07, 2017 17:41*

looks like I missed this line, fixed and test fire worked:

switch.laserfire.enable true


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 17:41*

S value can be between 0 and 1 (fire power level) 


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 17:42*

The config works as is.  You don't need to change that. 


---
**Steven Whitecoff** *March 07, 2017 17:58*

I presume this gives me fire at the panel set power. Now that it has fired I see I can only get it to do one thing before I have to reset things...disconnect, power down restart, clear the lock and purge. Then sometimes it works, sometime not.




---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 18:00*

Try G1 X10 S1 F600

You should have a movement to the right while the laser fires. 


---
**Steven Whitecoff** *March 07, 2017 18:28*

Yes Ray, it worked with S0.4  and S1 and its clearly increasing power with S1. I've been having a problem with it appearing to not work consistantly. Turns out, when testing basic function, I often use the HOME button to see if its listening. Well I've determined it ONLY cuts when it starts from home. So is that correct and if so how do we deal with that? 


---
**Steven Whitecoff** *March 07, 2017 18:30*

As to the config line, I'll test it set false...as mentioned above I had more than one thing affecting firing




---
**Steven Whitecoff** *March 07, 2017 19:00*

After much fiddling with LW3 I got a cut. I'm trying to figure out just how the power is working...first impression is the panel controls is are functioning during a cut as I could go below minimum and kill it and vice versa. Maybe you could comment on how it should work with the digital readout(I know the readout only reflects an electronic "pot" value interpreted to correlate to % power)


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 19:20*

Please see this new support article I started regarding the configuration for LaserWeb - hopefully it clears up how homing works and we can continue the conversation from there: [cohesion3d.freshdesk.com - LaserWeb Configuration : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726545-laserweb-configuration)



Yes, it is very important to not just go fiddling with other values when something appears not to work.  That makes it more difficult for everyone trying to help you.



The way power works is a proportion of a proportion.  You set the "ceiling" on the pot/ digital panel and then your range in LaserWeb which will be a % of that ceiling. 

Depending on what you are doing, 10mA is a good overall value to have, but some common sense needs to be applied as well.  You do not want anywhere near that high if you are trying to do an engraving on cardboard. 


---
**Steven Whitecoff** *March 07, 2017 21:40*

Thanks, last night I would have bet money I couldnt get here in less than days- with your help. I understand the power explanation as that's what I observed, I think that arrangement will work well. I changed the config from seeing a post here where they did it enroute to success...its the only change I've made so far. Will read the print off your guide and do it as indicated. 




---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 21:44*

I'll look forward to your progress/ feedback and reply here with any more questions. 


---
**Steven Whitecoff** *March 07, 2017 23:13*

Well first I hope this thread gets found by other who have the same version of the digital K40. A few pictures can make a world of difference. Or put another way, if this thread had been here first, I would have been doing a test cut the same hour the Mini was installed...it is quite simple IF you have complete info. Baring that,  Ray is the next best thing :D



This is a huge  step forward (except for several hours spent getting here, of course its worth it) because I am now starting to be confident that using the Mini for the large machine won't be a cluster <b>**</b>. Better still I looks like the funds for the big machine isn't going to a dentist as I was fearing. 


---
**Bob Buechler** *March 14, 2017 04:45*

**+Ray Kholodovsky** Tried to copy Steven's cabling here (minus the pwm cable), but my power cable has what seems to be an incompatible plug on it. There's a horizontal plastic ridge on the plug that runs across all the pins. When seated on the board, his ridge pushed against the port backstop and it doesn't feel right, but flipping the cable doesn't help, as I can't really even plug it in that way. 



Does this look right? I don't want to power it up for fear of shorting it out. ![images/29d859db6c6739776ec79a604eee6ceb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/29d859db6c6739776ec79a604eee6ceb.jpeg)


---
**Bob Buechler** *March 14, 2017 04:46*

A closer look at the cable![images/abe65d2eb0c7044f1e7850a040d1f19e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/abe65d2eb0c7044f1e7850a040d1f19e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 04:47*

This is a China thing. The bump is supposed to push the wall out. 


---
**Bob Buechler** *March 14, 2017 05:46*

Wow... Okay then. So if everything else lines up with Steven's pic I'm okay to see if it will post? (Four pin cable on Y, ribbon cable with contacts facing in toward the board)

![images/6288c15302bbe9e2502d65d22962e897.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6288c15302bbe9e2502d65d22962e897.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 14, 2017 05:49*

Should be good to go. No telling whether Y connection needs to be flipped or not, if it homes (G28.2) the wrong way then just power down and flip that. 




---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/DHsyxaSis76) &mdash; content and formatting may not be reliable*
