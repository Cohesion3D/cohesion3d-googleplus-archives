---
layout: post
title: "can the Cohesion3D run a Chinese cnc router with mach3 software?"
date: August 16, 2017 15:04
category: "C3D Mini Support"
author: "Allen Russell"
---
can the Cohesion3D run a Chinese cnc router with mach3 software?





**"Allen Russell"**

---
---
**Jim Fong** *August 16, 2017 15:34*

No Mach3 is a machine controller software that runs on a PC. It isn't compatible to Cohesion3d running smoothieware, grbl or any of the other firmware that runs on various Arduino or other micro controllers.  






---
**Griffin Paquette** *August 16, 2017 15:39*

^ couldn't have said it better. 


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2017 15:44*

Yep, can't use Mach3 but can use LaserWeb, Vectric, or any other CAM software that puts out gCode. 


---
**Ray Kholodovsky (Cohesion3D)** *August 16, 2017 21:15*

**+Allen Russell** 



Will add that the little green A4988 drivers won't be enough to drive the larger motors seen in the more powerful cnc machines. External drivers advised. When you're ready to order email me and we'll work out those details. 




---
**Allen Russell** *August 26, 2017 14:44*

Ok, Thanks


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/CwVCjYvBvxB) &mdash; content and formatting may not be reliable*
