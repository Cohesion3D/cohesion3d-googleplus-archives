---
layout: post
title: "Hey everyone. Ray can this power supply be connected to your cohesion board?"
date: June 09, 2018 13:02
category: "General Discussion"
author: "Angela Newingham"
---
Hey everyone.  Ray can this power supply be connected to your cohesion board? If so do you have any drawings of how it would be done... i need something for Dummies version  please. ;)

![images/4e6fdadc0f9bbd6979deefd3141488bc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e6fdadc0f9bbd6979deefd3141488bc.jpeg)



**"Angela Newingham"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 09, 2018 13:29*

I need to make sure you understand our goals here. 



If you have a k40, you can keep the power supply that’s in there to drive the laser. We just want a 24v 6a power supply to power the board more reliably. 


---
**Angela Newingham** *June 09, 2018 14:45*

i was only going by a suggestion to change ps for less failures 


---
**Angela Newingham** *June 09, 2018 14:46*

is that not needed?


---
**Ray Kholodovsky (Cohesion3D)** *June 09, 2018 14:52*

We recommend keeping the stock laser power supply to drive the laser tube (15,000 volts), and adding a separate 24 volt power supply to run the C3D board more reliably.



[plus.google.com - I want permit the K40 power supply to only be tasked to power the K40 laser a...](https://plus.google.com/117720577851752736927/posts/emzYWQAAXuW)






---
**Angela Newingham** *June 09, 2018 15:49*

ok thanks i already have an extra psu just need to hook it up


---
**Angela Newingham** *June 09, 2018 18:04*

i have looked the the cohesion site and have not found instructions for connecting my 24v 10 a to my type 2 psu


---
**Joe Alexander** *June 09, 2018 18:10*

if your 24v power supply is separate it doesn't need to be connected to the laser psu, just the cohesion control board.


---
**Angela Newingham** *June 09, 2018 18:13*

for now I'd just like to connect it to my stock board nano m2


---
**Anthony Bolgar** *June 10, 2018 14:29*

You won't find instructions for that on the C3D site as the M2Nano is NOT a C3D product. Try one of the laser groups on facebook or the Google K40 group.




---
**Angela Newingham** *June 11, 2018 15:08*

i stayed with the stock psu, added a second 24v 10a, now just to get the cohesion and rewire everything 


---
*Imported from [Google+](https://plus.google.com/101357182958712120601/posts/iwrTAdVyGi5) &mdash; content and formatting may not be reliable*
