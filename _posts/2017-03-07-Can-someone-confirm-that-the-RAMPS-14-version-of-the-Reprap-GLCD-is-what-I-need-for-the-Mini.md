---
layout: post
title: "Can someone confirm that the RAMPS 1.4 version of the Reprap GLCD is what I need for the Mini?"
date: March 07, 2017 23:03
category: "C3D Mini Support"
author: "Steven Whitecoff"
---
Can someone confirm that the RAMPS 1.4 version of the Reprap GLCD is what I need for the Mini? Just like to be sure since I havent seen RAMPS mentioned in that context here, lots of RAMPS boards instead of Mini but cant find anything explicit.





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 23:04*

Yeah they do have ramps in the name. Get from Amazon in case it is DOA. ![images/24196d85daaf498829846abeb80b5596.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/24196d85daaf498829846abeb80b5596.png)


---
**Steven Whitecoff** *March 07, 2017 23:16*

I'll be ordering the ones I can get by last month, lol. Trigger pulled w/Prime. As a side note I see Hobbyking sells them, I buy a ton of RC model stuff off them but alas, they only have them in HK not the US warehouse so Amazon Prime will have it here Thursday or maybe even tomorrow.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/7iFZ4QxZDEj) &mdash; content and formatting may not be reliable*
