---
layout: post
title: "I just recieved my cohesion3d mini board, it come very fast, upon recieining the board I noticed that the 5 pin connection next to the 4 pin 24v supply is meant for the endstops according to the pinout diagram on the website"
date: December 29, 2017 03:59
category: "C3D Mini Support"
author: "Trey Roudebush"
---
I just recieved my cohesion3d mini board, it come very fast, upon recieining the board I noticed that the 5 pin connection next to the 4 pin 24v supply is meant for the endstops according to the pinout diagram on the website.  My 5 pin connection is used for my pwm.  Where do I plug in this 5 pin connection?





**"Trey Roudebush"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:03*

Can you provide pictures of your wiring please? I think there might be a miscommunication. 


---
**Trey Roudebush** *December 29, 2017 04:03*

Here are some pictures

![images/494601a5e939065bd8047f2ec699d7af.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/494601a5e939065bd8047f2ec699d7af.jpeg)


---
**Trey Roudebush** *December 29, 2017 04:04*

![images/b83be6a9efbd5c4ee6bfb72532ee2259.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b83be6a9efbd5c4ee6bfb72532ee2259.jpeg)


---
**Trey Roudebush** *December 29, 2017 04:04*

![images/f78770e482aba6262037f445c7c98554.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f78770e482aba6262037f445c7c98554.jpeg)


---
**Trey Roudebush** *December 29, 2017 04:05*

The 5 pin wire goes to my power supply and the yellow wire is the pwm


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:06*

You don't unplug anything from the PSU.  The laser will be controlled via the large plug that is already connected in your pics, labeled "K40 Power"



What was your original control board and what exactly was connected to it, and how?


---
**Trey Roudebush** *December 29, 2017 04:11*

My original board was the m2 nano and there was a 4 pin power connection, one motor connection, the ribbon cable containing all the endstops and one motor, and the 5 pin connection I spoke about.  According to a psu pinout I found online the laser is controlled via pwm from the power supply.  If that board doesn't connect to the power supply how does it control the pwm?

![images/ee657d1c4048a749d38ea1ea0ed6e473.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ee657d1c4048a749d38ea1ea0ed6e473.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:17*

I am concerned that you are wiring multiple things incorrectly. 



Please scroll all the way down to "Here is another possible wiring configuration of the K40" at: [https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/solution/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)



If you original wiring looked like what you described, then your final wiring should look like this. This is all you need. As I said, the laser firing will be controlled via "L" - Laser Fire which is one of the wires in the large power connector plugged into the C3D.









![images/dcbd7c5f151abf5611bb62fa5e899880.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dcbd7c5f151abf5611bb62fa5e899880.jpeg)


---
**Trey Roudebush** *December 29, 2017 04:23*

I read through the guide and connected the wires like this.  Is this right?

![images/243b385f7d8b01d88894e08c6230e496.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/243b385f7d8b01d88894e08c6230e496.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:25*

That does seem more reasonable, yes. You need to be sure that the cable plugged into the Y port is indeed the one from the Y stepper motor. 


---
**Trey Roudebush** *December 29, 2017 04:29*

Right now, I am just testing the board, but I will make sure when I put the machine back together.  So the 5 pin connection does not plug into anything?  If it doesn't how does the board control the pwm since the IN Input is in the 5 pin connection?


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:32*

You need to plug that 5 pin back into the psu.  





The board will pulse L.  Assuming you have either a pot or a digital panel, you will set the max power % there.  Then the board will control power within that realm.  It is a % of a % - best quality (maximum contrast) of grayscale engraving this way. 


---
**Trey Roudebush** *December 29, 2017 04:35*

That connection did not go into the psu it went into the front panel control

![images/71c6e9082146687dba9dad69e01ab96a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/71c6e9082146687dba9dad69e01ab96a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:37*

You would need to keep that panel in play. 


---
**Trey Roudebush** *December 29, 2017 04:38*

Where does the board pluse L?


---
**Ray Kholodovsky (Cohesion3D)** *December 29, 2017 04:42*

As I said, the laser firing will be controlled via "L" - Laser Fire which is one of the wires in the large power connector plugged into the C3D.



Please backtrack.  You need to follow the instructions precisely and do nothing else.  You disconnected the original green board and put the C3D in its place.  The things that were plugged into the green board get plugged into the C3D.  Nothing else gets plugged into the C3D.  Everything else must stay the same. 


---
**Joe Alexander** *December 29, 2017 08:30*

the common mode of pwm is by pulsing the connection from L ==> ground through a mosfet(grounding L activates the laser assuming the WP circuit is also closed). there are some dual wire setups but its generally easier to keep the pot controlling the max power and vary accordingly. so in essence the only controlling wires from laser PSU to controller is the L wire and a ground wire. Does that clear it up for you?


---
**Trey Roudebush** *December 29, 2017 16:59*

Yes. Thanks!


---
*Imported from [Google+](https://plus.google.com/107573574842290750272/posts/E1rpgbSXCFA) &mdash; content and formatting may not be reliable*
