---
layout: post
title: "Just installed my Cohesion3d (and GLCD) in my K40 laser"
date: June 11, 2017 19:44
category: "C3D Mini Support"
author: "Terry Taylor"
---
Just installed my Cohesion3d (and GLCD)  in my K40 laser. Display works and I can jog the x and y steppers. I am on Windows 7, 64 bit, latest service pack. I have installed the Smoothieware USB driver but, it is NOT connecting properly. Windows has an error and says that the driver is not installed properly.





![images/f3bdf55804f0dd1a6ea0a65d2b8df093.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f3bdf55804f0dd1a6ea0a65d2b8df093.jpeg)
![images/6f1d2850be7ee3b1d7bf2d7799f7c2d3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6f1d2850be7ee3b1d7bf2d7799f7c2d3.jpeg)

**"Terry Taylor"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2017 20:03*

Ah, hello Terry.  

Please uninstall the driver and use the instructions here for a manual install: [smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)




---
**Terry Taylor** *June 11, 2017 20:29*

Thanks, I thought that I had tried that, but apparently, the old driver have not uninstalled. All good now!


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/6cSWAMc5Hwt) &mdash; content and formatting may not be reliable*
