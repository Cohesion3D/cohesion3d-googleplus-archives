---
layout: post
title: "I have a stock K40 that I am in the process of upgrading"
date: July 29, 2018 19:43
category: "General Discussion"
author: "Lynn Roth"
---
I have a stock K40 that I am in the process of upgrading.



If I want to run a separate 24V power supply for the Mini, how much amperage should it be able to deliver?  



What is the amp rating on the power supply that can be included with the Cohesion 3D Mini bundle?





**"Lynn Roth"**

---
---
**Tech Bravo (Tech BravoTN)** *July 29, 2018 19:49*

I think the recommend and approved power supply for thec3d mini that is available on cohesion3d's site is rated at 4a.


---
**Ray Kholodovsky (Cohesion3D)** *July 29, 2018 19:50*

Recommend you start here:



[cohesion3d.freshdesk.com - K40 Additional 24v Power Supply Upgrade Guide : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000783636-k40-additional-24v-power-supply-upgrade-guide)



[http://cohesion3d.com/power-supply-upgrade-kit/](http://cohesion3d.com/power-supply-upgrade-kit/)


---
**Lynn Roth** *July 29, 2018 20:22*

**+Tech Bravo** **+Ray Kholodovsky** Thank you.


---
*Imported from [Google+](https://plus.google.com/+LynnRoth/posts/FR1Hf97Km1c) &mdash; content and formatting may not be reliable*
