---
layout: post
title: "I just deleted my old post because after troubleshooting a new issue, I'm finding that the problem I had a couple of weeks ago may be the complete underlying issue still"
date: June 20, 2017 22:38
category: "C3D Mini Support"
author: "John Milleker Jr."
---
I just deleted my old post because after troubleshooting a new issue, I'm finding that the problem I had a couple of weeks ago may be the complete underlying issue still.



Cohesion3D Mini, K40 laser. What started out today as the inability to send a raster generated in PhotoGrav through Laserweb without X creep. I exported the gcode from Laserweb to the SD Card to start trouble shooting my new computer setup and when I sent that file what I got was an inch of my file and then it skipped two inches and started printing an old old old test file that doesn't even exist on the card anymore.



Prepped and installed a new card, exported the same file to it and sent the file via the LCD with no connection to the computer and my Cohesion3D Mini is just hard locking up again like I had issues with a few weeks ago. Ray, you already had me adjust the pots. 



The file is a 300dpi 5x7 raster. 12mb on the SD card.



With these problems and the weird gcode file merging with an old (long gone file), is it possible that the file controller on the board is bad? I've attached a few photos, including the laserweb screen of what I'm exporting, what a gCode viewer opened and what the laser burned. This was before the new SD card.



So, new computer, new SD card, new file, when I had issues before I even switched out the drivers. What's my next step to finally get this thing running reliably? 



![images/ce9b8b64ca6c37e2178900c8a876948a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ce9b8b64ca6c37e2178900c8a876948a.jpeg)
![images/efdf91176d8ee09f80c62e7b4894d661.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/efdf91176d8ee09f80c62e7b4894d661.jpeg)
![images/31c022a872de3504a936d0d73cd0e76f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/31c022a872de3504a936d0d73cd0e76f.jpeg)

**"John Milleker Jr."**

---
---
**John Milleker Jr.** *June 21, 2017 03:32*

Thanks Jim, I'll give that a shot. I'll have some funds to get a spare next week. Was hoping to get GRBL-LPC working on my Cohesion3D Mini but I'm afraid it's over my head. Can't find any compile instructions other than for Arduino and just spun my wheels for several hours. Compiled versions allegedly for C3D and K40 aren't working.



Going back to Cohesion firmware and going to start messing with other shots in the dark in the meantime.


---
**Jim Fong** *June 21, 2017 04:11*

I wrote up compile instructions for grbl-lpc and posted them in the Laserweb forum.  I'm running grbl-lpc on a CoreXY test machine so I needed a special version. I also compiled a version that should work on the c3d but I won't be able to test it on the laser until this weekend maybe.  



The k40 power supply isn't the highest quality.  There was a post somewhere here where a user installed a external 24volt supply and reported better performance. I plan to do that eventually, since I have a extra one in my junk pile.  Especially since I plan to use the rotary axis more and that is another driver/motor to power up. 






---
**John Milleker Jr.** *June 21, 2017 04:15*

I'll have to find them, everything I kept seeing used the Arduino IDE and even though that didn't work anyway, I still never found the end way to make a firmware.bin file. I figured with about another week or tinkering I could edit the config to make it work but was really getting frustrated. I just want raster to work.. :)



And I like having an LCD. So, maybe in the future. My CNC runs GRBL and I don't remember having so many problems getting it set up.




---
**Jim Fong** *June 21, 2017 04:25*

**+John Milleker Jr.** Arduino will only compile the original atmel AVR grbl version which I've compiled before.  This newer version LPC requires the ARM compiler tool chain.  It's really simple to do.  



No version of grbl supports LCD that I know of. 


---
**Alex Krause** *June 21, 2017 04:56*

The PSU from the laser is only rated at 1amp on the 24v output... Pretty sketchy when driving the motors at .5 amp each, and the controller has amp draw as well... Just my 2 cents... **+Don Kleinschnitz**​ is our resident PSU guru maybe he can chime in


---
**John Milleker Jr.** *June 21, 2017 06:50*

Interesting Alex, sounds like a good upgrade to put the Cohesion3D and steppers on a separate 24v even if that's not my issue.



I keep thinking I have a buffering problem. It doesn't seem to be power (but then again, I'm basing that off of amateur observation). I would think longer vector line fill jobs with much more jerky motion would cause more drain on power than a gentle sweeping motion of a raster engrave.



USB/Laserweb engraves seem to fail around the same spot, running these jobs from the SD card hard lock the Cohesion board around the same spot.



After a few failed sheets of plywood tonight, I'm wondering if a well timed pause now and then would help clear the buffer and look forward to seeing if it helps. Grasping at straws at this point and as much as I would hate to do so, really thinking about putting the stock junk board back in so that I can get to finally running the jobs I want to run.




---
**Jim Fong** *June 21, 2017 10:34*

**+John Milleker Jr.** I've done some pretty long raster engraving jobs running off the sdcard. I've not seen the c3d board lock up hard.  I've seen the LCD not initialize correct on boot, however the board still operates.  I just send board reset command in Laserweb4 and that fixes it.  That's probably due to buying the cheapest repraplcd on eBay that was available.  



Really the only issues I've had are from the machine such as laser alignment, belts, dirty rollers etc. If you can post the gcode and I'll run it here to check.  


---
**Don Kleinschnitz Jr.** *June 21, 2017 13:30*

**+Alex Krause**, **+John Milleker Jr.** **+Jim Fong**



I recommend using an external 24V supply because I consider 24VDC @1A to be marginal for this application. That said many are using the C3D with the standard supply on std k40 steppers without problems. 



Certainly if you are adding <b>anything</b> beyond the std setup you will need a better supply. I use an external for everything except my lift table and I use the K40 24VDC supply for that.



Another reason is that I do not trust having a HV power supply providing logic power and I do not trust the K40 power supply design. If the HV rail ever got connected to the LV rail you would fry <b>everything</b> :). The HVPS is a source of HV noise so I like to keep that far away as well :).



In any case I would add an inline fuse to all power sources. 

Finally, its a good idea to measure current draw from these supplies to find out if they are running marginal. A drooping supply can cause a lot of weird problems.




---
**John Milleker Jr.** *June 21, 2017 16:18*

Thanks again **+Jim Fong** , I get the LCD initialize issue often,   it's good to know I can just send a few resets. I bought the LCD that Ray supplies, but I'm sure they're all built the same - he supplies them knowing they're going to work.. Ish.. :)



I'm going to do a bunch more tests today, but if I can get one that is a guarantee bust every time and also locks up the LCD (I've changed my orignal file so much that I don't know if it's going to work or not) I'll send it to you. Thank you so much for the offer. Laserweb is dodgy on my Win8 computer and Win10 laptop, so it always bugs me that maybe the file created might be messed up.



**+Don Kleinschnitz**, that's a lot of great info. I'm not running anything else except I did have my Z driver installed (nothing hooked up though). I've pulled that out and will kill power to the LED strip and see if that works. I found a $17 24v 6A power supply on the Amazons, that'll be here next week. 



I also want to mess with acceleration and jerk, saw that in another post. I  did run the file in the diagonal a few times and the shift up and left was even more dramatic - I don't know if that's a tell tale sign, but maybe power since two steppers move at a time?


---
**Jim Fong** *June 21, 2017 16:48*

**+John Milleker Jr.** I have a long led strip but I didn't even try to power it off the original laser PS.  I put in a external 12volt PS which also runs the pump and exhaust fan.  



I run Laserweb4 on several machines. Win7 and win10.  The major issue I've ran across is generating raster engravings on larger high resolution images.  When the laser diameter is set to a low value, the graphic screen will turn white.  I've been able to reproduce this on every Laserweb4 version, doesn't matter what machine or windows version I use.  This generates gcode that is huge.  Lowering the resolution or increasing laser diameter will solve the problem.  



My main workstation has 4.5ghz I7 with 32gb memory.  Win10. I can easily crash laserweb4 if I try.   


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/4uz1pSNPARK) &mdash; content and formatting may not be reliable*
