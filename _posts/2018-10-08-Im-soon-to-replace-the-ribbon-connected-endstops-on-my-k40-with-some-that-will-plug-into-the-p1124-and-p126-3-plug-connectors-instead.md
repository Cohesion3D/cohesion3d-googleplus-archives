---
layout: post
title: "I'm soon to replace the ribbon-connected endstops on my k40 with some that will plug into the p1.124 and p1.26 3-plug connectors instead"
date: October 08, 2018 20:30
category: "C3D Mini Support"
author: "David Piercey"
---
I'm soon to replace the ribbon-connected endstops on my k40 with some that will plug into the p1.124 and p1.26 3-plug connectors instead.   Do I need to change or update the firmware at all to enable them to be recognized?





**"David Piercey"**

---
---
**Tech Bravo (Tech BravoTN)** *October 11, 2018 04:25*

Usually not. The board is made to handle optical and/or mechanical switches.


---
**David Piercey** *October 11, 2018 16:18*

**+Tech Bravo**  Thanks.  Since the p numbers are the same, I was hoping it would read either without change.



Now to put these crimps on the wires....what an exercise in masochistic frustration.




---
*Imported from [Google+](https://plus.google.com/114972156206786232028/posts/cL3MdXjdWJg) &mdash; content and formatting may not be reliable*
