---
layout: post
title: "I'm looking to add jogging to my K40 running Cohesion3DMini and GRBL-LPC"
date: July 29, 2017 22:03
category: "FirmWare and Config."
author: "John Milleker Jr."
---
I'm looking to add jogging to my K40 running Cohesion3DMini and GRBL-LPC. Right now the computer is 6' away from the laser and I constantly struggle with moving the laser where I want it on the laser. I know that GRBL-LPC has jogging capability but I'm not quite sure how to hook it to the C3D board (if it's even possible) and compiling my own version is way over my head.



I'm also running the USB of the C3D through a Raspberry Pi and Laserweb. Has anyone added a keyboard and some settings to their RPi and gotten keyboard or gamepad jogging to work?



Failing all of this, I'm going to try to get a USB numeric keypad on a USB extension cable so that I can move the laser with the numeric keys, I'm just hoping for a more elegant solution.



Thanks!





**"John Milleker Jr."**

---
---
**Jeff Lamb** *July 29, 2017 22:33*

Personally I use a Bluetooth keyboard on my pc but not sure if this will work with the pi. 


---
**John Milleker Jr.** *July 30, 2017 05:00*

I've got a really nice wireless Microsoft keyboard that is the pits for distance. It's not bluetooth but those do stretch all the way into next week. There's got to be an easier way though!


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/Ywn1yjyby4F) &mdash; content and formatting may not be reliable*
