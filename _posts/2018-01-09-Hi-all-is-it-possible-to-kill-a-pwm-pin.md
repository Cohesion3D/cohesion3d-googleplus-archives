---
layout: post
title: "Hi all, is it possible to kill a pwm pin?"
date: January 09, 2018 17:41
category: "C3D Remix Support"
author: "Marc Pentenrieder"
---
Hi all, is it possible to kill a pwm pin?

I am using pin 2.4 (servo) for my bltouch to move the probe in and out. Everything worked fine until yesterday evening. It simply will not deploy or remove the probe.

So I started to measure the voltage between ground an pin 2.4 on the remix with probe out and in commands. They are nearly equal (out 0,013V / in 0,015V).

Then I tried the bltouch with my re-arm board and the sdcard from remix board and everything works as expected. Voltage with probe out is 0,014V and voltage with probe in is 0,036V.



So I think something on pin 2.4 on the remix board is broken.

Do you agree with me? How can this happen?





**"Marc Pentenrieder"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2018 17:50*

I would recommend to make sure your config is not the cause of the issue - aka use a clean config file and configure a simple switch on pin 2.4 then send the gcode to turn it on and meter this. 



Failing that, I can advise you what other hw pwm pins are available for you to connect the bltouch to. 


---
**Marc Pentenrieder** *January 09, 2018 17:53*

The config works with the re-arm board from panacut, but I will try it.


---
**Marc Pentenrieder** *January 09, 2018 19:50*

**+Ray Kholodovsky** 

I used a sample [config.txt](http://config.txt) and configured a digital switch an pin 2.4.

When the switch is turned off i get 0.011V and when on i get 0.055V between ground and pin 2.4. So I think, the pin died ;-(


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2018 19:59*

Do you use all the extruders? 



There are several other h/w pwm pins...  You can disable leds and use step/ dir/ en which is shared with the leds, in stepper socket 6 "C".



[https://cohesion3d.freshdesk.com/solution/articles/5000718967-cohesion3d-remix-getting-started](https://cohesion3d.freshdesk.com/solution/articles/5000718967-cohesion3d-remix-getting-started)



[smoothieware.org - lpc1769-pin-usage [Smoothieware]](http://smoothieware.org/lpc1769-pin-usage)


---
**Marc Pentenrieder** *January 09, 2018 20:04*

I will use 2 extruders in the near future, so I can use the pwm pin from the 3rd extruder.

Are pwm pins also 5v tolerant ?

What is the right pin definition in [config.txt](http://config.txt) for pwm pins (pwm or hwpwm)? I used hwpwm for pin 2.4


---
**Marc Pentenrieder** *January 11, 2018 14:58*

**+Ray Kholodovsky** Thanks for the link to the pin description. It is very usefull for me. I will now try to get my bltouch running again.


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/hanLskWLzvR) &mdash; content and formatting may not be reliable*
