---
layout: post
title: "Hello! Still new to the C3D game, and I'm having a bit of trouble"
date: April 16, 2017 18:57
category: "C3D Mini Support"
author: "Richard Pike"
---
Hello!



Still new to the C3D game, and I'm having a bit of trouble.



I've installed both Laserweb3 and 4, but I can not seem to get it to communcate with the laser at all.  It doesn't recognize it as even being there.



I'm at a bit of a loss at this point on how to trouble shoot it, to see what the cause might be (program just being buggy/fault with the board/ect).



Is there another way I can send some sort of ping to know the board is actually working?  (when powered on, all green but the last two lights are red).





**"Richard Pike"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 19:00*

What is the computer type and operating system? 


---
**Richard Pike** *April 16, 2017 19:04*

It's a Dell Optiplex 755 with a fresh install of win10 (was originaly Vista, which was working fine with the original board). Upgraded to win10 to see if having a current OS would help. It hasn't.




---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 19:07*

Ok, first thing that jumps out is that you used the word "upgraded".  Did you have the smoothie drivers installed on vista? Now when you upgraded to 10 did it keep all your files and stuff or was it a clean install (from scratch)?



Can you open "devices and printers" and see the board there? 


---
**Richard Pike** *April 16, 2017 19:19*

I can not.  Opening it up does not reveal the connection to the board in any form (and the drivers site says not to install in win10).  And this was a completely fresh install of win10, nothing saved from the old install.




---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 19:23*

Ok, it was a clean install and you did not install the drivers on windows 10. Good. 



Questions: 



Are you using a good USB cable? The one that comes with the k40 has been known to cause issues. 



The MicroSD card is in the board? 



When you plug in the USB cable, what do the lights on the board do? 



Is the board installed in the laser right now? 




---
**Richard Pike** *April 16, 2017 19:32*

USB cable is good, tested it earlier though I can get another just to be sure.   The SD card was in the board, and I just did a test by removing it. That caused it to show up in the printer section as 'Smoothie' where it wasn't before.



With the cable in and SD card in, All lights are green, one light red closest to the board.  Right now with the SD card removed all lights are green.



Board is installed and connected in the laser right now.



Hope this helps!


---
**Richard Pike** *April 16, 2017 19:48*

I just put the SD card back in, and now all lights green, and LW4 actually connected to it...of course LW4 is still giving me trouble but it did see it (even if it doesn't control it yet).



I don't know what changed, but I must have jogged something when pulling the card out and putting it back in.


---
**Richard Pike** *April 16, 2017 20:27*

Got it working, but now I gotta buy a cheap vid card for it. No webgl!  (and it was a free computer that works. Can't knock it :))


---
**Ray Kholodovsky (Cohesion3D)** *April 16, 2017 20:28*

I know, this was being discussed before in an fb group. LW would not run properly on Vista (3d viewer grid not showing - whole host of graphics concerns). Suggestion was made to get new computer, but first they are trying to use this one.  


---
**Richard Pike** *April 16, 2017 20:31*

Yes, I was trying all options before having to put out the cash.  Now that I've sussed out the options for it (and being told multiple times to get with the times), I will be looking for a replacement.  Thanks for your help and I'll hopefully be up and running by next week, with a newer computer.




---
**Richard Pike** *April 16, 2017 21:36*

I have moved the entire rig into my room from the shop area so I could plug it into a modern computer and get this thing set up.  LW4 now reads it, the board appears to be working...but if it isn't one thing, it is another.   I tried to tell it to do a test run, and the program seems to be running the job, but nothing in the laser is actually moving, nor laser firing.   



admittedly, I might have missed some setting in LW4, (set it to smoothie in settings, adjusted table size and such), but it won't even manually jog the arms or table.  



(the laser does fire when I press the test button, but that is it at the moment).


---
**Richard Pike** *April 16, 2017 22:07*

and the last statement is retracted. I've gotten so frustrated I forgot to do the simple thing of checking connections.  The power connector to the board had come lose.   Thanks again for all your help.


---
*Imported from [Google+](https://plus.google.com/113450419015576024390/posts/Koh32Zmz5yu) &mdash; content and formatting may not be reliable*
