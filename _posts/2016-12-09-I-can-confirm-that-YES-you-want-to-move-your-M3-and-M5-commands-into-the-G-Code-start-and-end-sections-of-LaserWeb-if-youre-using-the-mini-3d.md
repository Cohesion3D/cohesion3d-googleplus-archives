---
layout: post
title: "I can confirm that YES, you want to move your M3 and M5 commands into the G-Code start and end sections of LaserWeb if you're using the mini 3d"
date: December 09, 2016 21:25
category: "General Discussion"
author: "Carl Fisher"
---
I can confirm that YES, you want to move your M3 and M5 commands into the G-Code start and end sections of LaserWeb if you're using the mini 3d. I've gained almost all of my speed and smoothness back that I was missing previously. 



Don't put them in Laser Enable and Laser Disable.





**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 21:27*

Good. **+Joe Spanier** make note of this if you haven't already for testing. 

We'll get this and other relevant info into the final docs. 


---
**Joe Spanier** *December 09, 2016 21:37*

intersting Ill try it


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/9Cyxjkng615) &mdash; content and formatting may not be reliable*
