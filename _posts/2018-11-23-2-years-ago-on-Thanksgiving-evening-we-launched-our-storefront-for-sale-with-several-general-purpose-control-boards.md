---
layout: post
title: "2 years ago, on Thanksgiving evening, we launched our storefront for sale with several general purpose control boards"
date: November 23, 2018 02:22
category: "General Discussion"
author: "Cohesion3D"
---
2 years ago, on Thanksgiving evening, we launched our storefront for sale with several general purpose control boards. Little did we know that our little board for laser cutting would represent the vast majority of our sales. Over this time we have learned what the needs are for a laser cutter controller, what the pitfalls are in all of these cheaply made machines, and we're super excited to announce our new, honed in, Cohesion3D LaserBoard!

LaserBoard should make upgrading a Laser Cutter even easier than it was before:



It has 4 embedded Trinamic drivers that can go to higher currents, meaning you can drive your XY head AND a Z table AND a rotary directly from the board (you'll want to double check the current your motor uses to be sure though).



It's now super easy to connect external stepper drivers if you do need the extra current, and there's no firmware or config changes, it should just work.



There's a Jack so that our power supply will plug directly into the board, rendering the limited and unstable 24v rail out of the laser no longer a problem.



It is isolated and protected in a bunch of different ways, making it harder for the big bad laser with an ungrounded frame and power spike happy LPSU to kill the board.



It can drive CO2 laser PSU's, diode lasers, and CNC machines too.



LaserBoard joins our lineup as the big brother to the Cohesion3D Mini - it is more resilient, easier to use, and a better value especially for the people that want to use a Z Table and Rotary with their laser.



LaserBoard is available for pre-order now and expected to ship in about 2 weeks. The Mini remains available, and is in stock for immediate shipping.



Head on over to the product page to learn more. We can't wait to see what you make with your machines, our boards, and the amazing LightBurn Software!

[http://cohesion3d.com/cohesion3d-laserboard/](http://cohesion3d.com/cohesion3d-laserboard/)



![images/59d0ac9cb29d08f039a4b2fdafb5e8f5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/59d0ac9cb29d08f039a4b2fdafb5e8f5.jpeg)
![images/4d0fd29f81911cb714b330d0151dd3a2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4d0fd29f81911cb714b330d0151dd3a2.jpeg)
![images/2021827c018c613df0b5bb05eb54ce8c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2021827c018c613df0b5bb05eb54ce8c.jpeg)
![images/5339b0ebcdedbeee3298a24cb6b639de.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5339b0ebcdedbeee3298a24cb6b639de.jpeg)
![images/ff0aaac3cd157c3e2918efab311157e9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ff0aaac3cd157c3e2918efab311157e9.jpeg)
![images/81989ce7086d8a27d0146c074d746c8d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/81989ce7086d8a27d0146c074d746c8d.jpeg)

**"Cohesion3D"**

---
---
**Eric Lien** *November 23, 2018 02:42*

You make such great equipment Ray. I am sure everyone will love the new board. Congratulations.


---
**Anthony Bolgar** *November 23, 2018 04:04*

Nice job on the board Ray!




---
**Marc Miller** *November 23, 2018 05:45*

Excellent. Nice work Ray!


---
*Imported from [Google+](https://plus.google.com/+Cohesion3d/posts/BTHYvjcib2G) &mdash; content and formatting may not be reliable*
