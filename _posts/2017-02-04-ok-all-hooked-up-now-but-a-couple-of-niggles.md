---
layout: post
title: "ok, all hooked up now but a couple of niggles"
date: February 04, 2017 21:01
category: "Laser Web"
author: "Colin Rowe"
---
ok, all hooked up now but a couple of niggles. Dont know if its a laserweb problem?

Some times on the jog screen it has the power and feed that can be over ridden, but most times its not there??? no idea why

Also at end of cut it locks up and freezes and I have to reset (comes up with error that its locked me out and have to click 1 or 2 to start it)

Not sure why it keeps doing this as its a pain to keep resetting it. some times it will not connect to board and i have to turn laser off and on.

running win7





**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2017 00:06*

Hi Colin,

Can you provide more information so that someone can try to reproduce these issues? 

For example, the "lock up" you describe is called halting in smoothie. Does it happen every job? Is there a specific sequence of events you can specify that will cause this to occur? Can you do a backup of the LW settings (last tab), and provide a cut file, and the resultant gCode file, that would cause this? 


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/CSRb9SaMVPK) &mdash; content and formatting may not be reliable*
