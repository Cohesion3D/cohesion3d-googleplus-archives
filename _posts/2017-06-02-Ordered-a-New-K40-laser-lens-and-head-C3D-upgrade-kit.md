---
layout: post
title: "Ordered a New K40 laser, lens and head + C3D upgrade kit"
date: June 02, 2017 13:24
category: "K40 and other Lasers"
author: "Bromide Ligand"
---
Ordered a New K40 laser, lens and head + C3D  upgrade kit. I am ready for some fun. Will a 20l/Min air pump be enough for air assist ?





**"Bromide Ligand"**

---
---
**E Caswell** *June 03, 2017 13:18*

**+Bromide Ligand** personally I think it's too small. If I was you I would go up to 60l/min (3600l/hr) much better . That's what I have and does what I want it to do.


---
**Bromide Ligand** *June 03, 2017 15:50*

Actually, I am thinking about getting an air compressor, 2HP with 95l/min Free air delivery (21litre tank). It will double up when I want to use an air brush too.




---
**Bromide Ligand** *June 03, 2017 15:51*

its 50/50 duty cycle tho. will it be enough?




---
**Bromide Ligand** *June 04, 2017 00:59*

nvm I found out that compressor I was looking at uses oil, so the air wont be pure.


---
*Imported from [Google+](https://plus.google.com/108393288879485897084/posts/Dsr6YaVQGtp) &mdash; content and formatting may not be reliable*
