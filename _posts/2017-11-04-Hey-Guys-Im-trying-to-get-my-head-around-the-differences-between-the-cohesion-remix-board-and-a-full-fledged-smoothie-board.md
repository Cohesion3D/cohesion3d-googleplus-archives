---
layout: post
title: "Hey Guys, I'm trying to get my head around the differences between the cohesion remix board and a full fledged smoothie board"
date: November 04, 2017 17:13
category: "General Discussion"
author: "Todd Mitchell"
---
Hey Guys, I'm trying to get my head around the differences between the cohesion remix board and a full fledged smoothie board.  Has anyone seen a comparison chart?





**"Todd Mitchell"**

---
---
**Eric Lien** *November 04, 2017 18:33*

Main differences:



Smoothie = integrated drivers (5max on board, one external)



Remix = socketed drivers (6 onboard, user provides drivers of their choice). And onboard esp8266 to run serial over wireless... Or flash the esp8266 with something like esp3D firmware if you feel adventurous.



That is the major differences for a beginning user. There are other small differences... But thats the major ones.


---
**Todd Mitchell** *November 04, 2017 19:44*

Thanks Eric!  That's clear.


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2017 20:21*

Let's see... 

Prices are similar enough. 

5 vs 6 axis and sockets vs inbuilt drivers as Eric said. 

ReMix has protected endstop ports which means you can hook 12-24v inductive probes directly to it without requiring external circuitry. 

ReMix has the GLCD connectors built in instead of requiring that large adapter panel. 

The wifi thing, but it's pretty basic and probably wouldn't apply to your use cases. 

C3D boards also break out other things like a servo header (helps with certain kinds of probes like BLTouch or Servo arm). 

ReMix also has the most available gpio pins because the Ethernet is broken out as an expansion header - so that's an additional 8 gpio's - we have actually hit this when wiring up a 4 head extruder - 4 heaters, 7 motors, and lots of fun :) 


---
**Todd Mitchell** *November 04, 2017 20:41*

**+Ray Kholodovsky** brilliant, thanks.  I think I'll order a second one here in just a bit.  the thing I'm trying to sort out - it comes with wifi - which means I can hit the built-in http server and use/extend the apps for controlling this, right?  is the only reason for the Ethernet cable adapter for environments that don't allow wifi?


---
**Ray Kholodovsky (Cohesion3D)** *November 04, 2017 20:55*

All the implementations are a bit different:

Smoothieboard has ethernet.  It serves this web interface: [smoothieware.github.io - Smoothie web interface choice](http://smoothieware.github.io/Webif-pack/)

(click on classic and improved).

I find it a bit simple and with slow uploads.

The Wifi Module was put there for development and is running ESP-Link which will just let you stream gCode over Wifi.  Prone to disconnects as all Wifi connections are less than ideal.... 



Wifi will not serve the same web interface as the Smoothie ethernet.



I still do believe that using the USB on the board and a Raspberry Pi (running OctoPrint for 3D Printing or the LaserWeb comms server for laser/ cnc) is the way to go if you don't want to be tethered to the computer with a USB Cable.  (Unless you get the GLCD... then you can run files from the MicroSD card in the board via its menus...)



Maybe get the first ReMix, try the DRV8825 in it, see if you like it and that setup works for you...


---
**Todd Mitchell** *November 04, 2017 22:11*

Fair point yet I'm confident I'll like it based on my experience with the mini.  I was merely trying to get the parts right.  I can always start with a laptop and USB then minimize from there.  One will use the DRV8825 and the other will use external drivers with nema 34s


---
**Seon Rozenblum** *February 21, 2018 10:01*

**+Ray Kholodovsky** Are there any docs for connecting a rPI to the Cohesion 3D Mini (for k40) and Lightburn to not need a USB cable? My board is still on the way to Australia, hopefully be here early next week, and I want to use Lightburn (I'm on a Mac) but would prefer not to have to move my Mac to the K40 and tether every cut as I only have the 1 Mac and use it all day long.



Thanks!


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/hCdK8bCGrHM) &mdash; content and formatting may not be reliable*
