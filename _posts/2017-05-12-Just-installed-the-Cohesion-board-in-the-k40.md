---
layout: post
title: "Just installed the Cohesion board in the k40"
date: May 12, 2017 10:02
category: "K40 and other Lasers"
author: "Donny Hilsenrath"
---
Just installed the Cohesion board in the k40. Works great! Only issue I'm having is I'm getting a very loud noise on the X axis while engraving. While jogging around and manually moving there is no noise and it glides smoothly. Belt tension is tight as well. I'm assuming it's a setting in LaserWeb?

![images/6e72d1c15368ed920a21959f5e2ba365.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6e72d1c15368ed920a21959f5e2ba365.jpeg)



**"Donny Hilsenrath"**

---
---
**Donny Hilsenrath** *May 12, 2017 19:53*

**+Peter van der Walt** not seeing a solution there. Is this an ongoing problem? Makes it pretty unusable. 


---
**Todd Fleming** *May 12, 2017 22:06*

This is why brave souls are trying grbl-lpc. Please don't tag Peter. He's taking a break from support questions.


---
**Donny Hilsenrath** *May 12, 2017 22:07*

**+Todd Fleming** I keep seeing that. I may have to give that a go. Thanks!


---
**Jim Fong** *May 13, 2017 03:09*

For now I copy the gcode over to the sdcard and run using the smoothie player.  It will run fast that way. I find that any vector or raster gcode that have feed speeds over 25mm/sec or so will stutter when using Laserweb4 serial.  



I just tested grbl-lpc on a re-ramps arm board and got it to run.  I'll test it on the Cohesion3d/k40 soon.  


---
**Donny Hilsenrath** *May 13, 2017 03:39*

**+Jim Fong** yes please let me know how that goes for you. I'd love to see it in action. 


---
**E Caswell** *May 31, 2017 21:55*

So gents, I know this is a sensitive subject over on LW4 and I have researched it and cant find where or how to set up the grbl-lpc. 

I am a noob to the smoothie, LW and C3D so would appreciate how to set it up.



I have sent messages to Devs but have not received any feed back.

Again, I am not getting at anyone but would appreciate some direction as what to set up, copy and run if that's possble please?



I have run from the card but have had problems with the raster not hitting exactly the same point it should due to some C3D set up issues I have had with running direct from the SD card. As I had been running from LW4 it wasn't a problem until I needed raster for jpeg. Normally use the laser fill process on standard svg files so that's not an issue.



I understand on LW that it's a "taboo" subject but would really appreciate some simple steps of what to do please?








---
*Imported from [Google+](https://plus.google.com/116233537121721630298/posts/czw4WgQj5vK) &mdash; content and formatting may not be reliable*
