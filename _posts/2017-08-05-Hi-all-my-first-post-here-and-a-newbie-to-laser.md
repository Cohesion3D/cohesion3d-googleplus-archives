---
layout: post
title: "Hi all, my first post here and a newbie to laser"
date: August 05, 2017 19:15
category: "K40 and other Lasers"
author: "Kma Jack"
---
Hi all, my first post here and a newbie to laser. 

I've got the K40 Chinese laser and bought the Gohesion3D Mini which I just installed. Can someone please tell me if I connected the cables properly before I switch it on



TIA



Eric





![images/11e6e249ac03f95f73f86f0379459b5e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11e6e249ac03f95f73f86f0379459b5e.jpeg)



**"Kma Jack"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 19:35*

Big connector notch pushing out the white wall of the connector on the board? 



Your Y motor cable, push that in more to make sure it's secure. Maybe add a dab of hot glue later on. 


---
**Kma Jack** *August 05, 2017 19:45*

Thanks for the prompt reply Ray, much appreciate it



Yes, the big connector notch is pushing the white wall of the connector



I've pushed the Y connector in as well.



Is that all connected correctly?


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 19:51*

Looks good to me. 


---
**Kma Jack** *August 05, 2017 20:02*

Thank you. I was a bit afraid to turn it on so thought I'd ask to make sure first. 



Thank you for your help. I'll go and turn it on now and start learning how to use it



Thanks again


---
*Imported from [Google+](https://plus.google.com/107177313666688527432/posts/EzDyvTwScWB) &mdash; content and formatting may not be reliable*
