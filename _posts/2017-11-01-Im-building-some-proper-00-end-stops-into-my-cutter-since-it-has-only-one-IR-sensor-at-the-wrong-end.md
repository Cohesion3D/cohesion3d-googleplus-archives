---
layout: post
title: "I'm building some proper 0,0 end stops into my cutter since it has only one IR sensor at the wrong end"
date: November 01, 2017 18:35
category: "K40 and other Lasers"
author: "Ryan Palmertree"
---
I'm building some proper 0,0 end stops into my cutter since it has only one IR sensor at the wrong end. I have a couple questions before I go mucking with everything though.



What sort of header is used to connect the end stops?  I know it's a JST, but I'm not sure what type will fit that fitting properly.  



Will the JST XHP connector work without issues?

When wiring the endstop should I wire:

 c to gnd

 n/c to (x/y) min



Thanks for the assist!





**"Ryan Palmertree"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 18:38*

Bottom row, there's dedicated X and Y endstop headers. The white connectors with the tabs sticking up. 

Please consult the Mini pinout diagram on the Documentation site. 



You'll also need to change your config.txt for Y (beta) to home to min. 


---
**Ryan Palmertree** *November 01, 2017 20:01*

Thanks for the information!



I wasn't clear in what I was asking, I apologize.  I'm curious about the type of connector to use on the endstop header.



It looks like the JST XHP connectors will work, but before buying a couple I wanted to make sure that was the case. 


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 20:09*

Yeah, I definitely have a 5 pin JST XH on the board there.  XHP looks like the mating housing. 



Center 3 pins are Gnd, X, and Y. 



But if you are wiring new switches I didn't understand why you wouldn't want to use the dedicated ports for them so that you don't need to put both grounds into the 1 XH pin and other crimping that I consider more painful.  (I find the Molex KK to be the easiest to crimp with just a fine needle nose plier)



How you wire on the switches end is up to you.  I prefer to use normally closed wiring for better noise immunity.  You can also always flip this logic in the config.txt.


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 20:15*

Here's a printer with the Mini, and those white connectors are the KK connecting to Sig and Gnd on the Mini going to the NC and C of a simple endstop switch. ![images/de9dbb57df38fdc29340466c46a9740b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/de9dbb57df38fdc29340466c46a9740b.jpeg)


---
**Ryan Palmertree** *November 01, 2017 20:46*

Honestly I am a bit new when it comes to wiring new electronics. I've wired up several things, but most of it was modifying was already there or following a plan.  This is the first real time I have started from scratch (all be it a small change).



I really appreciate the feedback and will take the suggestions to heart!


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 20:52*

That's completely fine.  

Either way will work. 


---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 21:20*

Pronterface and the M119 command will be your best friend when you do this. 


---
*Imported from [Google+](https://plus.google.com/108523384656507675919/posts/6XDLw3sq8JC) &mdash; content and formatting may not be reliable*
