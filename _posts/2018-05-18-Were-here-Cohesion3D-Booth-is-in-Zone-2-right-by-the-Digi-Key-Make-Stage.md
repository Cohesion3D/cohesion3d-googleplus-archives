---
layout: post
title: "Were here! Cohesion3D Booth is in Zone 2 right by the Digi-Key/ Make Stage!"
date: May 18, 2018 18:38
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We’re here! 



Cohesion3D Booth is in Zone 2 right by the Digi-Key/ Make Stage! 



If you’re here come say hello! 



Bay Area Maker Faire

![images/8ca2d491cf06dd9d8faece0e1c477365.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8ca2d491cf06dd9d8faece0e1c477365.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *May 18, 2018 18:47*

Like the banner.


---
**Gary Hangsleben** *May 19, 2018 09:18*

Looks nice Ray.  Great what you have been able to supply the community.  Have a fun weekend, wish I could attend.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/2ZMRjXrvQMf) &mdash; content and formatting may not be reliable*
