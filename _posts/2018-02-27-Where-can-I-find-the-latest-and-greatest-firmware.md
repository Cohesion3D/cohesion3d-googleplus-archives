---
layout: post
title: "Where can I find the latest and greatest firmware?"
date: February 27, 2018 02:26
category: "FirmWare and Config."
author: "Ron Jones"
---
Where can I find the latest and greatest firmware? Been digging and am more confused than when I started. ;-)





**"Ron Jones"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 27, 2018 02:49*

Dunno, Jim made some new grbl-lpc compiles, if you're not having problems then leave what works, alone :)


---
**Ron Jones** *February 27, 2018 02:54*

**+Ray Kholodovsky** I think I’m OK. Current file is from January 2017 so I was curious. 



Also, I like to live dangerously! ;-)


---
*Imported from [Google+](https://plus.google.com/111391078367575553327/posts/3gUcSFCXwCz) &mdash; content and formatting may not be reliable*
