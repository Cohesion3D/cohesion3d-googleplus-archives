---
layout: post
title: "Is there a list of software that is an alternative to laserweb but also compatible with the cohesion3d board?"
date: September 29, 2017 13:45
category: "General Discussion"
author: "ki ki"
---
Is there a list of software that is an alternative to laserweb but also compatible with the cohesion3d board? I have found the [http://smoothieware.org/software](http://smoothieware.org/software) list but was hoping for other alternatives. Thanks.





**"ki ki"**

---
---
**Jim Fong** *September 29, 2017 18:10*

Is this a laser, CNC? What do you want to do with the machine.  Are you interested in cad/cam? 


---
**Ray Kholodovsky (Cohesion3D)** *September 29, 2017 21:41*

Yes, that is the main list.  The board takes gCode so anything that can create that works.  Visicut is the main alternative for laser, but far fewer people use it and thus it is less documented (to the best of my knowledge).  



We are also working hard with the developer of Lightburn, which is currently in a closed beta and the feedback has been great (Jim would know :) )



Jim I believe this is a continuation of this thread: [plus.google.com - Hello, I have just got the cohesion3D mini as a replacement board for my K40 ...](https://plus.google.com/101815393777110076549/posts/bxUNmEuGYyH) - still trying to help with LW settings. 


---
**ki ki** *September 30, 2017 08:21*

Thanks again for the help Ray. Indeed, this question stems from my difficulties in getting laserweb to work correctly with my K40. I was just wondering if there was anything that was easier to understand than laserweb.


---
*Imported from [Google+](https://plus.google.com/101815393777110076549/posts/Keng2gzHp97) &mdash; content and formatting may not be reliable*
