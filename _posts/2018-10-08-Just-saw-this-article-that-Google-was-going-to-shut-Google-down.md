---
layout: post
title: "Just saw this article that Google was going to shut Google + down"
date: October 08, 2018 20:06
category: "General Discussion"
author: "Frank Broadway"
---
Just saw this article that Google was going to shut Google + down. If it's true I hope this forum does not move to Facebook. 





**"Frank Broadway"**

---
---
**Tech Bravo (Tech BravoTN)** *October 08, 2018 20:09*

the facebook group is quite a bit more active than this g+ group.  i'm guessing a good majority of people on this group don't favor facebook. i hate it but its a necessary evil for me


---
**Ray Kholodovsky (Cohesion3D)** *October 08, 2018 20:15*

We have some time to figure out next steps.  There is already a C3D Facebook group that as Brian says is quite active.  I like G+, I know not everyone has Fb, and hosting our own forum (I like the discourse platform) costs money and management time... so not sure of what to do just yet. 


---
**Frank Broadway** *October 08, 2018 21:07*

I don't hate FB. I use FB for personal and business but I feel it's not suited to use as a support site because of how hard it is to search and group threads. Just my 2 cents :)


---
**Don Kleinschnitz Jr.** *October 08, 2018 21:57*

To many  "techwanabees" on FB..... uggh. Sure glad I kept a blog!


---
**Brent Dowell** *October 08, 2018 23:16*

A subReddit might not be a bad way to go.




---
**Brian Albers** *October 09, 2018 02:32*

Have you looked into a slack or discord group?


---
**Ray Kholodovsky (Cohesion3D)** *October 09, 2018 02:50*

I actually did set up a Discord recently.  Just a little scared of the whole "I WANT INSTANT HELP" thing.  I keep an eye on stuff and try to respond in 1 business day if someone wasn't already helped.  Fb has subthreads so that adds to the "keeping track burden".  Now add a constant stream? Can't handle it. 


---
*Imported from [Google+](https://plus.google.com/+FrankBroadway/posts/Pd5Ub8WT5Mp) &mdash; content and formatting may not be reliable*
