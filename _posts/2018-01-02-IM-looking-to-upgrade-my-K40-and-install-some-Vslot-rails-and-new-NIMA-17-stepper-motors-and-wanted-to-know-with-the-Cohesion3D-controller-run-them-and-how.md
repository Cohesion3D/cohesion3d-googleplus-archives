---
layout: post
title: "IM looking to upgrade my K40 and install some Vslot rails and new NIMA 17 stepper motors and wanted to know with the Cohesion3D controller run them and how?"
date: January 02, 2018 20:05
category: "C3D Mini Support"
author: "Bear Cubb"
---
IM looking to upgrade my K40 and install some Vslot rails and new NIMA 17 stepper motors and wanted to know with the Cohesion3D controller run them and how? Right now it's plug n play i know but i want to swap out the rails or build a whole new gantry frame with Vslots and NIMA 17 stepper motors and not sure if I toss the C3d and go another route. Thanks for any help and links.





**"Bear Cubb"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2018 20:11*

Sure... just depends on the size of the motors (there are shorter and longer ones) - and how fast you want to be able to go. The short answer is yes.



As far as the wiring itself, there is an X and a Y motor port at the top for direct connection, check the pinout diagram.



Already answered you on fb :)


---
**Bear Cubb** *January 02, 2018 20:37*

**+Ray Kholodovsky** thanks ill look for it, i think there small 48mm motors 78oz tq 






---
**Mike De Haan** *February 13, 2018 03:42*

I just installed the C3D Mini and I'm having an issue where the controller will respond to a few commands from Lightburn, but will get stuck shortly after. Lightburn says the K40 is "Busy" and will no longer respond to any commands until the system is completely shutdown and restarted.



When I start up, the board has a red led on top, L1 and L4 are solid green, and L2 and L3 are blinking green.



The system will stay alive long enough to "home" and draw half a circle when I submit gcode to it. After that, the system is stuck in "Busy" mode.



I have replaced the USB cable twice to the same effect.



Any advice would be greatly appreciated.


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2018 03:54*

Please paste this content into a new post and I will help you debug. 


---
*Imported from [Google+](https://plus.google.com/102652955276292466478/posts/ZQstQpj6z4j) &mdash; content and formatting may not be reliable*
