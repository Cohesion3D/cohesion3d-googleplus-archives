---
layout: post
title: "When a cut is in process, I sometimes need to stop the cut"
date: January 03, 2019 15:06
category: "C3D Mini Support"
author: "Joel Brondos"
---
When a cut is in process, I sometimes need to stop the cut. Using LightBurn, I hit the Stop button or Pause button, but most often, the laser does not stop or pause.



I'm a little concerned about this for various reasons like safety and wasted stock. Is this a Smoothie issue?



Is there some kind of firmware upgrade I need to make so that when I click the Pause or Stop button on the LightBurn controls, the Cohesion 3D Mini will actually interrupt the cut promptly?





**"Joel Brondos"**

---
---
**Anthony Bolgar** *January 03, 2019 16:21*

The smoothie firmware does not have an instant stop feature, it must clear the commands in the buffer first. If you switched to grbl-lpc you could use the instant stop.


---
**Jim Fong** *January 03, 2019 17:20*

What Anthony said.  I have a K40 with the laser on/off button on top panel.  I’ve learned to use that button for emergency laser off while running smoothieware. 


---
**LightBurn Software** *January 03, 2019 17:27*

**+Arthur Wolf** **+Wolfmanjm** - any chance of this ever happening with Smoothieware 1.0?


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/1o6YDqHX55h) &mdash; content and formatting may not be reliable*
