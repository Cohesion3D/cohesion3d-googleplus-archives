---
layout: post
title: "Hi, Has anyone ever used the cohesion board with a 5 axis cnc?"
date: November 01, 2017 05:34
category: "CNC"
author: "Todd Mitchell"
---
Hi,



Has anyone ever used the cohesion board with a 5 axis cnc?  If so, which software are you using with it?





**"Todd Mitchell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2017 14:30*

The ReMix board supports up to 6 axis, and the Smoothie firmware does indeed support XYZABC for up to 6 axis movement like what you'll need. 



So, it'll take the gCode, but you'll need the CAM software to make that gCode. 



Fusion360 would be the first software I check out. 



[cnccookbook.com - It Finally Happened: 5 Axis CNC for the DIY and Maker - CNCCookbook: Be A Better CNC'er](https://www.cnccookbook.com/it-finally-happened-5-axis-cnc-for-the-diy-and-maker/)



I did a google search for "5 axis cnc cam" and that was the first result, along with some others. 


---
**Todd Mitchell** *November 02, 2017 01:27*

Thanks.  Yea - I did the same.  I'm looking at your remix board compared to the smoothieboard and cannot distinguish any differences that matter.



Question: Can I use a touch screen like this one:



[amazon.com - Amazon.com: KINGPRINT 3D Printer Controller Board MKS TFT32 3.2-Inch Full-Color Touch Screen for 3D Printer: Industrial & Scientific](https://www.amazon.com/KINGPRINT-Printer-Controller-3-2-Inch-Full-Color/dp/B074TZ79GC/)


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 01:30*

I have that here. It just feeds gCode to the board, so yes. Simple connection to gnd, 5v, and rx tx serial header on C3D. 


---
**Todd Mitchell** *November 02, 2017 01:41*

--> It just feeds gcode to the board



Not sure what you mean?  any issues with the touch screen with the onboard UI?



Are you saying it ok to use the SD card reader on the LCD?



<b>***</b>



One other thing - I'm going to use two of these boards - one for a sherline 2000 which has 2.8A



For another one of my machines, I'm going to use some heavy duty steppers ([https://www.amazon.com/Nema-Shaft-Stepper-Motor-KL23H2100-50-4BM/dp/B007QMD656/ref=sr_1_1?ie=UTF8&qid=1509586551&sr=8-1&keywords=stepper+motor+oz-in](https://www.amazon.com/Nema-Shaft-Stepper-Motor-KL23H2100-50-4BM/dp/B007QMD656/ref=sr_1_1?ie=UTF8&qid=1509586551&sr=8-1&keywords=stepper+motor+oz-in)) 





I was looking at this guy: DRV8825 ([https://www.pololu.com/product/2133](https://www.pololu.com/product/2133)) which lists 2.2a per coil.  the stepper is rated at 3.5A per phase.  so it looks like it's not strong enough.  do you know of any alternatives that do not require me to use an external driver?

[amazon.com - Nema 23, Dual Shaft Stepper Motor 570 Oz-in, KL23H2100-50-4BM: Electric Motors: Amazon.com: Industrial & Scientific](https://www.amazon.com/Nema-Shaft-Stepper-Motor-KL23H2100-50-4BM/dp/B007QMD656/ref=sr_1_1?ie=UTF8&qid=1509586551&sr=8-1&keywords=stepper+motor+oz-in)


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 01:45*

You're going to want to use external black box stepper drivers, no question. Simply can't cool a stepstick even remotely well enough for those amps. 



We have the external stepper adapter to make wiring easier. 



That screen is for the most part a one way interface. You can jog XYZ, programm some UI buttons to run more gcode scripts, and you can use the SD slot in it to run jobs from. But it's not like the GLCD that connect directly to the board where you can actually see coordinates and what the machine is doing. 


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 01:46*

Borrowed this pic from another post showing the external stepper adapters on Remix. We also have a guide on wiring them in the k40 docs. ![images/08602150e25a9e1f1d5348aab62e1aa8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/08602150e25a9e1f1d5348aab62e1aa8.jpeg)


---
**Todd Mitchell** *November 02, 2017 01:49*

Steppers - thanks.  For the large ones, i'll use the external black box.  For the sherline, i'll use the DRV8825 (do you sell these per chance?)



As for the screen, I was considering extending the built-in UIs to add some setup features like - probeZ, probe center circle, etc. which is why the touch screen is ideal.


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 01:52*

Yep, the MKS screen can be useful for that. 



Worth noting that custom commands can also be configured onto the GLCD Menu via the Smoothie config.txt. 



I have some DRV's here, sure. 


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 01:56*

Just let me know when you're ready to order and I'll set everything up for you. 



I'm going to recommend that you get a full set of the external stepper adapters to be safe. Decent chance that the DRV frustrates you... 


---
**Todd Mitchell** *November 02, 2017 01:57*

Nice.  Is there a configuration setting to set it at 1/16 or 1/32nd step?  How can I order those DRV's from you rather than a separate order on amazon?  




---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 02:00*

There's solder jumpers on the back of the board. They're hardcoded to max so for DRV that'll be 1/32. You can cut some traces to manipulate the microstep settings. Recommend leaving at max. 


---
**Todd Mitchell** *November 02, 2017 02:01*

--> external stepper adapters:  I'll be using those for my 5 axis.  I first need to get the sherline up and running.  these motors are rated 3.v @ 2.0A



[sherlineipd.com - www.sherlineipd.com/stepspec.pdf](http://www.sherlineipd.com/stepspec.pdf)




---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 02:02*

Please promise me lots of pictures? :) 



I'll go check on the DRV's. 


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 02:06*

How many DRV's might you need? 


---
**Todd Mitchell** *November 02, 2017 03:55*

5 DRVs with the remix.  and what is the name of the stepper motor wire connector?


---
**Todd Mitchell** *November 02, 2017 03:56*

How much are you asking for the DRV's anyway?


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 03:56*

Sorry, I checked earlier and I do not have enough.  An amazon order will be cheaper anyways. 

Which wire/ connector? 


---
**Todd Mitchell** *November 02, 2017 04:01*

All good.  the wire connector that goes to the stepper.



pin # ' dir1x, dir2Y, dir3Z, etc.'





Pictures? sure :)  i managed to find some ridiculously sturdy (lab grade) XY stage tables that weigh like 200lbs each so I'm hoping to have something worthy of being called a mill rather than an engraver.


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 04:05*

Ah, the white polarized header.  Those are Molex KK and there's also the chinese version going by KF2510-4p.  Endstops are 3p of the same. 



Btw, everything going well with the de-<b>cap</b>-acitated mini? 


---
**Todd Mitchell** *November 02, 2017 04:20*

Nice.  Just need to find some pre-wired.



de-cap'd mini - yes, last i used the laser cutter all was well.  such an easy switch. thank you.





Now, I'm hoping to use your remix board to build a similar setup to the laser cutter except with an LCD so i dont need multiple computers to control it.   I have a gecko on my other mill but it requires mach3, an old pc and.. well you know how that goes.


---
**Todd Mitchell** *November 02, 2017 04:21*

although my mate who was using the laser cutter it recently said it was acting funny..  but i hope it was her error


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 04:28*

May want to grab a GLCD then to see if you like the interface Smoothie provides. 



You may also want to talk to **+Jim Fong** , you're speaking his language at the moment. 


---
**Todd Mitchell** *November 02, 2017 06:19*

board was ordered.  I decided to skip the panel for now on both fronts so i can focus on the machines.



do you happen to know a good method to calculate the PSU needed?  I sit stepper voltage * stepper count?  (same for amps)?


---
**Todd Mitchell** *November 02, 2017 06:19*

Thanks again for the chat


---
**Ray Kholodovsky (Cohesion3D)** *November 02, 2017 15:01*

For the DRV8825 drive I'd use 24v and max 2a per stepper. Add 1 amp for logic and misc.



A 24v15a PSU LED Style is common. 



For external stepper drivers you could just run the board off USB. 


---
**Todd Mitchell** *November 03, 2017 21:43*

Hey Ray, I ordered a board 2 days ago.  Status is awaiting fulfillment.  Are you back-ordered per chance?


---
**Ray Kholodovsky (Cohesion3D)** *November 03, 2017 21:45*

Hey Todd.  Boards are here, I just need to solder up the external stepper adapters.  Everything will go out shortly.  :)


---
**Todd Mitchell** *November 03, 2017 21:48*

oh, wait.. I ordered the adapter to be used later.  I ordered a set of DRVs for this board


---
**Todd Mitchell** *November 03, 2017 21:51*

I'm just a bit confused.  I assumed they plugged in like the normal drivers.  is that the case (and you're really just saying you need to make them but not attach to the cohesion board)?




---
**Ray Kholodovsky (Cohesion3D)** *November 03, 2017 21:52*

Yeah they're separate, what you said. It's the one thing I still solder by hand. 


---
**Ray Kholodovsky (Cohesion3D)** *November 03, 2017 22:27*

Everything I ship is still a bit handcrafted/ tested/ prepared in some fashion. Thank you for your patience! ![images/3d888f29502fcbfe848c3c38db5f1fd0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d888f29502fcbfe848c3c38db5f1fd0.jpeg)


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/9yxunYLEDQP) &mdash; content and formatting may not be reliable*
