---
layout: post
title: "How long does it take to fill a order bought one yesterday and it says still waiting to be filled"
date: July 17, 2018 23:10
category: "C3D Mini Support"
author: "ryan"
---
How long does it take to fill a order bought one yesterday and it says still waiting to be filled





**"ryan"**

---
---
**Tech Bravo (Tech BravoTN)** *July 17, 2018 23:13*

![images/ef3406cfd9a19ca3f1a7e7bfa4fb886d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef3406cfd9a19ca3f1a7e7bfa4fb886d.png)


---
**Griffin Paquette** *July 17, 2018 23:13*

Shouldn’t be too long! The C3D team is pretty good to ship. 


---
**ryan** *July 17, 2018 23:14*

So 3-4 days to fill? Thought that ment to you in 3 days


---
*Imported from [Google+](https://plus.google.com/100696059698311964115/posts/dHfwjQqcYZ2) &mdash; content and formatting may not be reliable*
