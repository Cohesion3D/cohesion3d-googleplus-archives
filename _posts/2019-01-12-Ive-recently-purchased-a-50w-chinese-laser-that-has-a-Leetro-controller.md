---
layout: post
title: "I've recently purchased a 50w chinese laser that has a Leetro controller"
date: January 12, 2019 17:00
category: "C3D Mini Support"
author: "Brandon Cullum"
---
I've recently purchased a 50w chinese laser that has a Leetro controller.  I wanted to see if that could use a C3D controller so that I could use Lightburn.  I know Leetro is a potential for the future of Lightburn but its not supported right now.







![images/73f5d79e6e91e9ccba09a5622b3f845c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/73f5d79e6e91e9ccba09a5622b3f845c.jpeg)
![images/0fe005329d327bbff5b654dbd7cc29a7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0fe005329d327bbff5b654dbd7cc29a7.jpeg)
![images/dc21ca7cc21d8b6aa5d2632da98fc3ee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dc21ca7cc21d8b6aa5d2632da98fc3ee.jpeg)
![images/f8cc927316af50ea600b948f4f9f186f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8cc927316af50ea600b948f4f9f186f.jpeg)

**"Brandon Cullum"**

---
---
**Tech Bravo (Tech BravoTN)** *January 12, 2019 18:34*

The LaserBoard, while designed to be a drop-in replacement for the M2 Nano board, has been thoughtfully engineered to provide scew terminals and connections for flexible installation in a wide array of devices. Long story short, yes you could replace the leetro controller with a LaserBoard with a few variations in wiring and configuration.


---
*Imported from [Google+](https://plus.google.com/105401509639508418326/posts/iohQAN22PBY) &mdash; content and formatting may not be reliable*
