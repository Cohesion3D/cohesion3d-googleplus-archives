---
layout: post
title: "I installed the Cohesion 3D Mini in my laser and it is working"
date: January 20, 2017 14:07
category: "General Discussion"
author: "Mike Mauter"
---
I installed the Cohesion 3D Mini in my laser and it is working. It was a pretty standard installation using the 3 wire PWM cable(power supply has the white connectors). What I am having a problem with is that I have to set the minimum laser power to 75 to 80% when burning a calibration file for it to even mark in the 0 to 50% range.  I have 2 K40's This particular laser has always required a higher setting to do the same thing as the other laser. I have voltmeters on both lasers. This laser would take 1.6 volts to produce 4 to 5 milliamps where the other one only requires .5 volt to produce 4 to 5 milliamps. I have had a Smoothieboard(one of Scott's ACR kits) in this laser before and it worked.Comparing the Configurations files I don't see any great differences in the laser power settings. Do you have any ideas of something that I can try to get this dialed in? 





**"Mike Mauter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 16:19*

Mike, you may be better off putting the pot back into play, removing the pwm cable, and running L to the - bed/ 2.5 MOSFET terminal. It's described in a bunch of other thread comments here. 

![images/57ab14200e36a606dfdcafcc6940de32.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/57ab14200e36a606dfdcafcc6940de32.png)


---
**Mike Mauter** *January 20, 2017 18:19*

Thanks Ray, I will give that a try. 


---
**Bill Keeter** *January 20, 2017 18:54*

**+Mike Mauter** FYI, i'm running the laser at 100% in Laserweb using the white PWM wire. It's showing 10mA when firing. I think the power level in Laserweb is directly controlling your mA. Also I don't think 100% power in Laserweb is equal to 100% power of the tube. Just me 2 cents from tests last night.



I even ran 110% power in laserweb and got 12mA. 



(please note, this is only confirmed in my K40 setup. Who knows if others will get the same)


---
**Mike Mauter** *January 21, 2017 02:04*

Bill, are you running through the pot?. My understanding is that when the pot is included that the allowed current is a product of the laser web setting and the pot setting. I hooked mine up as Ray suggested and it seems to be working better now. I didn't have time to check it that closely but it appears that I have greater range in grayscale. 




---
**Bill Keeter** *January 21, 2017 02:26*

I might switch it but right now I'm running the white wire from the pcb to the power supply without the pot. 


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 02:31*

Ok.  I'm very open to hearing data points on this. As we're reaching more and more machine types out there the pwm pot approach may be a pain point. Ah, fun fun.


---
*Imported from [Google+](https://plus.google.com/106963308841855914613/posts/Qsds5joK8TG) &mdash; content and formatting may not be reliable*
