---
layout: post
title: "In need of some help. I have a red k40 laser and I'm not sure witch port on the psu to use for the laser"
date: March 05, 2017 04:38
category: "C3D Mini Support"
author: "Perro Malo"
---
In need of some help.

I have a red k40 laser and I'm not sure witch port on the psu to use for the laser.

I have tried G, -k and IN and as soon is in contact with -2.5/bed the laser will fire.

Any help will be appreciated.



Thanks 

![images/05a79c9fe2279ac09af3c9b6526fdcec.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/05a79c9fe2279ac09af3c9b6526fdcec.jpeg)



**"Perro Malo"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 04:44*

You would need to put all the wiring back as it was.  You want to connect L to 2.5.  L is the blue wire on the right.  But before you start uplugging or cutting wires:

Did you just get your board?  The wiring is different between v2.2 (last batch) and v2.3 (the ones that just started arriving today). 


---
**Perro Malo** *March 05, 2017 04:52*

Just got it today. 


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 04:55*

I was putting together a new upgrade guide.  I have the pictures I took, still writing the text to accompany them.  Hope this helps get you started.  Like I mentioned elsewhere we're not replacing the pot so leave that in play.  

And if you have a different wiring variant than what you see here that's find, just let know. 

[dropbox.com - C3D Mini Laser v2.3 Photos](https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0)


---
**Perro Malo** *March 05, 2017 05:04*

Thanks for the pictures.

Everything else is working fine. I can jog  and home the axis with laser web.

Is 2.5 at the same location that is in the pin out diagram on the web site?  


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 05:05*

So with the new board you don't need to worry about 2.5. It's already wired. 


---
**Perro Malo** *March 05, 2017 05:36*

It was my mistake. I was pressing the test fire on laser web and that didn't work. So I thought it had to do with the 2.5 pin. I send the command to test the laser and it worked!!  Thanks for your help!!


---
*Imported from [Google+](https://plus.google.com/111451898661193016661/posts/TGJF39yxk6A) &mdash; content and formatting may not be reliable*
