---
layout: post
title: "LightBurn 0.8.02 released - Embedded Illustrator images, filled rendering, Linux fixed, Ruida scan fixed, and much more:"
date: August 26, 2018 02:44
category: "Other Softwares"
author: "LightBurn Software"
---
LightBurn 0.8.02 released - Embedded Illustrator images, filled rendering, Linux fixed, Ruida scan fixed, and much more:



[https://lightburnsoftware.com/blogs/news/lightburn-0-8-02-support-for-embedded-images-filled-rendering-linux-dependencies-fixed-and-much-more](https://lightburnsoftware.com/blogs/news/lightburn-0-8-02-support-for-embedded-images-filled-rendering-linux-dependencies-fixed-and-much-more)





**"LightBurn Software"**

---


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/YKiqKhLVZ7p) &mdash; content and formatting may not be reliable*
