---
layout: post
title: "This is really cool and I've been a long time fan of the design and Rene's work"
date: April 27, 2017 18:36
category: "Show and Tell"
author: "Ray Kholodovsky (Cohesion3D)"
---
This is really cool and I've been a long time fan of the design and Rene's work. Guess what board it runs. 



<b>Originally shared by René Jurack</b>



Now it's official. The DICE is called the "sexiest tiny printer". 😋





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Griffin Paquette** *April 27, 2017 19:26*

**+Peter van der Walt** I was thinking MKS Sbase knowing Ray but could be that too :)


---
**René Jurack** *April 27, 2017 19:34*

Well, I don't think it's a ReMix. There would be no room for it. ;)


---
**Claudio Prezzi** *April 27, 2017 20:56*

It's a C3D Mini :)


---
**Maker Fixes** *April 30, 2017 00:56*

This is a very cool looking machine, with tons of overkill, yea overkill is not needed but I like overkill!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/P4kNoUh2PkT) &mdash; content and formatting may not be reliable*
