---
layout: post
title: "I bought a mA meter but nut sure where to tie it in with K40 using the mini?"
date: May 01, 2017 10:11
category: "General Discussion"
author: "Renegadebgp Nexus"
---
I bought a mA meter but nut sure where to tie it in with K40 using the mini?





**"Renegadebgp Nexus"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 01, 2017 12:10*

It would not be connected to anywhere on the Mini.  You can try asking in the k40 group they can advise best:  [K40 LASER CUTTING ENGRAVING MACHINE](https://plus.google.com/communities/118113483589382049502?iem=1)


---
**Joe Alexander** *May 01, 2017 13:04*

if its an analog ma mater like in all the pictures then it goes between the tube negative and the Laser PSU L- effectively reading the output of the tube. (Note this is NOT the red wire that comes out of the big transformer!)


---
**Sebastian Szafran** *May 01, 2017 13:32*

Disconnect power from the laser and let it discharge fully. You don't want to shake high voltage hand.

Ammeter should be connected in series - imagine that you cut the tube's negative wire (usually black) and connect ammeter to both ends of the wire. In case the arrow leans out to the opposite direction, reverse polarity by swapping wires to ammeter connections.


---
**Renegadebgp Nexus** *May 02, 2017 14:45*

Thanks


---
*Imported from [Google+](https://plus.google.com/116995074291856974627/posts/M8eKaD5E2pq) &mdash; content and formatting may not be reliable*
