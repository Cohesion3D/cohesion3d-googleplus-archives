---
layout: post
title: "Just started playing with my K40 and Cohesion3D upgrade: Would you recommend setting the analog potentiometer to a high level (>50%) and control the actual power by software?"
date: January 14, 2018 11:55
category: "K40 and other Lasers"
author: "Christoph Bisping"
---
Just started playing with my K40 and Cohesion3D upgrade: Would you recommend setting the analog potentiometer to a high level (>50%) and control the actual power by software?



As I plan is to cut/engrave different materials I really want to store the dependent parameters in somekind of database rather than fiddling around with the knob.





**"Christoph Bisping"**

---
---
**Joe Alexander** *January 16, 2018 07:37*

I use a similar board and I personally set my pot at ~15ma(measured by the analog ammeter that is stock) and tweak my settings in software. engravings I start at 10ma but have recently been changing my settings to work at 15 so I dont have to change it hardly at all.


---
**Christoph Bisping** *January 16, 2018 20:56*

Thanks. So basically you say that you won't use more than 15mA at all and just regulate power up to this value? That makes sense in terms of laser lifetime.


---
*Imported from [Google+](https://plus.google.com/106971700969979677692/posts/gXkNTY12Lo9) &mdash; content and formatting may not be reliable*
