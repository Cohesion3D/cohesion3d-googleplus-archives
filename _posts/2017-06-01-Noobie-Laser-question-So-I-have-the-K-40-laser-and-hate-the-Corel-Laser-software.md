---
layout: post
title: "Noobie Laser question So I have the K 40 laser and hate the Corel Laser software"
date: June 01, 2017 01:35
category: "K40 and other Lasers"
author: "Keith Burns"
---
Noobie Laser question



So I have the K 40 laser and hate the Corel Laser software. I purchased the Cohesion3D board and with a quick answer from Ray, I have it up and running sort of. That leads to several questions.



1. I can jog the x and y axis in LaserWeb4 with no issues. I moved it to the home position (upper left) with the jog controls  and then did a SET HOME. I can now move the laser carriage to anywhere and press the lower button and it will go to home, but if I hit the HOME button, it will not. 



2. Does anyone have a very simple LaserWeb4 project file that they can share for me to test with? So far, ever SVG file I have loads but LaserWeb tells me there is an issue with it, so I get no GCode.  That is a LaserWeb/User Error, not a problem with the laser cutter, so having a test file, if only a small circle to verify with would be helpful.



3. My old control board had 3 cables, a flat ribbon, power and a single 4 pin cable. The original instructions indicate where 2 of the 3 went. Ray updated once I opened a ticket, so now it tell me where the cable goes, but not which pin goes where. I matched the installation to another thread in this group, so it seems to work, but more details would have been nice.



4. I also upgraded the laser head to one with air assist. I got it from [lightobject.com](http://lightobject.com). Their instructions were vague at best, but I suspect that I might have the final lens in backwards. The power I am getting out of the bottom is very low. I can burn paper, but I used to cut .25 inch plywood with the old Corel Laser. Any thoughts or links that would help to make sure I have that part correct would be helpful.



Thanks! This is a great forum. I got lots of questions answered.





**"Keith Burns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 01:39*

1. Homing is top left (0, 200), but origin is bottom left (0,0) so you would set zero at the bottom left corner of your piece, or you would home to the top left. 

3. Which leads me to answering, when you home (G28.2) the head should move to the back left.  If it moves the other way, you need to power down and flip the Y motor cable.  Colors don't matter, just the orientation of the plug needs to match what I described. 

4. You might get a better answer in the K40 group, linked in the about community here if you don't know it already. 



See, even quicker answer from me when you use G+ :)


---
**Joe Alexander** *June 01, 2017 01:41*

almost got it first, do'h :P


---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 01:42*

Joe, I have work for you.  Search this group for "G28.2" and find a better description of what I wrote for #1 and 3 here.  I think I've written some gold on the matter before.  I'll add it to the FAQ since that's currently the #1 thing I end up explaining.  :)


---
**Joe Alexander** *June 01, 2017 01:43*

as for you lens it should be installed bump up for orientation. Make sure your beam is hitting dead center and not getting clipped by the air nozzle, had to slightly adjust my laser tube to compensate for this and now it cuts like a champ


---
**Joe Alexander** *June 01, 2017 02:09*

kk will do :)


---
**Keith Burns** *June 01, 2017 03:28*

**+Joe Alexander** 

Joe, how do I verify that it is hitting dead center of the lens? If I align to the dead center of the hole to the left, facing the left mirror, does that ensure that the laser is in the center of the  lens?




---
**Keith Burns** *June 01, 2017 03:31*

Ray, let me make sure I understand what you are saying. The "HOME" position is going to be the upper left which would be 0,200, but the origin is lower left or 0,0? Is that correct? The lower left is the origin and home positions for my CNC, so why are they different?


---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 03:35*

Kinda yeah, it homes to the back because the K40 has the switch in the back, but standards put 0,0 as the front left. 


---
**Keith Burns** *June 01, 2017 03:46*

OK, I can go with that. So my original question was what should happen when I press the HOME button on the LW4 control screen? I can  set 0,0 (which I did to the wrong place) and the lower go to origin or something goes to that point, but based on what you are saying, which makes perfect sense now, it should go to the lower left. When I press it, it does nothing.




---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 03:47*

In the settings, the homing gCode must be defined as G28.2 

Then when you press the home button the head should move left and back, touch off against the switches there, and stop. The position should read as 0, 200


---
**Keith Burns** *June 01, 2017 03:52*

So if it does not move when you press home, what is wrong? The laser does not move at all.

When I tell it to go to 0,0 it will move and if I jog the controls it moves.


---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 03:55*

Please use the terminal at the bottom of LW to send the G28.2 command.


---
**Ashley M. Kirchner [Norym]** *June 01, 2017 04:09*

For the Home to work in LaserWeb, go in Settings -> Gcode, scroll down and in the 'GCODE HOMING' box, enter "G28.2" (without the quotes.)



As for the error message you're getting, I have yet to come across an SVG file that doesn't generate an error. Doesn't mean there is anything wrong. There is no gcode until you create whatever operation you want (engrave, cut, path fill), and then click on the Generate button.


---
**Keith Burns** *June 01, 2017 04:31*

Ashley,



Thanks! That helps. I just quit after getting the error on the file as I could not figure out what the error was. I have used the same files in Aspire and they work fine. Appreciate the help!




---
**Ashley M. Kirchner [Norym]** *June 01, 2017 04:46*

Yup, there's no indication of what the error actually is. Rather pointless in my opinion. I export SVG files every day and they work just fine despite that apparent error with the file ... whatever it is.


---
**Joe Alexander** *June 01, 2017 04:57*

just noticed the posts, that's what I get for looking away.

  So what I do is stick a label on the laser head between mirror 2 and 3 (so the upper opening not after focus mirror). I have ones i printed with lil targets on em that i center in that hole. Then i fire a quick shot in all four corners checking to see that:

a) all of the dots are stacked on top of each other

b) the stack of dots are centered in the hole so as to be hitting the last reflection mirror dead-center

  then stick a label on the very end to see if you get a clean round circle with a whiteish dot in center.

    if the dots aren't stacked then intensity will fluctuate from location to location on the workspace, and inversely if your not dead-center then your beam could be getting "clipped" by the air nozzle. If the air nozzle is the issue you might notice a crescent shape output from nozzle end(beam hits wall and energy deflects from target) Most of us know alignment is a huge issue and only gets better with every tuning(practice makes perfect)

Look up the floating wombat alignment guide, best one I have read so far. And check out **+Don Kleinschnitz**'s blog about his various updates, they are all well documented. might as well throw in a #RTFM :) Main documentation website for laserweb is [cncpro.co](http://cncpro.co).



TL;DR  read [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) [cncpro.co](http://cncpro.co) and do floating wombat alignment guide many times :)


---
**Anthony Bolgar** *June 01, 2017 06:55*

**+Todd Fleming** any reason the console is displaying the error if the files still work? Are you devs using it for debugging, or is it something we can get rid of?


---
**Keith Burns** *June 01, 2017 12:50*

**+Joe Alexander** Joe any chance you can share a pdf of your target labels? I will check those things out. I have been using another guide on the internet that works, but does not sounds a thorough as what you are describing. I do believe that I am hitting something or am off center. The power to the board below is nothing like it was before the upgrade.


---
**Ray Kholodovsky (Cohesion3D)** *June 01, 2017 12:58*

In config the max power is limited to 0.8 by default. You can change that to 1 or disable the line by putting a # in front of it. But that's if you're using software test fire or actually running a job. 


---
**Todd Fleming** *June 01, 2017 14:27*

**+Anthony Bolgar** we should filter out messages for things we know don't matter (e.g. things which live in the sodipodi namespace). I'll create an issue.


---
**Joe Alexander** *June 01, 2017 14:41*

i can look for the file but i quite literally googled "crosshair" took one that looked decent with 2-3 rings and printed them on a 2"x4" label at 3/4" sizing. was random extra labels. for a easy cal check make a file in inkscape, adjust document to 300x200mm(to match your work area). make a dot in all 4 corners and the middle and run. It will check how accurate your calibration is via the dot stack method :)


---
**Joe Alexander** *June 01, 2017 15:04*

heres the crosshair i use for calibration

![images/147c2f8abb9af73bb7f120805dc4ae4a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/147c2f8abb9af73bb7f120805dc4ae4a.jpeg)


---
**Anthony Bolgar** *June 01, 2017 16:10*

Thanks **+Todd Fleming**


---
**Ashley M. Kirchner [Norym]** *June 01, 2017 16:34*

**+Todd Fleming**​, if there is a fixable issue with the file, wouldn't it be more helpful to add that to the error message in the console, perhaps as a single collapsed line which the user can then click on to see the full error? When those errors first starting popping up they were a little more descriptive and I could actually track it down in my files. But now it doesn't tell me anything other than 'there's a problem'... 


---
**Todd Fleming** *June 01, 2017 16:36*

**+Jorge Robles** is **+Ashley M. Kirchner**'s request doable?


---
**Jorge Robles** *June 01, 2017 18:15*

I will take a look :)


---
**Jorge Robles** *June 02, 2017 07:48*

Available on next release :)


---
*Imported from [Google+](https://plus.google.com/105762576990882303746/posts/SXZV18S5drD) &mdash; content and formatting may not be reliable*
