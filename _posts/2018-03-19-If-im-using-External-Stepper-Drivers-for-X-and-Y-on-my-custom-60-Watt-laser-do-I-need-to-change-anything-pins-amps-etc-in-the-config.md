---
layout: post
title: "If i'm using External Stepper Drivers for X and Y on my custom 60 Watt laser do I need to change anything (pins, amps, etc) in the config?"
date: March 19, 2018 03:03
category: "K40 and other Lasers"
author: "Bill Keeter"
---
If i'm using External Stepper Drivers for X and Y on my custom 60 Watt laser do I need to change anything (pins, amps, etc) in the config? I've been following your Z-bed guide. With the only difference i'm using NEMA23 and external stepper drivers from OpenBuild. I've gone through my wiring and verified the wires and dip switches. 



When powered up the system the drivers come on and the motors lock in place. Then using the GLCD I scroll to jog and try to move X or Y. Nothing. Then I try to home. Nothing. I'm wondering if there's something simple i'm forgetting.



(The system is running a stand alone 24v PSU.)





**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2018 03:05*

Still Smoothie firmware? 


---
**Bill Keeter** *March 19, 2018 03:14*

**+Ray Kholodovsky** yeah, still running your stock C3D mini with the external drive adapters with your config file. Running X and Y without a Z. As most of my material is pretty thin (3 to 6mm).



Looking at your config for Z, i'm wondering if I need to add "!o" to the end of the 3 pin references for both X and Y in the config file.


---
**Ray Kholodovsky (Cohesion3D)** *March 19, 2018 03:15*

Yes, you do. It's in the guide :) 



I'm assuming you wired the driver similarly to how it's shown in the guide? 


---
**Bill Keeter** *March 19, 2018 04:36*

**+Ray Kholodovsky** and it's alive. whoohoo. 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/1C6pxMz8Y38) &mdash; content and formatting may not be reliable*
