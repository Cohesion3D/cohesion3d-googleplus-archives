---
layout: post
title: "We're carrying optics for Laser Cutters now!"
date: May 08, 2018 22:43
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We're carrying optics for Laser Cutters now!  In particular 20mm MO Mirrors (which should hold up to some abuse) and 12mm ZnSe lenses for the stock K40, as well as 18mm ZnSe lenses if you've upgraded your head. 



We hope you'll find these to be high quality optics that make your laser work better at a reasonable price.



[http://cohesion3d.com/optics/](http://cohesion3d.com/optics/) 



![images/3c7a9c8f8f537fef012e65e4f1d13d4c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c7a9c8f8f537fef012e65e4f1d13d4c.jpeg)
![images/73e88c9287e272ea8a9eafdf27ade615.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/73e88c9287e272ea8a9eafdf27ade615.png)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Don Kleinschnitz Jr.** *May 09, 2018 13:25*

Can you tell us something about the quality of these mirrors/lens. I am not sure how to quantify such. What does the manufacture tell you? Who makes them?

I have an order sitting in my cart :)!


---
**Ray Kholodovsky (Cohesion3D)** *May 09, 2018 17:12*

I’m using the same manufacturer that Hakan does. He says the quality is as good as LightObject or better. I don’t know how to quantify it though. The optical imaging sensor/ tester to test reflectivity or whatever can cost half a million bucks! 


---
**Kelly Burns** *May 10, 2018 18:17*

I purchased MO mirrors a couple months ago, but didn’t install them yet. Great that you are carrying these.  Been wanting a spare 18mm lens, so I think I’ll grab one. 


---
**Don Kleinschnitz Jr.** *May 10, 2018 21:30*

**+Ray Kholodovsky** i ordered a set. Been on my list but never got around to the research to get good ones.


---
**richieharcourt** *May 11, 2018 21:12*

Does anyone have a 'recommendation for the complete package of flying optics to build a laser ? I see a lot of them for sale I'm just wondering which are better quality better machined parts with quality finishing ??? 


---
**Kelly Burns** *May 11, 2018 22:17*

**+richieharcourt** I am building a laser and have most of the parts.  I was going to stick with same size as my K40 (Lens:18mm Mirror:20), but the price wasnt much more to go to 20 and 25mm and it should make aligning a bit easier.   I haven't bought the Mirrors and Lens yet, but I did buy the Mounts and Laser Head from LightObject.      

Lens Holder: [https://www.lightobject.com/Pro-Laser-Head-Mount-for-25mm-Mirror-20mm-Focus-Lens-LR-Type-P586.aspx](https://www.lightobject.com/Pro-Laser-Head-Mount-for-25mm-Mirror-20mm-Focus-Lens-LR-Type-P586.aspx)  



Mirror:  [lightobject.com - Pro 25mm Reflection Mirror Mount for Co2 Laser Machine](https://www.lightobject.com/Pro-25mm-Reflection-Mirror-Mount-for-Co2-Laser-Machine-P588.aspx)


---
**Kelly Burns** *May 11, 2018 22:27*

For those that are wondering about the MO mirrors, the benefit is supposed to be that they are more tollerant of a dirty/dusty environment.   My Laser is in my shop.  I haven't put mine in yet because I was trying to maximize power.  The MO's are slightly less reflective than the SI ones so in theory slightly less power.  In a Dusty enrionment, the SI will get dirt baked on very quickly and will be less efficient or possibly build up heat and crack.  


---
**Don Kleinschnitz Jr.** *May 11, 2018 23:19*

**+Kelly Burns** what causes one material to be more tolerant than another to dirty environments?


---
**Mario Gayoso** *July 25, 2018 17:59*

**+Ray Kholodovsky** If I have a 12 mm lens but I upgraded to air assist can I ad a 3d printed ring to it to make up to 18mm diameter?


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/imoH3APz94x) &mdash; content and formatting may not be reliable*
