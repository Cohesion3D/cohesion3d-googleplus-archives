---
layout: post
title: "Smoothieware vs GRBL-LPC Now that GRBL has been ported to LPC and can be used on our boards, I was wondering what the pros and cons of each firmware are..."
date: June 04, 2017 14:08
category: "FirmWare and Config."
author: "Dushyant Ahuja"
---
Smoothieware vs GRBL-LPC



Now that GRBL has been ported to LPC and can be used on our boards, I was wondering what the pros and cons of each firmware are...



One of the things that is making me consider GRBL is Jog Controls: [https://github.com/gnea/grbl/wiki/Grbl-v1.1-Jogging](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Jogging)



This brings my favourite feature from Marlin (babystepping) - and one that the developers of Smoothieware do not even consider to add.



However, I'm not sure if Smoothieware has better motion planning or GRBL.



Would like to understand from this community what the pros and cons are of one firmware over the other. Have yet to take the plunge.





**"Dushyant Ahuja"**

---
---
**Claudio Prezzi** *June 04, 2017 15:12*

One pro of Smoothieware is probably the easier config file (all in one place). With grbl-LPC some params like pin mapping has to be done with compile switches before compiling. Another pro is the support for 3d-printing, which grbl hasn't. And not to forget the bigger community that can help.



On the other side, with grbl-LPC you can reach much faster feeds (3-4x) for raster images without stutter. Grbl has realtime commands like pause/resume (that can even stop a already running move with decelleration) or feed/spindle overrides that are immediate. Grbl has special jogging that can be cancelled. And the grbl motion planner is more efficient in my opinion.


---
**Dushyant Ahuja** *June 04, 2017 15:57*

I didn't realise that GRBL doesn't support 3D printing. Didn't even think to check. 


---
**Marc Miller** *June 04, 2017 17:53*

This page says "not released yet" but it looks fairly promising.

[smoothieware.org - jogger [Smoothieware]](http://smoothieware.org/jogger)


---
**David Cantrell** *June 05, 2017 19:50*

It sounds to me that grbl-LPC might be a better option for CnC tables/laser cutters. **+Ray Kholodovsky**, Any chance of getting an officially supported grbl-LPC firmware for C3D?


---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2017 19:52*

[github.com - grbl-LPC](https://github.com/cprezzi/grbl-LPC/releases)


---
**David Cantrell** *June 05, 2017 20:00*

Thanks. I had not noticed the the second option lists support for C3D. Thats really cool.




---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2017 20:03*

You'll specifically want the last option.  Last I spoke with **+Claudio Prezzi** there's a different pin designation on the 4 Axis in regard to the Y endstop switch pin I think. 


---
**Cid Vilas** *October 05, 2017 19:21*

**+Dushyant Ahuja** How do you know where the 'spindle speed' or 'laser power' PWM pin is located?  I am unable to find informatino about the pin config.  I appreciate the help!




---
**Dushyant Ahuja** *October 05, 2017 20:41*

**+Cid Vilas** - I haven't used GRBL-LPC since I realised that it doesn't support 3D printing - so wouldn't be able to help you.




---
**Claudio Prezzi** *October 06, 2017 06:35*

**+Cid Vilas** The PWM pin is 2.5 as written in the release description.


---
**Cid Vilas** *October 07, 2017 04:26*

**+Claudio Prezzi**   Can ths pin be re-assigned?  I have a TTL laser so i need it to be a 5V output.  Is this possible as it is now?  Hopefully without recompiling?




---
**Claudio Prezzi** *October 07, 2017 10:38*

**+Cid Vilas** The PWM pin assignment options are very limited and needs to recompile the firmware. If you are on a smoothieboard you have the pin2.5 on Q6 (before the mosfet). If your board don't have that pin, you can easily invert the signal with a pnp transistor or p-channel mosfet (or just connect a cable to the gate leg of the 2.5 mosfet).


---
**Claudio Prezzi** *October 08, 2017 09:06*

**+Cid Vilas** What board do you have?

For a C3D mini I could complie a special version with PWM on pin 2.4 instead of P2.5, because P2.4 has a connector on the mini.


---
**Cid Vilas** *October 11, 2017 16:50*

**+Claudio Prezzi** I have the MKS SBASE. One of the Max end stop connectors would work or possibly use one of the extender ports. I don't know which pin would be compatible with PWM.

This is the module I intend to use:



[m.banggood.com - EleksMaker® LA03-3500 450nm 3.5W Blue Laser Module With TTL Modulation for DIY Laser Cutter Engraver Sale - Banggood Mobile](https://m.banggood.com/450nm-3500mW-3_5W-Blue-Laser-Module-With-TTL-Modulation-for-DIY-Laser-Cutter-Engraver-p-1103261.html)


---
**Claudio Prezzi** *October 12, 2017 10:38*

grbl-LPC works fine with the MKS Sbase. I use it in my K40. If you compile your own version, you can assign one of these pins for PWM out: P1.18, P1.20, P1.21, P1.23, P1.24, P1.26, P2.0, P2.1, P2.2, P2.3, P2.4, P2.5.

So you could use Y- pin for PWM, but would have to remap the Y endstop to the Y+ for example.


---
**Cid Vilas** *October 20, 2017 01:04*

**+Claudio Prezzi** I dont know how to recompile it.  Can you help?




---
**Claudio Prezzi** *October 20, 2017 07:00*

**+Cid Vilas** Just follow the <b>Build notes</b> on [github.com - cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)


---
**Cid Vilas** *October 21, 2017 17:45*

**+Claudio Prezzi**​, I cannot get it to compile. I'm also having trouble figuring out where to configure the pins for the MKS SBASE. I would appreciate the help. Thank you. 


---
**Claudio Prezzi** *October 22, 2017 07:30*

What error do you get if you execute make?

The make command must be executed in the top folder of the git respository.


---
**Claudio Prezzi** *October 22, 2017 07:38*

To use the MKS SBASE pinout you first need to disable (add // in front) the line "#define CPU_MAP_SHOOTHIEBOARD" and enable (remove //) the line "#define CPU_MAP_MKS_SBASE" in grbl/config.h. 


---
**Claudio Prezzi** *October 22, 2017 07:50*

If you just want to change the PWM pin to P2.4, you can enable the line "#define SPINDLE_PWM_PIN_2_4" in config.h.

Further changes of the pin mapping can be done in the MKS_SBASE section of grbl/cpu_map.h.


---
**Cid Vilas** *October 23, 2017 22:33*

**+Claudio Prezzi**  Currently im stuck here: 



                                               ^

grbl-lpc/current_control.cpp:67:47: note: suggested alternative:

In file included from grbl/grbl.h:34:0,

                 from grbl-lpc/current_control.cpp:24:

/usr/include/newlib/math.h:282:15: note:   'round'

 extern double round _PARAMS((double));

               ^

Makefile:139: recipe for target 'build/src/current_control.o' failed

make: <b>*</b> [build/src/current_control.o] Error 1






---
**Cid Vilas** *October 24, 2017 00:39*

**+Claudio Prezzi** I fixed the above by going into the current_control.ccp file and changing "..std::round(.." to just "..round(.."   The problem im hitting now is that the output of 1.26 is 2.782V when off, so the laser is constantly on, and i cant control the output.

I set "SPINDLE_PWM_USE_PRIMARY_PIN" to true

I also changed the pins so the endstops are working now, but the output of 1.26 isnt working.


---
**Claudio Prezzi** *October 24, 2017 07:46*

I don't know if 1.26 even can be used as output. If there is some sort of input transistor, diode or optocuppler, the connector pin can not be used as output. Did you also set SPINDLE_PWM_USE_SECONDARY_PIN false? Is something else still configured to use 1.26? It looks like 1.26 is still configured somewhere as input with pullup.



If you don't need the E0 stepper, you can try to use 2.3 instead. This pin is available at the pins before the E0 stepper driver.

Or you could use 1.23 (available at the additional pin header J8).


---
**Claudio Prezzi** *October 24, 2017 07:54*

See [google.ch - Bild: Monoprice Maker Select v2 MKS Sbase conversion - Album on Imgur](https://www.google.ch/imgres?imgurl=https://raw.githubusercontent.com/chris334/MKS-SBASE/Mks-Sbase-V1.2-English/Mks-Sbase%2520v1.2%2520Pin%2520Layout.jpg&imgrefurl=https://imgur.com/gallery/vSKQr&docid=545zRyP1YdvX5M&tbnid=51rFe3uGcxorvM:&vet=10ahUKEwiS-cyr4IjXAhXEJMAKHer2CvwQMwgmKAAwAA..i&w=1000&h=550&client=firefox-b&bih=771&biw=1536&q=mks+sbase+pinout&ved=0ahUKEwiS-cyr4IjXAhXEJMAKHer2CvwQMwgmKAAwAA&iact=mrc&uact=8)


---
**Cid Vilas** *October 24, 2017 22:15*

**+Claudio Prezzi** , The code that i see isn't easy to just assign specific pins, as far as i can see and understand.  Can you clear things up and assume i want to output the spindle speed to pin 1.23?


---
**Claudio Prezzi** *October 25, 2017 06:52*

To use pin 1.23 as the spindle PWM output you must set the following (in the CPU_MAP_MKS_SBASE section of cpu_map.h):

#define SPINDLE_PWM_CHANNEL PWM1_CH4

#define SPINDLE_PWM_USE_PRIMARY_PIN true

#define SPINDLE_PWM_USE_SECONDARY_PIN false



And make sure that #define CPU_MAP_MKS_SBASE is active in config.h (and no other CPU_MAP define).



Then in LW4, verify that M4 S0 is in the start gcode and M5 in the end gcode. M4 means that the PWM value will be adjusted to the speed at accelleration and decelleration and will be zero when not moving. G0 moves will also not fire the laser.


---
**Cid Vilas** *October 26, 2017 00:17*

**+Claudio Prezzi** awesome! I didn't understand that part, but it worked. Thank you so much for your help. I've got it all tested and configured.  O was able to successfully cut and raster.  Great work!


---
*Imported from [Google+](https://plus.google.com/+DushyantAhuja/posts/aM5g2r4ezWj) &mdash; content and formatting may not be reliable*
