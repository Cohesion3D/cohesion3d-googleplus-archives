---
layout: post
title: "Just a quick update. Board now mounted inside"
date: January 29, 2017 17:02
category: "General Discussion"
author: "Colin Rowe"
---
Just a quick update.

Board now mounted inside.

main power now split to drive the pump when switched on.

Screen added.

I have not connected a computer yet, all controls off the screen for now.

So. Power up and all looks ok, getting things on the screen.

Can jog about and change the step size.

I am sure that the Y axis is inverted and moving it Y- should bring it towards the front of the machine but it moves towards the back.

On setting it yo do a return to home it wants to move to the front left corner, not the back left.

is there a way to reverse the axis or have I put something in wrong??





**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 29, 2017 17:04*

Remove power. Flip the Y motor cable 180 degrees. 


---
**Colin Rowe** *January 29, 2017 17:09*

the 4 pin that goes into the board?






---
**Ray Kholodovsky (Cohesion3D)** *January 29, 2017 18:24*

No, the smaller cable that is going to where it says "Y". This is above the green motor driver board. 


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/WUsWLB2JDnZ) &mdash; content and formatting may not be reliable*
