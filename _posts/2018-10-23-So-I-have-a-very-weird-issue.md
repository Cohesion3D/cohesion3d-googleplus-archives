---
layout: post
title: "So I have a very weird issue"
date: October 23, 2018 06:08
category: "C3D Mini Support"
author: "Chad Traywick"
---
So I have a very weird issue. I have been running external stepper drivers on all axis for a few weeks now on my cohesion3d mini. All was good, I started having one driver that was loosing steps so I decided to upgrade all 3 of my driver to better ones. Now I have this almost double image during engraving. Nothing else was changed, shut the machine down, set the new driver settings and swapped them out.



Second pic is before the swap, first is after. (the jagged edge of the font in the first picture is supposed to be like that)



I did the usual, checked mirrors just in case, belts all the usual.



I use lightburn and even rotated the image and engrave in the y axis and does the same.



New drivers are DM542T's



Any suggestions?



![images/f425f5585fc2ed79ccc0e4206d8b12bb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f425f5585fc2ed79ccc0e4206d8b12bb.jpeg)
![images/419ef12de8e6030d0ddc2e7da4a20ee0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/419ef12de8e6030d0ddc2e7da4a20ee0.jpeg)

**"Chad Traywick"**

---


---
*Imported from [Google+](https://plus.google.com/101895536937391772080/posts/PsSAa9LEaPo) &mdash; content and formatting may not be reliable*
