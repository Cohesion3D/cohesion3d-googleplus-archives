---
layout: post
title: "Hi, I look for documentation of this board, bnot just drawings and a short \"how to\", but real docs"
date: November 29, 2017 07:51
category: "General Discussion"
author: "X Y"
---
Hi, I look for documentation of this board, bnot just drawings and a short "how to", but real docs. There is not much to find on the website, in fact what is there is pretty useless to me. 



I need docs about which command switches which output. 



Also, how are the FETs switched? The pinout diagram shows "-" and "+VMOSFET" Do they switch +VMOSFET to ground? Or are that highside switches?



Another one is the step/dir outputs, are they open collector? Or logic ttl 5V?



What are the maximum ratings vor "main power in" and what is driven by it?



What do the pin numbers mean (Po.22, P2.3 etc)? Are that the controller pins? Is there a schematic somewhere?



Sorry for the many questions, but I wonder how all you guys wire up your machines without knowing all this ;-) Maybe I missed a pdf showing all this.







**"X Y"**

---
---
**Andy Shilling** *November 29, 2017 09:49*

+Louis Schreyer, I'm sure Ray will come and answer your questions but dude I don't understand any of what your asking and I got mine working without a problem. I'm sure you'll be fine.



Just trust in the power of the C3D and the people already using it. 😉


---
**Dushyant Ahuja** *November 29, 2017 13:05*

The C3D boards are based on Smoothieware and have the same pin allocations as the Smoothieboard. You'll find a lot of documentation on the Smoothieware site. 


---
**Griffin Paquette** *November 29, 2017 14:20*

I can answer most of these: **+Ray Kholodovsky** I’m sure will come and clarify it all later. 



1) The commands are down to the gcode. You specify what pin will turn on and off in the config.txt file found on the SD card. These pins are in Px.xx format which is what the P0.22 and P2.3 correspond to. Essentially you can move around pin configurations in your configuration file. 



2) All mosfets are low side N-channel switching FETs. 



3) can’t help sorry



4) input max is 24V if I remember correctly. Uses an XL1509 switching regulator to step down to 5V. Ray can confirm here. 



There is also a github that has all design files if you want to check that out, as well as a full diagram showing all pinouts and functions which I have attached below. 



-Griff![images/dec0032368f2a83703d770753effa1fa.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dec0032368f2a83703d770753effa1fa.png)


---
**Ray Kholodovsky (Cohesion3D)** *November 30, 2017 02:58*

Hi Louis,



I see from your email that you want to build a Pick n Place Machine.  We have several people using our ReMix board for this application. 



Real docs: 



[https://cohesion3d.freshdesk.com/support/home](https://cohesion3d.freshdesk.com/support/home)



I've got mechanical drawings, CAD Models, pinout diagram, case designs... 



Here are all the design files for ReMix: [https://www.dropbox.com/sh/sp9kfoxc4ihlqrw/AAApjO8Mn0raxKPxMYFKgFWWa?dl=0](https://www.dropbox.com/sh/sp9kfoxc4ihlqrw/AAApjO8Mn0raxKPxMYFKgFWWa?dl=0)



As the guys said, it's all Smoothie, so you define your own commands (such as M3, M4, M5, etc...) to do exactly what you want. 



I recommend reading:

[http://smoothieware.org/](http://smoothieware.org/)

and in particular:

[http://smoothieware.org/switch](http://smoothieware.org/switch)



The board says max 24v in although it should be able to take up to 30v.  



Regarding question about stepper signals, these are 3.3v, direct from io pins of the microcontroller. 



Quite a few external drivers can take 3.3v fine, otherwise you can use 5v by switching the pin to open drain in the config file as shown here:

[https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



Best regards,

Ray

![images/ed6de6c9b5924b4cdde166224f20044f.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ed6de6c9b5924b4cdde166224f20044f.png)


---
**X Y** *December 03, 2017 14:49*

Thank you very much! All questions solved. Perfect.


---
*Imported from [Google+](https://plus.google.com/113608627901891383220/posts/S6hKx1nrHW2) &mdash; content and formatting may not be reliable*
