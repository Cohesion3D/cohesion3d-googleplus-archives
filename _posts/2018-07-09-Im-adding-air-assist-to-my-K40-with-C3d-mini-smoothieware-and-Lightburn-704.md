---
layout: post
title: "I'm adding air assist to my K40 with C3d mini, smoothieware, and Lightburn 7.0.4"
date: July 09, 2018 18:14
category: "C3D Mini Support"
author: "Robert Luken"
---
I'm adding air assist to my K40 with C3d mini, smoothieware, and Lightburn 7.0.4.  Lightburn has a switch to operate air assist, but it isn't clear if this will control an output on the mini and if so which one. Is there any info on setting this up?





**"Robert Luken"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 09, 2018 21:17*

Save and Check the output gCode file. I think it’s using M106 and M107. 



You should see a line such as M106 S255 at the top. 



The smoothie config already has a fan switch defined and you just need to decide what pin to use. 



What exactly do you need to control to run your air assist - relay, SSR?


---
**Robert Luken** *July 09, 2018 22:17*

**+Ray Kholodovsky** I'm going to use a 24V solenoid operated air valve. Plan on having the air pump running whenever the laser is powered and feeding air to the LO air assist nozzle through a needle valve to maintain a minimum air flow to protect the focus lens The solenoid air valve would bypass the needle valve and allow full air flow.



I assumed that the Bed or Extruder FET would be the best option (solenoid draws ~.25A). Just don't know how to configure the board/firmware.



Also can this still be done if I choose to go to the GRBL firmware?


---
**Robert Luken** *July 12, 2018 22:53*

I was able to set the fan control in the config file and the M106/M107 gcodes now control P2.6 (active low),  but I haven't been able to find any information on the FET and whether it can handle the 20W (0.8A) solenoid. I assume it will at least require some diode protection. I don't want to risk killing it. The board is not cheap. If it's the same device as used on the smoothieboard, it should be 3A. Does anyone have info on it?


---
**Ray Kholodovsky (Cohesion3D)** *July 13, 2018 15:05*

The large FET 2.7 (SEVEN!) can do 3 amps, sure. Not the smaller ones like 2.6 and 2.4.  You have correctly identified a reason that I recommend handling all larger and especially inductive loads separately from the board.  You can get an external mosfet and flyback diode. for cheap, some of those modules off aliexpress even have opto isolation from the control signal. 


---
**Chuck Comito** *July 14, 2018 21:45*

**+Robert Luken** Robert did this with a separate power supply and a module for watching the solenoid on and off. Search the smoothie group. I posted the config file and the schematic to how I did it. **+Don Kleinschnitz** helped me with it. 


---
*Imported from [Google+](https://plus.google.com/107953279340834579617/posts/DemzXCJsEsL) &mdash; content and formatting may not be reliable*
