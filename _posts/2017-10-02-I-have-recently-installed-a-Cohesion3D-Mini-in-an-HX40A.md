---
layout: post
title: "I have recently installed a Cohesion3D Mini in an HX40A"
date: October 02, 2017 16:21
category: "K40 and other Lasers"
author: "Patrick Keys"
---
I have recently installed a Cohesion3D Mini in an HX40A. I have a few questions, if I may:



- when executing a homing operation, the carriage initially homed to the front-right corner, which isn't correct. I turned the Y-axis servo cable upside down - now homes to back-right corner. Obviously I need to change the direction of the X axis servo cable - but this is connected via the white flexi cable. Is there a way to reverse this axis on the 3D mini, or do I need to change the connections at the far end of the flexi?



- I'm a little unclear as to whether the PWM operation takes place via the 'L' output on the 4-way header, or whether I need a second connection to the laser PSU. The documentation doesn't mention a second connection and also talks about using the current limit pot, but the connection diagram and config file both talk about a separate connection. Which should I be using, and how should it be connected?



I've attached some before and after pictures... note that I'm intending to replace the laser PSU with the enclosed version (also photographed) - I'm not a huge fan of open-frame high voltage power supplies. On the old laser PSU, the headers are (L to R): potentiometer, interlock (including laser on button etc.), 'L' connection from PSU board, test button and mains input.



Also, the connections between the PSU board and Cohesion3D board are as follows:



L -> L

24V -> 24V

GND -> GND



No other pins are connected.



Thanks in advance for any pointers...



Photos:



1) Current laser PSU connections

2) New laser PSU

3) Cohesion3D Mini installed

4) Old controller board

5) PSU board connections (5V, VGND, 24V, GND, 5V, L top to bottom)





![images/fcc99212ee8971549f629f2de890c011.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fcc99212ee8971549f629f2de890c011.jpeg)
![images/8a2edbac82257f27aafc300fcf5d0b24.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8a2edbac82257f27aafc300fcf5d0b24.jpeg)
![images/5eb0972cfff2edf5a2b8920c3e07b0c7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5eb0972cfff2edf5a2b8920c3e07b0c7.jpeg)
![images/97fe6373ef76dccc794c81d130d4c590.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/97fe6373ef76dccc794c81d130d4c590.jpeg)
![images/a9e164209e750d1fdb5cc79ab99487b3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a9e164209e750d1fdb5cc79ab99487b3.jpeg)

**"Patrick Keys"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2017 18:00*

Welcome Patrick!  



I have not heard of this model # before, do you have a link from where you purchased this machine so I may take a look?  

Is this an older machine?  I have not seen an open air psu in quite some time. 



The default setup in the bundle is for the K40 laser which has a Y endstop in the rear of the machine.  Is this the case for you? 

It is set up to home to max.  When you send a G28.2 home command it should move to the rear left, touch switches, and think it is at 0,200 coordinates.  If your machine is different, please advise and we can make this change in config.txt.  The front left corner should be treated as 0,0. 



The board will pulse L for pwm, you will set the "max" on the pot - not the full amount but 10mA would be a good start, and then the % power in software will be a proportion of that 10mA max. 



Since this machine is different, let me confirm that you do need only 24v, gnd, and L from the laser psu to the board, they can go either to the large white connector as you have it (assuming to the correct pins per our pinout diagram on the docs site) or to screw terminals on the top left corner for 24v and Gnd, and the 4th from the left of the bottom screw terminals (P2.5 -) for L. 






---
**Patrick Keys** *October 02, 2017 18:45*

Thanks for such a speedy reply! It is quite an old model I believe - I ended up with it (them - there are two!) because a friend was getting rid of them, so unfortunately no link to the place that sold them. I have included some additional photos below... it looks like a beige version of the current K40s, but with all of the keyswitch and interlock stuff that is required for sale in the EU.



Anyway, I've had a good feel around inside - it looks like the two limit switches are mounted at the right-hand end of the crossbeam, and at the front right corner of the bed. Assuming they switch to ground, then I can hear them engaging, but no change on a meter. I guess I'll need to disassemble the bed to find out what's wrong.



But it definitely looks like it wants to home to the front-right corner. It just makes quite a racket when it gets there without limit switches in place!



As far as the laser PSU connections go - that makes perfect sense - thanks. I'm happy with the new PSU wiring, and since I have a spare tube, I might as well swap both of them.





![images/4e4ffb16bd2a3d3410d0c0a63113f0f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4e4ffb16bd2a3d3410d0c0a63113f0f0.jpeg)


---
**Patrick Keys** *October 02, 2017 18:52*

Ah... so it has optical end stops... that explains why I can't see any continuity change with the power off!


---
**Patrick Keys** *October 02, 2017 19:21*

So some definite progress: 'home all' sends it to rear-right, which indicates coordinates of 0, 200. Sending it to XY Zero moves it to front-right.



The laser tube definitely does work in test mode... but alas no control from the Cohesion board :-(


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2017 19:25*

If you are telling me that your endstops are located at the right for X and at front for Y then we need to adjust the config.txt to make x (alpha) homing to max and y (beta) to min. Please figure out exactly how long the X axis is/ can travel and set this as the alpha max value. 



You need to configure the 2 test fire values in LaserWeb settings before the fire button will work. Try sending a G1 X10 S0.6 F600 line after you have jogged to a safe location (ie the middle or origin) and set zero there. 


---
**Patrick Keys** *October 02, 2017 20:52*

Thanks so much for your help so far... I've got the motion and homing figured out now. I had to do a bit more than measure and change the alpha axis to home_to_max - I also had to invert the direction signal, and swap over the min and max endstop inputs. So it now homes to rear-right, and when 'Go to Zero' is pressed, it goes to front-left. This is good.



No luck on the laser control yet, though. I'm 99% sure it's a wiring issue - it looks like there's an opto-isolator on the PSU board which is driven by the L signal, and another one on the laser PSU board. I'm wondering whether there's a separate GND for the opto, or perhaps a 5V that needs connecting through?



A question: does the 3D mini switch the L input to ground, or does it raise it to 5V? I'm going to have a look in more detail tomorrow when I'm a bit less tired, but have a hunch that it'll be something fundamental like a missing ground. Thanks once again for your help!


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2017 21:00*

The left mosfet on the Mini is what switches L, it connects it to Gnd to turn it on. A quick test would be to connect your L wire to Gnd on the Mini to check whether the laser fires.  You can also give 12/24v in to the first terminal on the bottom for the mosfet power in, then the red led will turn on when you fire the laser (showing that the fet turns on). 

As you said, the Mini needs to have L and the Gnd of the LPSU. 


---
**Patrick Keys** *October 03, 2017 16:52*

OK, so it also requires VGND to be commoned with GND. Once I did this, I was able to use the Laser Test function in Laserweb4 to fire a test pulse. This works, and the laser was activated.



However, if I try to run a simple G Code file (generated in Laserweb), the laser never comes on. The generated file has the following commands:



G21         ; Set units to mm

G90         ; Absolute positioning

M3           ; Turn on laser



G0 X156.56 Y98.75

G1 X156.62 Y98.76 S100.00 F1000

G1 X156.68 Y98.76



...



G1 X173.26 Y100.12

M5          ; Switch tool off

M2          ; End



This all looks correct, but no luck with the laser. What's different between the laser test commands, and the generated ones? I don't seem to be able to see any G Code command window, so I can't tell for sure what is being sent to the 3D mini. Any ideas?


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2017 16:55*

Your S value should be between 0 and 1.  I see there an S value of 100.  Do you have a S value maximum in the settings set as 100?  This should be set as 1. 



Does the laser fire when you manually send the G1 X10 S0.6 F600 I mentioned earlier? 


---
**Patrick Keys** *October 03, 2017 16:59*

Awesome - that was it. The S value was way too high as you said. Many thanks for noticing so quickly!


---
*Imported from [Google+](https://plus.google.com/108403577695975026405/posts/52xTDYZB9gj) &mdash; content and formatting may not be reliable*
