---
layout: post
title: "Hi peoples just starting the wire up and a bit stumped I am adding an external power supply but thought it may be better to get everything hooked up and running standard before I start playing around adding the power supply"
date: October 03, 2018 22:48
category: "C3D Mini Support"
author: "Sean Willmott"
---
Hi peoples just starting the wire up and a bit stumped I am adding an external power supply but thought it may be better to get everything hooked up and running standard before I start playing around adding the power supply and driver etc.



The first pick seems standard Axis and Endstops hooked up hopefully that's right?



Second is where I am a little stumped the chinesium board has a 6 pin header C3D=4 not sure on LO and 5V



GND = Not Connected

LO = Green Wire

5v= Blue Wire

Fv= Not Connected

24v= Blue Wire

GND=Blue Wire



Hopefully the following makes sense



![images/2a9edc321f5fac27158395c1d55f281d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2a9edc321f5fac27158395c1d55f281d.jpeg)
![images/14271c0c66dcaaa7f8183fc7c562d7a4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/14271c0c66dcaaa7f8183fc7c562d7a4.png)

**"Sean Willmott"**

---
---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 23:11*

This may help: [https://www.lasergods.com/lihuiyu-6-pin-power-pinouts/](https://www.lasergods.com/lihuiyu-6-pin-power-pinouts/)

[lasergods.com - Lihuiyu 6 Pin Power Pinouts](https://www.lasergods.com/lihuiyu-6-pin-power-pinouts/)


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 23:12*

And yes. Get the basic install working first then add the external supply. I have docs for that too


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 23:27*

There are screw terminals for 24v gnd and laser so the 6 pin connector can be discarded


---
**Sean Willmott** *October 03, 2018 23:43*

Yes found those TB, I think  I have worked out the L wire but not sure on the 5v wire is that required cant find anything referencing that ?


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2018 23:44*

Don't hook up 5v. 

Adjust the heatsinks so they are not touching the pins on the drivers. 


---
**Tech Bravo (Tech BravoTN)** *October 03, 2018 23:45*

5v wire?


---
**Sean Willmott** *October 04, 2018 02:28*

**+Tech Bravo** Pin 3


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 02:35*

Yeah... ^^what ray said^^ :)


---
**Sean Willmott** *October 04, 2018 04:46*

Up and running Ray and TB homing without an issue just need to work out why LB and my axis are all over the place lol


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 05:18*

check this: [lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)




---
**Sean Willmott** *October 04, 2018 06:13*

**+Tech Bravo** Yeah na still have an issue ill have another look later after I get the next job out on my other machine.



I dislike that setup through my 900x600 is 0,0 Top left, working fine for 4 years is there a way to do the same in Smoothie?


---
**Tech Bravo (Tech BravoTN)** *October 04, 2018 06:17*

If you had an m2 nano in the machine then 0,0 (origin) was front left. The endstops are at the home position rear left or 0,600. At least thats how every other Chinese machine with a nano is. Home can be defined whereever you like it. Rear left is just standard because its out of the way there


---
**Sean Willmott** *October 04, 2018 06:38*

**+Tech Bravo** it was a Lihuiyu Studio Labs Card 🤔


---
**Sean Willmott** *October 04, 2018 06:39*

![images/3f97f691eaa39782d3c7b6b5ec0a97fc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f97f691eaa39782d3c7b6b5ec0a97fc.jpeg)


---
**Sean Willmott** *October 04, 2018 07:19*

Hi My Name is Sean 



Hiiiii Sean...



I have a Problem. 



What's the Problem Sean



I over think stuff 



How do you fix that Sean



You Check the simple stuff first



What did you forget Sean



The bloody job origin!



Yes I need a drink lol








---
**Sean Willmott** *October 04, 2018 07:19*

All good peoples.


---
*Imported from [Google+](https://plus.google.com/118103470310096610318/posts/L2HQ6KVgoc8) &mdash; content and formatting may not be reliable*
