---
layout: post
title: "I have some travel to attend to this week and will be mostly out of internet range"
date: August 13, 2017 12:26
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
I have some travel to attend to this week and will be mostly out of internet range. 



I leave you all in the hands of capable group moderators **+Ashley M. Kirchner** and **+Griffin Paquette**. Thank you all for your continued support and the camaraderie in helping each other out. 



-Ray





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Griffin Paquette** *August 13, 2017 14:14*

Anyone needs me/help just tag me!


---
**Ashley M. Kirchner [Norym]** *August 13, 2017 20:17*

Same here, let's play a game of tag.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/N9y8nsjp6BZ) &mdash; content and formatting may not be reliable*
