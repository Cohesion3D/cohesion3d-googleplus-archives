---
layout: post
title: "Thank you, received a Cohesion mini from you"
date: October 06, 2017 11:52
category: "C3D Mini Support"
author: "Vasily Ivanov"
---
Thank you, received a Cohesion mini  from you. A couple of questions, if you please. 1. Memory card with what characteristics do you recommend? 2. If I understand correctly, this board can be powered from a Laser PSU? 3.  Why, when using the Z axis, do you recommend using an external driver, such as a TB6600 although the bed for the driver plate on Polulu 4498, Polulu 8825 card is on the board?





**"Vasily Ivanov"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 07, 2017 05:11*

I assume you're talking about k40 here? 

MicroSD card between 1-32gb should be good, we provide 2gb, 4 or 8gb is fine. 

Format it fat32. 

Because the a4988 drivers we provide usually cannot provide enough current for things the LightObject z table and rotary. Tb6600 definitely can. Drv8825, maybe, depends on the exact table you will use. Lightobject ones are most common and require a lot of power. You will have to add a fan to cool the 8825. I am not a fan of this driver. Trinamic is better, like TMC2208. 



The K40 psu is barely powerful enough to run the XY motors for the head. If you want to move fast or add additional peripherals you should get a separate 24v psu, 6a should be good, for powering the board and peripherals. 


---
**Vasily Ivanov** *October 07, 2017 06:08*

Thank you for your detailed response. Many thanks for your product. Before some of the answers came himself, some you explained. Thank you.




---
**Vasily Ivanov** *October 11, 2017 14:24*

A few new questions, if you will allow ... I make my machine, do not upgrade K40. Its cutting area is  900x600 mm. I need to set this cutting area in both the config file and the Laserweb settings. Right? Or the residual only in laserweb? How to control the laser, if my laser is switched emission on with a low logic level? Which switch-on circuit should be used If I want to use a PWM output of  Cohesion mini?


---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2017 14:26*

Do you have homing switches? Where are they located. The k40 config is set for X home min Y home max. 



Picture how to connect: power the board with 24v and Gnd. Connect "L" from psu to terminal #4 on bottom. [https://plus.google.com/photos/.](https://plus.google.com/photos/.)..


---
**Vasily Ivanov** *October 11, 2017 14:53*

Can't see your image


---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2017 14:55*

It is the same as Jody's last comment here:  [plus.google.com - Let me start with a big thanks for creating this board! It has renewed my in...](https://plus.google.com/107797353699813204127/posts/JhLrU2nwoaE)


---
**Vasily Ivanov** *October 11, 2017 15:00*

Where are the end-points traditionally placed? If you look at the laser cutter from the front, then I plan to place it's in the upper left corner. This is X min and Y max? This also needs to be specified in the config file?




---
**Ray Kholodovsky (Cohesion3D)** *October 11, 2017 15:03*

Yes. The k40 has endstop in left and in rear. This is where the head will go to home. However the origin is the front left, this is the proper 0,0. So you need to configure the Y travel in config as beta max. 



Example: k40 Y is 200mm. When it touches switches it reports it is at 0,200. When I say go to 0,0 it moves to the front. 


---
*Imported from [Google+](https://plus.google.com/109550403531973425921/posts/7Z8snDDL8sh) &mdash; content and formatting may not be reliable*
