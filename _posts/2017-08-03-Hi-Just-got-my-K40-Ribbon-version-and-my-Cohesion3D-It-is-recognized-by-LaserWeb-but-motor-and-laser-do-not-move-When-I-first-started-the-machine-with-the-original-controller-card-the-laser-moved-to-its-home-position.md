---
layout: post
title: "Hi, Just got my K40 (Ribbon version) and my Cohesion3D It is recognized by LaserWeb, but motor and laser do not move When I first started the machine with the original controller card, the laser moved to its home position"
date: August 03, 2017 20:50
category: "K40 and other Lasers"
author: "Fabien Taylor"
---
Hi, 



Just got my K40 (Ribbon version) and my Cohesion3D

It is recognized by LaserWeb, but motor and laser do not move



When I first started the machine with the original controller card, the laser moved to its home position



This does not happen with the cohesion3D( I moved the laser prior)



Here is a photo on my installation, thanks for your help

![images/5861e74b9a9dc302ece8f30f1c320be4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5861e74b9a9dc302ece8f30f1c320be4.jpeg)



**"Fabien Taylor"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 03, 2017 20:55*

It will not home on startup unless you explicitly configure it to.  Have you tried actually jogging in LW or sending G28.2 homing command?


---
**John Austin** *August 03, 2017 21:42*

Following 


---
**Fabien Taylor** *August 03, 2017 23:17*

ok, plugged the card back and everythings works, just that the machine is now noisy when moving


---
*Imported from [Google+](https://plus.google.com/108052364627466740668/posts/1v6k1nnVZsG) &mdash; content and formatting may not be reliable*
