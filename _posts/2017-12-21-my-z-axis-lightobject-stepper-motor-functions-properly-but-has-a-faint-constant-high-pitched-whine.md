---
layout: post
title: "my z axis (lightobject) stepper motor functions properly but has a faint constant high pitched whine"
date: December 21, 2017 02:26
category: "C3D Mini Support"
author: "Tech Bravo (Tech BravoTN)"
---
my z axis (lightobject) stepper motor functions properly but has a faint constant high pitched whine. i am assuming i have the polarity wrong. i researched the nema 17 motor on lightobject for the pinouts and  the c3d k40 kit z axis installation documentation and everything looks okay. i am driving the z axis with an external 24vdc psu and tb6600 driver. any ideas?





**"Tech Bravo (Tech BravoTN)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2017 04:13*

Might just be the current on the TB set too high. Or it might just be the driver itself. We've had a lot of bad ones. Lots of subjective stuff there. 


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 04:32*

**+Ray Kholodovsky** well, i will check everything again. i think if i had a pole and/or polarity issue it would be symptomatic of such so.........i think i will monitor it closely and let er roll! Thanks :)


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 04:36*

good news is my z axis has been calibrated at 9 points on the bed against  the head, a ramp test is complete, focal plane is known, focus gauge has been cut, and i'm off and running (and finally reading some directions for once, or rather, watching the entire series of lightburn videos and actually following along)!


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/SAbPu1dv1Mg) &mdash; content and formatting may not be reliable*
