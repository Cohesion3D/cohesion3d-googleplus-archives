---
layout: post
title: "Hi! I have one of those K40 with a psu without pwm connector"
date: April 09, 2017 17:46
category: "K40 and other Lasers"
author: "Georg Petritsch"
---
Hi!



I have one of those K40 with a psu without pwm connector. Please correct me if i am wrong about this, but i guess to have nice engravings i have to let laserweb control the output power instead of setting the power on the potentiometer. Or to say laserweb "cut this with 40% and that with 60%"... This is, so i assume, done with the pwm cable i got with my cohesion board. 



Do i have to remove the potentiometer and replace it with the cable, or is there any other way to connect the pwm somewhere to the psu?



So if there is a shematic available to do this or at least someone who can put me in the right direction, i would really appreciate that.



Thanks and all the best, Georg





**"Georg Petritsch"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 17:51*

Well, we can replace the pot with pwm cable for sure. This is not the concern. 



But the default wiring will pulse L which is already wired. . And if you are not seeing results there we should tune that first. 



Set the pot for 10mA. Try running G1 X10 S0.4 F600 lines with different X value and S value (0-1). You should see different burn levels but also see different values on the mA meter. 


---
**Georg Petritsch** *April 09, 2017 18:08*

Ok, there is a difference! So i have somewhere a configuration error, and i blamed the cable. shame on me :) So it makes no sense to replace the pot with the cable?

But that helped a lot, thanks! Now i know where to start searching. 


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 18:10*

Do not replace the pot. Set pwm cable aside for now. 



Try stuff in LaserWeb. 



Perhaps after some testing you can try setting the pwm period in the config.txt from 200 to 400 and see if you get better grayscale response. 


---
**Georg Petritsch** *April 09, 2017 18:15*

Thanks a lot Ray, will do :)


---
**Tony Sobczak** *April 18, 2017 19:49*

**+Ray Kholodovsky** Is there anyone that has had good success with gray scale?  I mean actually see difference in the percentages. Any examples available?

 


---
**Ray Kholodovsky (Cohesion3D)** *April 18, 2017 19:56*

**+Tony Sobczak** plenty.  But it does take some tuning as every k40 is different.   The process I described above pretty much sums up what it takes, playing with the PWM period, trying some different pot values like 5mA and 10mA, etc... Then when you run the G1 lines with the different S values you should see different output on the mA gauge if you have one.   

Ultimately it is a function of each individual machine. 


---
*Imported from [Google+](https://plus.google.com/112258464907238301214/posts/W2e5kRwSoav) &mdash; content and formatting may not be reliable*
