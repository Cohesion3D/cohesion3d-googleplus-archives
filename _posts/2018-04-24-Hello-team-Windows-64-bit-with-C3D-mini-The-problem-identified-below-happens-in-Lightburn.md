---
layout: post
title: "Hello team, Windows 64 bit with C3D mini The problem identified below happens in Lightburn"
date: April 24, 2018 09:30
category: "FirmWare and Config."
author: "E Caswell"
---
Hello team,

Windows 64 bit with C3D mini

The problem identified below happens in Lightburn.

I have been testing Grbl-1.1 as I have a couple of larger raster jobs looming and wanted to use Grbl for its raster capabilities.



I have an issues when running Grbl (Smoothie runs perfectly.)



When I run a job the positioning of the job isn't starting in the correct X/Y position as referenced on the cam.



See pics attached of what set up is with LB and the issues experienced.



I not sure if its a LB or firmware issue so if anyone has or knows of a solution then please give me some direction or solutions.



Already tried moving from absolute coords and current position to the user origin but no change.



With the Testing file (seen in the pic) it starts with it centered at around 33mm when its actually supposed to be centered at 90 on screen.



I have tested this on Smoothie and works perfectly.



So I presume its either the Grbl firmware or its LB settings. any help appreciated please. 



PS..set up and tested with Cohesion C3D mini, Grbl and Grbl 1.1 and its still the same.



Tried it with Origin top left, bottom left and it makes no difference as to the positioning.



Tried the ?? in console and it doesn't look like there e is an off set.



Really am at a block here and can't get it working, any help would be appreciated. 



**+Jim Fong** ? ? 



![images/0e26f1d7ba740164d067ea1bedb87576.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0e26f1d7ba740164d067ea1bedb87576.jpeg)
![images/9591abf4559d18e80996830de74420b4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9591abf4559d18e80996830de74420b4.jpeg)
![images/640f4a49de4f14dcdbbba9bcd5f07df2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/640f4a49de4f14dcdbbba9bcd5f07df2.jpeg)
![images/91d05c0ce56ff04a2d83fdc040d68448.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/91d05c0ce56ff04a2d83fdc040d68448.jpeg)
![images/89853f662ec090bc1f5e8cb2d32a0bb9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/89853f662ec090bc1f5e8cb2d32a0bb9.jpeg)
![images/583f045b9a11d22f45c9a9083a28e0d8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/583f045b9a11d22f45c9a9083a28e0d8.jpeg)
![images/44c22fed1c9b4cca1a1911a1d4d7f19d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/44c22fed1c9b4cca1a1911a1d4d7f19d.jpeg)

**"E Caswell"**

---
---
**Jim Fong** *April 24, 2018 10:51*

I only use run from current position so haven’t seen the problem.   Take a look at the gcode from smoothieware vs grbl and see if there is a difference.  If it is the same then there is a issue within grbl. 


---
**E Caswell** *April 24, 2018 11:31*

Tried from current position Jim but that was the same as well. will look through the G code later cheers




---
**E Caswell** *April 25, 2018 19:59*

**+Jim Fong** Rather than bogging the other thread that Josh has raised Jim, links to the files that I have shared on google drive under grbl testing



The G code is different for GlX95 between Grbl and smoothie. Not sure what that means at the moment so you may be able to help there..





[drive.google.com - grbl testing.lbrn](https://drive.google.com/file/d/1JxFWYcWYfAOIfkK2k_fPLdAirySgZv_Z/view?usp=sharing+https://drive.google.com/file/d/1AG41pr5rJ6Oifu4p9uKDaBzVppB9OjXO/view?usp%3Dsharing+https://drive.google.com/file/d/1G3wSza__JL5nKpxR7Qv-FUYBBkZnmVtS/view?usp%3Dsharing)


---
**Jim Fong** *April 25, 2018 20:19*

**+PopsE Cas** ok will take a look


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/GQZFMUFoTGg) &mdash; content and formatting may not be reliable*
