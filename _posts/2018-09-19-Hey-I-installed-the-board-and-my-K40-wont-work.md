---
layout: post
title: "Hey I installed the board and my K40 wont work"
date: September 19, 2018 14:48
category: "K40 and other Lasers"
author: "darkin brown"
---
Hey I installed the board and my K40 won’t work.

The “arms” are not recognizing the Y axis.

The laser fires and the X arm moves but that’s it.

I included some photos of the wiring



![images/dd37a8b7478c602c242f2587194bfc83.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dd37a8b7478c602c242f2587194bfc83.jpeg)
![images/0e2e683349bb07eb341db6361fc83664.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0e2e683349bb07eb341db6361fc83664.jpeg)

**"darkin brown"**

---
---
**Tech Bravo (Tech BravoTN)** *September 19, 2018 14:53*

Looks like the y stepper motor is not connected


---
**Joe Alexander** *September 19, 2018 14:55*

Shouldn't there be 4 wirees from the Y stepper motor to the 4 pins on the Y axis chip on the C3D? is that what is plugged into the port next to the ribbon cable instead? I believe that is for the pot and such.


---
**Tech Bravo (Tech BravoTN)** *September 19, 2018 14:57*

Yep. Y is plugged in to the wrong header


---
**Ray Kholodovsky (Cohesion3D)** *September 19, 2018 15:05*

It looks like the Y motor is plugged into the endstop header. 


---
**Ray Kholodovsky (Cohesion3D)** *September 19, 2018 15:09*

Also, insulate your board from the frame. Plastic standoffs, 3D printed holder, or acrylic plate. 


---
**darkin brown** *September 28, 2018 13:49*

**+Ray Kholodovsky** Thanks, is there a diagram that i could follow? the one that i am using is not giving me correct information. Thanks again, I have standoffs.




---
**Ray Kholodovsky (Cohesion3D)** *September 28, 2018 14:01*

Follow the instructions at [cohesion3d.com - Getting Started](http://cohesion3d.com/start) 


---
*Imported from [Google+](https://plus.google.com/108492051424989759588/posts/b85AKW32ubu) &mdash; content and formatting may not be reliable*
