---
layout: post
title: "Hi Ray Kholodovsky - I have an Ultimaker Original+ with original board"
date: March 02, 2018 23:44
category: "3D Printers"
author: "Richard Vowles"
---
Hi **+Ray Kholodovsky** - I have an Ultimaker Original+ with original board. Is it worthwhile to replace it with a Cohesion3d board? 

![images/739ad2e92775f336eb2b7f1e250ac2fe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/739ad2e92775f336eb2b7f1e250ac2fe.jpeg)



**"Richard Vowles"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 02, 2018 23:48*

I was a really good computer salesman, I firmly believed that the best computer for a customer was the one they already had that still worked :)

So, does it work? Are you hitting roadblocks with the current system? 


---
**Richard Vowles** *March 02, 2018 23:50*

Ish - its not very accurate and not very fast, but it "works". I suppose I am asking is will the Cohesion3d add anything to it or is the mechanism in the Ultimaker just too inaccurate to benefit?


---
**Ray Kholodovsky (Cohesion3D)** *March 02, 2018 23:53*

How many extruders?



We can go faster and quieter with one of our boards and Trinamic drivers, possibly add a screen for headless operation to the mix... 


---
**Richard Vowles** *March 03, 2018 00:12*

Just the one extruder, thermistor as well I'm pretty sure. 


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 00:14*

Mini with Trinamic drivers would be nice for you :) 

Actually you should look into that because UM's now use thermocouples which would be problematic. 


---
**Richard Vowles** *March 03, 2018 00:17*

Ok, I'll check and get back to you


---
**Richard Vowles** *March 03, 2018 03:08*

Ok, it seems to be something called a PT100 B sensor - which I presume is going to cause problems.


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 03:10*

Ok, pt100 is different in other ways. [http://smoothieware.org/pt100](http://smoothieware.org/pt100)


---
**Richard Vowles** *March 03, 2018 03:13*

I need to order some of those MAX31865 for thermo-couples as well anyway, so I'll put it on my list before i order the Mini - thanks!


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 03:16*

I read that as the MAX is not currently supported, only the E3D amp board is.  



So I personally enjoy putting new heads and boards in machines, but whether you want to go down that route and replace the hotend is up to you.. 


---
**Richard Vowles** *March 03, 2018 03:18*

Ah, ok -right, wrong end of the stick then. I'll give it up then! 


---
**Alex Hayden** *March 03, 2018 05:00*

**+Richard Vowles** does this mean you are not going to fix it? 


---
**Richard Vowles** *March 03, 2018 05:04*

Indeed, I'll leave it as it is...


---
**Alex Hayden** *March 03, 2018 05:12*

Will you still use it? Would you consider a complete rebuild? I mean you already have most of the hardware, 


---
**Richard Vowles** *March 03, 2018 05:14*

I don't have that kind of time. It would be cheaper to buy a Prusa or similar. I'll just leave it for the stuff where accuracy and speed and reliability aren't important 😁


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2018 05:15*

I like upgrading Prusas. I just got Di3 #4 :) 


---
**Remco Schoeman** *March 03, 2018 08:34*

If your early ultimaker is not very fast and not very accurate, I have 2 questions for you:

1) do you have the original (32 mm 1.3 inch high and underpowered) motors of the very early ultimakers on the x and y?

2) have you added belt tensioner springs to all 4 long x and y belts?



I have replaced the x and y motors with 48 mm high ones and added belt tensioner springs, and I can print accurately and fast with 150mm/s and rapids of 200mm/s


---
**Remco Schoeman** *March 03, 2018 08:39*

I've put your picture under a microscope and can't see a belt tensioner on the long belt, add one like this on every long belt, and your accuracy will increase significantly, I promise! ;-) also, you can see the bigger motor is sticking out quite a bit. [https://lh3.googleusercontent.com/iyYP8BxZ4gDo9WPPS8MrNpJC2BeJD2HyuTtAjMmZBh1bs8uSq2qgOjb9efJV2Mjhj4Ntjuirr2M](https://lh3.googleusercontent.com/iyYP8BxZ4gDo9WPPS8MrNpJC2BeJD2HyuTtAjMmZBh1bs8uSq2qgOjb9efJV2Mjhj4Ntjuirr2M)


---
**Richard Vowles** *March 03, 2018 08:53*

Awesome! And they are cheap! Thanks for the tip.


---
*Imported from [Google+](https://plus.google.com/+RichardVowles/posts/KpxMq2aPG1s) &mdash; content and formatting may not be reliable*
