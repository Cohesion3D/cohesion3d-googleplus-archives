---
layout: post
title: "Just got the c3d installed and have the motors running"
date: May 01, 2018 16:40
category: "General Discussion"
author: "james blake"
---
Just got the c3d installed and have the motors running.  I have not tried the end stops yet and I have to get it to find home (it just jogs it around or grinds back center of machine).  When I check position it says machine is sitting at 0x and 210y and if I move head to bottom of machine it says it is at 420y but I can try to figure these out latter.  The power is working and it runs cuts and image, so I am happy so far but looks like a LOT more playing to get it honed in.  Thanks to every one who helped get me this far.





**"james blake"**

---


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/f9mAxcrSzSu) &mdash; content and formatting may not be reliable*
