---
layout: post
title: "It's been quiet around the LAYZOR (600 K40 laser cutter rebuild) but I've been working hard on making the custom part kits available"
date: November 26, 2018 14:30
category: "K40 and other Lasers"
author: "Frederik Deryckere"
---
It's been quiet around the LAYZOR (€600 K40 laser cutter rebuild) but I've been working hard on making the custom part kits available. The first limited batch has already been sold out, now the second batch is publicly available for pre-order. Shipping is expected by mid-december. So for those who were hesitant about getting access to the custom parts: this is it!



Please visit the LAYZOR website for more details:

[https://manmademayhem.com/layzor/product/layzor-custom-part-kit/](https://manmademayhem.com/layzor/product/layzor-custom-part-kit/)



On my blog there are also some articles about making these kits:

[https://www.manmademayhem.com](https://www.manmademayhem.com)



Thanks for watching!





**"Frederik Deryckere"**

---


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/Zta98eJzXnr) &mdash; content and formatting may not be reliable*
