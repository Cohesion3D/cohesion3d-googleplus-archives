---
layout: post
title: "A Z table designed by a fellow Cohesion3D Mini Laser user..."
date: January 25, 2017 21:13
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
A Z table designed by a fellow Cohesion3D Mini Laser user... 

Awesome!

[https://hackaday.io/post/51315](https://hackaday.io/post/51315)





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Anthony Bolgar** *January 25, 2017 21:15*

Nice eh?




---
**Ray Kholodovsky (Cohesion3D)** *January 25, 2017 21:16*

Yeah, thanks for posting that Anthony.


---
**Anthony Bolgar** *January 25, 2017 21:17*

:)


---
**Anthony Bolgar** *January 25, 2017 21:18*

I am going to give this one a try for my newest K40 in the farm (Well 2 K40's and an LE400)


---
**Alex Krause** *January 25, 2017 21:27*

Like the belt tensioner :P


---
**Kris Sturgess** *January 26, 2017 00:06*

Nice!


---
**Ryan Branch** *January 27, 2017 09:27*

Thanks! Glad you like it! It could use a better belt tensioner. I have noticed when using the z axis that the belts can still slip on the pulleys causing the bed to become un-tram a little. It hasn't been a huge problem though, so I haven't bothered modifying it.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/6G5qcBsDiz1) &mdash; content and formatting may not be reliable*
