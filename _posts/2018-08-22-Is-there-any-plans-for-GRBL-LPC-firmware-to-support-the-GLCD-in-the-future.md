---
layout: post
title: "Is there any plans for GRBL-LPC firmware to support the GLCD in the future?"
date: August 22, 2018 01:46
category: "K40 and other Lasers"
author: "Bill Keeter"
---
Is there any plans for GRBL-LPC firmware to support the GLCD in the future? I run my custom 60w laser in the garage. While my Mac is in my office. So i'm loading a SD Card with my gcode files and running directly on the machine with the GLCD. 



I'm fine with Smoothie except with engraving speeds. My machine can handle much faster than the 200mm/s max. With LightBurn i'm getting this constant speed up and speed down as I engrave. As it engraves fast but then slows down over blank area (assuming smoothie is forcing the slower speed)

![images/a1d54a963a0a70a5de48e8101790775c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a1d54a963a0a70a5de48e8101790775c.jpeg)



**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 01:58*

We’ve tried improving the raster speed in smoothie and also tried adding the screen to grbl. Both have proved very difficult. 



Perhaps you could try grbl-lpc connected to a raspberry pi running octoprint? **+Jim Fong** gave this a shot and the results were good. 

Or have a computer next to the laser for sending the job - you can run LB on multiple machines. 


---
**Bill Keeter** *August 22, 2018 12:54*

**+Ray Kholodovsky** While not ideal, i'll look into those 2 options. As I have a bunch of RPi's sitting around... or i'll buy a cheap laptop of craigslist.



Will my existing config file on the SD card work with GRBL-LPC? As I had to customize a lot of the setting to support the larger machine with bigger stepper motors and external drivers.




---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 14:15*

No, GRBL is configured using the $$ commands in console.

[github.com - grbl](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration)


---
**Ray Kholodovsky (Cohesion3D)** *August 22, 2018 14:16*

And if you had to do the !o thingy in config for external drivers, you'd need a different compile of grbl-lpc.  Please advise if this is the case (it depends on which way you've wired the external drivers, and we may have discussed this before). 


---
**Bill Keeter** *August 23, 2018 02:15*

**+Ray Kholodovsky** yeah, I'm that guy lol. I had to use !o for the external drivers. I can post my smoothie config file if it helps.


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2018 02:17*

No, I just need to know which way you've wired the external drivers - do they have a common ground or a common +5v at the step/dir/ en signals?


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2018 02:17*

Disregard.  You had to use !o. Reading... I just need to know which axes have the external drivers. 


---
**Bill Keeter** *August 23, 2018 02:21*

**+Ray Kholodovsky** Both X and Y are using external drivers to run my NEMA23 setup from OpenBuilds


---
**Ray Kholodovsky (Cohesion3D)** *August 23, 2018 02:22*

Do you use a Z table or Rotary or have any plans to?




---
**Bill Keeter** *August 23, 2018 02:28*

**+Ray Kholodovsky** No Z table. As I really only cut acrylic or wood sheets. I may try adding Rotary in the future. Just to engrave cups sometime down the road..


---
**Bill Keeter** *August 23, 2018 02:30*

**+Ray Kholodovsky** Here's my smoothie config file. It should hopefully explain my setup (and if I have anything configured wrong):  [dropbox.com - config.txt](https://www.dropbox.com/s/4ngem2f1vx5xv3e/config.txt?dl=0)


---
**Bill Keeter** *January 20, 2019 17:27*

**+Ray Kholodovsky** Just thought I would revisit this old conversation. I've added an old macbook air to my 60w laser. So I'm now running Lightburn directly on the machine with Smoothie firmware. Could you help me switch to GRBL?


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/ZJgJ6oyzmAv) &mdash; content and formatting may not be reliable*
