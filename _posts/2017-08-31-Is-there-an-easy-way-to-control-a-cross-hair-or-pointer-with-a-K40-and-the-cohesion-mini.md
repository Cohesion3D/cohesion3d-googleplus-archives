---
layout: post
title: "Is there an easy way to control a cross hair or pointer with a K40 and the cohesion mini?"
date: August 31, 2017 20:22
category: "K40 and other Lasers"
author: "Travis Sawyer"
---
Is there an easy way to control a cross hair or pointer with a K40 and the cohesion mini?  I'm running the stock firmware.  I'd like it for positioning and checking outlines, etc.  I'm running LW4 on a Pi3 connected to the C3d module. I'd like to be able to turn the pointer/crosshair on/off so it isn't always firing.





**"Travis Sawyer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 31, 2017 20:25*

Yep. You can hook the - of it up to the 2nd mosfet (screw terminal 6 on the bottom) and use a switch module in config.txt to toggle it with a gCode. Read up on smoothieware switch and the Mini pinout diagram for that pin #. 


---
**Erik McClain** *September 05, 2017 13:25*

**+Travis Sawyer**  Quick side question,  how are you implementing the cross hairs? Does your method reduce the power of the cutting laser in anyway?


---
**Travis Sawyer** *September 05, 2017 14:59*

I'm thinking of doing this mod:  [thingiverse.com - Laser Pointer Holder for 40W CO2 Laser by kai](https://www.thingiverse.com/thing:53301)



Run the outline with the hood open, check square, shut the lid and off you go.  I have about 10 5mw laser diodes coming in soon.  I like this solution as it doesn't add weight to the gantry.


---
**Erik McClain** *September 05, 2017 15:41*

wow thats  cleaver,  just at first glance not sure you would align it to the actual laser beam, also would need a latch to hold it up out of the way, but I like it.



I have see some that use special mirrors setups that combine the beams but it just seems like you would loose wattage.  But this I really like.  Thanks!!


---
**Travis Sawyer** *October 20, 2017 21:37*

I finally got my 6mm red lasers in.  Unfortunately, I'm not having much luck configuring smoothie / getting it to work.



Here's my new config lines:

#TBS - 20 Oct 2017, add pointer laser switch. Attach negative of the pointer to

# negative of the 2nd mosfet (P2.7)

switch.laserpointer.enable                      true

switch.laserpointer.output_pin                  2.7

switch.laserpointer.output_type                 digital

switch.laserpointer.failsafe_set_to             0

switch.laserpointer.input_on_command            M106.2

switch.laserpointer.input_off_command           M107.2



If I put the laser pointer on the VAUX (+5V) of the mini, it lights up, so cursory check is done.  If I issue M106.2, I get nothing.

Should I be connecting the + lead of the diode to VAUX, or to the + side of the FET?



Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2017 21:39*

At first glance, I don't think you can define a subcode that way.  Try just M106 and M107


---
**Travis Sawyer** *October 20, 2017 22:11*

That was it.  No subcode.  I could have sworn I saw them available for switch somewhere.  Now to wire it up properly :)


---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2017 22:17*

It's a separate config line to set up a subcode.  They'd also have to be different in this way:

M106.1 to turn on, M106.2 to turn off. 

You'd define M106 as the M code and subcode 1 on, subcode 2 off. 


---
**Travis Sawyer** *October 20, 2017 22:30*

I also have to hack lw4 to issue those commands on a check outline (or do it manually :( )


---
**Ray Kholodovsky (Cohesion3D)** *October 20, 2017 22:39*

For now you could just add a macro button to turn the laser pointer on and off.  2 clicks instead of 1, I'm dealing with worse odds on this end.  How's about 12 clicks and 2 full filament resets (physical actions). :)


---
*Imported from [Google+](https://plus.google.com/+TravisSawyer/posts/CmMd2uDsCaY) &mdash; content and formatting may not be reliable*
