---
layout: post
title: "Anyone using a rPI to connect wirelessly/ethernet to their Cohesion3D Mini?"
date: February 24, 2018 10:17
category: "C3D Mini Support"
author: "Seon Rozenblum"
---
Anyone using a rPI to connect wirelessly/ethernet to their Cohesion3D Mini? So no USB is needed and the controller computer can be in another room?



Is that even possible? I'm sure I read or saw something about it somewhere.



Can Lightburn do that too?





**"Seon Rozenblum"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 23:15*

LaserWeb had a pi comms server. Lightburn presently does not. 



**+Jim Fong** recently experimented with steaming to the board from Octoprint. Worked well with a few specific settings configured. 


---
**Seon Rozenblum** *February 24, 2018 23:18*

**+Ray Kholodovsky** Ok, thanks for the info. It's not a huge issue for me as i'll likely just re-purpose an older MacBook Air to run the Laser.



Hopefully in the future this will become an option either in Lightburn or some other tool.



I've never used Octoprint, but I plan to get it setup for my new Prusia MK3 when it arrives, so if I find that "user friendly" I might hit Jim up for more details on his setup.



Thanks again!


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/grwuDwqATmT) &mdash; content and formatting may not be reliable*
