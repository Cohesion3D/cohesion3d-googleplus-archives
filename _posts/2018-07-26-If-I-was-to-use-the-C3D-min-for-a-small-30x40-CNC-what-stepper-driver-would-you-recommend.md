---
layout: post
title: "If I was to use the C3D min for a small 30x40 CNC what stepper driver would you recommend"
date: July 26, 2018 13:47
category: "C3D Mini Support"
author: "E Caswell"
---
If I was to use the C3D min for a small 30x40 CNC what stepper driver would you recommend. CW5045 or TB6600?





**"E Caswell"**

---
---
**Jim Fong** *July 26, 2018 15:32*

I think you mean cw5045. These look to be clones of the familiar leadshine DM542 series but rated at 4.5amps instead.  



If they operate similar to DM542, then it would be a significantly better stepper driver than a TB6600.   



The DM542 I have use IR FETs and can handle the full 50volt supply voltage, run cool even at full rated current, low noise, anti-resonance, very smooth running stepper motor compared to my TB6600. 














---
**E Caswell** *July 26, 2018 16:15*

**+Jim Fong** Thank you for the reply Jim, yes, corrected the typo now.. looked at that many stepper drivers, from very cheap that are definitely clones and some that are very expensive.

The TB6600 seems to be very popular and fairly cheap and the CW5045 seems to be more money but if I get a better running machine then that has to be better.

I have been offered x5 2nd hand boxed an unused TB6600's for £25 so was wondering if it was worth having them.


---
**Jim Fong** *July 26, 2018 16:40*

**+PopsE Cas** careful about drivers that are advertised as TB6600. Many are now using the TB67s109 chip but advertising it as TB6600.  I have both types here. I prefer the TB6600 over the s109.  



TB6600 16microstep.  

s109 32microsteps. 



TB6600 4.5amp

s109 3.0amp










---
**E Caswell** *July 26, 2018 17:48*

**+Jim Fong** Thanks for that Jim, the guy selling the TB6600 did a CNC job for me early in the year and is selling everything due to terminal illness. 



I will get more info on what they are before purchasing, thank you for that bit of info it's much appreciated.



I brought the 30x40 but its got a blue board Mach3 unit in which is a 3 axis PP controlled. So would like to change it out.

Still in research mode to hopefully get it right. not an overnight job.



I have a Rudia to put in my Laser so the C3D will come spare.


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/CQrZ1MuSima) &mdash; content and formatting may not be reliable*
