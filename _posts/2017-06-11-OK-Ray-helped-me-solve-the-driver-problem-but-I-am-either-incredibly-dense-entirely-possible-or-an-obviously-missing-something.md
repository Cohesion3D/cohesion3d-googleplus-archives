---
layout: post
title: "OK, Ray helped me solve the driver problem, but I am either incredibly dense (entirely possible) or an (obviously) missing something"
date: June 11, 2017 21:16
category: "Laser Web"
author: "Terry Taylor"
---
OK, Ray helped me solve the driver problem, but I am either incredibly dense (entirely possible) or an (obviously) missing something.



I brought a raster file into LaserWeb4, drug it down to lower section, set it to engrave, generated the G-Code, but I cannot figure out a way to add it to the queue to actually engrave.





**"Terry Taylor"**

---
---
**Claudio Prezzi** *June 12, 2017 07:18*

Does connecting to the machine and Jogging work?


---
**E Caswell** *June 12, 2017 08:57*

**+Terry Taylor** have a look at this and have a read through

[cncpro.co - Live Jogging](http://cncpro.co/)



Did you connect to the machine?  if so then click on the "jog" tab and then select run from the button selection.


---
**E Caswell** *June 12, 2017 09:33*

**+Terry Taylor** have a look on the G+ forum documentation, good source of help if its a software issue.  Click on filter and select "documentation" happy reading :-)

[LaserWeb/CNCWeb](https://plus.google.com/u/0/communities/115879488566665599508/stream/57697394-3b78-49a8-9317-c6bb6e29b900)


---
**Terry Taylor** *June 12, 2017 22:30*

Totally uninstalling the driver and then doing the manual install did, but I am giving up on the C3D board.  



Please, no one (especially you, Ray) take this wrong, the hardware is EXCELLENT and installs and works well. I just cannot stand LaserWeb.  I KNOW that a lot of hard work went into LaserWeb and I appreciate that, but I just find that working with it is more trouble than it is worth. 



The user interface is NOT intuitive and there are too many steps to get from drawing to output. Coupled with the fact that you have to reference everything to the lower left (vice upper left) (which makes the loading of material difficult) and then the fact gantry does not return to home, but just stops and must be manually homed.  Hopefully, some day someone will come up with drivers for Windows and Mac that are transparent (like printer drivers) that make this easier to deal with.



I also do not like having to author/create in one program and then convert and load into another program to print. I do all of my creation in CorelDraw and will live with the limitations of CorelLaser.  Anybody want to make me an offer for my C3d Board, display, display adapter and 2 extra drivers?


---
**E Caswell** *June 13, 2017 07:19*

**+Terry Taylor** I understand where you are coming from, however The stock board and software in my opinion is very limiting.  

LW is very different from stock equipment but can do so much more.

Where you can do raster, cutting and laser fill all in the same job in one process. Where stock gear you have to do raster and cut as a separate job.

I had the same feeling as you but persevered through it and TBH I have gone back to the stock board a few times when experienced a few problems on LW. Remember LW is developing all the time and each update has a little snagging list wich the Devs are always working hard to resolve.

It is frustrating but in my experiance is worth the effort.

By the way, there are other softwares you can use and not stuck to LW, see link below.



[http://smoothieware.org/software](http://smoothieware.org/software)

Where are you in the world? 


---
**Terry Taylor** *June 13, 2017 16:30*

I am near San Francisco, CA


---
**Terry Taylor** *June 13, 2017 17:17*

+PopsE Cas (Silver)



Looking at the description, VisiCut only supports Smootie on Ethernet




---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 18:27*

Terry, I completely understand where you're coming from.  



To touch on a few points:

It is indeed possible to reference to top left in LW. Just need to shift the work coordinates by 200 in the Y. 

Further, both "setting zero" and homing are supported for setting job origin. 



It sounds like you want the head to return home at end of job?  G28.2 in your end gCode. 



Also, tell me what you think of this:  
{% include youtubePlayer.html id="playlist" %}
[youtube.com - LightBurn Progress - YouTube](https://www.youtube.com/playlist?list=PL6x69t5MbTC4QcDd3vGVp9BFHwgeMrjYL)


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 19:39*

**+Terry Taylor**, let me begin by saying that I understand your frustration, but I'd like to offer my opinion here if you don't mind. Let me start by saying, I have never used CorelDraw, even though it did come with my system (together with LaserDRW and the CorelLaser plugin). I just never cared to run a pirated copy of it nor to go buy a license when I already own the full Adobe suite. So from the beginning I was already looking for other options and for nearly a year I ran with creating everything in Photoshop and Illustrator, exporting as a bitmap, and importing in the LaserDRW interface to then talk to the laser. I understand that had I used CorelDraw, I wouldn't need this last additional step of manually loading files in the LaserDRW interface. Touché. I've heard of many who have problems with that Corel* setup.



When I switched to a Cohesion3D board, my workflow barely changed, however it got much faster and easier. I swapped out the LaserDRW program for LaserWeb. The same steps I was performing in LaserDRW, are similarly done in LaserWeb. In both, I had to manually set the cutting or engraving operations. However the big difference here is that with LaserDRW, those operations are performed one job at a time on the laser (engraving is one job, then cutting is another job), whereas in LaserWeb, they're all done in the same job. Saves a great deal of time when I am performing both engraving and cutting on the same piece. Furthermore, LaserWeb allows me to control what gets cut in what order. A lot of my designs require a lot of cutting and by default, neither LaserDRW nor LaserWeb do a great job at optimizing travel distances. But LaserWeb at least gives me the option of selecting the parts or entire sections that I want to do in a specific order (traveling the material sheet in a more controlled manner.)



I used to work at a sign shop and looking around the shop I can tell you all of those processes are done in similar fashion:

- the large CNC has its own computer and interface where you have to load in files that came from other applications

- all of the large format printers have their own interface that load up a variety of files from other applications

- 3D printers all have their own separate interface



And in your case, you aren't using CorelDraw to directly send to the laser cutter either. Your intermediary is the CorelLaser plugin which exposes the options to control the laser within CorelDraw. You will never find a laser interface that can come close to what a native graphics program will do, nor will you ever get a graphics program that can natively work with a laser. <b>There will always be "something in the middle".</b>



However, ultimately, this comes down to personal preference. If you are comfortable with the limitations of the stock control board, if you are comfortable with what you are doing with CorelDraw, then stick with it. For others it's simply too limiting. For me, I can't get proper gray scale engraving with it, I can't do multiple operations within the same job task with it, and as mentioned before, I have no desire to either run a pirate copy of it nor buy a license. So, I not only swapped out my control board, I also packed it up and sent it on its way (to another user who needed the dongle.) There is no going back option for me (unless I choose to buy it again from eBay.) But you couldn't pay me enough to switch back to the stock controlled. :)


---
**Terry Taylor** *June 13, 2017 21:59*

**+Ashley M. Kirchner****+Ray Kholodovsky**

I appreciate your comments, but first, let me point out that I am NOT using the pirated copy of Corel Draw that came with the K40. I have been a licensed Corel Draw user since (at least) version 4.



While what you say about the intermediary program is true, it is ALSO true that it is MUCH more seamless than having to save in another format, import, massage, encode and then send ( I HATE the word 'Play' for printing the file, this is NOT a DVD/VCR player <VBG>) the file to the laser. 



 I will look again at doing the Y offset (have to explore how to do that).I have been following the Lightburn progress and it looks interesting.


---
**Ray Kholodovsky (Cohesion3D)** *June 13, 2017 22:01*

Check the LaserWeb community regarding the offset, there was a new feature post not too long ago. 


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 22:32*

I agree, it is much simpler to do that. It's that same reason that a lot of things in life have an 'easy mode'. Push the 'Popcorn' button on a microwave and it will happily nuke the kernels for you. Push the shutter button on a camera and you get a perfect picture. There's nothing wrong with that. But what if the popcorn you're trying to make need an extra 7 seconds? What if you want the camera to focus on a slightly different subject, or a different exposure. Those 'easy mode' buttons aren't going to do the job.



THAT SAID, as I mentioned before, if for you the 'easy mode' works, you have zero reasons to change that. It's what works for you, it's your workflow and no one can tell you to change that. For many of us who did the conversion, we want better control of the machine (or for an unfortunate few, their original board fried itself.) The ability to control the minutia of the machine to get better results, or results that we want, whether it's a lighter burn, or a light cut that doesn't go all the way through, that's where having full control comes into play.



I only ever use the PLAY function (on the glcd) for large raster jobs and I agree, I also dislike that terminology. A lot of my smaller 'rasters' are actually done with LaserWeb's "Fill Path" operation which imitates a single color raster. All of my work is done and sent straight from within LaserWeb, just hitting 'run job', which makes more sense than 'PLAY'.


---
**Ashley M. Kirchner [Norym]** *June 13, 2017 22:38*

By the way, I'm not trying to convince you to 'stick with it', not at all. It's totally your choice.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/jdoTK8JvAEV) &mdash; content and formatting may not be reliable*
