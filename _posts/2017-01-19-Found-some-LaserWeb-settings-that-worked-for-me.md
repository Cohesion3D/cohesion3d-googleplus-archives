---
layout: post
title: "Found some LaserWeb settings that worked for me"
date: January 19, 2017 03:26
category: "General Discussion"
author: "David Komando"
---
Found some LaserWeb settings that worked for me. I have LaserWeb running on a RPi3 inside the K40 case connected USB to the Cohesion 3D Mini and all seems to work well. I think the biggest issue I had was the power of the laser. Somehow was missing that it was at 100%!



I went ahead and screen capped a bunch of different settings for anyone else to use or for anyone to let me know I have them wrong. ;)



![images/41221db3ffc24275662e05af94a4d6cd.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/41221db3ffc24275662e05af94a4d6cd.png)
![images/b85c8cb7b6e1094cbaf8f41b8d428760.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b85c8cb7b6e1094cbaf8f41b8d428760.png)
![images/5818f33ad56cd62a9ead06f01994dc29.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5818f33ad56cd62a9ead06f01994dc29.png)
![images/2ae1d315586e4bfc7e9f42c968a41dfd.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ae1d315586e4bfc7e9f42c968a41dfd.png)
![images/f06e103fd8834f61470eb8f9658b04f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f06e103fd8834f61470eb8f9658b04f4.jpeg)

**"David Komando"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 03:30*

I have some different stuff on the Size tab, the Gcode tab is fine. Dpi is going to vary person to person based on the output of the software they use. Sorry I'm behind on the docs, the LW settings is part of what we need to provide in the getting started info. 


---
**David Komando** *January 19, 2017 03:43*

No problem at all. Always good to ask for help but usually try to figure stuff out on my own as well. 



The RaspberryPI 3 making the K40 all self contained seals the deal for me, making this project pretty much perfect IMO. I'll be re-mounting the C3D Mini with the RPi3 this weekend. Will post some more pictures then.



Thanks for making a very cool and easy to connect product!


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 03:45*

Laser Beam Diameter could range from 0.1 to 0.5 so play with that if your rasters seem off. 

I'm glad you like it.  Keep us posted.


---
**David Komando** *January 19, 2017 03:53*

**+Peter van der Walt** thanks as well to you and team for making some kick-ass software!


---
**Kris Sturgess** *January 19, 2017 18:41*

**+David Komando**​ any chance of getting a image copy of your RPI3?  Been fighting with install.


---
**David Komando** *January 19, 2017 19:05*

**+Kris Sturgess** Might be easier for me to write a bash script to install everything for you minus the settings or if you want to let me know what problems you are having I might be able to help.

I will say it took me a bit to find a good tutorial on setting the wireless IP manually. My router had run out of slots to enter Mac addresses for the dedicated IPs.



Also, had some small issues with getting LaserWeb3 to start on boot since I didn't want to use the default pi user. Ended up using nodejs PM2 project.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 19:06*

This is interesting.


---
**Kris Sturgess** *January 19, 2017 19:07*

Biggest issue was SerialPort failing install. Tried all the "fixes" but still got nowhere.


---
**Kris Sturgess** *January 19, 2017 19:08*

Anything you could share **+David Komando**​ would be a great help.


---
**David Komando** *January 19, 2017 19:39*

In the middle of the workday here so would be a day or so before I could bust out the script. For now here are some of the tutorials I used to get going:



My nodejs install instructions ... slightly different than the LaserWeb3 instructions: (ignore this since google+ formats strikethrough with dashes)

$ curl sL [https://deb.nodesource.com/setup_6.x-](https://deb.nodesource.com/setup_6.x-) | sudo E bash -

$ sudo apt-get update && sudo apt-get -y install nodejs

$ sudo apt-get -y install build-essential



LaserWeb3 RPI install.

[https://github.com/LaserWeb/LaserWeb3/wiki/Install:-Raspberry-Pi-Image-Build](https://github.com/LaserWeb/LaserWeb3/wiki/Install:-Raspberry-Pi-Image-Build)



PM2 Auto Start for users other than PI: (Scroll down to: Starting Node-RED on boot)

[https://nodered.org/docs/getting-started/running](https://nodered.org/docs/getting-started/running)

You will also need to switch out this line:

pm2 start /usr/bin/node-red - -v

With something more like this:

pm2 start /home/<YOUR-NEW-USER>/laserweb3/server.js

To make this process work.



RPI3 Static IP:

[https://www.pahoehoe.net/raspbian-wlan0-static-ip/](https://www.pahoehoe.net/raspbian-wlan0-static-ip/)



As for the USB port, the first thing I can think of for why this wouldn't work for you, could be if you downloaded the LaserWeb3 GitHub repo as a zip file and extracted to a directory. If you did you will definitely want to start over and install LaserWeb3 with a "git clone". If you did happen to do a "git clone" and it is not working ... maybe the USB port you are using or maybe even the cable you are using to connect to the C3D Mini.



Also, very important to make sure once LaserWeb3 has been downloaded you run:

npm install



I'll attach a photo of my RPI3 setup of the lower USB port I am using, ultra basic headless SBC.

![images/71c85a281cc351e15a1fc009cbea725c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/71c85a281cc351e15a1fc009cbea725c.jpeg)


---
**David Komando** *January 19, 2017 19:42*

Not sure why there are lines through everything. G+ doesn't like me I guess.


---
**Ray Kholodovsky (Cohesion3D)** *January 19, 2017 19:43*

Formatting - if dash at start and end might think strike through

<s>test1</s>

- test2 -




---
**Kris Sturgess** *January 19, 2017 19:56*

Thanks **+David Komando** i'll give it a whirl in a bit. My last attempt was following the Wiki word for word. It would error out and wouldn't load LaserWeb  (serialport error). I was using a fresh install of Raspbian and git pull for the files. Using 8" USB cable to board. Do you recommend anything different than Raspbian os?


---
**David Komando** *January 19, 2017 20:22*

I'm just using the latest version of Raspbian-Jesse here. The full version not the lite version. I figured the 64GB card could handle the extra load. I'll work out a script on a different SD Card and see if I can get it all working automatically.


---
**Kris Sturgess** *January 19, 2017 20:25*

Awesome Thanks! I'll tinker and try again in the meantime. Hoping to have a similar setup on my laser to my OctoPrint setup on my 3D printer.


---
**David Komando** *January 19, 2017 23:37*

**+Kris Sturgess** I went ahead and busted out my install script, basically retracing the steps I took to get it all working. 



I did end up testing on an old class10 card and it was a bit slow. Use to the class10-U3 cards these days.





Anyways, quick disclaimer. 

I will not held responsible for anyone running this script. Run at your own risk! 

[https://www.amazon.com/clouddrive/share/n5NS7GnbRprEAu7xxarvdzPrydU9A32UJRH5wpDTqGH?ref_=cd_ph_share_link_copy](https://www.amazon.com/clouddrive/share/n5NS7GnbRprEAu7xxarvdzPrydU9A32UJRH5wpDTqGH?ref_=cd_ph_share_link_copy)



It's meant to be run on a fresh install of:

2017-01-11-raspbian-jessie.img



Also, I would:

- review the file in a text editor. (never trust people on the internet) ;)

- drop the file in your RPi user directory. 

- run a "chmod +x rpi-laserweb-install.sh" without quotes.

- then run the script with: "sudo ./rpi-laserweb-install.sh" again, without quotes.





If you don't want to run every bit of the file you can copy / paste what you need from a text editor.



Hope this helps you track down your issue or fix it all together!


---
**Kris Sturgess** *January 20, 2017 06:19*

Hi **+David Komando** Attempted the install. Fresh copy of Raspbian Jessie. I did a manual line by line from your script. I've attached the error log and what it says when I try to run LaseWeb3. I'm wondering if there is a directory off???

If you could take a looksie when you have a chance.



[drive.google.com - error.txt - Google Drive](https://drive.google.com/open?id=0B2TmtKs6ubOYZmplQzk2Q21zZm8)




---
**David Komando** *January 20, 2017 13:07*

I see you got a 404 for that library. Which I did as well the first time I ran the sudo npm install. I just ended up re-running the sudo npm install a few times and eventually it went away. Guessing npm is having issues with that library or with their servers. 


---
**Kris Sturgess** *January 20, 2017 16:58*

**+David Komando**​ it's the error I keep running into. Believe me I've tried running npm install over and over.. no luck. Even tried some work arounds as well and they fail.

I'm no RPi/Linux wizard but the file it's trying to locate/install where is that being called from? As this is the LaserWeb npm install is it something that needs fixing in the install sequence?


---
**Kris Sturgess** *January 21, 2017 03:22*

Thoughts anyone? The file it's trying to install is no longer available (404). Is there somewhere we can change what file it tries to download? **+David Komando** 


---
**David Komando** *January 21, 2017 03:47*

The problem is its a 3rd party library. Guessing it been implemented into LaserWeb. So just switching it out probably isn't an option.



I was just able to get this working:

inside my LaserWeb folder and without sudo

npm install serialport@4.0.6 --save



It will still 404 on the packaged ARM version then should being to compile an ARM version as a fallback.




---
**David Komando** *January 21, 2017 03:51*

As a note the latest version 4.0.7 compiles as well.

Again you can run this in your LaserWeb3 folder without sudo:

npm install serialport --save



I did verify it is still finding my USB port in the LaserWeb3 interface.


---
**Kris Sturgess** *January 21, 2017 04:57*

uggg. I don't know.. It just isn't working for me. Is there a permission setting I'm missing? Error log attached. **+David Komando**



[drive.google.com - error 2 - Google Drive](https://drive.google.com/open?id=0B2TmtKs6ubOYeGk2azRfT0lZUzg)


---
**David Komando** *January 21, 2017 05:07*

So, the last think i can think of tonight to try would be to elevate your privileges to root and not just use a sudo.

sudo su -



From there navigate into your "/home/pi/LaserWeb3" folder.

cd /home/pi/LaserWeb3



Then finally run the last command one more time:

npm install serialport --save



From the error log you posted, it looks like this one should work for you.


---
**Tako Schotanus** *February 21, 2018 16:10*

**+David Komando** a slightly off-topic question: you have a separate power supply for your rPi? Or are you using the 5v terminal of the K40's power supply? (Is that even possible?)


---
**David Komando** *February 21, 2018 16:21*

**+Tako Schotanus** I'm using a separate power supply. Was the easiest way to go for me.


---
*Imported from [Google+](https://plus.google.com/+DavidKomando/posts/SewGL37rGEc) &mdash; content and formatting may not be reliable*
