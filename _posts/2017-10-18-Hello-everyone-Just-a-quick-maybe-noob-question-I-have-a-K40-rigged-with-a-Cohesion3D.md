---
layout: post
title: "Hello everyone, Just a quick (maybe noob) question: I have a K40 rigged with a Cohesion3D"
date: October 18, 2017 22:29
category: "K40 and other Lasers"
author: "Michele Caleffi"
---
Hello everyone,

Just a quick (maybe noob) question:

I have a K40 rigged with a Cohesion3D.

I remember that with the stock board (sad times) it did zero itself on start, so I guess the machine has endstops, right? 

If not, can anyone point to how to install some on a K40 and how to set up the smoothie?

Thanks a lot guys!





**"Michele Caleffi"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 22:32*

The command G28.2 should home your laser. 



You can make a text file called on_boot.gcode on your microsd card and put G28.2 as the contents of it, then it will home on each boot. 



But first, you should run that command manually via the gCode terminal like in LaserWeb to make sure the head does move to the left rear. It will touch twice then stop. 


---
**Michele Caleffi** *October 18, 2017 22:59*

Hey Ray,

Thanks a lot, I'll try tomorrow.

Loving the C3D!


---
**Don Kleinschnitz Jr.** *October 19, 2017 11:01*

**+Ray Kholodovsky** heh... did not know you could do this :)!\

#K40BootGcode


---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 12:57*

**+Don Kleinschnitz** see, if I put that on the memory card I include I would have had 100 less support inquiries to answer. But alas, I have hesitations about machine moving without the user directly commanding it to do so. 


---
**Michele Caleffi** *October 19, 2017 14:27*

**+Ray Kholodovsky** I understand your concern! But coming from MOSHI I wouldn't have been surprised, that's what it was doing before the upgrade. 

So if I understand correctly, if I test the code from LW and it goes in the far-left corner, I'm good to go to create the on_boot.gcode.

Then each time I start the machine the head would park on far left corner, but will Laserweb know where the head is or would I have to specify it, since that would be X0:Y200 and not X0:Y0. 

Or is there another magic code string to apply to the on_boot.gcode to have it going to X0:Y0?




---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 14:34*

That's already taken care of. It will report it is at 0,200 after homing. 


---
**Michele Caleffi** *October 19, 2017 14:37*

Perfect, I'll give it a go and report back. Thanks!




---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 15:56*

Do you want the head to physically move to 0,0 front left after homing? 

G0 X0 Y0 F3600


---
**Michele Caleffi** *October 19, 2017 21:48*

Not necessarily. If LW knows where the head is (home) it is better out of the way I guess. Thanks Ray!




---
**Ray Kholodovsky (Cohesion3D)** *October 19, 2017 21:49*

Exactly my thought. 


---
**Michele Caleffi** *October 25, 2017 13:46*

Works flawlessly, homes fast, touches, gets back a tad on both axes and homes again. It reports 0,200. Just what I needed.

I'll create the code for the sd card.

Thanks!

P.S. I think if I had the code installed by default it would have made my life easier and I wouldn't have worried a bit about the head moving alone, it was doing it before upgrading to C3D.


---
*Imported from [Google+](https://plus.google.com/104424443167139318708/posts/WQeEz284bPS) &mdash; content and formatting may not be reliable*
