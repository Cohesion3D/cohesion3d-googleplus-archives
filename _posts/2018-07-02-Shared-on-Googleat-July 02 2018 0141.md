---
layout: post
title: "Shared on July 02, 2018 01:41...\n"
date: July 02, 2018 01:41
category: "General Discussion"
author: "Kirk Melby"
---


![images/7049d9f8327e6116d51e6f2680d761a6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7049d9f8327e6116d51e6f2680d761a6.jpeg)



**"Kirk Melby"**

---
---
**Kirk Melby** *July 02, 2018 01:42*

Why does this happen?


---
**Joe Alexander** *July 02, 2018 02:21*

what firmware are you running, smoothie or grbl? Grbl supposedly does faster engravings with less of these kinds of errors. how long was the engraving job and at what settings?


---
**LightBurn Software** *July 02, 2018 03:03*

Assuming you're asking about the shift in the image, it could be a number of things, from most likely to least likely:



- You're using Smoothieware and asked it to process more than 800 gcodes per second (254 DPI @ faster than 80mm/sec)

- Acceleration is too high

- Speed is too high

- Stepper power supply is weak

- Stepper current set too low

- Machine rails or bearings are sticky


---
**Kirk Melby** *July 02, 2018 03:56*

Well, having posted first and researched later, (sorry), I'm pretty sure this is a smoothie problem.  I just got grbl working and I'm re-running it now, so we will see.  I have to say I wish the firmware process was a little better documented.



I've seen variations of this problem before, while engraving, as well as a few other alignment problems when cutting.  Things occasionally just do not cut like they are on the screen.  I'm hoping grbl cures all.


---
**LightBurn Software** *July 02, 2018 04:58*

If you get misaligned cuts, there's a good chance the acceleration value is too high. I had that issue on my personal K40 / C3D setup, and lowering it from the default helped a great deal, especially when running cuts with lots of rapid moves and direction changes. For engraving, it's no contest - Grbl can handle about 3x the throughput of Smoothie or better.


---
**Kirk Melby** *July 03, 2018 11:24*

Here's one example of the alignment problem. The outer most rectangle is on one layer and cuts all the way through. One another layer I have the same rectangle plus another one offset to the inside.  The inner one is a very light cut to just mark a line.  I have the outer cut one duplicated on the same layer as the marking one to make things easier to line up in the software.  One pic is of the computer screen showing everything is OK, the other is how it cuts.restarting everything fixed this one.  I've had this happen a few times and it seems yo only effect curtain lightburn files.  I can open a different file with similar layers and cuts and it's fine, re-open the problem one and it still has problems.  FWIT, the preview window always shows everything correct



![images/9371248bb653c83cd35ae651fd40bf1d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9371248bb653c83cd35ae651fd40bf1d.jpeg)


---
**Kirk Melby** *July 03, 2018 11:24*

![images/9017e524e19784a98635b5a2f3df3d9b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9017e524e19784a98635b5a2f3df3d9b.jpeg)


---
**Kirk Melby** *July 03, 2018 11:35*

And here's another odd one.  The notch in the bottom is not in the design. The lightburn file had two identical copies of this pattern.  One cut correctly and one with this notch.  Both looked good on screen and in the preview.  Restarting did not fix this one.  I ended up moving the part a little in lightburn and then it worked.

![images/9d489ef493a9cdbeb9f0f4995f8162c2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9d489ef493a9cdbeb9f0f4995f8162c2.jpeg)


---
**LightBurn Software** *July 04, 2018 05:48*

That looks like you have machine tuning issues. See my original list of suggestions up top.  Make sure all the mechanical things are correct first, and then try lowering your acceleration (it's in the smoothie config file on the SD card).


---
*Imported from [Google+](https://plus.google.com/112981589482660381925/posts/a8n9WkYvzqf) &mdash; content and formatting may not be reliable*
