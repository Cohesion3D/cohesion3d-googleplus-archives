---
layout: post
title: "ETA: Fixed. See my first comment. Got my Cohesion3d Mini board installed in my K40 today"
date: January 06, 2018 20:24
category: "K40 and other Lasers"
author: "Jeff Harbert"
---
ETA: Fixed. See my first comment.



Got my Cohesion3d Mini board installed in my K40 today. Doing some test cuts to get the hang of Lightburn and I noticed my circles aren't circular. I drew up a quick test SVG file of 20mm squares and circles, and here is the result.



I numbered the object in the order they were cut by the machine. The squares look ok except for the start of the first one. The circles definitely aren't round, though they are consistent, and the first one has a gap between the start and finish of the cut. I'm cutting at 100mm/sec.



I'm not sure where to start troubleshooting this. Any suggestions would be appreciated.

![images/a16e685c8b8b57e10f064912f8603181.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a16e685c8b8b57e10f064912f8603181.jpeg)



**"Jeff Harbert"**

---
---
**Jeff Harbert** *January 06, 2018 20:53*

I found the problem. The shaft that drives the left side y-axis belt was disconnected. I fixed this and the problem went away.



Leaving this post up in case it can help others in the future.

![images/68be7b3af9bbcf2000402da081d1e81e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/68be7b3af9bbcf2000402da081d1e81e.jpeg)


---
**Don Kleinschnitz Jr.** *January 06, 2018 21:53*

#K40DriveProblems


---
**Ray Kholodovsky (Cohesion3D)** *January 07, 2018 00:22*

Great. 


---
**Steve Clark** *January 21, 2018 00:10*

Thanks for showing the resolution/problem solved. I hate it when people just wander off with the answer but don't report back.


---
*Imported from [Google+](https://plus.google.com/+JeffHarbert/posts/CxRgRwFYyp8) &mdash; content and formatting may not be reliable*
