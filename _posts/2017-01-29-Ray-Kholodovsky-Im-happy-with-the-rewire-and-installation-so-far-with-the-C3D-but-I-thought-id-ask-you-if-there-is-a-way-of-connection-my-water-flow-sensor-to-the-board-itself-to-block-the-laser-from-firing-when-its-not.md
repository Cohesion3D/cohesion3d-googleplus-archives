---
layout: post
title: "Ray Kholodovsky I'm happy with the rewire and installation so far with the C3D but I thought i'd ask you if there is a way of connection my water flow sensor to the board itself to block the laser from firing when it's not"
date: January 29, 2017 21:41
category: "General Discussion"
author: "Andy Shilling"
---
**+Ray Kholodovsky** I'm happy with the rewire and installation so far with the C3D but I thought i'd ask you if there is a way of connection my water flow sensor to the board itself to block the laser from firing when it's not detected?

 i've included the spec if this helps.

Minimum rated working voltage: DC4.5 5V-24V

Maximum current: 15 mA(DC 5V)

Working voltage range: DC 5~18 V

Load capacity: ≤10 mA(DC 5V)



Also if it worth fitting the new Pot that I've got or just rely  on the pwm from the board?





**"Andy Shilling"**

---
---
**Anthony Bolgar** *January 29, 2017 21:56*

Use the interlock terminals on the PSU


---
**Andy Shilling** *January 29, 2017 22:11*

That's the P+ and G on the PSU isn't it. I was thinking, if I run the P+ to the sensor and then use the signal from the sensor  to go back to the switch and throught the switch return to the G on the PSU.

Sorry  but I've spent the last couple of days trying to get things rewired and my brain isn't working so well lol.

 


---
**Don Kleinschnitz Jr.** *January 29, 2017 22:29*

**+Andy Shilling** all of these questions are answered within the section of the build index!



Check these sections:

...The interlock Subsystem

...Aligning, Cooling, Operating and Protecting The Laser



If you have an active water flow sensor that is a more complex but unnecessary implementation. What sensor are you using?



The water sensor needs to be reliable so I recommend using a simple switch and wire it in to the interlock system as **+Anthony Bolgar** suggests.



.........................

Oh my, you need the pot! Search the forum for the background.

...........................



[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



Interlocks, pots etc.


---
**Andy Shilling** *January 29, 2017 22:56*

**+Don Kleinschnitz**​ I'm sorry but where has this been? I have never come across it before. Might be because I use my phone for G+ or I have just missed it but thank you that does answer a lot of questions for me.


---
**Don Kleinschnitz Jr.** *January 29, 2017 23:17*

**+Andy Shilling** LOL its been been there all along and linked to:

[http://smoothieware.org/laser-cutter-guide](http://smoothieware.org/laser-cutter-guide)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/5mbHjaNE1dy) &mdash; content and formatting may not be reliable*
