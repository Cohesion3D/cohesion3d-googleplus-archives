---
layout: post
title: "I am planning to build my own rotary attachment and Z axis, I have a couple of spare Nema 17 steppers so plan to use them"
date: May 28, 2017 21:37
category: "K40 and other Lasers"
author: "E Caswell"
---
I am planning to build my own rotary attachment and Z axis, I have a couple of spare Nema 17 steppers so plan to use them. 



I have read up on what may be needed but was a little puzzled if I needed to go to the TB6600  driver controller and break out board from the C3D mini.

No issues with power supply as I have a separate 24v 5a supply for the board.

Don't want to Risk smoking the board or drivers.

Already connected the steppers up to the board and they work well when manually controlled using the z axis controller on LW4

Any advice would help. 





**"E Caswell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 28, 2017 21:46*

The A4988 drivers can run Nema 17's.  I've moved some hefty stuff with them.  But some implementations (like LightObject) just need too much power and that's where the external drivers come into play.  I've seen designs for tables and rotary's that run fine off the A4988 drivers. 



If it works, it works. 


---
**E Caswell** *May 29, 2017 08:00*

Thanks Ray. I was taking the load to the stepper is going to be low,  the only things it's turning is whatever I make it out of and the resistance load Will be low 


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/7MqyQ2LxYH6) &mdash; content and formatting may not be reliable*
