---
layout: post
title: "im sure this has been answered (i searched the community for a bit to no avail) but i don't like the c3d mini board/display being powered by the usb"
date: December 17, 2017 01:27
category: "C3D Mini Support"
author: "Tech Bravo (Tech BravoTN)"
---
im sure this has been answered (i searched the community for a bit to no avail) but i don't like the c3d mini board/display being powered by the usb. sounds like trouble. is there boot sequence to avoid issues? or better yet is it okay to clip the  +5v on the usb (or is there a jumper on the board or a config param to do this)?





**"Tech Bravo (Tech BravoTN)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 17, 2017 01:31*

Solder Jumper on bottom of board USB_PWR - you can cut the trace connecting the 2 pads but be very careful not to cut any traces as the SD Card SPI lines run right alongside it. 



Modifying a USB Cable to cut the +5v wire - some people have also done this. 



Ultimately I would like people to have a separate 24v PSU connected to run their board - removes burden from the underpowered K40 PSU, no more risk of brownouts... 


---
**Tech Bravo (Tech BravoTN)** *December 17, 2017 01:38*

i do have a dedicated 24v psu coming for the c3d and z axis. doing that will not affect the usb issue one way or another i don't think. i will modify a cable. thats a lot cheaper than a new board. thanks




---
**Bill Schober** *December 19, 2017 21:43*

How would you connect the 24 PSU?


---
**Tech Bravo (Tech BravoTN)** *December 19, 2017 22:19*

i connected the 110vac from one of the "outlets" on the rear of the unit (clipped them as soon as i got it) and made sure i got a good chassis ground on the frame (all the electronics in the unit need to reference the same ground potential or it will act up) then 24vdc (+ and -) to the cohesion3d board. i plan on adding videos and KB Articles of my mods very soon.




---
**Tech Bravo (Tech BravoTN)** *December 19, 2017 22:22*

see the Aux Power Header just behind the end stops connector. Disconnect the 24v + and - from there (leave the laser wire!) and connect the external 24v psu to the Main Power In. 



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5077459912/original/u5eZ63AohtQjomn9OxoR_j4JmrBzqIUPxw.png?1484797570)


---
**Bill Schober** *December 19, 2017 23:30*

Thank you!  Is there any particular PSU that you would recommend?  

This one maybe? [amazon.com - Amazon.com: New DC 24V 15A Switching Power Supply Transformer Regulated for Cctv, Radio, Computer Project: Computers & Accessories](https://www.amazon.com/Switching-Transformer-Regulated-Computer-Project/dp/B00ANFJ26U/ref=sr_1_2?ie=UTF8&qid=1513719722&sr=8-2&keywords=24v+psu)


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2017 23:40*

Like that, but a 24v 6a is all you should need.



The going recommendation is disconnect the 24v pin from the laser psu, get a 24v6a power supply, and wire the 24v and Gnd from that to the "main power in" screw terminal. 



It is important to maintain a common ground between the new psu and the stock one in order to keep firing the laser. This is why we only disconnect the original lpsu 24v. You can pull the wire from the screw terminal, cut the wire, or pull the pin from the big power connector going to the C3D Mini. 



The lack of power/ cleanness from the stock psu is my number one cause of grief.  


---
**Tech Bravo (Tech BravoTN)** *December 20, 2017 00:18*

ah yes! thanks for the clarification i will make a note of it. it makes total sense that the low voltage side needs to share the same ground reference.


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 00:19*

[plus.google.com - I want permit the K40 power supply to only be tasked to power the K40 laser a...](https://plus.google.com/u/0/117720577851752736927/posts/emzYWQAAXuW)



Some more info and a diagram in the comments there.


---
**Seon Rozenblum** *February 21, 2018 10:19*

Wouldn't it just be easier to replace the K40 PSU with a better one and run everything from that? Rather than running 2 PSUs and having to re-wire?


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/KjqgoS5CUib) &mdash; content and formatting may not be reliable*
