---
layout: post
title: "HELP!!! re: cohesion3D mini K40 upgrade >> last month I stupidly burned the cohesion board by using the aux 5v out for powering neopixel led strip..."
date: March 20, 2018 18:34
category: "K40 and other Lasers"
author: "Maurizio Martinucci"
---
HELP!!!   re: cohesion3D mini K40 upgrade >> last month I stupidly burned the cohesion board by using the aux 5v out for powering neopixel led strip... smoke came out from the Cohesion and from the Y endstop sensor board. 



I got a new Cohesion, installed as in [https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726542-k40-upgrade-with-cohesion3d-mini-instructions-)   all works but the Y motor, it just doesn't respond at all.  X responds and moves perfectly with Lightburn. I have tested the Y motor itself connecting it to another controller (arduino nano + A4988) and it seems to respond OK. So something seems faulty in sending power and/or commands to the Y motor from the Cohesion.  I have checked also the endstop and actually replaced the sensor TCST1030 and the resistor according to this >> [http://donsthings.blogspot.nl/search/label/K40%20Optical%20Endstops](http://donsthings.blogspot.nl/search/label/K40%20Optical%20Endstops)   After no success with the 100 Ohm replacement I put the 1K back there but no luck again.  I have no idea if/how I can test the working of the endstop otherwise.



Needless to say I am quite desperate at this point.  HELP!!!!





**"Maurizio Martinucci"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 18:41*

As a quick fix you can do this:



Power off.  Move the motor cable and driver from Y to A (the 4th socket).



Remove the config.txt file currently on the card and place this new one onto it. You can do this via USB cable (board shows up as a drive in computer) and then you have to dismount the drive in Windows and reset the board.  



[dropbox.com - C3D Mini Laser Batch 2 - Provided Config with Y remapped to A](https://www.dropbox.com/sh/9b6z1rpg1nbmnkn/AAAAmp2KY9oDjijR6r3X0ndWa?dl=0)






---
**Maurizio Martinucci** *March 20, 2018 19:13*

**+Ray Kholodovsky**  I did exactly as you indicated, but again no response whatsoever from Y motor. Tested X motor again and responds absolutely fine with Lightburn manual "set laser position". I also tried and run a gcode with G28.2 + G0 X0 Y0  and X homed nicely but Y didn't move at all...   Any other way to test it?



![images/ef77b8184e71b6672513aad65408ea02.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef77b8184e71b6672513aad65408ea02.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 19:15*

Can you swap the X and Y driver modules with each other?  Please make sure to have power off and be careful not to plug them in backwards. 


---
**Maurizio Martinucci** *March 20, 2018 19:18*

**+Ray Kholodovsky**   Ha!  that worked!!  that means the driver is faulty then!  




---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 19:19*

Do you have another driver? 


---
**Maurizio Martinucci** *March 20, 2018 19:29*

Yes, I have a A4988 (see pic), tested it and all seems to work now... should I move the driver to the original Y position (instead of A) and put the old config.txt now?  

![images/bc1599ddc887f58698b42d0b845d6b79.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bc1599ddc887f58698b42d0b845d6b79.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 19:31*

You can do that, sure. 


---
**Maurizio Martinucci** *March 20, 2018 20:01*

**+Ray Kholodovsky**  now suddenly both drivers smoked :(((((     I didn't do anything this time... may I msg you in private?


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 20:04*

You can send an email via the company website. 


---
**Maurizio Martinucci** *March 20, 2018 20:28*

**+Ray Kholodovsky**  done.




---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 21:02*

Got it.  Unplug everything from the board including drivers and just plug in USB, tell me exactly what LEDs and behavior you get.  Try with and without SD card in board. 


---
**Maurizio Martinucci** *March 20, 2018 21:14*

Thanks for the prompt assistance, gotta be tomorrow though... I am home now, k40 is at lab.. kind of late here.  Cheers




---
**Maurizio Martinucci** *March 21, 2018 11:53*

Ok  **+Ray Kholodovsky** , I unplugged everything, connected USB to laptop, just one led goes green, no blinking, nothing... same behavior with and without the SD card

![images/6d8685b787d3662d9fa5532d56685273.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6d8685b787d3662d9fa5532d56685273.jpeg)


---
**Maurizio Martinucci** *March 23, 2018 16:01*

**+Ray Kholodovsky**, any feedback there?


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2018 16:15*

Traveling now for trade show, back Tuesday, will respond then. 


---
**Maurizio Martinucci** *March 23, 2018 16:21*

ok cool. 


---
*Imported from [Google+](https://plus.google.com/117027581754233433770/posts/ShQ2EECjyCW) &mdash; content and formatting may not be reliable*
