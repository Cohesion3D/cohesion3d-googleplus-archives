---
layout: post
title: "I'm finally trying to setup my A-Axis but I'm not finding any settings in the smoothie config to support it"
date: February 12, 2017 00:16
category: "C3D Mini Support"
author: "Carl Fisher"
---
I'm finally trying to setup my A-Axis but I'm not finding any settings in the smoothie config to support it. Has anyone done this yet?





**"Carl Fisher"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 00:56*

Completely separate firmware build and I believe the smoothie upgrade notes on github show what to put in config. 


---
**Carl Fisher** *February 12, 2017 01:47*

Hrmm, the only firmware I see is the firmware.bin, firmware-cnc.bin (which I'm using). and firmware-disablemsd.bin


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 01:58*

Need to compile new firmware using 

make AXIS=4 CNC=1

Read up here: [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/upgrade-notes.md)

I can deal with compiling this for you after tomorrow. 


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/LyWUQqGRBFr) &mdash; content and formatting may not be reliable*
