---
layout: post
title: "Hi Ray Kholodovsky , do you also have a 3d-model of the CAT5_Extruder_Board ?"
date: February 08, 2017 14:17
category: "C3D Remix Support"
author: "Marc Pentenrieder"
---
Hi **+Ray Kholodovsky** ,



do you also have a 3d-model of the CAT5_Extruder_Board ?





**"Marc Pentenrieder"**

---
---
**Marc Pentenrieder** *February 18, 2017 14:02*

Hi, anybody interested in an enclosure like this?

It includes a RemixBoard, 2x Cat5 Extruder Boards and a GLCD ;-)

I just threw a few things together and rendered it.

![images/545ca6899bedf612dd88082bd65d5ca5.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/545ca6899bedf612dd88082bd65d5ca5.png)


---
**Marc Pentenrieder** *February 18, 2017 15:17*

**+Ray Kholodovsky**​ What do you think of an enclosure like the one above?


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 15:19*

Very cool. Send over cad files :)


---
**Marc Pentenrieder** *February 18, 2017 15:20*

Its not finished yet, but I will make one for me ;-)


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 15:21*

**+Eric Lien** you may want to take a look at the pic ^^


---
**Marc Pentenrieder** *February 18, 2017 15:23*

**+Ray Kholodovsky**​ **+Eric Lien**​ Perhaps you know somebody, who can make a nice 3d model of the cat5_extruder_board?


---
**Eric Lien** *February 18, 2017 15:39*

Wow, beautiful work.


---
**Marc Pentenrieder** *February 21, 2017 20:56*

**+Ray Kholodovsky** Today I managed to design "my" enclosure.

I will also give it to the community if it's okay?

Many thanks to **+Eric Lien** for designing the base enclosure.

If there are any advises, what is missing I am glad to hear from you.

![images/1b9810c252b7d5a13c0e37ee85cc3cf4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b9810c252b7d5a13c0e37ee85cc3cf4.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 20:58*

I like it!


---
**Marc Pentenrieder** *February 23, 2017 16:28*

**+Ray Kholodovsky** Is there a place to put the stl and step files to ? Or should I put it on Thingiverse ?


---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 16:29*

Thingiverse is fine. 


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/gY2EaGk2daQ) &mdash; content and formatting may not be reliable*
