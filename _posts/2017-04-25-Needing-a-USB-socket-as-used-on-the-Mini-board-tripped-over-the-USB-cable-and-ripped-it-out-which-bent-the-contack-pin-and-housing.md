---
layout: post
title: "Needing a USB socket as used on the Mini board- tripped over the USB cable and ripped it out which bent the contack pin and housing"
date: April 25, 2017 17:52
category: "C3D Mini Support"
author: "Steven Whitecoff"
---
Needing a USB socket as used on the Mini board- tripped over the USB cable and ripped it out which bent the contack pin and housing. Straightened things up enough to work but its pretty loose so I need to replace it. Either the OEM one or spec for equivilent to fit the board.





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 17:58*

Generic USB B jack like so: [digikey.com - USB-B1HSW6 On Shore Technology Inc. &#x7c; Connectors, Interconnects &#x7c; DigiKey](https://www.digikey.com/product-detail/en/on-shore-technology-inc/USB-B1HSW6/ED2982-ND/2677743)



Are you comfortable doing this repair or would you like me to? 


---
**Steven Whitecoff** *April 25, 2017 19:04*

Piece of cake, Ray. I have desoldering tools. I will probably just open shell, cut leads/mount pins off from top side of PCB, cut off contacts. Then pull each contact and mount pin out individually so at to make it easy instead of trying to work them all out together(too much chance of excess heat)

.  Then again I've been meaning to buy a cheap reflow over :D


---
**Ashley M. Kirchner [Norym]** *April 25, 2017 19:07*

Even with a reflow, I'd still do it the first way you explained. There's no need to reflow the whole board if all you're fixing is the one connector. Take your time, pull/cut what you can off, particularly the plastic bits, then desolder the pins one by one. Adding the new one in is easy.


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 19:09*

Seconded. Take pics while you're at it.  I have to do this same procedure in a few days and cutting the leads then extracting them one by one was the plan as well. 

And do not reflow, you'll melt the connectors!


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/TXVH4aHH6dC) &mdash; content and formatting may not be reliable*
