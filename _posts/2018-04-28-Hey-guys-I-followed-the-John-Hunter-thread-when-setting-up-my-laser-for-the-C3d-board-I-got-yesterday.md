---
layout: post
title: "Hey guys, I followed the John Hunter thread when setting up my laser for the C3d board I got yesterday"
date: April 28, 2018 03:11
category: "K40 and other Lasers"
author: "Michael Howland"
---
Hey guys, I followed the John Hunter thread when setting up my laser for the C3d board I got yesterday. I have it behaving nicely right now as far as motion control is concerned. I am stuck at the laser PSU wiring. This was a Ruida controlled machine to start with just like in the mentioned thread. My biggest thing is what to do with the water protect circuit. 



Wires on the PSU shown in the pic are as follows. 



Red wire > P2.5

Black Wire > Main PWR Ground

The white wire that jumps is still how it was wired for the RD board. 

I encluded a pic of the old wiring setup. (sorry I am not near a scanner so I took a pic of it)



![images/177c55af80853a526dc28af0fbfcc096.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/177c55af80853a526dc28af0fbfcc096.jpeg)
![images/5eb9e880ae1905c036ab6c044fcdbc2c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5eb9e880ae1905c036ab6c044fcdbc2c.jpeg)
![images/976318d252c412e224d03aa15894d5d8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/976318d252c412e224d03aa15894d5d8.jpeg)
![images/dfa4d41dad97cf41b051dcdf306de081.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dfa4d41dad97cf41b051dcdf306de081.jpeg)

**"Michael Howland"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2018 03:19*

If you touch the red wire to ground does the laser fire? I want to make sure you got the ground from the laser psu in there as well (is this the blue wire going to gnd?) 


---
**Michael Howland** *April 28, 2018 03:30*

Do you mean jump the red wire to ground? On what side PSU? Also there is no blue wire running, however if you are referring to the other thread the yes, except this one is black. I didn't have any blue. :)


---
**Michael Howland** *April 28, 2018 03:32*

Oh another thing. The RD diagram is upside down for some reason. The white "Loop" I was using as my reference point because that was how the other was setup when it was running.




---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2018 03:46*

I meant disconnect the red wire from the Mini and touch it to ground. This is how you test if your "protect" is enabled and the laser will fire on the board's command. 


---
**Michael Howland** *April 28, 2018 03:59*

New Development. Firing up the laser PSU for your test the laser is now continuing to fire. Taking red off the mini to ground stops it firing. 

It was firing with the red wire connected to nothing as well before going to ground.


---
**Ray Kholodovsky (Cohesion3D)** *April 30, 2018 19:37*

Perhaps it is possible that your logic is the other way and it wants 5v instead of ground to fire the laser.  That would be interesting.  Please trace the red wire and see if you can find a label for the other end - something like TL or TH perhaps?


---
**Michael Howland** *April 30, 2018 21:06*

I got it running! Being that I had an extra power supply laying around and the pin-out documentation was so good on the mini I just tried different setups for the PSU. It's a total mess but when I clean up the wiring I'll map the way I go it to work. I did wind up using the laser fire pin in the k40 socket of the mini.


---
**Michael Howland** *April 30, 2018 21:07*

![images/f4081e61b0157524b9697003e7e5d236.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f4081e61b0157524b9697003e7e5d236.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 07, 2018 18:06*

Great!



"I did wind up using the laser fire pin in the k40 socket of the mini" - I would like to confirm what this means, can you send a pic of the final wiring?


---
**Michael Howland** *May 07, 2018 18:10*

Sure thing, it's on my list of things to do along with putting all the wires in their tracks so they aren't hanging all over the place.




---
**Michael Howland** *May 10, 2018 02:07*

Sorry for the delay, here is how I managed to get it going. 



Here is the port allocations from the laser PSU. These are based on the diagram from when the RDC mainboard was connected (See below).

**Note RDC diagram is upside down for some reason.



From top to bottom diagram reads: 5v, IN, GND, WP, TL, TH.

Actual on the laser psu is: TH, TL, WP, GND, IN, 5v



Here are the connections that made it work.



TH- Not used

TL - Connected to "Laser Fire" on mini using the K40 socket.

WP - Connected to lead "from" water sensor.

GND - Creates artifacts if connected anywhere so not connected. (see pic)

IN - Connected to "P 2.5 on Mini" & Lead "to" Water sensor.

5V - Connected to 5V of 24v PSU





![images/7f7216e7da663a29a417bf1e9f066241.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7f7216e7da663a29a417bf1e9f066241.jpeg)


---
**Michael Howland** *May 10, 2018 02:07*

RD Diagram

![images/a16387b22516e09bef6179809571ee00.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a16387b22516e09bef6179809571ee00.jpeg)


---
**Michael Howland** *May 10, 2018 02:08*

Artifact example when GND connected.

![images/166db33fb2ecbb755def6aecf1465dff.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/166db33fb2ecbb755def6aecf1465dff.jpeg)


---
**Michael Howland** *May 10, 2018 02:10*

Pic of Mini

![images/c8144c46aff97caf13ffccbc77c5f83e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8144c46aff97caf13ffccbc77c5f83e.jpeg)


---
**Michael Howland** *May 10, 2018 02:11*

Pic of PSU

![images/d20e188f26f8aa602d33c1b3b4d8af08.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d20e188f26f8aa602d33c1b3b4d8af08.jpeg)


---
*Imported from [Google+](https://plus.google.com/+ErnestHowland/posts/fbiAF75imUC) &mdash; content and formatting may not be reliable*
