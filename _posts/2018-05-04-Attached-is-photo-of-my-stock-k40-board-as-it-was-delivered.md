---
layout: post
title: "Attached is photo of my stock k40 board as it was delivered"
date: May 04, 2018 01:02
category: "C3D Mini Support"
author: "Richard Magnanti"
---
Attached is photo of my stock k40 board as it was delivered.  It works with the stock software. Notice the yellow wire is not the outer wire on one of the plugs. I have installed the mini. When the k40 homes the gantry starts in the correct direction.  If it hits the Y home switch first it stalls (chatters) there.  If it hits the X home switch first it stalls (chatters) there. I have tried reversing the X plug with the red lead inboard as shown in the installation video without success. I’m using LightBurn software. Any suggestions?



![images/3abfd780086824642c763b053047ac5c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3abfd780086824642c763b053047ac5c.jpeg)
![images/f08e2747abd35c42792d34d0e426e472.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f08e2747abd35c42792d34d0e426e472.jpeg)

**"Richard Magnanti"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2018 01:35*

It backs off and touches a second time at a slower speed. This sounds like grinding to a lot of people. Give it a moment. 


---
**Richard Magnanti** *May 04, 2018 03:52*

Success.  Axis plug installer (ME!) mess up.  Ended up the plugs are not oriented like shown in the Cohesion3D instructions. 

![images/1c7817fbb6bc8884a227feb0183e77ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1c7817fbb6bc8884a227feb0183e77ce.jpeg)


---
*Imported from [Google+](https://plus.google.com/107893729797124436040/posts/2raK7J3iPsS) &mdash; content and formatting may not be reliable*
