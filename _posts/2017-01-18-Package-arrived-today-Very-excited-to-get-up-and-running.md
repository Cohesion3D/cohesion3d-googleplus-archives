---
layout: post
title: "Package arrived today. Very excited to get up and running"
date: January 18, 2017 03:46
category: "General Discussion"
author: "Bill Keeter"
---
Package arrived today. Very excited to get up and running. GUYS, don't forget to backup that config file on the SD card before you go off playing! 



Quick question. Are the steppers setup or do I need to adjust the pots? 



Thanks for including that older style GLCD connector for me. Hate that my connectors are backwards on my display. Guess that's what I get for buying a clone.



![images/5a23458fa2810d6c1b13f265017bf8b6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5a23458fa2810d6c1b13f265017bf8b6.jpeg)



**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 03:56*

Sweet!

I definitely can, and probably should, post the default config file on the support site. So no worries there. 

Put heatsinks on drivers!  You may need to adjust them clockwise 45 degrees, up.  Particularly the Y one.  Start with what's there, see if you get any stuttering when you jog or run a job.

The LCDs are all clones.  It's part of why I haven't stocked them myself yet.  That cable flipped thing happens.  


---
**David Komando** *January 18, 2017 21:24*

If you have the link to that C3D Mini - LaserWeb config file. I would greatly appreciate the help.


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 22:24*

Wait, which one? 

There's a config.txt file that comes with the board on the memory card. It configures smoothie to the various K40 parameters. 

There's also a block of settings that need to be put into LaserWeb. But the C3D board just follows commands, it doesn't know anything about LaserWeb, so to speak. 


---
**David Komando** *January 18, 2017 23:47*

Sorry, specifically for LaserWeb. Any guidance would be appreciated. 


---
**Bill Keeter** *January 20, 2017 03:43*

**+Ray Kholodovsky** I'm up and running now! Cut some test circles after using the Laserweb config in **+David Komando** screen shots. 



One question. I noticed my laser was set to a 100% in Laserweb and it's burning at 10 mA. Where's the setting if I want 100% to equal 12mA? That little extra helps with cutting 3mm acrylic. :D


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:45*

**+Bill Keeter** are you set up with the PWM wire replacing the Pot or the one pulsing L way.  I can't keep track of who is doing what.


---
**Bill Keeter** *January 20, 2017 03:54*

**+Ray Kholodovsky** I think whatever is your default setup. I'm just running the 3 prong white wire to the power supply.



here's some pics showing my setup.



![images/72999e4e94c7964ed34d3bd12243df44.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/72999e4e94c7964ed34d3bd12243df44.jpeg)


---
**Bill Keeter** *January 20, 2017 03:54*

![images/ab642db479a4484f8e0c31e7ad9c46e7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ab642db479a4484f8e0c31e7ad9c46e7.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 03:56*

Ok, yeah, the dedicated pwm way. What's the max you were able to reach before when you had the pot in at full blast? 


---
**Bill Keeter** *January 20, 2017 04:00*

not sure, I think about half way got me 12mA. Know i've had it over 20mA before by mistake when turning the dial too far. Maybe 25mA. Been nervous about running it too high. As i've heard the stories on the K40 community about either a tube or power supply breaking at full blast.


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 04:05*

Wow ok, that is high. I think 17mA is the highest I have seen in a while, but my laser has a few hours on it. 

Ok, so it seems like quite a few people have some issue with this 2 wire method. We didn't see any such issues with the testers. But anyways....  So I'm gonna ask you to switch over to the other method which is put the pot back in, remove the pwm cable entirely from the loop, cut  or pop out the contact of the blue wire for L, and connect it to -bed/ 2.5. 

Change config laser pin from 2.4! to 2.5

Consult pinout diagram and other thread comments for more details. ![images/0d4afc69fc7415294d04604ae51b41e4.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0d4afc69fc7415294d04604ae51b41e4.png)


---
**David Komando** *January 20, 2017 04:12*

Wonder if it's power supply related. I have the same setup with a different power supply and was able to control the laser amperage with the LaserWeb percentages. 


---
**Bill Keeter** *January 20, 2017 04:13*

If needed, sure. 



Question though. What's limiting the mA to 10? It's not just a matter of changing a setting? Either bumping LaserWeb to 110% power or something in the smoothing config?


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 04:20*

I'm not sure.  As we've been setting these up on a wider variety of machines, there have been some quirks with interfacing to the pot of these PSUs/ machines.  The 1 wire pulse L method (described above) seems to cure it, as the stock pot remains in play to set the ceiling. 


---
**Bill Keeter** *January 20, 2017 04:21*

**+David Komando** Yeah, I ran the laser at 50% in Laserweb and noticed the mA dropped down to around 5mA. Maybe i'll see if it'll let me run over a 100% if I want to hit 12mA.



Wait in the LaserWeb Settings we all put in PWM Max S value of  "1". Could that be setting it to 10mA?


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 04:22*

No, that 1 value is how it talks to smoothie, and that should be telling the Mini to tell the K40 full power.


---
**Bill Keeter** *January 20, 2017 04:32*

**+Ray Kholodovsky** **+David Komando** SOOOO it turns out LaserWeb allows you to run the laser at more than 100%. Put in 110% for a simple circle and it cut at 12mA. I tried the same thing at 120% and got about 14mA. So that's a simple answer without changing the basic hardware setup for Cohesion3D mini. Score  :D


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 04:34*

I have no idea how that's possible, but ok.


---
**Bill Keeter** *January 21, 2017 19:15*

**+Ray Kholodovsky** i'm having trouble getting the test fire macros setup. I was using the Laserweb tutorial but I think the issue is that I need to run M3 to enable the laser.. then M5 at the end to turn it back off.



Could you maybe share your gcode macro for doing a test fire?


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 19:18*

Type 

fire 50 

Into console. 

Then press the test fire button on the machine. See what happens. 

Alternatively, use the GLCD menu to navigate to laser fire. And again test fire button on laser. 


---
**David Komando** *January 23, 2017 18:20*

So something worth mentioning in this thread is that over the weekend I mounted my Pi3 and C3D Mini into my K40. I then noticed (first time I was really paying attention) that when LaserWeb was set to 100% my laser was hitting 20mA. Setting it to 80% brought it back to the normal 15mA which sounds like the complete opposite of **+Bill Keeter** issue. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 18:22*

Yeah, 15mA is already high enough, don't want to be exceeding that. Keep your tube alive!


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/FL2V2XNAY9c) &mdash; content and formatting may not be reliable*
