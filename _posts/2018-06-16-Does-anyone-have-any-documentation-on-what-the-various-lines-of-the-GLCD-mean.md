---
layout: post
title: "Does anyone have any documentation on what the various lines of the GLCD mean?"
date: June 16, 2018 16:02
category: "C3D Mini Support"
author: "Karl Notyourbusiness"
---
Does anyone have any documentation on what the various lines of the GLCD mean? Lines 1-4 fairly obvious but 5 and 6 not quite gelling with me.



Also, does the Cohesion board maintain a high pitched sound all the time or can it be turned off? Can't be certain the sound is coming from there but that's the only place it seems to be coming from. Thoughts anyone?







**"Karl Notyourbusiness"**

---
---
**Anthony Bolgar** *June 16, 2018 16:33*

It is most likely a configuration iussue with the Cohesion board. Double check config.txt to make sure you have the correct GLCD settings.(Cut and paste from the original C3D config.txt on the Cohesion3D website.




---
**Anthony Bolgar** *June 16, 2018 16:34*

Documentation for the GLCD can be found at [Smoothieware.org](http://Smoothieware.org)




---
**Ray Kholodovsky (Cohesion3D)** *June 16, 2018 16:35*

What board did you get, for what application, and what config files are you using? 


---
**Jim Fong** *June 16, 2018 16:54*

[reprap.org - RepRapDiscount Full Graphic Smart Controller - RepRap](https://reprap.org/wiki/RepRapDiscount_Full_Graphic_Smart_Controller)





The sound you are probably hearing is from the laser power switching power supply.   If you have really good hearing, you may detect the pwm switching noise from the stepper motors/drivers/laser pwm output.   Too many loud concerts/bands have killed my higher frequency hearing so I don’t notice anything. 


---
**Karl Notyourbusiness** *June 17, 2018 19:05*

Jim, not sure on the sound. I don't remember hearing it with the M2 Nano , only from after C3D installed. I have poor hearing so it has to be quite strong for me to hearing.




---
**Karl Notyourbusiness** *June 17, 2018 19:07*

**+Ray Kholodovsky** C3D Mini with laser cutter engraver. Seems to go with laser firing but coming from the board.




---
**Jim Fong** *June 17, 2018 19:28*

**+Karl Notyourbusiness** I think the m2nano board is laser on/off only.  The c3d uses PWM to vary the laser power.  I forget what the default switching frequency is but some can hear it.  It’s normal. 


---
*Imported from [Google+](https://plus.google.com/101870677018387715648/posts/WJjxN888wyQ) &mdash; content and formatting may not be reliable*
