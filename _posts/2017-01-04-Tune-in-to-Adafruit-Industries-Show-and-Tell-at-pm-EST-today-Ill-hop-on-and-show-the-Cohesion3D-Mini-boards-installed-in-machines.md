---
layout: post
title: "Tune in to Adafruit Industries Show and Tell at pm EST today, I'll hop on and show the Cohesion3D Mini boards installed in machines"
date: January 04, 2017 21:24
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Tune in to **+Adafruit Industries** Show and Tell at 
{% include youtubePlayer.html id="aD2rlpCXYhM" %}
[7:30](https://youtu.be/aD2rlpCXYhM?t=7m30s)pm EST today, I'll hop on and show the Cohesion3D Mini boards installed in machines.  Link to watch is here: 
{% include youtubePlayer.html id="aD2rlpCXYhM" %}
[https://youtu.be/aD2rlpCXYhM](https://youtu.be/aD2rlpCXYhM)﻿





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ashley M. Kirchner [Norym]** *January 04, 2017 21:32*

Take a nap before you do that, so you don't end up looking like a zombie. :)


---
**Jonathan Davis (Leo Lion)** *January 04, 2017 21:36*

indeed and i will try to watch it.


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 22:06*

But 3-5am is when all the good productivity happens...![images/9987c71ee8a8b1dd9f422103c468eaa5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9987c71ee8a8b1dd9f422103c468eaa5.jpeg)


---
**Ashley M. Kirchner [Norym]** *January 04, 2017 22:11*

I never said that's wrong. I just suggested you take a nap prior to the live stream.


---
**Ray Kholodovsky (Cohesion3D)** *January 04, 2017 22:12*

I know. I just wanted to state the time and share the picture :)


---
**Jonathan Davis (Leo Lion)** *January 04, 2017 22:23*

**+Ray Kholodovsky** I assume Adafruit is going to be a dealer of your boards? If that is the case Good luck as they have been know to be quite expensive.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/MfFktXfLr5h) &mdash; content and formatting may not be reliable*
