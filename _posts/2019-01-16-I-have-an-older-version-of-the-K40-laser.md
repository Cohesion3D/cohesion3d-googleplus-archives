---
layout: post
title: "I have an older version of the K40 laser"
date: January 16, 2019 06:51
category: "General Discussion"
author: "Adrian Rietdijk"
---
I have an older version of the K40 laser. It has a parallel  computer connection rather than a USB socket. I figured that the C3D laser board would get rid of the parallel connection as well. Problem is: the power socket is a 6-pin socket that does not fit the 4-pin socket on the C3D board. I can measure the voltages coming in on the socket but I am not sure how they are connected on the board. I was wondering if any of the screw connectors on the board can be used for the incoming power rather than the socket and, if so, exactly what the connections are. Thanks in advance.

Adrian R.



![images/027ffff734d7db841d9423eb941318d9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/027ffff734d7db841d9423eb941318d9.jpeg)
![images/783e788ebaedcf3d09307e17c4f25a2e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/783e788ebaedcf3d09307e17c4f25a2e.jpeg)

**"Adrian Rietdijk"**

---
---
**Joe Alexander** *January 16, 2019 10:34*

Which board are you referring to, the C3D Mini or the Laserboard? I know the Laserboard has screw terminals for incoming power near where the 4-pin would be plugged in. Should be marked.(i think the mini has em also just located a little further away iirc)


---
**Ray Kholodovsky (Cohesion3D)** *January 16, 2019 15:30*

We don’t want incoming power, the power brick will power the laserboard. 

We want L for Laser Fire and Ground and these will go to the 4 pin screw terminal next to the power plug on LaserBoard. 


---
**Adrian Rietdijk** *January 19, 2019 10:16*

**+Ray Kholodovsky** 

**+Joe Alexander** 

Thanks for responding to my question. I am referring to the C3D laser board. 

Ray, the Laser Fire and Ground connection normally comes in on a 4-pin socket except in my case, the 4-pin socket is a 6-pin socket. When I turn on the power to the K40 with the laser board disconnected, the voltages on the plug with respect to the most positive wire are >25V, <1V and >20V. All DC. The markings on the board next to the screw terminals are + and - in and one is marked PWM and one is not marked. I assume that the 25V goes to the + and - in but which one goes to PWM? and what does PWM mean anyway?


---
**Adrian Rietdijk** *January 21, 2019 01:31*

**+Ray Kholodovsky** But exactly what wire goes to what terminal? Thanks


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:26*

We only want the wires L (Laser Fire) and Gnd (Laser Ground).  You can read the bottom of the board to see what's what. Here is the red wire (L) hooked up to screw terminal position 4 and Gnd to position 2.  The screw terminal has the same connections as the power plug next to it, to make your life easier. 

![images/9edcb1736733a19e3aac80689bf31797.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9edcb1736733a19e3aac80689bf31797.jpeg)


---
**Adrian Rietdijk** *January 22, 2019 00:11*

**+Ray Kholodovsky** 

Thank you Ray. That is very helpful. Hopefully I will get a chance today to connect it up as per your instructions and give it a go.


---
**Adrian Rietdijk** *January 22, 2019 06:16*

Hi Ray,

The connections on the existing board in the K40 are marked 1 to 8 (although only four of them are actually used). No other markings on the top or the bottom of the board. All the wires are blue. Do you know of another way I can identify the L and ground connections? Perhaps by measuring the voltage. Two of the wires have 25V between them. I am guessing that they are the power connections that we do not need. Would that be correct? The other two connections have about 6 volts with the K40 turned on. would they be the laser and ground connections?


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2019 04:20*

There are schematics available for modern LPSU's but I'm not sure of what you've got there. 


---
*Imported from [Google+](https://plus.google.com/112590888995938365610/posts/P6wXSitDHff) &mdash; content and formatting may not be reliable*
