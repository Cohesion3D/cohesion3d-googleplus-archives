---
layout: post
title: "Calvin Herl you asked about what board to use for 3d printers but I think comments are disabled on that post"
date: August 18, 2017 15:08
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
**+Calvin Herl** you asked about what board to use for 3d printers but I think comments are disabled on that post. 



Either the Mini or the ReMix will run a printer, it depends on how large the heated bed is in terms of amps and how many heads you want to run (multiple extruders). That would determine whether the Mini or the ReMix board would be a better fit. Both are Smoothie. 





**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/MVVkC4jFFf6) &mdash; content and formatting may not be reliable*
