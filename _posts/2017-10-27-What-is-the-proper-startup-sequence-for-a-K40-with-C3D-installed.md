---
layout: post
title: "What is the proper startup sequence for a K40 with C3D installed?"
date: October 27, 2017 15:23
category: "C3D Mini Support"
author: "Alex Raguini"
---
What is the proper startup sequence for a K40 with C3D installed?  If I leave he USB cable unplugged and power the machine up, the GLCD does not display anything but random dots an doesn't stop whistling.  If I turn the machine off and plugin the USB cable the board boots and the GLCD displays everything properly.  I can then power the K40 up without issue. Any ideas?





**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2017 15:27*

This is one of the tricky ones that traces back to how noisy the power supply in the K40 is.  We need a few seconds of clean power to get the GLCD booted up properly. 

But if the buzzer goes off continuously... can you monitor the status LEDs on the Mini board as you turn on the K40 and let me know what you see? I'm talking about the green LEDs L1 - L4 


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2017 15:28*

And pictures of your wiring, etc... are always useful to see if I can spot something unusual. 


---
**Alex Raguini** *October 27, 2017 17:02*

USB Power Only



![images/a8484350a48a2511ef874793a20cbea7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a8484350a48a2511ef874793a20cbea7.jpeg)


---
**Alex Raguini** *October 27, 2017 17:02*

With K40 power turned on.

![images/6fe26c77df7c98b557d26ffa1a95dc58.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6fe26c77df7c98b557d26ffa1a95dc58.jpeg)


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/i1uQJqDDFNP) &mdash; content and formatting may not be reliable*
