---
layout: post
title: "cd3dmini K40 or lw4 or corel i have a scaling problem on imported svg from corel they are about 10% to short x and y is this what i change in the config.txt Is there a gcode file i could use to check the setup scaling # Arm"
date: March 26, 2017 16:06
category: "C3D Mini Support"
author: "Don Sommer"
---
cd3dmini K40  or lw4  or corel i have a scaling  problem on imported svg  from corel  they are about 10% to short x and y  is this what i change in the config.txt



Is there a gcode file i could use to check the setup scaling



 # Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions

alpha_steps_per_mm                           157.575          # Steps per mm for alpha stepper

beta_steps_per_mm                            157.575          # Steps per mm for beta stepper

gamma_steps_per_mm                           1600             # Steps per mm for gamma stepper







**"Don Sommer"**

---
---
**Lance Ward** *March 26, 2017 17:14*

I had same issue.  Change SVG import pixels per inch to 90 in LW4 file settings.  Fixed it for me.  I don't think Corel  has a setting for this during export (at least that I've found).


---
**Don Sommer** *March 27, 2017 20:53*

i did some experimenting  jogging 10m across the area manually firing the laser its exacty 10mm   drew a known size bitmap and its ok just svg import  i noticed that when  change and save dpi it changes back to 96  the file didnt change  i also have some js errors listed 






---
**Don Sommer** *March 29, 2017 02:46*

lw4 didnt save any settings the file stayed the same??i uninstalled lw4 and reinstalled and now it works something was corrupt


---
*Imported from [Google+](https://plus.google.com/102954017085080987074/posts/U1PnkVxXPig) &mdash; content and formatting may not be reliable*
