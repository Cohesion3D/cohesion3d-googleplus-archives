---
layout: post
title: "Update #2: The assembler made the 2 samples of the Cohesion3D Mini and just shipped them to me for further testing"
date: December 17, 2016 03:02
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Update #2:

The assembler made the 2 samples of the Cohesion3D Mini and just shipped them to me for further testing. 

Here is the pic that was sent over. 

With the holidays, shipping might be a bit spotty.  

Roughly speaking though, I expect to have the package here sometime this week, and I'll get one of the boards over to **+Joe Spanier** right away for testing. 

Then the assembler needs 1 week for assembly of the rest of the batch, testing, and all that.  Then shipping the batch over here. 

We're right on the 3 week mark now, and for anyone that ordered right on day 1 (Thanksgiving/ Black Friday) we're going to be shipping right on the 6 week mark (barring any delays).  We'll keep fingers crossed for Fedex to get the samples here quickly and for everything else to stay on track as well. 

Memory cards are here and motor drivers should be arriving soon. 



Cheers to all, 

-Ray

![images/169224b48c5c61d41e65c47583cc0454.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/169224b48c5c61d41e65c47583cc0454.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jonathan Davis (Leo Lion)** *December 17, 2016 03:04*

Sounds like everything is Going along well> Plus I will do what I can to write up a Good install manual For Benbox based systems systems


---
**Joe Spanier** *December 17, 2016 03:44*

Hopefully it comes better than the tablet I ordered from BestBuy last week. It came from Ohio, UPS lost it in a depot 2 hours from me in IL for 5 days. Today it showed up in Utah and is supposedly gonna take a week to get to me. So much for 2 day shipping. 



Also. Stoked for new laserWeb toys. Cant wait to work on it.


---
**Jonathan Davis (Leo Lion)** *December 17, 2016 03:46*

**+Joe Spanier** Yeah if its a Insigna good luck as i have heard form review that they suck.


---
**Joe Spanier** *December 17, 2016 04:10*

Nah new samsung tab with s-pen. Supposedly pretty awesome. Just unposssible to buy right now, hence my frustration with it disappearing.


---
**Jonathan Davis (Leo Lion)** *December 17, 2016 04:11*

**+Joe Spanier** Understandable,  and usually samsungs are a bit overpriced in terms of specs.


---
**Andy Shilling** *December 18, 2016 22:39*

Man I need this like yesterday lol. My second Moshi board died today leaving me with nothing left to work with and orders coming still popping up for xmas. I can't wait to get this awful board out and start using some real software.




---
**Jonathan Davis (Leo Lion)** *December 18, 2016 22:41*

**+Andy Shilling** Amazon.com and overnight shipping.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/Kvko3XXjbmb) &mdash; content and formatting may not be reliable*
