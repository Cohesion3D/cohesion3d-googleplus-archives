---
layout: post
title: "What do I need to do to hook this up to my K40 laser cutter?"
date: April 15, 2017 03:34
category: "K40 and other Lasers"
author: "Sean Cherven"
---
What do I need to do to hook this up to my K40 laser cutter?



![images/7ededb63889362d876300a6356a86ea7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7ededb63889362d876300a6356a86ea7.jpeg)
![images/37d6ef8eb6e018b519e459e5a79e35d0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/37d6ef8eb6e018b519e459e5a79e35d0.jpeg)

**"Sean Cherven"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 03:45*

The Cohesion3D Mini for Laser Bundle supports this wiring orientation, please see this thread in addition to the Documentation site guide (also pinned to the top of this community)



[plus.google.com - C3d Mini – Setup My laser machine has 2 Model numbers 1st 1 is KH40W from ven...](https://plus.google.com/u/0/117786858532335568822/posts/TfiJPuYAdAn)


---
**Sean Cherven** *April 15, 2017 13:08*

It looks like the power pin is too big for the Cohesion3D Mini. It uses 4-pin, mine has 6-pin.



I can buy a replacement 4-pin connector, but what way should I wire it?


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 15:40*

Nah you can just run wires to screw terminals on the mini. Figure out what wires are 24v, Gnd, and L from in there. 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 15:45*

24v, gnd, and L all have screw terminals on the Mini for you to put the wires in (you can cut the 6 pin connector off or run new wires from the screw terminals on the psu). L goes to 2.5 - bed which is the 4th from the left bottom screw terminal. 

The 24v and gnd are top left. 



Please get acquainted with the pinout diagram for Mini on the docs site. 


---
**Sean Cherven** *April 15, 2017 15:46*

Okay, and how do I wire the pwm?


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 15:47*

Leave the cable that came with the bundle out. You just need to hook up L and the power as described just now above. The pot on the k40 stays in play. 


---
**Sean Cherven** *April 15, 2017 15:48*

So how is the laser power controlled then?


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 15:49*

Via L, it is pulsed. L is the fire signal. 


---
**Sean Cherven** *April 15, 2017 16:04*

Oh, so it pulses the fire signal instead of using pwm? How effective is this vs. pwm? 


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 16:06*

Very. 


---
**Sean Cherven** *April 15, 2017 16:10*

Okay thanks!

If I bought a 4 pin connector, would I be able to use the connector? Or do I have to cut the wire and use the bed mosfet?


---
**Ray Kholodovsky (Cohesion3D)** *April 15, 2017 16:13*

You could. Or you could even reposition the pins to the proper order within the 6 pin. **+Andy Shilling** might be able to help with that. 

Again, important that you consult the pinout diagram. 


---
**Sean Cherven** *April 15, 2017 16:17*

Okay. I may actually have some of those 4-pin connectors. I'll have to look in my parts drawers later.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/YfL6SedaPVS) &mdash; content and formatting may not be reliable*
