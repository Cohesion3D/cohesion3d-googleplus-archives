---
layout: post
title: "EverytimeI connect laserweb to the C3D mini I get the error in the photo"
date: February 06, 2017 04:43
category: "Laser Web"
author: "Tony Sobczak"
---
EverytimeI connect laserweb to the C3D mini I get the error in the photo. Everything seems to work ok. Laser, X & Y axis.

![images/48b77dfd8cfe97d9cc1c5490d924a06d.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/48b77dfd8cfe97d9cc1c5490d924a06d.png)



**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 04:44*

Yeah just press the green button. I don't know why this happens. I will try to replicate it. 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 04:55*

**+Peter van der Walt** quite familiar with halt state :) or :( depending on how you look at it. 

More specifically, I am not aware of why people are reporting seeing the halt at power up, job start, and or job end. 

My immediate thought was some wonky character in the start/ end gCode fields/ in LW comms init routines when hitting connect. 

I really don't have info to make statements that are not 100% speculation on my part. 


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 16:09*

The Mini Laser Bundle comes with CNC firmware on the card. So after the first power up the boards are on CNC firmware. 

I will investigate further. 

**+Tony Sobczak** please make a backup of your settings file in LW (settings --> last tab --> backup) and the config file on the microsd card. Email to info (at) cohesion3d (dot) com. I will try to replicate. 


---
**Colin Rowe** *February 06, 2017 22:21*

Yes I keep getting this, thought it was because I put M30 at end of prog but still does it even after i removed it (random and not every time)




---
**Colin Rowe** *February 06, 2017 22:38*

Is that the command window that has a load of scrolling data on?


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 22:39*

Yes, Peter is referring to the terminal window. See if there is anything else in there when the halt occurs.


---
**Colin Rowe** *February 06, 2017 22:40*

👍


---
**Tony Sobczak** *February 06, 2017 23:22*

**+Ray Kholodovsky** Check your email. 


---
**Colin Rowe** *February 20, 2017 11:22*

![images/b536e9cf780323384e38431c339d4993.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b536e9cf780323384e38431c339d4993.jpeg)


---
**Colin Rowe** *February 20, 2017 11:23*

![images/07b44644cc0b599d981123a541795147.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/07b44644cc0b599d981123a541795147.jpeg)


---
**Colin Rowe** *February 20, 2017 11:23*

![images/f55ee5396efac6e0fa0cb7704ab3bd2e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f55ee5396efac6e0fa0cb7704ab3bd2e.jpeg)


---
**Tony Sobczak** *February 21, 2017 00:13*

**+Ray Kholodovsky**  did you get a chance to look at the files I emailed you?


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2017 00:14*

Been sick, Tony. Just getting back into things now, hopefully. I did get your email...


---
**Claudio Prezzi** *February 21, 2017 15:58*

The screenshots tell me, that the alarm is received before we get the firmware version. Could it be, that the board was already in that state before the connection? From the last shutdown?


---
**Claudio Prezzi** *February 21, 2017 16:01*

Or is it just that smoothie tells you to home your machine, like Grbl does?


---
**Colin Rowe** *February 21, 2017 16:02*

I will home it before I connect and see what it does then.


---
**Claudio Prezzi** *February 21, 2017 16:12*

Ok, found. After opening the serial connection we send ctrl-x to reset boards that are not connected through USB, but via serial RX/TX. This causes smoothie to go into "HALTED" alarm mode.


---
**Claudio Prezzi** *February 21, 2017 16:17*

I don't know why smoothie does that even when the planner is empty. Other firmwares dont go into alarm on reset when nothing is running.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/YjSc1DooT92) &mdash; content and formatting may not be reliable*
