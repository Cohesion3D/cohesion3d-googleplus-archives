---
layout: post
title: "Maker Faire! Back home after setup and lots of fun"
date: September 23, 2017 02:37
category: "General Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Maker Faire! 



Back home after setup and lots of fun. We're in zone 2 by the printers all the way in the far left corner. 3 doors down from Lulzbot :) 



![images/342705aafc800effe3ff3f1379f805f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/342705aafc800effe3ff3f1379f805f4.jpeg)
![images/1ba75dfd9478d89713b708d574f92504.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1ba75dfd9478d89713b708d574f92504.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Travis Sawyer** *September 23, 2017 17:57*

Great to meet you today, Ray!!!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/72HsrWEXgwq) &mdash; content and formatting may not be reliable*
