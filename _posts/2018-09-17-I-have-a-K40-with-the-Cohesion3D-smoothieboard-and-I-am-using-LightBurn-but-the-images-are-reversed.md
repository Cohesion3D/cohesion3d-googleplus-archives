---
layout: post
title: "I have a K40 with the Cohesion3D smoothieboard and I am using LightBurn but the images are reversed"
date: September 17, 2018 03:49
category: "K40 and other Lasers"
author: "Timothy Lathen"
---
I have a K40 with the Cohesion3D smoothieboard and I am using LightBurn but the images are reversed. If I mirror the image in lightburn it comes out correct but it's kinda confusing. Any help?







**"Timothy Lathen"**

---
---
**James Rivera** *September 17, 2018 04:59*

Sounds like some stepper wires are reversed.


---
**Tech Bravo (Tech BravoTN)** *September 17, 2018 12:34*

Try this [https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)

[lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Timothy Lathen** *September 18, 2018 03:11*

Thanks guys for your comments however it was working correctly until I updated the firmware on the Cohesion3D smoothieboard and updated the lightburn software. I don't think it's the steppers as that would have been the case before and it wasn't 


---
**James Rivera** *September 18, 2018 05:08*

**+Timothy Lathen** I doubt it was the update, at least not by itself, or other people would have problems, too (unless there is a LightBurn setting you changed). Therefore, it would seem more likely that you may have a Smoothieware setting wrong. 


---
**Timothy Lathen** *September 22, 2018 00:09*

yeah I think a setting in Lightburn may have changed after the update maybe I did have the steppers backwards and then changed something. The only thing I can think of that I changed was the origin. I really don't know. 

 


---
**Timothy Lathen** *September 22, 2018 00:14*

lol Yeah the first vid was the answer most likely. My bad 

Just curious why would changing the origin change the horizontal stepper to a mirror image. 

  


---
*Imported from [Google+](https://plus.google.com/+TimothyLathen/posts/jkWHtHRanWY) &mdash; content and formatting may not be reliable*
