---
layout: post
title: "I have received my C3d Mini and I installed it my k40"
date: May 11, 2017 16:18
category: "K40 and other Lasers"
author: "David Cantrell"
---
I have received my C3d Mini and I installed it my k40. Next I will need to make sure the mirrors are aligned. I have some questions that have come up.



1. I have searched for a screen mount panel but none of my searches have turned up anything. Could you point me in the right direction to find a dxf for a panel to mount the screen?



2. Do the buttons (laser on, test), amp meter, and amp knob continue to function in the same way as they did before the upgrade?



3. I have downloaded Laserweb4 and I am wondering what other types of software people use for driving a laser. I am wanting to learn several software packages in case to end up getting to use other people's lasers, I want to already know the software. 





**"David Cantrell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 16:29*

Hi David.

1. There are quite a panel designs from people in this community, perhaps some can weigh in here or a new post may be in order here/ in the k40 community with that specific question.  I too get asked this often and would like to have a comprehensive list to point to.

2. Yes, the test fire button will still work as long as the white cable included with the kit was not wired - this is for legacy applications only.  You will use the pot to set the current ceiling and then the % value in LaserWeb will be a proportion of that.  10mA on the pot is a good place to start.

3. There is also Visicut.  For cutting, you can adapt any CNC CAM that generates gCode such as Vectric Aspire or Fusion360.  For engraving, LaserWeb is king.  


---
**Ashley M. Kirchner [Norym]** *May 11, 2017 18:13*

Regarding #1: There's no "set" design as we all custom make it. In my case, I removed the pot (legacy wiring) and added 2 temperature gauges, so my panel looks like this for the time being. Eventually it will be redone as I remove the temp gauges and add a single LCD in their place.

![images/3d7c512eb1b6d8ef10f5c1bd002a5b23.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d7c512eb1b6d8ef10f5c1bd002a5b23.jpeg)


---
**Jim Fong** *May 11, 2017 22:07*

**+Ray Kholodovsky** Vectric stuff works well for gcode for milling but doesn't play well with smoothieboard laser.   Smoothie requires a 0-1 "S" value for laser power output.   Vectric post processor only outputs Integer S values greater than 1.  There isn't a way to change that.  You can always manually edit the gcode but what fun would that be.  






---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 22:30*

I have a post processor that works. Email me. 


---
**Jim Fong** *May 11, 2017 23:08*

**+Ray Kholodovsky** I actually don't own anything by Vectric yet.  I was playing around with the evaluation copy before I buy.   If I try to enter a value such as .5 for 50% power as a spindle value. The post procesessor will round up to 1 when it outputs the gcode.   The PP PDF handbook says spindle speeds has to be integer values.  Since I couldn't get it to work, I never ended up buying it.  


---
**Ray Kholodovsky (Cohesion3D)** *May 11, 2017 23:10*

Dunno, I modified the postprocessor from inventables and it does what I want.   It translates 0-100% in the Feedrate input to 0-1 with 2 decimal places in the gcode ouput.  It's all configurable in the PP.


---
**Jim Fong** *May 11, 2017 23:17*

**+Ray Kholodovsky** ok have to try again.  I'll email you.  Might be a issue with eval version maybe. 


---
*Imported from [Google+](https://plus.google.com/107911974344505492651/posts/BTyxuzkVe5J) &mdash; content and formatting may not be reliable*
