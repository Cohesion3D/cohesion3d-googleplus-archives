---
layout: post
title: "I have a C3D mini in my K40 and I want to find out if it will do some things I want"
date: July 11, 2017 19:22
category: "C3D Mini Support"
author: "Cris Hawkins"
---
I have a C3D mini in my K40 and I want to find out if it will do some things I want.



I would like to add switches like a CNC machine for Cycle Start, Cycle Hold, and Stop. I plan to use my laser as a cutter rather than etching or engraving, so the gcode will be on the SD card. This way I don't need a computer at the machine.



This leads me to the following questions:



1. Does the C3D mini support an external SD card reader?

The reader on my GLCD doesn't work.



2. Where does the C3D mini get 5 volts?

At this point in my testing, I have not yet connected it to the laser power supply and I have been powering it from the USB connector (it's not connected to a computer). Will the laser power supply provide 5 volts to the board? The pinout diagram indicates the 5 volt pin is not connected.



3. Can I add switches for the functions mentioned above?

I know Smoothieware supports switches, but it looks like I'll have to steal some unused I/O from the C3D mini for them.



Also FYI - I think I found an error in the Pinout Diagram. The GLCD header lists SCK1, MISO1 and MOSI1 implying it uses SPI1 when it uses SPI0 in the config file.





**"Cris Hawkins"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2017 19:37*

1.  The official stance of the smoothie folks is that SPI over a cable is noisy and unreliable - I had the external sd socket wired on the ReMix board and found it unusable.  As such, the Mini GLCD Adapter does not even wire the SD Socket.   That said, I heard some people had luck with using a MicroSD extender going to the main MicroSD Socket on the board.  If you can use the internal socket, great.  If not, you can spend a few bucks on an extender and see if that works for you.



2.  There's a 5v regulator on the Mini and the header for it is lower left corner.  I think we're thinking the same thing: [cohesion3d.freshdesk.com - Wiring a Z Table and Rotary with External Stepper Drivers : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000739904-wiring-a-z-table-and-rotary-with-external-stepper-drivers)



3.  I'm not 100% sure regarding that terminology.  We have a kill button on the board with a header to use an external one, this can be remapped for play/ pause.  That's about all I know of.  Perhaps the smoothieware docs site can be of more help. 



The pinout diagram will be useful in locating 3 additional spare GPIO pins you can use for various things: XMX, YMX, and the pin on the servo header.  



Smoothieware article named switch explains configuring functions. 



One additional tidbit is the on_boot.gcode file - you can have this on the card and it will run on startup/ reset.  



Re: SPI, actually no.  Electrical SPI channel 0 is for the SD cards, electrical channel 1 is for the GLCD data (this is the one on the glcd header).  But I think it's the other way around for smoothie config purposes.  


---
**Cris Hawkins** *July 11, 2017 20:34*

Ray,



I am aware that SPI can be unreliable, so I was not surprised it wasn't enabled. I now know that it isn't even wired in the hardware (also not surprising). Thanks.



It seems I was not clear about my 5 volt question. The only voltage sources I can determine is the 24 volts from the laser power supply and 5 volts from USB. I am wondering if I need to provide 5 volts via the USB if there is no computer attached or if 5 volts is already available from another source.



Do I need to provide 5 volts to the C3D mini board if it is NOT connected to a computer via USB?



I am aware of "Switch" in Smoothieware and you have confirmed what I suspected. I can use various unused GPIO pins as you suggest.



Now that you clarified the SPI 1 or 0 issue, I remember seeing something similar with some Atmel products I have used. The logic begins counting at 0, but in the verbal description, it starts at 1....


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2017 20:50*

5v:

You do not need to have USB connected.

There is a switching regulator that creates 5v from the 24v input.  

A way to confirm this is to not have USB connected and turn on the 24v power.  The red LED indicates the 24v in, and you should be seeing the green LEDs doing stuff which indicates brain activity.



The way the circuit works is that the 5v from the buck and the 5v from the USB each go through a barrier diode MBRA210LT3 this creates a common 5v pool from either, or both, input sources, and prevents backfeed. 



The 5v aux header is also tapped into the common pool, as such it is critical not to attempt powering the board from there, and only use it for a peripheral such as a fan or the + of the optos on external drivers. 



The buck is rated for 5v 2a, and the inductor is 850mA rated, so 850mA is the limit for all the logic and braining, the GLCD, and any additional peripherals being added.  





Regarding SPI, you're right.  The SD Card is on channel 1 and and GLCD on channel 0.  <b>However</b> the net names (this is taken from smoothieboard design files) call them out as SD_MISO and MISO1, and we based the pinout diagram off the latter designation.  So you're right, but I have an excuse and I'd rather explain it once every few months than change the diagram :)




---
**Cris Hawkins** *July 11, 2017 21:00*

Thanks for the thorough explanation!



It is good that you mention not powering the board through the aux header. I don't know that I would, but it's good to know I shouldn't.




---
*Imported from [Google+](https://plus.google.com/112522955867663026366/posts/KW48YjZzDeb) &mdash; content and formatting may not be reliable*
