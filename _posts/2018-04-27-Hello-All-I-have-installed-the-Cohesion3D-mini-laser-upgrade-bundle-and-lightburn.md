---
layout: post
title: "Hello All, I have installed the Cohesion3D mini laser upgrade bundle and lightburn"
date: April 27, 2018 05:46
category: "C3D Mini Support"
author: "Colin Davies"
---
Hello All,

I have installed the Cohesion3D mini laser upgrade bundle and lightburn. I have set Lightburn to Smoothie and it communicates with the K40. If I try  to cut however, the path is followed but it does not fire the laser. The laser fires from the button on the machine and I have aligned the mirrors. Any hints as to fault finding please?





**"Colin Davies"**

---
---
**Kieth Wallace** *April 27, 2018 11:10*

Did you set the power levels under the cut tab in lightburn?


---
**Colin Davies** *April 27, 2018 12:26*

Hi Keith, does this look right?

![images/72e3169a1d6e100531b906c8d88ff92d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/72e3169a1d6e100531b906c8d88ff92d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 27, 2018 18:55*

Fairly thorough debugging for this issue in the comments here: [facebook.com - Log into Facebook &#x7c; Facebook](https://www.facebook.com/groups/1045892858887191/permalink/1114795435330266/)



This is the Cohesion3D Users group on Fb. 


---
**Colin Davies** *April 28, 2018 07:25*

Thanks Ray, I'll follow that set of instructions.


---
*Imported from [Google+](https://plus.google.com/+ColinDavies/posts/fDWmecQZPVv) &mdash; content and formatting may not be reliable*
