---
layout: post
title: "Ray Kholodovsky Jim Fong First test if everything fits inside my control cabinet"
date: April 10, 2017 18:27
category: "Show and Tell"
author: "Marc Pentenrieder"
---
**+Ray Kholodovsky**​ **+Jim Fong**​ 

First test if everything fits inside my control cabinet. Still waiting for some more parts.



![images/c6c7f3c2466a054e20dd92df62cb0576.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c6c7f3c2466a054e20dd92df62cb0576.jpeg)
![images/1fba479b8ab19230022f52999ac04aa7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1fba479b8ab19230022f52999ac04aa7.jpeg)
![images/b665dc7576baaee94efaa3b916423a7d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b665dc7576baaee94efaa3b916423a7d.jpeg)

**"Marc Pentenrieder"**

---
---
**ALFAHOBBIES** *April 10, 2017 19:40*

What are you controlling with it?


---
**Marc Pentenrieder** *April 10, 2017 19:41*

A big 3D printer ;-) 


---
**Marc Pentenrieder** *April 30, 2017 09:41*

Why can't I see my new comments ? They just disappear after posting ;-(


---
*Imported from [Google+](https://plus.google.com/+MarcPentenrieder/posts/YbQZKaM68gc) &mdash; content and formatting may not be reliable*
