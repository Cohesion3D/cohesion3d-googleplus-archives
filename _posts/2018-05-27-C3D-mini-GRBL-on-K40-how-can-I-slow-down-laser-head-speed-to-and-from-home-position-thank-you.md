---
layout: post
title: "C3D mini, GRBL on K40, how can I slow down laser head speed to and from home position, thank you"
date: May 27, 2018 08:01
category: "C3D Mini Support"
author: "Gwilym Hughes"
---
C3D mini, GRBL on K40, how can I slow down laser head speed to and from home position, thank you. 





**"Gwilym Hughes"**

---
---
**Kelly Burns** *May 27, 2018 11:42*

Homing speed is a specific setting,  $24 (feed in mm/mm) and $25 (seek in mm/min) control the speed for homing.  Jogging speed is usually controlled in the software.  Overall maximum speed limit is controlled with $110,$111,$112 (x,y,z).  These are also in mm/min.  


---
**Gwilym Hughes** *May 27, 2018 12:38*

**+Kelly Burns** Thanks for that,




---
*Imported from [Google+](https://plus.google.com/111834960287465398533/posts/JAHN7ZoGZw6) &mdash; content and formatting may not be reliable*
