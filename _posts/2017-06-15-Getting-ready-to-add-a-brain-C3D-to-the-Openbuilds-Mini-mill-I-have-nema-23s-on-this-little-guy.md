---
layout: post
title: "Getting ready to add a brain (C3D) to the Openbuilds Mini mill, I have nema 23's on this little guy..."
date: June 15, 2017 15:22
category: "General Discussion"
author: "Alex Krause"
---
Getting ready to add a brain (C3D) to the Openbuilds Mini mill, I have nema 23's on this little guy... Should I go with A4988's or Drv8825's or another type of external stepper motor... Has anyone set up a CNC to run nema 23 stepper motors?

![images/a541f0a667550cf7391cd1f76c29cbfe.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a541f0a667550cf7391cd1f76c29cbfe.jpeg)



**"Alex Krause"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 15:24*

DRV's are going to run hot, cool them well and give it a shot. I still think you might need external drivers (like TB6600 or the one openbuilds carry) 


---
**Griffin Paquette** *June 15, 2017 15:49*

You need external drivers for nema 23's. Even 8825's aren't gonna be enough if you need real torque. 


---
**Anthony Bolgar** *June 15, 2017 17:54*

The DQ542MA drivers on openbuilds would be great for this mill.


---
**Alex Krause** *June 15, 2017 18:03*

**+Anthony Bolgar**​ if I had the space funds to get 100$ worth of drivers I would... But I have kinda overspent my hobby budget for the next couple of months getting all the supplies to do resin casting... I am going to get the tb6600's for now then upgrade after I have sold some of my things made from the resin


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 18:04*

Alex I have Tb6600's here if you're getting a board. Otherwise eBay :)


---
**Alex Krause** *June 15, 2017 18:05*

**+Ray Kholodovsky**​ I had some amazon coupons to use up so I picked up a set of 3 for 30$ after coupons used 


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2017 18:06*

Cool!


---
**Jim Fong** *June 15, 2017 19:05*

Currently testing the updated Laserweb4 rotary functions.  The rotary has a 3amp nema23.  I didn't want to bother hooking up one of my external gecko stepper drivers (lazy!) to the Cohesion3d board and I'm all out of a4988 drivers, I just plugged in a spare drv8825. 



I'm not going to get full stepper torque but it's enough to use the rotary without any issues i.e. for testing purposes. 



The TB6600 are not my favorite but they will work just fine.  Much better than the tb6560's. 




---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/DBSrDz4NbcD) &mdash; content and formatting may not be reliable*
