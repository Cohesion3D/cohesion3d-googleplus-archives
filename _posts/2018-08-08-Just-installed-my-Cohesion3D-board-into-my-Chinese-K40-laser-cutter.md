---
layout: post
title: "Just installed my Cohesion3D board into my Chinese K40 laser cutter"
date: August 08, 2018 01:30
category: "K40 and other Lasers"
author: "Frank Broadway"
---
Just installed my Cohesion3D board into my Chinese K40 laser cutter. My K40 was working perfectly before I installed the Cohesion3D board.



I am having a problem homing the laser on start up.  I start up the software, it detects the laser cutter and starts to move the head to the home position which is the rear left. Once it gets there it keeps trying to move the X axis which keeps banging into the Y rail. After around 10 seconds the X axis motor stops. The only command I seem to be able to give the laser is home and that just repeats the problem. I do not think the optical end stops are bad since it was working fine before the board swap.



The Console messages are

Waiting for connection...

Homing

Smoothie

ok

[Caution: Unlocked]

ok

ALARM: Homing fail



I saw two post by other members with the same problem but no solution was posted for this problem.





**"Frank Broadway"**

---
---
**Tech Bravo (Tech BravoTN)** *August 08, 2018 02:14*

[lasergods.com - Cohesion3D Mini Origin, Stepper Cable Verification, & Home When Complete](https://www.lasergods.com/cohesion3d-mini-origin-stepper-cable-verification-home-when-complete/)


---
**Frank Broadway** *August 08, 2018 02:41*

**+Tech Bravo** I'm a subscriber to your channel and have watched this video before I did my install. I will rewatch and double check my setup.


---
**Ray Kholodovsky (Cohesion3D)** *August 08, 2018 02:47*

A video would help, I can imagine it but would be good to spot anything extra weird if you can make a video of the crashy thingy...



Also pictures of your machine and wiring please. 



The last person I remember having this issue ended up having plugged the X motor into the Y socket and vice-versa. 


---
**Roberto Fernandez** *August 08, 2018 10:35*

Homing fault; can be that in the firmware you have Max travel for the homing in some axe. If the switch is not activate before the end of this travel the homing is in fault.


---
**Frank Broadway** *August 09, 2018 01:11*

**+Ray Kholodovsky** Here is a photo of my board 

![images/51b2e703500689f1d7e2a13b46bff44d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/51b2e703500689f1d7e2a13b46bff44d.jpeg)


---
**Frank Broadway** *August 09, 2018 01:17*

**+Ray Kholodovsky** Here is the video 


{% include youtubePlayer.html id="MuskUrJevoQ" %}
[youtube.com - YouTube](https://youtu.be/MuskUrJevoQ)


---
**Frank Broadway** *August 09, 2018 01:20*

**+Ray Kholodovsky** I have check my connections multiple times. I also reviewed my device settings in lightburn and everything looks properly setup.


---
**Frank Broadway** *August 09, 2018 01:21*

**+Tech Bravo** I re-watched your video and rechecked my wiring. Nothing solved my problem. I posted a photo and video below.


---
**Frank Broadway** *August 09, 2018 01:24*

**+Roberto Fernandez** I have not changed anything in the firmware. How would I check for Max travel?


---
**Roberto Fernandez** *August 09, 2018 15:27*

**+Frank Broadway** here is part of my config file. 

# Endstops

endstops_enable                              true             # the endstop module is enabled by default and can be disabled here

corexy_homing                                false             # set to true if homing on a hbit or corexy

alpha_min_endstop                            1.24^            # add a ! to invert if endstop is NO connected to ground

alpha_max_endstop                            1.25^           # NOTE set to nc if this is not installed

alpha_homing_direction                       home_to_min      # or set to home_to_max and set alpha_max

alpha_min                                    0                # this gets loaded after homing when home_to_min is set

alpha_max                                    330              # this gets loaded after homing when home_to_max is set

beta_min_endstop                             1.26^!            #

beta_max_endstop                             1.27^!            #

beta_homing_direction                        home_to_min      #

beta_min                                     0                #

beta_max                                     265              #

#gamma_min_endstop                           1.28^           #

gamma_max_endstop                            1.29^           #

gamma_homing_direction                       home_to_max      # home_to_min

gamma_min                                    0                #

gamma_max                                    55              #



alpha_max_travel                             330              # Max travel in mm for alpha/X axis when homing

beta_max_travel                              265              # Max travel in mm for beta/Y axis when homing

gamma_max_travel                             55              # Max travel in mm for gamma/Z axis when homing





# optional order in which axis will home, default is they all home at the same time,

# if this is set it will force each axis to home one at a time in the specified order

homing_order                                 ZXY              # x axis followed by y then z last



# optional enable limit switches, actions will stop if any enabled limit switch is triggered

alpha_limit_enable                          true            # set to true to enable X min and max limit switches

beta_limit_enable                           true            # set to true to enable Y min and max limit switches

gamma_limit_enable                          true            # set to true to enable Z min and max limit switches



alpha_fast_homing_rate_mm_s                  50               # feedrates in mm/second

beta_fast_homing_rate_mm_s                   50               # "

gamma_fast_homing_rate_mm_s                  4                # "

alpha_slow_homing_rate_mm_s                  25               # "

beta_slow_homing_rate_mm_s                   25               # "

gamma_slow_homing_rate_mm_s                  2                # "



alpha_homing_retract_mm                      3                # distance in mm

beta_homing_retract_mm                       3                # "

gamma_homing_retract_mm                      3                # "



#endstop_debounce_count                      100              # uncomment if you get noise on your endstops, default is 100



# optional Z probe

zprobe.enable                                true           # set to true to enable a zprobe

zprobe.probe_pin                            1.28^!          # pin probe is attached to if NC remove the !

zprobe.slow_feedrate                         5               # mm/sec probe feed rate

#zprobe.debounce_count                       100             # set if noisy

zprobe.fast_feedrate                         100             # move feedrate mm/sec

zprobe.probe_height                          60              # how much above bed to start probe 5

gamma_min_endstop                            nc              # normally 1.28^ Change to nc to prevent conflict,




---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 15:32*

Have you modified the config at all? If so, reload the original [config.txt](http://config.txt) file and test for proper operation. If it works correctly let me know what parameters you need to change. If its just bed size a have a cheat sheet for that


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 15:34*

From the pic it looks like there is no harness for endstops connected and just one stepper motor is plugged in. Is that correct? If so, we need a pic with it connected as it was when the problem is occuring.


---
**Frank Broadway** *August 09, 2018 16:48*

**+Tech Bravo** I have not modified any files. the ribbon cable is the for the X-motor and the Y-motor is the other cable is the Y-motor. The endstops are optical and connect to a small board at the Y-motor.


---
**Frank Broadway** *August 09, 2018 16:53*

Here is the photo I took moments before I installed the Cohesion3D board. The K40 was operating perfectly with the old board.

![images/e9a84328215fb31fb5af0116c82e48ce.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e9a84328215fb31fb5af0116c82e48ce.jpeg)


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 16:57*

Was the other pic you posted (of the cohesion3d) just they way it was when it was failing or did you disconnect things then take the pic?


---
**Frank Broadway** *August 09, 2018 17:04*

**+Tech Bravo** The Photo I posted earlier is the board wire as it should be according to the install guide.


---
**Frank Broadway** *August 09, 2018 17:05*

**+Tech Bravo** There is nothing else to attach to the C3d board on my K40.


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 17:07*

okay. i see :) wow i looked again. i must have missed the ribbon cable the first time. hmmmm...


---
**Frank Broadway** *August 09, 2018 19:42*

**+Tech Bravo** I guess I will need to swap the optical endstops for mechanical. 


---
**Tech Bravo (Tech BravoTN)** *August 09, 2018 20:11*

Well the thing is there is a daughter board for the optical endstops that make the output for the ribbon identical to mechanical stops. Mechanical and optical work with the nano and the c3d the same way. I don't think that would help. Here is more on the optical endstops: [donsthings.blogspot.com - K40 Optical Endstops](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html?m=1)


---
**Tech Bravo (Tech BravoTN)** *August 10, 2018 01:10*

This may help: [lasergods.com - Testing K40 Endstops With LightBurn](https://www.lasergods.com/testing-k40-endstops-with-lightburn/)


---
**Ray Kholodovsky (Cohesion3D)** *August 10, 2018 13:56*

Don’t swap anything. Use the M119 command to see if the endstops are being read. 


---
**Frank Broadway** *August 12, 2018 19:39*

I was able to diagnose and solve my problem using M119 and the article **+Tech Bravo** linked above. My end stop was working but the piece of metal underneath the carriage was to high to properly trigger the end stop. I was able to use a piece of tape to extend it lower. Now the endstop is triggered every time. ![images/db71d9f063ccd6d10c255a532890a52e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/db71d9f063ccd6d10c255a532890a52e.jpeg)


---
**Tech Bravo (Tech BravoTN)** *August 12, 2018 19:41*

Im glad you found it! Good luck and have fun with you laser :)


---
**Frank Broadway** *August 12, 2018 20:36*

**+Tech Bravo** Do you know the solution to my problem in this other post? 

[plus.google.com - When I command the laser to move using Lightburn the X-axis moves twice as fa...](https://plus.google.com/+FrankBroadway/posts/HQFq2wfw8NU)


---
*Imported from [Google+](https://plus.google.com/+FrankBroadway/posts/XMLpKTNudgw) &mdash; content and formatting may not be reliable*
