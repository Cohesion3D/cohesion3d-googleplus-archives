---
layout: post
title: "Trouble with z probe sensor. Here is what I have"
date: October 02, 2017 07:51
category: "C3D Remix Support"
author: "Alex Hayden"
---
Trouble with z probe sensor.

Here is what I have. LJ 12A3-4-Z/BX  NPN NO

Wired it the way the schematic shows. 

+ Brown

- Blue

Signal​ Black



I have tried other wire configurations as well. Nothing works. I think the firmware is set correctly (see blue dots in photo). I get a forward voltage to the signal wire of 4.7v with the switch socket 5v which actually measures 4.7v also. There is no change in forward voltage when you trigger the sensor with metal. The LED barely comes on (shown in picture with sensor mounting nut off).



I have also tried powering the + wire for the sensor with 12v and the forward voltage to signal is only 3.1v. If switch is triggered with metal LED lights up (shown in picture with sensor mounting nut on) and there is no change in forward voltage, still 3.1v on signal. 



So what am I doing wrong? FYI when sending z home command nothing happens, while when sending z move commands z moves. 



Do I need to use a different sensor? 

LJ 12A3-4-Z/BY  PNP NO

This is the sensor that is used on printrbots.



![images/3687f7ab337e4630fb9ba49b4403a3cb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3687f7ab337e4630fb9ba49b4403a3cb.jpeg)
![images/18872a37dd695a9d0556aeefefc1c06e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/18872a37dd695a9d0556aeefefc1c06e.jpeg)
![images/af5a85adea902d65d5c1868a8a7eae38.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/af5a85adea902d65d5c1868a8a7eae38.jpeg)
![images/82407313168b41313ab62606191fe314.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/82407313168b41313ab62606191fe314.jpeg)
![images/d098efb21c34f0a6b15be2b3103de3cb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d098efb21c34f0a6b15be2b3103de3cb.jpeg)
![images/d29805b91a668bca2d7b12a140a867f0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d29805b91a668bca2d7b12a140a867f0.jpeg)
![images/72f7bcdd490b87b0bf2c7197f70692e3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/72f7bcdd490b87b0bf2c7197f70692e3.jpeg)

**"Alex Hayden"**

---
---
**Alex Hayden** *October 02, 2017 08:05*

**+Ray Kholodovsky**​ isn't this sensor the same as the one pictured on your website for the remix board? Specificly the [remix-peripherals.jpg](http://remix-peripherals.jpg) 



Did you get your sensor to work? How do I get this sensor working? 


---
**Noel Kuck** *October 02, 2017 11:18*

What is your bed material?


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2017 14:13*

Let's start with the electrical diagnostic only. If you power your sensor from +12v and Gnd and with the signal wire disconnected from the board, when you meter the signal wire voltage, you should see 0v and 12v when triggered and not triggered. 



Let's start there. If that's not the case then something is unusual with that sensor. 



Fast forward: once in a blue moon one of these guys doesn't have enough oomf to flip my diode on the Remix endstop circuit. Then the workaround is to manually create a voltage divider with 2 resistors and feed it to the servo signal pin 2.4. 

I had to do this recently when a SN-04 green sensor was not reading on the printrbot. But it still read 12v and 0v on the meter. 


---
**Alex Hayden** *October 02, 2017 15:54*

**+Ray Kholodovsky**​​ thanks. One of the sensors was not acting the way you said, so I switched it out with the other one and got what you said. 12v when not triggered and 0.4v when triggered. 



So I have a bad sensor. Guess that one is trash. Tried hooking back to the board. And movement still wont home and wont stop the movement when triggered. So I guess something in my firmware is messed up?


---
**Alex Hayden** *October 02, 2017 15:57*

**+Noel Kuck** I am just diagnosing the problem so I am using my crimpers and they are steel.


---
**Alex Hayden** *October 03, 2017 11:55*

**+Ray Kholodovsky**​ tired moving to the 2.4 pin like you said. I might have the voltage drop too high. Used a 100k and 150k. Still nothing triggered when movements were active. Going to try a 24v power supply tonight.


---
**Ray Kholodovsky (Cohesion3D)** *October 03, 2017 13:14*

You'll need to do a voltage measurement to see what the output is. 

Did you change the pin in config? Are you using M119 to read? 


---
**Alex Hayden** *October 06, 2017 20:51*

**+Ray Kholodovsky**​ I am trying this again at 24v on the zmin 1.28 pin. I am getting 24v when not triggered and 0v when triggered. Do I need to turn of the ! That inverts the pin? 


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 21:14*

Please use M119 to read the actual state that the board is reading.  Should be 0 when not triggered and 1 when triggered.  If it is flipped, then invert. 


---
**Alex Hayden** *October 06, 2017 21:17*

Ok so with that I get 0 for both cases.


---
**Alex Hayden** *October 06, 2017 21:19*

And if I remove the ! I get 1 for probe not triggered and still get 1 while probe is triggered.


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 21:20*

So then your sensor is one of the unique ones and you should use a voltage divider and connect to 2.4. 


---
**Alex Hayden** *October 06, 2017 21:27*

I tried that also still got nothing different.


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 21:27*

Please meter the output voltages of your divider when the sensor is triggered and not. 


---
**Alex Hayden** *October 06, 2017 21:28*

Should I try a different sensor style?


---
**Alex Hayden** *October 06, 2017 21:29*

Triggered it reads 0.7v and not reads 23v.


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 21:31*

The OUTPUT of the divider that's connected to 2.4? If that was the case your board would be fried. 


---
**Alex Hayden** *October 06, 2017 21:39*

I am sorry I must be miss understanding you. Or perhaps I have fried the board. When my sensor is connected to 1.28 pin and 24v and gnd. The 1.28 pin to gnd measures 24v when sensor is not triggered. And 0.7v when it is triggered.


---
**Alex Hayden** *October 06, 2017 21:40*

Similar measurements for pin 2.4 and gnd.


---
**Alex Hayden** *October 06, 2017 21:40*

Everything else on the board still works so I doubt it is fried.


---
**Alex Hayden** *October 06, 2017 21:44*

When testing 2.4 I add 2 resistors a 100k and a 150k. 100k jumps 2.4 pin to ground and 150k is wired in series to 2.4pin. This is similar to what Tom did in his video about how to wire sensors, just used larger values. 


---
**Alex Hayden** *October 06, 2017 21:46*

![images/1c95d1429b3e9570412926d675136baa.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1c95d1429b3e9570412926d675136baa.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 06, 2017 21:46*

Misunderstanding, it seems. 



Clarifying some things from earlier:

Sometimes a particular sensor will not trip our circuit (but the board will be fine, it just won't read the sensor, as it seems is happening here). 



To overcome this, you must use 2 resistors to create an external voltage divider circuit to drop 24v down to ~ 3.3v and hook this up directly to a gpio pin, such as the 2.4 pin available.



Please google "voltage divider inductive sensor" and read the first handful of results.  You should also be able to find a voltage divider calculator that will help you select the 2 resistor values to drop 24v down to 3.3v  The range is somewhat flexible, you really just need to end up 2-3v to be super safe.  The pin can take 5v, but I prefer that 2-3v output of the divider. 


---
**Kenneth McCall** *October 22, 2017 20:57*

Personally, I would use a Zener "regulator". Just a 100K resistor and  4.7 v Zener. [https://www.tinkercad.com/things/2RVOHx2g43h-grand-hango/](https://www.tinkercad.com/things/2RVOHx2g43h-grand-hango/)


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/Wa2aWLN1Qa6) &mdash; content and formatting may not be reliable*
