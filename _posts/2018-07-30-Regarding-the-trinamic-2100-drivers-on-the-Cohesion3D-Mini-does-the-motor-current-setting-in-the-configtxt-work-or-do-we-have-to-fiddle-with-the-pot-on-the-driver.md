---
layout: post
title: "Regarding the trinamic 2100 drivers on the Cohesion3D Mini, does the motor current setting in the config.txt work, or do we have to fiddle with the pot on the driver?"
date: July 30, 2018 04:50
category: "C3D Mini Support"
author: "Michael Davidson"
---
Regarding the trinamic 2100 drivers on the Cohesion3D Mini, does the motor current setting in the config.txt work, or do we have to fiddle with the pot on the driver?  





**"Michael Davidson"**

---
---
**Griffin Paquette** *July 30, 2018 11:21*

You will have to set the reference voltage via the individual potentiometers on the top of your stepper drivers. The C3D mini does not have an onboard digipot. 


---
**Ray Kholodovsky (Cohesion3D)** *July 30, 2018 13:36*

Also, the TMC2100 is not “smart” so it is not possible to configure them over SPI. 


---
*Imported from [Google+](https://plus.google.com/+MichaelDavidson68spc/posts/18UXbNVWPrP) &mdash; content and formatting may not be reliable*
