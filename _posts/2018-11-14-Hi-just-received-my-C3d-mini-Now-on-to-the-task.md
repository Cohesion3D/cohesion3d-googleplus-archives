---
layout: post
title: "Hi, just received my C3d mini:-) Now on to the task"
date: November 14, 2018 00:09
category: "C3D Mini Support"
author: "Em W"
---
Hi, just received my C3d mini:-)  Now on to the task. Convert FS Gen 5. 



Looking at the [https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram](https://cohesion3d.freshdesk.com/support/solutions/articles/5000721601-cohesion3d-mini-pinout-diagram)



QUESTION: Just want to know what pin/s are equal to A-,A,B-,B going from Left to Right on the C3d link provided for the X and Y axis:



X pin1 = <i>____</i>, X pin 2 = <i>___</i>, X pin 3 = <i>___</i>, X pin 4 = <i>____</i>



Y pin1 = <i>____</i>, Y  pin 2 = <i>___</i>, Y  pin 3 = <i>___</i>, Y  pin 4 = <i>____</i>



Copy/paste and fill in the <i>_____</i> variable if that makes it easier. Thank you in advance. My brain seems fixated on knowing simply what pin is equal to A-,A,B-,B. 



Reference just in case: I have Nema sterppers 0.9 degree LDO-42STH33-1334MB, number of teeth = 20, pitch = P=2.032, Pitch Diameter: _12.936mm



My current board [https://www.emsplace.com/fsl-gen5/index.html#/view/ID2488546](https://www.emsplace.com/fsl-gen5/index.html#/view/ID2488546) has the X (right side)  and Y just to the left. The other blocks are limit switches and power items. The steppers are wired as A- = blue, A = red, B- = black, B = green. X and Y values are the same, but the Y block is mirrored (for example the A- blue wire on X is on the block/s right, but on Y the A- blue wire is on the block's left.)









**"Em W"**

---
---
**Kelly Burns** *November 14, 2018 00:28*

Ok,  you can’t play with the laser until you eat all your vegetables.  


---
**Em W** *November 14, 2018 00:43*

**+Kelly Burns** Hunh:-) ?


---
**Kelly Burns** *November 14, 2018 01:09*

This is the wiring using the markings that you have.  You should be able to use this along with Ray's image to convert to the correct wiring.



Not sure what the spacing is, but it looks to me like you should be use the plugs as is.  With the connectors on C3D you can simply flip the plug if it moves in the wrong direction or invert the direction in the Firmware config. 

 

![images/7b9c1a64755a571595f5c339e6745070.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7b9c1a64755a571595f5c339e6745070.jpeg)


---
**Kelly Burns** *November 14, 2018 01:10*

**+Em W** You said talk to you like you are Six years old


---
**Em W** *November 14, 2018 01:57*

TY Kelly +Ray Kholodovsky  but still not computing the pin Q. Going to edit my post above since the way I asked my Q seems to confusing. Hope that will be better:-)




---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2018 02:29*

Just crimp the pins and put them in the same order as they were on the original connector. You can flip the motor in config later. 


---
**Em W** *November 14, 2018 03:16*

**+Ray Kholodovsky** OK, if you could quickly look at this image and give me an OK - same order and how I am thinking about your reply in visual form. And would this way of wiring below still need to "flip the motor in config later"? Thank you.

[emsplace.com](https://www.emsplace.com/fsl-gen5/images/stepper.jpg)


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2018 03:29*

Em, I can't do this level of hand holding.  Just do it.  As far as not killing the board, I mainly need you to not plug in power backwards and always have power off when you fiddle with wiring. 


---
**Joe Alexander** *November 14, 2018 03:35*

if you get the motor wiring wrong it will just not rotate, it wont kill the control board. a multimeter can be used to check continuity of the wires for the pairs a and b. if they are connected its a pair, put those wires in either a or b side. if its running backwards to the commands sent the config file can be changed very easily or the connector can be rotated 180°.




---
**LightBurn Software** *November 14, 2018 21:59*

The simplest way to find pairs is just pick two, twist them (or just hold them) together, try to turn the motor. If it resists, you’ve found a coil pair.  Then when you run everything, if the motor goes the wrong way, swap the connectors for one of the pairs.


---
**Em W** *November 14, 2018 23:22*

**+LightBurn Software** Hi truly appreciate the reply.  I actually know what the pairs are from the Nema 17 stepper schematics I have - which is black/green is a pair and red/blue is a pair (bipolar). What I was trying to convey was that my gen 5 shows them going black B-, green B, and red A, blue A- into my current board (marked as such on the board). 



However, on the C3d pin out diagram I linked, they use these designations: 1B, 1A, 2A, 2B looking from left to right. I surmise that 1 B/A is one pair, and 2 A/B is the other pair? Would still like to know if in the C3d lingo which letter, A or B, is considered the positive "+" (unless it is the number that denotes the positive). Once I knew that, then I would have felt more comfortable wiring the C3d board correctly the first time around.



Basically just X (for example where X=B- on my board) is which number/letter on the C3d board as being the + or <s>. But I don't know how to ask a simple question, well... simply:</s>) BTW, I also purchased Lightburn as part of the package. Thank you!



PS Tech Bravo provided a great Gen 5 schematic that pretty much shows my current state of affairs before any C3d conversion [lasergods.com - FSL Gen.5 Hobby Laser Schematic](https://lasergods.com/fsl-gen-5-hobby-laser-unofficial-schematic/) 


---
**LightBurn Software** *November 14, 2018 23:26*

I normally just wing it - It’s trivial to change, and honestly the labeling is wrong about 1 of 5 times, so even if you get it right, you don’t. :)


---
**Kelly Burns** *November 14, 2018 23:37*

You are Making this way more complicated than it needs to be. You just need to hook up the pairs.  + and - don’t matter.  That’s why many motor schematics don’t use them. 



If you get pairs wrong, the only risk is that the motor won’t turn. Literally, the worst that can happen is motor runs backward.  As Ray said that’s either firmware config change or flip of the connector.  






---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/TGEk4bjmUmU) &mdash; content and formatting may not be reliable*
