---
layout: post
title: "I received my C3D laser upgrade yesterday, and am installing it today"
date: July 10, 2017 02:05
category: "General Discussion"
author: "Cris Hawkins"
---
I received my C3D laser upgrade yesterday, and am installing it today. I got the C3D mini after having problems getting a Re-ARM board to work properly.



I am using external stepper drivers (I'm not a fan of the Pololu style drivers and I have a dozen external drivers).



After dealing with some wiring errors, the X and Y axes WORK GREAT! I have not gotten to the laser control yet, so that's next.



The errors I had were Step and Dir signals swapped on the Y axis and an open circuit on my X axis endstop. Once those issues were corrected the axes both operate as expected. If I were using the stock C3D Drivers and hadn't messed with the endstop wiring for my Re-ARM installation, everything would have worked right out of the box!



Thanks Ray for a nice upgrade for the K40 laser! The documentation and installation is straightforward and painless. Great product!



P.S. Now that I have a working system, it may be of help to find out why the Re-ARM didn't work properly (I have a few other motion control projects)....





**"Cris Hawkins"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 02:07*

Glad to hear it Chris! 

Please do post pics of the setup, I want to see what electronics you've got going there. 


---
**Jim Fong** *July 10, 2017 02:16*

I'm running/testing a re-arm with grbl-lpc/laserweb. There are some pin mappings that are slightly different from smoothieboard/c3d.  Board works good but I'd rather have a second c3d instead. Need to order another soon. 



I never was a big fan of pololu drivers either. I use a bunch of Geckodrives and other higher end drivers. However the a4988 drivers have been rock solid on the c3d board. I haven't had a bad laser job gone wrong due to missing steps yet.  


---
**Cris Hawkins** *July 10, 2017 02:47*

Last century I was the entire mechanical engineering department for the first two years of Compumotor's existance. If you are unfamiliar with Compumotor, they were the first company to sell micro-stepped stepper motors and drivers.



I have about a dozen Compumotor ZETA drivers, and I'm using a couple of them for the laser. They are overkill for these little motors, but they get a lot out of them. One of the reasons I like these drivers is that they don't require a separate power supply for the motors like Geckos do.



When I get the laser running, I'll post pictures.



Thanks Jim, grbl-lpc is a good idea! I think I'll try it on the Re-ARM....


---
**Jim Fong** *July 10, 2017 02:52*

**+Cris Hawkins** I'm using a pair of Parker E-DC stepper drivers on the re-arm. I believe they bought Compumotor or something like that. I still have a few Compumotor servo drivers/motors laying around. 


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 02:53*

I think you guys are gonna get along great. 


---
**Cris Hawkins** *July 10, 2017 03:09*

Yeah, Parker Hannifin bought Compumotor a while back.



I am currently building a custom assembly machine for a big instrument company. It uses six axes controlled by two ATmega328s with grbl. I'm using another ATmega to coordinate the other two.



So grbl is a program I am familiar with, but I only recently discovered grbl-lpc. It will be fun to play with....



Thanks Ray, I hope to get along great with many. There is something unique with motion control folks.


---
**Steve Clark** *July 10, 2017 16:18*

**+Cris Hawkins**  Hi Cris, Steve Clark here ...Just saying hello...Its been a few years...


---
**Cris Hawkins** *July 10, 2017 18:43*

I've seen the name Steve Clark on some posts, but I figured that there are probably a million guys with that name. I didn't even consider it would be the Steve Clark I knew a few decades ago!



Hi Steve! Thanks for posting....


---
**Steve Clark** *July 10, 2017 19:22*

Yep, I thought the same on your name until I looked at the spelling! Small world. Looking forward to seeing  your build.


---
**Cris Hawkins** *July 11, 2017 01:20*

Here is a photo of my test setup. I mounted my C3D board so the SD card is on top for easier access. I connected jumpers to direct the Step and Dir outputs from the Pololu style connectors to output the signals and grounds to the four pin header normally intended for the motor phase outputs. Those four pins are then connected to my external driver Step and Dir pins.

![images/6f5dd37db87f84d92694959d8b24a921.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6f5dd37db87f84d92694959d8b24a921.png)


---
**Cris Hawkins** *July 11, 2017 01:28*

After confirming all the connections were correct, I made this simple adapter PCB on my CNC Bridgeport to take the place of the jumpers.

![images/32395a77e1e83465737374e62ad5d38e.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/32395a77e1e83465737374e62ad5d38e.png)


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2017 01:32*

Reroute signals to output lines. I've got a design like that. 


---
**Cris Hawkins** *July 11, 2017 01:35*

Here is the board installed in its proper place in the laser cabinet. It includes the adapter shown above. Also note I am using the endstop inputs on the left side of the board instead of the dedicated connector for the stock laser connector at the end of the board.

![images/c8466f8dd9074c9a9697820451f99f10.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8466f8dd9074c9a9697820451f99f10.png)


---
*Imported from [Google+](https://plus.google.com/112522955867663026366/posts/UoK7AGB5BaU) &mdash; content and formatting may not be reliable*
