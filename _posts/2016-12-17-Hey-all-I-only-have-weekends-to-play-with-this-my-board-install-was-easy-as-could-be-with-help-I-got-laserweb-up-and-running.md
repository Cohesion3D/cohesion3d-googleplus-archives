---
layout: post
title: "Hey all! I only have weekends to play with this, my board install was easy as could be, with help I got laserweb up and running"
date: December 17, 2016 16:08
category: "General Discussion"
author: "rick jarvis"
---
Hey all! I only have weekends to play with this, my board install was easy as could be, with help I got laserweb up and running. I'm not savy at all with software so I'm learning that. I've got a problem I could use help with please (as I only have the weekends) 

I can get machine to connect and x & y to jog , but I cannot get the laser to fire... Any help with what I've done wrong?  Thanks for the help

![images/e75e884bd45d2a117127e8466ea40abf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e75e884bd45d2a117127e8466ea40abf.jpeg)



**"rick jarvis"**

---
---
**Carl Fisher** *December 19, 2016 01:18*

Which control panel do you have? Can you show a picture of the lid with all of the controls? There are a few possible solutions but they will depend on the exact iteration of your machine.




---
**rick jarvis** *December 19, 2016 01:25*

The cohesion mini.

Ray helped me get it going. We couldn't get it running pwm even though the voltages where as he expected them to be. We did get it going a different way though


---
**Carl Fisher** *December 19, 2016 01:27*

Unfortunately that's not what I meant. I mean do you have the machine with an analog ammeter and pot or do you have the digital control board? Depending on how the stock machine is wired, there may be a few things to try different on the power supply wiring but I don't want to suggest anything until I see what machine iteration you have.






---
**Ned Hill** *December 19, 2016 03:32*

**+Carl Fisher**I looked back at his prior postings and he has the analog version.


---
**Carl Fisher** *December 19, 2016 16:46*

Ok, so I assume X and Y travel are fine but you're just not getting laser fire correct? I assume you left the hardware laser enable switch in place and that it's working properly? 



If you run the following command, do you get movement but no laser? 



M3 G1 X100 F6000 S0.5 M5



If you still have the old test button in place, which it looks like you do based on your wiring of the PSU, click the enable the laser with the switch and then go back to X0 and run the same command again but press and hold the test fire button, do you get any laser output then?


---
**rick jarvis** *January 03, 2017 13:18*

im sorry i have not seen these post... my apologies.. it is the analog version


---
*Imported from [Google+](https://plus.google.com/105230811165966980201/posts/RzH2SRrNgLG) &mdash; content and formatting may not be reliable*
