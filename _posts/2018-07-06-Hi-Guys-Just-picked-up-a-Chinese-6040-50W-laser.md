---
layout: post
title: "Hi Guys! Just picked up a Chinese 6040 50W laser"
date: July 06, 2018 09:29
category: "General Discussion"
author: "Luke Prattley"
---
Hi Guys!



Just picked up a Chinese 6040 50W laser.



I am looking to convert to C3D, but have afew questions.



First of all:



-I have attached pictures of two PCB's. one is the standard control board, can somebody confirm that the other is an external driver of some description for the Z axis up/down?



Secondly:



-I have attached pictures of the current front panel. After fitting a C3D board, will the buttons for Z up and down and the analog current Pot still be required, or am I better off to handle all of this straight to the C3D Board? if so, what will I need to purchase in the way of drivers/converters to do so??



Looking forward to making the conversion, just want to check up on some things before going for it!



Looking forward to becoming part of the C3D Community!



![images/e3772f7310dd81512b800dc439417bae.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e3772f7310dd81512b800dc439417bae.jpeg)
![images/a02c18e394aa0545fc0bc374ce1b7692.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a02c18e394aa0545fc0bc374ce1b7692.jpeg)
![images/d54cd45f39fa405af129e125153da278.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d54cd45f39fa405af129e125153da278.jpeg)
![images/6599976acc5c8413784198645b60f30b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6599976acc5c8413784198645b60f30b.jpeg)

**"Luke Prattley"**

---
---
**Don Kleinschnitz Jr.** *July 06, 2018 13:56*

Hard to tell: Looks like a stepper driver but cannot see the part # on the IC's clear enough to look them up.

What do the two cables connect to?


---
*Imported from [Google+](https://plus.google.com/104306411116306910187/posts/6c319hSD2Eh) &mdash; content and formatting may not be reliable*
