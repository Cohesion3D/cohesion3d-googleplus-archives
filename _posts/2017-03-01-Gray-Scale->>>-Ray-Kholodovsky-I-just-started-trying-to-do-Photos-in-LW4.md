---
layout: post
title: "Gray Scale-->>> Ray Kholodovsky . I just started trying to do Photo's in LW4"
date: March 01, 2017 19:54
category: "FirmWare and Config."
author: "Tony Sobczak"
---
Gray Scale-->>>  **+Ray Kholodovsky** .  I just started trying to do Photo's in LW4. I have my Mini set up as in Carl's instructions and I too Have no gray scale reference  **+Andreas Benestad**'s post. Carl's instructions never mentioned the L wire.  Should that be connected between the PSU and the Mini or disconnected?  Or does it go to to the MSOFet?  I'ver tried changing laser_module_minimum_power to .1 and laser_module_pwm_period to 200 and 400 with no change.  There is no pot in my setup and the center wire from PWM 2.4 is going to the In on the PSU.  Assistance will be GREATLY appreciated.





**"Tony Sobczak"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2017 20:05*

You should do the other way. Which is plug the pot back in, don't run that white cable I included with the board, and connect the wire from L on the PSU to 2.5 bed - 



Then config change laser pin from 2.4! to 2.5

Save, rest board, etc. 

Set pot to 10mA. 

Try G1 lines with various S values. 


---
**Tony Sobczak** *March 01, 2017 20:59*

Thanks I'll try that as soon as I get home.


---
**Tony Sobczak** *March 02, 2017 18:42*

I've tried doing those commands without response from the engraver.

M3

G1 X10 S0.2 F600



Gets nothing no head movement.  So what am I doing wrong?



Pot is back in the circuit.  It's set to 10ma at 100% power.  Config is as follows:



## Laser module configuration

laser_module_enable                           true

laser_module_pin                              2.5

laser_module_maximum_power                    0.8

laser_module_minimum_power                    0.0

#laser_module_default_power                   0.8

laser_module_pwm_period                       200



switch.laserfire.enable               true

switch.laserfire.output_pin  	2.6

switch.laserfire.output_type     digital

switch.laserfire.input_on_command   M3

switch.laserfire.input_off_command   M5



L from psu to P2.5






---
**Ray Kholodovsky (Cohesion3D)** *March 02, 2017 18:47*

Try the X value as 0 or 20. 


---
**Tony Sobczak** *March 02, 2017 19:29*

This is what I get left is 0-80% and light 400, dark 250. Right 0-60% same speeds.

Almost there. 



![images/8897bf11bee607d6c1195886ec45aed3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8897bf11bee607d6c1195886ec45aed3.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 04:36*

Nice looks like you're getting there! 

What is the pot set as with these? (I check by doing a test fire and looking at the mA gauge)


---
**Tony Sobczak** *March 03, 2017 05:17*

10ma. Looking at the meter current is ranging between 2-5 ma during the Engraving. 


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 05:20*

10mA is good. I was playing with a bunny with the pot set at 5mA yesterday. 

Mdf chars easy...![images/4be1b400c9156f6bc805ec3f82ae50da.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4be1b400c9156f6bc805ec3f82ae50da.jpeg)


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/7xQ4T8UNGcX) &mdash; content and formatting may not be reliable*
