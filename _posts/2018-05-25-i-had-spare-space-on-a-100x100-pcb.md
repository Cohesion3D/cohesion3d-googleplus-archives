---
layout: post
title: "i had spare space on a 100x100 pcb.."
date: May 25, 2018 01:44
category: "Show and Tell"
author: "ekaggrat singh kalsi"
---
i had spare space on a 100x100 pcb.. so added the cor3d from github.. lets see if i can solder it and get it to work..! 

![images/3c94fe9a51276bbcaca5ac68debfd532.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3c94fe9a51276bbcaca5ac68debfd532.jpeg)



**"ekaggrat singh kalsi"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 02:09*

Oooh. 



Check if FET1- makes continuity to the large pad. I screw that up on one rev. Or maybe it was FE2-



I actually didn’t use Names but Values as my silkscreen, so that’s why it looks bad. 



I see soldermask between the QFP pads so doing that by hand shouldn’t be too bad. 2 years ago I did 40 Remix’s and 40 Mini’s by hand. 


---
**ekaggrat singh kalsi** *May 25, 2018 02:13*

the brd files seems ok..

![images/2d6ca88969506097e8a17a3b91be1e55.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d6ca88969506097e8a17a3b91be1e55.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 02:39*

Oh good! 


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 02:41*

**+Joe Spanier** got one of first ones of these. 



And **+Brian Bland** helped build a few by hand when I couldn’t keep up. He’ll remember the wire bypass to fix that FET issue. 


---
**ekaggrat singh kalsi** *May 25, 2018 02:43*

lets see when i put it together.. i dont mind bypassing mistakes with jumper wires. it is for a experimental gus simpson  which is running marlin right now


---
**Griffin Paquette** *May 25, 2018 03:37*

That style mini was my first C3D board. Good stuff!


---
**Brian Bland** *May 25, 2018 05:01*

It was FET1. ![images/5cd5aceac2a42dadc80edecc86957b65.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5cd5aceac2a42dadc80edecc86957b65.png)


---
*Imported from [Google+](https://plus.google.com/+ekaggrat/posts/YqceUjmFnZE) &mdash; content and formatting may not be reliable*
