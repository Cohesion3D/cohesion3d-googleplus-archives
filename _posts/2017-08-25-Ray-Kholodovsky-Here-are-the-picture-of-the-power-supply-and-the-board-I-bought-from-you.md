---
layout: post
title: "Ray Kholodovsky Here are the picture of the power supply and the board I bought from you"
date: August 25, 2017 01:18
category: "General Discussion"
author: "Kim Clayton"
---
Ray Kholodovsky

Here are the picture of the power supply and the board I bought from you



![images/1266312ea2eb2c76b9f72e1099ac267d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1266312ea2eb2c76b9f72e1099ac267d.jpeg)
![images/8fa00f853a54ffe427257a0ca6197b48.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8fa00f853a54ffe427257a0ca6197b48.jpeg)
![images/01fb3f535187d0bcf5691f5e35b982ca.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/01fb3f535187d0bcf5691f5e35b982ca.jpeg)
![images/7d86a6c7957ad6a954e385762c5f0424.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7d86a6c7957ad6a954e385762c5f0424.jpeg)

**"Kim Clayton"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 01:28*

Hi Kim.  Do you remember you sent me better pics of the red board on fb earlier? Where I can clearly see the connector on the board?  Please post that one in the comments here. 


---
**Kim Clayton** *August 25, 2017 01:28*

![images/fa1a70bb3240cc3cabfafb2e209edadc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fa1a70bb3240cc3cabfafb2e209edadc.jpeg)


---
**Kim Clayton** *August 25, 2017 01:29*

![images/700dc9e516b66e3402f2e70fc08e0b25.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/700dc9e516b66e3402f2e70fc08e0b25.jpeg)


---
**Kim Clayton** *August 25, 2017 01:29*

![images/9bed85a48bc5d8d90823f758982a0cf4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9bed85a48bc5d8d90823f758982a0cf4.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 01:32*

I need a direct shot of the board so I can read the labeling by the connector. 


---
**Kim Clayton** *August 25, 2017 01:35*

Here's the pictures I took




---
**Kim Clayton** *August 25, 2017 01:49*

![images/3d26ea6b94186e29dcbc6ae437984e29.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3d26ea6b94186e29dcbc6ae437984e29.jpeg)


---
**Kim Clayton** *August 25, 2017 01:51*

![images/e52af9217cdafe380045692a3963bfc4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e52af9217cdafe380045692a3963bfc4.jpeg)


---
**Kim Clayton** *August 25, 2017 01:52*

I took more pictures hoping this helps.




---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 03:21*

Ok, I can barely make out the writing next to that 6 pin power connector in your last pic now, but I still need to see that shot (perhaps from more of a left angle) with the cable plugged in so that I can advise you based on the wire colors. 


---
**Kim Clayton** *August 25, 2017 03:22*

Ok I'm going to take more pictures right now




---
**Kim Clayton** *August 25, 2017 03:26*

![images/0b30b523d362acb40fed2d2fba9d75f8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0b30b523d362acb40fed2d2fba9d75f8.jpeg)


---
**Kim Clayton** *August 25, 2017 03:26*

![images/4749fc843e1cf8f5ec86e1c2c2452683.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4749fc843e1cf8f5ec86e1c2c2452683.jpeg)


---
**Kim Clayton** *August 25, 2017 03:27*

![images/6fb8f5fd8154f9ab5b68c8fccc660323.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6fb8f5fd8154f9ab5b68c8fccc660323.jpeg)


---
**Kim Clayton** *August 25, 2017 16:35*

Any ideas?




---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 16:37*

Still waiting to see pictures of the red board with the wires plugged in, as per my last reply.  I need that clear view of the connector with the cable plugged into it.


---
**Kim Clayton** *August 25, 2017 17:31*

I"m trying to get my husband to take pictures since I'm at my office and the board is at home.




---
**Kim Clayton** *August 25, 2017 23:33*

![images/d6e47c67615689f7a259f3931c74dc6a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d6e47c67615689f7a259f3931c74dc6a.jpeg)


---
**Kim Clayton** *August 25, 2017 23:34*

![images/9e8c1b2bc3c4c86782e2372835dae190.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9e8c1b2bc3c4c86782e2372835dae190.jpeg)


---
**Kim Clayton** *August 25, 2017 23:35*

Here are the other pictures of the original board with the parts plugged in.


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2017 23:41*

Perfect, thank you. 



Looks like: 

Green wire is Ground. 

Red wire is +24v 

Yellow wire is Laser or "L"



Follow the instructions as normal for the ribbon cable variant (it's at the bottom). 



When you get to the power cable step, 



+24v and Ground go to the C3D Mini "Main Power In" screw terminal at the top left and L goes to "2.5-" which is the 4th screw terminal from the left on the bottom row of the Mini. 

There's also a Mini pinout diagram on the Documentation site to assist you with locating what's what. 


---
*Imported from [Google+](https://plus.google.com/105548492328480652648/posts/6ofpxLbkU8c) &mdash; content and formatting may not be reliable*
