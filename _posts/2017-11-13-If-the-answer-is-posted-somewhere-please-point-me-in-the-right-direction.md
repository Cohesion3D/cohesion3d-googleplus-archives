---
layout: post
title: "If the answer is posted somewhere, please point me in the right direction..."
date: November 13, 2017 15:23
category: "C3D Mini Support"
author: "Alex Raguini"
---
If the answer is posted somewhere, please point me in the right direction...



I've read the docs on adding a z-table, etc.  I've looked at the c3d pinout diagram. Here is what I want to do.  I want to have the lps dedicated to the laser. I want to add a 24 power supply to power the c3d and stepper motors for x and y. What is he best way to do this. Is it as simple as simply providing 24v to the C3D 24V power header?  Do I need to disconnect any wires fro the lps to the c3d? I want to be sure I don't make a mistake and fry anything.. 







**"Alex Raguini"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2017 17:51*

Plug the new PSU 24v and Gnd into "MAIN POWER IN" screw terminal at top left of the C3D Mini board. 



Disconnect the 24v coming from the Laser PSU to the board.   If the lpsu has screw terminals then disconnect there, or pull the pin on the board side plug, or cut the wire. 



The grounds from both PSU should remain connected though, I just don't want 2 24v sources.


---
**Alex Raguini** *November 14, 2017 02:34*

Thank you.  That's what I was guessing by wanted to be absolutely certain. 


---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/BW48NdzcKqR) &mdash; content and formatting may not be reliable*
