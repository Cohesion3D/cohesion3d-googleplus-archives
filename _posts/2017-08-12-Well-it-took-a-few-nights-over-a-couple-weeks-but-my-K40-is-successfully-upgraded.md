---
layout: post
title: "Well, it took a few nights over a couple weeks but my K40 is successfully upgraded"
date: August 12, 2017 21:59
category: "General Discussion"
author: "Tom Arnold"
---
Well, it took a few nights over a couple weeks but my K40 is successfully upgraded.  All the bits and pieces I've purchased over the last year are installed and working with the C3D just fine.  Mechanical home switches are now where they should be.  Z-table and thus air-assist head all installed.  PWM looks to be working as it should.



Beyond one bit of weirdness with the LCD display being garbled, which seems to have been solved with a dedicated 24v power supply,  its just been a case of follow the instructions.



No questions in here, just thanks for a nifty product.





**"Tom Arnold"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2017 22:00*

Glad to hear it Tom.  Cheers!


---
*Imported from [Google+](https://plus.google.com/112624015909891269236/posts/ERHgw6iV5H1) &mdash; content and formatting may not be reliable*
