---
layout: post
title: "Hey everyone! Just started using my K40 laser with C3D mini upgrade"
date: October 18, 2017 01:58
category: "C3D Mini Support"
author: "Chris Bennett"
---
Hey everyone!



Just started using my K40 laser with C3D mini upgrade. I've had success while running over USB. Now, I'm trying to load gcode onto an sd to use on the rep rap lcd and it's not reading that a card is inserted. Is there code missing or am I missing something? 





**"Chris Bennett"**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 02:01*

Are you trying to use the SD slot in the screen? That's purposefully not connected because it was unstable. Smoothie has a long article on this in the panel article. 



Please use the MicroSD socket/ card in the Mini board itself.


---
**Chris Bennett** *October 18, 2017 02:16*

Thanks, for the quick response, Ray. I didn't find that info with my search terms. 


---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 02:20*

[http://smoothieware.org/panel#external-sd-card-setup](http://smoothieware.org/panel#external-sd-card-setup)![images/a1c7a07f0a7846ae5d841a7bae5c42ab.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a1c7a07f0a7846ae5d841a7bae5c42ab.png)


---
**Chris Bennett** *October 18, 2017 04:17*

Ray, is laser web gcode supposed to override the laser % on the k40 panel?




---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 04:18*

Nope. The digital panel is the same as the pot. You'd set your max power on that.  I recommend 10mA. 



Then your LW % is a proportion of that. Best grayscale contrast this way, by setting the mA as low as you can (the max you need for the particular job). 


---
**Chris Bennett** *October 18, 2017 17:37*

Thanks! The display on mine is digital and in percentage. 






---
**Ray Kholodovsky (Cohesion3D)** *October 18, 2017 17:41*

Figured. I don't know what the exactly the % maps to in mA. Ideally you would install an mA meter to see for yourself, otherwise I'm sure that some folks in the k40 group here on g+ or on fb have done so and can provide the info. 

I'd ballpark start at 60% on the panel and adjust from there.  



Also know that the board pwm power is limited to 80% by default to prevent people from running their tube at full power (bad for the tube lifespan) so now that you have read this note you can go to config.txt and change the laser max power from 0.8 to 1.0 then save, safely dismount the drive, and reset the board. 


---
**Chris Bennett** *October 18, 2017 17:42*

This one



[amazon.com - Amazon.com: Orion Motor Tech 12"x 8" 40W CO2 Laser Engraver Cutting and Engraving Machine with Water-Break Protection: Arts, Crafts & Sewing](https://www.amazon.com/Engraver-Cutting-Engraving-Water-Break-Protection/dp/B06XCJKS4T/ref=sr_1_14?ie=UTF8&qid=1508348500&sr=8-14&keywords=k40+laser)


---
*Imported from [Google+](https://plus.google.com/112985754335466399031/posts/RpezCQAFpSi) &mdash; content and formatting may not be reliable*
