---
layout: post
title: "I am trying to set up the lightobject z table"
date: July 10, 2017 01:19
category: "C3D Mini Support"
author: "Kevin Lease"
---
I am trying to set up the lightobject z table.  I have the light object 24V power supply and zlm3020 2A external stepper driver.  I saw the diagram on the cohesion3d documentation for wiring it up with cohesion3d,  I followed that and have had some problems.  I am using the glcd to try to jog the z axis.  Stepper moves but not smoothly and only in one direction no matter which I direction I try to jog z axis; x and y axes jog normally.  The external stepper driver is switches are set to supply 2A.  The micro steps switches are set for 1600.  I am using the 5V off the cohesion3d board for the 5V with the PUL+, ENA+, DIR+ all jumpered together with the 5V supply.  I confirmed the pairs of leads to the steppers hooked up to A+A- have continuity and the leads with B+/B- have continuity.  The power supply is working and putting out 24V.  

I used the default config.txt file.  I saw a post in which two gamma pin settings were changed to addend with !o and I updated the config.txt file with those changes and rebooted, then the stepper doesn't move at all.  I would welcome your suggestions.

Thank you,

Kevin Lease





![images/0995ff9d487a63b0899d59c1f1883b0c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/0995ff9d487a63b0899d59c1f1883b0c.jpeg)
![images/4f0ec516e9ef1347dc9692a450b44ac3.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4f0ec516e9ef1347dc9692a450b44ac3.jpeg)

**"Kevin Lease"**

---
---
**Kevin Lease** *July 10, 2017 01:21*

Also note, I tried changing micro steps to the other options and it didn't fix the problem


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 01:23*

First question... how are you using an external stepper driver if the Z and A sockets have the green A4988 modules in them? 



More complete pictures of your board/ wiring please, so that everything can be seen....


---
**Kevin Lease** *July 10, 2017 01:23*

btw, the green wire on the stepper is not the same green wire you see attached to the c3d board, it goes to the stepper


---
**Kevin Lease** *July 10, 2017 01:27*

I have the output from the A4988 going to the external driver


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 01:30*

That's incorrect. Per our diagram on how to wire these up, it shows that we need the STEP, DIR, and ENABLE signals from the Pololu socket on the board, not the output of an A4988 driver, to feed the external stepper driver. 



That diagram also shows the "external stepper adapter" which is a different product than the A4988 driver, being used. 


---
**Kevin Lease** *July 10, 2017 01:33*

ok, thank you for your help, I'll remove the A4988 from Z area on the board and hook up the leads directly to the socket


---
**Jim Fong** *July 10, 2017 02:00*

**+Ray Kholodovsky** I noticed that right away when I saw the pic. Hopefully the external stepper driver optocouplers wasn't blown by 24volts from the a4988.  


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2017 02:03*

**+Jim Fong** - indeed, I was wondering how resilient that LED would be, and also where that white wire goes to. There might be a case where there was 24v to 5v going through that, lots of opportunity for issues. 


---
**Kevin Lease** *July 10, 2017 02:31*

I removed the a4988 and used jumper wires from ena, stop and dir from the female sockets on the board on z to to my external stepper driver, it makes one movement with glcd z jog then won't move again, I reset c3d and it will have the same behavior and make one movement.  I have a second identical external stepper driver that I can use to test next in case I damaged the first one.  If that has the same behavior then I will try an external stepper controller instead of c3d and see if that can drive it.


---
**John Hunter** *July 12, 2017 13:31*

Ray, you mentioned a diagram for wiring this type of machine.   Can you provide a link to it? I just bought a 100w machine with this same set up and i'm a bit lost .  

Thanks!




---
**Ray Kholodovsky (Cohesion3D)** *July 12, 2017 13:38*

Hey John. I'm assuming the 100w has the external black box stepper drivers. We have a diagram on how to wire those on the docs site --> k40 --> wiring a z table and rotary. 



Could you start a new thread here with pics of your machine/ electronics so I can take a look? 


---
**Kevin Lease** *July 13, 2017 01:24*

I was able to get the light object stepper motor controller with the stepper driver above to move the stepper, it still works ok fortunately.  I put a4988 on the z axis of c3d and put the y axis stepper cable on it and it can jog the y axis fine via glcd, so I think the z axis on my c3d works.  I then removed the a4988 from z axis and put jumper wires in dir, step and enable and ground and measure voltage with digital voltmeter between ground and those pins with z axis jog 10 via glcd, I get 3.3V or  0V for dir depending on direction I turn the knob which makes sense, but with with step and enable they are both 0v with z axis 10 jog, which doesn't make sense to me, can someone explain this behavior?  On a related note, does the external stepper driver adapter do more than just tap into the pins?


---
**Jim Fong** *July 13, 2017 02:36*

**+Kevin Lease** if you are jogging, the step output pin is pulsing at a fairly fast rate. Faster than some digital multimeters are able to get a good reading.  Even my expensive 5 1/2 digit fluke doesn't do well and only averages the reading which will be around 1.5 volts.  Inexpensive ones may not even get a reading.  That's where having a oscilloscope comes in handy. 



Enable line is active low so should always read near zero volts if there is any step/direction activity.  It will be high (3.3volts) if the driver is disabled. 






---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/AcvshqCivbB) &mdash; content and formatting may not be reliable*
