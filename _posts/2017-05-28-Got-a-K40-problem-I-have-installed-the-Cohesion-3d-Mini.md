---
layout: post
title: "Got a K40 problem. I have installed the Cohesion 3d Mini"
date: May 28, 2017 21:14
category: "K40 and other Lasers"
author: "Richard Gallatin"
---
Got a K40 problem. I have installed the Cohesion 3d Mini. I have got the flat ribbon cable installed and the power cable installed. I only have one drive cable that I am guessing connects to the 4 pins above the Y drive controller. When I powered on my laser I only got 1 red LED light on and that is all. Also I can not seem to get the drivers installed on my PC. I am running Windows 8.1 64 bit. I have downloaded and the smoothie v1.0 drivers since I seen those work better with 8.1. 





**"Richard Gallatin"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 28, 2017 21:16*

Got a MicroSD reader? Pop the card in and see if your computer recognizes it. You should see a firmware and config file on it. 


---
**Richard Gallatin** *May 28, 2017 21:24*

Yes I can read the config.txt and there is a firmware.bin file 




---
**Ray Kholodovsky (Cohesion3D)** *May 28, 2017 21:28*

Ok, so now put the card back in to the board.  Try plugging in over USB. 



Here is what I expect: 



Red light indicates k40 is providing 24v. Good. 

Green light right under it labelled "3v3" should be on with either power option,, this indicates 3.3v logic voltage. 

L1 - L4 should first count up in binary (first boot firmware flashing).



Then L1 and L4 should be on, L2 and L3 flashing. 


---
**Richard Gallatin** *May 28, 2017 21:38*

OK that is what I seen in the setup page. What else could I try if this does not work. Only reason I am asking is we are getting hit by a good thunderstorm and I dont want to mess with it during a storm. Thanks for responding. 




---
**Ray Kholodovsky (Cohesion3D)** *May 28, 2017 21:43*

Can you describe what happens exactly when you power it over USB with the card in? 


---
**Ray Kholodovsky (Cohesion3D)** *May 28, 2017 21:45*

Oh.  Now I understand what you mean. 



Ok so game plan is as follows: 

The try USB.  

Perhaps try powering each way without the card in. 



At the very least, I need to see that green LED 3v3 on.  If it's not, then we'll have to deal with that before proceeding. 


---
**Richard Gallatin** *May 29, 2017 12:00*

Ray I have powered it on with just the USB plugged in and get just the red led light. I have powered it with just the SD card in and get just the red led light. I have tried with both the card and the USB plugged in and get just the red led light. 




---
**Richard Gallatin** *May 29, 2017 12:02*

![images/8d75b466a673f349067cc0e76442cf47.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8d75b466a673f349067cc0e76442cf47.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 13:13*

Your Y driver is inserted upside down. 


---
**Richard Gallatin** *May 29, 2017 14:17*

Yes I seen that and replaced it with the extra driver that I ordered. Does the cable plug in the y drive or the x drive. 


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 14:18*

Y 


---
**Richard Gallatin** *May 29, 2017 14:21*

OK I replaced driver and tried only get the red led. I reinstalled my board that came with it and it works so i know it is not the unit. Could it be a wiring issue or is it a firmware or board problem. 

I even tried the cable for the drive both ways in the y drive just to be sure I was not backwards. 




---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 14:25*

Unplug all the cables and driver modules and try once more with just USB.


---
**Richard Gallatin** *May 29, 2017 14:39*

I tried with everything unplugged but the cable from the psu only get the red led. 

![images/637218e448fd6f118c7817fa21f9b576.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/637218e448fd6f118c7817fa21f9b576.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 15:26*

It seems like the board was damaged due to the incorrect Y driver orientation. 


---
**Richard Gallatin** *May 29, 2017 15:29*

OK that makes since. I can order a new board NP. Is my mini board fried or would you want it back to see if it is the board or something else. 


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 15:35*

The Mini board is fried. 

Sounds like your SD Card might still be good, not sure if the A4988 drivers are. 

You can get just the Mini and a few drivers to be on the safe side. 


---
**Richard Gallatin** *May 29, 2017 15:40*

I do have one other question. What is this and where is it located. I never seen or messed with this in the directions.   



Notice the potentiometer at the top of the module, and that it has a flat side which should be facing up/ out.  You may need to rotate this about 45 degrees clockwise.  Ideally you would use a ceramic screwdriver.  Minimize your risk of shocking the module by touching something metal like a radiator with both hands or otherwise grounding yourself. 


---
**Ray Kholodovsky (Cohesion3D)** *May 29, 2017 15:50*

[lh4.googleusercontent.com](https://lh4.googleusercontent.com/-8DvpkJyHmy0/Tsd5FdyLquI/AAAAAAAAF2U/Qm9xijmsHEI/s576/pololu.jpg)


---
**Richard Gallatin** *May 29, 2017 15:53*

OK Now i see it. Thanks for all your help in my stupid error. Hope a new board is the answer. 




---
**Kelly Burns** *May 29, 2017 22:48*

Looking at the mounting, it looks like the board is a bit close to metal carrier.  Is it possible something is shorting out?  If there aren't already (as it looks) I would get some spacers in there.  






---
**Richard Gallatin** *May 30, 2017 00:05*

There is 1/4 to 3/8 inch spacers between board and metal case. 




---
**Kelly Burns** *May 30, 2017 00:48*

That's good. Definitely doesn't look like that much.  


---
*Imported from [Google+](https://plus.google.com/102728020130316654078/posts/KXVJVTjepcc) &mdash; content and formatting may not be reliable*
