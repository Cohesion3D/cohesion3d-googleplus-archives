---
layout: post
title: "Ray Kholodovsky - sorry if this has been covered before, i could not find the article - please link if there is one as i do not wish to waste anyone's time :S - So i'm new to the whole laser engraving/ cutting world and"
date: February 08, 2018 19:25
category: "General Discussion"
author: "sam cv"
---
**+Ray Kholodovsky**  - sorry if this has been covered before, i could not find the article - please link if there is one as i do not wish to waste anyone's time :S - So i'm new to the whole laser engraving/ cutting world and stumbled across Cohesion3D - Awesome product btw! - but one thing i could not find is what exactly is the board, because i see elements of normal arduino style components, but then a smoothieboard style upgrade sort of thing... how does it differ from a normal smoothie board/ arduino grbl set up? and is it cross-compatible with the hardware and or firmware ? - any clarification would be appreciated.





**"sam cv"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 19:28*

Fundamentally - It's a smoothieboard derivative.  It has a 32 bit processor. 

The Mini is a 4 Axis and the ReMix is a 6 Axis.

The Mini is enough to drive a Single Head 3D Printer, CNC, or Laser, and the ReMix would be useful for 

For CNC and Laser Applications, there is a 32 bit port of grbl (grbl-lpc) that allows faster operation. 

We make a few drop in upgrades such as the one for the K40 Laser.



What is it that you are trying to do?


---
**sam cv** *February 08, 2018 19:40*

**+Ray Kholodovsky** I would be looking to upgrade my K40 machine ... I am used to working with grbl software and a 5w laser diode system and decided to pull the trigger on a k40 machine. During the research the majority of the comments was that the k40 controller is junk and that one should replace it asap. so whilst the k40 is on its way i thought i would do some research into controller upgrades for it and so far Cohesion is coming out tops. Which brings me to the next question, do you ship to South Africa and if so, how long would it take? - ps, thanks for the speedy reply and the explanation - much appreciated




---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 19:59*

Yes, it's all handled in cart.  USPS First class is typically 2-3 weeks but can take longer.

Priority says 6-10 business days.  

Neither has a guarantee of how long it will take. 



ZA Postal system is slow so keep that in mind...


---
**sam cv** *February 08, 2018 20:20*

I did see that, but thought i better as before i pull the trigger - i was a bit hesitant of the "ship almost anywhere world wide" haha, but glad to know you will shit to SA - Yeah, our postal system is shocking unfortunately :'( - have you had many/ any orders from SA before and heard any feedback from customers here ?


---
**Ray Kholodovsky (Cohesion3D)** *February 08, 2018 20:58*

I've shipped there a few times. It arrived.  That's about all I know :)


---
**sam cv** *February 08, 2018 22:37*

**+Ray Kholodovsky** awesome, good to hear, at least i know it enters the country then. I'm going to do a bit more research, but i'll probably place an order very soon ! (very excited hehe)


---
*Imported from [Google+](https://plus.google.com/101915993108014864632/posts/891N9L35QdN) &mdash; content and formatting may not be reliable*
