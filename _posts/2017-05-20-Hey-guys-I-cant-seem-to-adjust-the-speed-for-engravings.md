---
layout: post
title: "Hey guys, I can't seem to adjust the speed for engravings"
date: May 20, 2017 19:40
category: "General Discussion"
author: "Collin Turner"
---
Hey guys, I can't seem to adjust the speed for engravings.  I have no experience with laser web 3 or 4.  When I jog the axis they move at a speed that I am able to change, but I don't see where I could adjust the speed while engraving.  It seems to move at a good speed when it's on a white spot, but when the laser is on it moves extremely slow (roughly 0.1mm per second).  The S% (I assume speed) is set to 100% while it's running.  Any help is appreciated, thanks.





**"Collin Turner"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 22, 2017 18:15*

Your 2 posts got caught in our spam filter, kudos to google. Anyways, I think these questions are better suited for the LaserWeb community, please post over there: [LaserWeb/CNCWeb](https://plus.google.com/communities/115879488566665599508?iem=1)

Pictures and a save of your workspace (Dropbox link) tend to be helpful. 


---
**Collin Turner** *May 22, 2017 18:17*

That's what I wondered, that's why I posted a second one because I couldn't find the first one after I posted it.  I'll post it in there, thanks.


---
*Imported from [Google+](https://plus.google.com/106338687452907889031/posts/FZsCsoWKmHu) &mdash; content and formatting may not be reliable*
