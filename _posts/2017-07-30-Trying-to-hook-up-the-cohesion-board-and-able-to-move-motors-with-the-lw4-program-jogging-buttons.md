---
layout: post
title: "Trying to hook up the cohesion board and able to move motors with the lw4 program jogging buttons"
date: July 30, 2017 22:27
category: "C3D Mini Support"
author: "John Austin"
---
Trying to hook up the cohesion board and able to move motors with the lw4 program jogging buttons. I can not get the lcd to work. Is there supposed to be a sd card in the back of the lcd?



![images/43454adcd9efc878b09ef76f3134a3ee.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/43454adcd9efc878b09ef76f3134a3ee.jpeg)
![images/055ee48a483e9bd514d1846a7d93ce3a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/055ee48a483e9bd514d1846a7d93ce3a.jpeg)

**"John Austin"**

---
---
**Ashley M. Kirchner [Norym]** *July 30, 2017 22:33*

The SD slot on the back isn't connected to anything, it's not used. For the lcd, make sure you have the connectors the correct way around as well as the adapter on the C3D board. I need to see close up pictures. 


---
**John Austin** *July 30, 2017 22:41*

I tried that but, for some reason i just plugged it back in and was going to try messing with it and poof it worked.

Never fails ask a question and it fixes it self.



Thanks




---
**Ashley M. Kirchner [Norym]** *July 31, 2017 02:58*

Gremlins.


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/5YPH6aJPWm5) &mdash; content and formatting may not be reliable*
