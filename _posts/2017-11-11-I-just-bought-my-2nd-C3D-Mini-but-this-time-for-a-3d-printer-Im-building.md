---
layout: post
title: "I just bought my 2nd C3D Mini but this time for a 3d printer Im building"
date: November 11, 2017 05:21
category: "C3D Mini Support"
author: "Alvin Manalastas"
---
I just bought my 2nd C3D Mini but this time for a 3d printer I’m building. Before installing anything on it, I’m trying to see if my RepRap LCD works. My PSU is 24v and using default config.txt. When I tried with my LCD connected, it just whines without turning on. Tried it with no LCD and VM0T stays red with the other green leds flashing. Do I need to put the steppers to try and test the board? My first one for K40 it just works so not sure if Im doing something wrong.



thanks!



![images/a274225f54de5671ccf834acbc7cac4e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a274225f54de5671ccf834acbc7cac4e.jpeg)



**"Alvin Manalastas"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 14:58*

Check if it's configured in config.txt 




---
*Imported from [Google+](https://plus.google.com/112841546145461029963/posts/aqac7qS143e) &mdash; content and formatting may not be reliable*
