---
layout: post
title: "Morning looking for a bit of help which in not necessary related to the cohesion 3d board but some one hear might be able to help, let me explain"
date: August 05, 2018 09:05
category: "General Discussion"
author: "Ryan Lovejoy"
---
Morning looking for a bit of help which in not necessary related to the cohesion 3d board but some one hear might be able to help, let me explain.



My laser from received fires on the test button but not when running a program using the supplied software or k40 whisper. My plan was to always get the cohesion board as a upgrade so I got this and installed it as I assumed the original board was damaged in some way.  With the cohesion board installed the laser connects and the machine moves as if going though a program but the laser still does not fire when running a program using light burn. Anyone have some advice, I'm guessing the must be a issue with the power supply?





**"Ryan Lovejoy"**

---
---
**Don Kleinschnitz Jr.** *August 05, 2018 14:18*

See if this helps:

[donsthings.blogspot.com - Troubleshooting a K40 Laser Power System](http://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)


---
**Ryan Lovejoy** *August 05, 2018 15:41*

Cheers Don, it pretty much says I need more help lol. Going to pull out the PSU as this seems the culprit see if there is any damage




---
**Don Kleinschnitz Jr.** *August 05, 2018 16:22*

Did you run through the troubleshooting flowchart, that should help you find the culprit.



Some common problems:

The Laser Sw is not pushed or its bad or wire disconnected.

The water sensor (if you have one) is bad.



Some  basic troubleshooting:

Does the laser fire with the Laser Switch "ON" and you press the Test switch on the panel?

Does the laser fire with the test switch (red button) down on the supply?




---
**Ryan Lovejoy** *August 05, 2018 16:29*

Yes ran through the it, both test fire button work on panel and on the PSU. Signal just does not seam to get through form the board, will check everything against again tomorrow the garage is a oven now 




---
**Don Kleinschnitz Jr.** *August 05, 2018 16:49*

Assuming you are driving the L pin on the LPS. Disconnect the C3D connection to the L pin on the LPS. With the Laser Switch enabled try grounding the L pin and see if it fires.



If it does the problem is on the C3D side if not the LPS is suspect.


---
*Imported from [Google+](https://plus.google.com/100308732839779074238/posts/g2MTLFGQ8TQ) &mdash; content and formatting may not be reliable*
