---
layout: post
title: "Ok another question, me and my friend both did the upgrade, i gotta get em in here lol but he had an issue, and if i run into the same issue i wanna know what to do, so ....."
date: April 04, 2017 19:01
category: "C3D Mini Support"
author: "michael chartier"
---
Ok another question, me and my friend both did the upgrade, i gotta get em in here lol but he had an issue, and if i run into the same issue i wanna know what to do, so ..... 



He installed everything including drivers and software, he sent a job to the laser to run a test and nothing happened, what could cause this problem?





**"michael chartier"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 19:02*

I need about 38 more details to be able to diagnose that. 

Like a screenshot of LaserWeb, etc... 


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 19:23*

38 ... that's being minimal. :)


---
**michael chartier** *April 04, 2017 23:47*

Ok well i just installed what i needed and gonna go run a test soon see if i can get it to work, if it works ill tell him what i did, if i have same problem, ill come jn here and see what i need help with lol laserweb is new to me... but i hope i can pick it up


---
**Ashley M. Kirchner [Norym]** *April 04, 2017 23:54*

Except, it's not all LW. We also need to check your C3D config file to make sure that's configured properly.


---
**Ray Kholodovsky (Cohesion3D)** *April 04, 2017 23:55*

Brand new customers. Config file stabilized with batch 2 


---
**michael chartier** *April 05, 2017 01:04*

I cant connect

![images/fd847edd1d9ab7697a7ca4717f2a5423.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fd847edd1d9ab7697a7ca4717f2a5423.jpeg)


---
**michael chartier** *April 05, 2017 01:04*

![images/bf6ff75e7d964dd509f0f1ea33d86852.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bf6ff75e7d964dd509f0f1ea33d86852.jpeg)


---
**michael chartier** *April 05, 2017 01:05*

I put same coordinates in what i think is where id enter my zero.... just as a starting point..... installed drivers previously to hooking up usb.... and software... ill try restarting software


---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2017 01:06*

Looks like Windows 10?  

You said you <b>did</b> install the drivers?  You're not supposed to install the drivers on Win 10. 


---
**michael chartier** *April 05, 2017 01:09*

Well oops but i restarted software and im connected.... 


---
**michael chartier** *April 05, 2017 01:15*

This software is feeling very foreign.... how do i load a picture of what im engraving?


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 01:37*

From the CAM screen, click on 'add document'. Questions about LW should be directed to the LW group.


---
**michael chartier** *April 05, 2017 01:37*

Ok i got the picture in and clicked run job..... i got a few lines in the text box but nothing happened....

![images/145973e1fc31cee49da32b8a16cabe18.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/145973e1fc31cee49da32b8a16cabe18.jpeg)


---
**michael chartier** *April 05, 2017 01:42*

Also seems like my y axis is no longer moving.... 


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 01:47*

You're missing a whole set of steps before you can run the job. Loading a document is just that, it loads it for you to use. You still have to set the operation for it, whether vector cut, (inside, outside), fill path, or raster (for which you need to create a preset with your settings), then generate the gcode, then actually run the job after you've verified that you are connected to the machine and have homed the laser head. You can start by checking the instructions at [cncpro.co](http://cncpro.co).



When you "load" yourself into a car, it doesn't just start moving (not yet at least). You have to make sure it has gas in it, the seat is right, the mirrors are set, does the engine start, etc., etc.


---
**michael chartier** *April 05, 2017 02:08*

Is there any other software i can use with this board


---
**michael chartier** *April 05, 2017 02:15*

Its now not connecting :( someone plz help


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 02:22*

Well sure, you can also try Visicut. I'm sure there are other programs out there, CNC stuff, that will output gcode for you. The difference is, there is a ton of support for LW in its respective G+ group. We don't (also) support any of the others. The board is Smoothieware compatible, so you can use that as your starting point for searching if you really want to look for something else.


---
**michael chartier** *April 05, 2017 02:23*

I installed everythig on another conputer and its saying the firmware doesnt exist

![images/93acfd85f97b77f3253de52ffd6ad803.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/93acfd85f97b77f3253de52ffd6ad803.jpeg)


---
**michael chartier** *April 05, 2017 02:25*

Ashley with this software i need to know where all that is..... how to do it all, that link u gave me is not helping me at all ....


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 02:33*

**+michael chartier**, if I may impart a small piece of advice on you: try not to jump from one thing to the next without investigating what is actually going wrong. There are several things that need to be checked and verified, and steps that need to be followed before you get any output from the laser. A moment ago you were on one machine, of which we have no information on, and now you're onto another machine, for which, again, we have no information on. By the time we can digest what you are telling us is (not) happening and come up with a suitable answer, you would've jumped onto something else, and create a whole new set of problems for yourself. This is really hard on anyone trying to offer support.



Please take a breather. Let it sit overnight. Tomorrow, come back and explain, in details please, what machine are you trying to get things working on. What you did, both hardware and software. Provide pictures of the hardware connections so we can verify everything is wired correctly. What OS is on the computer, what have you installed, or not installed. Show us the C3D config.txt file, the settings in LW itself, screenshots are a huge help here.



There are way too many variables here to try and guess what's going on. Alternatively, you can ignore this post and keep struggling. :)


---
**michael chartier** *April 05, 2017 02:55*

I dont plan on ignoring anything, i can take it...... i was on windows 10 but i installed the drivers (which i wasnt supposed to and missed that, idk how) anyway im on windows 7 now.... my laser doesnt have a wire for y axis jst plug, its looped and wired with the x axis, i was told by ray would be fine, plug into the x axis, so i did ...... 



The sd card came pre loaded with firmware.... just so u know when i say different machine i mean i switched from laptop to desktop... desktop i installed drivers and software.... it wont recognize firmware, problem with installing when plugging in usb..... according to device manager its fine but it has little exclamation next to it






---
**Ashley M. Kirchner [Norym]** *April 05, 2017 03:10*

Windows 7 does need drivers. And if you installed version 1.1 and it's not correctly identifying the board when you plug the machine in, then uninstall them, and do the manual install using the version 1.0 zipfile on the same site.


---
**michael chartier** *April 05, 2017 03:42*

I think i got it figured out ill fire it up n let u know but i figured out why my y axis wasnt moving.... bad driver module

![images/a6abd4b6322a2dbb858ffb6b81c305eb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a6abd4b6322a2dbb858ffb6b81c305eb.jpeg)


---
**michael chartier** *April 05, 2017 04:13*

Also had to change a wire and route the L connection from power supply to the p2.5 connector ... now i have my Y axis working and my laser fires... now im learning how to raster... its coming to me.... slowly but surely

![images/11d025cf1ade8640de559e82c1089d24.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/11d025cf1ade8640de559e82c1089d24.jpeg)


---
*Imported from [Google+](https://plus.google.com/116057139333311855526/posts/cyapnvEhHg4) &mdash; content and formatting may not be reliable*
