---
layout: post
title: "Hello guys, whats the best way to power the cohesion3d independent from the laser power source?"
date: May 19, 2018 08:30
category: "K40 and other Lasers"
author: "james poulton"
---
Hello guys, 



whats the best way to power the cohesion3d independent from the laser power source?



also what benefit is there to running lightburn through whisperer?if i can at all.



ive ordered the cohesion3d and software bundle but its a long wait. 



kind regards.





**"james poulton"**

---
---
**Anthony Bolgar** *May 19, 2018 08:34*

Can't use Whisperer with LightBurn, two different products, and Whisperer only works with the stock M2 Nano board. Best way to power the C3D board is by a 4 to 6A 24V power supply, not your laser power supply.


---
**james poulton** *May 21, 2018 11:56*

Cheers Anthony are just have to wait for it to arrive. I brought a new PS today, it goes up to 15 amp. 


---
*Imported from [Google+](https://plus.google.com/105192351368694731803/posts/6iJQwKnrMta) &mdash; content and formatting may not be reliable*
