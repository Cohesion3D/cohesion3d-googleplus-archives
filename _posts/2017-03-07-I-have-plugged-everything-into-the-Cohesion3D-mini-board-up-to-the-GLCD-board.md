---
layout: post
title: "I have plugged everything into the Cohesion3D mini board up to the GLCD board"
date: March 07, 2017 01:52
category: "C3D Mini Support"
author: "Matthew Wilson"
---
I have plugged everything into the Cohesion3D mini board up to the GLCD board. I am just trying to get some clarity on these last steps. Will anyone post pictures of everything once its fully connected?





**"Matthew Wilson"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2017 01:53*

My pics: [https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0](https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0)


---
*Imported from [Google+](https://plus.google.com/101681639472204631291/posts/jWiVgdGV3Pm) &mdash; content and formatting may not be reliable*
