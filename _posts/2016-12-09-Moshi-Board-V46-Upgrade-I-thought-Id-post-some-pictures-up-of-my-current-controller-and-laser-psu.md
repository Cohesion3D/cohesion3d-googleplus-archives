---
layout: post
title: "Moshi Board V4.6 Upgrade. I thought I'd post some pictures up of my current controller and laser psu"
date: December 09, 2016 11:31
category: "General Discussion"
author: "John Sturgess"
---
Moshi Board V4.6 Upgrade.

I thought I'd post some pictures up of my current controller and laser psu. There is a 2nd psu powering the moshi board separately. It looks like I'll need to order a screw terminal to hook up the C3D mini to the laser psu for 24 & 5V, I'll measure to make sure I order the correct pitch. 



![images/76c09ae2978755ba34b53e55b9f3408e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/76c09ae2978755ba34b53e55b9f3408e.jpeg)
![images/2ed4f84da8a30a52ba21e0b2afb3cccb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2ed4f84da8a30a52ba21e0b2afb3cccb.jpeg)

**"John Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 09, 2016 20:49*

Yep, ribbon cable (top) and Y motor (bottom) look the same on the Moshi Board.  The power connector (middle) will require some reworking.  To be absolutely clear, we don't want to give the C3D 5v from there.  Just 24v and Ground (mini will generate its own 5v), and then the 'L' laser fire wire. 


---
**John Sturgess** *December 09, 2016 21:03*

Ahh, makes sense. Would there be any advantage in keeping the 2nd 24V PSU to run the C3D or will that stop the laser triggering via the 'L' wire? Also does it matter if its connected to the 'L' wire on the 4 way connector or the one on the 6 way? assuming they are connected internally.


---
**John Sturgess** *December 11, 2016 15:43*

**+Ray Kholodovsky** What style/pitch is the power connector on the C3D mini? I want to get the appropriate one ready to connect up


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 00:09*

**+John Sturgess** I replied to you for the 2 days ago one but I don't see that here :(



I don't know what 2nd psu you're talking about, please elaborate. 



Remember, the mini has headers or terminals for each other thing. So for example the 24v can go to the power input screw terminal. And the L can go to the header for one of the small MOSFET outputs, or to a different MOSFET. The laser power connector is a JST VH 3.96 since you asked, but you don't need to "conform" your connections to it. Lots of options. 


---
**John Sturgess** *December 12, 2016 08:10*

**+Ray Kholodovsky** here is a picture of the 2nd psu, I think over the various iterations some parts are followed over, I also have the large green ballast resistor which is more familiar on the models with the older open psu. I've also got a small pcb with screw terminals, it's purely used to combine various ground and the laser fire signals. I'm going to draw up the schematic and attempt to make a visual diagram. 

![images/d3eaa712d732a67c126a0712e958e869.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d3eaa712d732a67c126a0712e958e869.jpeg)


---
**John Sturgess** *December 12, 2016 08:11*

![images/c0a4be169836cb01f6657ef5712738cb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c0a4be169836cb01f6657ef5712738cb.jpeg)


---
**John Sturgess** *December 12, 2016 08:11*

![images/352ab333c9dbaf2de2a75f1b8a7ec09e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/352ab333c9dbaf2de2a75f1b8a7ec09e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 12, 2016 23:16*

Oh.... This is indeed different.   Well, like I said,  the components we do need seem to be available.  It will be good to have you trace what the lines from the red board are going to. 


---
**John Sturgess** *December 13, 2016 10:54*

Red board? The Moshi't'board V4.6, and various other versions have a 6 way power connector. On mine i've got pins 1 & 2 connected to the 24V PSU & pins 5 & 6 connected to the breakout board as shown below. on the BoB pins 1,4,5, & 8 are all connected to GND and pins 2,3,6, & 7 are all effectively laser fire.



Excuse the dodgy diagram, my first attempts with Visio!

![images/9dfbf71b4506248532ddd371b477e132.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9dfbf71b4506248532ddd371b477e132.png)


---
**John Sturgess** *January 20, 2017 09:52*

**+Ray Kholodovsky** As per your instructions i'm going to keep the pot connected to set the max value and hook up "L" on the LaserPSU to Bed - 2.5 on the Mini. As i have my 24V supply to the Mini coming from a different PSU as shown in the pictures will this work or do i need continuity between the Mini & Laser PSU?


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 16:55*

Well, you'd have to provide 24v and gnd to the mini. You can do this thru the main power in terminal block (see pinout diagram). I would just move the existing cables for now and see what happens. 


---
**Ray Kholodovsky (Cohesion3D)** *January 20, 2017 16:56*

Meter all the voltages btw. Don't want any surprises. 


---
**John Sturgess** *January 22, 2017 14:11*

**+Ray Kholodovsky** got the mini installed, I can't get the laser to fire. I suspect it's because other than the "L" to bed- wire there are no other connections between the laser PSU and the mini. Config changes have been made. I'm not sure if it'll be better to power the mini from the main laser psu (have to get the correct connector) or connect a GND from the mini to the laser psu like the moshiboard had. 


---
**John Sturgess** *January 23, 2017 08:26*

So all is clear, here is my original wiring. The CN5 connector on the Moshiboard had 24V & GND coming from a 2nd PSU, it then had the "L" trigger and a GND going to the Laser PSU, i pressume for a reference voltage.

![images/001f855161381016da251c556e7544f7.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/001f855161381016da251c556e7544f7.png)


---
**John Sturgess** *January 23, 2017 08:45*

**+Ray Kholodovsky**This is How i have wired in the C3D Mini (excluding the green dot dash wire), the laser doesn't fire and i pressume its because the Laser PSU has no ref voltage so can't to anything with the PWM feed from Bed- to "L". If i add the green dot dash wire will this cause any issues? This'll effectively be wired exactly the same as the Moshiboard then.

![images/6eb1e458f25f2b5be6676817905304ae.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/6eb1e458f25f2b5be6676817905304ae.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 17:39*

Hi John,

Yes, this occurred to me earlier that there may be a need to connect grounds between the power supplies. 

It is important to note that the 2 Gnd connections on Mini you have indicated are the same (as in they are all internally connected on the board).  I would feel more comfortable if you just tied the grounds of the power supplies together directly. 

Based on your previous pics, it looks like all the G's on the connection block are connected together.  So just tie into ground there. Don't do it thru the Mini. 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 17:40*

So, to clarify my intention:   Green dashed wire from "Ref G" to "G on 24V PSU"


---
**John Sturgess** *January 23, 2017 17:43*

**+Ray Kholodovsky** great stuff, yeah that makes more sense :) 


---
*Imported from [Google+](https://plus.google.com/112856126515261932166/posts/iimCuVWJsBk) &mdash; content and formatting may not be reliable*
