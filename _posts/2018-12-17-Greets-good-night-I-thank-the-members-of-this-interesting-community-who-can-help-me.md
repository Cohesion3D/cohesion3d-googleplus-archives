---
layout: post
title: "Greets, good night !!!! I thank the members of this interesting community who can help me"
date: December 17, 2018 02:20
category: "K40 and other Lasers"
author: "Freddy Clemente"
---
Greets, good night !!!! I thank the members of this interesting community who can help me. I have a k40 co2 machine with a Moshidraw V4.1 card. She worked perfectly for a month until without any reason the X-Y axes stopped moving. When I turn on the machine it should be positioned at point 0,0 but it does not move. Likewise when you start the computer, the axes should move to the position started in the software. When I send a file to print the software indicates that the machine is busy. The connection is well made because the devices in the control panel appear well configured. What could be the failure ???





**"Freddy Clemente"**

---
---
**James Rivera** *December 17, 2018 07:04*

Wrong community. This one is for people who have a Cohesion3D board.


---
**Freddy Clemente** *December 19, 2018 14:21*

Regards !!!! Do you know any forum about the Moshidraw card ???


---
**James Rivera** *December 19, 2018 17:18*

You could try the K40 group.[plus.google.com - GETTING STARTED?..........NEED HELP? IF YOU ARE NEW TO THIS COMMUNITY, G+, A...](https://plus.google.com/+DonKleinschnitz/posts/BLPdnUPo4ds)


---
**Freddy Clemente** *December 20, 2018 18:04*

**+James Rivera** thanks men !!!


---
*Imported from [Google+](https://plus.google.com/+FreddyClemente/posts/7BqoQjJgd9S) &mdash; content and formatting may not be reliable*
