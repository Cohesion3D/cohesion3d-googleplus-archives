---
layout: post
title: "I received my board the other day and I have it hooked up as described, and I am unable to make the laser fire from the software"
date: August 04, 2017 17:49
category: "K40 and other Lasers"
author: "jeff gee"
---
I received my board the other day and I have it hooked up as described, and I am unable to make the laser fire from the software. No test fire, no firing during a program. It will still fire with the test button on the machine. So I don't think it is a power supply issue.

I have tried both with the green wire "L" in the 4-pin and with it rewired to the screw terminal at pin 2.5. I also verified that I am getting -4 volts on that pin. When I, very briefly, pull the pin low manually the laser fires. 

I just flashed the latest firmware from the dropbox. And no changes.

Both steppers are working just fine as of now. It is just the laser that isn't happy.



![images/90980ccdd17c477bc31c3b51babedb72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/90980ccdd17c477bc31c3b51babedb72.jpeg)
![images/65db597f281dd8706ec36322cb784e1a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/65db597f281dd8706ec36322cb784e1a.jpeg)
![images/2f86ee594b404d0720ace4b6bdd3ff8d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2f86ee594b404d0720ace4b6bdd3ff8d.jpeg)

**"jeff gee"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 17:52*

Pleaes advise exactly how you are trying to do the firing in more detail.  The test fire button needs setting configured first.  Try a G1 X10 S1 F600 command, what happens? 


---
**jeff gee** *August 04, 2017 18:05*

I tried that command and the head moves, but no laser. I tried to load a job and the head moved but no laser. I tried to use the test fire function on the GLCD and in LaserWeb with no success. 


---
**jeff gee** *August 04, 2017 18:05*

I removed the glcd connector in the pictures so you could see the wires better. 


---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 20:40*

Let's please do this.  Can you run a wire from the +24v on the main power in screw terminal to the + on the fet in one.  (first pin at top left of board, to first pin at bottom of board terminals).   Also disconnect the L wire from pin 4 for now.  Now send the G1 line.  You should be seeing a red led light up in the lower left of the board.  


---
**jeff gee** *August 04, 2017 21:31*

Did as you requested. There was a red light but that light is on all the time, whether or not the +24 v line is in the first terminal or not. 

![images/333b7a6b39e848249801e6f3f7da5af0.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/333b7a6b39e848249801e6f3f7da5af0.jpeg)


---
**jeff gee** *August 04, 2017 21:32*

![images/e3348cd193ff7b1ac41e26c0cbbb4dc8.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e3348cd193ff7b1ac41e26c0cbbb4dc8.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 21:33*

The other bottom left of the board :) 


---
**Ray Kholodovsky (Cohesion3D)** *August 04, 2017 21:34*

That's not how I meant to wire the green wire. Will send diagram soon. Please power down. 


---
**jeff gee** *August 04, 2017 21:39*

Thank you, standing down.


---
**jeff gee** *August 05, 2017 00:00*

![images/1b20cd427e6f54c30285e3622bb3659c.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b20cd427e6f54c30285e3622bb3659c.png)


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 00:06*

Bingo!


---
**jeff gee** *August 05, 2017 00:59*

Changes made. No led lit.


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 00:59*

No LED when you run the G1?


---
**jeff gee** *August 05, 2017 01:03*

correct.


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 01:04*

Going to work up an alternate wiring plan for you. Touch base tomorrow. 


---
**jeff gee** *August 05, 2017 01:07*

Copy that, Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *August 05, 2017 01:22*

You got the white pwm wire in the bundle? 


---
**jeff gee** *August 05, 2017 01:29*

Yup, I also have a box of JST connectors. So I can make any custom cable I need, 


---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 03:30*

Alternate wiring instructions for L.  Essentially we're going to use a different mosfet from the board to drive your laser firing.



Cut and prepare the PWM cable as so.  The center wire is needed, and this will plug into the L terminal of the PSU.  All else should be disconnected from that terminal and insulated. 



In config find laser pin 2.5 and change to 2.4



Just 2.4



Save, dismount card, and put back in board/ reset board.  Try G1 lines again.  Send results and pics of wiring as needed.

![images/5ab9982f45eeca31a0ffc2018bf22a63.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5ab9982f45eeca31a0ffc2018bf22a63.png)


---
**jeff gee** *August 06, 2017 06:15*

That seems to have solved it. Thank you very much. I will post after my first cut. 


---
**jeff gee** *August 06, 2017 07:13*

![images/d56ccbcf4fe3331f6f818ff5c4de18d4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d56ccbcf4fe3331f6f818ff5c4de18d4.jpeg)


---
*Imported from [Google+](https://plus.google.com/114869929766524387528/posts/FC8GorP5N8D) &mdash; content and formatting may not be reliable*
