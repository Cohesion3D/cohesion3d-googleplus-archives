---
layout: post
title: "My DIY CNC Milling Machine project (running Cohesion3D controller)"
date: February 05, 2017 12:24
category: "Show and Tell"
author: "\u00d8yvind Amundsen"
---
My DIY CNC Milling Machine project (running Cohesion3D controller).



Next I have to wrie the end-stops, fans, emergency stop..



I`m going to use a 25Pin serial cable between the control electronics and CNC machine for the end-stop signals.. for the stepper motors I`m going to use seperate shielded cables.



![images/96c4664bbad7af8c60eaac6fda873cf4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/96c4664bbad7af8c60eaac6fda873cf4.jpeg)
![images/75a11ccd8c2169c19f7f60a30a890747.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/75a11ccd8c2169c19f7f60a30a890747.jpeg)

**"\u00d8yvind Amundsen"**

---
---
**Griffin Paquette** *February 05, 2017 17:13*

Awesome setup!


---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 19:27*

Don't forget: [plus.google.com - This is the right orientation of the LAN Module in the Remix Board😀](https://plus.google.com/u/0/115163232171765331523/posts/hupmg7L3jJJ)


---
**Øyvind Amundsen** *February 06, 2017 20:24*

In the other end (on the CNC) I'm going to mount this![images/47e28cbb1234eee12cc3dcce598faa42.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/47e28cbb1234eee12cc3dcce598faa42.jpeg)


---
**Øyvind Amundsen** *February 06, 2017 20:24*

The inside ![images/ef3fae70d35cf6cacd08611a9b4c1b13.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ef3fae70d35cf6cacd08611a9b4c1b13.jpeg)


---
**Øyvind Amundsen** *February 06, 2017 20:25*

**+Ray Kholodovsky** I'll start testing Ethernet soon - then I'll fix it


---
**Jerry Helm** *February 07, 2017 18:11*

Your wiring technique is excellent. Nice and clean. My complaints.




---
**Øyvind Amundsen** *February 07, 2017 18:12*

**+Jerry Helm** Thank you


---
**Øyvind Amundsen** *February 07, 2017 18:59*

Started assembling the gantry - I guess I need some hours adjusting/tuning before it is ok. I'm going to install a 6mm aluminum plate behind the two x profiles to make it more stable![images/c637f2c9e69f59ded66199393e0cf434.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c637f2c9e69f59ded66199393e0cf434.jpeg)


---
*Imported from [Google+](https://plus.google.com/102309741047174439430/posts/3HxH2vjSnfw) &mdash; content and formatting may not be reliable*
