---
layout: post
title: "I bought the LCD adapter for the c3d conversion, can you provide me a configure file for it or a link"
date: March 28, 2017 00:47
category: "FirmWare and Config."
author: "Lance Sponberg"
---
I bought the LCD adapter for the c3d conversion, can you provide me a configure file for it or a link.  It is the rep rap display, I looked on the smoothie site and it walks through the changes.  I was looking for the easy activation.





**"Lance Sponberg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 01:25*

It should work using the config already provided on the card in the bundle. It is already activated. You should not have to change anything. 


---
**Joe Alexander** *March 28, 2017 01:26*

in the standard config go to the bottom where it lists panel info and uncomment the lines (remove the hashtag in the beginning). Every config i have had was preset for a reprap screen. Also note that some clone LCD's have backwards connectors on the adaptor board, requires either unsoldering them and reversing or snipping the tab off the cable end and plug it in backwards. IF your screen is the white board(genuine) this is not an issue.


---
**Alex Krause** *March 28, 2017 02:53*

Keep the cable short that connects the LCD to the board... Is the only advice I can offer


---
**Lance Sponberg** *March 28, 2017 14:46*

Screen lights up and it beeps when you push the button, but no text shows up. Might be a china unit.


---
**Ray Kholodovsky (Cohesion3D)** *March 28, 2017 14:48*

There's a new k40 guide on the documentation site. Compare yours against the pics there. 

Might need to fiddle with contrast (the pot at the bottom left) 


---
**Russ “Rsty3914” None** *March 29, 2017 03:20*

To use separate power supply for board and steppers j would only use common from laser supply or ground both commons on power supply...right ?


---
**Ray Kholodovsky (Cohesion3D)** *March 29, 2017 03:24*

**+Russ None** never anything related to the AC voltage side, you used the word commons so I want to make sure you know that big. 



Yeah, a common ground between the psu's directly would be good. Pull the power connector out of the Mini. Wire +24v and gnd from the new psu into the main power in screw terminal on the Mini. Verify against the polarity indications on the board and the pinout diagram. 


---
**Lance Ward** *March 29, 2017 19:09*

**+Lance Sponberg** Had the same issue.  Was a faulty display board and I had to return it.  The replacement worked fine.


---
**Russ “Rsty3914” None** *March 29, 2017 21:52*

Sorry I should say commons...I meant the -24vdc.


---
**Russ “Rsty3914” None** *March 31, 2017 17:27*

Well i think we still need the 5volts coming from the power supply and common, so that plug would stay in (Except for 24v From laser PS ).  I'm still in design phase.  New Control panel finished, now need to wire it.  Waiting for more parts.  Power supply filter coming in today...Anyone have a good Water flow sensor control module ?




---
**Ray Kholodovsky (Cohesion3D)** *March 31, 2017 17:31*

The Mini doesn't take 5v from the psu. Just 24v and gnd (it regulates its own 5v from that) and the L wire. 


---
**Russ “Rsty3914” None** *April 01, 2017 03:44*

Ok, thought there was a 5 volt supply...my bad




---
**Ray Kholodovsky (Cohesion3D)** *April 01, 2017 03:48*

Again, I'm a sticker for wording :)

There is a 5v "supply" <b>coming from the psu</b>

But the Mini doesn't need it. 


---
**Russ “Rsty3914” None** *April 01, 2017 03:48*

Good to know..


---
**Lance Sponberg** *April 04, 2017 00:41*

Got my machine all up and running with laserweb 4, and C3D mini board.  cut my first raspberry pi case out of acrylic, worked awesome. There is an endless supply of things to make on Thingiverse, so nice to be able to run g code files.


---
*Imported from [Google+](https://plus.google.com/112218001247692140739/posts/STTZVvHKwcG) &mdash; content and formatting may not be reliable*
