---
layout: post
title: "Here's my K40 electronics. I've got an analog control panel and the pot goes to that 3-pin B/R/G connector, 2nd from the top on the PSU"
date: March 08, 2017 05:09
category: "C3D Mini Support"
author: "Dave Durant"
---
Here's my K40 electronics. I've got an analog control panel and the pot goes to that 3-pin B/R/G connector, 2nd from the top on the PSU. 



I was all ready to pull that connector and use the PWM cable to the C3D Mini instead but just now saw a comment from Ray saying "Take everything you read with a grain of salt, for example we no longer recommend replacing the pot."



Is that comment talking about this? Should I just not use the PWM cable for now or am I misreading things? 



Thanks!



![images/2d440f66891e9001ba5b37704ef05ba4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d440f66891e9001ba5b37704ef05ba4.jpeg)



**"Dave Durant"**

---
---
**Alex Krause** *March 08, 2017 05:14*

I had this same config to start on my k40 kept the pot and I get pretty acceptable grayscale engraves and it's a simpler change over 


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 05:15*

Good man, reading the things I say. 

Summing up, don't wire the pot/ pwm cable (the white cable included in the bundle) 

And don't cut any wires. 



You have the same machine as me. Just follow my pics: [dropbox.com - C3D Mini Laser v2.3 Photos](https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0)



And the wire you are pointing to is not pwm but the endstops cable. 


---
**Dave Durant** *March 08, 2017 05:21*

That's just me holding the board - not pointing! :)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2017 05:24*

Derp. Well it's past midnight here so I get a pass on that one. Put a pic if you want me to check your wiring to the new board in the morning. But mainly just don't plug the big power connector in backwards. 


---
**Dave Durant** *March 08, 2017 18:04*

LOL - you absolutely get a pass.. 



On your pictures, are your X & Y connectors reversed? I just wired up the Mini and that's the only one I wasn't sure of - I ended up putting the keyed side of both connectors towards the inside of the Mini. 



Also, why has the PWM cable fallen from popularity? Does it cause problems or does it just not really add anything any more? It seems pretty easy to swap in so if it's even a 1% improvement, it seems worth the effort.


---
**Dave Durant** *March 08, 2017 19:02*

Here's what it looks like. The X/Y connectors may get a dab of hot-glue, once I'm sure they're correct.

![images/9c014dabe5ccfae8399277dc099a3c5e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9c014dabe5ccfae8399277dc099a3c5e.jpeg)


---
**Dave Durant** *March 11, 2017 02:04*

**+Ray Kholodovsky**: how's that look? I think everything is right but a 2nd pair of eyes never hurts...


---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 02:06*

Looks good to me 


---
*Imported from [Google+](https://plus.google.com/+DaveDurant/posts/5sxh6gs1Z6q) &mdash; content and formatting may not be reliable*
