---
layout: post
title: "Ok maybe I am missing something but is there a way to do the laser test pulse (for mirror alignment) using the CHmini board?"
date: July 23, 2018 21:24
category: "C3D Mini Support"
author: "Atherton .Wiseman"
---
Ok maybe I am missing something but is there a way to do the laser test pulse (for mirror alignment) using the CHmini board? My machine was one of the newer digital readout ones with the all in one pcb. 





**"Atherton .Wiseman"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2018 21:30*

Your machine should still have a test fire button, does it not? 


---
**Tech Bravo (Tech BravoTN)** *July 23, 2018 21:31*

is there a reason why you don't use the test button on the digital control panel? i am asking because i think you can fire the laser from the mini if you have the glcd but its buried in a menu and the test fire button on the original control panel is way easier


---
**Atherton .Wiseman** *July 23, 2018 21:37*

I thought the digital panel would not work anymore as the laser pwm pin was connected to the mini?


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2018 21:43*

If you have followed our install instructions, then it should still work. 


---
**Atherton .Wiseman** *July 23, 2018 21:44*

what would the Gcode command be to fire the laser at X percentage for X seconds then turn off? I could add that to the custom menu items page. 


---
**Tech Bravo (Tech BravoTN)** *July 23, 2018 21:53*

Can you tell me why not just use the test button on the original digital panel? Im trying to determine if it is disconnected ir just not easily accessible because it is still the easiest way to fire the laser


---
**Tech Bravo (Tech BravoTN)** *July 23, 2018 21:57*

Here is more on the gcodes but i suggest using the systems already in place to test fire the laser: [smoothieware.org - laser [Smoothieware]](http://smoothieware.org/laser#testing)


---
*Imported from [Google+](https://plus.google.com/109924700452829503954/posts/PVJBL1wVyMC) &mdash; content and formatting may not be reliable*
