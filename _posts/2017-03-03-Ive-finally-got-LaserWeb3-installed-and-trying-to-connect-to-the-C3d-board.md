---
layout: post
title: "I've finally got LaserWeb3 installed and trying to connect to the C3d board"
date: March 03, 2017 17:34
category: "C3D Mini Support"
author: "John McKeown"
---
I've finally got LaserWeb3 installed and trying to connect to the C3d board. I get this... ! Machine Control Disabled . Please Enter PIN to Unlock. Any ideas what I can do to move forward? Apologies if this is already obvious to some. I'm wading my way through this slowly :) Thanks, John

![images/f8cf8ee4195678ddcf841004fb11886d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f8cf8ee4195678ddcf841004fb11886d.jpeg)



**"John McKeown"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 17:35*

Just put 1 2 3 4

You can disable it in settings (I've found I have to enable --> save --> disable --> save and then it goes away with page refresh). 


---
**Jonathan Davis (Leo Lion)** *March 03, 2017 17:41*

I use the tool as a safety feature 


---
**John McKeown** *March 03, 2017 18:01*

Bahh. It is disabled in settings. I think it's a bigger issue as I can't connect to the board. It could be my wiring setup. Should I start a thread or post pics somewhere else? I followed the standard set up as my K40 seemed the same as that. Red light is flashing on the board when I power up the laser.


---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 18:02*

Yes, enable --> save --> disable --> save and then it goes away with page refresh.

Your red light on the Mini is flashing?  Red should be solid and 2 middle greens only should be flashing. 


---
**John McKeown** *March 03, 2017 18:08*

I have two solid outside green leds, two middle flashing leds and a solid red. Sorry for the confusion. I've now lost LaserWeb too




---
**John McKeown** *March 04, 2017 13:37*

Thanks for that helpful suggestion, but I'd tried that several times and it didn't work.... OK so I reinstalled everything. Thanks to a guy on Facebook, a problem with the Laserweb folder was solved and it seems to at least be communicating with the machine. I've adjusted the settings as per the guide on here. The laser is not firing and the axis are all over the place. I'm assuming the problem is with the wiring. Here are a few pics. Hopefully someone can tell me where I'm going wrong. Thanks.


---
**John McKeown** *March 04, 2017 13:38*

This is the board I removed

![images/5d08a9bb6fd0dece11eebedb006911cf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5d08a9bb6fd0dece11eebedb006911cf.jpeg)


---
**John McKeown** *March 04, 2017 13:38*

This is the original wiring to the PSU

![images/96f3c6a6c015350b3809400fd421af67.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/96f3c6a6c015350b3809400fd421af67.jpeg)


---
**John McKeown** *March 04, 2017 13:39*

This is how I have it wired now

![images/fcaa70b9db97fbc2262316111621ff9b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fcaa70b9db97fbc2262316111621ff9b.jpeg)


---
**John McKeown** *March 04, 2017 13:40*

and this is the wiring to the new board

![images/732718f206f2ae01a258253dfc416af7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/732718f206f2ae01a258253dfc416af7.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 15:10*

Hey John, 

We've changed the recommended wiring a bit, if you read through some of the other threads in this group you will find it. 

The jist is: remove the white cable provided with the C3D and rewire the potentiometer to the psu. You need to run a wire from L (the far right on the psu) to bed - 2.5 terminal. 

There will also need to be a change in config.txt laser pin from 2.4! to 2.5 

Save, reset the board. 



Now I don't know what you mean by "axes all over the place" but let's get you caught up to the new recommended way of wiring and then work through the issues. 


---
**John McKeown** *March 04, 2017 15:15*

Thanks Ray. I've realised I've missed a lot since I was last on here. I thought I had everything up to date but it's all changed since then :D Getting it sorted slowly but surely now. The axes are now moving in the correct direction... just needed to turn the connection.... and I'm rewiring the pot. One question. How do I reset the board?


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 15:21*

Little yellow button top right. 

Make sure you save the config file and safely eject in the computer first. 


---
**John McKeown** *March 04, 2017 15:30*

Got it Thanks!


---
**John McKeown** *March 04, 2017 15:37*

So I have a green wire running from L to Laser Fire on the board. Do I remove it and run a wire to -2.5 or run both wires?


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 15:39*

Disconnect the green wire from the psu. Run a new wire from there to the terminal on mini. Put some tape on the green wire end so it doesn't short. 


---
**John McKeown** *March 06, 2017 00:33*

All set up now thanks.Unfortunately It seems I can't run Laserweb... still waiting for a broadband connection to new house and  currently using a mobile connection. It's just too slow. I'll look for an alternative and get this thing running :)


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 00:36*

LaserWeb isn't Internet based. You just need to clone/ download the repo once to your local machine. 


---
**John McKeown** *March 06, 2017 01:30*

OK ... then I have another problem :/ It keep s crashing my chrome browser


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 01:34*

What do you do when this happens? 


---
**John McKeown** *March 06, 2017 02:03*

So far I've tried reloading the page a few times ...and then my laptop crashed.  The rest of the day was spent trying to repair it.. so not much else. I guess I should have run the program from cmd again?


---
**Ray Kholodovsky (Cohesion3D)** *March 06, 2017 18:55*

Is your computer a somewhat recent model? 


---
**John McKeown** *March 07, 2017 01:21*

Not exactly .. it's a few years old HP elitebook 8440p Maybe it's just too old, Now I'm reinstalling everything on a newer laptop to see if that works better.




---
**John McKeown** *March 12, 2017 15:35*

OK so I'm back again!  Board is all wired up as advised. Thanks ;) Laserweb 3 is installed and I'm using the preloaded settings for K40. I can jog the laser no problem but can't seen to run a file. Opening a jpeg and generating gcode gives the error TypeError: Cannot read property 'value' of null ([http://localhost:8000/js/threegcode.js](http://localhost:8000/js/threegcode.js) on line 29

Any suggestions? Should I now move this conversation to another group or page? Thanks again! John


---
**John McKeown** *March 12, 2017 15:38*

Also I get an Alarm page every time I open the software or clear the job. I've just been clearing this and continuing, assuming that's the only option right now?


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 16:11*

Alarm is a known thing. 

Try LW4 - there's an exe to install. 


---
**John McKeown** *March 12, 2017 19:17*

will do!




---
**John McKeown** *March 12, 2017 21:43*

Any change in firmware required for Laserweb4? When I connect the machine it homes to a point in the centre of the X axis, off my work area. I've set my own k40 machine profile with G28.2 homing


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 21:46*

No change should be required. G28.2 should touch switches in upper left. 


---
**John McKeown** *March 12, 2017 21:53*

It did in Laserweb3 but not now




---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 21:59*

Then it may not be sending the right command 


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 21:59*

Post to the laserweb group with this. 


---
*Imported from [Google+](https://plus.google.com/108648609732667106559/posts/Srun66uLLHh) &mdash; content and formatting may not be reliable*
