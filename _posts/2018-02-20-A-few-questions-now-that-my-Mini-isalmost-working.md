---
layout: post
title: "A few questions, now that my Mini is(almost) working"
date: February 20, 2018 15:07
category: "C3D Mini Support"
author: "Chapman Baetzel"
---
A few questions, now that my Mini is(almost) working.  

1. Do i need to power the FET-IN and the Main power connectors?  Do they need to be separate power supplies? Can a 12V and 24V supply be used? 12V for the heaters, and 24V for the motors?  

2. Can i drive a fan from the 2.6 and/or 2.4 pins(the MOSFET 3 and 4 headers) Do these pins support PWM, and can they supply enough current for a 12V 30mm fan?  





**"Chapman Baetzel"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 20, 2018 19:13*

1. You do have to provide power to both terminals, you can use the same psu and run 2 sets of wires. 

2. Fet3 and 4 are designed for fans yes. A normal 30 or 40mm fan is fine, say up to 100-150mA.  


---
**Chapman Baetzel** *February 20, 2018 19:36*

Excellent.  will FET3 and 4 put out 12V if the main supply is 24V?  


---
**Ray Kholodovsky (Cohesion3D)** *February 20, 2018 20:00*

Nope, that's not how an N Channel FET works. It just connects to ground when it's on. Get a separate 12v PSU or a 24v --> 12v converter and power the + of the fan there.  Then connect the - of the fan to the center pin of those FET3/ 4 headers. 

And finally your 2 PSU's will need to have a common ground connection. 


---
**Chapman Baetzel** *February 20, 2018 20:31*

that sounds like way too much work to run 12V fans.  I'll just buy 24V fans and swap them in.  I say 'them' but that printer only has 1 on the e3d hotend. I thought about adding one for the controller itself, but cooling hasn't really been an issue.


---
**Chapman Baetzel** *February 21, 2018 16:11*

ok so just to recap how i should go about converting to 24V.  1. Replace bed and hotend heaters with 24V units.  Replace fans with 24V fans, + to Vin, - to MOSFET pin.  Should I expect to adjust my driver current?


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 16:15*

Since you said they were getting hot you might want to lower the current a little bit (small hole in the driver bottom edge, turn the pot counterclockwise a little bit) but that's a general thing. Motors will perform better at 24v but current will be the same. 


---
**Chapman Baetzel** *February 21, 2018 16:52*

the current isn't digitally controlled? I know i saw a setting for that in the smoothie config.


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 16:53*

Yes, that's because we kept the stock smoothieboard config file, but C3D/ Stepstick drivers don't have that option. 


---
**Chapman Baetzel** *February 21, 2018 18:03*

alrightey then.  good to know. thanx


---
*Imported from [Google+](https://plus.google.com/+ChapmanBaetzel/posts/5iGCyeC6urp) &mdash; content and formatting may not be reliable*
