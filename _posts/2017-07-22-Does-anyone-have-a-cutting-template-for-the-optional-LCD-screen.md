---
layout: post
title: "Does anyone have a cutting template for the optional LCD screen?"
date: July 22, 2017 22:42
category: "K40 and other Lasers"
author: "Ken Purcell"
---
Does anyone have a cutting template for the optional LCD screen? Was going to make a cutting template for mounting it in panel above the other controls on my K40.





**"Ken Purcell"**

---
---
**Ashley M. Kirchner [Norym]** *July 22, 2017 22:59*

I have an SVG file of my panel specifically, but you can extract the shape for the GLCD from it:

[drive.google.com - Control Panel-01.svg - Google Drive](https://drive.google.com/file/d/0B8NExy6DoX2RSlE5R0R3Qm1GLVU/view?usp=sharing)


---
**Ken Purcell** *July 23, 2017 01:13*

Thanks so much, I appreciate you sharing the file. Probably now get some temp gauges ;-)




---
**Ashley M. Kirchner [Norym]** *July 23, 2017 01:21*

Those holes are based on this specific gague: [ebay.com - Digital Thermometer Temperature LCD Module Gauge °C/°F MOD PC From -10℃ to 80℃ &#x7c; eBay](https://www.ebay.com/sch/sis.html?_nkw=Digital+Thermometer+Temperature+LCD+Module+Gauge+%C2%B0C/%C2%B0F+MOD+PC+From+-10%E2%84%83+to+80%E2%84%83&_id=331895490183&&_trksid=p2057872.m2749.l2658)


---
**Ashley M. Kirchner [Norym]** *July 23, 2017 01:22*

Other gauges may have tabs or protrusions that don't match. Your mileage may vary.


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2017 01:34*

Add a cutout for an odometer so you can compare miles later :) 


---
**Ashley M. Kirchner [Norym]** *July 23, 2017 01:41*

Could be fun actually, read the travel motions from the GCODE and translate it. I nominate you **+Ray Kholodovsky** to implement that.


---
**Jody Roth** *October 06, 2017 18:22*

**+Ashley M. Kirchner**  I opened your template in Inkscape, made some edits, and laser'd some acrylic.  It came out a bit small.  That'll teach me for not measuring twice!   It appears to be about 80% smaller when I open the file Inkscape.  But in LaserWeb it gave me a profile that looked right with the 'check size' function.  I don't know by how much I resized but after burning a half dozen card stock pages I managed to get one of the right size.  



Thank you for the head start, your layout is well done!


---
**Ashley M. Kirchner [Norym]** *October 06, 2017 23:02*

**+Jody Roth**, that's possibly because you have a different DPI setting in LW than me. Mine is set to 72. Yours may be set to 96.


---
**Jody Roth** *October 07, 2017 18:28*

 That occurred to me as well. So I tried to scale up by exactly that amount and didn't turn out very well. I'll have to change the settings try again when I get back to the house thanks for the tip. 



I always design in millimeters rather than pixels so this has never really been an issue I've had to accommodate.


---
**Ashley M. Kirchner [Norym]** *October 08, 2017 02:19*

I also design everything in mm, however when I save an SVG out of Illustrator, the dpi is set to 72. That's not something I control, that's the software. In LW you can set the dpi of the files you normally work with. For me that's 72, for others it's 96.


---
**Jody Roth** *October 08, 2017 16:52*

**+Ashley M. Kirchner** Okay, I had  to use 72 dpi and the "force px per inch" setting as well in LaserWeb file settings.   


---
*Imported from [Google+](https://plus.google.com/+KenPurcell/posts/S4stgt5jGQQ) &mdash; content and formatting may not be reliable*
