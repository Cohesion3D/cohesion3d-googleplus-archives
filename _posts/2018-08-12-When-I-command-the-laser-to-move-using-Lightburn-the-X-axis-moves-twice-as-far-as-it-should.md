---
layout: post
title: "When I command the laser to move using Lightburn the X-axis moves twice as far as it should"
date: August 12, 2018 19:32
category: "FirmWare and Config."
author: "Frank Broadway"
---
When I command the laser to move using Lightburn the X-axis moves twice as far as it should. When I was using K40whisper I was able to change it’s settings for X scale factor to 0.5 and then it moved 10 when I sent the command to move 10. How can I do this with Lightburn. I’m think this might occur because I had to replace the X-axis motor when I first purchased the K40 and it may not move T the sale rate. 





**"Frank Broadway"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 12, 2018 20:29*

It's in the board's config.txt file on the memory card not in Lightburn.  

Brian might have resources for you. 


---
**Frank Broadway** *August 12, 2018 20:31*

**+Ray Kholodovsky** Thank's 


---
**Tech Bravo (Tech BravoTN)** *August 12, 2018 20:55*

im working something up for you now **+Frank Broadway**


---
**Frank Broadway** *August 12, 2018 21:08*

**+Tech Bravo** Thanks


---
**Tech Bravo (Tech BravoTN)** *August 12, 2018 21:14*

here ya go:

[lasergods.com - Steps Are Off After Replacing A Motor](https://www.lasergods.com/steps-are-off-after-replacing-a-motor/)


---
**Frank Broadway** *August 12, 2018 21:24*

**+Tech Bravo** Your solution worked perfectly! Thank you very much!


---
**Tech Bravo (Tech BravoTN)** *August 12, 2018 21:39*

**+Frank Broadway** you are welcome. 


---
*Imported from [Google+](https://plus.google.com/+FrankBroadway/posts/HQFq2wfw8NU) &mdash; content and formatting may not be reliable*
