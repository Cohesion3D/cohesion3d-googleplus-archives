---
layout: post
title: "Dropping in my C3D tonight. Connections look fairly simple"
date: January 22, 2017 04:47
category: "General Discussion"
author: "Kris Sturgess"
---
Dropping in my C3D tonight. 



Connections look fairly simple.

- X/Y Plug-in (reverse if running backwards)... I do not have ribbon cable

- Endstops Plug-in

- PSU Connector (24v/G/5V/L) - minus the 5V

- 3 pin PWM control wire. Straight plug-In???? What about K+ on my PSU side? I have no problem getting rid of the laser power control all together. Just not 100% on what is what with the wires. 



Pics below of my PSU ect.



I haven't powered anything up as of yet. Just getting warmed up ;-)



![images/21fad4b9de4bb2b60c43ca8d5f3ccd2d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/21fad4b9de4bb2b60c43ca8d5f3ccd2d.jpeg)
![images/f55ebb66021363dfe93cb2914ddbf8b5.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/f55ebb66021363dfe93cb2914ddbf8b5.jpeg)
![images/879e4f890d2ede9ac5807a04673d61bd.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/879e4f890d2ede9ac5807a04673d61bd.jpeg)

**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 04:53*

Yeah that digital control wasn't working out too well for the other guy. Skip the pwm cable, cut L on the mini side and pop it into the - bed 2.5 MOSFET. Described many times in other threads here. 


---
**Kris Sturgess** *January 22, 2017 05:29*

10-4. Then make change in config. Do I leave the other PWM stuff connected then? Just confirming.. I see you recommended keeping it connected. 


---
**tyler hallberg** *January 22, 2017 05:53*

I ended up completely changing my wiring this morning Ray as doing it the way we talked about I was not getting good results. Will post up some pics shortly on  what I changed and got good results.


---
**tyler hallberg** *January 22, 2017 05:58*

Leave the 4 plug power cable alone that goes to the cohesion board. pull out both plugs that go to the factory digital display from the psu. Remove the middle wire from the 3 wire plug going from cohesion board to the psu and put it where the single wire was going into the dual plug from the display. Sorry if none of this makes sense, I'll put up pics shortly 


---
**tyler hallberg** *January 22, 2017 06:00*

![images/881b59ff33dd03848f7a5ea46a75b29e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/881b59ff33dd03848f7a5ea46a75b29e.jpeg)


---
**tyler hallberg** *January 22, 2017 06:01*

This is how I have it right this second

![images/16aedb1833e43dd95b10cc0e6ff008f9.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/16aedb1833e43dd95b10cc0e6ff008f9.jpeg)


---
**tyler hallberg** *January 22, 2017 06:01*

Don't add m3 and m5 to the start and end code or your laser won't shut off 


---
**tyler hallberg** *January 22, 2017 06:08*

Laser power 10-30% as my laser doesn't fire under 11%

![images/084fb9777427ae45c4e530a2127e863b.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/084fb9777427ae45c4e530a2127e863b.jpeg)


---
**Kris Sturgess** *January 22, 2017 06:12*

**+Ray Kholodovsky** Well that didn't go so well. Just blew the board. I confirmed all connections. 24v-24v G-G L-2.5

Config was modified for the 2.5 change.



Popped the big round thing (capacitor) by the ribbon cable port.



Confirmed with volt meter PSU is putting out 23.8v.



Not sure how or why this happened. :-(


---
**Kris Sturgess** *January 22, 2017 06:47*

**+Ray Kholodovsky** wanna give me a call in the AM? I think I see what is wrong. 403-596-1618 or drop me a private e-mail lumpy692002@hotmail.com


---
**Tony Sobczak** *January 22, 2017 09:22*

Following


---
**Kris Sturgess** *January 22, 2017 23:40*

**+Ray Kholodovsky**​ Any update for me Ray?


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2017 00:03*

**+Kris Sturgess** Just replied to your email, thanks for your patience today.


---
**Kris Sturgess** *January 23, 2017 05:00*

Thanks **+Ray Kholodovsky** for your help, the phone call, and all around excellent customer support!


---
**Kris Sturgess** *February 02, 2017 00:08*

**+Ray Kholodovsky** Just wanted to let you know, board arrived today. I was back up and running in about 10 minutes. Laser is working like a charm!!!!! Awesome Product!


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/WWgPaj23iUw) &mdash; content and formatting may not be reliable*
