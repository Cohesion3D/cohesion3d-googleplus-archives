---
layout: post
title: "I can't read the settings on Can someone just type them out?"
date: July 19, 2017 19:35
category: "Laser Web"
author: "timne0"
---
I can't read the settings on [https://cohesion3d.freshdesk.com/support/solutions/articles/5000726545-laserweb-configuration](https://cohesion3d.freshdesk.com/support/solutions/articles/5000726545-laserweb-configuration)



Can someone just type them out? Even with the image maxmised it's just blur.





**"timne0"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2017 19:53*

I just published for LW4: [cohesion3d.freshdesk.com - LaserWeb4 Configuration : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000743202-laserweb4-configuration)



Please see if this is any better.  


---
**timne0** *July 19, 2017 20:00*

OK Done that!


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/A5oLb9hVFdv) &mdash; content and formatting may not be reliable*
