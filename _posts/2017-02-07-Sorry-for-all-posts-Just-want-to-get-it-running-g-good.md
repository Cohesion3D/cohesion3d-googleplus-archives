---
layout: post
title: "Sorry for all posts. Just want to get it running g good"
date: February 07, 2017 07:20
category: "FirmWare and Config."
author: "Colin Rowe"
---
Sorry for all posts. Just want to get it running g good. Almost there. Just little bits need sorting.

This is a little one but bugs me a lot.

Is there anyway to change the direction of the pot on the reprap screen ??. If you spin to clockwise it moves minus. Anticlockwise it moves plus. Total opposite of what it should do ( well on all cnc machines I have worked on)

Is there anywhere this could be changed?





**"Colin Rowe"**

---
---
**Colin Rowe** *February 07, 2017 08:26*

if i reverse these would that work?



panel.encoder_a_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 5


---
**Don Kleinschnitz Jr.** *February 07, 2017 12:27*

Heh I noticed the same thing. **+Arthur Wolf** can it be changed?


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 15:37*

Just change 3.25 and 3.26 with each other. Keep the symbols the way they were originally. 


---
**Don Kleinschnitz Jr.** *February 07, 2017 16:37*

**+Ray Kholodovsky** nice...


---
**Colin Rowe** *February 07, 2017 17:08*

Thanks


---
**Colin Rowe** *February 10, 2017 08:31*

Tried it last night, did not work, what am I missing? do I need to do something else?


---
**Colin Rowe** *February 10, 2017 17:17*

ok working now  for some reason??


---
**Ray Kholodovsky (Cohesion3D)** *February 10, 2017 17:19*

It wouldn't not work. So did you reset the board after saving the config? 


---
**Colin Rowe** *February 10, 2017 17:23*

How do you reset?


---
**Ray Kholodovsky (Cohesion3D)** *February 10, 2017 17:24*

Little yellow button on top right corner of board. Or remove all power and USB from board. 


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/jbuwjk654bJ) &mdash; content and formatting may not be reliable*
