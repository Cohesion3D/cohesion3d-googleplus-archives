---
layout: post
title: "I just bought the Cohesion3D remix to build my 3D printer, and i would like someone to guide me, as this is the first time i use smoothieware firmware"
date: August 20, 2017 13:37
category: "3D Printers"
author: "Mana"
---
I just bought the Cohesion3D remix to build my 3D printer, and i would like someone to guide me, as this is the first time i use smoothieware firmware.



My current setup that I want to build is a cartesian type similar to prusa i3 mk2

X-axis stepper motor - 1x

Y-axis stepper motor - 1x

Z-axis stepper motor - 2x (would like to use the unused stepper driver instead of connecting parallel into the single Z stepper driver.)

E3D Chimera hotend  - 2x heater block 

                                          1x Hotend fan 

                                          2x temperature sensor

Bondtech extruder      - 2x

BLtouch Autobed leveling sensor 1x

No heated bed.

Print fan - 1x

Reprapdiscount GLCD screen

Program use Simplify3D



12v power supply



Mechanical Endstop - 3x 

Z-axis endstop located at the top end of linear rail

X-axis endstop located at the left side of linear rail

Y-axis endstop located at the back of the printer.



1. How do I configure dual Z axis stepper motor for configuration and connecting of the motor?



2. Problem: LCD display isnt on when connected.



I hope this is not confusing to you guys.









**"Mana"**

---
---
**Griffin Paquette** *August 20, 2017 14:40*

1) I would advise against dual stepsticks for one input. Unless you seriously need the torque output, which most z's don't, it can create more problems than solve with mismatched step timings. 



2) have you double checked the wiring order? Is it showing anything at all or completely dark?


---
**Mana** *August 20, 2017 15:00*

1) how do i go about connecting dual z motor as the board only support 1 connector at Z axis driver? This my first time im converting from Ramps 1.4 configuration to smoothieware so im abit confuse there.



2) Its completely dark


---
**Ray Kholodovsky (Cohesion3D)** *August 20, 2017 15:25*

Please send pictures of wiring, front and back of glcd, etc.



Did you get the GLCD from Cohesion3D as well, or from somewhere else? 


---
**Mana** *August 21, 2017 15:29*

1) I will try connecting it in parallel for my dual Z axis stepper motor.



2)I got it from old printer. Are there any difference? I will send some picture later.


---
**Mana** *August 27, 2017 14:36*

![images/cc10b1ee6fc7075322ddddece3a7cc27.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc10b1ee6fc7075322ddddece3a7cc27.jpeg)


---
**Mana** *August 27, 2017 14:36*

![images/ac47408628098ce247c1021d58809e71.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/ac47408628098ce247c1021d58809e71.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2017 15:03*

Yes, many screens from other sources are assembled incorrectly. I cannot see from your picture but I would guess the plugs on your screen are upside down. 


---
**Mana** *August 27, 2017 15:13*

It seems to be working when i plug in to my Ramps 1.4


---
**Mana** *August 27, 2017 15:17*

![images/c8b0e6652bd4159ade6000cc55642a45.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8b0e6652bd4159ade6000cc55642a45.jpeg)


---
**Mana** *August 27, 2017 15:18*

There is a soft beep from the display when i power up


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2017 15:22*

Please compare yours to these pictures:



[s3.amazonaws.com](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080144862/original/cy9KEAjg3fh5rc__zMWDGdqL1Vg-byOMOg.jpg?1489516991)



[https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080144965/original/wuah6TgRNuc3ssjL6a6f5IopzUkmS9ZuOg.jpg?1489517133](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5080144965/original/wuah6TgRNuc3ssjL6a6f5IopzUkmS9ZuOg.jpg?1489517133)



This is the correct way.  




---
**Mana** *August 27, 2017 15:31*

Ah, the board is slightly different


---
**Ray Kholodovsky (Cohesion3D)** *August 27, 2017 15:33*

Yes, the Remix has it the correct way. Many Ramps and Chinese screens are assembled incorrectly. But if both parts are incorrect it still works :) 


---
**Mana** *September 10, 2017 17:31*

Just bought the glcd and its working now.



I'm currently working on my Z axis, and it seems that my Z axis endstop isnt stopping any movement when im moving Z axis. I currently connect my endstop to Z -min. 



May i know which line should i change to activate Z axis endstop? 


---
**Ray Kholodovsky (Cohesion3D)** *September 10, 2017 17:34*

Do you mean

1. That if you jog and hit the endstop it keeps moving?

2. That homing does not work?

Or something else? 


---
**Mana** *September 11, 2017 00:00*

yes it does not work on jogging.


---
**Ray Kholodovsky (Cohesion3D)** *September 11, 2017 00:05*

You need to enable limits in config.  



First do M119 to make sure all the switches show 0 when not pressed. 



All the info is here: [smoothieware.org - endstops [Smoothieware]](http://smoothieware.org/endstops#limit-switches)


---
*Imported from [Google+](https://plus.google.com/100658934934492802934/posts/VVQpNGyrUjd) &mdash; content and formatting may not be reliable*
