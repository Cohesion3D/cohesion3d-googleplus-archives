---
layout: post
title: "I have a Eleks Maker A3 with a C3D mini, early hand soldered model, and well considering that the laser engraving system lacks any safety features whatsoever"
date: September 14, 2018 06:41
category: "CNC"
author: "Jonathan Davis (Leo Lion)"
---
I have a Eleks Maker A3 with a C3D mini, early hand soldered model, and well considering that the laser engraving system lacks any safety features whatsoever. That it might be possible using something of the original parts it could be converted into a CNC Machine but with the Laser module being converted into like an accessory attachment. 

Though tool wise I don't really have much to work with other than a dremel and a rotory saw. Along with a number of screwdrivers, a drill, and basic tool set.





**"Jonathan Davis (Leo Lion)"**

---


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/Z2a8obhwPxw) &mdash; content and formatting may not be reliable*
