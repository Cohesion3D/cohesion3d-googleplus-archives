---
layout: post
title: "Hello, Pre-sales Questions for LaserBoard First a note to say I put the C3d mini I purchased in a K40 I purchased a couple months ago and using Lightburn I purchased through you and all is working great:-) But, I still want"
date: January 08, 2019 17:50
category: "K40 and other Lasers"
author: "Em W"
---
Hello, Pre-sales Questions for LaserBoard



First a note to say I put the C3d mini I purchased in a K40 I purchased a couple months ago and using Lightburn I purchased through you and all is working great:-)



But, I still want to get the FSL Gen 5  running since it has a large bed and all the bells and whistles already (air assist, beam combiner etc.)



Ok on to the reason for this post. Ray mentioned still wanting to see me get the FSL running and I am debating between the AWC... and the LaserBoard.



So LaserBoard pre-sale questions:



A) "...Smoothie struggles with high detail raster engraving at higher speeds.  For this we have GRBL-LPC which is blazing fast and much more barebones ..." I do a LOT of HQ raster. So will gcode Lightburn license work or do I need the DSP also? Y/N



B) Since I will want to focus on "GRBL-LPC which is blazing fast and much more barebones" does the LaserBoard come with what I need for that. I am now understanding of now the K40 and config memory chip works... So what do I need for the GRBL-LPC. Will there be support to get this running with the LaserBoard... Yes, I need to do more homework on this, but if I purchase the LaserBoard need to know what it comes with vs. what I need to know/purchase otherwise?



C) I will be using external stepper motors and already have these on hand and wired to the FSL [https://www.omc-stepperonline.com/digital-stepper-driver-10-42a-20-50vdc-for-nema-17-23-24-stepper-motor-dm542t.html?search=Digital%20Stepper%20Driver%201.0-4.2A%2020-50VDC%20for%20Nema%2017%2C%2023%2C%2024%20Stepper%20Motor%20](https://www.omc-stepperonline.com/digital-stepper-driver-10-42a-20-50vdc-for-nema-17-23-24-stepper-motor-dm542t.html?search=Digital%20Stepper%20Driver%201.0-4.2A%2020-50VDC%20for%20Nema%2017%2C%2023%2C%2024%20Stepper%20Motor%20) Will those work with the LaserBoard? Y/N



D) Since I am going to assume the stepper controllers I have should work, and looks like the same Phoenix blocks as what is on C3d site [https://cohesion3d.com/wp-content/uploads/2018/12/external-driver.jpg](https://cohesion3d.com/wp-content/uploads/2018/12/external-driver.jpg) 



Can I use your external stepper driver cable [https://cohesion3d.com/wp-content/uploads/2018/12/laserboard-external-driver-wiring.jpg](https://cohesion3d.com/wp-content/uploads/2018/12/laserboard-external-driver-wiring.jpg) to run my current stepper drivers to the Laserbard. I will need the connector for the LaserBoard since everything I have is Phoenix blocks. Y/N



E) I need the connector block/s to wire in things like the "L" etc if no Phoenix block ability. What would I need and where to purchase?



F) Do I understand correctly that I do NOT need the Graphic LCD control panel to use GRBL-LPC? Y/N



G) My knowledge and DIMS (do it my self) has really expanded over the last four+ months of the FLS journey so my Qs once I have the parts/board on hand should be minimal but want to feel comfortable asking a few support questions to get things running. 



Anything else I need to know for dong LaserBoard with Lightburn and GRBL-LPC for FSL that I didn't ask but need to know for items purchase? I already have mA (wired in) and Potetiometer (if needed).



Side note, since I only have two of the stepper drivers I linked for X, Y - I would most likely also pick up one of C3d external stepper drivers that I also linked to, in order to hook up the rotary I have and never used yet.



Ok, this looks long winded, but should be simple Q's I hope someone can help answer for decision making. THANKS ahead of time!













**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 08, 2019 19:24*

Ok, let's do this: 



A) - same LightBurn gCode license for Smoothie and GRBL.

B) - the processor in the LaserBoard and the firmware choices are identical to that of the Mini.  So have you tried doing engravings at 254 DPI above 80mm/s on your Mini and hitting any of the speed limits? 

C and D) - yes, they will work and you will need our cable pack. BUT, laserboard has drivers built in and you should use those for XY (head) and Rotary.  Unlike a DSP LaserBoard has drivers built in. 

E) - there's a screw terminal for L on LaserBoard.  Just trust me. 

F) The only consequence of choosing GRBL is that the GLCD screen won't work. Neither firmware requires it.  You <b>can</b> use it on Smoothie, and it simply won't work if you switch to GRBL.  



Having a pot already will be good. 



Ultimately LaserBoard is just a much improved Mini so you can test your requirements (the fast raster and Smoothie vs Grbl choices) with the K40 and Mini you already have. 


---
**Ray Kholodovsky (Cohesion3D)** *January 08, 2019 19:28*

And grab 2 of these in case your optical endstops end up giving you too much trouble. [cohesion3d.com - Mechanical Limit Switch](https://cohesion3d.com/shop/peripherals/mechanical-limit-switch/)



We also carry a multi turn pot if you do need that. 


---
**Em W** *January 09, 2019 00:01*

**+Ray Kholodovsky** Thanks Ray, just what I need to know.



Can you give me a link to the Pot, I would throw that in with the order.



To sum up my understanding based on what you said:



~ Laserboard

~ One more stepper driver controller for A rotary axis

~ Limit switch x 2, just in case

~ pot, just because

~ Cable pack x 3 (X, Y, A)

~ NO Display panel (didn't want that anyway. Just want to go computer direct to laser like the K40)



~ In regard to the "engravings at 254 DPI above 80mm/s on your Mini and hitting any of the speed limits" I think I have, and did not noticed an issue - well except my x axis belt seems sloppy and one engrave seemed to jump off a step/over mid way in the run. I think in tests I was engraving at 100/150/200mm sec at 250 and 500 dpi if I recall. Been a few days. I don't really do anything over about 4" and mostly smaller for engraving. 



~ Sounds lik I can decide if want to use Smoothie or GRBL once I start the conversion. Did I understand that correctly? That I can go either way and the board/files (or config help) will accomplish this. I understand for Smoothie I can start with the default and just tweak for FSL if needed? I need to do some GRBL homework, but it sounds like I don't need anything outside of what comes with the Laserboard purchase, correct? Perhaps to keep it simple if so, I will start with smoothie and get that all up and running and then move on to GRBL, which if I remember from earlier convos is a bit more complicated? What do you think...



~ The only thing you mentioned  " laserboard has drivers built in and you should use those for XY (head) and Rotary" I didn't quite get what (head) means. I will paraphrase and see if I am interpreting correctly:   Are you saying best to use the laserboard drivers for controlling/driving the X, Y and A axis (not talking about the physical stepper controllers we talked about) so I don't have to find/load other drivers from elsewhere?



Sounds like you're game, therefore so am I. Don't really think it will be that difficult given what I know now. Truly, thank you!




---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 00:08*

My word “driver” is your word “controller”. You know those green things for X and Y on Mini? There are 4 of them that are built into the board on LaserBoard. So those will probably drive XY and Rotary. 



You need 1 cable pack. It has 4 cables - just in case you need to use the black box drivers for anything. I don’t think you will. 



The pot is in the peripherals section of the store. 



If you don’t want the screen anyways then yes you can decide firmware later. You will probably start on smoothie to get the config right, yes. 


---
**Em W** *January 09, 2019 00:13*

**+Em W** BOOKMARK for me only (so I can find it later):



From Tammy Novak post

K40 and other Lasers

Kholodovsky (Cohesion3D)

Owner

GRBL-LPC for LaserBoard is now up on Jim's site: 



[embeddedtronicsblog.wordpress.com](http://embeddedtronicsblog.wordpress.com) - grbl-lpc C3D LaserBoard Firmware Builds



Use the spreadcycle version unless told otherwise.

grbl-lpc C3D LaserBoard Firmware Builds

grbl-lpc C3D LaserBoard Firmware Builds

[embeddedtronicsblog.wordpress.com](http://embeddedtronicsblog.wordpress.com)

2w

Ray Kholodovsky (Cohesion3D)'s profile photo

Ray Kholodovsky (Cohesion3D)

Owner

Download the file, rename it to firmware.bin



Turn your board off and unplug the USB Cable. Pull the SD Card, put the files on the SD Card (backup then delete the other files), then put it back into the board. Power up.  Wait for a few seconds while the LEDs count up a bit.  Then plug in the USB Cable.  You'll currently need to install the usbser.inf driver. [dropbox.com](http://dropbox.com) - GRBL-LPC Driver



That's a manual install through the Device Manager, and Win8 or higher might cause it to fail in which case you'll need to do this: 



[windowsreport.com - Install unsigned drivers on Windows 10, 8, 8.1 in two easy steps](https://windowsreport.com/12109-install-unsigned-drivers-windows-8/#.XCWQFFVKiHs)


---
**Ray Kholodovsky (Cohesion3D)** *January 09, 2019 00:19*

There’s more docs on fb, and with g+ going down in the coming months we’ll need to move over to fb soon. 


---
**Em W** *January 09, 2019 01:18*

**+Ray Kholodovsky** Order placed. THANKS again!


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/gbuMq8xJ8io) &mdash; content and formatting may not be reliable*
