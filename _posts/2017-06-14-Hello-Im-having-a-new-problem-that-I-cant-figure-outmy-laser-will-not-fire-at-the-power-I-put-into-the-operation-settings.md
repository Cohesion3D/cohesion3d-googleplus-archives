---
layout: post
title: "Hello! I'm having a new problem that I can't figure out...my laser will not fire at the power I put into the operation settings"
date: June 14, 2017 02:14
category: "C3D Mini Support"
author: "Aaron Russell"
---
Hello!



I'm having a new problem that I can't figure out...my laser will not fire at the power I put into the operation settings.  It seems like it is firing at the preset limit of 80%, regardless of what I put on the settings.  The mA reads ~ 20, even with the job set at 1%.  Any help?





**"Aaron Russell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2017 02:17*

Check comments here regarding removing that 80% limit in config.txt: [https://plus.google.com/113228590072160389068/posts/CuAW28fc8Yn](https://plus.google.com/113228590072160389068/posts/CuAW28fc8Yn)



Now, if you can't get intermediate feedback, you need to tune the pwm: [cohesion3d.freshdesk.com - Troubleshooting/ FAQ : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000734038-troubleshooting-faq)


---
**Joe Alexander** *June 14, 2017 02:22*

another possibility is if you have the potentiometer on the front panel it could have failed. see this post also 

[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)


---
**Aaron Russell** *June 14, 2017 02:49*

Thank you so much Ray! That did it


---
*Imported from [Google+](https://plus.google.com/111661714770079378479/posts/hUEX3o821Sw) &mdash; content and formatting may not be reliable*
