---
layout: post
title: "Greetings All, I am sure that somewhere is the answer I am looking for but after searching the site, I am drawing a blank"
date: September 05, 2018 19:40
category: "C3D Mini Support"
author: "Robert Hopkins"
---
Greetings All,



I am sure that somewhere is the answer I am looking for but after searching the site, I am drawing a blank.  I recently bought the Cohesion laser board and have it installed and seems to be working fine.  I am using the Lightburn software and updated to the most up to date version.  The problem I am having is that when I load a file, the laser accepts it and starts the operation.  The program will run for about 3-5 seconds and the software disconnects.  What am I doing wrong?





**"Robert Hopkins"**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 06, 2018 02:37*

There are a number of things that can contribute to this and we will need more details in order to assist. 



Show us your setup - picture of the board, wiring, etc... tell us which hardware you’ve installed exactly. 



Stock USB cable from the K40 causes issues. 

24v from stock power supply causes issues and can brown out the board. 

Power save in windows causes issues. 

We recommend isolating the board from the machine frame... use nylon standoffs or screw the board to an acrylic plate so the board screws aren’t touching the metal frame of the machine. 


---
**Robert Hopkins** *September 06, 2018 17:48*

**+Ray Kholodovsky** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/107135068190439007193/albums/6598169718091211313/6598169716713157858)


---
**Robert Hopkins** *September 06, 2018 17:48*

**+Robert Hopkins** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/107135068190439007193/albums/6598169853560820353/6598169852694747954)


---
**Robert Hopkins** *September 06, 2018 17:53*

USB cable replaced and the computer is not on power save.  I had just removed the board to check any other possible loose connections.  Please understand that I am working on this on my days off, so I may not answer right away.


---
**Robert Hopkins** *September 06, 2018 17:55*

Hardware installed is a Cohesion3D Mini Laser 


---
**Ray Kholodovsky (Cohesion3D)** *September 12, 2018 19:07*

**+Robert Hopkins** my apologies for the delay in responding.  I must have overlooked your replies.   



Disconnects can occur due to other factors in the laser. 

I would recommend making sure the frame is grounded properly and fully.  I would also recommend isolating the board from the laser frame - a 3D Printed holder, a piece of a acrylic sheet with a few holes drilled in it, or M4 nylon standoffs.  






---
**Robert Hopkins** *September 12, 2018 19:22*

What other software works with this board?  I want to make sure if the problem is with the software or the board.


---
**Ray Kholodovsky (Cohesion3D)** *September 12, 2018 19:24*

There should not be any problems with LightBurn. 


---
**Robert Hopkins** *September 12, 2018 21:07*

I am using standoffs for the board and the frame is grounded.




---
**Ray Kholodovsky (Cohesion3D)** *September 13, 2018 00:35*

I see a metal screw and nut going from the board to the frame. That is what I am saying you should avoid. 


---
**Kestrels Fury** *October 03, 2018 01:45*

This is still Robert Hopkins, but signed in with my Gmail account.

![images/de88aee19989e4785082bf83b98c68bc.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/de88aee19989e4785082bf83b98c68bc.jpeg)


---
**Kestrels Fury** *October 03, 2018 01:45*

![images/95505fe3feda1fed49dcd2a08052ee1e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/95505fe3feda1fed49dcd2a08052ee1e.jpeg)


---
**Kestrels Fury** *October 03, 2018 01:45*

![images/b7d16daa575d65d7f5399c307b835a1c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b7d16daa575d65d7f5399c307b835a1c.jpeg)


---
**Kestrels Fury** *October 03, 2018 01:46*

![images/341d83888efbd7351b13ad7f9c12b5b6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/341d83888efbd7351b13ad7f9c12b5b6.jpeg)


---
**Kestrels Fury** *October 03, 2018 01:51*

I have added a 24v 5 amp power supply to my machine and cannot get the board to initialize now.  I have checked the polarity and it is fine.  Where do I switch the power so that the clean power supply can power the board and I can get to work?


---
*Imported from [Google+](https://plus.google.com/107135068190439007193/posts/W2SNcdvL1Jk) &mdash; content and formatting may not be reliable*
