---
layout: post
title: "There is quite a variety of arduino/raspberry pi camera modules out there"
date: March 17, 2017 23:39
category: "General Discussion"
author: "Bob Buechler"
---
There is quite a variety of arduino/raspberry pi camera modules out there. Curious if there are any roadmap plans to include an optional HD camera in the K40 C3D mini upgrade bundle in the future? 



Anyone know if it's hard to find a compatible HD cam module that can see the whole bed?





**"Bob Buechler"**

---
---
**Abe Fouhy** *March 18, 2017 03:42*

**+Peter van der Walt**​ does LaserWeb allow you to auto align your text to the edge of a object with a webcam (like a phone case) and offset from an edge? Like glowforge says they can?


---
**Bob Buechler** *March 18, 2017 15:37*

Thanks, **+Peter van der Walt**​. The Logitech cams are great, and small, so it should be pretty easy to mount them. I'll plan on that route for now.


---
**Bob Buechler** *March 18, 2017 19:12*

Oh that's just fine. With all my noviceness, I won't be dealing with this enhancement until at least then. I'm still trying to learn basic focal point techniques, improve my water cooling and ventilation and other such fundamentals.


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/FjbW75cjCjX) &mdash; content and formatting may not be reliable*
