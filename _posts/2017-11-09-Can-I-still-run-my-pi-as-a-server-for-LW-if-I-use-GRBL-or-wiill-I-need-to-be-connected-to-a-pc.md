---
layout: post
title: "Can I still run my pi as a server for LW if I use GRBL or wiill I need to be connected to a pc?"
date: November 09, 2017 13:56
category: "FirmWare and Config."
author: "Andy Shilling"
---
Can I still run my pi as a server for LW if I use GRBL or wiill I need to be connected to a pc? I thought I read somewhere that GRBL uses different windows 10 drivers and thought if that's the case will the pi be different also.





**"Andy Shilling"**

---
---
**Ariel Yahni (UniKpty)** *November 09, 2017 14:38*

You can use LW server on the pi with any of the supported forwarded.  If you try to run the LW gui on the PI it will struggle.






---
**Andy Shilling** *November 09, 2017 14:50*

Thank you **+Ariel Yahni**​ note all I need to do if get the firmware to load onto the board. I keep getting no firmware found. Is there done secret to installing it as I have now reverted back to smoothie but now I've lost my glcd.


---
**Joe Alexander** *November 09, 2017 15:21*

when you swap the firmware make sure to disconnect and reconnect the usb cable. This was one of my issues as my computer wouldn't detect the GRBL firmware unless cycled. That and losing my GLCD made me swap back to smoothie.


---
**Andy Shilling** *November 09, 2017 15:32*

**+Joe Alexander**  At the moment I don't know where I am lol the grbl firmware doesn't seem to flash and now the smoothie flashes but I still have no glcd and once connected nothing wants to move of fire from the LW interface. Any other suggestions welcome.




---
**Joe Alexander** *November 09, 2017 15:35*

yep sounds familiar lol. I regularely only got the firmware to register once, if that...then it wouldnt work. A lot of flashing and rebooting did the trick. as for your GLCD did you save your original config file? drop the original firmware.bin on, let it flash(becomes .cur) then copy over the config file and reboot. Make sure the GLCD parts are uncommented, etc. After that it should hopefully be back up and running :)


---
**Andy Shilling** *November 09, 2017 15:38*

That could be my problem no config file. I should remember to check things before formatting. Is there a link to the file somewhere?




---
**Joe Alexander** *November 09, 2017 15:41*

[raw.githubusercontent.com - Smoothieboard configuration file, see ...](https://raw.githubusercontent.com/Smoothieware/Smoothieware/edge/ConfigSamples/Smoothieboard/config) here you are :) copy into a text file and tweak accordingly.


---
**Andy Shilling** *November 09, 2017 15:42*

Thank you I guess I just save as config?




---
**Andy Shilling** *November 09, 2017 15:48*

I have just tried grbl again and all I get if the first two green lights flash then nothing. **+Ray Kholodovsky** am I missing something with this. I would really like to resolve the skipping,slow running on this board. 


---
**Andy Shilling** *November 09, 2017 15:50*

Not sure if you can see the data on this but a small 11x6 raster took over an hour last night. I was doing these in about 15 minutes last year on my old moshi board.

![images/2d85cda9f512123ffd6494de56b6f848.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/2d85cda9f512123ffd6494de56b6f848.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 15:53*

Is that job being run through the pi server? 



Only thing I know in regard to flashing firmware troubles is that it can be picky about the sd card and/ or having enough power and also clean power. Please try another microsd card, format it fat32, and put only the grbl-lpc firmware.bin file on it. Put it into the board and power up with USB disconnected. 


---
**Andy Shilling** *November 09, 2017 15:57*

Ive tried 3 cards now, one of them being the card I was using smoothie on so that card should be fine. Yes that was run through the pi as a server but it has never been that slow before.


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:01*

Do you have any speed issues running a job off the microsd card and glcd menu? 


---
**Andy Shilling** *November 09, 2017 16:06*

I can't say I have tried that for some time and now I can't get smoothie back on the board I can not test it lol



I'm just reformatting the sd card now to try smoothie again.






---
**Andy Shilling** *November 09, 2017 16:16*

**+Ray Kholodovsky** how long should this flashing take?, I'm sure it wasn't over 4 minute last time I updated it.



Just checking but the config file is saved as config and not anything else Like conf or confi is it?


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:19*

30 seconds at most? The LEDs should count up in binary right away. 


---
**Andy Shilling** *November 09, 2017 16:27*

It's ok I realised the config file had only saved as Config not Config.txt. All back to how it was but I would really like some input as to why a can't get grbl to run on this board.



 I have downloaded the firmware that somebody else was linked to, copied it to the root of my card but it will not flash. All I get is light 1 2 1 and that is it.  I take it the light code does mean something?


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:34*

Take a card.  Format it FAT32.  Put nothing else on it.  Put just the firmware.bin file from my dropbox here onto the card: [cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)



Safely dismount.  Put the card in the board. 

Disconnect the GLCD from the board for now.  



Power up. 

If the LEDs do not do what I said, press the reset button and watch again.  


---
**Andy Shilling** *November 09, 2017 16:36*

Ok buddy I'll do it now. Thanks




---
**Andy Shilling** *November 09, 2017 16:39*

Also back on Smoothie but I can not home from LW now, I've put G28.1 for homing. do I need to adjust anything in the config for this.


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:41*

G28.2 for homing


---
**Andy Shilling** *November 09, 2017 16:45*

**+Ray Kholodovsky** So all I got was light sequence 121 and that was it and once I reset the board I had nothing at all?




---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:50*

How is the board being powered? 

K40 psu, do you have a separate psu, USB power trace cut? (If I remember correctly) 


---
**Andy Shilling** *November 09, 2017 16:51*

New Psu not even a year old and yes usb power cut to stop it mounting on my mac.




---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 16:53*

One more test please, power up while holding the reset button down. Wait a few seconds then release it. 


---
**Andy Shilling** *November 09, 2017 16:54*

Although I'm now using a windows system connected direct hoping to stop all the interference and problems I've had.




---
**Andy Shilling** *November 09, 2017 16:57*

OK one minute

Iv'e just checked the card and it is changing it to firmware.cur so something is happening in there.


---
**Andy Shilling** *November 09, 2017 17:04*

Exactly the same thing light 121, number 1 light stays lit but if I plug my usb lead in it goes out. and the file has once again changed to firmware.cur


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 17:04*

So I guess it flashed quicker than usual.  FIRMWARE.CUR means it worked. 


---
**Andy Shilling** *November 09, 2017 17:08*

Ok but I get no supported firmware on LW interface, could this be down to usb drivers then maybe?


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 17:09*

Ah.  GRBL-LPC does need a different driver. It's on the cprezzi github I think. 


---
**Andy Shilling** *November 09, 2017 17:16*

Ok I'm looking but not sure how  I actually download it  as I dont really use github. do I click the raw file and copy/paste it?


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 17:25*

[https://github.com/cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)



Use "clone or download" --> Download Zip to get the whole repo, then unzip it. 

As I read it, they want VCOM_lib/usbser.inf to be installed. 

I think it would be similar to the manual install procedure shown here: [http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers) - if we are working on Windows below Win10.  


---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 17:25*

I am not sure if Mac or Pi needs driver.

**+Claudio Prezzi** can you advise what the exact requirements are for grbl-lpc drivers? 


---
**Andy Shilling** *November 09, 2017 17:29*

If need be I wont use the Pi and I can share files between my laptop and mac to run them. I'd just be happy getting results Like the one's others are posting :)


---
**Andy Shilling** *November 09, 2017 18:45*

**+Ray Kholodovsky** I found this and it has now got my laptop talking to my machine. All I need to do now is work out how to get live jog and homing sorted. Hopefully now I've got this far the rest will fall into place.



[plus.google.com - Search Results - Google+](https://plus.google.com/u/0/s/installing%20grbl%20drivers%20windows%2010/top)


---
**Andy Shilling** *November 09, 2017 19:17*

**+Claudio Prezzi** can I invert my mechanical endstops in GRBL. I'm presuming they are stopping me homing.



I have no experience with GRBL so any tips would be great.


---
**Claudio Prezzi** *November 10, 2017 07:58*

Yes, you can invert the limit pins with $5=1. See [https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration) for details.


---
**Claudio Prezzi** *November 10, 2017 08:18*

And don't forget to load the GRBL default machine profile in LW4 for the correct settings.


---
**Andy Shilling** *November 11, 2017 09:56*

**+Claudio Prezzi** I have connected to grbl via putty and managed to unlock using $x, I then used $5=0 and I now have the laser homing thank you. Is there anything else I should change whilst I'm at it that you can think of that might make my life a little easier.



**+Ray Kholodovsky** Thank you for all your help again my friend I sometime wonder if you are just sat by the pc waiting for idiots like me to ask questions ;) 


---
**Andy Shilling** *November 11, 2017 10:34*

One last thing, everything is moving the correct way and homing ok but I have no laser, in the gcode I have M4 /M5 in start stop sections but under the M4 start I also have S0 (0 power) should I change this and if so is it a percentage of total power?


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/A1kS2gx4oxW) &mdash; content and formatting may not be reliable*
