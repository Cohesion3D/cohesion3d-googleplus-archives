---
layout: post
title: "Set up laser cutter with GLCD. Hi, I have a C3D Mini controller on a 40x60cm C02 Cutter with 60 Watts"
date: December 19, 2018 21:30
category: "FirmWare and Config."
author: "Rey Man"
---
Set up laser cutter with GLCD.



Hi, I have a C3D Mini controller on a 40x60cm C02 Cutter with 60 Watts.



I use it with Lightburn and I can handle all oprations well. Now I wanted to use it with the GLCD Panel.



I chose a gcode file on the display and after pressing start button the laser head grinded with a terrible noise along the top axis.



Does anyone know how to adjust the laser properties with the GLCD? I have been thinking that the controller has all information from the config and the gcode file (witch was generated with Lightburn and LB already has such information. The config is edited for 40x60 cm laser bed). 



But obviously one has to adjust dimensions with GLCD Panel. Or is there any link or tut regarding this?



Thank you





**"Rey Man"**

---
---
**Joe Alexander** *December 23, 2018 10:23*

when you setup your graphic make sure you adjust the document template to match the workspace of your machine. In inkscape I always set my document to 320x230 as this is the available workspace of my laser. Then wherever you position your graphic it should work out. also verify that your limit switch wiring is not intermittent as this will cause said grinding issue.


---
*Imported from [Google+](https://plus.google.com/117035180069976838179/posts/b9X4t4tVKuu) &mdash; content and formatting may not be reliable*
