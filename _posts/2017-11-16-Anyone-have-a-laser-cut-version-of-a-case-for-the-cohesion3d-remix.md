---
layout: post
title: "Anyone have a laser cut version of a case for the cohesion3d remix?"
date: November 16, 2017 13:17
category: "C3D Remix Support"
author: "Todd Mitchell"
---
Anyone have a laser cut version of a case for the cohesion3d remix? or at least DXF's of the panel with the USB, reset pins, etc.





**"Todd Mitchell"**

---
---
**Eric Lien** *November 16, 2017 13:30*

I can get you a dxf version of this: [https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5075567512/original/NVVBHpbQxVAEmYlHK5KrxIpPh_A1rVlA0A.png?1481222802](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/5075567512/original/NVVBHpbQxVAEmYlHK5KrxIpPh_A1rVlA0A.png?1481222802)


---
**Eric Lien** *November 16, 2017 13:32*

Also all of these 3d models are available if you want to design it in 3D: [https://cohesion3d.freshdesk.com/support/solutions/articles/5000718079-cohesion3d-remix-cad-models](https://cohesion3d.freshdesk.com/support/solutions/articles/5000718079-cohesion3d-remix-cad-models)


---
**Todd Mitchell** *November 16, 2017 14:27*

**+Eric Lien** - thanks Eric.  LOL, i opened the STEP file with a sketchup plugin.  the whole locked up:(



Anyway, this gives me what I needed!


---
**Ray Kholodovsky (Cohesion3D)** *November 16, 2017 15:22*

Eric also designed some cases for printing, it's under Enclosures there. 


---
**Todd Mitchell** *November 16, 2017 17:37*

**+Ray Kholodovsky** Yea, i saw those.  I needed more components in mine so I'll lasercut some panels.  I'll send you the skp/dxf files for the community when done




---
**Eric Lien** *November 16, 2017 18:23*

DXF:  [https://drive.google.com/file/d/1bby6-XJp28VMT9kX2mohCzG0EYVEAJsa/view?usp=sharing](https://drive.google.com/file/d/1bby6-XJp28VMT9kX2mohCzG0EYVEAJsa/view?usp=sharing)



DWG:  [https://drive.google.com/file/d/1FsgQifvHoJKsXoi5xHVNcPt-xOul9TB8/view?usp=sharing](https://drive.google.com/file/d/1FsgQifvHoJKsXoi5xHVNcPt-xOul9TB8/view?usp=sharing)





[drive.google.com - C3D00002-D.DXF](https://drive.google.com/file/d/1bby6-XJp28VMT9kX2mohCzG0EYVEAJsa/view?usp=sharing)


---
**Eric Lien** *November 16, 2017 18:25*

That print has stepper drivers populated to make sure to maintain clearance for the heat sinks.


---
**Ray Kholodovsky (Cohesion3D)** *November 16, 2017 18:26*

Damn, Eric never ceases to amaze 


---
**Eric Lien** *November 16, 2017 18:33*

**+Ray Kholodovsky** If it helps a guy like you maintain your unparalleled level of customer support... just name the task and I will do what I can to help. 



Keep up the good work man. You are truly a guy who proves that the best products are backed by even better support. I know of few companies that field support calls in the trenches like you do. Keep it up :)


---
**Todd Mitchell** *November 17, 2017 05:41*

**+Eric Lien** Hey Eric - do you mind posting DXF files of these?  I could be great to get the precise measurements




---
**Eric Lien** *November 17, 2017 06:56*

**+Todd Mitchell**​ i did. They are linked above for download.



[https://drive.google.com/file/d/1bby6-XJp28VMT9kX2mohCzG0EYVEAJsa/view?usp=sharing](https://drive.google.com/file/d/1bby6-XJp28VMT9kX2mohCzG0EYVEAJsa/view?usp=sharing)








---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2017 02:24*

Hey **+Todd Mitchell**, you posted earlier today about wiring a Kill switch but I can't find that post now.  The header right behind the kill button on ReMix/ Mini is the exactly same signal for wiring an external momentary button.  



Whether a "locking" button could work to trigger the halted state is a maybe, but "releasing" it would not recover to an idle/ unhalted state.  Momentary is best.  Even better is to use your locking button with an AC relay/ SSR/ however professional systems work to kill power to the machine in the event of a true emergency.  That's how a real E STOP is supposed to work. 


---
**Todd Mitchell** *November 20, 2017 18:33*

Hey Ray - thanks mate.  I deleted that post since I saw another question answered already.


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/e163q69qVP1) &mdash; content and formatting may not be reliable*
