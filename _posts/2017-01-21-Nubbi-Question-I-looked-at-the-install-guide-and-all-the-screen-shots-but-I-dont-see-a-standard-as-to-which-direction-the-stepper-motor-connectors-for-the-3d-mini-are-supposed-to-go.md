---
layout: post
title: "Nubbi Question - I looked at the install guide and all the screen shots but I don't see a standard as to which direction the stepper motor connectors for the 3d mini are supposed to go"
date: January 21, 2017 21:53
category: "General Discussion"
author: "Tim Mcgough"
---
Nubbi Question - I looked at the install guide and all the screen shots but I don't see a standard as to which direction the stepper motor connectors for the 3d mini are supposed to go.  On some pics I see the white wire on the left and on other pictures I see the white wire on the right.  I looked on the original pc board but there is no pin 1 marking and so I don't know which is the right way to connect the stepper motor pins for those that don't have the thin ribbon cable.







**"Tim Mcgough"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 21:56*

There's no set way and all the machines have different color coding "standards". It's a 4 pin cable. If the motor goes the wrong way flip the cable. (Power off in between). 


---
**Tim Mcgough** *January 21, 2017 21:59*

thx


---
**Tim Mcgough** *January 21, 2017 23:51*

OK Got everything plugged in and turned on and no explosions yet so that's good.  When I hook the usb cable up to the k40 and turn it on, Windows wants to format it as a drive?  Is there a different serial driver I need to be able to access the 3dmini than what had worked with the k40 before?  Thx.




---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2017 23:52*

That's what we call weird behavior.  

1. What version of windows? If it's under Win 10, then you need drivers, yes.


---
**Tim Mcgough** *January 22, 2017 00:00*

Here is the wiring in case you need it.



![images/5c96beead62826e862c698227f790bf7.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5c96beead62826e862c698227f790bf7.jpeg)


---
**Tim Mcgough** *January 22, 2017 00:04*

sorry yes, meant to put that windows 10.   What kind and were can I get them.  Thx.






---
**Tim Mcgough** *January 22, 2017 00:05*

Is this board compatible with the t2 laser software? I'll go check on that board but if you know ;-)


---
**Tim Mcgough** *January 22, 2017 00:10*

looks like the drivers are already in and ok....   Interesting

![images/c8a5bfef3b9bff589ff6bb915485d7f2.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c8a5bfef3b9bff589ff6bb915485d7f2.png)


---
**Tim Mcgough** *January 22, 2017 00:11*

Well laserweb is seeing it on port 8 but doesn't seem to move when I jog the x and y access.  Maybe I should play around a little and then get back.  Thanks for the quick response.  


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 00:14*

So, if Windows 10 then you should not need drivers.

2) Can you try this: make sure the board is powered from the laser - turn the laser on (red led will be on).  Then plug in USB.  Same symptoms?


---
**Tim Mcgough** *January 22, 2017 00:20*

Got a message back that it is not moving because of an alarm but don't know what "Alarm" it is talking about......

![images/8dbfc57ed9e41b37f4abf1fb2d07e82b.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8dbfc57ed9e41b37f4abf1fb2d07e82b.png)


---
**Tim Mcgough** *January 22, 2017 00:21*

is it ok to format the card.  I think you sent software or configurations on that sdcard so I am pretty sure I shouldn't format it....




---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 00:39*

Don't format the card. Can you answer my question #2 please. 


---
**Tim Mcgough** *January 22, 2017 00:58*

**+Ray Kholodovsky** Ray, is it possible to get a download link to the files that were installed in the sdcard.   It seems the card is corrupt and the only way I can get it to work again is to format and put those config files back on the card.  Thx 


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 00:59*

Yeah, I can put those up. But again for the 3rd time, that's not yet been confirmed as the issue. Can you, please, follow question #2 and let me know what happens. 


---
**Tim Mcgough** *January 22, 2017 01:03*

![images/81da15323bcc376053a553bea1227d9a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/81da15323bcc376053a553bea1227d9a.png)


---
**Ray Kholodovsky (Cohesion3D)** *January 22, 2017 01:19*

Hi Tim,

I am trying to better understand your issue, and unfortunately the screenshot response was not enough for me to understand what is going on.  I appreciate your patience, and ask that you respond very specifically to the following questions using words:

You said that you plugged the board in to your computer and it said the the drive needed to be formatted. 

2a) If the board is completely off (no lights, laser is off too) and you plug the board into computer over USB, can you see the drive show up and you can open it or does it say that it needs to format it or something else?

2b) With USB unplugged, if you power up the laser, and then plug USB in, what happens? Can you see the drive show up and you can open it or does it say that it needs to format it or something else?




---
**Tim Mcgough** *January 22, 2017 05:16*

Sorry Ray, I wasn't ignoring you. My PC completely quit on me and I just got another pc on line.  When I reimage my laptop I will try the steps above and get back with you. I won't reimage anything until you tell me to.  If I remember correctly though both with the Laser power off or with it on, both times it connected and said it couldn't use the device and had to reformat it.  Again, I'll update when I get back to reality ;-)   Sadly my laptop is toast now so it might be a day or two.  Thanks for the quick reply.  Tim.


---
**Tim Mcgough** *January 26, 2017 20:39*

Ok, Ray, I'm back.  I got a Raspberry Pi and followed the steps to install LaserWeb on it so the pc is out of the picture.  However, even using the Pi and Laserweb and the configurations by David Komando's pictures, I am unable to get the stepper motors to move or get access to the sd card.  Please let me know what kind of diagnostics you would like and I will provide.  Thx In advance.  Again, this is just using the Raspberry Pi Laserweb and usb to the K40 machine.




---
**Ray Kholodovsky (Cohesion3D)** *January 26, 2017 20:42*

1. A picture of the LaserWeb window.

2. Checking if you can see the drive with config file when you plug the board into a computer.  (Install smoothie drivers if Win 7 or 8, not Win 10)


---
**Tim Mcgough** *January 27, 2017 01:05*

I tried with Windows 8.1 and loaded the drivers successfully and still doesn't work.  With the laser off, or with the laser on I get the message that Windows wants to format the drive.  Here are the pics you asked for.  Thx. Tim.

[photos.google.com - LaserWeb](https://goo.gl/photos/xrG2nhruJsDTUapx6)


---
**Tim Mcgough** *January 27, 2017 20:31*

Any new suggestions? thx. Tim.




---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 20:32*

Not ignoring or forgotten about you, just haven't sat down to do the "real" tech support work yet. That's more of evening time for me. 


---
**Tim Mcgough** *January 27, 2017 20:34*

ok No hurry. Just playing hookie from work and thought I would check on a response ;-)


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 20:35*

1 full day to respond.  That's what I ask.  Then feel free to bug me again. :)


---
**Tim Mcgough** *January 27, 2017 23:03*

PS: I hooked the pi laserweb up to my Chinese 2.5 watt led laser and it was able to connect and move the x and y so I am pretty confident that the laserweb and pi are working ok.  Just can't get it to talk to the co2 laser with the mini3d board.




---
**Ray Kholodovsky (Cohesion3D)** *January 28, 2017 01:40*

**+Tim Mcgough** I messaged you on hangouts.


---
*Imported from [Google+](https://plus.google.com/+TimMcgoughSr/posts/dL6LcEcpejZ) &mdash; content and formatting may not be reliable*
