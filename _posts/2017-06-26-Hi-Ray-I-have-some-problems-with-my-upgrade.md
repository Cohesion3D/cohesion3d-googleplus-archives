---
layout: post
title: "Hi Ray, I have some problems with my upgrade"
date: June 26, 2017 20:53
category: "C3D Mini Support"
author: "Naveen Chandra Dudarecha"
---
Hi Ray, I have some problems with my upgrade. After I add a Bitmap or vector image to LW4, the rastering is only but a black area as big and wide as the boundary of the design. I have shared images with Ariel Yahni on the CNCpro g+ and so waiting for some more suggestions from him. 



In the meantime pls could you have look at the attached pics I have taken and screenshots and pls suggest what could be wrong and any suggestions to fix it. I have also added the settings and profile. 

I have a white ribbon kind of K40. The stepper motion is all good, the laser also fires after i added and M2 and M3 commands although the laser starts firing as soon as I click the play button on LW4. 



Appreciate any help pls.   





**"Naveen Chandra Dudarecha"**

---
---
**Ashley M. Kirchner [Norym]** *June 27, 2017 01:25*

I need to see more of the operation window, unfortunately your image cut off the upper part. However, just glancing at the LW screens, everything appears to be in order. Please take a screen shot showing the operation you selected and how it's configured.


---
**Claudio Prezzi** *June 27, 2017 07:43*

It seems you have M3 only in the start-gcode. This will activate the laser with the last used power. Use M4S0 instead to activate the laser in dynamic mode with zero power.

The laser power is set automatically from the CAM in each G1 line with the S param.


---
*Imported from [Google+](https://plus.google.com/108859395222728856894/posts/2h4Gj1NNDWD) &mdash; content and formatting may not be reliable*
