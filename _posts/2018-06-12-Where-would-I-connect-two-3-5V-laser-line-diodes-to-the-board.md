---
layout: post
title: "Where would I connect two 3-5V laser line diodes to the board?"
date: June 12, 2018 11:57
category: "C3D Mini Support"
author: "Wayne Ryan"
---
Where would I connect two 3-5V laser line diodes to the board? I want to hard wire them in rather than using batteries

![images/fa539e923732cd723893a6055b3929f4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/fa539e923732cd723893a6055b3929f4.jpeg)



**"Wayne Ryan"**

---
---
**Joe Alexander** *June 12, 2018 12:13*

i have the same dual laser addon and i just ran 5v through a switch on the front panel to toggle them on/off. no connection to the board necessary. note that i added a 12v1a/5v1a psu i had lying around for those and the led lighting.


---
**Anthony Bolgar** *June 12, 2018 14:30*

The line lasers take so little power you can use the 5V output on the laser PSU to power them. But if you really want to connect to the board, you could use the Zmax endstop connector.


---
**Ray Kholodovsky (Cohesion3D)** *June 12, 2018 21:22*

I don’t recommend connecting anything “extra” to the board. Use the 5v on the psu or get a separate 5v buck converter. 


---
**Joel Brondos** *June 30, 2018 16:27*

Is this make on Thingiverse? I can't find it, but I like it.


---
*Imported from [Google+](https://plus.google.com/101642450378746266949/posts/f3WfQh2Z7rq) &mdash; content and formatting may not be reliable*
