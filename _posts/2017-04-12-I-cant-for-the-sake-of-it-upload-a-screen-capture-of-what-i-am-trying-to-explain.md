---
layout: post
title: "I cant for the sake of it upload a screen capture of what i am trying to explain"
date: April 12, 2017 16:38
category: "C3D Mini Support"
author: "bob f"
---
I cant for the sake of it upload a screen capture of what i am trying to explain.

I installed the C3D Mini without a hitch. It cuts, engraves etc.

In the jog tab of LaserWeb 4 where it displays the 200x300 grid, from what my understanding should be home X0:Y200,(k-40 home) the light blue dot signifying the movement of the tool is moving around outside the grid below from where it should be. The blue dot stops at X0 Y0 when i home the laser tool.

Yet the jog controls X+ -, Y+ - work fine, the "Home All" moves the laser where it should home on the machine.







**"bob f"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 12, 2017 16:49*

Proper behavior is as follows: 

When you home (command G28.2) the head moves to the back left as this is where the K40 switches are. 

This is 0, 200 and the blue dot in LW should show in the top left corner. 



You orient your job at 0,0. 

What do you do that the blue dot is elsewhere? 


---
**bob f** *April 12, 2017 17:38*

Solved the issue, from 0 , 200, set the jog increments to 100 mm, jogged twice and set y to 0(work coordinates). Homed the laser and when it stopped the blue dot was at 0, 200


---
**John McKeown** *April 15, 2017 13:47*

Thanks for this... it's been confusing me for ages.


---
*Imported from [Google+](https://plus.google.com/100229975240583037270/posts/Vmifrx59ysc) &mdash; content and formatting may not be reliable*
