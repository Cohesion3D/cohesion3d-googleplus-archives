---
layout: post
title: "Ray Kholodovsky any idea how well the Cohesion 3D mini would fair in the Full Spectrum Laser Hobby 4th gen machines?"
date: March 12, 2017 17:49
category: "K40 and other Lasers"
author: "Ben Delarre"
---
**+Ray Kholodovsky** any idea how well the Cohesion 3D mini would fair in the Full Spectrum Laser Hobby 4th gen machines?



Attached a couple of pics of the current electronics. X and Y motor pins on the right, a combined xy end stop connector at the top, and I'm struggling to figure out where the laser is actually driven by the control board. 



The big connect at the bottom seems to indicate power, but is connected to the unit in the second photo which mostly looks like a power supply. The 4 pins the controller are hooked to are labelled '24 G 5 L' so I'm assuming thats the two power rails, and I'm guessing the Laser activation line?



![images/73e68b62049e0ab0bf350dfd9ba70694.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/73e68b62049e0ab0bf350dfd9ba70694.jpeg)
![images/aa72e23914b64a524a7f8cc1da3d8ece.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/aa72e23914b64a524a7f8cc1da3d8ece.jpeg)

**"Ben Delarre"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 17:54*

You've got all the major workings there. Psu looks very similar to the k40 one too. L is the laser Fire line, and that's what will get pwm'ed by the controller for greyscale/ power control. 



Motors will re plug right in. 

You'll have to check the endstop connector and potentially re crimp for the individual X Y endstop connections on the mini. You can check the pin out on our documentation site for the 5 pin k40 endstop connector.  So either way, just a bit of crimping may be needed. 



And the bottom thing on the board looks to be power, I see that being cut, 24v and gnd going to the power in screw terminal, and L going to the 2.5 MOSFET terminal. 


---
**Ben Delarre** *March 12, 2017 17:59*

Thats pretty much what I figured, the L line then was the last piece I wasn't sure of. Guess i'll need to figure out a mounting method too, but I can probably just 3d print an adapter or something.



The endstop connector is wired G X Y G, so yeah probably some recrimping.



I think there's a lot of unhappy FSL hobby laser owners out there who would be very happy with this upgrade.




---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 18:21*

There's enough k40 variations that some people have to drill and tap new mounting holes in the white panel. Fairly trivial with a tap-drill set from HF. 



The k40 endstops is a 5 pin jst xh, the outer 2 are unused. They ran the grounds of the 2 switches into a single pin. Or the ribbon cable...



Awesome how do I find these owners? Is there a group? 


---
**Ben Delarre** *March 12, 2017 18:35*

Yeah I just checked the internals and there's a mounting plate I can easily drill and put some standoffs into. Whats the dimensions of the mini? Wondering if I can also fit something in there to give me an ethernet connection rather than USB, maybe a Pi?



You should be able to find many disgruntled owners on [fullspectrumforum.com - FULL SPECTRUM FORUM • Index page](http://www.fullspectrumforum.com/) thats the user run forum they setup because FSL censor the official forum. The FSL retina engrave software kinda sucks, is Windows only, and has some sort of weird activation thing which confuses a lot of second hand purchasers into paying $300 for a new activation key.



The older 4th gen machines are pretty easy to get hold of now, and not horribly expensive.


---
**Ben Delarre** *March 12, 2017 18:48*

Just took this snap of the main power and laser control connector. Thinking it might be worth making up small adapter board to make this a drop in upgrade. A connector for this with power lines coming out for the screw terminals on the mini, and a adapter for the endstop cable taking the existing 4 pin jst and converting to two endstop plugs for the mini.

![images/8b016f2d6d18d044db31f940c34394af.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/8b016f2d6d18d044db31f940c34394af.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 18:57*

CAD: [dropbox.com](http://dropbox.com) - C3D Miniv2 CAD



Ok you're showing me a 6 pin power connector, there are some k40 users with something like these and they are either relocating the pins to line up with our pinout, or cutting the wires.  Like I said, 24v, gnd, and L are all accessible via screw terminals. 






---
**Ben Delarre** *March 12, 2017 19:03*

That link didn't work, just goes to [dropbox.com](http://dropbox.com) not to the file.



Yeah its pretty easy to just cut the cables and wire them in, I probably will make an adapter though since I'd like to be able to revert to factory default.






---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 19:05*

Yeah I'm not against designing something up, if we can figure out what the connectors are we can oshpark up a few.



Trying again:

[dropbox.com - C3D Miniv2 CAD](https://www.dropbox.com/sh/efgye1ebn9uatty/AABivNiYdTp4c-B7e3583MEPa?dl=0)


---
**Ben Delarre** *March 12, 2017 19:17*

That worked, and excellent there's enough space in there for a Raspberry Pi 3 and a mini board side by side!



I'll see if I can identify the connector. Looks like a pretty standard one to me so shouldn't be too hard. The endstop is definitely a 4pin jst, so thats pretty easy.



The motor pins are the same connector, though the color coding on the wires indicates perhaps they've paired up the positive and negativeve pins on either side of the connector (A-, B-, A+, B+), so that might need remapping too?



Then all I'll need is a nice 3d printed insert for the big gaping hole in the side of the box to port the ethernet and maybe pi usb/hdmi out to. Looking forward to this upgrade!


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2017 19:28*

I don't think so, if the motor works then it should work again.  



All else noted. 


---
**Pete Prodoehl** *April 14, 2018 14:43*

**+Ray Kholodovsky**  I've got the Full Spectrum 4th Generatin Series 40w Hobby Laser and plan to drop in the Cohesion3D Mini. I'd be happy to assist with documenting it, figuring out connectors, etc.




---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/2MY58g8BPQL) &mdash; content and formatting may not be reliable*
