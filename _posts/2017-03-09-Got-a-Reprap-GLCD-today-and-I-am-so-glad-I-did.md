---
layout: post
title: "Got a Reprap GLCD today and I am so glad I did"
date: March 09, 2017 23:19
category: "General Discussion"
author: "Steven Whitecoff"
---
Got a Reprap GLCD today and I am so glad I did. My computer is in another room and I can't see the K40 from it, so it was pretty nice to have control at the panel. Pretty  impressive unit for the tiny price, plug it in, done other than configuring some power settings. And will it be a nice touch for my large format machine build.





**"Steven Whitecoff"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 09, 2017 23:53*

Yeah, it's handy to have. 

For printing too, good way to keep an eye on temps. 


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/SMif2rwPz3e) &mdash; content and formatting may not be reliable*
