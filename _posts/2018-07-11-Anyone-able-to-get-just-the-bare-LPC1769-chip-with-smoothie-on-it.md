---
layout: post
title: "Anyone able to get just the bare LPC1769 chip with smoothie on it?"
date: July 11, 2018 05:01
category: "C3D Mini Support"
author: "julian contain"
---
Anyone able to get just the bare LPC1769 chip with smoothie on it?  I traced my problem down to that being basically dead after trying to install a z axis.





**"julian contain"**

---
---
**Tech Bravo (Tech BravoTN)** *July 11, 2018 05:41*

you can request a sample here: [nxp.com - LPC1769FBD100&#x7c;ARM Cortex-M3&#x7c;32-bit MCU&#x7c;NXP](https://www.nxp.com/products/processors-and-microcontrollers/arm-based-processors-and-mcus/lpc-cortex-m-mcus/lpc1700-cortex-m3/512kb-flash-64kb-sram-ethernet-usb-lqfp100-package:LPC1769FBD100?tab=Buy_Parametric_Tab&amp;fromSearch=false)


---
**Tech Bravo (Tech BravoTN)** *July 11, 2018 05:44*

keep in mind, even if you have the proper rework station and skills, this may void any support for the device you are attempting to repair. it may be better to describe what led up to the failure before you continue.


---
**julian contain** *July 11, 2018 07:28*

Basically installed the step driver backwards when testing the Z.  I wanted to see if the motor wasn't spinning because of a bad/unadjusted driver vs the motor windings not matching the default pins on the board.  Switched the Y and Z and managed to swap one backwards and the board died.  Already ordered another since that isn't covered under warranty I imagine but wanted to troubleshoot the board anyway in case anyone else does this.



24v rail is the only light that lights up.  Checked voltages and got weird stuff coming from the 3.3 and from the 5.0 when plugged into usb.  Took off the 3.3 regulator and tested it and was fine.  Was also getting 5v from USB after that.  Reinstalled the 3.3 reg and the voltages went crazy again.  Removed the LPC1769.  All voltages became nominal with a 3.3v light turning green.  Ordered the chips on aliexpress after seeing that the bootloader could be flashed on the cohesion 3d board.  If not I have an FTDI programmer that I can use.


---
*Imported from [Google+](https://plus.google.com/116932289299679953939/posts/4ssB4zfkWrX) &mdash; content and formatting may not be reliable*
