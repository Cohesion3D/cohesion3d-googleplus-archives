---
layout: post
title: "Quick question for the GRBL people, have you managed to get homing working?"
date: June 30, 2017 18:44
category: "C3D Mini Support"
author: "Tim Anderson"
---
Quick question for the GRBL people, have you managed to get homing working? I have it so it will home x, bounce off, slightly move y and then through up an error saying it didn't come off the switch (it did) 





**"Tim Anderson"**

---
---
**Jim Fong** *June 30, 2017 21:21*

I have a working 3axis firmware for the Cohesion3d Mini.  I'll post it later tonight when I get home. 


---
**Tim Anderson** *June 30, 2017 22:38*

I got em working now, I wouldn't be able to use your firmware as my X pins are dead but thank you so much for the offer It is appreciated :)


---
**Tim Anderson** *June 30, 2017 22:46*

**+Jim Fong** In fact, do you have the source for your firmware? I would love a nosey at the config.h file :)


---
**Jim Fong** *June 30, 2017 23:30*

**+Tim Anderson** I compiled this one

[github.com - cprezzi/grbl-LPC](https://github.com/cprezzi/grbl-LPC)


---
**Jim Fong** *July 01, 2017 00:54*

[https://www.dropbox.com/s/bsc931zsoyvvulk/firmware_grbl-lpc_3axis_c3dmini.bin?dl=0](https://www.dropbox.com/s/bsc931zsoyvvulk/firmware_grbl-lpc_3axis_c3dmini.bin?dl=0)



This is the grbl-LPC firmware for Cohesion3d mini with working home switch config.  3 axis version, no rotary.  **+Ray Kholodovsky** Can you copy this on your website or someplace your users can download. Before flashing your board, make backup of your configuration!!!   


---
**Ray Kholodovsky (Cohesion3D)** *July 01, 2017 02:08*

**+Jim Fong** here you go: [cohesion3d.freshdesk.com - GRBL-LPC for Cohesion3D Mini : Cohesion3D](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)



If you have more content I can post on grbl-lpc please let me know!


---
*Imported from [Google+](https://plus.google.com/109492805829132327540/posts/ZQsEVo7JPcz) &mdash; content and formatting may not be reliable*
