---
layout: post
title: "Hey All, I'm thinking my tube is kaput after some long hard months of usage"
date: May 14, 2017 03:06
category: "K40 and other Lasers"
author: "Kris Sturgess"
---
Hey All,



I'm thinking my tube is kaput after some long hard months of usage.



I have a new K40 with digital readout (mA gauge not installed yet) and recently installed C3D board.



Laser will engrave but not cut anymore.



I cleaned lenses and checked alignment but still no go with cutting.



How can I confirm if it's the tube? or possibly power supply?



Suggestions for tube replacement? (LightObjects 35w or go bigger and add oversize box?)



Any good blog tutorials on tube replacement? more concerned with the wire connections to the tube.



Any help is appreciated.



Kris





**"Kris Sturgess"**

---
---
**Joe Alexander** *May 14, 2017 03:24*

ouch sounds like you got the hit by the ban hammer, sry to hear that. As for tube connections best way I have found is to use some stranded copper wire to wrap around the silicon High Voltage wire onto the terminals. place a 1/2" long piece of water tubing around it (make a reservoir and extra layer) and fill it with silicon. I got my tube off ebay a couple weeks back from  a random buyer for $120 but recently saw [lightobject.com - LightObject](http://lightobject.com) having a sale for $99.


---
**Joe Alexander** *May 14, 2017 03:26*

ouch lightobject back up to 165


---
**Joe Alexander** *May 14, 2017 03:29*

[lightobject.com - JW 35W CO2 Sealed Laser Tube (850mm)](http://www.lightobject.com/JW-35W-CO2-Sealed-Laser-Tube-850mm-P46.aspx) heres the link for $100 tube  guess they still have it for a bit longer


---
**Kris Sturgess** *May 14, 2017 03:35*

Thanks **+Joe Alexander** I'm currently weighing my options for where to get a new tube from and what size to get. If I can go slightly bigger without upgrading PSU that would be a good option.

As for the other group, I won't go into much details other than they have a mod over there that has ZERO people skills and should not be a mod.




---
**Kris Sturgess** *May 14, 2017 03:44*

**+Joe Alexander** I was looking at that one. Looks like I would have to do the extension as it's not a direct K40 replacement. Also wondering about quality? Just browsing E-Bay now as well.




---
**Don Kleinschnitz Jr.** *May 14, 2017 11:41*

If you increase tube power, lps may also have to change!


---
**Steve Clark** *May 16, 2017 17:16*

I was just on the LO site and there is still a sale on the 35w tube. Just a heads up it is longer than the K40 tube.



[lightobject.com - JW 35W CO2 Sealed Laser Tube (850mm)](http://www.lightobject.com/JW-35W-CO2-Sealed-Laser-Tube-850mm-P46.aspx)


---
**Kris Sturgess** *May 16, 2017 20:36*

Saw that one. I also found some threads regarding cooling water conductivity causing similar issues to what I'm experiencing. Just waiting for a electrical conductivity tester to come in. I'll test my water than swap in some new water with chlorox. Hopefully that resolves it. Worth a try for $6 of water. Plan B will be tube shopping.


---
**Steve Clark** *May 16, 2017 20:58*

My understanding is flushing with  water and 1/2 cup of bleach per gallon is suppose to be ok but  then that solution must be flushed out until no bleach is left.



If your doing a test you might just test how bleach effects the conductivety of distilled water.



I don't have time right now to look up what Don Kleinschnitz testing results were but I think he may have had one with bleach added to the water cooling.



I use only distilled water and Tetra Algae Control. 






---
**Kris Sturgess** *May 16, 2017 21:17*

Don's was 6ml Chlorox to 5gal water mix.




---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/Yi2ZCBV5FkE) &mdash; content and formatting may not be reliable*
