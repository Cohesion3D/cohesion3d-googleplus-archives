---
layout: post
title: "drivers not found??? cannot connect laserweb3 to board??"
date: February 02, 2017 11:28
category: "General Discussion"
author: "Colin Rowe"
---
drivers not found??? cannot connect laserweb3 to board??

can see the sdcard though







**"Colin Rowe"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 12:29*

Need pictures and thorough explanation. Was it working before or is this what happens first time you try to connect with LaserWeb? What operating system? 




---
**Colin Rowe** *February 02, 2017 12:42*

OK away from laser for today, first time set up.

loaded laserweb3 on pc win 7 (i think)

can run all software, import and get G code for file.

board all set up and working. Can jog via screen set datums and ref the laser.

Plug in USB and says searching for drivers, then no drivers found.

It can see the SD card as a drive.

I can see COM 1 at top of screen and it connects to that but cannot control laser or upload files.

If i copy file to card in board I can play via the screen and all works great. Its just the connection.

In device manager, it has yellow triangle by "Smoothie" and looking it says no drivers loaded????


---
**Colin Rowe** *February 02, 2017 13:25*

Mature moment in rush to see it working

"If you run Windows lower than Windows 10, you may need to install the Smoothie Drivers"

OK will go find them and try again later


---
*Imported from [Google+](https://plus.google.com/+rowesrockets/posts/DXn65du6cwX) &mdash; content and formatting may not be reliable*
