---
layout: post
title: "What is the Power Supply Upgrade kit in the Cohesion3D Mini Laser Upgrade Bundle?"
date: July 13, 2018 00:48
category: "C3D Mini Support"
author: "Sebastian B"
---
What is the Power Supply Upgrade kit in the Cohesion3D Mini Laser Upgrade Bundle? I can't find any information about it on the web site.





**"Sebastian B"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 13, 2018 00:54*

It's new, we just got them in, still working on getting pics and the separate product page for it up. 



Essentially, the 24 volts coming out of the K40/ other laser Power Supply is shit - very weak amount of current and noisy.  Barely enough to run just the board - it has been known to brown out and cause the board to be unstable, and you'll blow it if you try to run additional drivers. 



As such, we have a high quality 24v power supply brick, and the adapter and wiring to connect it to your Mini.  



The goal is not to replace the power supply in your laser, but rather to have that one just drive the tube, and have this higher quality one drive the board and motors. 



It is highly recommended, and also to use a higher quality USB cable as the one that comes with the K40 is <b>also</b> known to cause communications issues.  


---
**Ray Kholodovsky (Cohesion3D)** *July 13, 2018 00:56*

![images/09a6403058a5a64ae69d1984640e5b7d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/09a6403058a5a64ae69d1984640e5b7d.jpeg)


---
**Sebastian B** *July 13, 2018 04:46*

Thanks Ray, good to know.


---
*Imported from [Google+](https://plus.google.com/115501066355460002800/posts/fq7pgx7XgnZ) &mdash; content and formatting may not be reliable*
