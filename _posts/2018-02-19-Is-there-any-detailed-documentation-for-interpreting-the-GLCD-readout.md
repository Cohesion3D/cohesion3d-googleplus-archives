---
layout: post
title: "Is there any detailed documentation for interpreting the GLCD readout?"
date: February 19, 2018 14:34
category: "C3D Mini Support"
author: "Joel Brondos"
---
Is there any detailed documentation for interpreting the GLCD readout?



How does the potentiometer on the GLCD relate to the potentiometer regulating the current on the K40 (as indicated on the analog meter)?



Is there any windows-based utility which interfaces or accesses the C3D board or the GLCD with various readouts or settings controls? E.g., is there anything in LightBurn which would "interact" with the GLCD readout?





**"Joel Brondos"**

---
---
**Richard Vowles** *February 19, 2018 17:12*

You might find answers in the smoothie documentation as that is what the board is running. Apparently the percentage is the percentage of your analog meter. So if your analog meter shows 50% and you have your power in light burn set to 25% then it will be .25 x .5.


---
**Joel Brondos** *February 19, 2018 22:39*

**+Richard Vowles** So, let me just reiterate:



1) LightBurn power for a cut in my project says 80%

2) The analog current meter is showing a peak of 20A.

3) The digital readout on the GLCD reads 100% . . .



This means I'm getting 80% of 20A?



I also noticed that the GLCD can be cranked to 107% or more? I didn't really try to max it out, I just noticed that it ran over 100% by at least that much.



So, getting the right burn out of my K40 means I have to set the % in LightBurn and monitor the result, adjusting the current manually for fine tuning. Does that sound right?


---
**Richard Vowles** *February 19, 2018 23:05*

That seems to be what it is - thats what I was doing just now as my k40 just arrived an Ray has been helping me get it going.


---
**Joel Brondos** *February 19, 2018 23:12*

You're going to LOVE it! and Ray is a great help.


---
**Richard Vowles** *February 19, 2018 23:41*

He is indeed!


---
**Joe Alexander** *February 20, 2018 11:41*

actually the analog meter is your "true" readout as it tells you how much it is currently outputting. most 40w tubes have a max output of 23ma, so 19-20ma sounds like roughly 80%. However I would keep the power between 10-15ma to preserve LPS and laser tube life. I leave my original pot set at 15ma(with the program set to 100%) so I can never exceed that, then tweak from there. As for using the GLCD pot it dynamically changes the job speed, which ramps up the x and y movement speed incrementally(100mm/s @ 101%=101mm/s)does that make sense to you guys? Ideally keep the GLCD pot set at 100% and just use it for the menu navigation.


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/ekqbFNzvgMi) &mdash; content and formatting may not be reliable*
