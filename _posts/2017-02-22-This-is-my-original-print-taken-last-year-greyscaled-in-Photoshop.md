---
layout: post
title: "This is my original print taken last year, greyscaled in Photoshop"
date: February 22, 2017 10:17
category: "Show and Tell"
author: "Andy Shilling"
---
This is my original print taken last year, greyscaled in Photoshop. Job settings were

 20mms speed, very slow I know lol

Laser diameter .2

Max power 30%

Min power 7.5%

Averaging around 5mA.

 I have an abundance of 4mm card at the moment and other than the colour I find it responds well to the laser and is ideal for testing.

![images/d64023278dca2710d5311c30069956d4.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d64023278dca2710d5311c30069956d4.jpeg)



**"Andy Shilling"**

---
---
**Alex Hodge** *February 22, 2017 16:25*

Seems you worked out your banding issues. Looks great!


---
**Andy Shilling** *February 22, 2017 16:50*

Yes changing the diameter in LW made all the difference. I now need to work on speed to power ratios.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/WuJYX6yb69J) &mdash; content and formatting may not be reliable*
