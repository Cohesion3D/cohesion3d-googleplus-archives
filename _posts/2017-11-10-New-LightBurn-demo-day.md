---
layout: post
title: "New LightBurn demo day!"
date: November 10, 2017 08:27
category: "Show and Tell"
author: "LightBurn Software"
---
New LightBurn demo day!


{% include youtubePlayer.html id="hLDGV1XlozE" %}
[https://www.youtube.com/watch?v=hLDGV1XlozE](https://www.youtube.com/watch?v=hLDGV1XlozE)





**"LightBurn Software"**

---
---
**Marko Marksome** *November 10, 2017 16:57*

Great work you are doing!


---
**David Fruehwald** *November 12, 2017 03:06*

Is this Windows only or multi-platform?  I’m interested in Linux.


---
**LightBurn Software** *November 12, 2017 03:08*

It's currently being built for both PC and Mac.  A Linux build is planned, but I do not own a Linux machine, so it will happen, but after the initial release.  The framework and code is all cross-platform, so it shouldn't take much time once I have a machine configured.


---
**David Fruehwald** *November 12, 2017 03:24*

Great news, I don’t do Windows.  I do have access to a Mac but prefer Linux so when you need to test that I’ll be happy to give it a try.


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/6aKMLZFrNvx) &mdash; content and formatting may not be reliable*
