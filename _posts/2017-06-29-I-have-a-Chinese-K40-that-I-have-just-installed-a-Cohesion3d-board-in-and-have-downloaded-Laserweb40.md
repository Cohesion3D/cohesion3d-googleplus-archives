---
layout: post
title: "I have a Chinese K40 that I have just installed a Cohesion3d board in and have downloaded Laserweb4.0"
date: June 29, 2017 15:38
category: "General Discussion"
author: "David Joe Hunter"
---
I have a Chinese K40 that I have just installed a Cohesion3d board in and have downloaded Laserweb4.0. My computer is running Windows10. I can move the laser head by jogging it in Laserweb but when I generate my code and run it the head does not move. It just burns a spot in the location the head was in when I hit the run button. I am new to this so please bear with my as I struggle.





**"David Joe Hunter"**

---
---
**David Joe Hunter** *June 29, 2017 16:08*

It's working but just moving very slowly


---
**Ashley M. Kirchner [Norym]** *June 29, 2017 16:12*

I need to see what kind of settings you're using to create your gcode. Screen captures of the settings please. 


---
**David Joe Hunter** *June 29, 2017 17:20*

![images/22b2fc80004abc5c15d35ec080abae2a.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/22b2fc80004abc5c15d35ec080abae2a.png)


---
**David Joe Hunter** *June 29, 2017 17:21*

I guess I just don't know where to change the speed at.




---
**David Joe Hunter** *June 29, 2017 17:40*

![images/d96a84c2f3d0319808711ccde32bb13b.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d96a84c2f3d0319808711ccde32bb13b.png)


---
**David Joe Hunter** *June 29, 2017 17:40*

![images/a4046d27d608f92f920b0fb8d9fd613b.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a4046d27d608f92f920b0fb8d9fd613b.png)


---
**David Joe Hunter** *June 29, 2017 17:41*

![images/450b7f0f18780656a95eea4aeb3cbcef.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/450b7f0f18780656a95eea4aeb3cbcef.png)


---
**John Milleker Jr.** *June 29, 2017 17:43*

David, on your first photo. There's a line underlined in blue called 'Cut Rate'. That's your speed, you have it set at 1mm/s, that's 1 millimeter per second. Take it to about 75mm/s for starters and try again.




---
**David Joe Hunter** *July 01, 2017 22:56*

Thank you. It's working great now!


---
*Imported from [Google+](https://plus.google.com/111180151185147927558/posts/GDBggYw9XGS) &mdash; content and formatting may not be reliable*
