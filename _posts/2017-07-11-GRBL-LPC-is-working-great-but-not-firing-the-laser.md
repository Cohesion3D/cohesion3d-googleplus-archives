---
layout: post
title: "GRBL-LPC is working great, but not firing the laser"
date: July 11, 2017 00:15
category: "C3D Mini Support"
author: "John Milleker Jr."
---
GRBL-LPC is working great, but not firing the laser. Reading tons of reports that M4 at the start of gcode in LaserWeb is all it takes.



Do I need to do something different since I have an external power supply? My laser lead is connected to P2.5/BED instead of the laser fire pin along with the power supply connector?



Laser test on the unit works fine. 





**"John Milleker Jr."**

---
---
**Jim Fong** *July 11, 2017 02:46*

You need M4 at the start of the gcode.  



If you are using cprezzi branch, he has PWM max S value set at 1000.   Grbl parameter $30



You need to make sure PWM MAX S VALUE is set to the same value.  Default is 1000. ![images/1d9a2c5f5f9451bd017eecc4b14414bf.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d9a2c5f5f9451bd017eecc4b14414bf.jpeg)


---
**Jeff Lamb** *July 11, 2017 06:22*

If you are running an external 24v supply you also need to make sure the k40 psu low side 0v is connected to the c3d 0v.


---
**John Milleker Jr.** *July 11, 2017 13:46*

**+Jim Fong** - I think what you're telling me is to change the $30 value. I am using perhaps a differently compiled CPrezzi branch that **+Ray Kholodovsky** is hosting on his pages at [https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini) - Thank you to those who compiled this and got it online for people like me that looks at the instructions and drools on the keyboard on confusion.



My value for $30 = 1000. I had adjusted those settings from a posting on the Laserweb forum with someone else saying they had a Cohesion3DMini board. With your help I changed this to $30=0 and I get laser firing. Does that sound right? I'll see if I can post my $$ configuration. Thank you for your help!



**+Jeff Lamb** - Thanks! I think I'm good there, I have my 24v coming in to the main power in header, 24+ and GND. Changing my $30 to 0 gets my laser working.


---
**John Milleker Jr.** *July 11, 2017 13:50*

This is my GRBL-LPC configuration using the build of the CPrezzi branch hosted on [https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini](https://cohesion3d.freshdesk.com/support/solutions/articles/5000740838-grbl-lpc-for-cohesion3d-mini)



It may not be perfect, but at the moment I'm getting laser firing with these settings and am in the process of running a test engraving through LaserWeb. Don't particularly like sending it through Windows now that I'm used to using the SD Card, but I am excited that there are smart people actively working to make the LCD and SD Card slots work!



 $0=10

 $1=255

 $2=0

 $3=3

 $4=0

 $5=1

 $6=0

 $10=0

 $11=0.010

 $12=0.002

 $13=0

 $20=0

 $21=0

 $22=1

 $23=1

 $24=50.000

 $25=6000.000

 $26=250

 $27=2.000

 $30=0

 $31=0

 $32=1

 $33=5000.000

 $34=0.000

 $35=0.000

 $36=100.000

 $100=160.000

 $101=160.000

 $102=160.000

 $110=24000.000

 $111=24000.000

 $112=24000.000

 $120=2500.000

 $121=2500.000

 $122=2500.000

 $130=300.000

 $131=200.000

 $132=0.000

 $140=0.000

 $141=0.000

 $142=0.000


---
**Jim Fong** *July 11, 2017 14:05*

**+John Milleker Jr.** you need to set it to any number greater than zero.  Smoothie defaults to 1, grbl defaults to 1000.  You just need to make sure Laserweb4 and grbl have the same values.  I used 1000.   



That the 3axis compiled version of grbl-lpc, one loaded in my Cohesion3d board. Been running it for the past couple of weeks and haven't had any issues so far.   Version that I modified for Cohesion3d home switch settings. 



I liked the sdcard but the performance of grbl-lpc makes up for it. 


---
**John Milleker Jr.** *July 11, 2017 14:08*

**+Jim Fong** - I just tried 1 and it's still working. 1000 didn't even try to fire the laser, no mAh needle jumps at all. Thanks for the help! Almost done a test job. A few things to iron out, I think it's acceleration or jerk as the ends are getting more burn than the middles.




---
**Jim Fong** *July 11, 2017 14:18*

**+John Milleker Jr.** just make sure pwm max s value is also set to 1


---
**John Milleker Jr.** *July 11, 2017 14:22*

In LaserWeb.. Gotcha, found it and it's already at 1. Thank you!



Should I set the $30 back to 1000 and the PWM MAX S in Laserweb 1000? Will that give me any better laser performance?



(Edit: I did it anyway, if the build is configured for 1000 as default, I upped LaserWeb to 1000 and it's working fine.)


---
**Jim Fong** *July 11, 2017 14:27*

**+John Milleker Jr.** I never tried 1 so not really sure. I don't think it would matter.  Laserweb4 just needs to know what the board was programmed $30 so it can calculate the right pwm percentage. 



Edit. This needs to be put in the grbl-lpc/Laserweb4 config docs.   I ran into the exact same problem initially.  The settings were not the same and no laser output.  


---
**John Milleker Jr.** *July 11, 2017 14:50*

**+Jim Fong** - Thanks bud for all your help, and I read your name on the Cohesion docs that you were the one who compiled this and gave it to Ray.. Thanks for that! 


---
*Imported from [Google+](https://plus.google.com/+JohnMillekerJr/posts/9Cy34iFdqEa) &mdash; content and formatting may not be reliable*
