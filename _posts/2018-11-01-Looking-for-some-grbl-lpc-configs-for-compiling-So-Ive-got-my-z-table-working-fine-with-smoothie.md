---
layout: post
title: "Looking for some grbl-lpc configs for compiling So I've got my z-table working fine with smoothie"
date: November 01, 2018 15:29
category: "FirmWare and Config."
author: "Brent Dowell"
---
Looking for some grbl-lpc configs for compiling



So I've got my z-table working fine with smoothie.  I have a limit switch on the bottom, and have it homing to the max level and an appropriate values set up so I can tell the table to move to 50.8 and lo and behold, it does it.



But I'd really like to use grbl because of the faster raster capabilities.  I've got a virtual machine setup already where I can compile grbl-lpc just fine, but was looking for some sample configs that have been used for the xyza with homing firmware that is out there.



With the xyza homing firmware I downloaded, it wants to home my z to the min, and I don't believe i can accomplish what I want with just the available $ config options.



I believe I would be looking for config.h, cpu_map.h, and default.h?



Thanks in advance for your help.





**"Brent Dowell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 01, 2018 15:51*

**+Jim Fong** is the guy for this. 


---
**Jim Fong** *November 01, 2018 17:49*

I don’t have Z table so can’t test.  Have you tried playing around with the homing direction $23 value.   If that doesn’t work, you need to modify the HOMING_CYCLE parameter in default.h under DEFAULTS_K40



Then re-compile the firmware. 


---
**Brent Dowell** *November 01, 2018 17:54*

Jim, Can you share with the the config files you used for compiling the 'firmware grbl-lpc c3dmini 4axis with XYZ homing.bin'?  



I'm having fun compiling and testing things out, but the defaults right now are causing it to home correctly, but the coordinates are wrong.  IT's some how shooting to x250, y-250 after homing.  I'm sure I can dig through the settings, but there's an awful lot of them.


---
**Jim Fong** *November 01, 2018 18:01*

**+Brent Dowell** 



Uncomment the 

HOMING_FORCE_POSITIVE_SPACE in config.h



This is only in the CPREZZI branch version of grbl-lpc. That is what I use. 


---
**Brent Dowell** *November 01, 2018 18:04*

$23 doesn't have much effect.  When it starts up, It doesn't home the X or Y, Z makes a brief blip the wrong direction, then I get an alarm:8




---
**Jim Fong** *November 01, 2018 18:14*

**+Brent Dowell** there are some features of grbl that may not have been fully tested with the grbl-lpc port.  I have never changed $23 default settings so it may have a problem.  


---
**Brent Dowell** *November 01, 2018 19:45*

Yep, Using the CPREZZI branch, and HOMING_FORCE_POSITIVE_SPACE is Un-commented.  



Are you saying that you didn't make any changes to the config.h, cpu_map.h, or defaults.h from that branch?  



If I could get those 3 files from a build that is known to work with C3D I'd be most appreciative.




---
**Jim Fong** *November 01, 2018 20:04*





[dropbox.com - grbl-LPC.zip](https://www.dropbox.com/s/f243u17phk2lpj3/grbl-LPC.zip?dl=0)


---
**Brent Dowell** *November 01, 2018 20:07*

Fantastic, Thanks Jim!




---
**Jim Fong** *November 01, 2018 20:15*

There are lots of modifications to this version of the code. Most will not be useful for you.  Careful on which #Defines you uncomment. It may break something. 


---
**Brent Dowell** *November 01, 2018 20:21*

Maybe not, but it gives me a starting point closer than the stock versions cloned from git




---
**Brent Dowell** *November 01, 2018 20:43*

Getting much much closer.  That was really a big help.


---
**Jim Fong** *November 01, 2018 20:46*

I’m at work and that may be a older version that’s on my Dropbox.   Would have to check PC when I get home. 


---
**Brent Dowell** *November 01, 2018 23:09*

Well, I've got interesting results in 2 different ways.



It seems to be related to $5 "Limit Pins Invert"/DEFAULT_INVERT_LIMIT_PINS in the defaults.h file.



 If I disable the z homing and set DEFAULT_INVERT_LIMIT_PINS to 1       

  The x and y will home just fine.  



IF I enable z homing, Z won't home.  Set DEFAULT_INVERT_LIMIT_PINS  to 0 and z will home, but the x and y wont.



I'm using that little board type micro switch with the led on it as the end stop as the z.

I Checked the switches on the k40 and they are NC, the one I'm using is NO.  It seems smoothie doesn't care if they are mismatched.



So While I probably didn't need to do all the compiling and could have just tested, this was probably good training for me as my next project will be to get my OX cnc up and running with a C3d Remix board.



So I reckon what I'm going to do is to replace the limit switches on the k40 with the same kind I have on the z axis and then go from there.



TLDR; More of a hardware/firmware issue than a straight firmware issue.



Thanks again for your help.


---
**Jim Fong** *November 02, 2018 00:07*

**+Brent Dowell** 

I recall that grbl requires all the same type of limit switches. Either all NO or NC.   



Some of the limitations of grbl is due to the limited memory of the original atmega328 /UNO main branch.  There is not any more space to add features.  So the various ports have not fully utilized the extra resources that are available. 


---
**Brent Dowell** *November 02, 2018 05:07*

And I now have complete success with GRBL and zhoming.  Built a custom firmware with all my default settings.






---
**Brent Dowell** *November 02, 2018 05:31*

![images/bd0b2cca389ce9ef6d96919bfa104af7](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/bd0b2cca389ce9ef6d96919bfa104af7)


---
*Imported from [Google+](https://plus.google.com/117727970671016840575/posts/dfeE2ibKpTp) &mdash; content and formatting may not be reliable*
