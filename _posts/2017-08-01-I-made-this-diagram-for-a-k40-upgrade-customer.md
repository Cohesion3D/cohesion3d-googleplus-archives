---
layout: post
title: "I made this diagram for a k40 upgrade customer"
date: August 01, 2017 03:46
category: "K40 and other Lasers"
author: "Ray Kholodovsky (Cohesion3D)"
---
I made this diagram for a k40 upgrade customer.  On the not-well-known HT-Master board (it's the red one, but not Moshi, apparently this is what comes in the new "upgraded" K40D now), the endstops are on a 5 pin JST, but unlike the M2NANO where there is a X wire, Y Wire, and common Ground (2 wires crimped to 1 pin), here there are 2 separate ground wires with the 2nd one on what I believe to be the lowest pin.  Since this is not connected on the Cohesion3D Mini, and since not everyone may want to recrimp that entire set of endstop connections, a quicker workaround may be to pop out that wire and move it to a ground connection on the Mini, such as the Ground pin of the 5v Aux header, all the way at the bottom left.  Just wrap the pin in electrical tape or something first, because we don't want it to short 5v to Gnd. 

![images/80bafe25a615632faf78d377b08af07b.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/80bafe25a615632faf78d377b08af07b.png)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Griffin Paquette** *August 01, 2017 03:51*

I advise using heat shrink on the Aux. header pin. Yields the cleanest and best insulation. 


---
**Marc Miller** *August 01, 2017 05:16*

You make nice diagrams Ray.  :)


---
**Don Kleinschnitz Jr.** *August 01, 2017 14:21*

**+Ray Kholodovsky** these are nice diagrams. Do you have a specialized tool that does this or is it all done in a standard graphics tool?


---
**Ray Kholodovsky (Cohesion3D)** *August 01, 2017 14:23*

To be clear... the pinout diagram itself has been around for a while, this was done by Ulf and Eric... I just scribbled the notations on it in Paint. 



I think it was done in Inkscape and other standard graphics editors. 


---
**martin courtney** *August 01, 2017 14:32*

Thank you ray I got it working,just to learn laserweb


---
**Ray Kholodovsky (Cohesion3D)** *August 01, 2017 14:36*

Glad to hear it Martin!


---
**martin courtney** *August 01, 2017 14:39*

what is GBRL


---
**Ray Kholodovsky (Cohesion3D)** *August 01, 2017 14:49*

A different firmware.  The board runs smoothie by default. 


---
**Jason Smith** *January 27, 2018 09:13*

Where could I tap into to power some LED lights 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/1sBj7vYtxgq) &mdash; content and formatting may not be reliable*
