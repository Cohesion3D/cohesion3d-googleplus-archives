---
layout: post
title: "A little confused as what is needed besides the mini bundle"
date: October 07, 2018 19:07
category: "C3D Mini Support"
author: "Angela Newingham"
---
A little confused as what is needed besides the mini bundle. I have a rotary from Randy Smith that I want to use, so could you guide me in what else I need.





**"Angela Newingham"**

---
---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 19:13*

try this: [lasergods.com - Cohesion3D Product Configurator](https://www.lasergods.com/cohesion3d-product-configurator/)




---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 19:22*

let me know how that works (its in beta :) ) but you will need the mini bundle, 1 external power supply upgrade, 1 external stepper driver, 1 external stepper driver adapter, and the 4 axis smoothie firmware and config file.


---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 19:27*

[lasergods.com - C3D External Stepper Driver Wiring Diagrams](https://www.lasergods.com/c3d-external-stepper-driver-wiring-diagrams/)


---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 19:28*

[lasergods.com - C3D 4 Axis Firmware & Config](https://www.lasergods.com/c3d-4-axis-firmware-config/)


---
**Tech Bravo (Tech BravoTN)** *October 07, 2018 19:35*

[lasergods.com - C3D Z Table and Rotary Wiring Instructions](https://www.lasergods.com/c3d-z-table-and-rotary-wiring-instructions/)


---
**Angela Newingham** *October 08, 2018 00:31*

Thanks I have the second psu already,  you helped me with that.


---
*Imported from [Google+](https://plus.google.com/101357182958712120601/posts/iTsm7P68aWr) &mdash; content and formatting may not be reliable*
