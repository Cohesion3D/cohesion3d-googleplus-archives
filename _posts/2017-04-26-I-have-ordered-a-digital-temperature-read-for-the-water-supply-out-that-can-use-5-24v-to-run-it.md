---
layout: post
title: "I have ordered a digital temperature read (for the water supply) out that can use 5-24v to run it"
date: April 26, 2017 15:50
category: "C3D Mini Support"
author: "Maker Fixes"
---
I have ordered a digital temperature read (for the water supply) out that can use 5-24v to run it. Can I run this off the board or should I tap off the power supply?





**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2017 15:52*

Not off the board, and depending on how much current it takes I might not recommend tapping it from the power supply either.  You will find that any additional load can blow it and a separate power supply is usually the safest bet for all the extras. 


---
**Maker Fixes** *April 26, 2017 17:13*

thank you just ordered one.... lol begining to wonder how much K40 I will have left compared to custom stuff. lol 


---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2017 17:15*

That tends to be how it works. 


---
**Joe Alexander** *April 26, 2017 18:15*

just the case...hehe


---
**Ray Kholodovsky (Cohesion3D)** *April 26, 2017 18:17*

Yeah, the case is all that's left Joe :). Can sub out the mechanics, ducts, fans, controllers, tubes, psu's, panels... but for some reason (namely the inconvenience and cost of sheet metal fabrication) everyone keeps the blue box. 


---
**Maker Fixes** *April 26, 2017 22:30*

Mine only worked for 2 hours, still waiting for replacement part to come in, but the next time it runs, I will have the following upgrades 

Air Assist

 LCD panel

Cohesion3D (of course)

Modified  work area increased. 

Temperature read out of water 

Improved cooling of water (Radiator )

Drag Chain (I hope)

Dual power supply

Improved ground post and plugs on machine itself

Adjustable work table

new work table (still deciding what material to use for it)

Extra external cooling fan for electronics area.

Addition lighting for work area.



Planned upgrades

Emergency Stop

New exhaust system 

Improved control panel board (still trying to figure out an affordable solution to that) 

Increase work by taking out middle wall and building a new X-Y gantry (open build here we come...) 

Safety stop on door

Better water flow sensor 

Axis Rotary addon 

Laser guides 



Oh yea then learn how to use it! lol this is my second laser but my 1st I got a couple months ago is a super carver go ahead laugh but it got me hooked!



 




---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/jAxTfnWe4ML) &mdash; content and formatting may not be reliable*
