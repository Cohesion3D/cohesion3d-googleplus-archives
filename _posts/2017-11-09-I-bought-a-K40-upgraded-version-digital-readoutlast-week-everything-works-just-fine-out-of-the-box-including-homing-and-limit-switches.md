---
layout: post
title: "I bought a K40 upgraded version (digital readout)last week everything works just fine out of the box including homing and limit switches"
date: November 09, 2017 00:58
category: "C3D Mini Support"
author: "CheRRyManCustoms"
---
I bought a K40 upgraded version (digital readout)last week everything works just fine out of the box including  homing and limit switches. The limit switch connector, 4 wire power supply molex and stepper motors are the only wires in my setup. I installed the C3D with laserweb4.  Machine Jogs and laser burns files.  The only issue is does not home on startup and limits do not stop the gantry on x or y.   I did inspect for pulled wires in the limit plug it looks ok. The connector only fits one way in the c3d for the limit plug connector, but I tried the plug in reverse in the off chance the connector was soldered on the board backwards no change .  Is there a software setting that needs to be configured? Thanks Cherryman





**"CheRRyManCustoms"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 09, 2017 01:00*

I just answered a few of those items here: 



[plus.google.com - Hey guys, i installed the coh.3d mini into my k40 and when i start the machin...](https://plus.google.com/110288443111992997281/posts/RK2zECXnvti)



Hard limits are off by default, that can be turned on in the config file but it would mean if you touch the switches the board would halt. It does not replace the actual homing of G28.2 requirement. 


---
*Imported from [Google+](https://plus.google.com/+CheRRyManCustoms/posts/QoHvqyQvCdZ) &mdash; content and formatting may not be reliable*
