---
layout: post
title: "Hi Ray not too sure where to post this, so apologies if it's in the wrong place, I installed your excellent mini3d laser bundle today, I honestly thought I had done something wrong because it went so smoothly :-) I am using"
date: January 27, 2017 19:56
category: "General Discussion"
author: "Richard Treherne"
---
Hi Ray not too sure where to post this, so apologies if it's in the wrong place, I installed your excellent mini3d laser bundle today, I honestly thought I had done something wrong because it went so smoothly :-)  I am using laserweb and do my design in Cut 2D export as dxf to lw and it is so smooth and easy it worries me because I struggled so much with corellaser. Anyway just to say that it is so easy to do, if anyone is thinking about it, just do it, you won't believe the difference it makes. So thanks again Ray and all the brilliant contributors who make this all happen.





**"Richard Treherne"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2017 19:59*

Hey Rich, you've come to the right place for sure.  There are some K40 and Laser related groups here on G+ and Facebook, take lots of pictures and spread the word!  


---
**Ariel Yahni (UniKpty)** *January 27, 2017 22:12*

**+Richard Treherne**​ Happy to hear you are happy :).


---
**Andreas Benestad** *February 07, 2017 13:09*

That is so good to hear... I am a complete newbie and received my C3d board in the mail today. The excitement about finally getting it has been somewhat replaced by a fear of complete failure in installing this thing... Would you say it is doable also for people with no programming experience? :-)


---
**Andreas Benestad** *February 07, 2017 13:25*

**+Peter van der Walt** haha! 😂 You are right about that. Just found Carl's Guide now, so I will put my trust in that. 

Thanks for the constructive input. 😉


---
**Andreas Benestad** *February 07, 2017 13:36*

**+Peter van der Walt** thanks for the link, Peter. I'm sure you get your fair share of ridiculous questions... On behalf of the ignorant tribe I represent: My sincere apologies. 😉 I'll try my very best not to be a dumbass. 👍


---
**Ariel Yahni (UniKpty)** *February 07, 2017 13:45*

We should have a name for those first free chance 


---
**Andreas Benestad** *February 07, 2017 13:59*

What happens after the first free chance...? 😉

I'll take the risk anyway. Is there another "manual" that is better or more updated compared to Carl's Guide?


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 15:44*

Read the threads in the group here. Don't run the PWM white wire. You just need to change over the existing wiring going to the old board. 

Post a new thread in this group with pictures of your machine and we can take it from there. 


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 16:14*

**+Peter van der Walt** yes. I am no longer recommending the pot swap approach. And since the L in the power connector on this board rev is not a PWM capable pin, they need to wire L to the bed MOSFET - 2.5 terminal and make the change in config from 2.4! to 2.5 for the laser pin. 


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 16:16*

Still 1 or 2 I think but they decide to contact me off list. "Yay" :)

And I fixed the pin in the power connector to 2.5 for the next revision being manufactured now. It pulls from the bed MOSFET now. 


---
*Imported from [Google+](https://plus.google.com/117933873290891611475/posts/aLSSp11tzKv) &mdash; content and formatting may not be reliable*
