---
layout: post
title: "Installed my LCD tonight. What do most of you use your LCD for?"
date: February 03, 2017 04:49
category: "General Discussion"
author: "Kris Sturgess"
---
Installed my LCD tonight. 



What do most of you use your LCD for?



I can JOG X/Y but don't see any other major value for it????



Is there advance features that can be setup for it? Examples?





**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 04:54*

Also playing files from the memory card (some people prefer to preload the job instead of streaming over USB) and test firing using the commands in the laser sub menu (depends on which wiring configuration you have, since the replacing the pot renders red test button useless - you have to set a fire level then press the red button). 


---
**Kris Sturgess** *February 03, 2017 05:07*

I didn't see a laser sub-menu. Might have missed it. 



I'll look again in the AM. 



I have the "digital" pot which is still connected. Test fire still works but if i re-cut a new panel I may relocate it internally as I control power via Laseweb anyways.



SIck as a dog again...grrrr


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 05:08*

Click the encoder to access the menues.  Scroll all the way down.  It should be there. 

Feel better bud.


---
**Kris Sturgess** *February 03, 2017 05:29*

**+Ray Kholodovsky** do the C3d boards ship with the firmware-cnc.bin? Just ran downstairs and ya not seeing the laser sub-menu.




---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 06:04*

They should. But maybe your replacement didn't have it.  They come with regular firmware flashed.  Here's the link, just grab the firmware file from either folder, put it on the card, and reset the board. 

[dropbox.com - C3D Mini Laser v2.2 Release](https://www.dropbox.com/sh/42l2ps3l53xq79t/AAAP9pLoaG9BDDwyrR3d85Sla?dl=0)


---
**Kris Sturgess** *February 03, 2017 06:33*

That did the trick. Thanx again.


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 06:41*

Anytime. 


---
**Bill Keeter** *February 03, 2017 13:26*

yeah, I load my cut files onto the SD card. That way I don't have to walk my laptop to the garage every time I need to cut something. Just save to SD card and go.


---
**Kris Sturgess** *February 05, 2017 23:58*

**+Bill Keeter** tossed a small file to my SD card and it worked like a charm. Just working on a new panel layout for mA and LCD components.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/NbLKEaLnMLL) &mdash; content and formatting may not be reliable*
