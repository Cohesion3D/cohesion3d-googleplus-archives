---
layout: post
title: "I have just installed the C3D laser board in my K40 laser"
date: February 06, 2019 11:39
category: "K40 and other Lasers"
author: "Adrian Rietdijk"
---
I have just installed the C3D laser board in my K40 laser. Installation went without a hitch and everything seemed to work well until I started doing some serious projects. I found that the Y axis is well out of calibration. All vertical dimensions are about 15% too large. Is there some way I can calibrate the axis in the software or is there a setting that I am not aware of? Any help would be greatly appreciated.





**"Adrian Rietdijk"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2019 16:14*

G+ is shutting down and we are moving to [forum.cohesion3d.com](http://forum.cohesion3d.com) 



Please post your question again there and we will be happy to help. [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](http://forum.cohesion3d.com)


---
*Imported from [Google+](https://plus.google.com/112590888995938365610/posts/ggGePhUDFJ3) &mdash; content and formatting may not be reliable*
