---
layout: post
title: "Figured I would share and see if anyone wanted the STL"
date: February 05, 2017 20:54
category: "Thank you and other tips"
author: "Bill Keeter"
---
Figured I would share and see if anyone wanted the STL. I made a quick bracket that rotates the C3D so the SD card faces up. As I run jobs directly from the card using a GLCD. 



![images/99653ed65ef83ffd4905fb0896e94999.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/99653ed65ef83ffd4905fb0896e94999.jpeg)
![images/d96bff459d61b15dbe67d42f194f96fb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d96bff459d61b15dbe67d42f194f96fb.jpeg)

**"Bill Keeter"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 06, 2017 00:19*

I like it. 


---
**Bill Keeter** *February 06, 2017 01:38*

Here's the STL on Thingiverse if anyone wants it. 



[thingiverse.com - Cohesion3D 90 degree bracket by lions3](http://www.thingiverse.com/thing:2088044)


---
**Don Sommer** *April 03, 2017 01:50*

Whats the largest flash card the mini recognizes?


---
**Ray Kholodovsky (Cohesion3D)** *April 03, 2017 01:52*

32 gigs enough for you?  I've seen a few people use that.  Just have to format it to FAT32... 


---
**Don Sommer** *July 25, 2017 01:07*

i like the idea of turning it 90






---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/KNPBASaEkXs) &mdash; content and formatting may not be reliable*
