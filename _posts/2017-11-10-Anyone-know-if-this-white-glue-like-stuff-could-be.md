---
layout: post
title: "Anyone know if this white glue like stuff could be?"
date: November 10, 2017 09:11
category: "General Discussion"
author: "Robert Hubeek"
---
Anyone know if this white glue like stuff could be? 

![images/26e653e61278f4bbd345f2c7446edf56.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/26e653e61278f4bbd345f2c7446edf56.jpeg)



**"Robert Hubeek"**

---
---
**Jorge Robles** *November 10, 2017 09:24*

Ultrasonic alcohol bath will solve it.


---
**Robert Hubeek** *November 10, 2017 09:32*

Guess i need to find a place that will do that :) here in australia. 




---
**Greg Nutt** *November 10, 2017 11:41*

If this white glue stuff like could be, then let it be not. 


---
**Griffin Paquette** *November 10, 2017 13:35*

Hmm strange. Did you get it like that?


---
**Ray Kholodovsky (Cohesion3D)** *November 10, 2017 14:37*

Some of the early remix boards came a bit less than clean in this regard. I did my best to clean them up before shipping. 

Options: 

Do nothing. 

See if you can get it off with just a toothbrush. 

Use isopropyl alcohol, followed by deionized water to wash it off. 


---
**Brad Hill** *November 11, 2017 17:19*

#3 is that a collector's item?


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2017 17:22*

Maybe in 20 years. But there's also quite a few ones I hand made that are even less clean than that. 


---
**Steve Clark** *December 20, 2017 01:07*

Did you happen to use Super Glue around the board or in your cabinet? It looks a lot like vapor from that type of glue in the picture.  Sorry for the late post...


---
**Robert Hubeek** *December 20, 2017 02:14*

I have not used anything. i have not even connected power to it yet. I will this weekend and see what happens. it will either work and all is fine or i will be on the market for a new board. 


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2017 02:23*

Keep me posted. 


---
*Imported from [Google+](https://plus.google.com/+RobertHubeek/posts/NR45Hcen9kn) &mdash; content and formatting may not be reliable*
