---
layout: post
title: "Ok just dove into the world of Laser, went and got the worst money can buy the k40 type"
date: April 17, 2017 11:38
category: "C3D Remix Support"
author: "Maker Fixes"
---
Ok just dove into the world of Laser, went and got the worst money can buy the k40 type. I am looking at upgrading (well upgrading more have had it 2 days and have already been up grading)  I am looking at getting the upgrade kit offered, what extra features does the remix version offer? 





**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 16:17*

Hi Calvin!



The Mini is the one with all the connectors specific to the k40 hence making it a drop in upgrade for this machine. 



The Remix works for sure in it (a few people are running it in their k40's) but rewiring was required.  



Ultimately the ReMix is a high performance board that can handle multiple head 3D Printers as well as CNC's, and Lasers, while the Mini is for a single head printer but also has the added bits to make it drop in to the k40 wiring. 


---
**Maker Fixes** *April 17, 2017 20:48*

Ok thanks I just got my k40 and most of the time the laser won't fire but it does everything else. I am thinking bad board. One advantage of the laser not firing I never run out of materials! 


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 20:49*

If you press the test fire button, will the laser fire?  Try that a bunch of times.  


---
**Maker Fixes** *April 17, 2017 21:22*

not right now, the machine worked great last night, this am not, then all of a sudden it worked for on run, now its back to not working


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 21:26*

If the physical test fire button does not get the laser to fire then there is a chance this is not board related. 


---
**Maker Fixes** *April 17, 2017 21:36*

I was planing on getting a better power supply when funds allow, may be soon than I wanted. In any case from what I read the stock ones are pretty dangerous 


---
**Maker Fixes** *April 17, 2017 22:09*

think I found it one of the pins on the power supply is not connected (may have been laying on pin before I found it.  Now I have the project of getting that pin to stay connected, so far no luck its in the G in5v plug (the right more wire while looking at the power supply. the old dead big fingers gonna be fun but I think the connector is bent




---
**Maker Fixes** *April 17, 2017 22:27*

anyone know what size those pins are looks like I need to get something to hook it up


---
**Ray Kholodovsky (Cohesion3D)** *April 17, 2017 22:28*

It's a JST 3.96mm connector. 

What's cool is that my board has screw terminals available for the 24v, gnd, and L from that connector so you could just cut and deal with the bare wires. 


---
**Maker Fixes** *April 18, 2017 00:29*

ok I got the wire hooked up. LEDs numbers for Laser power are now lite up, the test button in the power supply does fire the laser (good news laser still good)  but the test button on the control panel  does light up the light that indicates it connected but does not fire the laser


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/96tJqi27pc1) &mdash; content and formatting may not be reliable*
