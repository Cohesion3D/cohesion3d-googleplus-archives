---
layout: post
title: "LightBurn 0.8.07 released - equations, color camera view, AWC grayscale, and much more:"
date: December 14, 2018 08:03
category: "Other Softwares"
author: "LightBurn Software"
---
LightBurn 0.8.07 released - equations, color camera view, AWC grayscale, and much more:



[https://lightburnsoftware.com/blogs/news/lightburn-0-8-07-equations-color-camera-view-grayscale-ppi-for-awc-and-more](https://lightburnsoftware.com/blogs/news/lightburn-0-8-07-equations-color-camera-view-grayscale-ppi-for-awc-and-more)





**"LightBurn Software"**

---


---
*Imported from [Google+](https://plus.google.com/110213862985568304559/posts/C9vzng9AGtQ) &mdash; content and formatting may not be reliable*
