---
layout: post
title: "I have a newer K40 laser machine with digital power setting on machine"
date: March 20, 2018 13:58
category: "General Discussion"
author: "Brad Januchowski"
---
I have a newer K40 laser machine with digital power setting on machine. Any issue when upgrading to the Cohesion3d board?





**"Brad Januchowski"**

---
---
**Jake B** *March 24, 2018 12:30*

I have the same.  I intend to replace the digital control with a pot eventually, but it is not required to use a C3D mini.  The power setting becomes simply the "max power" that can be commanded by the C3D board (and the power at which test fire occurs)


---
*Imported from [Google+](https://plus.google.com/111956962431778575847/posts/HcrERgtqQf2) &mdash; content and formatting may not be reliable*
