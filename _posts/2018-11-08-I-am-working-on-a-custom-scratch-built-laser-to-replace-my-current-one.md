---
layout: post
title: "I am working on a custom scratch built laser to replace my current one"
date: November 08, 2018 23:05
category: "C3D Mini Support"
author: "Greg Smith"
---
I am working on a custom scratch built laser to replace my current one.  I'm thinking that the C3D Mini might be a good engine but I'm having trouble tracking down any tech data on it that is not phrased in terms of how well it works with K40 parts.  My current system is not a K40 and I don't know the specs on the the K40 parts.  



Can anyone point me to other scratch built laser project info based on the C3D mini or generic tech data that I can use when sourcing my other parts?





**"Greg Smith"**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 08, 2018 23:09*

What kind of info are you looking for? 



There are some people driving custom machines with C3D, this one comes to mind: [manmademayhem.com - LAYZOR upgrades](https://www.manmademayhem.com/?p=3505)


---
**Anthony Bolgar** *November 09, 2018 03:02*

The laser PSU's are generic, just size it to your tube, the C3D mini can control the laser output. Use a separate PSU for your motors, 24V 10A will handle any Nema17 Stepper you end up using. The stepper drivers are step stick style, use whatever drivers you want, and if the step stick drivers can't handle the current draw of your motors, you can always wire it up using external steppers. That is all you need to know, and basically, as long as your machine uses a CO2 glass tube laser source or a diode laser source, uses stepper motors not servo motors, you can use the C23D mini to run it.




---
**Greg Smith** *November 10, 2018 01:48*

Thanks.  I had to set aside my project for a little while and am just restarting it.  I remember getting stuck trying to match up components and that I was frustrated  trying to get more details.  I'll look at what you provided and post some more detailed questions as I refresh my memory.


---
*Imported from [Google+](https://plus.google.com/112338849306977866669/posts/iKXmueVowmi) &mdash; content and formatting may not be reliable*
