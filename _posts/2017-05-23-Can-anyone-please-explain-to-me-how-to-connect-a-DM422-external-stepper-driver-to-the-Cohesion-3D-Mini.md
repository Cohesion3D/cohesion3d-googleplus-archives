---
layout: post
title: "Can anyone please explain to me how to connect a DM422 external stepper driver to the Cohesion 3D Mini?"
date: May 23, 2017 08:13
category: "C3D Mini Support"
author: "Andy Knuts"
---
Can anyone please explain to me how to connect a DM422 external stepper driver to the Cohesion 3D Mini? 

I'm using the C3D breakout board which has 4 pins and connected it to the DM422 like this:

GND->GND

DIR->DIR

STP->PUL

EN->ENA



Then I connected 24Vdc to the DM422 stepper driver and the stepper motor to A+/A-/B+/B-.



But when I go to my menu -> Jog -> Z and try to adjust Z height, it doesn't do anything...



There's a led on the DM422. When I power on the machine, the led is on too. So it does have power.



The DM422 also has a pin called "OPTO" which I have not connected. Do I need to connect this to a +5v source? ( for example the +5V pin of the servo pins on the C3D Mini? As I'm not using a servo anyway...)



Or did I do something else wrong?



I installed the C3D breakout board in this direction: the "U" mark on the side matches the one drawn on the C3D Mini board, that's correct, right?





**"Andy Knuts"**

---
---
**Jim Fong** *May 23, 2017 08:59*

OPTO does need to be connected to +5 volts for the driver to work. 


---
**Andy Knuts** *May 23, 2017 09:04*

Yeah, I figured. But connecting +5 didn't help at first. I also had to invert the gamma_step_pin, gamma_dir_pin and the enable pin in the config in order to make it work.



It's working now ;)






---
**Jim Fong** *May 23, 2017 09:38*

**+Andy Knuts** they should also be open drain output too.  


---
**Andy Knuts** *May 25, 2017 08:24*

Yes,  added "!o" to the pins and after that it worked perfectly fine ;)




---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2017 02:08*

**+Jim Fong** ever tried to see if we can use +3.3v as the positive end for the opto instead of 5v?  Seeing as how everyone is wiring this way with common anode I'm thinking about updating the adapter to include the +, but only 3.3v is available from the pololu socket. 


---
**Jim Fong** *May 26, 2017 03:52*

**+Ray Kholodovsky** it really depends on the opto and the series current resistor used in the driver design.  3.3v may not be enough to fully turn on the opto. Some like the newer Geckodrives and the Parker e-dc stepper drivers I use are 3.3volt compatible.  



I would add the 3.3volt terminal.  The end user will have to test and see if it works for their driver. If it doesn't then they would have to grab +5 from somewhere else (from USB port for example).  














---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2017 04:02*

Jim you have any TB6600's? 


---
**Jim Fong** *May 26, 2017 04:12*

**+Ray Kholodovsky** I have a single driver version of the TB6600. 


---
**Ray Kholodovsky (Cohesion3D)** *May 26, 2017 04:18*

I'll test the ones I have (TB6600 in the black box) on 3.3v. I'm thinking about stocking them - I figure most people are doing Z beds and that'll be enough for it. Not sure if there's any quality concerns with using them for a rotary (increased precision requirements or something?) 


---
**Jim Fong** *May 26, 2017 04:37*

**+Ray Kholodovsky** TB6600 are OK, some are better made than others from what I've seen on CNCzone. The one I bought to test exhibits bad resonance issues with stepper motors that have high inductance.  I have several dozen different stepper motors that I use to test drivers with and the ones that have low (<3mh) inductance work best with the 6600. 






---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/TcTqr4QhQ4L) &mdash; content and formatting may not be reliable*
