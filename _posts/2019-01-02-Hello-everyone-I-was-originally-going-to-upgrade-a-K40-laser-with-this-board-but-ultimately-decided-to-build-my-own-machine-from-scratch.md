---
layout: post
title: "Hello everyone, I was originally going to upgrade a K40 laser with this board but ultimately decided to build my own machine from scratch"
date: January 02, 2019 19:53
category: "General Discussion"
author: "Caleb Chico"
---
Hello everyone, I was originally going to upgrade a K40 laser with this board but ultimately decided to build my own machine from scratch. I should be to power it within a few days. Here's how she is looking so far. I'm going to attempt to drive XY and Z, and I'll be using a rotary tool from time to time so I'll be using all 4 drivers. Just mentally prepping everyone for when I start asking questions. Cheers



![images/b6d793ef9128832de6f4a99ef01c1d41.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b6d793ef9128832de6f4a99ef01c1d41.jpeg)
![images/a80cdcced883d1ac128b4b61b19d3f72.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a80cdcced883d1ac128b4b61b19d3f72.jpeg)
![images/99db0f7503a615396e1b02cb06e97e28.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/99db0f7503a615396e1b02cb06e97e28.jpeg)

**"Caleb Chico"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 02, 2019 20:10*

WOW.  



What motors are you using for each axis? 


---
**Caleb Chico** *January 02, 2019 20:48*

**+Ray Kholodovsky**  I'm using nema 23 motors. It's still a work in progress but it finally looks like a machine. I'm attaching an old windows surface on the top of the machine and control it with LightBurn via USB or sending the commands through the reprapdiscount controller. 


---
**Caleb Chico** *January 02, 2019 20:50*

![images/30eaad035f76981dc71be61cc3f28ff2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/30eaad035f76981dc71be61cc3f28ff2.jpeg)


---
**James Rivera** *January 04, 2019 18:42*

Wow! Looks like a beast. Shiny!


---
**Caleb Chico** *January 04, 2019 20:23*

**+James Rivera** Thanks. I’m glad you said that because I was thinking of naming her the RGB Beast. I’m having too much fun with this project. Now I’m trying to come up with a logo. Thinking squid or octopus bc they can change colors like a chameleons. 


---
*Imported from [Google+](https://plus.google.com/109382398188341438774/posts/ZJzDMaaKEKU) &mdash; content and formatting may not be reliable*
