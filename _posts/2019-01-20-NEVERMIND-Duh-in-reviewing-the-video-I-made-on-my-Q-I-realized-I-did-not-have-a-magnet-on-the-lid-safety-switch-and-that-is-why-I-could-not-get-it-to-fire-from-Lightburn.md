---
layout: post
title: "NEVERMIND!!! Duh, in reviewing the video I made on my Q I realized I did not have a magnet on the lid safety switch and that is why I could not get it to fire from Lightburn"
date: January 20, 2019 20:20
category: "K40 and other Lasers"
author: "Em W"
---
NEVERMIND!!!



Duh, in reviewing the video I made on my Q I realized I did not have a magnet on the lid safety switch and that is why I could not get it to fire from Lightburn.



I put a magnet on the lid safety and now, YES, YEAH, it is firing. All is well in the laserboare/lightburn FSL conversion!!! :-)



The only issue I may have left is that the Y axis bumps/stops at a certain point when I comes toward a lower left origin. It stops about half way to the bottom (I have a 12 Y by 24 X inch bed. I will play around with that first as I have some ideas. But I may be back to ask for feedback if I can't figure that one last thing out.



THANK you so much for all the help and patience shown to me during this FSL Gen 5 conversion journey. It was actually pretty simple once I figured it all out with help from this board.







**"Em W"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 17:32*

I’m not one to say to take apart a good thing, but:

You can hook the endstops up to 2 of the 3 pins “Sig” and “Gnd” on the dedicated endstop connectors (bottom row) 

X min and Y max ones. 

Should work with no config changes or anything. 



You can hook up the x and y motors directly to the LaserBoard - it has built in drivers. You don’t need to use the external drivers. There’s a header and a screw terminal for each position. You may need to increase the motor current in the config file if you have grinding when you try to jog. 


---
**Em W** *January 21, 2019 19:24*

**+Ray Kholodovsky** WOW, great ideas. I understand the endstops idea and would be simply to move them there. I do need to extend the wires because they are way to short  and assume there is no issue to do so.



Also understand about hooking up the stepper directly to the board. I see on the underside of the board it speaks my language (if I remember correctly for diagramming it) that is notes terms like B+, B-, A+, A- which is how the FSL spoke about it - so that part should be simple if I move them over.



Was thinking the external would be better for not over taxing the power to the laserboard since I had them? I do intend to hook up an A axis at some point since I already have the rotary I purchased the same time as the FSL. They had it to where if I wanted to use it, I would unhook the Y axis and plug in the router since the are just phoenix blocks.



What I am not clear about is the motor current change. I know my steppers are 1.3A which I needed to know for the external stepper dip switch setting.



In the config file I see this line, for example:



beta_current                                 1.5              # Y stepper motor current



and was wondering if I actually needed to change that to 1.3? or is that even the line that talks about the stepper Amp setting?



Again, thanks. 


---
**Ray Kholodovsky (Cohesion3D)** *January 21, 2019 19:58*

Just keep the motor wires in the same order and put them to the screw terminal. 



Set the current to 1.2 amps which is the max the LaserBoard drivers can do, but it’s a different unit of measure so don’t worry about this. 



I was still interested in figuring out how to support the stock homing sensors btw. 


---
**Em W** *January 22, 2019 19:07*

**+Ray Kholodovsky** Oh OK, I did a video for you about the stock homing sensors that should, hopefully, give the details to help determine where to put the wires/s and what connector needs to be used:-) Thanks!




{% include youtubePlayer.html id="k-HjhORNkl8" %}
[youtube.com - ems-gallery's live br11oadcast](http://youtu.be/k-HjhORNkl8) 


---
**Ray Kholodovsky (Cohesion3D)** *January 23, 2019 04:18*

I'm not going to be able to take a look for a bit, please poke back in a few days.


---
**Em W** *January 23, 2019 17:16*

**+Ray Kholodovsky** No hurry at all. This is/was just an FYI for you. I am not personally going to worry about the stock limits any time soon since the mechanical ones I purchased from your suggestion are working. 



Truth be told, I want to play with the laser now that it works, and after spending five months obsessing with the conversion, am ready to put it behind for now.



I truly thank everyone who helped with this journey:-)!!!


---
*Imported from [Google+](https://plus.google.com/100127157283740761458/posts/3anbZpP56pd) &mdash; content and formatting may not be reliable*
