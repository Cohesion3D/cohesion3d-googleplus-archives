---
layout: post
title: "Can't seem to control my laser power from laserweb"
date: August 06, 2017 23:36
category: "C3D Mini Support"
author: "Christopher Pennington"
---
Can't seem to control my laser power from laserweb.



I see people wiring stuff to the board but don't know which or what i should be wiring in order to control that. pics of my setup attached.



![images/a1bbfd760ebdbf8ff991c4aa32d37b11.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/a1bbfd760ebdbf8ff991c4aa32d37b11.jpeg)
![images/b27ba55475d75228e32b84b86792f02e.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b27ba55475d75228e32b84b86792f02e.jpeg)
![images/9667120946f949f167285d50429e609d.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/9667120946f949f167285d50429e609d.jpeg)
![images/895063f5d9979015965e4950c2d85a22.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/895063f5d9979015965e4950c2d85a22.jpeg)
![images/be5e613f2a18727e902e5fe94d94df88.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/be5e613f2a18727e902e5fe94d94df88.jpeg)

**"Christopher Pennington"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 23:46*

Hi Christopher, 



The laser firing is part of the standard wiring in the instructions. You shouldn't need to do anything else. 



Could you elaborate on what you are doing to fire, what you expect to happen, what is actually happening, etc. 



And other than "your basic wiring seems correct" and "you have a digital panel style machine" I'm not able to make out anything else from those pictures. 


---
**Christopher Pennington** *August 06, 2017 23:54*

The only thing I'm trying to do is set the power level.



When I set it via GLCD, the change isn't reflected on the build in digital power display. See attached pic: GLCD says 93% power, built in display says 80%



If I set it via S0.4 it does not show 40 on the digital display.

![images/d19f70a222c92330e4394094e9d32c8f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/d19f70a222c92330e4394094e9d32c8f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 23:59*

These are separate systems. 



You use the pot or digital display of the k40 to set the power max. 10mA is good, I don't know what the % equivalent is. 



Then your power level in the GLCD/ LaserWeb/ the job is a proportion of that. 



You just need to run a G1 X10 S0.6 F600 line to the board from LaserWeb and you should see the head move while laser firing. 


---
**Anthony Bolgar** *August 07, 2017 00:01*

The display is the overall limiting of the power level, so if it is set at 80% and the GLCD is set at 40% you are effectively setting the power at 40% of 80%. The C3D board does nor effect the digital display. SO what is happening is that you are limiting the max power that the C3D board can vary. Hope this makes sense to you.


---
**Anthony Bolgar** *August 07, 2017 00:09*

On my digital display K40 65% is approximately 10mA. I will be installing a mA meter tonight so I will be able to give you a more exact value once it is installed.


---
*Imported from [Google+](https://plus.google.com/109465610998019460623/posts/9AbLqG4HUr8) &mdash; content and formatting may not be reliable*
