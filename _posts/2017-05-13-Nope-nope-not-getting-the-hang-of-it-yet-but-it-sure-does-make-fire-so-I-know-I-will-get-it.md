---
layout: post
title: "Nope nope not getting the hang of it yet, but it sure does make fire so I know I will get it!"
date: May 13, 2017 00:41
category: "General Discussion"
author: "Maker Fixes"
---
Nope nope not getting the hang of it yet, but it sure does make fire so I know I will get it!



![images/1d5c56cab8a1f87435373cee1c2bfe68.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1d5c56cab8a1f87435373cee1c2bfe68.jpeg)
![images/e67c4b281c9766fb4f3a0f52edb1e39f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e67c4b281c9766fb4f3a0f52edb1e39f.jpeg)

**"Maker Fixes"**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 00:44*

Well the 2nd one looks good. And if your results are like the first one it could need some combination of lower power or an air assist. 


---
**Maker Fixes** *May 13, 2017 00:51*

I have air assit somehow the laser shifted and doubled up the line did it  more then one see the line shooting out. My guess belt is slipping


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 00:52*

Did you increase the stepper driver current as described in the troubleshooting guide? 


---
**Griffin Paquette** *May 13, 2017 03:53*

Agreed with Ray there. Skipped steps is almost always a current issue once you have your jerk and acceleration dialed in (which the config should take care of). I would crank up the current a bit. 


---
**Maker Fixes** *May 13, 2017 13:17*

On issue I am having with the laserweb4 is all the configurations settings on the help site are for Laswerweb3 there are some that do not appear to be in laserweb4 or at least I cant find them 


---
**Maker Fixes** *May 13, 2017 23:06*

ok I went in to set up the pot like the sire shows, turn the machine on and I noticed I am only getting 1 red light and nothing more. both those images where  done together with the exact same settings. Do I have a failure in progress maybe? My LCD screen is not it lighting up either. ( I am not sure if it worked when I did the above as I did not look since I have it stuck in cardboard box to prevent it from shorting and was not reading it for these test


---
**Ray Kholodovsky (Cohesion3D)** *May 13, 2017 23:10*

Power up and watch the board. Tell me if any of the other LEDs do anything. 


---
**Maker Fixes** *May 14, 2017 00:17*

nope just the red one its on solid 




---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 00:23*

Try taking the memory card in and out (with power off) then power up again. 

Next up is plug in over USB (with laser power off). 


---
**Maker Fixes** *May 14, 2017 11:47*

Ok Ray the SD card seems to be set in socket ok. When I power up via the main power I get a red LED on solid, and my LCD screen lights up but no smoothie intro text shows up.



If I use  just the USB cable I get the alarm that sounds when you fire it up but that never ends. All LEDS are off and the LCD screen is dark.  


---
**Maker Fixes** *May 14, 2017 15:04*

FYI I hooked up the cheapo chinese board to test the power supply and servos it all that tested out fine


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 15:12*

Ok, sounds like you've taken the C3D board out? 

Please take a picture of it in the current state and attach here. 

Then please remove the GLCD adapter and the stepper drivers from it, leaving just the bare board with the memory card, and plug it in to USB. 


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 15:13*

 Again I'm looking to hear if any of the green leds do anything, even if there is a momentary flash when you plug it in. 


---
**Maker Fixes** *May 14, 2017 17:19*

None they do not even flicker


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 17:23*

Can you describe what changes were made between the last time it worked and the time you turned it on and it didn't work? You mentioned setting up the pot, this is in regard to the drivers, right? Can you describe exactly what you did, and send a picture of the board and drivers please? 


---
**Maker Fixes** *May 14, 2017 20:06*

I never got to set up pot. when I got to where you told me to test that it had stopped lighting up

Here is a history of events since I installed card. 

The X and Y axis shifted some right from start so I checked belts ect then It did burn a few images I was never able to get it to cut but I think that's an alignment issue nothing to do with the card. A couple time when I would fire up the machine the software would not connect. I got a better USB cable same result, if I rebooted the Cohesion3D board (both USB and power it would work) so I figured it was just being finicky being on for 30 min before I would run the laser. The the skipping got worse  like I showed you. When you told me to adjust POT I never got to the system would not fire up. I just went and bought a cheap volt/ ohm meter today if you would  like me to take a reading somewhere. 



Software change I make was to edit the DPI to 600 and put in G28.2 like the docs strat to do. I also put in .5 in tool test power. 



BTW Thank you so much for you dedicated support, I hate I am having issues and taking up your time, but sure am glad you are there to help a fellow out of a bind


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 21:01*

I have a suspicion of what is wrong but no indication of what would have caused it. See the thing called AMS1117? It has 3 legs and a head. I want you to meter ground (power in - as shown on pinout diagram) and the metal head of this thing for a voltage measurement. Do it from main power on and also from powering by USB. Let me know if/ what voltage you can see on there. 


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 21:04*

Here's a clear pic. AMS1117 is to be left of the male header that the GLCD adapter would plug into. ![images/5c842f8abf6b432eb1ef201e73d800e2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5c842f8abf6b432eb1ef201e73d800e2.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *May 14, 2017 21:33*

Oh, there's also a 5v and gnd header in the lower left, can you also confirm the voltages across that with each power variant? 


---
**Maker Fixes** *May 15, 2017 10:14*

Ok I put the ground on the ground for the power in ground and tested all 3 legs of the AMS1117. No voltage - nor was their voltage on the 5v in the lower left. This was done with the stepper motors disconnected but the drivers were plugged into their slots. The GLCD was not plugged it. 


---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 14:11*

How was the power applied for this test? 


---
**Maker Fixes** *May 15, 2017 14:36*

By turning machine on, then by usb alone. I am able to put old board in and run the machine


---
**Ray Kholodovsky (Cohesion3D)** *May 15, 2017 14:42*

I have a decent idea of what's gone wrong but no idea what would have caused it from your account of the issues. You can email me (contact form on the website) to discuss possible resolutions. 


---
**Maker Fixes** *May 17, 2017 00:36*

The Board is coming back home for some TLC




---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/TrQEtpWV1cM) &mdash; content and formatting may not be reliable*
