---
layout: post
title: "Got my C3D today, tried to read C3D Mini Laser Config MicroSD on computer so I could make a backup of files but doesnt recognize it, what do I do to view it"
date: March 04, 2017 18:04
category: "C3D Mini Support"
author: "Allen Russell"
---
Got my C3D today, tried to read C3D Mini Laser Config MicroSD on computer so I could make a backup of files but doesn’t recognize it, what do I do

to view it.







**"Allen Russell"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 18:11*

You plugged the card direct into the computer via a reader or you plugged the board into the computer?


---
**Allen Russell** *March 04, 2017 18:13*

plugged the card direct into the computer via a reader


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 18:15*

This is one of those weird ones... I personally program all the cards and then put them in an anti static bag and then they reach the customer corrupted.  What operating system, and please show me a screenshot of Computer.


---
**Allen Russell** *March 04, 2017 18:38*

Windows 7 -  32 and 64

![images/5b104a6ae86b1938fcc051bc048c36bb.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5b104a6ae86b1938fcc051bc048c36bb.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 18:48*

Screenshot of "My Computer" open please.


---
**Allen Russell** *March 04, 2017 18:53*

is this what you want

![images/b7a8c14c09b0a6a2921c3c1c6874e777.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b7a8c14c09b0a6a2921c3c1c6874e777.jpeg)


---
**Allen Russell** *March 04, 2017 18:54*

Or This

![images/803c51b6066d17decb3e8250d6ca930a.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/803c51b6066d17decb3e8250d6ca930a.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 18:56*

That last one.  So where's the card?  I need to see what the card shows up as. 


---
**Allen Russell** *March 04, 2017 23:12*

the card doesn't show up at all, when I insert the card I get a Ding but nothing there :-(


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 23:14*

Interesting.  

Well I have a dropbox with the files if you need them in the future.  Go ahead and put the card in the board and plug the board in over USB.  For Win 7 you will need drivers: 

[smoothieware.org - windows-drivers [Smoothieware]](http://smoothieware.org/windows-drivers)


---
**Allen Russell** *March 04, 2017 23:49*

Said it was installed, hooked up USB and this is what I got

![images/00dd1680391ade74ca04c0c403451fba.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/00dd1680391ade74ca04c0c403451fba.jpeg)


---
**Allen Russell** *March 04, 2017 23:50*

checked disk

![images/260234170fc00a721ca6b42648074678.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/260234170fc00a721ca6b42648074678.jpeg)


---
**Allen Russell** *March 04, 2017 23:51*

could I download and reinstall on card


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2017 23:53*

Ok, memory card is fine. 

As for why you are getting that error, try restarting the computer. You installed that link at the top right? The usb driver 1.1?


---
**Allen Russell** *March 05, 2017 00:05*

yes, I will reboot


---
**Allen Russell** *March 05, 2017 00:14*

so I reboot, same-thing happen, so with it plugged in I re-install 1.1 and this is what I get

![images/7f02dafc4bf477d3200e715c4da4212f.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/7f02dafc4bf477d3200e715c4da4212f.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 00:18*

I would think your installation was not successful the first time either. Are you the admin account on that computer? Any anti virus software? 


---
**Allen Russell** *March 05, 2017 00:28*

got it to install on different computer. What all is suppose to be on drive?


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 00:31*

You don't need to worry about the drive. It's all already there. 


---
**Allen Russell** *March 05, 2017 04:32*

OK, so I can go ahead and install my C3D?


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2017 04:42*

Yeah.  I was putting together a new upgrade guide.  I have the pictures I took, still writing the text to accompany them.  Hope this helps get you started.  Like I mentioned elsewhere we're not replacing the pot so leave that in play.  

And if you have a different wiring variant than what you see here that's find, just let me know. 

[dropbox.com - C3D Mini Laser v2.3 Photos](https://www.dropbox.com/sh/e9giso5z7145t37/AABfwdW8XF5-IvxV5DIKG9RBa?dl=0)


---
**Allen Russell** *March 05, 2017 05:17*

OK, Thanks


---
*Imported from [Google+](https://plus.google.com/117786858532335568822/posts/Zgtywy33ogU) &mdash; content and formatting may not be reliable*
