---
layout: post
title: "We're having issues with the RedSail laser at our makerspace"
date: October 30, 2018 11:11
category: "K40 and other Lasers"
author: "Johan Jakobsson"
---
We're having issues with the RedSail laser at our makerspace. Movement isn't what it should be and iI'm thinking it's related to the TopWisdom controllers stepgeneration not playing nice with the ClearpathSD servos.

Thought I'd try replacing the TopWisdom controller with a Cohesion3D card as  we already have one. (A 3D printer project that's on hold).



The current controller has physical buttons to move the X & Y axis around, set origin, move Z up and down.

If I replaced the current controller those functions would have to work on the new controller or the other makerspace members would be quite upset with me and demand that I switch back.



So does anyone have experience with adding physical buttons and how well it works? I figured the switch module in smoothieware has to be used as the jogging module isn't released yet - (and compiling a new firmware with jogging just for us isn't an option as it would mean no one but me can update the firmware in the future)





**"Johan Jakobsson"**

---
---
**Johan Jakobsson** *October 30, 2018 11:13*

To clarify. It has to be physical buttons. Adding a GLCD where you have to click into menus and use the rotary encoder to move axis around isn't an option.

One option would be dedicated rotary encoders for each axis  instead of buttons. As long as you didn't have to go into menus on a GLCD each time to get them working.

And as an extra bonus its preferable, almost a requirement, that the buttons are connected to the controller and not the host running Lightburn as we want to be able to jog the machine without having the computer online


---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2018 16:55*

Read the articles at Cohesion3D.com/start , the z table one shows how to hook

up external drivers. 



In the smoothie firmware you can define switches to do things (gCodes) in the config file. [cohesion3d.com - Getting Started](http://Cohesion3D.com/start)


---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2018 17:04*

You want this post in the fb group. Search for g91 

![images/cc3deec3029504537dc69e4624aa10b3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/cc3deec3029504537dc69e4624aa10b3.png)


---
*Imported from [Google+](https://plus.google.com/+johanjacobsson/posts/ArhP1i6TzcB) &mdash; content and formatting may not be reliable*
