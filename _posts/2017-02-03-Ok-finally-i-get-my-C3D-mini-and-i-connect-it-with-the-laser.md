---
layout: post
title: "Ok finally i get my C3D mini and i connect it with the laser !!!"
date: February 03, 2017 19:42
category: "General Discussion"
author: "Kostas Filosofou"
---
Ok finally i get my C3D mini and i connect it with the laser !!! .. i have movement on X and Y axis but i can't make the laser to fire from the Laser test button on Laserweb .. i use the default settings for the K40 laser in Laserweb and i choose to connect the L cable in Bed connection (the 4th connection) and i change the value to 2.5 (without the exclamation) Any ideas **+Ray Kholodovsky** ?



.. And something else when i hit the homing button in Laserweb the laser head moved to the exact opposite position from my endstops, how i change that? 





**"Kostas Filosofou"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 20:10*

Check your movement directions when you jog.

X + should go right, X- should go left. 

Y+ should go towards the back of the machine, Y- should go towards the front.  Is this correct or is it the opposite way? 



For firing - I'm not sure that the fire button in LW works.  Try running a command such as G1 X10 S0.5 F600

Or you can use the GLCD for firing through the menu commands. 


---
**John Sturgess** *February 03, 2017 20:11*

Have you set homing sequence to G28.2 in LaserWeb and to test fire use a command such as "G1 X10 S0.5 F600" i've never been able to get the test fire button to work either.



Edit: when you manually jog, does the head move in the correct directions, i'm assuming you're using a ribbon cable for X and the end stops? that'll rule out it simply being the cables needing to be rotated 180 degrees.


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 20:12*

Haha John you took the words right out of my mouth. Nice work. The G28.2 was the next thing to check, I wanted to make sure the Y motor cable wasn't flipped first. 


---
**Kostas Filosofou** *February 03, 2017 20:18*

**+Ray Kholodovsky** 

When i jog the axis moving in the right direction only when i hit the home button moving in exact opposite direction...


---
**John Sturgess** *February 03, 2017 20:22*

**+Ray Kholodovsky** i think we must've been typing at the same time, just took me a little longer to type it out!


---
**Ray Kholodovsky (Cohesion3D)** *February 03, 2017 20:23*

And are you using G28.2 as the home command? 


---
**Kostas Filosofou** *February 03, 2017 21:17*

ok!! all worked fine! Thanks a lot **+Ray Kholodovsky** and **+John Sturgess** for the quick answers!! 


---
**Kostas Filosofou** *February 04, 2017 19:51*

Today came my GLCD and i connected the board with the adapter that came with C3D mini i flip the EXP2 cable on the display to work properly ..The Display work fine but the jog works only when i press it.. goes to the center menu but when i try to navigate left or right do nothing **+Ray Kholodovsky** this is software problem or from the display ?


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 19:53*

Send picture of back of GLCD please. 


---
**Kostas Filosofou** *February 04, 2017 20:07*

**+Ray Kholodovsky**

![images/dbe394eb3a29029834fdfec46f06fc07.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/dbe394eb3a29029834fdfec46f06fc07.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 20:08*

Can you unplug the cables and take the pic so I can see the orientation of the black box connectors better?


---
**Kostas Filosofou** *February 04, 2017 20:08*

**+Ray Kholodovsky** better?

![images/69e00159edf54def5f1f8c1f4bd0ee58.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/69e00159edf54def5f1f8c1f4bd0ee58.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 20:09*

There we go!  Why did you flip EXP2?




---
**Kostas Filosofou** *February 04, 2017 20:13*

**+Ray Kholodovsky** works only in this position..


---
**John Sturgess** *February 04, 2017 20:14*

**+Konstantinos Filosofou** I had to flip both of mine to get it to work properly. 


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 20:15*

What "works" or does not work?  Unplug EXP2 entirely (with board powered off). Just EXP1 connected you should have full visibility of text on the screen and be able to click the encoder button. 

The left and right scrolling are part of EXP2. 


---
**Kostas Filosofou** *February 04, 2017 20:15*

**+Ray Kholodovsky** shit ...my mistake i haven't reboot the board in the first plase


---
**Kostas Filosofou** *February 04, 2017 20:16*

i have flip it again in the right position ... reboot .. problem solved :)


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 20:16*

You should always have the board off when fiddling with wiring.  So what exactly happened/ is happening? 


---
**Kostas Filosofou** *February 04, 2017 20:25*

**+Ray Kholodovsky** at first i connected as supposed to be and i have only the usb cable connected no power .. and don't work then i flip the EXP2 works as i describe above and finally i disconnect the usb connected again in right order and works fine!


---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2017 20:27*

Well USB powers the board too (microcontroller logic)

When I say no power I mean machine 24v off and usb unplugged.  :)


---
**Kostas Filosofou** *February 04, 2017 20:27*

**+Ray Kholodovsky** all 'problems' solved with the board now i will be transferred to the LeserWeb community :) :) Thanks again Ray for all the support !! You are great!


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/Wu6sZtpQiD3) &mdash; content and formatting may not be reliable*
