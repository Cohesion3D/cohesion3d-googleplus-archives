---
layout: post
title: "Hi! I make a machine on the kinematics of CoreXY (Cohesion3D ReMix)"
date: February 19, 2018 09:20
category: "FirmWare and Config."
author: "\u041e\u043b\u0435\u0433 \u041f\u0435\u0440\u0435\u0432\u044b\u0448\u0438\u043d"
---
Hi!

I make a machine on the kinematics of CoreXY (Cohesion3D ReMix). Please share the config file for the controller for this use.





**"\u041e\u043b\u0435\u0433 \u041f\u0435\u0440\u0435\u0432\u044b\u0448\u0438\u043d"**

---
---
**René Jurack** *February 19, 2018 11:10*

I bet, I am not allowed to share mine 🤣 <b>thinking-at-Greg</b> **+Ray Kholodovsky** would probably suggest to get a NEW and blank sample config from Smoothieware directly instead of sharing old ones.


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2018 13:37*

I agree. Take the stock config file and read the smoothieware website for how to configure it. 


---
*Imported from [Google+](https://plus.google.com/109105331181661136210/posts/8KjQ5TCuZsN) &mdash; content and formatting may not be reliable*
