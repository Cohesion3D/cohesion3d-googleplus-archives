---
layout: post
title: "Just got this little guy installed in my Full Spectrum 40w Gen 4"
date: May 04, 2017 05:36
category: "General Discussion"
author: "Ben Delarre"
---
Just got this little guy installed in my Full Spectrum 40w Gen 4. Had to drill two holes in the mounting plate, and I just cut down the 6pin power connector to a 4pin and repinned the wires. Similarly I desocketed the endstop connector and heatshrink wrapped the pins and just popped them onto the cohesion board. Job done. Total conversion took about 30mins!



Fired up laserweb and first moves and lasering accomplished in no time at all.



 Top marks all around guys.

![images/5c1f9b3a59d7f67ad6cfbc35f9fc2c84.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/5c1f9b3a59d7f67ad6cfbc35f9fc2c84.jpeg)



**"Ben Delarre"**

---
---
**Joe Alexander** *May 04, 2017 06:07*

looks great, clean install


---
**Ben Delarre** *May 04, 2017 06:17*

Maybe.... ;-) We'll see if I get any time. I've got a big burning man project coming up that I need to dedicate the next few months to. And I have an LED platform project I really need to get off the ground.



However. I will check in and try and keep up to speed. From what I can see thus far though you guys have done a great job!



I've got a couple of things I need to figure out before it's fully useable. I want to switch the origin to top left instead of bottom right. Right now it's inverted from the machine, and it homes in the wrong y direction. And I need to sort out calibrating power settings for raster engraving (wasn't there a test image floating around for that?).



Also any idea how I hook up an esp8266 to the cohesion board?


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 14:19*

Where are your endstops located? On k40 it is at x min and y max (left and rear of machine) 



For ESP I have a backpack pcb or you can get a nodemcu and run rx tx and gnd to the serial header on C3D board. 


---
**Ben Delarre** *May 04, 2017 14:51*

Endstops are located at rear left. Positive Y is currently moving towards the front of the machine as I would expect. But G28.2 Y0 also moves towards the front. In laserweb it's showing the origin to be the bottom left, would expect that to be top left to match the machine.



I have a whole bunch of esp8266 boards so will just hook up the serial. Thanks.


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 14:55*

Flip your y motor cable. Homing is to rear, this sets position (0, 200). 


---
**Ben Delarre** *May 04, 2017 14:56*

Oh so it homes towards 0,200 rather than 0,0? Guess I will need to change that since I have 268 y mm.


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2017 15:00*

Interesting. Will try it as is first. G28.2 to home, then G0 X0 Y0 and see where it goes. That's your lower left LW origin. Adjust accordingly :)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/GZwgekGhsAM) &mdash; content and formatting may not be reliable*
