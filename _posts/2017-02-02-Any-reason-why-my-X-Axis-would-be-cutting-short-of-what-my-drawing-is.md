---
layout: post
title: "Any reason why my X Axis would be cutting short of what my drawing is?"
date: February 02, 2017 02:29
category: "General Discussion"
author: "Kris Sturgess"
---
Any reason why my X Axis would be cutting short of what my drawing is? It's off by -2mm. Its a simple rectangle H65mm X W100mm. It keeps cutting the width at 98mm. Height is bang on at 65mm.



Checked the config the stepper settings are the same as my other "smoothie" board. I haven't changed anything in the default Cohesion config.



Using LaserWeb3. Haven't figured out the workflow yet on LW4.



Thoughts?





**"Kris Sturgess"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 02:32*

Hi Kris,

I'm not understanding the issue 100%.  Can you attach pictures for explanation, etc? 


---
**Kris Sturgess** *February 02, 2017 04:42*

I'm trying to cut a rectangle 65mmx100mm.

It is cutting 65mmx98mm






---
**Ray Kholodovsky (Cohesion3D)** *February 02, 2017 12:41*

Curious that it is in one dimension and not the other. 

Usually if someone says their size is off I ask them if they are offsetting for laser width (I use CNC functions to make internal and external cuts with a .004" tool diameter). But 2mm is a lot. What if you try to move the axis some known amount and measure how much it actually moved? Got any calipers? 


---
**Kris Sturgess** *February 02, 2017 14:33*

I'll do some more testing tonight.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/5G15UYZ15d4) &mdash; content and formatting may not be reliable*
