---
layout: post
title: "Long story short... after a dumb, dumb, dumb mistake, I ruined my original board"
date: June 29, 2018 00:30
category: "General Discussion"
author: "K. J. VanWormer"
---
Long story short... after a dumb, dumb, dumb mistake, I ruined my original board. What better time than now to upgrade? I am going to need to put wiring connectors on everything going from the K40 to Cohesion. I ordered the board, what connectors should I buy?





**"K. J. VanWormer"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2018 00:48*

C3D sells a connector pack so you don’t have to worry about this. 


---
**K. J. VanWormer** *June 29, 2018 00:56*

Whew! Thanks a lot. 


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2018 01:11*

Of course, that’s not in your order. Also, you ordered the wrong thing. 


---
**K. J. VanWormer** *June 29, 2018 14:15*

If the picture of the connector kit featured on the Cohesion3D site is what's provided in the kit, it's very much not what I need as there isn't the larger type of connector used in for wiring for the power supply, no?



And more importantly, what <b>should</b> I be buying, then? 


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2018 14:32*

You bought the bare mini board and you need the laser upgrade bundle. 



You don't need to redo the large power pigtail because you can put those wires (24v, Gnd, L) to screw terminals. 

You are going to need the 3 and 4 pin connectors in the pack for the motors and endstops, if you do indeed need to crimp those.  


---
**K. J. VanWormer** *June 29, 2018 14:38*

Thanks for the answer. Can you cancel this or is there someone else I can contact? 


---
**Ray Kholodovsky (Cohesion3D)** *June 29, 2018 14:44*

I cancelled and refunded it. 


---
**K. J. VanWormer** *June 29, 2018 14:49*

Fantastic. Thanks for all of your help. 


---
*Imported from [Google+](https://plus.google.com/101105211923721675912/posts/3NnZvkETD2Q) &mdash; content and formatting may not be reliable*
