---
layout: post
title: "How does one go about cloning/mirroring an axis on a remix, I need two nema 23's to function the same way on my y axis"
date: March 12, 2018 20:25
category: "CNC"
author: "Joshua Jones"
---
How does one go about cloning/mirroring an axis on a remix, I need two nema 23's to function the same way on my y axis. I am running external tb6600 stepper drivers, and have them all wired as shown here.[https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions](https://cohesion3d.freshdesk.com/support/solutions/articles/5000744633-wiring-a-z-table-and-rotary-step-by-step-instructions)



 Do I need to run a physical jumper on the board itself between dr2 and dr4? Any help would really be appreciated. 





**"Joshua Jones"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2018 20:39*

If you have 1 external driver for the Y, connect both motors to it, if you have 2 drivers for the Y run 2 sets of wires to the same step dir en on the external stepper adapter in the Y socket. 


---
**Joshua Jones** *March 12, 2018 21:03*

I think I get it. so don't run it through the board like they have done here. I have a external driver for each and a stepper adapter for each, so your saying use just one stepper adapter from the y axis point on the board to both drivers?



![images/4b1e5d02f958531554248ae97e5bc655.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/4b1e5d02f958531554248ae97e5bc655.png)


---
**Ray Kholodovsky (Cohesion3D)** *March 12, 2018 21:08*

Yeah, that's smoothieboard and on board built in drivers, this is different. 

Our guide is much closer to what you need to do.  Yes. 


---
**Joshua Jones** *March 12, 2018 21:09*

I will take some pictures tonight just to make sure.


---
**Joshua Jones** *March 13, 2018 00:51*

? Like this?

![images/023310d5cffa5786b67a4f68a20169e1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/023310d5cffa5786b67a4f68a20169e1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 13, 2018 02:18*

That general idea, yes.  5v to the rest and change the pin defs in the config file as per the guide. 


---
*Imported from [Google+](https://plus.google.com/105400335561752829962/posts/YKSe9XAy9CR) &mdash; content and formatting may not be reliable*
