---
layout: post
title: "hey guyas having a slight problem how do i get my k40 to work i have the 3dmini upgrade in it and cant seem to get it seen by the computer i have everything connected properly but noting is working please help me"
date: August 18, 2018 22:36
category: "C3D Mini Support"
author: "James Muirhead"
---
hey guyas having a slight problem how do i get my k40 to work i have the 3dmini upgrade in it and cant seem to get it seen by the computer i have everything connected properly but noting is working please help me 





**"James Muirhead"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 20, 2018 14:58*

Put both files from Batch 3 onto the MicroSD Card, put that card in the board, then turn power on. 


---
**James Muirhead** *August 20, 2018 22:33*

Ive done that and it keeps saying format the card before use and does it beed the smoothie ware file on it as well 


---
**Tech Bravo (Tech BravoTN)** *August 20, 2018 23:35*

[lasergods.com - C3D 4 Axis Firmware & Config](https://www.lasergods.com/c3d-4-axis-firmware-config/)




---
**James Muirhead** *August 24, 2018 03:49*

Why would my k40 not have an X axis cable and connctor


---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 04:00*

If you have a ribbon cable it carries the endstops and the x axis motor connections.


---
**James Muirhead** *August 24, 2018 05:51*

Am i doing something wrong because its not working 




---
**Tech Bravo (Tech BravoTN)** *August 24, 2018 05:56*

so give me a little bit of back story. i am assuming you installed a cohesion3d mini in your k40. snap some pics of the board and its connections as they are now and post them please. aslo describe what you have done up until this point (lightburn setup, etc...) and what exactly you are having issues with. i gather that when you power up the laser it tries to go "home" but get movement only from y axis?


---
**James Muirhead** *August 24, 2018 06:43*

Yes thats correct and i followed your website 


---
*Imported from [Google+](https://plus.google.com/108641697491643088534/posts/5Em2PVPJdCg) &mdash; content and formatting may not be reliable*
