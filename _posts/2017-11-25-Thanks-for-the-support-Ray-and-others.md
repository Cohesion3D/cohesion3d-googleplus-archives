---
layout: post
title: "Thanks for the support Ray and others"
date: November 25, 2017 16:58
category: "Show and Tell"
author: "Todd Mitchell"
---
Thanks for the support Ray and others.



Cohesion3d remix up and running (almost - just need to find a decent post processor for v-carve).



4-axis running a sherline model 2000 mill

e-stop working as kill switch (with reset support) by changing the pin in config to 2.4

Fans running on mosfets +12v.  I used a converted wall adapter from the thiftstore

DM542T stepper drivers from stepperonline paired with the polu-adapters made by **+Ray Kholodovsky**

Seriously shielded stepper cable to keep out the noise









![images/e799292b3afffedce7d0031ffc19fa34.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e799292b3afffedce7d0031ffc19fa34.jpeg)
![images/56da3cff7b6c7d25f7beec387f718b40.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/56da3cff7b6c7d25f7beec387f718b40.jpeg)

**"Todd Mitchell"**

---
---
**Joe Alexander** *November 25, 2017 17:38*

nice setup! did you remember to put a diode on the fans so they wont burn out the mosfet? (was reading about this just the other day)


---
**Todd Mitchell** *November 25, 2017 18:26*

**+Joe Alexander** thanks!



ah! I didn't know you needed to do that.  I'll add them then.




---
**Ray Kholodovsky (Cohesion3D)** *November 25, 2017 21:52*

ReMix has diodes on the fets, albeit small ones :) 



Still, something to to keep in the back of head if any problems happen. 


---
**Todd Mitchell** *November 25, 2017 22:13*

thanks!


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 01:23*

nice work!


---
*Imported from [Google+](https://plus.google.com/100184887426384936456/posts/UTHYgMnbDtf) &mdash; content and formatting may not be reliable*
