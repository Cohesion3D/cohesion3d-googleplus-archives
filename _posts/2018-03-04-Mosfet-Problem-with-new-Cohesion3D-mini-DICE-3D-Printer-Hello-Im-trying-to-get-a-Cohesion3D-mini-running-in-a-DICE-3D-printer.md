---
layout: post
title: "Mosfet Problem with new Cohesion3D mini @ DICE 3D Printer Hello, I'm trying to get a Cohesion3D mini running in a DICE 3D printer"
date: March 04, 2018 12:46
category: "C3D Mini Support"
author: "Gregor"
---
Mosfet Problem with new Cohesion3D mini @ DICE 3D Printer



Hello,



I'm trying to get a Cohesion3D mini running in a DICE 3D printer.

Movement, hotend, heatbed, display work as they should.

But the control of the fans doesn't work.



The Mosfet4 (P2.4) is connected to the cooling pressure part. The cooling of the pressure part can also be controlled by PWM, but after a single activation it is not possible to completely deactivate it.

The fan continues to run permanently.

The Hotend fan is connected to Mosfet3 (P2.6). The Hotend fan also switches as set at 50°C, but with so little power that the fan does not turn on.

Here also the same problem: after cooling down below 50°C and less, the output does not switch off.

Voltages directly at the output or at the step-down converter were measured.



The behaviour has already been tested without plugged-in stepper motors etc.



A Main Power In @24V / Mosfet Power In @24V is connected.



Used smoothie config is attached.



Is it possible that the Mosfets are defective?



I can't go on with the problem and would be happy if you had any ideas on how to solve the problem.



many greetings

Gregor









**"Gregor"**

---
---
**Griffin Paquette** *March 04, 2018 17:03*

Try with a stock config file and see if it persists. Sometimes, burned mosfets/IC pins can cause a permanent open gate


---
**Ray Kholodovsky (Cohesion3D)** *March 04, 2018 17:43*

Rene's handwritten config file has caused problems before.  

Known good config file and firmware: [https://www.dropbox.com/sh/t2f37b4l182lig0/AACNhyTLnqMcfXiSuGuxiKOXa?dl=0](https://www.dropbox.com/sh/t2f37b4l182lig0/AACNhyTLnqMcfXiSuGuxiKOXa?dl=0)


---
**Gregor** *March 05, 2018 18:47*

**+Ray Kholodovsky** **+Griffin Paquette** 

Thanks for the help, both of you.

But unfortunately my problem is still there despite Stock Config / Firmware from Ray.

As soon as I put power on it, I have 24V voltage on both Mosfets.



Any ideas besides the Mosfets might be broken?



It's just annoying, because the cohesion should replace a broken AZSMZ-mini.


---
**Ray Kholodovsky (Cohesion3D)** *March 05, 2018 19:21*

Please show pictures of the wiring 


---
**Gregor** *March 05, 2018 20:13*

**+Ray Kholodovsky** 

![images/e66d2fd5e34e66cf1ed1cf707e188db2.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/e66d2fd5e34e66cf1ed1cf707e188db2.jpeg)


---
**Gregor** *March 05, 2018 20:13*

**+Gregor** 

![images/edaf41559565e6bd232d608a1f3870a6.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/edaf41559565e6bd232d608a1f3870a6.jpeg)


---
**Gregor** *March 05, 2018 20:13*

![images/214d1e16e3d7af66c9c096c41c0ca7ab.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/214d1e16e3d7af66c9c096c41c0ca7ab.jpeg)


---
**Gregor** *March 05, 2018 20:14*

![images/b1b8ea5d54a2b07cf1003db51d8c1619.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/b1b8ea5d54a2b07cf1003db51d8c1619.jpeg)


---
**Gregor** *March 05, 2018 20:18*

Sorry for the cable chaos, but there is really little space in the DICE with Rpi3 and UPS.

If you need exact photos of specific points or connections, I will have to remove the board.

![images/68935f503e51e7b56741ad81e1e05d43.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/68935f503e51e7b56741ad81e1e05d43.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 07, 2018 00:19*

Gregor,

I am not sure what is going on.  If you can use my stock config and firmware file provided, and configure the switch module for pin 2.4 and pin 2.6, send the gcode in there to turn the fan on (M106 and M107) and it is stuck, well this is a concern. 



One quick solution could be to use, if you have it, a small mosfet board, and you could hook the control signal for this up to any spare gpio pins on the board including in the GLCD and Ethernet expansion headers.  If the pinout diagram is insufficient I can direct you with the pin #'s to use. 



We can replace the board but obviously shipping will take some time.



Please let me know. 






---
**Gregor** *March 08, 2018 18:37*

**+Ray Kholodovsky** 

Hello, Ray,

got my "little" Mosfet Board today.

5V and Ground to the two inputs and signal to P3.26.

And it can be easily controlled by PWM.



How long would a replacement delivery take?

Could then lay the cables neatly until then;)

![images/1b9ba4540bc960cbeb971845502f8ee1.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/1b9ba4540bc960cbeb971845502f8ee1.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *March 08, 2018 21:16*

Please email us with your order #/ information, reference this thread,  and we will discuss the details. 


---
*Imported from [Google+](https://plus.google.com/103737771356760638876/posts/2kJWTvbCZYg) &mdash; content and formatting may not be reliable*
