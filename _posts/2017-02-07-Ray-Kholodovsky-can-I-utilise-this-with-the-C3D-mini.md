---
layout: post
title: "Ray Kholodovsky can I utilise this with the C3D mini?"
date: February 07, 2017 09:50
category: "General Discussion"
author: "Andy Shilling"
---
**+Ray Kholodovsky**​ can I utilise this with the C3D mini? I've got 4 coming and thought I might be able to create an adjustable bed.

![images/3f2bddba8a545a9263750b00d628a0f3.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/3f2bddba8a545a9263750b00d628a0f3.png)



**"Andy Shilling"**

---
---
**Ariel Yahni (UniKpty)** *February 07, 2017 11:44*

interesting idea


---
**Dushyant Ahuja** *February 07, 2017 11:51*

These are bipolar motors and don't work with the motor drivers with the cohesion 3D boards as is. You will have to convert them to unipolar and then use them. Also, since they work on 5V, you will have to manage the voltage difference somehow. Basically - not easy, but possible. 


---
**Andy Shilling** *February 07, 2017 11:53*

**+Ariel Yahni**​ I'm hoping I can run them in series and have the full depth table but while I'm happy being hands on and quite good at work arounds in the first to admit I lack the in depth  knowledge of electrickery.


---
**Griffin Paquette** *February 07, 2017 11:56*

**+Dushyant Ahuja** it's actually the other way around. These motors are unipolar from the start. You would need to crack them open and delete the trace that makes them uni. There are a number of guides for it. Not sure how the switching regulator would like you supplying 5V, but since it generates 5V there anyways it should be fine. I would let **+Ray Kholodovsky** chime in on that first though.


---
**Dushyant Ahuja** *February 07, 2017 11:57*

**+Griffin Paquette** you're right. I confused uni and bi :-)


---
**Andy Shilling** *February 07, 2017 12:23*

Thanks to you all for the help so far, I'll wait for Ray's input before making any type of decision on these motor then. If I get them working I'm hoping for a full depth table with my cutting size of 320x211.


---
**Jim Fong** *February 07, 2017 14:08*

[jangeox.be - Change unipolar 28BYJ-48 to bipolar stepper motor](http://www.jangeox.be/2013/10/change-unipolar-28byj-48-to-bipolar.html?m=1)






---
**Andy Shilling** *February 07, 2017 15:16*

So from what I've read so far if I've got this right. I make these bipolar and get another A4944 stepper driver I should be able to run these in series as they are only 90ma each and trip the pot  on the driver down to run at the 5v needed?


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 16:01*

Before we delve into all this, I should point out exactly how weak these motors are. They are very weak. I would not want to use them for a Z bed. I would recommend **+Ryan Branch**'s design which used a Nema 17 normal stepper. 


---
**Andy Shilling** *February 07, 2017 16:52*

That's a really nice Bed but as I have these parts coming anyway and do not own a 3d printer (yet) I will be looking at around £100+ to buy all those parts. whereas if I can use these I should be able to come up with something similar (hopefully).



I realise these motors are quite week but surely running one in each corner will compensate.


---
**Don Kleinschnitz Jr.** *February 07, 2017 16:57*

**+Ray Kholodovsky** Im with Ray I doubt even 4 of these will be enough torque :) ... and you have to sync them. You can find pretty cheap steppers and you only need one :).


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 17:01*

Well, you're welcome to give it a shot. Do not use the 5v regulator on the mini for this. Or the 5v from the laser... Bad... Get a 5v psu or cell phone charger type thing. 

If you want to run using an A4988 it would have to be external (not mounted on the Mini) so that you can feed 5v or whatever voltage to it. 


---
**Andy Shilling** *February 07, 2017 17:41*

Ok Thanks **+Ray Kholodovsky**.



**+Don Kleinschnitz** its not the cost of the Stepper that's the problem there is a pretty extensive list of 3d printed parts and I'm afraid in the uk nothing comes cheap, For the bit's needing printing I'm looking at around £70 without the stepper etc. 

The UK sucks when it comes to pricing of things. 



 Please don't think I am just ignoring your input  Ray and Don but I am just trying to work with what I have at the moment.


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 17:46*

We don't take stuff personally. I provided you my opinion, Don did the same, you are free to do what you want. 

If I were to be a bit cheeky I would say that I am expecting to hear a "you told me so" in the near future, but happy to be proven wrong :)


---
**Don Kleinschnitz Jr.** *February 07, 2017 17:49*

+Andy Shilling differences of opinion and experiences are the meat of a community like this and for me there is no foul in rejecting suggestions, only in not giving them.


---
**Andy Shilling** *February 07, 2017 17:54*

You told me so lol, No seriously though once I get everything up and running how I want them and producing bits I will be happy to donate to the cause. You guys really do put a lot of effort in and I for one  really appreciate it. Thank you


---
**Jim Fong** *February 07, 2017 18:35*

Since you really want to try this stepper.....



The 5 volt rating is the "safe" voltage to apply if you do not use a current limited driver.   All stepper motors have a similar voltage specification.  Although most don't print it on the motor itself.  I have stepper motors that have as low as 1.8volt and high as 24volts rating. 



Since the a4988 is a current limited driver, you can use a higher voltage IF you can set the current low enough to satisfy the motor.  To much current will overheat and destroy the motor.  



I believe the minimum voltage required for the a4988 to operate is 8volts. 



You will need a meter to read the pot adjustment voltage to calculate the current for the motor.  There are various howto on the a4988 driver you can look up.   I never tried setting a a4988 to 90ma and don't know if that is even possible.  Good luck. 



Edit

The a4988 is a microstepping driver.  I'm pretty sure it will not be able to resolve the very low microstep current levels accurately to make the motor move smoothly.  You will have a better chance if you set the driver work in full or half step mode. 


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 18:36*

And to reiterate, he needs to do this outside of the mini socket because that already has 24 volts going through its veins.


---
**Andy Shilling** *February 07, 2017 18:41*

Following your advice on outside of the mini is it possible to still control the motors using the mini?  that is the be all and end all for me really.


---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 18:49*

Hey **+Jim Fong** I can't seem to tag you in other places.  Can you jump on this thread: [https://plus.google.com/u/0/115163232171765331523/posts/NHyCv6RDmYS](https://plus.google.com/u/0/115163232171765331523/posts/NHyCv6RDmYS)

Marc is looking at doing a line driver (I think it is differential signaling) for his external drivers.  I said you're the guy to weigh in on this.


---
**Dushyant Ahuja** *February 07, 2017 18:55*

**+Andy Shilling** you can - you will have to take the control pins and pull them to the drivers like any external driver. I tried to run these motors using A4988 for another project. It worked - to an extent. I had to reduce the voltage to my board to 6.1 V (the minimum my driver's would work at). Any more and the motors would simply buzz and heat up. In your case, as your board is running off 24V, this would not be an option and you would need a separate power for these motors. 


---
**Andy Shilling** *February 07, 2017 19:02*

**+Dushyant Ahuja** Thank you. 


---
**Andy Shilling** *February 11, 2017 17:04*

**+Ray Kholodovsky** is it possible to plug the A4988 driver into the C3D mini  but remove the Vmot pin  so it doesnt connect to the 24v and then  solder a new connection from a different lower voltage supply to save running loads of wires to keep control from the C3D mini?


---
**Ray Kholodovsky (Cohesion3D)** *February 11, 2017 18:08*

**+Andy Shilling** that would be the idea. You could try cutting the header pin and soldering from above. Meter everything for lack of continuity, and I can't be responsible if something goes wrong. 


---
**Andy Shilling** *February 11, 2017 18:24*

**+Ray Kholodovsky** No certainly not anything I do is off my own back, I'm wondering if I could change these 5v motors for the 12v counterparts. Would that make any difference regarding the 24v?

 I only ask because I've been trying to read up on stepper motors and drivers and a lot of things I have read seem to say it's not the voltage that is the issue more so the current, is this correct?


---
**Don Kleinschnitz Jr.** *February 11, 2017 18:44*

**+Andy Shilling** I=E/R ...:)


---
**Dushyant Ahuja** *February 11, 2017 19:03*

**+Andy Shilling** you might be able to work with 12V motors. Running 5V motors off 24V needs so little current that the A4988 can't manage. 


---
**Don Kleinschnitz Jr.** *February 11, 2017 22:39*

**+Dushyant Ahuja** I would think that running 5v motors on 24V would be a good way of testing smoke detectors? :)


---
**Andy Shilling** *February 11, 2017 22:46*

**+Don Kleinschnitz**​ that's not what's being said I'm looking at swapping the 5v for 12v motors **+Dushyant Ahuja**​ is basically saying I wouldn't be able to run the 5v like I've already been advised.


---
**crispin soFat!** *June 18, 2017 07:55*

"The gearing has a few side effects which are important to note. First, you can turn the stepper by hand but not as smoothly as an un-geared stepper. It also means you shouldn't use interleaved or micro-stepping to control or it will take forever to turn. Instead use single or double stepping. The torque is fairly high but its slower than un-geared steppers - it maxed out at about 80 RPM by over-driving it a bit with 9VDC."

Holding Torque: 150 gram-force*cm, 15 N*mm/ 2 oz-force*in"


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/AZujtjLEVbU) &mdash; content and formatting may not be reliable*
