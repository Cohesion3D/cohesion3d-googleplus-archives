---
layout: post
title: "Ray Kholodovsky I've just fitted a new tube and finally got the C3D mini running but I seem to have a high pitch squeal coming from the tube"
date: February 18, 2017 19:21
category: "C3D Mini Support"
author: "Andy Shilling"
---
**+Ray Kholodovsky** I've just fitted a new tube and finally got the C3D mini running but I seem to have a high pitch squeal coming from the tube. I've checked the insulation around the tube pegs but that doesn't seem to be a problem, is there anything else that could be causing it?



I've removed the pot at the moment but it seems changing the power on the GLCD doesn't make any difference when I test the laser  does that mean to GLCD isn't controlling the laser at all.





**"Andy Shilling"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 19:27*

So the test fire method would be to select a test fire power level (say half power or full power) on the LCD, then press the physical test fire button on the board. 

Is there a mA gauge? What does it read when you do this/ when there is a squeal?


---
**Andy Shilling** *February 18, 2017 19:42*

Ok I didn't think to set the power before test fire, I'll do that now but the mA shows around 12mA.  I've just checked it with full and half power and the is no difference. The squeal remains lol


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 19:44*

Ok, is the tube firing? 


---
**Andy Shilling** *February 18, 2017 19:56*

Yes from the test switch. But what ever power setting I chose it outputs around the 12mA mark


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 19:59*

Has it worked differently before? Have you used the machine with c3d before changing the tube? 


---
**Andy Shilling** *February 18, 2017 20:01*

No I broke the tube before fitting the C3D and the new one has just turned up today.


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 20:01*

The pwm period can be 20 - 200 - 400, these values are worth experimenting with. 


---
**Andy Shilling** *February 18, 2017 20:53*

Sorry where is that setting?


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 20:56*

In the smothie config.txt on the board's memory card. It's pwm period in the laser section.


---
**Andy Shilling** *February 18, 2017 21:01*

Ok it's getting late here now so I'll check that in the morning. Should I return the pot back in to the system?


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2017 21:03*

Try the pwm period tinkering first.  Failing that, put the pot back and pulse the L wire from the -2.5 terminal like I told everyone else to do.  Use the test button to set the pot to around 10mA.   You may still need to fiddle with the pwm period with the new wiring way.


---
**Andy Shilling** *February 18, 2017 21:30*

Cheers **+Ray Kholodovsky** I will have a look in the morning.


---
**Andy Shilling** *February 19, 2017 10:43*

**+Ray Kholodovsky** right I might has caused this issue then I removed the pot and have still moved the L to p2.5! should I have left the pwm on p2.4 if removing the pot? I will replace the pot and test then report back.


---
**Andy Shilling** *February 19, 2017 13:46*

Problem of the power is now solved. (also create by me lol, I wired my pot wrong) I still have the squeal though, even on low power I am going to redo the insulation around the posts to see if that works out.




---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 14:35*

The values in config should be 2.4! if replacing the pot or 2.5 if keeping it. 


---
**Andy Shilling** *February 19, 2017 17:05*

**+Ray Kholodovsky**



Ok I have renewed the insulation and the noise persists, I am now thinking it is the tube so I have requested a replacement. 

Next problem is I uploaded a simple image to LW3 and run it but the laser seems to be on permanent. It was only a small image of a golfer 100mmx100m but the laser was on all the way from top left (home) to bottom left 0,0 on LW3 when doing the image to power was adjusted  but stayed on the whole time.


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 17:07*

What wiring configuration are you in now, and what is the laser pin in config set as?


---
**Andy Shilling** *February 19, 2017 17:14*

Pot back in play, pwm period still at 20 and L to P2.5 




---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 17:19*

You saved and reset the board after making the change? 

What pwm periods have you tried?

Did any of them work? 

What is the pot set to?

If you run G1 X10 S0.4 F600....

And then try with other S values like 0.2 - 1.0

(And you'll have to change the X value between 10 and 0 to move back and forth, also jog Y down a little bit. In short, run a bunch of G1 lines with different S values). 


---
**Andy Shilling** *February 19, 2017 17:27*

I haven't tried any pwm periods because I wasn't sure i needed to adjust it after the original problem was solved.



 I have a digital volt meter connected to the pot as per Don's wiring so that was displaying 1.4 and the mA was showing around 10mA. I will do the G codes in a second to see what it feeds back.


---
**Andy Shilling** *February 19, 2017 18:40*

**+Ray Kholodovsky**

After running a few codes S0.4- S1.0 the laser hasn't fired once. I can fire it with the test button on the panel but not the GLCD.  I have changed the pwm period to 200 and reset also. I have tried the file again and this is the G code for it. the laser is still firing all the time. Obviously M3 to start and M5 to finish.

; Raster:

; Laser Min: 0%

; Laser Max: 100%

; Black Speed: 1200mm/m

; White Speed: 1200mm/m

; Resolution (mm per pixel): 0.08466666666666665mm

; Laser Spot Size: 0.5mm

; X Offset: 0.0000016093254089355469mm

; Y Offset: 0.0000016093254089355469mm 



G0 F6000

G0 Y32.004

G0 Y31.504

G0 Y31.004

G0 Y30.504

G0 Y30.004

G0 Y29.504

G0 Y29.004

G0 Y28.504

G0 Y28.004

G0 Y27.504

G0 Y27.004

G0 Y26.504

G0 Y26.004

G0 Y25.504

G0 X17.0 S0

G1 X16.5 S0.07 F1200

G1 X16.0 S0.18

G1 X15.5 S0.17

G1 X15.0 S0.06

G0 Y25.004

G0 X13.0 S0

G1 X13.5 S0.68

G1 X14.0 S0.88

G1 X14.5 S0.15

G0 X17.5 S0

G1 X18.0 S0.10

G1 X18.5 S0.91

G1 X19.0 S0.76

G0 Y24.504

G0 X20.0 S0

G1 X19.5 S0.98

G1 X19.0 S0.53

G0 X12.5 S0

G1 X12.0 S1.00

G0 Y24.004

G0 X11.0 S0

G1 X11.5 S0.98

G1 X12.0 S0.28

G0 X19.5 S0

G1 X20.0 S1.00

G1 X20.5 S0.94

G1 X21.0 S0.90

G0 Y23.504

G0 X21.5 S0

G1 X21.0 S0.81

G1 X20.5 S1.00

G1 X20.0 S1.00

G1 X19.5 S1.00

G1 X19.0 S0.94

G0 X11.0 S0

G1 X10.5 S0.96

G0 Y23.004

G0 X9.5 S0

G1 X10.0 S0.29

G1 X10.5 S0.98

G0 X12.0 S0

G1 X12.5 S0.40

G1 X13.0 S1.00

G1 X13.5 S1.00

G0 X19.0 S0

G1 X19.5 S0.16

G1 X20.0 S0.96

G1 X20.5 S0.96

G1 X21.0 S0.96

G1 X21.5 S0.14

G0 X22.0 S0

G1 X22.5 S0.23

G0 Y22.504

G0 X23.0 S0

G1 X22.5 S0.09

G1 X22.0 S0.82

G1 X21.5 S0.14

G1 X21.0 S0.96

G1 X20.5 S0.98

G0 X14.0 S0

G1 X13.5 S0.96

G1 X13.0 S1.00

G1 X12.5 S1.00

G1 X12.0 S0.94

G0 X10.0 S0

G1 X9.5 S0.95

G1 X9.0 S0.13

G0 Y22.004

G0 X9.0 S0

G1 X9.5 S0.94

G1 X10.0 S0.06

G0 X12.0 S0

G1 X12.5 S0.96

G1 X13.0 S1.00

G1 X13.5 S0.99

G1 X14.0 S0.95

G0 X15.5 S0

G1 X16.0 S0.09

G0 X20.5 S0

G1 X21.0 S1.00

G1 X21.5 S1.00

G1 X22.0 S1.00

G1 X22.5 S0.96

G1 X23.0 S1.00

G0 Y21.504

G0 X23.5 S0

G1 X23.0 S0.22

G0 X22.0 S0

G1 X21.5 S1.00

G1 X21.0 S1.00

G1 X20.5 S1.00

G1 X20.0 S0.93

G0 X16.0 S0

G1 X15.5 S0.84

G1 X15.0 S0.99

G1 X14.5 S0.99

G0 X14.0 S0

G1 X13.5 S0.98

G1 X13.0 S1.00

G1 X12.5 S1.00

G1 X12.0 S0.67

G0 X9.5 S0

G1 X9.0 S0.70

G1 X8.5 S0.96

G0 Y21.004

G0 X8.0 S0

G1 X8.5 S0.73

G1 X9.0 S0.97

G0 X12.5 S0

G1 X13.0 S0.95

G1 X13.5 S1.00

G1 X14.0 S0.88

G1 X14.5 S0.98

G1 X15.0 S0.99

G1 X15.5 S0.96

G1 X16.0 S0.96

G1 X16.5 S0.91

G0 X20.5 S0

G1 X21.0 S0.82

G1 X21.5 S0.99

G1 X22.0 S0.98

G0 X23.5 S0

G1 X24.0 S0.53

G0 Y20.504

G0 X24.0 S0

G1 X23.5 S0.99

G1 X23.0 S0.98

G1 X22.5 S0.12

G1 X22.0 S0.91

G1 X21.5 S0.99

G0 X17.0 S0

G1 X16.5 S0.08

G1 X16.0 S0.97

G1 X15.5 S0.41

G1 X15.0 S0.74

G1 X14.5 S0.95

G1 X14.0 S1.00

G1 X13.5 S0.95

G1 X13.0 S0.96

G1 X12.5 S0.97

G0 X9.0 S0

G1 X8.5 S0.23

G1 X8.0 S0.94

G0 Y20.004

G0 X7.5 S0

G1 X8.0 S0.73

G1 X8.5 S1.00

G0 X12.0 S0

G1 X12.5 S0.19

G1 X13.0 S1.00

G1 X13.5 S1.00

G1 X14.0 S1.00

G1 X14.5 S0.96

G1 X15.0 S0.25

G1 X15.5 S0.93

G1 X16.0 S1.00

G1 X16.5 S1.00

G1 X17.0 S0.96

G0 X20.0 S0

G1 X20.5 S0.18

G0 X21.5 S0

G1 X22.0 S1.00

G1 X22.5 S0.99

G1 X23.0 S0.99

G1 X23.5 S0.95

G1 X24.0 S1.00

G1 X24.5 S0.46

G0 Y19.504

G0 X24.5 S0

G1 X24.0 S1.00

G0 X23.0 S0

G1 X22.5 S0.99

G1 X22.0 S0.98

G1 X21.5 S0.99

G1 X21.0 S0.98

G1 X20.5 S0.76

G1 X20.0 S0.93

G1 X19.5 S0.98

G0 X17.0 S0

G1 X16.5 S0.96

G1 X16.0 S1.00

G1 X15.5 S1.00

G1 X15.0 S0.99

G1 X14.5 S1.00

G1 X14.0 S0.09

G1 X13.5 S0.99

G1 X13.0 S1.00

G1 X12.5 S1.00

G0 X8.5 S0

G1 X8.0 S0.40

G1 X7.5 S1.00

G0 Y19.004

G0 X7.0 S0

G1 X7.5 S0.06

G1 X8.0 S1.00

G0 X12.0 S0

G1 X12.5 S0.72

G1 X13.0 S1.00

G1 X13.5 S0.93

G1 X14.0 S0.75

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S1.00

G1 X16.0 S1.00

G1 X16.5 S0.97

G0 X19.5 S0

G1 X20.0 S0.98

G1 X20.5 S1.00

G1 X21.0 S0.98

G1 X21.5 S0.86

G1 X22.0 S0.34

G1 X22.5 S1.00

G1 X23.0 S0.92

G0 X24.0 S0

G1 X24.5 S0.88

G0 Y18.504

G0 X25.0 S0

G1 X24.5 S0.32

G1 X24.0 S0.96

G1 X23.5 S0.07

G1 X23.0 S0.15

G1 X22.5 S0.96

G1 X22.0 S0.88

G0 X21.0 S0

G1 X20.5 S1.00

G1 X20.0 S1.00

G1 X19.5 S0.98

G1 X19.0 S0.45

G0 X16.0 S0

G1 X15.5 S0.31

G1 X15.0 S1.00

G1 X14.5 S1.00

G1 X14.0 S1.00

G1 X13.5 S0.96

G1 X13.0 S1.00

G1 X12.5 S0.97

G0 X12.0 S0

G1 X11.5 S0.11

G0 X8.0 S0

G1 X7.5 S1.00

G1 X7.0 S0.93

G0 Y18.004

G0 X7.0 S0

G1 X7.5 S0.98

G1 X8.0 S0.66

G0 X10.5 S0

G1 X11.0 S0.18

G0 X12.5 S0

G1 X13.0 S0.53

G1 X13.5 S0.97

G1 X14.0 S1.00

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S1.00

G1 X16.0 S0.54

G0 X20.0 S0

G1 X20.5 S0.67

G1 X21.0 S0.97

G0 X22.0 S0

G1 X22.5 S0.69

G1 X23.0 S0.96

G1 X23.5 S1.00

G1 X24.0 S1.00

G1 X24.5 S0.96

G1 X25.0 S1.00

G0 Y17.504

G0 X25.0 S0

G1 X24.5 S0.58

G0 X23.5 S0

G1 X23.0 S1.00

G1 X22.5 S1.00

G1 X22.0 S0.98

G1 X21.5 S0.98

G1 X21.0 S0.99

G1 X20.5 S0.98

G0 X16.0 S0

G1 X15.5 S0.99

G1 X15.0 S0.99

G1 X14.5 S1.00

G1 X14.0 S1.00

G1 X13.5 S1.00

G1 X13.0 S0.97

G1 X12.5 S0.89

G0 X10.5 S0

G1 X10.0 S0.74

G0 X7.5 S0

G1 X7.0 S1.00

G0 Y17.004

G0 X7.0 S0

G1 X7.5 S0.98

G0 X9.5 S0

G1 X10.0 S0.71

G0 X12.5 S0

G1 X13.0 S0.96

G1 X13.5 S0.98

G1 X14.0 S1.00

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S1.00

G1 X16.0 S1.00

G0 X20.0 S0

G1 X20.5 S0.61

G1 X21.0 S0.99

G1 X21.5 S0.99

G1 X22.0 S0.98

G1 X22.5 S0.82

G1 X23.0 S0.97

G1 X23.5 S0.99

G0 Y16.504

G0 X25.0 S0

G1 X24.5 S1.00

G1 X24.0 S0.07

G1 X23.5 S0.84

G1 X23.0 S1.00

G0 X21.5 S0

G1 X21.0 S0.99

G1 X20.5 S1.00

G1 X20.0 S0.97

G0 X16.0 S0

G1 X15.5 S0.98

G1 X15.0 S1.00

G1 X14.5 S1.00

G1 X14.0 S1.00

G1 X13.5 S1.00

G1 X13.0 S0.98

G1 X12.5 S0.59

G0 X10.0 S0

G1 X9.5 S0.08

G1 X9.0 S0.96

G0 X7.5 S0

G1 X7.0 S0.96

G0 Y16.004

G0 X7.0 S0

G1 X7.5 S0.97

G0 X9.0 S0

G1 X9.5 S0.84

G1 X10.0 S0.40

G0 X12.5 S0

G1 X13.0 S0.77

G1 X13.5 S0.96

G1 X14.0 S1.00

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S0.99

G1 X16.0 S0.96

G0 X20.5 S0

G1 X21.0 S0.67

G1 X21.5 S0.98

G1 X22.0 S0.27

G0 X23.0 S0

G1 X23.5 S0.99

G1 X24.0 S1.00

G1 X24.5 S0.98

G1 X25.0 S0.98

G0 Y15.504

G0 X24.0 S0

G1 X23.5 S0.95

G1 X23.0 S0.99

G1 X22.5 S0.97

G1 X22.0 S0.96

G1 X21.5 S1.00

G1 X21.0 S0.96

G0 X16.0 S0

G1 X15.5 S0.97

G1 X15.0 S1.00

G1 X14.5 S1.00

G1 X14.0 S1.00

G1 X13.5 S1.00

G1 X13.0 S0.99

G1 X12.5 S1.00

G0 X7.5 S0

G1 X7.0 S0.97

G0 Y15.004

G0 X7.0 S0

G1 X7.5 S0.98

G0 X12.5 S0

G1 X13.0 S1.00

G1 X13.5 S1.00

G1 X14.0 S1.00

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S0.99

G1 X16.0 S0.69

G0 X19.0 S0

G1 X19.5 S0.98

G1 X20.0 S0.57

G0 X21.0 S0

G1 X21.5 S1.00

G1 X22.0 S1.00

G1 X22.5 S1.00

G1 X23.0 S0.46

G1 X23.5 S0.92

G1 X24.0 S0.95

G0 Y14.504

G0 X25.0 S0

G1 X24.5 S0.72

G1 X24.0 S0.98

G1 X23.5 S0.93

G0 X22.0 S0

G1 X21.5 S0.97

G1 X21.0 S0.99

G1 X20.5 S0.97

G1 X20.0 S0.99

G1 X19.5 S1.00

G1 X19.0 S1.00

G1 X18.5 S0.26

G0 X15.5 S0

G1 X15.0 S0.99

G1 X14.5 S1.00

G1 X14.0 S1.00

G1 X13.5 S1.00

G1 X13.0 S1.00

G1 X12.5 S1.00

G0 X7.5 S0

G1 X7.0 S0.97

G0 Y14.004

G0 X7.0 S0

G1 X7.5 S0.94

G0 X12.0 S0

G1 X12.5 S0.60

G1 X13.0 S1.00

G1 X13.5 S1.00

G1 X14.0 S1.00

G1 X14.5 S1.00

G1 X15.0 S1.00

G1 X15.5 S0.99

G0 X18.0 S0

G1 X18.5 S0.40

G1 X19.0 S1.00

G1 X19.5 S1.00

G1 X20.0 S1.00

G1 X20.5 S0.96

G1 X21.0 S0.10

G1 X21.5 S0.92

G1 X22.0 S0.99

G1 X22.5 S0.21

G0 X23.5 S0

G1 X24.0 S1.00

G1 X24.5 S1.00

G1 X25.0 S0.36

G0 Y13.504

G0 X24.5 S0

G1 X24.0 S0.94

G1 X23.5 S1.00

G1 X23.0 S0.98

G1 X22.5 S1.00

G1 X22.0 S1.00

G1 X21.5 S0.97

G0 X20.5 S0

G1 X20.0 S0.14

G1 X19.5 S1.00

G1 X19.0 S0.96

G1 X18.5 S0.96

G0 X15.5 S0

G1 X15.0 S0.80

G1 X14.5 S0.98

G1 X14.0 S1.00

G1 X13.5 S0.96

G1 X13.0 S1.00

G1 X12.5 S0.98

G1 X12.0 S0.97

G0 X8.0 S0

G1 X7.5 S0.82

G0 Y13.004

G0 X7.5 S0

G1 X8.0 S0.96

G0 X12.0 S0

G1 X12.5 S0.93

G1 X13.0 S1.00

G1 X13.5 S1.00

G1 X14.0 S0.06

G1 X14.5 S1.00

G1 X15.0 S0.99

G0 X19.0 S0

G1 X19.5 S0.32

G1 X20.0 S1.00

G1 X20.5 S0.73

G0 X21.5 S0

G1 X22.0 S0.98

G1 X22.5 S0.99

G1 X23.0 S1.00

G1 X23.5 S1.00

G1 X24.0 S1.00

G1 X24.5 S0.96

G0 Y12.504

G0 X24.5 S0

G1 X24.0 S0.31

G0 X23.0 S0

G1 X22.5 S0.18

G1 X22.0 S0.98

G1 X21.5 S1.00

G1 X21.0 S0.95

G1 X20.5 S0.97

G1 X20.0 S0.98

G1 X19.5 S1.00

G1 X19.0 S0.09

G0 X18.0 S0

G1 X17.5 S0.54

G0 X15.0 S0

G1 X14.5 S0.99

G1 X14.0 S1.00

G1 X13.5 S0.44

G1 X13.0 S1.00

G1 X12.5 S1.00

G1 X12.0 S0.94

G0 X8.0 S0

G1 X7.5 S0.77

G0 Y12.004

G0 X8.0 S0

G1 X8.5 S0.47

G0 X12.0 S0

G1 X12.5 S0.98

G1 X13.0 S0.99

G1 X13.5 S0.14

G1 X14.0 S0.92

G1 X14.5 S1.00

G1 X15.0 S0.82

G0 X17.0 S0

G1 X17.5 S0.13

G1 X18.0 S0.99

G1 X18.5 S0.94

G1 X19.0 S0.35

G1 X19.5 S0.93

G1 X20.0 S1.00

G1 X20.5 S0.98

G1 X21.0 S0.99

G1 X21.5 S0.24

G1 X22.0 S0.96

G1 X22.5 S0.98

G0 Y11.504

G0 X24.0 S0

G1 X23.5 S0.34

G1 X23.0 S0.36

G1 X22.5 S0.98

G1 X22.0 S1.00

G0 X20.5 S0

G1 X20.0 S0.96

G1 X19.5 S0.98

G1 X19.0 S0.96

G1 X18.5 S1.00

G1 X18.0 S0.97

G1 X17.5 S1.00

G1 X17.0 S0.98

G1 X16.5 S0.39

G0 X15.0 S0

G1 X14.5 S0.17

G1 X14.0 S1.00

G1 X13.5 S0.99

G0 X13.0 S0

G1 X12.5 S1.00

G1 X12.0 S1.00

G0 X8.5 S0

G1 X8.0 S0.69

G0 Y11.004

G0 X8.5 S0

G1 X9.0 S0.24

G0 X11.5 S0

G1 X12.0 S0.11

G1 X12.5 S0.99

G1 X13.0 S1.00

G0 X13.5 S0

G1 X14.0 S1.00

G1 X14.5 S0.98

G0 X16.5 S0

G1 X17.0 S0.74

G1 X17.5 S0.98

G1 X18.0 S1.00

G1 X18.5 S0.97

G0 X19.5 S0

G1 X20.0 S0.96

G1 X20.5 S1.00

G0 X22.0 S0

G1 X22.5 S0.99

G1 X23.0 S1.00

G1 X23.5 S0.95

G0 Y10.504

G0 X23.0 S0

G1 X22.5 S1.00

G1 X22.0 S1.00

G1 X21.5 S0.96

G1 X21.0 S0.96

G1 X20.5 S0.99

G1 X20.0 S0.96

G1 X19.5 S0.34

G0 X18.5 S0

G1 X18.0 S0.97

G1 X17.5 S1.00

G1 X17.0 S0.29

G0 X14.5 S0

G1 X14.0 S1.00

G1 X13.5 S0.95

G0 X13.0 S0

G1 X12.5 S0.38

G1 X12.0 S1.00

G1 X11.5 S0.13

G0 Y10.004

G0 X12.0 S0

G1 X12.5 S1.00

G1 X13.0 S0.98

G1 X13.5 S0.34

G1 X14.0 S0.99

G1 X14.5 S0.94

G0 X17.5 S0

G1 X18.0 S0.72

G0 X19.5 S0

G1 X20.0 S0.96

G1 X20.5 S1.00

G1 X21.0 S0.98

G1 X21.5 S0.96

G1 X22.0 S0.95

G1 X22.5 S0.97

G0 Y9.504

G0 X22.5 S0

G1 X22.0 S0.06

G0 X21.0 S0

G1 X20.5 S0.98

G1 X20.0 S1.00

G1 X19.5 S0.97

G1 X19.0 S0.99

G0 X14.5 S0

G1 X14.0 S0.94

G1 X13.5 S1.00

G1 X13.0 S0.95

G0 X12.5 S0

G1 X12.0 S0.93

G0 Y9.004

G0 X12.5 S0

G1 X13.0 S0.09

G1 X13.5 S0.94

G1 X14.0 S1.00

G1 X14.5 S0.15

G0 X19.5 S0

G1 X20.0 S0.36

G1 X20.5 S1.00

G1 X21.0 S0.96

G0 Y8.504

G0 X21.0 S0

G1 X20.5 S1.00

G1 X20.0 S0.92

G0 X14.0 S0

G1 X13.5 S0.94

G1 X13.0 S0.98

G1 X12.5 S1.00

G1 X12.0 S1.00

G1 X11.5 S0.93

G0 Y8.004

G0 X12.5 S0

G1 X13.0 S0.40

G0 X19.5 S0

G1 X20.0 S0.94

G0 Y7.504

G0 X19.0 S0

G1 X18.5 S0.66

G1 X18.0 S0.90

G1 X17.5 S0.31

G0 Y7.004

G0 X15.0 S0

G1 X15.5 S0.11

G1 X16.0 S0.17

G1 X16.5 S0.16

G1 X17.0 S0.11

G0 Y6.504

G0 Y6.004

G0 Y5.504

G0 Y5.004

G0 Y4.504

G0 Y4.004

G0 Y3.504

G0 Y3.004

G0 Y2.504

G0 Y2.004

G0 Y1.504

G0 Y1.004

G0 Y0.504



![images/14830af3bd829315ed60972fd1a4d54c.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/14830af3bd829315ed60972fd1a4d54c.jpeg)


---
**Andy Shilling** *February 19, 2017 19:56*

**+Ray Kholodovsky**

Do I need to remove the exclamation mark from the laser module pin?


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 20:01*

If your job is firing the laser but your manual line is not then you are doing something wrong. 



If you send G1 X10 S1 F600 your laser should move while firing. 



What exactly is your laser module pin currently? Copy and paste that entire line into here. 


---
**Andy Shilling** *February 19, 2017 20:06*

The head moves but doesn't fire when sending the code.

Heres a screen shot of config txt.

I'm wondering if Laser_module_pin 2.4!

and switch, laserfire.output_pin 2.5 is wrong.

![images/c81ac8768e10d24ae27315df2efb3b64.png](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/c81ac8768e10d24ae27315df2efb3b64.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 20:07*

Yes that is wrong. 

You said you placed the pot back into play. 

Laser module pin should be 2.5 

Change laserfire pin back to 2.6 to get it out of the way. 


---
**Andy Shilling** *February 19, 2017 20:09*

Ok on it now 




---
**Andy Shilling** *February 19, 2017 20:36*

**+Ray Kholodovsky**​

That worked a treat all seems to be working now thank you. I just pulled this image off the web to see what would happen, I'm very happy with that as a first run, especially as I've not bothered aligning the laser yet. On a side note would you say the lines are a little spaced out although the picture isn't distorted in any way.

![images/afa78d2105ac9cdd7c8ed6f5a35bf160.jpeg](https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/images/afa78d2105ac9cdd7c8ed6f5a35bf160.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 20:39*

Cool. Fiddle with the laser beam diameter in LW. Make a new post with this pic, it's a great start. 


---
**Andy Shilling** *February 19, 2017 20:44*

Ok will do mate, is there a standard bean diameter it is it just adjust- run job and see what the outcome is?


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 20:45*

Anywhere from 0.1 to 0.5 

What are you using in that job?


---
**Andy Shilling** *February 19, 2017 20:51*

It's sat at .5 at the moment so next run I'll drop down to .3 i think, Do you know if Lw is happy to run in .05's if so I'll try .25 so in theory it will halve the lines.


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2017 20:55*

Should be ok. 0.2 is also a happy value. 


---
**Andy Shilling** *February 19, 2017 21:06*

Right I'll do it tomorrow and post the comparison  when it's done. Cheers Ray


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/abdH2fNhThi) &mdash; content and formatting may not be reliable*
