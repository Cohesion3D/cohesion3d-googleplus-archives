#!/usr/bin/python
# -*- coding: utf-8 -*-

# This is a really quick and dirty converter from a Friends+Me Google+ Exporter
# to a jekyll site.  It is barely functional for one community export.
# Expect to modify it if you try to use it.

# Basically, there is very little chance that this will work for you
# in any way as it stands. It barely works for me. If you don't know
# Python and regular expressions you don't have much chance.  I have
# not looked at which of the python modules I used are in the standard
# library and which aren't.

# Copyright Michael K Johnson
# You may use any part of this under the Creative Commons Attribution 4.0 International (CC BY 4.0) License
# https://creativecommons.org/licenses/by/4.0/

# Usage:
# cd /path/to/jekyll/base/dir
# import-friendsplus path/to/export.json path/to/google-plus-image-list.csv

# then start fixing whatever breaks, some of which you might not notice.

# If you have links to youtube, add the file _includes/youtubePlayer.html containing:
#     <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ include.id }}" frameborder="0" allowfullscreen></iframe>
# or equivalent

import codecs
import csv
import dateutil.parser
import hashlib
import json
import os
import re
import sys
import urllib
import urllib2

from string import Template

import epdb
sys.excepthook = epdb.excepthook()

sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

extraNLRe = re.compile(r'\n\n\n*')
youtubeRe = re.compile(r'https?://(?:www\.)?youtu(?:be\.com|\.be)/(?:watch\?v=|v/)?([^?&]*)')
missingVideoRe = re.compile(r'googleusercontent.*\.mp4\.')
googleImgRe = re.compile(r'https://plus.google.com/photos/([^/]*)/albums/([^/]*)/([^/]*)')

imageBase = 'https://gitlab.com/Cohesion3D/cohesion3d-googleplus-archives/raw/master/'

def markdownSafe(text):
    return text.replace('|', '&#x7c;')

if len(sys.argv) > 1:
    feed = json.load(open(sys.argv[1], 'rb'))
else:
    feed = json.load(open('import/feed.json', 'rb'))
imgMap = {}
try:
    with open(sys.argv[2], 'rb') as imgData:
        # "URL";"IsDownloaded";"FileName";"FilePath";"FileSize"
        imgs = csv.DictReader(imgData, delimiter=';')
        for row in imgs:
            imgMap[row['URL']] = row
except:
    # no new images expected, just using cache of already-fetched images
    pass

# to preserve the mapping for future changes, as well as downloaded images
imgCache = {}
if os.path.exists('import/map.json'):
    imgCache = json.load(open('import/map.json', 'rb'))

cats = feed['accounts'][0]['communities'][0]['categories']

categories = {}
categoryNames = {}
categoryList = []
for category in cats:
    categories[category['name']] = category
    # clean up category names for directories
    catname = category['name']
    catname = catname.replace(' (3&4)', '')
    catname = catname.replace('&', 'and')
    catname = catname.replace('...', '')
    catsafe = catname.lower()
    catname = catname.replace(':', '&#x3a;')
    categoryNames[category['name']] = catname
    codecs.open('_cats/' + catname + '.md', 'w', 'utf-8').write('\n'.join((
        '---',
        'layout: category',
        'category: ' + json.dumps(catname),
        '---',
        '',
    )))
    categoryList.append((catname, catsafe))

posts = []
for category in categories:
    for post in categories[category]['posts']:
        post['category'] = category
        posts.append(post)

def replaceURL(url):
    if url in imgCache:
        imagePath = 'images/' + imgCache[url]
    else:
        if url in imgMap:
            imagePath = 'images/' + imgMap[url]['FileName']
            imgCache[url] = imgMap[url]['FileName']
            if not os.path.exists(imagePath):
                open(imagePath, 'wb').write(open(imgMap[url]['FilePath'], 'rb').read())
        return 'missing image', url
    return imagePath, imageBase + imagePath

def _formatLink(text, link):
    return '[' + markdownSafe(text) + '](' + link + ')'

def formatLink(text, link):
    y = youtubeRe.match(link)
    if y and not '/user/' in link and not '/channel/' in link:
        if text == None or text == '':
            text = y.group(1)
        return '\n{% include youtubePlayer.html id="' + y.group(1) + '" %}\n[' + markdownSafe(text) + '](' + link + ')'

    if link in imgCache:
        # this is an image we downloaded separately
        return '!' + _formatLink(*replaceURL(link))
    else:
        g = googleImgRe.match(link)
        if g:
            return '!' + _formatLink(*replaceURL(text))
    if False:
        # if we find image URLs to scrape
        h = hashlib.sha1()
        h.update(link)
        imageBasename = h.digest()
        try:
            data = urllib2.urlopen(link)
        except:
            print 'lost image data, sorry!'
            return _formatLink(text, link)
        code = data.getcode()
        if code != 200:
            print 'lost image data, sorry!'
            return _formatLink(text, link)
        info = data.info()
        contentType = info.getheader('Content-Type')
        extensions = {
            'image/jpeg': '.jpg',
            'image/png': '.png',
            'image/gif': '.gif',
        }
        if contentType in extensions:
            imageName = imageBasename + extension
            imagePath = 'images/' + imageName
            open(imagePath, 'wb').write(data.read())
            imgCache[link] = imageName
        else:
            print 'link: ' + link
            print 'unsupported content type: ' + contentType
            return '\n**Image import unsupported for image**\n' + _formatLink(text, link)

    return _formatLink(text, link)

def formatImage(image):
    videoWarning  = ''
    video = None
    localname  = ''
    proxy = image['proxy']

    if missingVideoRe.search(proxy):
        mp4 = proxy.rsplit('.jpeg')[0]
        if mp4 != proxy and mp4 in imgCache:
            # this is an image we downloaded separately
            _, video = replaceURL(mp4)
        else:
            videoWarning = '\n**Video content missing for image ' + mp4 + '**\n'

    localname, image = replaceURL(proxy)

    if video:
        return '<a href="' + video + '">' + formatLink(localname, proxy) + '</a>'
    else:
        return videoWarning + '!' + formatLink(localname, image)

def formatImages(post):
    if 'images' in post:
        return '\n'.join(formatImage(x) for x in post['images'])
    return ''

def formatAuthor(post):
    return post['author']['name']

def formatDate(post):
    return dateutil.parser.parse(post['createdAt']).strftime(('%B %d, %Y %H:%M'))

def formatMessageElement(e):
    if e[0] == 0:
        if len(e) > 2:
            # We'll use HTML rather than markdown because markdown doesn't reliable nest in output
            if e[2].get('italic', False):
                return '<i>' + e[1] + '</i>'
            elif e[2].get('bold', False):
                return '<b>' + e[1] + '</b>'
            elif e[2].get('strikethrough', False):
                # choose s tag arbitrarily over del as more likely to be what users meant
                return '<s>' + e[1] + '</s>'
        return e[1]
    elif e[0] == 1:
        return '\n\n'
    elif e[0] == 2:
        return formatLink(e[1], e[2])
    elif e[0] == 3:
        return '**+' + e[1] + '**'
    elif e[0] == 4:
        # G+ hashtag which really doesn't really import per se
        return e[1]
    else:
        epdb.st()

def formatMessage(post):
    if 'message' in post:
        return ''.join(formatMessageElement(x) for x in post['message'])
    return ''

def formatComment(comment):
    s = Template('''---
**${author}** *${date}*

${message}
''')
    author = formatAuthor(comment)
    date = formatDate(comment)
    message = formatMessage(comment)
    return s.substitute(locals())

def formatComments(post):
    return '\n\n'.join(formatComment(x) for x in post['comments'])

def formatTitle(post):
    endPunct = ('.', '?', ';', '!')
    punct = ('.', '?', ';', '!', ',', ':', '(', ')', '*', '+', '@', '#', '&')
    quotes = ('"', "'")
    titleWords = []
    for fragment in [x[1] for x in post['message'] if x[0] in (0, 3)]:
        titleWords.extend(fragment.split())
    # remove zero-width spaces because they break JSON; also just because...
    titleWords = [x.replace(u'\u200b', '') for x in titleWords]
    # zero-width spaces were considered words
    titleWords = [x for x in titleWords if x]

    if len(titleWords):
        if len(titleWords) > 10:
            # long titles will be cut off at end of first sentence after first few words
            for i in range(6, len(titleWords)):
                if titleWords[i][-1] in endPunct:
                    break
                if titleWords[i][-1] in ('"', "'") and (len(titleWords[i]) == 1 or titleWords[i][-2] in endPunct):
                    break
            titleWords = titleWords[0:i+1]
        if titleWords[-1][-1] == '.' and len(titleWords[-1]) > 1 and titleWords[-1][-2] != '.':
            # cut off final '.' but not elipsis
            titleWords[-1] = titleWords[-1][:-1]
        title = ' '.join(titleWords)
        # too long doesn't fit
        while len(title) > 225:
            titleWords.pop(-1)
            title = ' '.join(titleWords)
    else:
        niceTime = dateutil.parser.parse(post['createdAt']).strftime('%B %d, %Y %H:%M')
        title = 'Shared on ' + niceTime + '...\n'
        titleWords = ['Shared', 'on', 'Google+' 'at', niceTime]

    postDate = dateutil.parser.parse(post['createdAt']).strftime(('%Y-%m-%d'))
    titleFilename = '-'.join((postDate, '-'.join(titleWords)))
    titleFilename = ''.join(x for x in titleFilename if x not in punct)
    titleFilename = ''.join(x for x in titleFilename if x not in quotes)
    titleFilename = titleFilename.replace('/', '-')
    titleFilename = '-'.join(x for x in titleFilename.split('-') if x)
    titleFilename = titleFilename + '.md'
    # jekyll doesn't like emoji in JSON title, sadly
    title = ''.join(x for x in title if ord(x) < 128)
    titleFilename = ''.join(x for x in titleFilename if ord(x) < 128)
    return titleFilename, title

def formatPost(post):
    s = Template('''---
layout: post
title: ${title}
date: ${date}
category: ${category}
author: ${author}
---
${message}

${image}

${images}

**${author}**

---
${comments}

---
*Imported from [Google+](https://plus.google.com/${postid}) &mdash; content and formatting may not be reliable*
''')
    if not post['isPublic']:
        print 'private, really?'
        epdb.st()

    titleFilename, title = formatTitle(post)
    title = json.dumps(title)
    date = formatDate(post)
    category = json.dumps(categoryNames[post['category']])
    author = formatAuthor(post)
    author = json.dumps(author)
    image = ''
    if 'image' in post:
        image = formatImage(post['image'])
    message = formatMessage(post)
    images = formatImages(post)
    comments = formatComments(post)
    postid = '/posts/'.join(post['publicId'])

    return titleFilename, s.substitute(locals())

for post in posts:
    titleFilename, postText = formatPost(post)
    #print titleFilename
    codecs.open('_posts/' + titleFilename, 'w', 'utf-8').write(postText)

json.dump(imgCache, open('import/map.json', 'wb'),sort_keys=True, indent=2)
